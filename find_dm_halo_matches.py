#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

import h5py as h5

# from scipy.integrate         import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

from scipy.optimize import linprog

from scipy.spatial import Delaunay

import ctypes

import multiprocessing as mp

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
BOX_SIZE = 33.885 #50 * 0.6777 #kpc
# SCALE_A  = 1
# DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

NO_GROUP_NUMBER = 1073741824 #2**30

PEANO_SO_LOC = '/home/mwilkinson/EAGLE/scripts/'
# PEANO_SO_LOC = '/home/mwilkins/EAGLE/scripts/'


def parametrized(dec):
    """This decorator can be used to create other decorators that accept arguments
    https://stackoverflow.com/a/64693650
    """

    def layer(*args, **kwargs):
        def repl(f):
            return dec(f, *args, **kwargs)

        return repl

    return layer


@parametrized
def sigsev_guard(fcn, default_value=None, timeout=None):
    """Used as a decorator with arguments.
    The decorated function will be called with its input arguments in another process.

    If the execution lasts longer than *timeout* seconds, it will be considered failed.

    If the execution fails, *default_value* will be returned.
    https://stackoverflow.com/a/64693650
    """

    def _fcn_wrapper(*args, **kwargs):
        q = mp.Queue()
        p = mp.Process(target=lambda q: q.put(fcn(*args, **kwargs)), args=(q,))
        p.start()
        p.join(timeout=timeout)
        exit_code = p.exitcode

        if exit_code == 0:
            return q.get()

        #logging.warning('Process did not exit correctly. Exit code: {}'.format(exit_code))
        return default_value

    return _fcn_wrapper


def my_read(particle_data_location, halo_data_location, kdtree_location, snap, bits):

    fn = f'{halo_data_location}groups_{snap}/eagle_subfind_tab_{snap}.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    # FirstSub = np.zeros(TotNgroups, dtype=np.int32)

    # # Subhalo arrays
    # GroupNumber = np.zeros(TotNsubgroups, dtype=np.int32)
    # SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int32)
    # SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    NGrp_c = 0
    # NSub_c = 0

    print('TotNGroups:', TotNgroups)
    print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = f'{particle_data_location}groups_{snap}/eagle_subfind_tab_{snap}.{str(ifile)}.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            # Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                # FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            # if Nsubgroups > 0:
            #     GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
            #     SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]
            #     SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]

                # NSub_c += Nsubgroups

    print('Loaded halos')

    #particles
    fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs
        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    print('NumPartTot:', NumPartTot)

    # PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    IDDM = np.zeros(NumPartTot[1], dtype=np.uint64)
    GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    BindingEnergyDM = np.zeros(NumPartTot[1], dtype=np.float32)

    NDM_c = 0

    for ifile in range(FNumPerSnap):

        fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.{str(ifile)}.hdf5'
        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[1] > 0:

                # PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"][()]
                IDDM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/ParticleIDs"][()]
                BindingEnergyDM[NDM_c:NDM_c + NumParts[1]] = fs["/PartType1/ParticleBindingEnergy"][()]

                NDM_c += NumParts[1]

    print(NDM_c)

    print('loaded particles')

    dm_tree_file_name = f'{kdtree_location}{snap}_dm_tree.pickle'
    start_time = time.time()
    print('DM tree file name ', dm_tree_file_name)
    if os.path.exists(dm_tree_file_name):
        print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            dm_kd_tree = pickle.load(dm_tree_file)
        print('Opened dm kdtree')

    else:
        print('Calculating')
        #load positions in EAGLE units
        PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
        NDM_c = 0
        for ifile in range(FNumPerSnap):
            fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.{str(ifile)}.hdf5'
            with h5.File(fn, "r") as fs:
                Header = fs['Header'].attrs
                NumParts = Header['NumPart_ThisFile']
                if NumParts[1] > 0:
                    PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                    NDM_c += NumParts[1]
        #make tree
        dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
        print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(dm_kd_tree, dm_tree_file, protocol=4)
        print('Saved dm kdtree.')
    print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    print('Done kdtree')

    # start_time = time.time()
    # PosDM_ICs = decode_peano_ids(IDDM, bits)
    # print('Decoded IC positions')
    # print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    #t=0 positions from IDs
    #needs snap so that order matches
    IC_positions_decoded_file_name = f'{kdtree_location}{snap}_ID_decoded_positions.hdf5'
    start_time = time.time()
    print('IC positions decoded name ', IC_positions_decoded_file_name)

    if os.path.exists(IC_positions_decoded_file_name):
        print('Loading')

        with h5.File(IC_positions_decoded_file_name, "r") as fs:
            PosDM_ICs = fs["PartType1/ICCoordinates"][()]
            IC_IDDMs = fs["PartType1/ParticleIDs"][()]

        if not np.all(IC_IDDMs == IDDM):
            raise ValueError('IDs do not mactch!')

        print('Opened IC positions')

    else:
        print('Calculating')
        #make tree
        PosDM_ICs = decode_peano_ids(IDDM, bits)
        print('Decoded IC positions in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with h5.File(IC_positions_decoded_file_name, "w") as fs:
            quant = fs.create_group('PartType1')
            quant.create_dataset('ICCoordinates', data=PosDM_ICs)
            quant.create_dataset('ParticleIDs', data=IDDM)
        print('Saved dm decoded IC positions.')
    print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    print('loaded eveything')

    return (TotNgroups, TotNsubgroups,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential, dm_kd_tree,
            IDDM, GrpNum_DM, BindingEnergyDM, PosDM_ICs)


def decode_peano_ids(ids, bits, BOX_SIZE=BOX_SIZE, so_loc=PEANO_SO_LOC):

    n = len(ids)

    key = (ctypes.c_uint64 * n)(*ids)

    c_peano_hilbert_key_inverse = ctypes.CDLL(f'{so_loc}peano_for_py.so').peano_hilbert_key_inverse_

    x_array = (ctypes.c_int64 * n)(*[0] * n)
    y_array = (ctypes.c_int64 * n)(*[0] * n)
    z_array = (ctypes.c_int64 * n)(*[0] * n)

    c_peano_hilbert_key_inverse(ctypes.byref(key), ctypes.c_int64(bits),
                                ctypes.byref(x_array), ctypes.byref(y_array), ctypes.byref(z_array),
                                ctypes.c_int64(n))

    x_np = np.array(x_array)
    y_np = np.array(y_array)
    z_np = np.array(z_array)

    pos_ics = np.vstack((x_np, y_np, z_np)).T / (2 ** bits) * BOX_SIZE

    pos_ics = pos_ics.astype(np.float32)

    return pos_ics

@sigsev_guard(default_value=False, timeout=300)
def in_hull(hull, p):
    """
    Test if points in `p` are in `hull`

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    https://stackoverflow.com/a/16898636
    """
    if not isinstance(hull, Delaunay):
        hull = Delaunay(hull)
    return hull.find_simplex(p)>=0


def my_compare_ids(TotNgroups0, TotNsubgroups0,
                   Group_M_Crit200_0, Group_R_Crit200_0, GroupCentreOfPotential0, dm_kd_tree0,
                   IDDM0, GrpNum_DM0, BindingEnergyDM0, PosDM_ICs0,
                   TotNgroups1, TotNsubgroups1,
                   Group_M_Crit200_1, Group_R_Crit200_1, GroupCentreOfPotential1, dm_kd_tree1,
                   IDDM1, GrpNum_DM1, BindingEnergyDM1, PosDM_ICs1,
                   nbound=50, nmin=50):

    most_matching_ids_one_to_zero = np.zeros(TotNgroups0, dtype=np.int32)
    most_matching_ids_zero_to_one = np.zeros(TotNgroups1, dtype=np.int32)

    for group_index in range(TotNgroups0):
        # try:

        r200 = Group_R_Crit200_0[group_index]  # eagle units
        centre_of_potential = GroupCentreOfPotential0[group_index]

        dm_index_mask = dm_kd_tree0.query_ball_point(x=centre_of_potential, r=r200)
        dm_index_mask = np.array(dm_index_mask)

        # Couldn't work out how to make it periodic.
        # centre positions instead
        if group_index == 0:
            PosDM_ICs1 = (PosDM_ICs1 - centre_of_potential + BOX_SIZE / 2) % BOX_SIZE
        else:
            PosDM_ICs1 = (PosDM_ICs1 - centre_of_potential + GroupCentreOfPotential0[group_index-1]) % BOX_SIZE

        best_group = NO_GROUP_NUMBER

        ndm = int(np.sum(dm_index_mask))
        if ndm <= nmin:
            pass

        else:
            PosDMs0 = PosDM_ICs0[dm_index_mask, :]

            if nbound is not None:
                EnergyDMs = BindingEnergyDM0[dm_index_mask]
                PosDMs0 = PosDMs0[np.argsort(EnergyDMs)[:min(nbound, ndm)], :]

            #Couldn't work out how to make it periodic.
            #centre positions instead
            PosDMs0 = (PosDMs0 - centre_of_potential + BOX_SIZE / 2) % BOX_SIZE
            # PosDM_ICs1 = (PosDM_ICs1 - centre_of_potential + BOX_SIZE / 2) % BOX_SIZE

            #the main calculation
            #check every DM particle
            full_mask = in_hull(PosDMs0, PosDM_ICs1)

            if full_mask is None:
                print(f'No match for group {group_index}')

            else:

                group_numbers = GrpNum_DM1[full_mask]

                gns, counts = np.unique(np.abs(group_numbers), return_counts=True)

                #remove ungrouped partices
                if NO_GROUP_NUMBER in gns:
                    gns = gns[:-1]
                    counts = counts[:-1]

                if len(counts) > 0:
                    best_group = gns[np.argmax(counts)]
                else:
                    best_group = NO_GROUP_NUMBER

            #uncentre
            # PosDM_ICs1 = (PosDM_ICs1 + centre_of_potential + BOX_SIZE / 2) % BOX_SIZE

        most_matching_ids_one_to_zero[group_index] = best_group

                # except Exception as e:
        #     print('Exception was thrown calculating halo properties for group: ', group_index)
        #     print(e)
        #     pass

        if group_index % 100 == 0 or group_index < 10:
            print(f'done {group_index}')

    else:
        try:
            PosDM_ICs1 = (PosDM_ICs1 + centre_of_potential + BOX_SIZE / 2) % BOX_SIZE
        except NameError:
            pass

    #TODO might not be necessary to do both
    for group_index in range(TotNgroups1):
        # try:

        r200 = Group_R_Crit200_1[group_index]  # eagle units
        centre_of_potential = GroupCentreOfPotential1[group_index]

        dm_index_mask = dm_kd_tree1.query_ball_point(x=centre_of_potential, r=r200)
        dm_index_mask = np.array(dm_index_mask)

        # Couldn't work out how to make it periodic.
        # centre positions instead
        if group_index == 0:
            PosDM_ICs0 = (PosDM_ICs0 - centre_of_potential + BOX_SIZE / 2) % BOX_SIZE
        else:
            PosDM_ICs0 = (PosDM_ICs0 - centre_of_potential + GroupCentreOfPotential1[group_index-1]) % BOX_SIZE

        ndm = int(np.sum(dm_index_mask))
        if ndm <= 4:
            best_group = np.nan

        else:
            PosDMs1 = PosDM_ICs1[dm_index_mask, :]

            if nbound is not None:
                EnergyDMs = BindingEnergyDM1[dm_index_mask]
                PosDMs1 = PosDMs1[np.argsort(EnergyDMs)[:min(nbound, ndm)], :]

            #Couldn't work out how to make it periodic.
            #centre positions instead
            PosDMs1 = (PosDMs1 - centre_of_potential + BOX_SIZE / 2) % BOX_SIZE
            # PosDM_ICs1 = (PosDM_ICs1 - centre_of_potential + BOX_SIZE / 2) % BOX_SIZE

            #the main calculation
            #check every DM particle
            full_mask = in_hull(PosDMs1, PosDM_ICs0)

            group_numbers = GrpNum_DM0[full_mask]

            gns, counts = np.unique(np.abs(group_numbers), return_counts=True)

            #remove ungrouped partices
            no_group = False
            if NO_GROUP_NUMBER in gns:
                no_group = True
                gns = gns[:-1]
                counts = counts[:-1]

            if len(counts) > 0:
                best_group = gns[np.argmax(counts)]
            elif no_group:
                best_group = NO_GROUP_NUMBER
            else:
                best_group = np.nan

            #uncentre
            # PosDM_ICs1 = (PosDM_ICs1 + centre_of_potential + BOX_SIZE / 2) % BOX_SIZE

        most_matching_ids_zero_to_one[group_index] = best_group

                # except Exception as e:
        #     print('Exception was thrown calculating halo properties for group: ', group_index)
        #     print(e)
        #     pass

        if group_index % 100 == 0 or group_index < 10:
            print(f'done {group_index}')

    return most_matching_ids_one_to_zero, most_matching_ids_zero_to_one


def my_write(output_data_location, snap,
             most_matching_ids_one_to_zero, most_matching_ids_zero_to_one):

    file_name = f'{output_data_location}{snap}_matched_haloes.hdf5'
    print('Output file ', file_name)
    # profile file
    with h5.File(file_name, 'w') as output:

        quant = output.create_group('MatchedHaloes')

        quant.create_dataset('OneToZero', data = most_matching_ids_one_to_zero)
        quant.create_dataset('ZeroToOne', data = most_matching_ids_zero_to_one)

    print('File written')

    return


def do_everything(particle_data_location0, halo_data_location0, kdtree_location0,
                  particle_data_location1, halo_data_location1, kdtree_location1,
                  snap, output_data_location):

    start_time = time.time()
    if '7x' in particle_data_location0: bits = 21
    else: bits = 14
    data0 = my_read(particle_data_location0, halo_data_location0, kdtree_location0, snap, bits)

    if '7x' in particle_data_location1: bits = 21
    else: bits = 14
    data1 = my_read(particle_data_location1, halo_data_location1, kdtree_location1, snap, bits)
    print(str(np.round(time.time() - start_time, 1)))

    (most_matching_ids_one_to_zero, most_matching_ids_zero_to_one
     ) = my_compare_ids(*data0, *data1, nbound=50)
    print(str(np.round(time.time() - start_time, 1)))

    my_write(output_data_location, snap,
             most_matching_ids_one_to_zero, most_matching_ids_zero_to_one)
    print('Finished snap in ' + str(np.round(time.time() - start_time, 1)) + 's')

    return


if __name__ == '__main__':
    _, snap_index = sys.argv

    snap_index = int(snap_index)

    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    # #ozstar
    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location0 = particle_data_location_list[snap_index // 4]
    particle_data_location1 = particle_data_location_list[1 + (snap_index // 4)]
    halo_data_location0 = halo_data_location_list[snap_index // 4]
    halo_data_location1 = halo_data_location_list[1 + (snap_index // 4)]
    output_data_location0 =  output_data_location_list[snap_index // 4]
    output_data_location1 =  output_data_location_list[1 + snap_index // 4]
    kdtree_location1 = output_data_location0
    kdtree_location0 = output_data_location1
    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]
    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    do_everything(particle_data_location0, halo_data_location0, kdtree_location0,
                  particle_data_location1, halo_data_location1, kdtree_location1,
                  snap, output_data_location0)

    print('Done all')

    pass
