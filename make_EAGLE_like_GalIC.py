#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 15:14:43 2021

@author: matt
"""
from functools import lru_cache

import numpy as np

from scipy.special   import kn, iv
from scipy.special   import lambertw
from scipy.integrate import quad
from scipy.integrate import dblquad
from scipy.optimize  import brentq

# import astropy.units     as u
# import astropy.constants as cst

# GalIC_G = 43018.7 / 43009.1727 * cst.G
#.to(u.kpc * u.km**2 / (10**10 * u.Msun) / u.s**2)

GalIC_G = 43018.7 #kpc (km/s)^2 / (10^10 Msun)
GRAV_CONST = GalIC_G #kpc (km/s)^2 / (10^10 Msun)

# HUBBLE                   = 3.2407789e-18      #  h/sec
# UnitLength_in_cm         = 3.085678e21        #  1.0 kpc
# UnitVelocity_in_cm_per_s = 1e5                #  1 km/sec
# Hub = HUBBLE * UnitLength_in_cm / u.kpc / (
#   UnitVelocity_in_cm_per_s / u.km * u.s)
# GalIC_H = 3.2407789e-18 * 3.085678e21 / 1e5 * u.km / u.kpc / u.s

GalIC_H = 0.1 # km / s / kpc, 100 km/s/Mpc
#translate halo properties

#all distances in kpc
#all speeds in km/s
#all masses in 10^10 Msun

def signif(x, p=6):
    '''Round to sig fig. From stack overflow
    '''
    x = np.asarray(x)
    x_positive = np.where(np.isfinite(x) & (x != 0), np.abs(x), 10**(p-1))
    mags = 10 ** (p - 1 - np.floor(np.log10(x_positive)))
    return np.round(x * mags) / mags

def r_200_for_GalIC(V200=200):
    '''Not actually r_200. Isothermal sphere parameter approximations used in GalIC.
    From Ho, Mao & White 1998. Eqn 2
    '''

    r_200 = V200 / (10 * GalIC_H)

    return(r_200)

def M_200_for_GalIC(V200):
    '''Not actually M_200. Isothermal sphere parameter approximations used in GalIC.
    From Ho, Mao & White 1998. Eqn 2
    '''

    M_200 = V200**3 / (10 * GalIC_G * GalIC_H)

    return(M_200)

def v_200_for_GalIC(M200):
    '''Not actually M_200. Isothermal sphere parameter approximations used in GalIC.
    From Ho, Mao & White 1998. Eqn 2
    '''

    v_200 = np.power(10 * GalIC_G * GalIC_H * M200, 1/3)

    return(v_200)

def nfw_parameters_from_GalIC(V200, conc):
    '''
    '''

    r200 = r_200_for_GalIC(V200)
    R_s   = r200 / conc

    M200 = V200**2 * r200 / GalIC_G

    rho_0 = M200 / (4 * np.pi * R_s**3 * (np.log(1+conc) - conc/(1+conc)))

    return(rho_0, R_s)

# # This is wrong
# def hernquist_mass_from_GalIC(v_200, c):
#   '''
#   '''

#   M_dm = v_200**3 / (10 * GalIC_G * GalIC_H)

#   return(M_dm)

def hernquist_a_from_GalIC(v_200, c):
    '''From Yurin & Sprignel 2014. Eqn 48
    '''
    r_200 = r_200_for_GalIC(v_200)

    a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))

    return(a)

# #Not needed
# def hernquist_parameters_from_GalIC(v_200, conc):
#   '''
#   '''
#   M = hernquist_mass_from_GalIC(v_200, conc)
#   a = hernquist_a_from_GalIC(v_200, conc)

#   return(M, a)

###############################################################################

def fc(c):
    '''Small calculation used in NFW stuff often
    '''
    f = c * (0.5 - 0.5 / (1 + c)**2 - np.log(1 + c) / (1 + c)) / (
        np.log(1 + c) - c / (1 + c))**2
    return(f)

def j_for_GalIC(v_200, c, lamb):
    '''GalIC structure.c line 93.
    '''

    j = lamb * np.sqrt(GalIC_G * M_200_for_GalIC(v_200)**3 *
                       2 * r_200_for_GalIC(v_200) / fc(c))

    return(j)

def GalIC_disk_starting_guess_R_s(v_200, c, lamb):
    '''GalIC structure.c line 108.
    '''
    R_s = 1 / np.sqrt(2) * lamb / fc(c) * r_200_for_GalIC(v_200)

    return(R_s)

def GalIC_jdisk_int(r, M_halo, a, M_bulge, b, M_disk, h):
    '''Function to be integrated.
    GalIC structure.c jdisk_int function lines 27 to 50.
    '''
    G = 43018.7

    if r > 1e-10 * a:
        vc2 = G * (M_halo * np.square(r / (r + a)) + M_bulge * np.square(r / (r + b))) / r
    else:
        vc2 = 0

    y = r / (2 * h)

    if y > 1e-4:
        vc2 += r * y * G * M_disk / (h*h) * (iv(0,y) * kn(0,y) - iv(1,y) * kn(1,y))

    vc = np.sqrt(vc2)

    out = np.square(2 * y) * vc * np.exp(-2 * y) #* 0.760722

    return(out)

def structure_disk_angmomentum(R200, M_halo, a, M_bulge, b, M_disk, h):
    '''Does the integration. Needs all inputs to be quantities so they can be put
    int the right units.
    '''
    if M_bulge != 0:
        raise NotImplementedError('Need to swap bulge out for spherical exponential')

    result = quad(GalIC_jdisk_int, 0,min(30*h,R200),
                  args=(M_halo,  a,
                        M_bulge, b,
                        M_disk,  h))

    out = result[0] * M_disk

    return (out)

@lru_cache(maxsize=10_000)
def GalIC_disk_size(R200, M200, Halo_C, Lambda, JD, DiskHeight,
                    M_halo, a, M_bulge, b, M_disk):
    '''The rest of GalIC structure.c
    '''
    if M_disk > 0:

        #guess
        #angular momentum of halo
        jhalo = Lambda * np.sqrt(2 * GalIC_G * M200**3 * R200 / fc(Halo_C))
        #desired angular momentum of disk
        jdisk = JD * jhalo

        #guess at disk length
        h = np.sqrt(2.0) / 2.0 * Lambda / fc(Halo_C) * R200

        #run through once
        #actual angular momentum of disk with these values
        jd = structure_disk_angmomentum(R200, M_halo, a, M_bulge, b, M_disk, h)

        #actual length with these values and difference
        hnew = jdisk / jd * h
        dh = hnew - h

        #check the difference isn't too big
        if abs(dh) > 0.5 * h:
          dh = 0.5 * h * dh / abs(dh)
        else:
          dh = dh * 0.1

        #adjust length and height
        h += dh
        # z0 = DiskHeight * h

        #while differences is too large
        while (abs(dh) / h > 1e-5):

            #actual angular momentum of disk with these values
            jd = structure_disk_angmomentum(R200, M_halo, a, M_bulge, b, M_disk, h)

            #actual length with these values and difference
            hnew = jdisk / jd * h
            dh = hnew - h

            #check the difference isn't too big
            if abs(dh) > 0.5 * h:
              dh = 0.5 * h * dh / abs(dh)
            else:
              dh = dh * 0.1

            #adjust length and height
            h += dh
            # z0 = DiskHeight * h
        return(h)
    return(1e-10)

###############################################################################

def disk_mass(r, M_d, r_s):
    '''For cylindrical / assume thin disk.
    '''
    M = M_d * (1 - (1+r/r_s) * np.exp(-r/r_s))

    return(M)

def hernquist_mass(r, M_dm, a):
    '''Hernquist 1990. Eqn 3
    '''
    M = M_dm * (r / (r + a))**2

    return(M)

def hernquist_X(s):
    '''Hernquist 1990. Eqn 33 and 34
    '''
    if s == 0:
        X = np.inf
    elif s < 1:
        X = 1/np.sqrt(1 - s*s) * np.arccosh(1/s)
    elif s == 1:
        X = 1
    else:
        X = 1/np.sqrt(s*s - 1) * np.arccos(1/s)

    # X = (s < 1) /np.sqrt(1 - s*s) * np.arccosh(1/s) + (
    #   s >= 1) /np.sqrt(s*s - 1) * np.arccos(1/s)

    return(X)

#TODO make this better
def hernquist_mass_cylindrical(R, M_dm, a):
    '''Hernquist 1990. Eqn 37
    '''
    try:
        if M_dm == 0:
            return(0)
        else:
            s = R/a
            M = M_dm * s**2 * (hernquist_X(s) - 1) / (1 - s**2)

    except ValueError:
        #R is an array
        M = [hernquist_mass_cylindrical(R_, M_dm, a) for R_ in R]
        M = np.array(M)
    return(M)

def GalIC_half_mass_zero(R, M_d, h, M_b, b):
    '''Need to find zero of this eqn.
    '''
    zero = (M_d + M_b) / 2 - (disk_mass(R, M_d, h) + hernquist_mass(R, M_b, b))

    return(zero)

def GalIC_half_mass_radii(M_d, h, M_b, b):
    '''Stellar half mass
    '''
    R = brentq(GalIC_half_mass_zero, 0.1*min(h, b), 10*max(h, b),
               args=(M_d, h, M_b, b))

    return(R)


def get_analytic_hernquist_density(r_measure, M, a):
    # easy to work out

    rho_r = M  / (2 * np.pi) * a / r_measure / (r_measure + a) ** 3

    return rho_r


def get_analytic_hernquist_dispersion(r_measure, M, a):
    '''
    '''

    disp_1d = np.sqrt(GRAV_CONST * M / (12 * a) * (
        12 * r_measure * (r_measure + a) ** 3 / a ** 4 * np.log1p(a / r_measure) -
        r_measure / (r_measure + a) * (25 + 52 * r_measure / a +
                                     42 * (r_measure / a) ** 2 + 12 * (r_measure / a) ** 3)))

    return disp_1d


def get_analytic_hernquist_potential_derivative(r_measure, M, a):

    dPhi_dR = GRAV_CONST * M / (r_measure + a)**2

    return dPhi_dR


def get_analytic_hernquist_potential_z_derivative(z, R, M, a):

    r = np.sqrt(z**2 + R**2)

    dPhi_dz = GRAV_CONST * M * z / (r * (r + a)**2)

    return dPhi_dz

###############################################################################

def get_exponential_spherical_density(R, M_star, R_d):
    '''
    '''
    rho_0 = M_star / (4 * np.pi * R_d ** 2)
    rho_R = rho_0 * np.exp(-R / R_d) / R

    return (rho_R)


def get_analytic_exponential_mass(r_measure, Mstar, R_d):
    # easy to work out

    M_r = Mstar * (1 - (r_measure/R_d + 1) * np.exp(-r_measure / R_d))

    return M_r


def get_exponential_spherical_potential(r_measure, M_star, R_d):
    '''
    '''

    Phi = - M_star * GRAV_CONST / r_measure * (1 - np.exp(-r_measure / R_d))

    return Phi


def get_analytic_spherical_exponential_potential_derivative(r_measure, M_star, r_d):

    dPhi_dR = M_star * GRAV_CONST / r_measure**2 * (1 - np.exp(-r_measure / r_d) * (1 + r_measure / r_d))

    return dPhi_dR


def my_funky_arcsin(a, z, z_dash, R):

    try:
        out = np.arcsin(2*a / (np.sqrt((z - z_dash)**2 + (a + R)**2) + np.sqrt((z - z_dash)**2 + (a - R)**2) + 1e-3))

        return out

    except Exception as e:

        print(a, z, z_dash, R)

        raise e

# def intergrand(z_dash, a):
#     try:
#         out = 1/ np.cosh( z_dash / z_0 )**2 * my_funky_arcsin(a, z, z_dash, R) * a * kn(0, a / r_d)
#     except FloatingPointError as e:
#         print(z_dash, a)
#         raise e
#     return out

def get_analytic_exponential_potential_z_derivative(z, R, M_star, r_d, z_0):

    # intergrand = lambda z_dash, a : 1/ np.cosh( z_dash / z_0 )**2 * my_funky_arcsin(a, z, z_dash, R) * a * kn(0, a / r_d)

    if z == 0:
        return 0

    intergrand = lambda z_dash, a: 1/ np.cosh( z_dash / z_0 )**2 * a * kn(0, a / r_d) * np.arcsin(
        2*a / (np.sqrt((z - z_dash)**2 + (a + R)**2) + np.sqrt((z - z_dash)**2 + (a - R)**2) + 1e-3))

    dz = z_0 * 0.1
    z = z + dz
    out_plus = dblquad(intergrand, 0, 18*r_d, -4*z_0, 4*z_0,)
    z = z - 2*dz
    out_minus = dblquad(intergrand, 0, 18*r_d, -4*z_0, 4*z_0,)

    dPhi_dz = -GRAV_CONST * M_star / (np.pi * r_d**3 * z_0) * (out_plus[0] - out_minus[0]) / (2*dz)

    return dPhi_dz


def get_exponential_potential(R, M_star, r_d):
    '''
    '''
    y = R / (2 * r_d)

    phi = -GRAV_CONST * M_star * R / (2 * r_d ** 2) * (iv(0, y) * kn(1, y) - iv(1, y) * kn(0, y))

    return phi

###############################################################################

def concentration_ludlow(z, M200):
    '''Ludlow et al. 2016. Appendix B
    '''
    delta_sc = 1.686

    c_0 = 3.395 * (1 + z)**(-0.215)

    beta = 0.307 * (1 + z)**0.540

    gamma_1 = 0.628 * (1 + z)**(-0.047)

    gamma_2 = 0.317 * (1 + z)**(-0.893)

    omega_l0 = 0.693
    omega_m0 = 0.307

    omega_l = omega_l0 / (omega_l0 + omega_m0 * (1 + z)**3)
    omega_m = 1 - omega_l

    psi  = omega_m**(4/7) - omega_l + (1 + omega_m / 2) * (1 + omega_l/70)
    psi0 = omega_m0**(4/7) - omega_l0 + (1 + omega_m0 / 2) * (1 + omega_l0/70)

    a = (1 + z)**(-1.0)

    Dz = omega_m / omega_m0 * psi0 / psi * (1 + z)**(-1.0)

    nu_0 = (4.135 - 0.564 * a**(-1.0) - 0.210 * a**(-2.0) +
            0.0557 * a**(-3.0) - 0.00348 * a**(-4.0)) * Dz**(-1.0)

    xi = (M200 / 10**10)**(-1.0)

    sigma = Dz * 22.26 * xi**0.292 / (1 + 1.53 * xi**0.275 + 3.36 * xi**0.198)

    nu = delta_sc / sigma

    c = c_0 * (nu / nu_0)**(-gamma_1) * (1 + (nu / nu_0)**(1/beta) )**(-beta*(gamma_2 - gamma_1))

    return(c)

def half_size(logM200=12, x=1):
    '''Just returns the medians that I measured.
    '''
    one_r_halfs   = [0.8051, 0.6661, 0.6027, 0.7947,
                   1.2232, 2.0554, 2.8398, 2.8726, 1.2404] #kpc 8.5 -> 12.5
    seven_r_halfs = [1.1302, 1.3342, 1.0583, 1.3402,
                   1.7454, 2.2717, 2.8369, 3.0234, 1.872]  #kpc 8.5 -> 12.5
    one_z_halfs   = [0.2459, 0.2289, 0.2132, 0.3264,
                   0.5331, 0.7658, 1.029,  0.8099, 0.6303] #kpc 8.5 -> 12.5
    seven_z_halfs = [0.2459, 0.2289, 0.2132, 0.3264,
                   0.5331, 0.7658, 1.029,  0.8099, 0.6303] #kpc 8.5 -> 12.5
    one_bary_r    = [0.00565969, 0.00317021, 0.00122978, 0.00099235,
                   0.00115207, 0.00197546, 0.00521531, 0.00877703, 0.0115513] #8.5 -> 12.5
    seven_bary_r  = [0.0069838,  0.00302346, 0.00133732, 0.00108502,
                   0.00128996, 0.00213345, 0.00549872, 0.00968008, 0.01082644] #8.5 -> 12.5
    one_N_star   = [3, 4, 4, 7, 26, 135, 1073, 7948, 20751]
    seven_N_star = [3, 3, 4, 8, 29, 143, 1142, 7861, 22988]

    inv_index = np.array([8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5])

    index = np.nonzero(inv_index == logM200)[0][0]

    if x == 1:
        r_half = one_r_halfs[index]
        z_half = one_z_halfs[index]
        bary_r = one_bary_r[index]
        N_bary = one_N_star[index]

    if x == 7:
        r_half = seven_r_halfs[index]
        z_half = seven_z_halfs[index]
        bary_r = seven_bary_r[index]
        N_bary = seven_N_star[index]

    return(r_half, z_half, bary_r, N_bary)

    # These will probably be useful again at some point.

    # #Moster et al 2013 halo mass
    # M_ = np.power(10, 11.59) * u.Msun
    # M_bary = M200 * 0.0702 / (np.power(M200/M_, -1.376) + np.power(M200/M_, 0.608))

    # #Lange et al 2016 late type bulge half light
    # half_light_bulge = 4.041 * np.power(M_bulge / (10**10 * u.Msun), 0.339) * u.kpc
    # #Hernquist 1990 approximation
    # b = half_light_bulge / 1.8153

    # #Shen et al 2003 size mass
    # half_light = 0.10*u.kpc * np.power(M_bary/u.Msun, 0.14) * np.power(
    #   1 + M_bary/(3.98 * 10**10 * u.Msun), 0.25)

###############################################################################

def work_out_all_inputs(logM200=12, beta=0, x=1):
    '''Works out all inputs based on desired parameters.
    '''
    EAGLE_omega_m = 0.307 #unitless
    EAGLE_omega_b = 0.0482519 #unitless
    EAGLE_gas_frac = (EAGLE_omega_m - EAGLE_omega_b) / EAGLE_omega_b

    EAGLE_dm_particle_mass = 6.570332889156362E-4 #10^10 Msun h
    EAGLE_h = 0.6777
    EAGLE_dm_particle_mass /= EAGLE_h #10^10 Msun
    if x == 7:
      EAGLE_dm_particle_mass /= 7 #10^10 Msun
    EAGLE_gas_particle_mass = EAGLE_dm_particle_mass / EAGLE_gas_frac #10^10 Msun

    # print('%e, %e'%(EAGLE_dm_particle_mass, EAGLE_gas_particle_mass))

    (r_half, z_half, bary_ratio, bary_N) = half_size(logM200, x) #kpc

    CC     = concentration_ludlow(z=0, M200=10**logM200) #unitless
    Lambda = 0.03 #unitless

    M200   = 10**((logM200*1.01) - 10) #10^10 Msun
    # beta   = beta #Bulge to total

    #pretty sure this should by / but guess and check says otherwise
    z0     = z_half / np.log(3) #kpc #median to sech^2

    V200   = v_200_for_GalIC(M200) #km/s
    R200   = r_200_for_GalIC(V200) #kpc

    #Star totals
    M_bary = bary_ratio * M200 #10^10 Msun
    # N_bary = int( np.round(M_bary / EAGLE_gas_particle_mass, 0) ) #unitless
    N_bary = bary_N #use measured value that includes mass loss of stars. ~< 2x M_bary / gas mass

    #Masses of components
    M_disk  = M_bary * (1 - beta) #10^10 Msun
    M_bulge = M_bary * beta #10^10 Msun
    M_halo  = M200 - M_bary #10^10 Msun

    #Mass fractions
    MD = M_disk  / M200 #unitless
    MB = M_bulge / M200 #unitless

    #Number of particles
    N_disk  = int(np.round(N_bary * (1 - beta))) #unitless
    N_bulge = int(np.round(N_bary * beta)) #unitless
    N_halo  = int(np.round(M_halo / EAGLE_dm_particle_mass)) #unitless

    #Particle masses
    if N_disk == 0:  M_disk_particle = 0
    else:            M_disk_particle = M_disk  / N_disk

    if N_bulge == 0: M_bulge_particle = 0
    else:            M_bulge_particle = M_bulge / N_bulge

    #sizes
    a = hernquist_a_from_GalIC(V200, CC) #kpc

    # b = r_half / 1.8153 #kpc #from projected half light Hernquist 1990. eqn 38
    b = r_half / (1 + np.sqrt(2)) #kpc Hernquist 1990. eqn 4

    BulgeSize = b/a #unitless

    #find the disk scale length that gives the correct stellar half mass
    if M_disk > 0:
        f = lambda h : GalIC_half_mass_radii(M_disk, h, M_bulge, b) - r_half
        h = brentq(f, 0.1, 10) #kpc

        # DiskHeight = z0 / h #unitless

        DiskHeight = 0.05 / h

        #find the disk angular momentum fraction
        g = lambda JD : GalIC_disk_size(R200, M200, CC, Lambda, JD, DiskHeight,
                                        M_halo, a, M_bulge, b, M_disk) - h
        JD = brentq(g, 0.000001, 10)

    else:
        h          = 0
        DiskHeight = 0
        JD         = 0

    print('Inputs')
    print('CC              =', signif(CC))
    print('V200            =', signif(V200))
    print('Lambda          =', signif(Lambda))
    print('MD              =', signif(MD))
    print('MB              =', signif(MB))
    print('JD              =', signif(JD))
    print('DiskHeight      =', signif(DiskHeight))
    print('BulgeSize       =', signif(BulgeSize))

    print('Structural parameters')
    print('R200            =', signif(R200))
    print('M200            = %e'%signif(M200))
    print('A  (halo)       =', signif(a))
    print('H  (disk)       =', signif(h))
    print('Z0 (disk)       =', signif(z0))
    print('A  (bulge)      =', signif(b))

    print('Computation things')
    print('N_HALO           =', N_halo)
    print('N_DISK           =', N_disk)
    print('N_BULGE          =', N_bulge)
    print('M_DM             = %e'%signif(N_halo))
    print('M_DISK_PARTICLE  = %e'%signif(M_disk_particle))
    print('M_BULGE_PARTICLE = %e'%signif(M_bulge_particle))

    # return

    model_name = 'Model_M200_%d_beta%d_x%d'%(int(logM200*10), beta, x)

    out = '''
    %------   File and path names, as well as output file format
    
    OutputDir       /mnt/su3ctm/mwilkinson/GalIC/EAGLE_like/{0}
    
    OutputFile      snap    % Base filename of generated sequence of files
    SnapFormat      3       % File format selection
    
    
    %------   Basic structural parameters of model
    
    CC             {1}        % halo concentration
    V200           {2}        % circular velocity v_200 (in km/sec)
    LAMBDA         {3}        % spin parameter
    MD             {4}        % disk mass fraction
    MB             {5}        % bulge mass fraction
    MBH            0.0        % black hole mass fraction. If zero, no black
                              % hole is generated, otherwise one at the centre
                              % is added.
    
    JD             {6}      % disk spin fraction, typically chosen equal to MD
    
    DiskHeight     {7}        % thickness of stellar disk in units of radial scale length
    BulgeSize      {8}        % bulge scale length in units of halo scale length
    
    HaloStretch    1.0        % should be one for a spherical halo, smaller than one corresponds to prolate distortion, otherwise oblate
    BulgeStretch   1.0        % should be one for a spherical bulge, smaller than one corresponds to prolate distortion, otherwise oblate
    
    %------   Particle numbers in target model
    
    N_HALO         {9}           % desired number of particles in dark halo
    N_DISK         {10}           % desired number of collisionless particles in disk
    N_BULGE        {11}           % number of bulge particles
    
    
    %------   Selection of symmetry constraints of velocity structure
    
    TypeOfHaloVelocityStructure    2      %  0 = spherically symmetric, isotropic
                                          %  1 = spherically symmetric, anisotropic (with beta parameter specified)
                                          %  2 = axisymmetric, f(E, Lz), with specified net rotation
                                          %  3 = axisymmetric, f(E, Lz, I_3), with <vz^2>/<vR^2> specified and net rotation specified
    
    TypeOfDiskVelocityStructure    3      %  0 = spherically symmetric, isotropic
                                          %  1 = spherically symmetric, anisotropic (with beta parameter specified)
                                          %  2 = axisymmetric, f(E, Lz), with specified net rotation
                                          %  3 = axisymmetric, f(E, Lz, I_3), with <vz^2>/<vR^2> specified and net rotation specified
    
    TypeOfBulgeVelocityStructure   2      %  0 = spherically symmetric, isotropic
                                          %  1 = spherically symmetric, anisotropic (with beta parameter specified)
                                          %  2 = axisymmetric, f(E, Lz), with specified net rotation
                                          %  3 = axisymmetric, f(E, Lz, I_3), with <vz^2>/<vR^2> specified and net rotation specified
    
    
    HaloBetaParameter              0    %  only relevant for TypeOfHaloVelocityStructure=1
    BulgeBetaParameter             0    %  only relevant for TypeOfBulgeVelocityStructure=1
    
    
    HaloDispersionRoverZratio      2.0   %  only relevant for TypeOfHaloVelocityStructure=3
    DiskDispersionRoverZratio      2.0   %  only relevant for TypeOfDiskVelocityStructure=3
    BulgeDispersionRoverZratio     2.0   %  only relevant for TypeOfBulgeVelocityStructure=3
    
    
    HaloStreamingVelocityParameter     0.0    % gives the azimuthal streaming velocity in the TypeOf*VelocityStructure=2/3 cases ('k parameter')
    DiskStreamingVelocityParameter     1.0    % gives the azimuthal streaming velocity in the TypeOf*VelocityStructure=2/3 cases ('k parameter')
    BulgeStreamingVelocityParameter    0.0    % gives the azimuthal streaming velocity in the TypeOf*VelocityStructure=2/3 cases ('k parameter')
    
    
    %------   Orbit integration accuracy
    
    TorbitFac                          10.0  % regulates the integration time of orbits
                                             % (this is of the order of the typical number of orbits per particle)
    TimeStepFactorOrbit                0.01
    TimeStepFactorCellCross            0.25
    
    
    %------   Iterative optimization parameters
    
    FractionToOptimizeIndependendly    0.001
    IndepenentOptimizationsPerStep     100
    StepsBetweenDump                   10
    MaximumNumberOfSteps               100
    
    MinParticlesPerBinForDispersionMeasurement  100
    MinParticlesPerBinForDensityMeasurement     50
    
    
    %------   Grid dimension and extenstion/resolution
    
    DG_MaxLevel    7
    EG_MaxLevel    7
    FG_Nbin        256                  % number of bins for the acceleration grid in the R- and z-directions
    
    
    OutermostBinEnclosedMassFraction  0.999   % regulates the fraction of mass of the Hernquist
                                              % halo that must be inside the grid (determines grid extension)
    
    InnermostBinEnclosedMassFraction  0.0000001 % regulates the fraction of mass enclosed by the innermost
                              % bin (regulates size of innermost grid cells)
    
    
    
    MaxVelInUnitsVesc                 0.9999  % maximum allowed velocity in units of the local escape velocity
    
    
    %------   Construction of target density field
    
    SampleDensityFieldForTargetResponse 1               % if set to 1, the code will randomly sample points to construct the density field
    SampleParticleCount                 1000000         % number of points sampled for target density field
    
    
    %------   Construction of force field
    
    SampleForceNhalo                    0               % number of points to use to for computing force field with a tree
    SampleForceNdisk                    1000000
    SampleForceNbulge                   0
    
    Softening                           0.05
    
    
    %------   Accuracy settings of tree code used in construction of force field
    
    TypeOfOpeningCriterion    1
    ErrTolTheta               0.4
    ErrTolForceAcc            0.0025
    
    %------   Domain decomposition parameters used in parallel tree code
    
    MultipleDomains 4
    TopNodeFactor   4
    
    
    %------   Parallel I/O paramaters, only affects writing of galaxy files
    
    NumFilesPerSnapshot       1
    NumFilesWrittenInParallel 1
    
    
    %------   Memory allocation parameters
    
    MaxMemSize                40000.0    % in MB
    BufferSize                100.0
    BufferSizeGravity         100.0
    
    
    %------   Specification of internal system of units
    
    UnitLength_in_cm         3.085678e21        %  1.0 kpc
    UnitMass_in_g            1.989e43           %  1.0e10 solar masses
    UnitVelocity_in_cm_per_s 1e5                %  1 km/sec
    GravityConstantInternal  0
    '''.format(model_name, signif(CC), signif(V200), Lambda,
               signif(MD), signif(MB), signif(JD),
               signif(DiskHeight), signif(BulgeSize),
               N_halo, N_disk, N_bulge)

    print(out)

    # with open('/home/matt/Documents/UWA/EAGLE/L0025N0376/z=2_ideal_GalIC_galaxies/'
    #           + model_name + '.param', 'w') as model:
    #   model.write(out)

    return

@lru_cache(maxsize=100_000)
def get_z_jeans_equation(r_half, M_disk, z0, R_d, M_halo, a):

    rho = lambda z, r: M_disk / (4 * np.pi * z0 * R_d ** 2) / np.cosh(z / z0) ** 2 * np.exp(-r / R_d)
    acc = lambda z, r: (get_analytic_hernquist_potential_z_derivative(z, r, M_halo, a) +
                        get_analytic_exponential_potential_z_derivative(z, r, M_disk, R_d, z0))

    integrad = lambda z: rho(z, r_half) * acc(z, r_half)
    sigma_r_star2 = quad(integrad, 0, 4 * z0)[0] / rho(0, r_half)

    return sigma_r_star2


def work_out_all_inputs2(fdisk=0.0, fbulge=0.01, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10, mu=5.0, seed=0, save=True):#'R12'):
    '''Works out all inputs based on desired parameters.
    '''
    # Lambda = 0.035  # unitless #spin parameter
    Lambda = 0.03  # unitless #spin parameter

    # z_half = 0.1 #kpc #median(|z|)

    r200 = r_200_for_GalIC(V200)  # kpc

    z_half = (0.1 * fdisk +
              0.5 * r200 * rd_on_r200_on_c / CC / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real * finter
              ) / (fdisk + finter) #kpc #median(|z|)

    M200 = M_200_for_GalIC(V200) # 10^10 Msun
    # Masses of components
    M_disk = M200 * fdisk + M200 * finter  # 10^10 Msun
    M_bulge = M200 * fbulge  # 10^10 Msun
    M_halo = M200 * (1 - fdisk - fbulge) # 10^10 Msun
    # Mass fractions
    MD = M_disk / M200  # unitless
    MB = M_bulge / M200  # unitless

    # sizes
    a = hernquist_a_from_GalIC(V200, CC)  # kpc

    R_d = rd_on_r200_on_c * r200 / CC #kpc
    BulgeSize = R_d / a #unitless
    #TODO check this isn't *
    z0 = z_half * 2 / np.log(3)  # kpc #median of sech^2
    DiskHeight =  z0 / R_d

    # find the disk scale length that gives the correct stellar half mass
    if M_disk > 0:
        # find the disk angular momentum fraction
        g = lambda JD: GalIC_disk_size(r200, M200, CC, Lambda, JD, DiskHeight,
                                       M_halo, a, M_bulge, BulgeSize * a, M_disk) - R_d
        JD = brentq(g, 0.000001, 10)
    else:
        h = 0
        DiskHeight = 0
        JD = 0

    # Number of particles
    N_halo = int(np.round(M_halo / 10**(lgMdm - 10)))  # unitless

    #work out interesting mu values
    model_name = 'mu_'

    if mu == 'R12':
        half = 0.5
        r_half = R_d / (-lambertw((half - 1) / np.exp(1), -1) - 1).real  # kpc

        if fdisk > 0:
            model_name += 'disk_half/'

            # np.seterr(all='raise')

            sigma_DM_2 = get_analytic_hernquist_dispersion(r_half, M_halo, a) ** 2

            sigma_r_star2 = get_z_jeans_equation(r_half, M_disk, z0, R_d, M_halo, a)

            mu = sigma_r_star2 / sigma_DM_2

            print(np.sqrt(sigma_r_star2))
            print('mu:', mu)

        elif fbulge > 0:
            model_name += 'bulge_half/'

            #Isotropic Jean's equations
            rho = lambda r: get_exponential_spherical_density(r, M_bulge, R_d)
            acc = lambda r: (get_analytic_hernquist_potential_derivative(r, M_halo, a) +
                             get_analytic_spherical_exponential_potential_derivative(r, M_bulge, R_d))
            integrad = lambda r: rho(r) * acc(r)
            sigma_r_star2 = quad(integrad, r_half, 10 * r200)[0] / rho(r_half)

            rho = lambda r: get_analytic_hernquist_density(r, M_halo, a)
            acc = lambda r: (get_analytic_hernquist_potential_derivative(r, M_halo, a) +
                             get_analytic_spherical_exponential_potential_derivative(r, M_bulge, R_d))
            integrad = lambda r: rho(r) * acc(r)
            sigma_r_dm2 = quad(integrad, r_half, 10 * r200)[0] / rho(r_half)

            mu = sigma_r_star2 / sigma_r_dm2

            print('mu:', mu)

    else:
        model_name += str(mu) + '/'

    N_disk = int(fdisk * N_halo * mu + finter * N_halo * mu) # unitless
    N_bulge = int(fbulge * N_halo * mu) # unitless

    M_disk_particle = 0
    if N_disk != 0:  M_disk_particle = M_disk / N_disk
    M_bulge_particle = 0
    if N_bulge != 0:  M_bulge_particle = M_bulge / N_bulge

    print('Inputs')
    print('CC              =', signif(CC))
    print('V200            =', signif(V200))
    print('Lambda          =', signif(Lambda))
    print('MD              =', signif(MD))
    print('MB              =', signif(MB))
    print('JD              =', signif(JD))
    print('DiskHeight      =', signif(DiskHeight))
    print('BulgeSize       =', signif(BulgeSize))

    print('Structural parameters')
    print('R200            =', signif(r200))
    print('M200            = %e' % signif(M200))
    print('a  (halo)       =', signif(a))
    print('Rd (disk)       =', signif(R_d))
    print('z0 (disk)       =', signif(z0))
    print('a  (bulge)      =', signif(R_d))

    print('Computation things')
    print('N_HALO           =', N_halo)
    print('N_DISK           =', N_disk)
    print('N_BULGE          =', N_bulge)
    print('M_DM             = %e' % signif(N_halo))
    print('M_DISK_PARTICLE  = %e' % signif(M_disk_particle))
    print('M_BULGE_PARTICLE = %e' % signif(M_bulge_particle))

    if fbulge > 0:
        model_name += f'fbulge_{fbulge}_'
    if fdisk > 0:
        model_name += f'fdisk_{fdisk}_'
    if finter > 0:
        model_name += f'finter_{finter}_'
    model_name += f'lgMdm_{lgMdm}_V200_{V200}kmps'
    model_name = model_name.replace('.', 'p')
    if seed != 0:
        model_name += f'_seed{seed}'

    out = f'''
%------   File and path names, as well as output file format

OutputDir       /mnt/su3ctm/mwilkinson/GalIC/{model_name}

OutputFile      snap    % Base filename of generated sequence of files
SnapFormat      3       % File format selection


%------   Basic structural parameters of model

CC             {signif(CC)}        % halo concentration
V200           {signif(V200)}        % circular velocity v_200 (in km/sec)
LAMBDA         {Lambda}        % spin parameter
MD             {signif(MD)}        % disk mass fraction
MB             {signif(MB)}        % bulge mass fraction
MBH            0.0        % black hole mass fraction. If zero, no black
                          % hole is generated, otherwise one at the centre
                          % is added.

JD             {signif(JD)}      % disk spin fraction, typically chosen equal to MD

DiskHeight     {signif(DiskHeight)}        % thickness of stellar disk in units of radial scale length
BulgeSize      {signif(BulgeSize)}        % bulge scale length in units of halo scale length

HaloStretch    1.0        % should be one for a spherical halo, smaller than one corresponds to prolate distortion, otherwise oblate
BulgeStretch   1.0        % should be one for a spherical bulge, smaller than one corresponds to prolate distortion, otherwise oblate

%------   Particle numbers in target model

N_HALO         {N_halo}           % desired number of particles in dark halo
N_DISK         {N_disk}           % desired number of collisionless particles in disk
N_BULGE        {N_bulge}           % number of bulge particles


%------   Selection of symmetry constraints of velocity structure

TypeOfHaloVelocityStructure    2      %  0 = spherically symmetric, isotropic
                                      %  1 = spherically symmetric, anisotropic (with beta parameter specified)
                                      %  2 = axisymmetric, f(E, Lz), with specified net rotation
                                      %  3 = axisymmetric, f(E, Lz, I_3), with <vz^2>/<vR^2> specified and net rotation specified

TypeOfDiskVelocityStructure    2      %  0 = spherically symmetric, isotropic
                                      %  1 = spherically symmetric, anisotropic (with beta parameter specified)
                                      %  2 = axisymmetric, f(E, Lz), with specified net rotation
                                      %  3 = axisymmetric, f(E, Lz, I_3), with <vz^2>/<vR^2> specified and net rotation specified

TypeOfBulgeVelocityStructure   2      %  0 = spherically symmetric, isotropic
                                      %  1 = spherically symmetric, anisotropic (with beta parameter specified)
                                      %  2 = axisymmetric, f(E, Lz), with specified net rotation
                                      %  3 = axisymmetric, f(E, Lz, I_3), with <vz^2>/<vR^2> specified and net rotation specified


HaloBetaParameter              0    %  only relevant for TypeOfHaloVelocityStructure=1
BulgeBetaParameter             0    %  only relevant for TypeOfBulgeVelocityStructure=1


HaloDispersionRoverZratio      2.0   %  only relevant for TypeOfHaloVelocityStructure=3
DiskDispersionRoverZratio      2.0   %  only relevant for TypeOfDiskVelocityStructure=3
BulgeDispersionRoverZratio     2.0   %  only relevant for TypeOfBulgeVelocityStructure=3


HaloStreamingVelocityParameter     0.0    % gives the azimuthal streaming velocity in the TypeOf*VelocityStructure=2/3 cases ('k parameter')
DiskStreamingVelocityParameter     1.0    % gives the azimuthal streaming velocity in the TypeOf*VelocityStructure=2/3 cases ('k parameter')
BulgeStreamingVelocityParameter    0.0    % gives the azimuthal streaming velocity in the TypeOf*VelocityStructure=2/3 cases ('k parameter')


%------   Orbit integration accuracy

TorbitFac                          10.0  % regulates the integration time of orbits
                                         % (this is of the order of the typical number of orbits per particle)
TimeStepFactorOrbit                0.01
TimeStepFactorCellCross            0.25


%------   Iterative optimization parameters

FractionToOptimizeIndependendly    0.001
IndepenentOptimizationsPerStep     100
StepsBetweenDump                   10
MaximumNumberOfSteps               100

MinParticlesPerBinForDispersionMeasurement  100
MinParticlesPerBinForDensityMeasurement     50


%------   Grid dimension and extenstion/resolution

DG_MaxLevel    7
EG_MaxLevel    7
FG_Nbin        256                  % number of bins for the acceleration grid in the R- and z-directions


OutermostBinEnclosedMassFraction  0.999   % regulates the fraction of mass of the Hernquist
                                          % halo that must be inside the grid (determines grid extension)

InnermostBinEnclosedMassFraction  0.0000001 % regulates the fraction of mass enclosed by the innermost
                          % bin (regulates size of innermost grid cells)



MaxVelInUnitsVesc                 0.9999  % maximum allowed velocity in units of the local escape velocity


%------   Construction of target density field

SampleDensityFieldForTargetResponse 1               % if set to 1, the code will randomly sample points to construct the density field
SampleParticleCount                 1000000         % number of points sampled for target density field


%------   Construction of force field

SampleForceNhalo                    0               % number of points to use to for computing force field with a tree
SampleForceNdisk                    1000000
SampleForceNbulge                   0

Softening                           0.05


%------   Accuracy settings of tree code used in construction of force field

TypeOfOpeningCriterion    1
ErrTolTheta               0.4
ErrTolForceAcc            0.0025

%------   Domain decomposition parameters used in parallel tree code

MultipleDomains 4
TopNodeFactor   4


%------   Parallel I/O paramaters, only affects writing of galaxy files

NumFilesPerSnapshot       1
NumFilesWrittenInParallel 1


%------   Memory allocation parameters

MaxMemSize                40000.0    % in MB
BufferSize                100.0
BufferSizeGravity         100.0


%------   Specification of internal system of units

UnitLength_in_cm         3.085678e21        %  1.0 kpc
UnitMass_in_g            1.989e43           %  1.0e10 solar masses
UnitVelocity_in_cm_per_s 1e5                %  1 km/sec
GravityConstantInternal  0
'''

    fname = '../../disk-heating/galaxies/spherical_model_ICs/' + model_name + '.param'
    if save:
        with open(fname, 'w') as model:
            model.write(out)
    else:
        print(out)

    print(fname)

    return


def work_out_all_gadget(fdisk=0.0, fbulge=0.01, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10, mu=5.0,
                        seed=0, static=False, long=False, max_timestep=0.02, save=True):

    model_name = 'mu_'
    if mu == 'R12':
        if fdisk > 0:
            model_name += 'disk_half/'
        elif fbulge > 0:
            model_name += 'bulge_half/'
    else:
        model_name += str(mu) + '/'

    if fbulge > 0:
        model_name += f'fbulge_{fbulge}_'
    if fdisk > 0:
        model_name += f'fdisk_{fdisk}_'
    if finter > 0:
        model_name += f'finter_{fdisk}_'
    model_name += f'lgMdm_{lgMdm}_V200_{V200}kmps'
    model_name = model_name.replace('.', 'p')
    if seed != 0:
        model_name += f'_seed{seed}'
    if static:
        model_name += '_static'

    out_name = model_name

    if lgMdm > 7 and not long:
        TimeBetSnapshot = 0.025 #simulation time units (.978 Gyr)
    else:
        TimeBetSnapshot = 0.1 #simulation time units

    TimeMax = 10
    if long:
        TimeMax = 100
        out_name += '_long'

    if max_timestep != 0.02:
        out_name += f'_timestep_{max_timestep}'
        out_name = out_name.replace('.', 'p')

    out = f'''
%Relevant files

InitCondFile /mnt/su3ctm/mwilkinson/GalIC/{model_name}/snap_010
OutputDir /mnt/su3ctm/mwilkinson/GalIC/{out_name}/data/
SnapshotFileBase  snapshot
OutputListFilename	outputs.txt

%---- File formats
ICFormat             3 
SnapFormat           3 

%---- CPU-time limits
TimeLimitCPU             864000   % 10d, in seconds
CpuTimeBetRestartFile     36000   % 10h,  in seconds

%----- Memory alloction
MaxMemSize                1800    % in MByte

%---- Caracteristics of run
TimeBegin                 0.0
TimeMax	                  {TimeMax}

%---- Basic code options that set the type of simulation
ComovingIntegrationOn     0 

%---- Cosmological parameters
Omega0	                  0   
OmegaLambda               0
OmegaBaryon               0
HubbleParam               1.0
Hubble                    100.0
BoxSize                   100               ; kpc/h

%---- Output frequency and output paramaters
OutputListOn              0
TimeBetSnapshot	          {TimeBetSnapshot}
TimeOfFirstSnapshot       0.0
TimeBetStatistics         0.05
NumFilesPerSnapshot       1
MaxFilesWithConcurrentIO  1 

%---- Accuracy of time integration
ErrTolIntAccuracy        0.01 
CourantFac               0.1
MaxSizeTimestep          {max_timestep}
MinSizeTimestep          0.0 

%---- Tree algorithm, force accuracy, domain update frequency
TypeOfOpeningCriterion                0
ErrTolTheta                           0.5
ErrTolThetaMax                        1.0
ErrTolForceAcc                        0.01
TopNodeFactor                         3.0

ActivePartFracForNewDomainDecomp      0.1

%---- Initial density estimate
DesNumNgb                        64
MaxNumNgbDeviation               1 

%---- System of units
UnitLength_in_cm         3.085678e21        ;  kpc / h
UnitMass_in_g            1.989e43           ;  1.0e10 Msun / h
UnitVelocity_in_cm_per_s 1e5                ;  1 km/sec
GravityConstantInternal  0

%---- Gravitational softening length
SofteningClassOfPartType0	   0.206986     ; kpc/h
SofteningClassOfPartType1      0.206986     ; kpc/h
SofteningClassOfPartType2      0.206986     ; kpc/h
SofteningClassOfPartType3      0.206986     ; kpc/h
SofteningClassOfPartType4      0.206986     ; kpc/h
SofteningClassOfPartType5      0.206986     ; kpc/h

SofteningMaxPhysClass0	      0.206986     ; kpc/h
SofteningMaxPhysClass1        0.206986     ; kpc/h
SofteningMaxPhysClass2        0.206986     ; kpc/h
SofteningMaxPhysClass3        0.206986     ; kpc/h
SofteningMaxPhysClass4        0.206986     ; kpc/h
SofteningMaxPhysClass5        0.206986     ; kpc/h

SofteningComovingClass0       0.206986     ; kpc/h
SofteningComovingClass1       0.206986     ; kpc/h
SofteningComovingClass2       0.206986     ; kpc/h
SofteningComovingClass3       0.206986     ; kpc/h
SofteningComovingClass4       0.206986     ; kpc/h
SofteningComovingClass5       0.206986     ; kpc/h

%----- SPH
ArtBulkViscConst             1.0
MinEgySpec                   0
InitGasTemp                  0
'''

    if static:
        out += '''        
%----- Hernquist
A_StaticHQHalo      34.511498  ; kpc / h
Mass_StaticHQHalo   184.10596  ; 1.0e10 Msun / h 
        '''
    else:
        out += '''
ActivePartFracForPMinsteadOfEwald 0.05
        '''

    fname = '../../disk-heating/galaxies/spherical_model_ICs/' + out_name + '_gadget.param'
    if save:
        with open(fname, 'w') as model:
            model.write(out)

    print(fname)

    return

if __name__ == '__main__':

    # for beta in [0, 1]:
    #   for x in [1, 7]:
    #     for logM200 in [8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5]:
    #       work_out_all_inputs(logM200=logM200, beta=beta, x=x)

    # work_out_all_inputs(logM200=12, beta=0, x=1)

    # for mu in [0.2,0.5,1,2,5,25]:
        # for seed in range(10):
        # seed=0
        # work_out_all_inputs2(fdisk=0, fbulge=0, finter=0.01, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
        #                      mu=mu, seed=seed, save=True)
        # work_out_all_inputs2(fdisk=0.01, fbulge=0, rd_on_r200_on_c=0.2, lgMdm=7, V200=200, CC=10, mu=mu, seed=seed, save=True)
        # work_out_all_inputs2(fdisk=0.01, fbulge=0, rd_on_r200_on_c=0.2, lgMdm=6, V200=200, CC=10, mu=mu, seed=seed, save=True)
        # work_out_all_gadget(fdisk=0, fbulge=0.01, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
        #                     mu=mu, seed=seed, static=False, long=False)
        # work_out_all_gadget(fdisk=0, fbulge=0.01, rd_on_r200_on_c=0.2, lgMdm=7, V200=200, CC=10, mu=mu, seed=seed, static=False,)
        # work_out_all_gadget(fdisk=0, fbulge=0.01, rd_on_r200_on_c=0.2, lgMdm=6, V200=200, CC=10, mu=mu, seed=seed, static=False,)
        # pass

    # work_out_all_gadget(fdisk=0, fbulge=0.01, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
    #                     mu=25, seed=0, static=False, max_timestep=0.04, long=False)
    # work_out_all_gadget(fdisk=0, fbulge=0.01, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
    #                     mu=25, seed=0, static=False, max_timestep=0.01, long=False)
    # work_out_all_gadget(fdisk=0, fbulge=0.01, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
    #                     mu=25, seed=0, static=False, max_timestep=0.005, long=False)

    mu = 0.77
    for seed in range(10):
        work_out_all_inputs2(fdisk=0.01, fbulge=0, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
                             mu=mu, seed=seed, save=True)
        work_out_all_gadget(fdisk=0.01, fbulge=0, finter=0, rd_on_r200_on_c=0.2, lgMdm=8, V200=200, CC=10,
                            mu=mu, seed=seed, save=True)
    pass