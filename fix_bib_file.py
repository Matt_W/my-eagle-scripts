import os
import sys

def load_file(filename):

    file_text = ''

    with open(filename, 'r') as file:
        for line in file:
            file_text += line

    entries = {}

    problems = 0

    for entry in file_text.split('@')[1:]:
        entry = '@' + entry

        start_key_i = None
        end_key_i = None
        for index, letter in enumerate(entry):
            if start_key_i == None:
                if letter == '{':
                    start_key_i = index
            if end_key_i == None:
                if letter == ',':
                    end_key_i = index

        key = entry[start_key_i+1:end_key_i]

        #check if there are problems
        if key in entries.keys():
            if entries[key].strip() != entry.strip():
                problems += 1

                print('text mismatch in ' + key)

                if len(entries[key].strip().split('\n')) != len(entry.strip().split('\n')):
                    print('length mismatch')

                for line_0, line_1 in zip(entries[key].split('\n'), entry.split('\n')):
                    if line_0 != line_1:
                        print(line_0)
                        print(line_1)

        else:
            entries[key] = entry.strip()

    if problems > 0:
        raise ValueError(f'Need to fix {problems} mismatched entries before proceeding.')

    return entries
    # print(entry, end='')

def sort_and_rewrite_file(entries, filename):

    out = ''

    for key in sorted(data.keys()):
        out += data[key] + '\n\n'

    with open(filename, 'w') as file:
        file.write(out)

    return

if __name__ == '__main__':

    data = load_file('/home/matt/Documents/UWA/writing/thesis/MattThesis/bibliography.bib')
    sort_and_rewrite_file(data, '/home/matt/Documents/UWA/writing/thesis/MattThesis/bibliography.bib')

    print('Done')