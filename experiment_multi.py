#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

# import matplotlib
# matplotlib.use('Agg')

import h5py as h5

from scipy.integrate import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

import traceback as tb

from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans

import multiprocessing as mp


def make_data():

    # N_p = 100_000_000
    # N_haloes = 1000
    N_p = 1_000_000
    N_haloes = 100

    np.random.seed(1)

    x = np.random.rand(N_p, 3)
    v = 1 - 2 * np.random.rand(N_p, 3)
    T = np.random.rand(N_p)

    print('started assigning numbers')
    # halo_id = np.random.randint(0, N_haloes - 1, N_p)
    # kmeans = KMeans(n_clusters=N_haloes, random_state=0, n_init=1).fit(x)
    kmeans = MiniBatchKMeans(n_clusters=N_haloes, random_state=0, batch_size=N_p//100, n_init=1).fit(x)

    group_number = kmeans.labels_

    halo_cops = kmeans.cluster_centers_
    print('finished assigning numbers')

    print('started kdtree')
    kd_tree = cKDTree(x, leafsize=10, boxsize=1)
    print('finished kdtree')

    print('finished loading')

    return (N_haloes, halo_cops, x, v, T, group_number, kd_tree)


def fib(n):
    if n > 1:
        return fib(n-1) + fib(n-2)
    else:
        return 1


def interable_calculate(halo):

    print(halo)
    # time.sleep(0.3)

    fib(30)

    cop = halo_cops[halo]
    index_mask = kd_tree.query_ball_point(x=cop, r=0.1)

    index_mask = np.array(index_mask)
    if len(index_mask) > 0:
        index_mask = index_mask[np.abs(group_number[index_mask]) == halo]
    if len(index_mask) != 0:
        x = DM_x[index_mask]
        v = DM_v[index_mask]
        T = DM_T[index_mask]

        x = x - cop - 0.5
        x %= 1
        x -= 0.5

        r = np.linalg.norm(x, axis=1)

        prof = get_kinematic_profiles(bin_edges, x, v, np.ones(len(index_mask)), r, x[:, 0], v[:, 0], v[:, 1], v[:, 2])
        ann = get_kinematic_annuli(0.03, x, v, np.ones(len(index_mask)), r, x[:, 0], v[:, 0], v[:, 1], v[:, 2])
        apa = get_kinematic_apature(0.03, x, v, np.ones(len(index_mask)), r, x[:, 0], v[:, 0], v[:, 1], v[:, 2])

        data = [prof, ann, apa]

        return [halo, *[np.hstack(([d[i] for d in data])) for i in range(len(data[0]))]]

    return [halo, *np.zeros((n_params, n_dim))]


def analyse_data(N_haloes, halo_cops, DM_x, DM_v, DM_T, group_number, kd_tree):

    global n_dim, n_params, bin_edges

    bin_edges = np.logspace(-3, -0.5, 5+1)
    n_dim = 2 + len(bin_edges) - 1 #7

    # bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi
    n_params = 5

    # global halo_cops, DM_x, DM_v, DM_T, group_number, kd_tree
    globals()['halo_cops'] = halo_cops
    globals()['DM_x'] = DM_x
    globals()['DM_v'] = DM_v
    globals()['DM_T'] = DM_T
    globals()['group_number'] = group_number
    globals()['kd_tree'] = kd_tree

    with mp.Pool(processes=8) as pool:
        out = pool.map(interable_calculate, range(N_haloes))

    # print([out[i][0] for i in range(N_haloes)])
    bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi = (np.array([out[i][param+1] for i in range(N_haloes)])
                                                      for param in range(n_params))

    return (bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi)


def save_data(bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi):
    print('Made it to here!')

    print(bin_N[:4])
    print(mean_v_phi[:4])
    print(sigma_z[:4])
    print(sigma_R[:4])
    print(sigma_phi[:4])

    return


def get_kinematic_profiles(bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z):

    number_of_bins = len(bin_edges) - 1

    # binned
    if number_of_bins > 1:
        (bin_mass, _, bin_number) = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
    #this needed to stop it from breaking
    else:
        return(np.zeros(0), np.zeros(0),
               np.zeros(0), np.zeros(0), np.zeros(0))

    bin_N = np.histogram(R, bins=bin_edges)[0]

    # sigmas
    mean_v_phi = np.zeros(number_of_bins, dtype=np.float32)

    sigma_z = np.zeros(number_of_bins, dtype=np.float32)
    sigma_R = np.zeros(number_of_bins, dtype=np.float32)
    sigma_phi = np.zeros(number_of_bins, dtype=np.float32)

    # would be faster without a for loop
    for i in range(number_of_bins):
        if bin_mass[i] > 0:

            mask = (bin_number == i + 1)

            bin_massi = 1 / bin_mass[i]

            mmass = mass[mask]
            mv_phi = v_phi[mask]

            # sigmas
            mean_v_phi[i] = np.sum(mmass * mv_phi) * bin_massi

            sigma_z[i] = np.sum(mmass * np.square(v_z[mask])) * bin_massi
            sigma_R[i] = np.sum(mmass * np.square(v_R[mask])) * bin_massi
            sigma_phi[i] = np.sum(mmass * np.square(mv_phi - mean_v_phi[i])) * bin_massi

    sigma_z = np.sqrt(sigma_z)
    sigma_R = np.sqrt(sigma_R)
    sigma_phi = np.sqrt(sigma_phi)

    return (bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi)


def get_kinematic_apature(apature, pos, vel, mass, R, z, v_R, v_phi, v_z):
    """Same as get_kinematic_profile, except apature rather than profile.
    """
    # mask
    mask = (R < apature)

    if np.sum(mask) > 0:

        pos = pos[mask]
        vel = vel[mask]
        mass = mass[mask]
        (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])

        bin_mass = np.sum(mass)
        bin_N = np.size(mass)

        bin_massi = 1 / bin_mass

        # sigmas
        mean_v_phi = np.sum(mass * v_phi) * bin_massi

        sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
        sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
        sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

        return (bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi)

    else:
        return(0, 0, 0, 0, 0)


def get_kinematic_annuli(apature, pos, vel, mass, R, z, v_R, v_phi, v_z):
    """Same as get_kinematic_profile, except apature rather than profile.
    """
    # mask
    dex = 0.2
    mask = np.logical_and(10 ** (-dex / 2) < R / apature, R / apature < 10 ** (dex / 2))

    if np.sum(mask) > 0:

        pos = pos[mask]
        vel = vel[mask]
        mass = mass[mask]
        (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])

        bin_mass = np.sum(mass)
        bin_N = np.size(mass)

        bin_massi = 1 / bin_mass

        # sigmas
        mean_v_phi = np.sum(mass * v_phi) * bin_massi

        sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
        sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
        sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

        return (bin_N, mean_v_phi, sigma_z, sigma_R, sigma_phi)

    else:
        return (0, 0, 0, 0, 0)


def do_everything():
    data = make_data()

    results = analyse_data(*data)

    save_data(*results)

    return


if __name__ == '__main__':
    do_everything()