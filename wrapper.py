#! /usr/bin/env python3
import ctypes
import numpy as np	

# std imports
import multiprocessing as mp


def parametrized(dec):
    """This decorator can be used to create other decorators that accept arguments
    https://stackoverflow.com/a/64693650
    """

    def layer(*args, **kwargs):
        def repl(f):
            return dec(f, *args, **kwargs)
        return repl
    return layer


@parametrized
def sigsev_guard(fcn, default_value=None, timeout=None):
    """Used as a decorator with arguments.
    The decorated function will be called with its input arguments in another process.

    If the execution lasts longer than *timeout* seconds, it will be considered failed.

    If the execution fails, *default_value* will be returned.
    https://stackoverflow.com/a/64693650
    """

    def _fcn_wrapper(*args, **kwargs):
        q = mp.Queue()
        p = mp.Process(target=lambda q: q.put(fcn(*args, **kwargs)), args=(q,))
        p.start()
        p.join(timeout=timeout)
        exit_code = p.exitcode

        if exit_code == 0:
            return q.get()

        logging.warning('Process did not exit correctly. Exit code: {}'.format(exit_code))
        return default_value

    return _fcn_wrapper


@sigsev_guard()
def c_square(*args, **kwargs):
    return ctypes.CDLL('./external_x2.so').square(*args, **kwargs)


if __name__ == '__main__':
    
    '''
    #x = 4 #np.array([4], dtype=np.int64)
    #out = c_square(ctypes.pointer(ctypes.c_int(x)))

    n = 4

    in_array = (ctypes.c_int64 * n)(*np.arange(n))
    out_array = (ctypes.c_int64 * n)(*[0]*n)

    c_square(ctypes.byref(in_array), ctypes.c_int(len(in_array) + 20), ctypes.byref(out_array))

    #print([out_array[i] for i in range(n)])
    print(np.array(out_array))
    
    print('end')

    '''

    bits = 14

    iddm = np.array([1899382772966, 1899435621100, 1899466926482, 1899465133232,
                     1899467046782, 1899417755410, 1899433421562, 1899431720304,
                     1899466762430, 1899465151450], dtype=np.uint64)
    
    n = len(iddm)

    key = (ctypes.c_uint64 * n)(*iddm)

    c_peano_hilbert_key_inverse = ctypes.CDLL('/home/matt/Documents/UWA/EAGLE/scripts/peano_for_py.so').peano_hilbert_key_inverse_

    x_array = (ctypes.c_int64 * n)(*[0]*n)
    y_array = (ctypes.c_int64 * n)(*[0]*n)
    z_array = (ctypes.c_int64 * n)(*[0]*n)

    c_peano_hilbert_key_inverse(ctypes.byref(key), ctypes.c_int64(bits), 
                                ctypes.byref(x_array), ctypes.byref(y_array), ctypes.byref(z_array), 
                                ctypes.c_int64(n))

    x_np = np.array(x_array)
    y_np = np.array(y_array)
    z_np = np.array(z_array)

    a_np = np.vstack((x_np, y_np, z_np)).T  / 2**bits * 33.855

    print(a_np.dtype)

    a_np = a_np.astype(np.float32)
    
    print(a_np)
    print(np.shape(a_np))
    print(a_np.dtype)

