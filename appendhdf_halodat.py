#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 11:46:19 2021

@author: matt
"""

import ReadEagleSubfind_halo
# #my file for merger trees
# import ReadDatabseSubFind
import h5py                   as h5
# import os
import numpy as np

# LITTLE_H = 0.6777

print('\nFor HYDRO ...')

data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/'
data_destination = '/fred/oz009/mwilkinson/EAGLE/processed_data/'
csv_names = ['Merger_028.csv', 'Merger_027.csv', 'Merger_026.csv', 'Merger_025.csv',
             'Merger_024.csv', 'Merger_023.csv', 'Merger_022.csv', 'Merger_021.csv',
             'Merger_020.csv', 'Merger_019.csv', 'Merger_018.csv', 'Merger_017.csv',
             'Merger_016.csv', 'Merger_015.csv']

#Where the data is
Base = '/fred/oz009/clagos/EAGLE/L0100N1504/data'
#os.chdir('/home/matt/Documents/UWA/EAGLE/results')
ident = '_100Mpc_1504'
# snap = '028_z000p000'

#which snapshots to look at
lines=['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
       '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
       '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
       '016_z001p737', '015_z002p012']


#do for all sizes for all times
for i in range(len(lines)):

  #pretty sure splitting like this isnt't necessary ...
  line=lines[i].split('\n')[0].split('_')
  Num=line[0]
  fend='_'+line[1]
  exts         = Num.zfill(3)
  fpart        = Base+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'
  fsub         = Base+'/groups_'      +exts+fend+'/'+'eagle_subfind_tab_'      +exts+fend+'.0.hdf5'

  if True:
    # Load halo catalogs into "HaloData" Structure
    HaloData     = ReadEagleSubfind_halo.ReadEagleSubfind_halo(Base, '', fend,exts)

    # DatabaseData = ReadDatabseSubFind.read_merger_csv(data_location, csv_names[i])

    #TODO next time, use with loop
    #TODO next time, have different groups (ID, masses, extra)

    fn = data_destination +exts+fend+ident+'_halodat.hdf5'

    output  = h5.File(fn, "a")
    grp0    = output.create_group("Header")
    # grp1    = output.create_group("HaloData")

    # dset    = grp0.create_dataset('h', data = PartData['HubbleParam'])
    # dset    = grp0.create_dataset('Omega',    data = HaloData['Omega'])
    dset    = grp0.create_dataset('Redshift', data = HaloData['Redshift'])
    # dset    = grp0.create_dataset('BoxSize',  data = HaloData['BoxSize'])
    # dset    = grp0.create_dataset('Ngrps',    data = HaloData['TotNgroups'])
    # dset    = grp0.create_dataset('Nsbgrps',  data = HaloData['TotNsubgroups'])
    # dset    = grp0.create_dataset('Nsubs',    data = HaloData['NumOfSubhalos'])
    # dset    = grp0.create_dataset('H(z)',     data = HaloData['H(z)'])

    # dset    = grp1.create_dataset('GroupPos',        data = HaloData['GroupPos'])
    # dset    = grp1.create_dataset('FirstSub',        data = HaloData['FirstSub'])
    # dset    = grp1.create_dataset('HaloSFR',         data = HaloData['SFRate'])
    # dset    = grp1.create_dataset('Vbulk',           data = HaloData['Vbulk'])

    # dset    = grp1.create_dataset('Vmax',            data = Vmax)
    # dset    = grp1.create_dataset('Rmax',            data = Rmax)

    # dset    = grp1.create_dataset('Group_M_Crit200', data = M200)
    # dset    = grp1.create_dataset('Group_R_Crit200', data = R200)

    # dset    = grp1.create_dataset('GroupNumber',    data = DatabaseData['GroupNumber'])
    # dset    = grp1.create_dataset('SubGroupNumber', data = DatabaseData['SubGroupNumber'])
    # dset    = grp1.create_dataset('Pos_cop',        data = DatabaseData['CentreOfPotential'] * LITTLE_H)

    # dset    = grp1.create_dataset('GalaxyID',     data = DatabaseData['GalaxyID'])
    # dset    = grp1.create_dataset('LastProgID',   data = DatabaseData['LastProgID'])
    # dset    = grp1.create_dataset('TopLeafID',    data = DatabaseData['TopLeafID'])
    # dset    = grp1.create_dataset('DescendantID', data = DatabaseData['DescendantID'])

    # dset    = grp1.create_dataset('BlackHoleMass',              data = DatabaseData['BlackHoleMass'])
    # dset    = grp1.create_dataset('BlackHoleMassAccretionRate', data = DatabaseData['BlackHoleMassAccretionRate'])
    # dset    = grp1.create_dataset('Mass',                       data = DatabaseData['Mass'])
    # dset    = grp1.create_dataset('MassType_DM',                data = DatabaseData['MassType_DM'])
    # dset    = grp1.create_dataset('MassType_Gas',               data = DatabaseData['MassType_Gas'])
    # dset    = grp1.create_dataset('MassType_Star',              data = DatabaseData['MassType_Star'])
    # dset    = grp1.create_dataset('MassType_BH',                data = DatabaseData['MassType_BH'])

    output.close()
    print('\nOutput:',fn)