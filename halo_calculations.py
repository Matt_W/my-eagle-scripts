#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 14:47:59 2020

@author: matt
"""

import numpy as np
from scipy.special     import kn, iv
from scipy.integrate   import quad
from scipy.optimize    import brentq
from scipy.optimize    import minimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.spatial.transform import Rotation
from scipy.signal      import savgol_filter

import os

import astropy.units     as u
import astropy.constants as cst

import matplotlib.pyplot as plt
import splotch           as splt
import time
# import twobody

GRAV_CONST   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06766 #km/s/kpc (Planck 2018)

################################################################################
#halos for plotting purposes.
#taken from my nfw_test.py

def nfw_rho_0(conc, M_vir, R_s):
  '''Calculates the initial NFW density given the concentration, virial mass,
  and scale radius.
  Result in units of M_vir / R_s^3.
  '''
  rho_0 = M_vir / (4*np.pi * R_s**3 * (np.log(1+conc) - conc/(1+conc)))

  return(rho_0)

def nfw_hard_code():
  '''Calculate V_max and R_v_max, which is needed to input to other scripts.
  '''
  conc  = 10
  M_200 = 181.124 * 10**10 *u.Msun  #pretty sure this is wrong
  V_200 = 200 * u.km/u.s

  R_200 = (cst.G * M_200) / V_200**2

  R_s   = R_200 / conc

  rho_0 = nfw_rho_0(conc, M_200, R_s)

  return(rho_0, R_s)

def nfw_mass(r, rho_0, R_s):
  '''mass at a given radii given central potential and scale radius.
  R_s and r shoud be in the same units
  Result in units of rho_0 * R_s^3
  May be numerical errors for small r
  '''
  M = 4*np.pi*rho_0*R_s**3 * (np.log((R_s+r)/R_s) - r/(R_s+r))
  return(M)

def nfw_velocity_circ(r, rho_0, R_s):
  '''potential at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.kpc
  if not isinstance(r, u.Quantity):
    r     *= u.kpc

  mass = nfw_mass(r, rho_0, R_s)
  v_circ = np.sqrt(cst.G * mass / r)
  v_circ = v_circ.to(u.km/u.s)

  return(v_circ)

def nfw_potential(r, rho_0, R_s):
  '''potential at a given radii given central density and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  Might not work for r=0.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.kpc
  if not isinstance(r, u.Quantity):
    r     *= u.kpc

  phi = -4*np.pi*cst.G*rho_0*R_s**3/r * np.log(1+r/R_s)

  return(phi)

def mass_weighted_mean(array, masses):
  '''Calculates the mass weighted mean of an array
  '''
  mwm = np.sum(masses * array) / np.sum(masses)

  return(mwm)

def mass_weighted_std(array, masses):
  '''Calculates the mass weighted standard deviation of an array
  '''
  mwm = mass_weighted_mean(array, masses)

  mws = np.sqrt(np.sum(masses * np.square(array - mwm)) / np.sum(masses))

  return(mws)

def mass_weighted_median(array, masses):
  '''Calculates the mass weighted standard deviation of an array
  '''
  if len(array) != 0:
    #probably better way to do this, just first that came to me
    arg_order = np.argsort(array)
    cum_mass = np.cumsum(masses[arg_order])
    middle_mass = cum_mass[-1] / 2

    #if list is even, takes the lower value. Not an accuarte choice
    index = np.sum(cum_mass < middle_mass)
    mwmed = array[arg_order][index]

    return(mwmed)

  else:
    return(np.nan)

################################################################################

GalIC_G = 43018.7 / 43009.1727 * cst.G
GalIC_H = 3.2407789e-18 * 3.085678e21 / 1e5 * u.km / u.kpc / u.s

def hernquist_mass_from_GalIC(v_200, c):
  '''
  '''
  if not isinstance(v_200, u.Quantity):
    v_200 *= u.km / u.s

  M_dm = v_200**3 / (10 * GalIC_G * GalIC_H)

  return(M_dm)

def r_200_for_GalIC(v_200=200):
  '''Not actually r_200. Isothermal sphere parameter approximations used in GalIC
  '''
  if not isinstance(v_200, u.Quantity):
    v_200 *= u.km / u.s

  r_200 = v_200 / (10 * GalIC_H)

  return(r_200)

def hernquist_a_from_GalIC(v_200, c):
  '''
  '''
  r_200 = r_200_for_GalIC(v_200)

  a = r_200 / c * np.sqrt(2 * (np.log(1 + c) - c / (1 + c)))

  return(a)

def hernquist_parameters_from_GalIC(v_200, conc):
  '''
  '''
  M = hernquist_mass_from_GalIC(v_200, conc)
  a = hernquist_a_from_GalIC(v_200, conc)

  return(M, a)

def hernquist_mass(r, M_dm, a):
  '''
  '''
  M = M_dm * (r / (r + a))**2

  return(M)

def hernquist_velocity_circ(r, M_dm, a):
  '''
  '''
  if not isinstance(M_dm, u.Quantity):
    M_dm *= 10**10 * u.Msun
  if not isinstance(a, u.Quantity):
    a *= u.kpc
  if not isinstance(r, u.Quantity):
    r *= u.kpc

  mass = hernquist_mass(r, M_dm, a)
  v_circ = np.sqrt(GalIC_G * mass / r)
  v_circ = v_circ.to(u.km/u.s)

  return(v_circ)

def hernquist_potential(r, M_dm, a):
  '''
  '''
  if not isinstance(M_dm, u.Quantity):
    M_dm *= 10**10 * u.Msun
  if not isinstance(a, u.Quantity):
    a *= u.kpc
  if not isinstance(r, u.Quantity):
    r *= u.kpc

  phi = - M_dm * GalIC_G / (a * (1 + r/a))

  return(phi)

################################################################################

def get_cylindrical(PosStars, VelStars):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  varphi   = np.arctan2(PosStars[:,1], PosStars[:,0])
  z        = PosStars[:,2]

  v_rho    = VelStars[:,0] * np.cos(varphi) + VelStars[:,1] * np.sin(varphi)
  v_varphi = -VelStars[:,0]* np.sin(varphi) + VelStars[:,1] * np.cos(varphi)

  # v_rho    = (PosStars[:,0] * VelStars[:,0] + PosStars[:,1] * VelStars[:,1]
  #             ) / np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  # v_varphi = (PosStars[:,0] * VelStars[:,1] - PosStars[:,1] * VelStars[:,0]
  #             ) / np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)

  v_z      = VelStars[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

################################################################################

def get_central_velocity(star_pos, star_vel, star_mass, velocity_apature=30,
                         verbose=False):
  '''
  '''
  #find velocity offset
  star_r = np.linalg.norm(star_pos, axis=1)
  star_mask = star_r < velocity_apature

  weighted_central_velocity = np.sum(star_mass[star_mask] * star_vel[star_mask].T,
                                     axis=1) / np.sum(star_mass[star_mask])

  return(weighted_central_velocity)

def total_angular_momentum(pos, vel, mass):
  '''Total angular momentum of given particles.
  Useful for parties.
  '''
  angular_momentum = np.sum(np.cross(pos, vel).T * mass, axis=1)

  return(angular_momentum)

def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  R = find_rotaton_matrix(galaxy_anular_momentum)
  pos = R.apply(pos)
  '''
  # j_mag = np.linalg.norm(j_vector)

  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  # j_intem = Rotation.from_euler('y', y, degrees=True).apply(j_vector)
  # j_out = Rotation.from_euler('x', x, degrees=True).apply(j_intem)
  # print(j_out)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)
  # print(j_tot)
  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

def z_specific_angular_momentum(r_vector, v_vector):
  '''Assume equal mass and already alligned.
  '''
  # j_z = np.cross(r_vector, v_vector)[:,2]
  j_z = r_vector[:,0] * v_vector[:,1] - r_vector[:,1] * v_vector[:,0]

  return(j_z)

def create_custom_binning(min_r=0.1, n_small=5, one=1, change_r=10, max_r=200):
  '''Custom binning.
  small linear 0.1 < r < 1  ~ 0.1 kpc
  linear         1 < r < 10 ~ 1kpc
  log           10 < r < 60 ~ 10%r
  '''

  bin_edge_tiny = np.linspace(min_r, one,    n_small)
  bin_edge_low  = np.arange(one,     change_r, one)[1:]
  bin_edge_hi   = 10**np.arange(np.log10(change_r), np.log10(max_r)+1e-3,
                                np.log10(1 + one/change_r))
  bin_edges = np.hstack((bin_edge_tiny, bin_edge_low, bin_edge_hi))
  r_bins = 0.5 * (bin_edges[:-1] + bin_edges[1:])
  n_bins = len(r_bins)

  return(bin_edges, r_bins, n_bins)

def my_quantile(array, q):
  '''Calls np.quantile. If array is empty, returns np.nan instead of an error.
  '''
  if np.size(array) == 0:
    return(np.nan)
  else:
    return(np.quantile(array, q))

#the main part
###############################################################################

def calculate_binned_circular(gas_pos,  gas_vel,  gas_mass,  gas_bind,
                              dm_pos,   dm_vel,   dm_mass,   dm_bind,
                              star_pos, star_vel, star_mass, star_bind,
                              bh_pos,   bh_vel,   bh_mass,   bh_bind):
  '''Calculates circular properties of a galaxy assuming spherical symmetry
  (this is usualy a good assumption, esp. for EAGLE / cosmo-hydro-sims).
  Calculates galaxy properties in *adaptive* spherical anulii bins:
    E(r)   (specific)
    V_c(r)
    V_c(E)
    j_c(r) (specific)
    j_c(E) (specific)
  '''
  #binning
  (bin_edges, r_bins, n_bins) = create_custom_binning()

  gas_r  = np.linalg.norm(gas_pos,  axis=1)
  dm_r   = np.linalg.norm(dm_pos,   axis=1)
  star_r = np.linalg.norm(star_pos, axis=1)
  bh_r   = np.linalg.norm(bh_pos,   axis=1)

  #particle specific energies
  gas_kin  = 0.5 * np.square(np.linalg.norm(gas_vel,  axis=1))
  dm_kin   = 0.5 * np.square(np.linalg.norm(dm_vel,   axis=1))
  star_kin = 0.5 * np.square(np.linalg.norm(star_vel, axis=1))
  bh_kin   = 0.5 * np.square(np.linalg.norm(bh_vel,   axis=1))

  gas_pot  = gas_bind  - gas_kin
  dm_pot   = dm_bind   - dm_kin
  star_pot = star_bind - star_kin
  bh_pot   = bh_bind   - bh_kin

  #
  # if the mass of the disk dominates, should instead use
  # two_body.midplane_potential
  #

  gas_bins  = np.digitize(gas_r,  bin_edges)
  dm_bins   = np.digitize(dm_r,   bin_edges)
  star_bins = np.digitize(star_r, bin_edges)
  bh_bins   = np.digitize(bh_r,   bin_edges)

  # #get average potential over all particles
  # pot_midplane = np.array([np.mean(np.hstack((
  #       gas_pot[gas_bins==(i+1)], dm_pot[dm_bins==(i+1)],
  #       star_pot[star_bins==(i+1)], bh_pot[bh_bins==(i+1)] )))
  #     for i in range(n_bins)])

  q = 0.1
  #TODO this should be mass weighted quantile
  pot_midplane = np.array([my_quantile(np.hstack((
        gas_pot[gas_bins==(i+1)], dm_pot[dm_bins==(i+1)],
        star_pot[star_bins==(i+1)], bh_pot[bh_bins==(i+1)] )), q)
      for i in range(n_bins)])

  indexes_fine = np.logical_not(np.isnan(pot_midplane))

  if np.any(np.logical_not(indexes_fine)):
    print('Warning: empty potential bins.')
    print('Attempting to fix be reducing resolution by',
          sum(np.isnan(pot_midplane)))

    r_bins       = r_bins[indexes_fine]
    bin_edges    = bin_edges[np.hstack((True, indexes_fine))]

    pot_midplane = pot_midplane[indexes_fine]

    if len(r_bins) < n_bins/2:
      raise ValueError('Had to reduce resolution too much.')

  #might not be strictly correct with different bin widths
  pot_midplane = savgol_filter(pot_midplane, 5, 3)

  #mass enclosed
  mass_enclosed = np.zeros(len(r_bins))
  for i in range(len(r_bins)):
    # lim = bin_edges[i+1]
    lim = r_bins[i]

    gas_mass_less_than  = np.sum(gas_mass[gas_r < lim])
    dm_mass_less_than   = np.sum(dm_mass[dm_r < lim])
    star_mass_less_than = np.sum(star_mass[star_r < lim])
    bh_mass_less_than   = np.sum(bh_mass[bh_r < lim])

    mass_enclosed[i] = (gas_mass_less_than + dm_mass_less_than +
                        star_mass_less_than + bh_mass_less_than)

  v_circ_shell = np.sqrt(GRAV_CONST * mass_enclosed / r_bins)

  #smooth over this
  #might not be strictly correct with different bin widths
  v_circ_smoth = savgol_filter(v_circ_shell, 5, 3)

  #just cause
  v_circ = v_circ_smoth

  #the main point of this function
  j_circ = v_circ * r_bins

  #the other main point
  binding_e = pot_midplane + 0.5 * np.square(v_circ)

  #checkes that energies are monotonically increasing
  check_indexes    = (binding_e - np.roll(binding_e, 1))
  check_indexes[0] = 1
  indexes_fine     = check_indexes > 0

  #cathces nans and negativevalues
  while np.any(np.logical_not(indexes_fine)):
    print('Warning: velocities are not monotonically increasting.')
    print('Attempting to fix by reducing resolution by',
          sum(np.logical_not(indexes_fine)))

    if np.isnan(binding_e[0]):
      binding_e[0] = binding_e[np.logical_not(np.isnan(binding_e))][0]

    j_circ       = j_circ[indexes_fine]
    binding_e    = binding_e[indexes_fine]
    r_bins       = r_bins[indexes_fine]

    #checkes that velocities are monotonically increasing
    check_indexes    = (binding_e - np.roll(binding_e, 1))
    check_indexes[0] = 1
    indexes_fine     = check_indexes > 0

    if len(r_bins) < n_bins/2:
      raise ValueError('Had to reduce resolution too much.')

  #v_circ = j_circ / r_bins
  return(j_circ, binding_e, r_bins)

def plot_potential_verify(gas_pos,  gas_vel,  gas_mass,  gas_bind,
                          dm_pos,   dm_vel,   dm_mass,   dm_bind,
                          star_pos, star_vel, star_mass, star_bind,
                          bh_pos,   bh_vel,   bh_mass,   bh_bind,
                          j_circ=None, binding_e=None, r_bins=None):

  if np.shape(j_circ) == ():

    (j_circ, binding_e, r_bins
     ) = calculate_binned_circular(gas_pos,  gas_vel,  gas_mass,  gas_bind,
                                   dm_pos,   dm_vel,   dm_mass,   dm_bind,
                                   star_pos, star_vel, star_mass, star_bind,
                                   bh_pos,   bh_vel,   bh_mass,   bh_bind)

  gas_r  = np.linalg.norm(gas_pos,  axis=1)
  dm_r   = np.linalg.norm(dm_pos,   axis=1)
  star_r = np.linalg.norm(star_pos, axis=1)
  bh_r   = np.linalg.norm(bh_pos,   axis=1)

  gas_kin  = 0.5 * np.square(np.linalg.norm(gas_vel,  axis=1))
  dm_kin   = 0.5 * np.square(np.linalg.norm(dm_vel,   axis=1))
  star_kin = 0.5 * np.square(np.linalg.norm(star_vel, axis=1))
  bh_kin   = 0.5 * np.square(np.linalg.norm(bh_vel,   axis=1))

  gas_pot  = gas_bind  - gas_kin
  dm_pot   = dm_bind   - dm_kin
  star_pot = star_bind - star_kin
  bh_pot   = bh_bind   - bh_kin

  #mass enclosed
  #measured
  mass_enclosed = np.zeros(len(r_bins))
  for i in range(len(r_bins)):
    # lim = bin_edges[i+1]
    lim = r_bins[i]

    gas_mass_less_than  = np.sum(gas_mass[gas_r < lim])
    dm_mass_less_than   = np.sum(dm_mass[dm_r < lim])
    star_mass_less_than = np.sum(star_mass[star_r < lim])
    bh_mass_less_than   = np.sum(bh_mass[bh_r < lim])

    mass_enclosed[i] = (gas_mass_less_than + dm_mass_less_than +
                        star_mass_less_than + bh_mass_less_than)

  #analytic
  ten = 0
  #nfw v_circ
  rho_0 = 0.001 #10^10 M_sun / kpc^3
  R_s   = 20    #kpc

  #quick and dirty, there are better ways of doing this. Biased to larger values.
  nfw_mass_least_squares = lambda args: np.sum(
    np.square((4 * np.pi * args[0] * np.power(args[1],3) * (
      np.log(1 + r_bins[ten:] / args[1]) - r_bins[ten:]/(args[1] + r_bins[ten:]))) -
      mass_enclosed[ten:]))

  out = minimize(nfw_mass_least_squares, (rho_0, R_s))

  # #quick and dirty, there are better ways of doing this.
  # nfw_mass_least_squares = lambda args: np.sum(
  #   np.square(np.log10(4 * np.pi * args[0] * np.power(args[1],3) * (
  #     np.log(1 + r_bins[ten:] / args[1]) - r_bins[ten:]/(args[1] + r_bins[ten:]))) -
  #     np.log10(mass_enclosed[ten:])))

  # #had to start very close to solution
  # out = minimize(nfw_mass_least_squares, out.x)

  rho_0 = out.x[0] * (10**10 * u.M_sun / u.kpc**3)
  R_s   = out.x[1] * u.kpc

  v_nfw      = nfw_velocity_circ(r_bins, rho_0, R_s).to(u.km/u.s).value

  pot_nfw    = nfw_potential(r_bins, rho_0, R_s).to(u.km**2/u.s**2).value
  E_nfw      = 0.5 * v_nfw**2 + pot_nfw

  cum_mass_nfw = nfw_mass(r_bins*u.kpc, rho_0, R_s).to(10**10 * u.M_sun).value

  #hernquist v_circ
  M_dm = 200 #10^10 M_star
  a    = 30  #kpc

  #quick and dirty, there are better ways of doing this.
  hernquist_mass_least_squares = lambda args: np.sum(
    np.square(np.log10(args[0] * np.square(r_bins[ten:] / (r_bins[ten:] + args[1]))) -
      np.log10(mass_enclosed[ten:])))

  out = minimize(hernquist_mass_least_squares, (M_dm, a))

  M_dm = out.x[0] * (10**10 * u.M_sun)
  a    = out.x[1] * u.kpc

  v_hernq      = hernquist_velocity_circ(r_bins, M_dm, a).to(u.km/u.s).value

  pot_hernq    = hernquist_potential(r_bins, M_dm, a).to(u.km**2/u.s**2).value
  E_hernq      = 0.5 * v_hernq**2 + pot_hernq

  cum_mass_hernq = hernquist_mass(r_bins*u.kpc, M_dm, a).to(10**10 * u.M_sun).value

  #plots
  plt.figure()
  plt.errorbar(r_bins, mass_enclosed,  ls='-', label='Galaxy', fmt='.')

  plt.errorbar(r_bins, cum_mass_nfw,   ls='-', label='NFW',    fmt='.')
  plt.errorbar(r_bins, cum_mass_hernq, ls='-', label='Hernq',  fmt='.')

  plt.loglog()
  plt.xlim(1e-1,1e3)
  # plt.xlim(1e-1,200)
  plt.xlabel(r'r [kpc]')
  plt.ylabel(r'Cumulative Mass [$10^{10}$M$_\odot$]')
  plt.legend(loc='upper left')

  plt.figure()
  # plt.errorbar(gas_r,  gas_bind,  ls='', fmt='.', alpha=0.8, label='gas binding')
  # plt.errorbar(dm_r,   dm_bind,   ls='', fmt='.', alpha=0.8, label='dm binding')
  plt.errorbar(star_r, star_bind, ls='', fmt='.', alpha=0.8, label='star binding')
  # plt.errorbar(bh_r,   bh_bind,   ls='', fmt='.', alpha=0.8, label='bh binding')

  # plt.errorbar(gas_r,  gas_pot,  ls='', fmt='.', alpha=0.8, label='gas potential')
  # plt.errorbar(dm_r,   dm_pot,   ls='', fmt='.', alpha=0.8, label='dm potential')
  plt.errorbar(star_r, star_pot, ls='', fmt='.', alpha=0.8, label='star potential')
  # plt.errorbar(bh_r,   bh_pot,   ls='', fmt='.', alpha=0.8, label='bh potential')

  # plt.errorbar(gas_r,  gas_kin,  ls='', fmt='.', alpha=0.8, label='gas kinetic')
  # plt.errorbar(dm_r,   dm_kin,   ls='', fmt='.', alpha=0.8, label='dm kinetic')
  plt.errorbar(star_r, star_kin, ls='', fmt='.', alpha=0.8, label='star kinetic')
  # plt.errorbar(bh_r,   bh_kin,   ls='', fmt='.', alpha=0.8, label='bh kinetic')

  kinetic_e   = 0.5*np.square(j_circ / r_bins)
  potential_e = binding_e - kinetic_e

  plt.errorbar(r_bins, binding_e,   label='total',     c='k', ls='-',  fmt='s')
  plt.errorbar(r_bins, potential_e, label='potential', c='k', ls='-.', fmt='o')
  plt.errorbar(r_bins, kinetic_e,   label='kinetic',   c='k', ls=':',  fmt='^')

  plt.errorbar(r_bins, E_nfw,        label='NFW total',     c='C3', ls='-',  fmt='s')
  plt.errorbar(r_bins, pot_nfw,      label='NFW potential', c='C3', ls='-.', fmt='o')
  plt.errorbar(r_bins, 0.5*v_nfw**2, label='NFW kinetic',   c='C3', ls=':',  fmt='^')

  plt.errorbar(r_bins, E_hernq,        label='Hernq total',     c='C4', ls='-',  fmt='s')
  plt.errorbar(r_bins, pot_hernq,      label='Hernq potential', c='C4', ls='-.', fmt='o')
  plt.errorbar(r_bins, 0.5*v_hernq**2, label='Hernq kinetic',   c='C4', ls=':',  fmt='^')

  plt.semilogx()
  plt.xlim(1e-1,1e3)
  # plt.xlim(1e-1,200)
  plt.xlabel(r'r [kpc]')
  plt.ylabel(r'energy [(km/s)$^2$]')
  plt.legend(loc='upper left')

  print(np.amax(j_circ / r_bins))

  return

def calculate_particle_circular(gas_pos,  gas_vel,  gas_mass,  gas_bind,
                                dm_pos,   dm_vel,   dm_mass,   dm_bind,
                                star_pos, star_vel, star_mass, star_bind,
                                bh_pos,   bh_vel,   bh_mass,   bh_bind,
                                j_circ=None, binding_e=None, r_bins=None,
                                do_gas=False, do_dm=False, do_star=True, do_bh=False):
  '''Calculates circular properties of a galaxy assuming spherical symmetry
  (this is usualy a good assumption, esp. for EAGLE / cosmo-hydro-sims).
  Calculates galaxy properties in *adaptive* spherical anulii bins:
    r(E)
    V_c(r) -> V_c(E)
    j_c(r) -> j_c(E)
  And particle-wise:
    j_c(E) (specific)
  '''
  if np.shape(j_circ) == ():

    (j_circ, binding_e, r_bins
     ) = calculate_binned_circular(gas_pos,  gas_vel,  gas_mass,  gas_bind,
                                   dm_pos,   dm_vel,   dm_mass,   dm_bind,
                                   star_pos, star_vel, star_mass, star_bind,
                                   bh_pos,   bh_vel,   bh_mass,   bh_bind)

  #make interpolator. hopefully midplane_potential takes care of everything.
  try:
    interp = InterpolatedUnivariateSpline(binding_e, j_circ,
                                          k=1, ext='const')

  except ValueError:
    #this should be taken care of already
    print('Try reducing nbins in halo_calculations.midplane_potential.')
    print('The halo might be clumpy.')
    print('All these values should be positive.')
    print((binding_e - np.roll(binding_e, 1))[1:])
    print((binding_e))
    raise ValueError('x must be strictly increasing')

  to_return = []

  if do_gas:
    gas_j_c = interp(gas_bind)
    to_return.append(gas_j_c)
  if do_dm:
    dm_j_c = interp(dm_bind)
    to_return.append(dm_j_c)
  if do_star:
    star_j_c = interp(star_bind)
    to_return.append(star_j_c)
  if do_bh:
    bh_j_c = interp(bh_bind)
    to_return.append(bh_j_c)

  return(to_return)

if __name__ == '__main__':

  pass