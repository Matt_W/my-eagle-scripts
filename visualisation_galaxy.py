#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

import os
import sys
import time

import numpy as np

import h5py as h5

from scipy.integrate   import quad

# from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
from scipy.interpolate import RegularGridInterpolator

from scipy.ndimage import gaussian_filter

from scipy.optimize    import minimize
from scipy.optimize    import brentq

from scipy.spatial import cKDTree
from scipy.spatial.transform import Rotation

import scipy.stats

import pandas as pd

import matplotlib
matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt

from sphviewer.tools import QuickView
from sphviewer.tools import Blend

import matplotlib.lines as lines
import matplotlib.patches as patches
import matplotlib.patheffects as path_effects
from matplotlib.legend_handler import HandlerTuple

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 12#20
rcParams['xtick.labelsize'] = 12#20
rcParams['ytick.labelsize'] = 12#20
rcParams['axes.labelsize'] = 12#22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777
SCALE_A = 1
BOX_SIZE = 33.885 #50 * 0.6777 #kpc

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

Omega_b = 0.04825
Omega_m = 0.307

EAGLE_EPS = 0.7 # kpc

def lbt_interp(a, omm=0.307, oml=0.693, h=0.6777, n=10_001):
    a_interp = np.linspace(0,1, n)
    z_interp = 1 / a_interp - 1
    t_interp = np.zeros(len(a_interp))

    for i in range(len(z_interp)):
        if a_interp[i] == 0:
            t_interp[i] = 13.82968685
        else:
            t_interp[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100 * h)) * quad(
                lambda z: 1 / ((1 + z) * np.sqrt(omm * (1 + z) ** 3 + oml)), 0, z_interp[i])[0] #Gyr

    return np.interp(a, a_interp, t_interp)


def find_rotaion_matrix(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0
    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)
    if j_tot[2] < 0:
        x += 180

    print(j_tot, y, x)

    return Rotation.from_euler('yx', [y, x], degrees=True)


def find_rotaion_matrix_x(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0
    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)
    if j_tot[2] < 0:
        x += 180

    return Rotation.from_euler('yx', [y, x + 90], degrees=True)


def vec_pacman_dist2(u, v):
    #50/h Mpc hard coded (h=0.6777)
    dx = np.abs(u[:, 0] - v[:, 0])
    dy = np.abs(u[:, 1] - v[:, 1])
    dz = np.abs(u[:, 2] - v[:, 2])

    dx[dx > 16.9425] = 33.885 - dx[dx > 16.9425]
    dy[dy > 16.9425] = 33.885 - dy[dy > 16.9425]
    dz[dz > 16.9425] = 33.885 - dz[dz > 16.9425]

    return dx * dx + dy * dy + dz * dz


def centre_and_units(pos, vel, mass, proj_centre):
    #takes pos and proj_centre in Mpc/h
    #returns pos with boundaried fixed and centred in kpc
    _pos = pos - proj_centre - SCALE_A * BOX_SIZE / 2
    _pos %= SCALE_A * BOX_SIZE
    _pos -= SCALE_A * BOX_SIZE / 2
    _pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

    _vel = vel * np.sqrt(SCALE_A)  # km/sun
    vel_offset = np.sum(mass[:, np.newaxis] * _vel, axis=0) / np.sum(mass)
    _vel -= vel_offset

    return _pos, _vel


# def load_star_data():
#     # TODO this will be extracted from kdtrees
#     with h5.File(f'/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_group257_cutout.hdf5',
#                  'r') as particles:
#     # with h5.File(f'/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_group272_cutout.hdf5',
#     #              'r') as particles:
#
#         # MassGas_0 = particles['PartType0/Mass'][()]
#         # PosGas_0 = particles['PartType0/Coordinates'][()]
#         #
#         # PosDM_0 = particles['PartType1/Coordinates'][()]
#
#         #TODO work out if I should use (current) mass or initial mass
#         MassStar = particles['PartType4/Mass'][()]
#         PosStar = particles['PartType4/Coordinates'][()]
#         VelStar = particles['PartType4/Velocity'][()]
#         StellatFormationTime = particles['PartType4/StellarFormationTime'][()]
#         Metallicity = particles['PartType4/Metallicity'][()]
#
#     #TODO unhard code
#     proj_centre_0 = np.mean(PosStar, axis=0)
#     proj_extent_mpch = np.mean(np.std(PosStar, axis=0)) * 2
#
#     # mask = np.logical_and(np.logical_and(PosStar[:, 0] - proj_centre_0[0] < proj_extent_mpch,
#     #                                      PosStar[:, 0] - proj_centre_0[0] > -proj_extent_mpch),
#     #                       np.logical_and(PosStar[:, 1] - proj_centre_0[1] < proj_extent_mpch,
#     #                                      PosStar[:, 1] - proj_centre_0[1] > -proj_extent_mpch))
#     mask = np.linalg.norm(PosStar - proj_centre_0[np.newaxis, :], axis=1) < 2*proj_extent_mpch
#
#     return (proj_extent_mpch, proj_centre_0,
#             MassStar[mask], PosStar[mask], VelStar[mask], StellatFormationTime[mask], Metallicity[mask])

def load_star_data(particle_data_location, halo_data_location, snap, group_number=0):
    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']

    # Halo arrays
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)

    NGrp_c = 0

    print('TotNGroups:', TotNgroups)

    for ifile in range(Ntask):
        fn = particle_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']

            if Ngroups > 0:
                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]

                NGrp_c += Ngroups

    GroupCentreOfPotential = GroupCentreOfPotential[group_number -1]

    print('Loaded halos')

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    # NumPartTot = [0, NumPartTot[1], 0, 0, 0]

    # if NumPartTot[4] > 0:
    MassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    PosStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    VelStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    Star_aform = np.zeros(NumPartTot[4], dtype=np.float32)
    Metallicity = np.zeros(NumPartTot[4], dtype=np.float32)
    GrpNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)
    SubNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)

    NStar_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[4] > 0:
                MassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Mass"][()]
                PosStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Coordinates"][()]
                VelStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Velocity"][()]
                Star_aform[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/StellarFormationTime"][()]
                Metallicity[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Metallicity"][()]
                GrpNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/GroupNumber"][()]
                SubNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SubGroupNumber"][()]

                NStar_c += NumParts[4]

    mask = GrpNum_Star == group_number

    MassStar = MassStar[mask]
    PosStar = PosStar[mask]
    VelStar = VelStar[mask]
    Star_aform = Star_aform[mask]
    Metallicity = Metallicity[mask]
    SubNum_Star = SubNum_Star[mask]

    SubMask = SubNum_Star == 0

    MassStar = MassStar[SubMask]
    PosStar = PosStar[SubMask]
    VelStar = VelStar[SubMask]
    Star_aform = Star_aform[SubMask]
    Metallicity = Metallicity[SubMask]

    StellatFormationTime = lbt_interp(Star_aform)

    print('loaded particles')

    proj_centre_0 = np.mean(PosStar, axis=0)
    proj_extent_mpch = np.mean(np.std(PosStar, axis=0)) * 2

    print(group_number, proj_centre_0, GroupCentreOfPotential)
    print('Nstars = ', len(MassStar))

    return (proj_extent_mpch, GroupCentreOfPotential, #proj_centre_0,
            MassStar, PosStar, VelStar, StellatFormationTime, Metallicity)


def load_bco3(file_loc='../BC03_stelib_chabrier/Stelib_Atlas/Chabrier_IMF/'):
    #22 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.7696, Y=0.2303, Z=0.0001
    #32 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.7686, Y=0.231, Z=0.0004
    #42 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.756, Y=0.24, Z=0.004
    #52 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.742, Y=0.25, Z=0.008
    #62 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.70, Y=0.28, Z=0.02
    #72 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.5980, Y=0.352, Z=0.0500
    #82 #TRACKS: Padova (1994) + S. Charlot (1997). X=0.4250, Y=0.475, Z=0.1000

    meta_Z = [0.0001, 0.0004, 0.004, 0.008, 0.02, 0.05, 0.1]

    out = None

    for number_name, Z in zip([22,32,42,52,62,72,82], meta_Z):

        file_name0 = file_loc + f'bc2003_hr_stelib_m{number_name}_chab_ssp.1color'
        file_name1 = file_loc + f'bc2003_hr_stelib_m{number_name}_chab_ssp.wfc3_color'
        file_name2 = file_loc + f'bc2003_hr_stelib_m{number_name}_chab_ssp.wfc3_uvis1_color'

        data0 = pd.read_csv(file_name0, sep='   ', header=30)
        data1 = pd.read_csv(file_name1, sep='   ', header=30)
        data2 = pd.read_csv(file_name2, sep='   ', header=30)


        data = pd.concat((data0[data0.keys()[:6]],
                          data1[data1.keys()[3:-5]],
                          data2[data2.keys()[8:-5]]), axis=1)
        data['Z'] = pd.Series(Z * np.ones(len(data[data.keys()[0]])))

        if np.all(out == None):
            out = data.copy()
        else:
            out = pd.concat((out, data), ignore_index=True)

    # print(out.keys())

    age_grid = np.unique(out['#log-age-yr'])
    log_Z_grid = np.log10(np.unique(out['Z']))
    shape = (len(age_grid), len(log_Z_grid))

    Uflux_grid = np.reshape(10 ** (-out['Umag'].to_numpy() / 2.5), shape, order='F')
    Bflux_grid = np.reshape(10 ** (-out['Bmag'].to_numpy() / 2.5), shape, order='F')
    Vflux_grid = np.reshape(10 ** (-out['Vmag'].to_numpy() / 2.5), shape, order='F')
    Kflux_grid = np.reshape(10 ** (-out['Kmag'].to_numpy() / 2.5), shape, order='F')


    #K correction
    # from https://www.astro.uu.se/~ez/yggdrasil/YggdrasilModels/Z=0.0004_kroupa_IMF_fcov_0_SFR_inst_HST_AB_lyman_alpha0_redshiftgroup2
    # Redshift Age (yr)  Mstars       Mconverted   F225W   F275W   F336W   F390W   F435W    F475W  F555W   F606W
    # 0.000 1.01000e+006 1.00000e+006 1.00000e+006 -14.903 -14.695 -14.344 -14.178 -14.061 -13.918 -13.686 -13.550
    # F625W   F775W   F814W   F850LP  F098W_IR F105W_IR F110W_IR F125W_IR F140W_IR F160W_IR
    # -13.389 -13.009 -12.941 -12.704 -12.552  -12.444  -12.323  -12.108  -11.904  -11.688

    #log-age-yr Vmag   Kmag   V-F225w V-F336w V-F438w V-F547m V-F555w V-F606w V-F625w V-F656n V-F657n V-F658n V-F814w
    #6.000000   1.5903 2.3178 3.1226  2.0125  0.3069  0.0010  0.0499  -0.0221 -0.0778 0.0972  -0.0247 0.0471  -0.2493
    # log Nly Mt/Lb  Mt/Lv  Mt/Lk
    # 46.7071 0.0213 0.0520 0.4084

    wfc110_mag_corr = 0
    wfc125_mag_corr = 0
    wfc160_mag_corr = 0
    wfc225_mag_corr = -14.903 - 3.1226
    wfc336_mag_corr = -14.344 - 2.0125
    wfc388_mag_corr = 0
    wfc438_mag_corr = -14.061 - 0.3069
    wfc555_mag_corr = -13.686 - 0.0499
    wfc814_mag_corr = -12.941 - -0.2493
    wfc220_mag_corr = 0
    # wfc625_mag_corr = 0
    # wfc225_mag_corr = 0
    # wfc336_mag_corr = 0
    # wfc438_mag_corr = 0
    # wfc547_mag_corr = 0
    # wfc555_mag_corr = 0
    wfc606_mag_corr = -13.550 - -0.0221
    wfc625_mag_corr = -13.389 - -0.0778
    wfc656_mag_corr = 0
    wfc657_mag_corr = 0
    wfc658_mag_corr = 0
    # wfc814_mag_corr = 0

    wfc110flux_grid = np.reshape(10 ** ((-out['V-F110W'].to_numpy() + wfc110_mag_corr)/ 2.5), shape, order='F')
    wfc125flux_grid = np.reshape(10 ** ((-out['V-F125W'].to_numpy() + wfc125_mag_corr)/ 2.5), shape, order='F')
    wfc160flux_grid = np.reshape(10 ** ((-out['V-F160W'].to_numpy() + wfc160_mag_corr)/ 2.5), shape, order='F')
    wfc225flux_grid = np.reshape(10 ** ((-out['V-F225W'].to_numpy() + wfc225_mag_corr)/ 2.5), shape, order='F')
    wfc336flux_grid = np.reshape(10 ** ((-out['V-F336W'].to_numpy() + wfc336_mag_corr)/ 2.5), shape, order='F')
    wfc388flux_grid = np.reshape(10 ** ((-out['V-FR388N'].to_numpy()+ wfc388_mag_corr)/ 2.5), shape, order='F')
    wfc438flux_grid = np.reshape(10 ** ((-out['V-F438W'].to_numpy() + wfc438_mag_corr)/ 2.5), shape, order='F')
    wfc555flux_grid = np.reshape(10 ** ((-out['V-F555W'].to_numpy() + wfc555_mag_corr)/ 2.5), shape, order='F')
    wfc814flux_grid = np.reshape(10 ** ((-out['V-F814W'].to_numpy() + wfc814_mag_corr)/ 2.5), shape, order='F')
    wfc220flux_grid = np.reshape(10 ** ((-out['V-ACS220W'].to_numpy() + wfc220_mag_corr)/ 2.5), shape, order='F')
    # wfc625flux_grid = np.reshape(10 ** ((-out['V-ACS625W'].to_numpy() + wfc625_mag_corr)/ 2.5), shape, order='F') #
    # wfc225flux_grid = np.reshape(10 ** ((-out['V-F225w'].to_numpy() + wfc225_mag_corr)/ 2.5), shape, order='F') #
    # wfc336flux_grid = np.reshape(10 ** ((-out['V-F336w'].to_numpy() + wfc336_mag_corr)/ 2.5), shape, order='F') #
    # wfc438flux_grid = np.reshape(10 ** ((-out['V-F438w'].to_numpy() + wfc438_mag_corr)/ 2.5), shape, order='F') #
    # wfc547flux_grid = np.reshape(10 ** ((-out['V-F547m'].to_numpy() + wfc547_mag_corr)/ 2.5), shape, order='F') #
    # wfc555flux_grid = np.reshape(10 ** ((-out['V-F555w'].to_numpy() + wfc555_mag_corr)/ 2.5), shape, order='F') #
    wfc606flux_grid = np.reshape(10 ** ((-out['V-F606w'].to_numpy() + wfc606_mag_corr)/ 2.5), shape, order='F')
    wfc625flux_grid = np.reshape(10 ** ((-out['V-F625w'].to_numpy() + wfc625_mag_corr)/ 2.5), shape, order='F')
    wfc656flux_grid = np.reshape(10 ** ((-out['V-F656n'].to_numpy() + wfc656_mag_corr)/ 2.5), shape, order='F')
    wfc657flux_grid = np.reshape(10 ** ((-out['V-F657n'].to_numpy() + wfc657_mag_corr)/ 2.5), shape, order='F')
    wfc658flux_grid = np.reshape(10 ** ((-out['V-F658n'].to_numpy() + wfc658_mag_corr)/ 2.5), shape, order='F')
    # wfc814flux_grid = np.reshape(10 ** ((-out['V-F814w'].to_numpy() + wfc814_mag_corr)/ 2.5), shape, order='F') #

    Uflux_interp = RegularGridInterpolator((age_grid, log_Z_grid), Uflux_grid, bounds_error=False, fill_value=None)
    Bflux_interp = RegularGridInterpolator((age_grid, log_Z_grid), Bflux_grid, bounds_error=False, fill_value=None)
    Vflux_interp = RegularGridInterpolator((age_grid, log_Z_grid), Vflux_grid, bounds_error=False, fill_value=None)
    Kflux_interp = RegularGridInterpolator((age_grid, log_Z_grid), Kflux_grid, bounds_error=False, fill_value=None)

    wfc110flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc110flux_grid, bounds_error=False, fill_value=None)
    wfc125flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc125flux_grid, bounds_error=False, fill_value=None)
    wfc160flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc160flux_grid, bounds_error=False, fill_value=None)
    wfc225flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc225flux_grid, bounds_error=False, fill_value=None)
    wfc336flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc336flux_grid, bounds_error=False, fill_value=None)
    wfc388flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc388flux_grid, bounds_error=False, fill_value=None)
    wfc438flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc438flux_grid, bounds_error=False, fill_value=None)
    wfc555flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc555flux_grid, bounds_error=False, fill_value=None)
    wfc814flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc814flux_grid, bounds_error=False, fill_value=None)
    wfc220flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc220flux_grid, bounds_error=False, fill_value=None)
    # wfc625flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc625flux_grid, bounds_error=False, fill_value=None)
    # wfc225flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc225flux_grid, bounds_error=False, fill_value=None)
    # wfc336flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc336flux_grid, bounds_error=False, fill_value=None)
    # wfc438flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc438flux_grid, bounds_error=False, fill_value=None)
    # wfc547flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc547flux_grid, bounds_error=False, fill_value=None)
    # wfc555flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc555flux_grid, bounds_error=False, fill_value=None)
    wfc606flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc606flux_grid, bounds_error=False, fill_value=None)
    wfc625flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc625flux_grid, bounds_error=False, fill_value=None)
    wfc656flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc656flux_grid, bounds_error=False, fill_value=None)
    wfc657flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc657flux_grid, bounds_error=False, fill_value=None)
    wfc658flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc658flux_grid, bounds_error=False, fill_value=None)
    # wfc814flux_interp = RegularGridInterpolator((age_grid, log_Z_grid), wfc814flux_grid, bounds_error=False, fill_value=None)

    # https://en.wikipedia.org/wiki/Photometric_system
    # Umag ~ 365nm
    # Bmag ~ 445nm
    # Vmag ~ 551nm
    # Kmag ~ 2190nm

    #SDSS
    # http://svo2.cab.inta-csic.es/theory/fps/index.php?mode=browse&gname=APO&gname2=SDSS&asttype=
    # Ultraviolet u 354
    # Green g 472
    # Red r 620

    return (Uflux_interp, Bflux_interp, Vflux_interp, Kflux_interp,
            wfc110flux_interp, wfc125flux_interp, wfc160flux_interp, wfc225flux_interp,
            wfc336flux_interp, wfc388flux_interp, wfc438flux_interp, wfc555flux_interp,
            wfc814flux_interp, wfc220flux_interp,
            # wfc625flux_interp,  wfc225flux_interp,  wfc336flux_interp,  wfc438flux_interp,
            # wfc547flux_interp,  wfc555flux_interp,
            wfc606flux_interp, wfc625flux_interp,
            wfc656flux_interp, wfc657flux_interp, wfc658flux_interp, ) # wfc814flux_interp)


def get_N_neighbour_sigma_z(PosStar, VelStar, MassStar, N=50):

    tree = cKDTree(PosStar, leafsize=10)

    mass_sigma_z = np.zeros(len(PosStar))

    for i in range(len(PosStar)):
        _, neighbour_i = tree.query(PosStar[i], N)

        mass_sigma_z[i] = np.std(MassStar[neighbour_i] * VelStar[neighbour_i, 2])

    return mass_sigma_z


def make_visualisation(particle_data_location, halo_data_location, snap, output_data_location, group_number=0,
                       nx_edges=101, name='visualisation', save=True, sphview=True, sigma_z=False):

    # orientations = ['face_on']
    orientations = ['box', 'face_on', 'edge_on']

    (Uflux_interp, Bflux_interp, Vflux_interp, Kflux_interp,
     wfc110flux_interp, wfc125flux_interp, wfc160flux_interp, wfc225flux_interp,
     wfc336flux_interp, wfc388flux_interp, wfc438flux_interp, wfc555flux_interp,
     wfc814flux_interp, wfc220flux_interp,
     # wfc625flux_interp,  wfc225flux_interp,  wfc336flux_interp,  wfc438flux_interp,
     # wfc547flux_interp,  wfc555flux_interp,
     wfc606flux_interp, wfc625flux_interp,
     wfc656flux_interp, wfc657flux_interp, wfc658flux_interp, # wfc814flux_interp))
    ) = load_bco3()

    (proj_extent_mpch, cop, MassStar, PosStar, VelStar, StellatFormationTime, Metallicity
     ) = load_star_data(particle_data_location, halo_data_location, snap, group_number)
    # (proj_extent_mpch, cop, MassStar, PosStar, VelStar, StellatFormationTime, Metallicity
    #  ) = load_star_data()

    #units
    proj_extent = proj_extent_mpch * 1000 * SCALE_A / LITTLE_H

    PosStar, VelStar = centre_and_units(PosStar, VelStar, MassStar, cop)

    # calculate fluxes
    min_Z = 1e-5
    Rflux = MassStar * wfc625flux_interp(np.array([np.log10(StellatFormationTime) + 9,
                                                  np.log10(Metallicity + min_Z)]).T)
    Gflux = MassStar * wfc555flux_interp(np.array([np.log10(StellatFormationTime) + 9,
                                                  np.log10(Metallicity + min_Z)]).T)
    Bflux = MassStar * wfc336flux_interp(np.array([np.log10(StellatFormationTime) + 9,
                                                  np.log10(Metallicity + min_Z)]).T)

    for orientation in orientations:

        if orientation == 'face_on':
            # r = np.linalg.norm(PosStar, axis=1)
            # mask = r < 30 #kpc
            # j_tot = np.sum(np.cross(PosStar[mask], VelStar[mask]) * MassStar[mask, np.newaxis], axis=0)
            J_tot = np.sum(np.cross(PosStar, VelStar) * MassStar[:, np.newaxis], axis=0)

            # find rotation
            rotation = find_rotaion_matrix(J_tot)

            # rotate stars
            PosStar = rotation.apply(PosStar)
            VelStar = rotation.apply(VelStar)

        if orientation == 'edge_on':
            # r = np.linalg.norm(PosStar, axis=1)
            # mask = r < 30 #kpc
            # j_tot = np.sum(np.cross(PosStar[mask], VelStar[mask]) * MassStar[mask, np.newaxis], axis=0)
            J_tot = np.sum(np.cross(PosStar, VelStar) * MassStar[:, np.newaxis], axis=0)

            # find rotation
            rotation = find_rotaion_matrix_x(J_tot)

            # rotate stars
            PosStar = rotation.apply(PosStar)
            VelStar = rotation.apply(VelStar)

        if not sigma_z:
            if not sphview: #use histogram2d
                x_edges = np.linspace(-proj_extent, proj_extent, nx_edges)

                Rflux_xy,_,_ = np.histogram2d(PosStar[:, 1], PosStar[:, 0], weights=Rflux, bins = [x_edges, x_edges])
                Gflux_xy,_,_ = np.histogram2d(PosStar[:, 1], PosStar[:, 0], weights=Gflux, bins = [x_edges, x_edges])
                Bflux_xy,_,_ = np.histogram2d(PosStar[:, 1], PosStar[:, 0], weights=Bflux, bins = [x_edges, x_edges])

                RGB_xy = np.zeros((*np.shape(Rflux_xy), 3))
                RGB_xy[:, :, 0] = Rflux_xy #R
                RGB_xy[:, :, 1] = Gflux_xy #G
                RGB_xy[:, :, 2] = Bflux_xy #B

                #units
                RGB_xy = RGB_xy / np.diff(x_edges)[0]**2

            else: #if sphview:
                print('Starting Sphview')
                print(proj_extent)

                xsize = nx_edges * 10  # 500 #is default
                r = 'infinity'
                R_star = QuickView(PosStar, mass=Rflux,
                                   r=r, plot=False, logscale=False, #max_hsml=max_hsml, #min_hsml=min_hsml,
                                   x=0, y=0, z=0, extent=[-proj_extent, proj_extent, -proj_extent, proj_extent],
                                   t=0, xsize=xsize, ysize=xsize)
                G_star = QuickView(PosStar, mass=Gflux,
                                   r=r, plot=False, logscale=False, #max_hsml=max_hsml, #min_hsml=min_hsml,
                                   x=0, y=0, z=0, extent=[-proj_extent, proj_extent, -proj_extent, proj_extent],
                                   t=0, xsize=xsize, ysize=xsize)
                B_star = QuickView(PosStar, mass=Bflux,
                                   r=r, plot=False, logscale=False, #max_hsml=max_hsml, #min_hsml=min_hsml,
                                   x=0, y=0, z=0, extent=[-proj_extent, proj_extent, -proj_extent, proj_extent],
                                   t=0, xsize=xsize, ysize=xsize)

                RGB_xy = np.zeros((xsize, xsize, 3))

                RGB_xy[:, :, 0] = R_star.get_image() # R
                RGB_xy[:, :, 1] = G_star.get_image() # G
                RGB_xy[:, :, 2] = B_star.get_image() # B

            #sets the dynamic range of the colours
            log_low_diff = -3
            stretch_lambda = lambda array: (np.log10(array + 1e-100) - np.amax(np.log10(array + 1e-100)) - log_low_diff
                                            ) / -log_low_diff

            RGB_xy = stretch_lambda(RGB_xy)

        else: #if sigma_z:
            print('Starting Sphview')

            mass_sigma_z = get_N_neighbour_sigma_z(PosStar, VelStar, Rflux)

            xsize = nx_edges * 10  # 500 #is default
            r = 'infinity'
            mass_sigma = QuickView(PosStar, mass=mass_sigma_z,
                               r=r, plot=False, logscale=False, #max_hsml=max_hsml, #min_hsml=min_hsml,
                               x=0, y=0, z=0, extent=[-proj_extent, proj_extent, -proj_extent, proj_extent],
                               t=0, xsize=xsize, ysize=xsize)
            mass = QuickView(PosStar, mass=Rflux,
                               r=r, plot=False, logscale=False, #max_hsml=max_hsml, #min_hsml=min_hsml,
                               x=0, y=0, z=0, extent=[-proj_extent, proj_extent, -proj_extent, proj_extent],
                               t=0, xsize=xsize, ysize=xsize)

            RGB_xy = np.zeros((xsize, xsize, 3))

            RGB_xy[:, :, 0] = mass_sigma.get_image() / mass.get_image() # R
            RGB_xy[:, :, 1] = mass_sigma.get_image() / mass.get_image() # R
            RGB_xy[:, :, 2] = mass_sigma.get_image() / mass.get_image() # R

            # RGB_xy /= np.amax(RGB_xy)

        #plot
        print('Starting plot')
        fig = plt.figure(frameon=False)

        fig.set_size_inches(6.6, 6.6, forward=True)

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        ax.set_axis_off()
        fig.add_axes(ax)

        if not sigma_z:
            ax.imshow(RGB_xy, extent=[-proj_extent, proj_extent, -proj_extent, proj_extent], origin='lower')
        else:
            ax.imshow(RGB_xy/np.amax(RGB_xy),
                      extent=[-proj_extent, proj_extent, -proj_extent, proj_extent], origin='lower')

        # ax.quiver(PosStar[:, 0], PosStar[:, 1], VelStar[:, 0], VelStar[:, 1], color='red')

        #for scale
        dist = int(np.round(proj_extent*3/4))
        ax.errorbar([-proj_extent * 7/8, -proj_extent * 7/8 + dist], [-proj_extent * 7/8]*2, c='white', lw=6)
        ax.text(np.mean([-proj_extent * 7/8, -proj_extent * 7/8 + dist]), -proj_extent * 6.8/8, f'{dist} kpc',
                c='white', horizontalalignment='center', verticalalignment='bottom', fontsize=40)

        if snap == '028_z000p000':
            zstr = r'$z=0$'

        if '7x' in particle_data_location:
            # res_lab = r'$M_{\rm DM} = 1.4 \times 10^6 {\rm M}_\odot$' + '\nHR, ' + zstr
            res_lab = 'HR, ' + zstr
        else:
            # res_lab = r'$M_{\rm DM} = 9.7 \times 10^6 {\rm M}_\odot$' + '\nLR, ' + zstr
            res_lab = 'LR, ' + zstr

        ax.text(-proj_extent * 7.5/8, proj_extent * 7.5/8, res_lab,
                c='white', horizontalalignment='left', verticalalignment='top', fontsize=40)

        ax.text(proj_extent * 7.5/8, -proj_extent * 7.5/8, 'group number = ' + str(group_number) +
                f'\n({cop[0]:1.2f}, {cop[1]:1.2f}, {cop[2]:1.2f}) Mpc/$h$',
                c='white', horizontalalignment='right', verticalalignment='bottom', fontsize=20)

        ax.text(np.mean([-proj_extent * 7/8, -proj_extent * 7/8 + dist]), -proj_extent * 6.8/8, f'{dist} kpc',
                c='white', horizontalalignment='center', verticalalignment='bottom', fontsize=40)

        if save:
            filename = f'{output_data_location}{name}_group{group_number}_{orientation}.png'
            print(filename)
            plt.savefig(filename, dpi=200)

            plt.close()

        if sigma_z:
            x_edges = np.linspace(-proj_extent, proj_extent, nx_edges*10)
            xs, ys = np.meshgrid(x_edges, x_edges)
            xs = np.hstack(xs)
            ys = np.hstack(ys)
            rs = np.sqrt(xs**2 + ys**2)
            sz = np.hstack(RGB_xy[:, :, 0])

            #plot
            fig = plt.figure(frameon=False)

            fig.set_size_inches(6.6, 6.6, forward=True)

            Rs = np.sqrt(PosStar[:, 0]**2 + PosStar[:, 1]**2)
            # plt.scatter(Rs, mass_sigma_z / MassStar, alpha=0.2)
            plt.scatter(Rs, mass_sigma_z / Rflux, alpha=0.2)

            plt.scatter(rs, sz, alpha=0.2)

            plt.xlabel(r'$r$ [kpc]')
            plt.ylabel(r'$\sigma_z$ [km/s]')

    return


if __name__ == '__main__':
    _, snap_index, group_number = sys.argv
    snap_index = int(snap_index)
    group_number = int(group_number)

    # snap_index = 0
    # group_number = 0

    #hyades
    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/images/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/images/']

    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location = particle_data_location_list[snap_index // 4]
    halo_data_location = halo_data_location_list[snap_index // 4]
    output_data_location =  output_data_location_list[snap_index // 4]

    kdtree_location = output_data_location

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]

    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    #TODO change medians to mass weighted median!

    # do the thing
    # output_data_location = '../results/'
    make_visualisation(particle_data_location, halo_data_location, snap, output_data_location,
                       group_number)#, name='proj_mass_sigma_z', sigma_z=True, save=False)

    # plt.show()
    print('Done')

    pass