#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""
import matplotlib.pyplot as plt
import numpy as np
from morphology_hdf_keys import *
from dm_run_plot_scripts import *
from galaxy_matching_experiments import *

def plot_x_vs_y_diff(raw_data0, raw_data1,
                     xkey, ykey, rkey, comp, ii, mask0, mask1,
                     xcentres0, ymedians0, yminus0, yplus0, boots0,
                     xcentres1, ymedians1, yminus1, yplus1, boots1,
                     colour='C0', linestyle='-', N_run=NRUN, thresholds=None):

    # path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    # plt.errorbar(xcentres0, ymedians1 - ymedians0,
    #              c=colour, ls=linestyle, linewidth=2, zorder=10, path_effects=path_effect)
    #
    # plt.fill_between(xcentres0, yminus1 - ymedians0, yplus1 - ymedians0,
    #                  color=colour, alpha=0.3, edgecolor='k', zorder=9)
    # plt.fill_between(xcentres0, ymedians1 - yminus0, ymedians1 - yplus0,
    #                  color=colour, alpha=0.3, edgecolor='k', zorder=9)

    if thresholds == None:
        thresholds = [0.53, 0.55, 0.60, 0.75, 0.90, 0.99]

    #
    xlims = lims_dict[xkey]
    ylims = lims_dict[ykey]

    if type(N_run) == int:
        N_run = np.linspace(*xlims, N_run)

    if np.sum(mask0) > 1:

        x0 = return_array_from_keys(raw_data0, xkey, rkey, comp, mask0, ii)
        y0 = return_array_from_keys(raw_data0, ykey, rkey, comp, mask0, ii)
        not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
        x0 = x0[not_crazy_mask0]
        y0 = y0[not_crazy_mask0]

        x1 = return_array_from_keys(raw_data1, xkey, rkey, comp, mask1, ii)
        y1 = return_array_from_keys(raw_data1, ykey, rkey, comp, mask1, ii)
        not_crazy_mask1 = np.logical_and(np.isfinite(x1), np.isfinite(y1))
        x1 = x1[not_crazy_mask1]
        y1 = y1[not_crazy_mask1]

        bins0 = np.digitize(x0, N_run)
        bins1 = np.digitize(x1, N_run)

        ymedian = np.zeros(len(N_run)-1)
        yminus = np.zeros(len(N_run)-1)
        yplus = np.zeros(len(N_run)-1)

        ygtrzero = np.zeros(len(N_run)-1)

        ythresholds = np.zeros((len(N_run)-1, len(thresholds)))

        for ibin in range(len(N_run)-1):
            y_diff_this_bin = []
            for y0_in_bin in y0[bins0 == ibin+1]:
                for y1_in_bin in y1[bins1 == ibin+1]:
                    y_diff_this_bin.append(y1_in_bin - y0_in_bin)
            y_diff_this_bin = np.array(y_diff_this_bin)

            ymedian[ibin] = np.median(y_diff_this_bin)
            yminus[ibin] = np.nanquantile(y_diff_this_bin, 0.16)
            yplus[ibin] = np.nanquantile(y_diff_this_bin, 0.84)

            ygtrzero[ibin] = np.sum(y_diff_this_bin > 0) / len(y_diff_this_bin)

            for j, threshold in enumerate(thresholds):
                ythresholds[j] = np.nanquantile(y_diff_this_bin, threshold)

    path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    plt.errorbar(xcentres0, ymedian,
                 c=colour, ls=linestyle, linewidth=2, zorder=10, path_effects=path_effect)

    plt.fill_between(xcentres0, yminus, yplus,
                     color=colour, alpha=0.3, edgecolor='k', zorder=9)

    #TODO this properly
    # [plt.axvline(0, 1, ythresh, c=colour, ls=':') for ythresh in ythresholds]

    # upper = np.sqrt((yplus1 - ymedians0)**2 + (ymedians1 - yplus0)**2)
    # lower = np.sqrt((yminus1 - ymedians0)**2 + (ymedians1 - yminus0)**2)
    # plt.fill_between(xcentres0, upper, lower,
    #                  color=colour, alpha=0.3, edgecolor='k', zorder=9)

    return ymedian, yminus, yplus, ygtrzero, ythresholds


def plot_x_vs_y_model_diff(raw_data0,
                           xkey, ykey, rkey, comp, ii, mask0,
                           xcentres0, ymedians0, yminus0, yplus0, boots0,
                           xcentresm, ymodel,
                           colour='C0', linestyle='-', N_run=NRUN, thresholds=None):

    if thresholds == None:
        thresholds = [0.53, 0.55, 0.60, 0.75, 0.90, 0.99]

    #
    xlims = lims_dict[xkey]
    ylims = lims_dict[ykey]

    if type(N_run) == int:
        N_run = np.linspace(*xlims, N_run)

    if np.sum(mask0) > 1:

        x0 = return_array_from_keys(raw_data0, xkey, rkey, comp, mask0, ii)
        y0 = return_array_from_keys(raw_data0, ykey, rkey, comp, mask0, ii)
        not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
        x0 = x0[not_crazy_mask0]
        y0 = y0[not_crazy_mask0]

        bins0 = np.digitize(x0, N_run)

        ymedian = np.zeros(len(N_run)-1)
        yminus = np.zeros(len(N_run)-1)
        yplus = np.zeros(len(N_run)-1)

        ygtrzero = np.zeros(len(N_run)-1)

        ythresholds = np.zeros((len(N_run)-1, len(thresholds)))

        for ibin in range(len(N_run)-1):
            y_diff_this_bin = []

#TODO
########################################################################################################################
            #TODO can use actual M200 of each halo rather than m200 of bin for cheap modesl
            for y0_in_bin in y0[bins0 == ibin+1]:
                y_diff_this_bin.append(y0_in_bin - ymodel[ibin])

########################################################################################################################
#TODO

            y_diff_this_bin = np.array(y_diff_this_bin)

            ymedian[ibin] = np.median(y_diff_this_bin)
            yminus[ibin] = np.nanquantile(y_diff_this_bin, 0.16)
            yplus[ibin] = np.nanquantile(y_diff_this_bin, 0.84)

            ygtrzero[ibin] = np.sum(y_diff_this_bin > 0) / len(y_diff_this_bin)

            for j, threshold in enumerate(thresholds):
                ythresholds[j] = np.nanquantile(y_diff_this_bin, threshold)

    path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    plt.errorbar(xcentres0, ymedian,
                 c=colour, ls=linestyle, linewidth=2, zorder=10, path_effects=path_effect)

    plt.fill_between(xcentres0, yminus, yplus,
                     color=colour, alpha=0.3, edgecolor='k', zorder=9)

    #TODO this properly
    # [plt.axvline(0, 1, ythresh, c=colour, ls=':') for ythresh in ythresholds]

    # upper = np.sqrt((yplus1 - ymedians0)**2 + (ymedians1 - yplus0)**2)
    # lower = np.sqrt((yminus1 - ymedians0)**2 + (ymedians1 - yminus0)**2)
    # plt.fill_between(xcentres0, upper, lower,
    #                  color=colour, alpha=0.3, edgecolor='k', zorder=9)

    return ymedian, yminus, yplus, ygtrzero, ythresholds


def make_figure_with_models(raw_data0, raw_data1=None, raw_data2=None, raw_data3=None,
                            xkey='m200', ykey='kappa_rot', rkey='r200', comp='Star', ckey=None,
                            cent_sat_split=True,
                            cent_only=False, four_z=False,
                            snap_str='', sim_str='', save=True, cbins=None, model=True):
    if cbins is None:
        cbins = [0, UNIVERSE_AGE]

    # if '000p000' in snap_str: z = 0
    # elif '000p503' in snap_str: z = 0.5
    # elif '001p004' in snap_str: z = 1
    # elif '002p012' in snap_str: z = 2
    # else:
    #     print('Warning redshift might be wrong for model')
    #     z = 0

    z = float(snap_str[7:].replace('p', '.'))
    a = 1 / (1 + z)
    snap_time = lbt(np.array([a]))[0]

    Nrun = NRUN

    times = np.array([8])

    extra_models = False

    if model:
        if xkey == 'm200':
            # cosmology
            omm = 0.307
            oml = 0.693

            H_z2 = HUBBLE_CONST ** 2 * (oml + omm * np.power(1 / (1 - z), -3))
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3

            g = 1  # 1 / (np.log1p(concs) - concs / (1 + concs)) #* concs**3 / 3
            rho_200 = 200 * rho_crit

            mdm0 = DM_MASS  # 1 / raw_data0['Header/MDMReciprocal'][()][0]
            mdm1 = DM_MASS / 7  # 1 / raw_data1['Header/MDMReciprocal'][()][0]

            #haloes
            xlims = lims_dict[xkey]
            m200_edges = np.linspace(*xlims, Nrun)  # log space
            logM200s = (m200_edges[1:] + m200_edges[:-1]) / 2

            conc = concentration_ludlow(z, 10 ** (logM200s - 10))

            r200s = np.power(3 * 10 ** (logM200s - 10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
            v200s = np.sqrt(GRAV_CONST * 10 ** (logM200s - 10) / r200s) #km / s
            rs = r200s / conc

            #disks
            r_half = 0.2 * rs  # kpc
            R_half = r_half * 3/4

            # this is not used
            # half = 0.5
            # R_half = -R_d * (lambertw((half - 1) / np.exp(1), -1) + 1).real  # kpc

            if ykey in ['sigma_z_v200']:
                # uncontracted
                nfw_vs_M200_raw = [[get_heating_at_R12(10 ** (logM200 - 10), MassDM=mdm0,
                                                       z=z, time=time - snap_time, ic_heat_fraction=0,
                                                       contracted=False, measured_R=False)
                                    for time in times] for logM200 in logM200s]
                nfw_vs_M200_raw = np.array(nfw_vs_M200_raw)

                nfw_vs_M200_0_raw = [[get_heating_at_R12(10 ** (logM200 - 10), MassDM=mdm0,
                                                       z=z, time=time - snap_time, ic_heat_fraction=0,
                                                       contracted=False, measured_R=False)
                                    for time in times] for logM200 in logM200s]
                nfw_vs_M200_0_raw = np.array(nfw_vs_M200_0_raw)

                nfw_vs_M200_1_raw = [[get_heating_at_R12(10 ** (logM200 - 10), MassDM=mdm1,
                                                         z=z, time=time - snap_time, ic_heat_fraction=0,
                                                         contracted=False, measured_R=False)
                                      for time in times] for logM200 in logM200s]
                nfw_vs_M200_1_raw = np.array(nfw_vs_M200_1_raw)

                # contracted
                nfw_vs_M200 = [[get_heating_at_R12(10 ** (logM200 - 10), MassDM=mdm0,
                                                    z=z, time=time - snap_time, ic_heat_fraction=0,
                                                    contracted=True, measured_R=False)
                                 for time in times] for logM200 in logM200s]
                nfw_vs_M200 = np.array(nfw_vs_M200)

                nfw_vs_M200_0 = [[get_heating_at_R12(10 ** (logM200 - 10), MassDM=mdm0,
                                                    z=z, time=time - snap_time, ic_heat_fraction=0,
                                                    contracted=True, measured_R=True)
                                 for time in times] for logM200 in logM200s]
                nfw_vs_M200_0 = np.array(nfw_vs_M200_0)

                nfw_vs_M200_1 = [[get_heating_at_R12(10 ** (logM200 - 10), MassDM=mdm1,
                                                    z=z, time=time - snap_time, ic_heat_fraction=0,
                                                    contracted=True, measured_R=True)
                                 for time in times] for logM200 in logM200s]
                nfw_vs_M200_1 = np.array(nfw_vs_M200_1)

                #TODO support for different indexes
                for i, time in enumerate(times):
                    # analytic_v_circ, analytic_dispersion,
                    # theory_best_v, theory_best_z, theory_best_r, theory_best_p

                    analytic_v_circ_raw = nfw_vs_M200_raw[:, i, 0]
                    analytic_v_circ_raw_0 = nfw_vs_M200_0_raw[:, i, 0]
                    analytic_v_circ_raw_1 = nfw_vs_M200_1_raw[:, i, 0]
                    analytic_v_circ = nfw_vs_M200[:, i, 0]
                    analytic_v_circ_0 = nfw_vs_M200_0[:, i, 0]
                    analytic_v_circ_1 = nfw_vs_M200_1[:, i, 0]

                    analytic_dispersion_raw = nfw_vs_M200_raw[:, i, 1]
                    analytic_dispersion_raw_0 = nfw_vs_M200_0_raw[:, i, 1]
                    analytic_dispersion_raw_1 = nfw_vs_M200_1_raw[:, i, 1]
                    analytic_dispersion = nfw_vs_M200[:, i, 1]
                    analytic_dispersion_0 = nfw_vs_M200_0[:, i, 1]
                    analytic_dispersion_1 = nfw_vs_M200_1[:, i, 1]

                    theory_best_v_raw = nfw_vs_M200_raw[:, i, 2]
                    theory_best_v_raw_0 = nfw_vs_M200_0_raw[:, i, 2]
                    theory_best_v_raw_1 = nfw_vs_M200_1_raw[:, i, 2]
                    theory_best_v = nfw_vs_M200[:, i, 2]
                    theory_best_v_0 = nfw_vs_M200_0[:, i, 2]
                    theory_best_v_1 = nfw_vs_M200_1[:, i, 2]

                    theory_best_z_raw = nfw_vs_M200_raw[:, i, 3]
                    theory_best_z_raw_0 = nfw_vs_M200_0_raw[:, i, 3]
                    theory_best_z_raw_1 = nfw_vs_M200_1_raw[:, i, 3]
                    theory_best_z = nfw_vs_M200[:, i, 3]
                    theory_best_z_0 = nfw_vs_M200_0[:, i, 3]
                    theory_best_z_1 = nfw_vs_M200_1[:, i, 3]

                    theory_best_r_raw = nfw_vs_M200_raw[:, i, 4]
                    theory_best_r_raw_0 = nfw_vs_M200_0_raw[:, i, 4]
                    theory_best_r_raw_1 = nfw_vs_M200_1_raw[:, i, 4]
                    theory_best_r = nfw_vs_M200[:, i, 4]
                    theory_best_r_0 = nfw_vs_M200_0[:, i, 4]
                    theory_best_r_1 = nfw_vs_M200_1[:, i, 4]

                    theory_best_p_raw = nfw_vs_M200_raw[:, i, 5]
                    theory_best_p_raw_0 = nfw_vs_M200_0_raw[:, i, 5]
                    theory_best_p_raw_1 = nfw_vs_M200_1_raw[:, i, 5]
                    theory_best_p = nfw_vs_M200[:, i, 5]
                    theory_best_p_0 = nfw_vs_M200_0[:, i, 5]
                    theory_best_p_1 = nfw_vs_M200_1[:, i, 5]

            if ykey == 'sigma_z_v200':
                xcentresm = logM200s
                ymodel = np.log10(analytic_dispersion / v200s)

                extra_models = True
                extra_ymodels = [#np.log10(analytic_dispersion_raw_0 / v200s),
                                 #np.log10(analytic_dispersion_raw_1 / v200s),
                                 np.log10(analytic_dispersion_0 / v200s),
                                 np.log10(analytic_dispersion_1 / v200s),
                                 np.log10(theory_best_z_raw_0 / v200s),
                                 np.log10(theory_best_z_raw_1 / v200s),
                                 # np.log10(theory_best_z_0 / v200s),
                                 # np.log10(theory_best_z_1 / v200s)
                ]

            if ykey == 'R12':
                R12 = R_half

                xcentresm = logM200s
                ymodel = np.log10(R12)

                extra_models = True

                #these are for projected radii
                if z == 0.0:
                    const0 = 10 ** 0.45
                    const1 = 10 ** -0.15
                if z == 0.503:
                    const0 = 10 ** 0.3
                    const1 = 10 ** -0.30
                if z == 1.004:
                    const0 = 10 ** 0.15
                    const1 = 10 ** -0.30
                if z == 2.012:
                    const0 = 10 ** -0.05
                    const1 = 10 ** -0.30

                r_half0 = (R_half ** 4 + const0 ** 4) ** (1 / 4)
                R_half0 = np.log10(r_half0) # kpc
                r_half1 = (R_half ** 4 + const1 ** 4) ** (1 / 4)
                R_half1 = np.log10(r_half1) # kpc

                extra_ymodels = [R_half0, R_half1]

    # plotting starts here
    nrows = (len(cbins) + 2) // 4
    ncols = min(len(cbins) - 1, 4)

    fig = plt.figure(figsize=(5 * ncols, 10 * nrows), constrained_layout=True)
    fig.set_constrained_layout_pads(w_pad=0, h_pad=0, hspace=0, wspace=0)

    spec = fig.add_gridspec(ncols=ncols, nrows=3 * nrows,
                            # width_ratios=1,
                            height_ratios=[2, 1, 1] * nrows)

    for ii in range(len(cbins) - 1):

        # ax = plt.subplot((len(cbins) + 2) // 4, min(len(cbins) - 1, 4), ii+1)

        ax = fig.add_subplot(spec[3 * (ii // 4), ii % 4])

        ax.set_aspect(np.diff(lims_dict[xkey]) / np.diff(lims_dict[ykey]))

        warnings.filterwarnings("ignore")
        plt.subplots_adjust(wspace=0, hspace=0)

        mask1key = ckey  # key0_dict[ckey][comp]
        mask1column = rkey  # column0_dict[ckey][rkey, ii]
        # mask1transform = lambda mask1: np.logical_and(cbins[ii] < transform_dict[ckey](mask1),
        #                                               transform_dict[ckey](mask1) <= cbins[ii + 1])
        mask1transform = lambda data: np.logical_and(cbins[ii] < data, data <= cbins[ii + 1])

        if cent_sat_split:
            if not cent_only:
                mask0key = 'sn'
                mask0column = None
                mask0transform = lambda mask: mask != 0

                mask = lambda raw_data: return_full_mask(raw_data,
                                                         mask0key, mask0column, comp, mask0transform,
                                                         mask1key, mask1column, comp, mask1transform,
                                                         mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                         mask_func_dict[ykey], ii)

                # satellites
                plot_x_vs_y(raw_data0,
                            xkey, ykey, rkey, comp, ii, mask(raw_data0),
                            colour='C2', linestyle=(0, (0.8, 0.8)), marker='.',
                            quartiles=quartile_dict[ykey], cmap='viridis')
                plot_x_vs_y(raw_data1,
                            xkey, ykey, rkey, comp, ii, mask(raw_data1),
                            colour='C3', linestyle='-.', marker='.',
                            quartiles=quartile_dict[ykey], cmap='viridis')

            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: mask == 0

            mask = lambda raw_data: return_full_mask(raw_data,
                                                     mask0key, mask0column, comp, mask0transform,
                                                     mask1key, mask1column, comp, mask1transform,
                                                     mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                     mask_func_dict[ykey], ii)

            # centrals
            (xcentres0, ymedians0, yminus0, yplus0, boots0
             ) = plot_x_vs_y(raw_data0,
                             xkey, ykey, rkey, comp, ii, mask(raw_data0),
                             colour='C0', linestyle='-', marker='.',
                             quartiles=quartile_dict[ykey], cmap='viridis')
            (xcentres1, ymedians1, yminus1, yplus1, boots1
             ) = plot_x_vs_y(raw_data1,
                             xkey, ykey, rkey, comp, ii, mask(raw_data1),
                             colour='C1', linestyle='--', marker='.',
                             quartiles=quartile_dict[ykey], cmap='viridis')

            title = 'z=' + snap_str[7:].replace('p', '.')
            if len(cbins) > 2:
                title += '\n' + label_dict[ckey] + ': ' + str(cbins[ii]) + ' - ' + str(round(cbins[ii + 1], 1))
            if cent_only:
                l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)), ],
                               [r'$1\times$DM Centrals',
                                r'$7\times$DM Centrals'],
                               title=title, ncol=1, )

            else:
                l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C3', ls='-.', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C2', ls=(0, (0.8, 0.8)), linewidth=3))],
                               [r'$1\times$DM Centrals', r'$1\times$DM Satellites',
                                r'$7\times$DM Centrals', r'$7\times$DM Satellites'],
                               title=title, ncol=1, )
            l.set_zorder(-10)
            l.get_title().set_multialignment('center')

        elif four_z:

            if cent_only:
                mask0key = 'sn'
                mask0column = None
                mask0transform = lambda mask: mask == 0
            else:
                mask0key = 'sn'
                mask0column = None
                mask0transform = lambda mask: True

            mask = lambda raw_data: return_full_mask(raw_data,
                                                     mask0key, mask0column, comp, mask0transform,
                                                     mask1key, mask1column, comp, mask1transform,
                                                     mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                     mask_func_dict[ykey], ii)

            (xcentres3, ymedians3, yminus3, yplus3, boots3
             ) = plot_x_vs_y(raw_data3,
                             xkey, ykey, rkey, comp, ii, mask(raw_data3),
                             colour=matplotlib.cm.get_cmap('inferno')(0.5 / 4), linestyle=(0, (0.8, 0.8)), marker='.',
                             quartiles=quartile_dict[ykey])
            (xcentres2, ymedians2, yminus2, yplus2, boots2
             ) = plot_x_vs_y(raw_data2,
                             xkey, ykey, rkey, comp, ii, mask(raw_data2),
                             colour=matplotlib.cm.get_cmap('inferno')(1.5 / 4), linestyle='--', marker='.',
                             quartiles=quartile_dict[ykey])
            (xcentres1, ymedians1, yminus1, yplus1, boots1
             ) = plot_x_vs_y(raw_data1,
                             xkey, ykey, rkey, comp, ii, mask(raw_data1),
                             colour=matplotlib.cm.get_cmap('inferno')(2.5 / 4), linestyle='-.', marker='.',
                             quartiles=quartile_dict[ykey])
            (xcentres0, ymedians0, yminus0, yplus0, boots0
             ) = plot_x_vs_y(raw_data0,
                             xkey, ykey, rkey, comp, ii, mask(raw_data0),
                             colour=matplotlib.cm.get_cmap('inferno')(3.5 / 4), linestyle='-', marker='.',
                             quartiles=quartile_dict[ykey])

            title = sim_str
            if len(cbins) > 2:
                title += '\n' + label_dict[ckey] + ': ' + str(round(cbins[ii], 2)) + ' - ' + str(
                    round(cbins[ii + 1], 2))
            l = plt.legend(
                [(lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(3.5 / 4), ls='-', linewidth=3)),
                 (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(2.5 / 4), ls='-.', linewidth=3)),
                 (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(1.5 / 4), ls='--', linewidth=3)),
                 (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(0.5 / 4), ls=(0, (0.8, 0.8)),
                               linewidth=3))],
                [r'$z=0$', r'$z=0.5$', r'$z=1$', r'$z=2$'],
                title=title, ncol=2, )
            l.set_zorder(-10)
            l.get_title().set_multialignment('center')

        if model:
            plt.errorbar(xcentresm, ymodel,
                         color='k', linestyle='-.', linewidth=3)

            if extra_models:
                for extra_y in extra_ymodels:
                    plt.errorbar(xcentresm, extra_y,
                                 color='k', linestyle='-.', linewidth=2)

        ax1 = fig.add_subplot(spec[3 * (ii // 4) + 1, ii % 4])
        ax1.set_aspect(np.diff(lims_dict[xkey]) / 2 / 2)

        # models
        if model:
            if not np.all(np.isclose(xcentres0, xcentres1)):
                raise ValueError(f'X bins are not the same! They are: \n{xcentres0} \n{xcentres1}')

            plt.axhline(0,1, 0, ls=':', c='grey')

            (ymedian, yminus, yplus, ygtrzero, ythresholds
             ) = plot_x_vs_y_diff(raw_data0, raw_data1,
                                  xkey, ykey, rkey, comp, ii, mask(raw_data0), mask(raw_data1),
                                  xcentres0, ymedians0, yminus0, yplus0, boots0,
                                  xcentres1, ymedians1, yminus1, yplus1, boots1,
                                  colour='C4', linestyle=(0, (1, 0.5, 1, 0.5, 4, 0.5)))

            (ymedian0, yminus0, yplus0, ygtrzero0, ythresholds0
             ) = plot_x_vs_y_model_diff(raw_data0,
                                        xkey, ykey, rkey, comp, ii, mask(raw_data0),
                                        xcentres0, ymedians0, yminus0, yplus0, boots0,
                                        xcentresm, ymodel,
                                        colour='C0', linestyle='-')

            (ymedian1, yminus1, yplus1, ygtrzero1, ythresholds1
             ) = plot_x_vs_y_model_diff(raw_data1,
                                        xkey, ykey, rkey, comp, ii, mask(raw_data1),
                                        xcentres1, ymedians1, yminus1, yplus1, boots1,
                                        xcentresm, ymodel,
                                        colour='C1', linestyle='--')

            # xcentres0, ymedians0, yminus0, yplus0, boots0

            pass

        ax2 = fig.add_subplot(spec[3 * (ii // 4) + 2, ii % 4])
        ax2.set_aspect(np.diff(lims_dict[xkey]) / 2)

        # models
        if model:
            plt.axhline(0.5, 0,1, ls=':', c='grey')

            path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
            plt.errorbar(xcentres0, ygtrzero,
                         c='C4', linestyle=(0, (1, 0.5, 1, 0.5, 4, 0.5)), linewidth=2, zorder=10, path_effects=path_effect)

            plt.errorbar(xcentres0, ygtrzero0,
                         c='C0', linestyle='-', linewidth=2, zorder=10, path_effects=path_effect)

            plt.errorbar(xcentres0, ygtrzero1,
                         c='C1', linestyle='--', linewidth=2, zorder=10, path_effects=path_effect)

            #TODO this properly
            # [plt.axvline(0, 1, ythresh, c=colour, ls=':') for ythresh in ythresholds]


        #
        # labels and colorbars
        if ii % 4 == 0:
            ylab = label_dict[ykey]

            if ((column0_dict[xkey][rkey, 0] == column0_dict[xkey]['test', 0]) and
                    (column1_dict[xkey][rkey, 0] == column1_dict[xkey]['test', 0]) and
                    (column2_dict[xkey][rkey, 0] == column2_dict[xkey]['test', 0]) and
                    (column0_dict[ykey][rkey, 0] == column0_dict[ykey]['test', 0]) and
                    (column1_dict[ykey][rkey, 0] == column1_dict[ykey]['test', 0]) and
                    (column2_dict[ykey][rkey, 0] == column2_dict[ykey]['test', 0])):
                pass
            else:
                ylab += ' ' + rkey_label_dict[rkey]
            ax.set_ylabel(ylab)
            # TODO
            ax1.set_ylabel(ylab + '-' + ylab)
            ax2.set_ylabel('Fraction')
        else:
            ax.set_yticklabels([])
            ax1.set_yticklabels([])
            ax2.set_yticklabels([])

        if ii // 4 == (len(cbins) - 2) // 4:
            plt.xlabel(label_dict[xkey])

            if xkey in ['m200', 'mtot']:
                ax.set_xticks([8, 9, 10, 11, 12, 13, 14])
                ax.set_xticklabels([])
                ax1.set_xticks([8, 9, 10, 11, 12, 13, 14])
                ax1.set_xticklabels([])
                ax2.set_xticks([8, 9, 10, 11, 12, 13, 14])
                ax2.set_xticklabels([8, 9, 10, 11, 12, 13, None])
            else:
                ax.set_xticklabels([])
                ax1.set_xticklabels([])
        else:
            ax.set_xticklabels([])
            ax1.set_xticklabels([])
            # ax2.set_xticklabels([])

        ax.set_xlim(lims_dict[xkey])
        ax.set_ylim(lims_dict[ykey])

        ax1.set_xlim(lims_dict[xkey])
        ax1.set_ylim([-1, 1])
        ax2.set_xlim(lims_dict[xkey])
        ax2.set_ylim([0, 1])

    if save:
        filename = '../results/full_'
        filename += filename_dict[xkey]
        filename += '_' + filename_dict[ykey]
        filename += '_' + filename_dict[rkey]
        filename += '_' + filename_dict[comp]
        if ckey != None:
            filename += '_' + filename_dict[ckey]
        if cent_sat_split:
            filename += '_' + snap_str
            if not cent_only:
                filename += '_cent_sat'
        elif four_z:
            filename += '_' + sim_str
        else:
            filename += '_' + sim_str + '_' + snap_str
        if cent_only:
            filename += '_cent'
        if len(cbins) > 2:
            filename += f'_{len(cbins) - 1}bins'
        filename += '.png'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def make_profile_with_models(raw_data0, raw_data1=None, raw_data2=None, raw_data3=None,
                             cent_sat_split=True,
                             cent_only=True, four_z=False,
                             snap_str='', sim_str='', save=True, cbins=None, nan_to_x=-10,#np.nan,#-10,
                             points=True, all_comp=True, N_ress=None, model=True, shade=True):

    if cbins is None:
        cbins = [0, UNIVERSE_AGE]

    if N_ress is None:
        N_ress = [20, 1] #[1000, 100, 1]

    rkey = 'r200'

    ckey = 'm200' #'m200'
    c0key = None #'r12_r200' #'mstarm200' #'age'
    # c0column = rkey #'eq_02rs' #'r200''

    # cbins = [0, 2, 4, 6, 8, 10, 12, UNIVERSE_AGE] # [[0, UNIVERSE_AGE],] #
    # ckey = 'age' #'m200' #'m200'
    # c0key = None #'r12_r200' #'mstarm200' #'age'
    # c0column = rkey #'eq_02rs' #'r200''

    comp = 'Star'

    ykeys = ['v_circ', 'mean_v_phi', 'mean_v_R', 'median_v_phi', 'mean_v_phi_v_circ', 'mean_v_R_v_circ',
             'sigma_z', 'cum_jz', 'jz', 'jtot', 'mstarr', 'sigma_phi']
    ykey = 'jtot' #'z12' #'mstarr' #'root_ek_v200' #'mean_v_phi_v200'

    if ykey in ['v_circ', 'mean_v_phi_v_circ', 'mean_v_R_v_circ']:
        points = False

    if ykey == 'jz':
        shade = False

    # mask2key = 'kappa_co'
    # mask2column = 'r200'
    # mask2transform = lambda k_co: k_co > 0.4

    # star_radial_bin_mask = np.zeros(n_dim + n_star_extra, dtype=bool)
    # star_radial_bin_mask[:n_log] = True
    # radial_bin_mask = np.zeros(n_dim, dtype=bool)
    # radial_bin_mask[:n_log] = True

    star_radial_bin_mask = np.arange(n_log)
    radial_bin_mask = np.arange(n_log)

    r_ls = np.array([btf(entry) for entry in lower_bin_edges][:n_log])
    r_hs = np.array([btf(entry) for entry in upper_bin_edges][:n_log])
    radial_bin_centres = (np.log10(r_ls) + np.log10(r_hs))/2

    r_lims = [-1, 2.5] #ckpc

    # ####
    # if len(cbins) - 1 > 1:
    plt.figure(figsize=(5.3 * (len(cbins) - 1), 5.3), constrained_layout=True)

    # elif cent_sat_split or four_z:
    #     plt.figure(figsize=(6.4,6.4), constrained_layout=True)
    # else:
    #     #need to make space for color bar
    #     plt.figure(figsize=(6.4,5.3), constrained_layout=True)
    # # plt.figure(constrained_layout=True)

    # if len(cbins) -1 > 1:

    for ii in range(len(cbins)-1):
        ax = plt.subplot(1, len(cbins)-1, ii+1)
        ax.set_aspect(np.diff(r_lims) / np.diff(lims_dict[ykey]))

        warnings.filterwarnings("ignore")
        plt.subplots_adjust(wspace=0)

        mask1key = ckey #mass
        mask1column = rkey
        mask1transform = lambda data: np.logical_and(cbins[ii] < data, data <= cbins[ii + 1])

        if cent_sat_split:
            if not cent_only:
                raise NotImplementedError

            # mask0key = 'sn'
            # mask0column = None
            # mask0transform = lambda mask: mask == 0

            mask0key = 'mstar'
            mask0column = 'r200'
            mask0transform = lambda data: np.logical_and(9 < data, data <= 9.2)

            mask = lambda raw_data : return_full_mask(raw_data,
                                                      mask0key, mask0column, comp, mask0transform,
                                                      mask1key, mask1column, comp, mask1transform,
                                                      # mask2key, mask2column, comp, mask2transform,
                                                      # mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                      # mask_func_dict[ykey],
                                                      ii)

            # for comp, colors in zip(['Star', 'Gas', 'DM'], [['C0', 'C1'], ['C2', 'C3'], ['C4', 'C5']]):

            # if all_comp:
            #centrals
            comp = 'Star'
            if c0key is None:
                plot_r_vs_y(raw_data0,
                            star_radial_bin_mask, radial_bin_centres,
                            ykey, comp, mask(raw_data0),
                            show_all=True,
                            colour='C0', linestyle = '-', marker = '.', shade=shade, nan_to_x=nan_to_x, N_ress=N_ress)
                plot_r_vs_y(raw_data1,
                            star_radial_bin_mask, radial_bin_centres,
                            ykey, comp, mask(raw_data1),
                            show_all=True,
                            colour='C1', linestyle = '--', marker = '.', shade=shade, nan_to_x=nan_to_x, N_ress=N_ress)

            else:
                plot_r_vs_y_percentiles(raw_data0,
                                        star_radial_bin_mask, radial_bin_centres,
                                        ykey, comp, mask(raw_data0), c0key, c0column,
                                        colour='C0', linestyle = '-', marker = '.', shade=False, nan_to_x=nan_to_x,
                                        N_ress=N_ress)
                plot_r_vs_y_percentiles(raw_data1,
                                        star_radial_bin_mask, radial_bin_centres,
                                        ykey, comp, mask(raw_data1), c0key, c0column,
                                        colour='C1', linestyle = '--', marker = '.', shade=False, nan_to_x=nan_to_x,
                                        N_ress=N_ress)

            # if all_comp:
            #     #not stars
            #     comp = 'Gas'
            #     plot_r_vs_y(raw_data0,
            #                 radial_bin_mask, radial_bin_centres,
            #                 ykey, comp, mask(raw_data0),
            #                 colour='C2', linestyle = '-', marker = '.', shade=False, nan_to_x=nan_to_x,
            #                 N_ress=N_ress)
            #     plot_r_vs_y(raw_data1,
            #                 radial_bin_mask, radial_bin_centres,
            #                 ykey, comp, mask(raw_data1),
            #                 colour='C3', linestyle = '--', marker = '.', shade=False, nan_to_x=nan_to_x,
            #                 N_ress=N_ress)

            if all_comp and ykey in ['sigma_z', 'sigma_R', 'sigma_phi']:
                comp = 'DM'
                try:
                    # _ykey = ykey
                    _ykey = 'sigma_z'
                    # _ykey = 'sigma_tot'
                    plot_r_vs_y(raw_data0,
                                radial_bin_mask, radial_bin_centres,
                                'DM' + _ykey, comp, mask(raw_data0),
                                colour='C4', linestyle = '-', marker = '.', shade=False, nan_to_x=nan_to_x,
                                N_ress=N_ress)
                    plot_r_vs_y(raw_data1,
                                radial_bin_mask, radial_bin_centres,
                                'DM' + _ykey, comp, mask(raw_data1),
                                colour='C5', linestyle = '--', marker = '.', shade=False, nan_to_x=nan_to_x,
                                N_ress=N_ress)
                except KeyError as e:
                    print('DM', e)
                    pass

            if all_comp and ykey in ['sigma_tot']:
                _comp = 'DM'
                try:
                    # _ykey = ykey
                    _ykey = 'sigma_tot'
                    # _ykey = 'sigma_tot'
                    plot_r_vs_y(raw_data0,
                                radial_bin_mask, radial_bin_centres,
                                'DM' + _ykey, _comp, mask(raw_data0),
                                colour='C4', linestyle = '-', marker = '.', shade=False, nan_to_x=nan_to_x,
                                N_ress=[-1])
                    plot_r_vs_y(raw_data1,
                                radial_bin_mask, radial_bin_centres,
                                'DM' + _ykey, _comp, mask(raw_data1),
                                colour='C5', linestyle = '--', marker = '.', shade=False, nan_to_x=nan_to_x,
                                N_ress=[-1])
                except KeyError as e:
                    print('DM', e)
                    pass

            if all_comp and ykey in ['mean_v_phi', 'mean_v_R', 'v_circ', 'median_v_phi']:
                comp = 'DM'
                _ykey = 'v_circ'
                plot_r_vs_y(raw_data0,
                            star_radial_bin_mask, radial_bin_centres,
                            _ykey, comp, mask(raw_data0),
                            colour='C4', linestyle = '-', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)
                plot_r_vs_y(raw_data1,
                            star_radial_bin_mask, radial_bin_centres,
                            _ykey, comp, mask(raw_data1),
                            colour='C5', linestyle = '--', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)

                # comp = 'ALL'
                # _ykey = 'v_circ'
                # plot_r_vs_y(raw_data0,
                #             star_radial_bin_mask, radial_bin_centres,
                #             _ykey, comp, mask(raw_data0),
                #             colour='C6', linestyle = '-', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)
                # plot_r_vs_y(raw_data1,
                #             star_radial_bin_mask, radial_bin_centres,
                #             _ykey, comp, mask(raw_data1),
                #             colour='C7', linestyle = '--', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)

            if all_comp and ykey == 'mstarr':
                _ykey = 'mgasr'
                _comp = 'Gas'
                plot_r_vs_y(raw_data0,
                            star_radial_bin_mask, radial_bin_centres,
                            _ykey, _comp, mask(raw_data0),
                            colour='C2', linestyle = '-', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)
                plot_r_vs_y(raw_data1,
                            star_radial_bin_mask, radial_bin_centres,
                            _ykey, _comp, mask(raw_data1),
                            colour='C3', linestyle = '--', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)

                _ykey = 'mdmr'
                _comp = 'DM'
                plot_r_vs_y(raw_data0,
                            star_radial_bin_mask, radial_bin_centres,
                            _ykey, comp, mask(raw_data0),
                            colour='C4', linestyle = '-', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)
                plot_r_vs_y(raw_data1,
                            star_radial_bin_mask, radial_bin_centres,
                            _ykey, comp, mask(raw_data1),
                            colour='C5', linestyle = '--', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)

                _comp = 'ALL'
                plot_r_vs_y(raw_data0,
                            star_radial_bin_mask, radial_bin_centres,
                            ykey, _comp, mask(raw_data0),
                            colour='C6', linestyle = '-', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)
                plot_r_vs_y(raw_data1,
                            star_radial_bin_mask, radial_bin_centres,
                            ykey, _comp, mask(raw_data1),
                            colour='C7', linestyle = '--', marker = '.', N_ress=[-1], shade=False, nan_to_x=nan_to_x)

            if all_comp and ykey in ['ek', 'cum_ek']:
                _comp = 'DM'
                plot_r_vs_y(raw_data0,
                            radial_bin_mask, radial_bin_centres,
                            'DMek', _comp, mask(raw_data0),
                            colour='C4', linestyle = '-', marker = '.', shade=False, nan_to_x=nan_to_x,
                            N_ress=[-1])
                plot_r_vs_y(raw_data1,
                            radial_bin_mask, radial_bin_centres,
                            'DMek', _comp, mask(raw_data1),
                            colour='C5', linestyle = '--', marker = '.', shade=False, nan_to_x=nan_to_x,
                            N_ress=[-1])

            if points:
                comp = 'Star'
                #points and medians
                # xkey = 'r12'
                # _rkey = 'eq_rh' #'r200'
                # plot_x_vs_y(raw_data0,
                #             xkey, ykey, _rkey, comp, 0, mask(raw_data0),
                #             colour='C0', linestyle = None, marker = 'P', bootstrap=False, shade=False)
                # plot_x_vs_y(raw_data1,
                #             xkey, ykey, _rkey, comp, 0, mask(raw_data1),
                #             colour='C1', linestyle = None, marker = 'P', bootstrap=False, shade=False)
                #
                # #put medians on plot
                # mask0 = mask(raw_data0)
                # if np.sum(mask0) != 0:
                #     x0 = return_array_from_keys(raw_data0, xkey, _rkey, comp, mask0)
                #     y0 = return_array_from_keys(raw_data0, ykey, _rkey, comp, mask0)
                #     N0 = return_array_from_keys(raw_data0, 'Npart', 'r200', comp, mask0)
                #     # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                #
                #     if nan_to_x is not None:
                #         x0[~np.isfinite(y0)] = nan_to_x
                #         y0[~np.isfinite(y0)] = nan_to_x
                #
                #     N_ress = [1000, 100, 1]
                #
                #     for s, N_res in enumerate(N_ress):
                #         N_mask = N0 > N_res
                #
                #         not_crazy_mask0 = np.logical_and(np.logical_and(np.isfinite(x0), np.isfinite(y0)), N_mask)
                #
                #         plt.errorbar(np.median(x0[not_crazy_mask0]), np.median(y0[not_crazy_mask0]),
                #                      marker='P', markersize=16, markeredgecolor='k', markerfacecolor='C0', markeredgewidth=1.6,
                #                      alpha=1-0.4*s, zorder=11-s)
                #
                # mask1 = mask(raw_data1)
                # if np.sum(mask1) != 0:
                #     x1 = return_array_from_keys(raw_data1, xkey, _rkey, comp, mask1)
                #     y1 = return_array_from_keys(raw_data1, ykey, _rkey, comp, mask1)
                #     N1 = return_array_from_keys(raw_data1, 'Npart', 'r200', comp, mask1)
                #     # not_crazy_mask1 = np.logical_and(np.isfinite(x1), np.isfinite(y1))
                #
                #     if nan_to_x is not None:
                #         x1[~np.isfinite(y1)] = nan_to_x
                #         y1[~np.isfinite(y1)] = nan_to_x
                #
                #     for s, N_res in enumerate(N_ress):
                #         N_mask = N1 > N_res
                #
                #         not_crazy_mask1 = np.logical_and(np.logical_and(np.isfinite(x1), np.isfinite(y1)), N_mask)
                #
                #         plt.errorbar(np.median(x1[not_crazy_mask1]), np.median(y1[not_crazy_mask1]),
                #                      marker='P', markersize=16, markeredgecolor='k', markerfacecolor='C1', markeredgewidth=1.6,
                #                      alpha=1-0.4*s, zorder=11-s)


                if 'cum_' in ykey:
                    _ykey = ykey[4:]
                    xkey = 'r12'#'r200'
                    _rkey = 'lt_r12' #'r200'
                else:
                    _ykey = ykey
                    xkey = 'r12' #'02rs' #'r12' #'02rs'
                    _rkey = 'eq_r12' #'eq_02rs' #'eq_r12' #'eq_02rs'

                # plot_x_vs_y(raw_data0,
                #             xkey, _ykey, _rkey, comp, 0, mask(raw_data0),
                #             colour='C0', linestyle = None, marker = 'X', bootstrap=False, shade=False,
                #             N_ress=N_ress, nan_to_x=nan_to_x)
                # plot_x_vs_y(raw_data1,
                #             xkey, _ykey, _rkey, comp, 0, mask(raw_data1),
                #             colour='C1', linestyle = None, marker = 'X', bootstrap=False, shade=False,
                #             N_ress=N_ress, nan_to_x=nan_to_x)

                #put medians on plot x2
                mask0 = mask(raw_data0)
                if np.sum(mask0) != 0:
                    x0 = return_array_from_keys(raw_data0, xkey, _rkey, comp, mask0)
                    y0 = return_array_from_keys(raw_data0, _ykey, _rkey, comp, mask0)
                    N0 = return_array_from_keys(raw_data0, 'Npart', _rkey, comp, mask0)
                    # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))

                    if nan_to_x is not None:
                        x0[~np.isfinite(x0)] = nan_to_x
                        y0[~np.isfinite(y0)] = nan_to_x

                    for s, N_res in enumerate(N_ress):
                        N_mask = N0 > N_res

                        not_crazy_mask0 = np.logical_and(np.logical_and(np.isfinite(x0), np.isfinite(y0)), N_mask)

                        plt.errorbar(np.nanmedian(x0[not_crazy_mask0]), np.nanmedian(y0[not_crazy_mask0]),
                                     marker='X', markersize=16, markeredgecolor='k', markerfacecolor='C0', markeredgewidth=1.6,
                                     alpha=1-0.4*s, zorder=11-s)

                mask1 = mask(raw_data1)
                if np.sum(mask1) != 0:
                    x1 = return_array_from_keys(raw_data1, xkey, _rkey, comp, mask1)
                    y1 = return_array_from_keys(raw_data1, _ykey, _rkey, comp, mask1)
                    N1 = return_array_from_keys(raw_data1, 'Npart', _rkey, comp, mask1)
                    # not_crazy_mask1 = np.logical_and(np.isfinite(x1), np.isfinite(y1))

                    if nan_to_x is not None:
                        x1[~np.isfinite(x1)] = nan_to_x
                        y1[~np.isfinite(y1)] = nan_to_x

                    for s, N_res in enumerate(N_ress):
                        N_mask = N1 > N_res

                        not_crazy_mask1 = np.logical_and(np.logical_and(np.isfinite(x1), np.isfinite(y1)), N_mask)

                        plt.errorbar(np.nanmedian(x1[not_crazy_mask1]), np.nanmedian(y1[not_crazy_mask1]),
                                     marker='X', markersize=16, markeredgecolor='k', markerfacecolor='C1', markeredgewidth=1.6,
                                     alpha=1-0.4*s, zorder=11-s)

        elif four_z:
            raise NotImplementedError
        else:
            raise NotImplementedError



        #models
        plt.axvline(np.log10(0.7),  0,1, c='grey', ls=':')
        plt.axvline(np.log10(2.8*0.7), 0,1, c='grey', ls=':')

        if model:
            radii = np.logspace(-1, 2.5, 5*7+1)
            path_effect = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]

            z = float(snap_str[7:].replace('p', '.'))
            if np.isclose(z, 0):
                time = 8.
            elif np.isclose(z, 0.503):
                time = 4.
            elif np.isclose(z, 1.004):
                time = 2.
            elif np.isclose(z, 2.012):
                time = 1.
            else: raise ValueError

            if ykey in ['v_circ', 'mean_v_phi', 'mean_v_R', 'median_v_phi']:
                if ckey == 'm200':
                    # M200_low = 10**(cbins[ii] - 10)
                    # M200_hi = 10**(cbins[ii + 1] - 10)
                    M200_mid = 10**((cbins[ii] + cbins[ii + 1])/2 - 10)
                    out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                                                 contracted=True, measured_R=True)
                    out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS/7, z=z, time=time,
                                                contracted=True, measured_R=True)

                    # plt.errorbar(np.log10(radii), np.log10(out_low[0]),
                    #              ls='-.', c='k', linewidth=2, path_effects=path_effect)
                    if ykey != 'v_circ':
                        plt.errorbar(np.log10(radii), np.log10(out_hi[0]),
                                     ls=':', c='k', linewidth=2, path_effects=path_effect)

                        plt.errorbar(np.log10(radii), np.log10(out_low[2]),
                                 ls='-.', c='C0', linewidth=2, path_effects=path_effect)
                        plt.errorbar(np.log10(radii), np.log10(out_hi[2]),
                                 ls=(0, (0.8, 0.8)), c='C1', linewidth=2, path_effects=path_effect)

                    # else:
                    #     # out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                    #     #                              contracted=False, measured_R=True)
                    #     out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS/7, z=z, time=time,
                    #                                 contracted=False, measured_R=True)
                    #
                    #     plt.errorbar(np.log10(radii), np.log10(out_hi[0]),
                    #                  ls='-.', c='k', linewidth=2, path_effects=path_effect)


                    # #NFW
                    # out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                    #                              contracted=False, measured_R=True)
                    # out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS/7, z=z, time=time,
                    #                             contracted=False, measured_R=True)
                    # #NFW
                    # plt.errorbar(np.log10(radii), np.log10(out_low[0]),
                    #              ls='-.', c='k', linewidth=2, path_effects=path_effect)
                    # plt.errorbar(np.log10(radii), np.log10(out_hi[0]),
                    #              ls=':', c='k', linewidth=2, path_effects=path_effect)

            if ykey == 'sigma_phi':
                if ckey == 'm200':
                    # M200_low = 10**(cbins[ii] - 10)
                    # M200_hi = 10**(cbins[ii + 1] - 10)
                    M200_mid = 10**((cbins[ii] + cbins[ii + 1])/2 - 10)
                    out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                                                 contracted=True, measured_R=True)
                    out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS/7, z=z, time=time,
                                                contracted=True, measured_R=True)

                    plt.errorbar(np.log10(radii), np.log10(out_low[1]),
                                 ls='-.', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_low[5]),
                                 ls='-.', c='C0', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[1]),
                                 ls=':', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[5]),
                                 ls=(0, (0.8, 0.8)), c='C1', linewidth=2, path_effects=path_effect)

            if ykey == 'sigma_R':
                if ckey == 'm200':
                    # M200_low = 10**(cbins[ii] - 10)
                    # M200_hi = 10**(cbins[ii + 1] - 10)
                    M200_mid = 10 ** ((cbins[ii] + cbins[ii + 1]) / 2 - 10)
                    out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                                                 contracted=True, measured_R=True)
                    out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS / 7, z=z, time=time,
                                                contracted=True, measured_R=True)

                    plt.errorbar(np.log10(radii), np.log10(out_low[1]),
                                 ls='-.', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_low[4]),
                                 ls='-.', c='C0', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[1]),
                                 ls=':', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[4]),
                                 ls=(0, (0.8, 0.8)), c='C1', linewidth=2, path_effects=path_effect)

            if ykey == 'sigma_z':
                if ckey == 'm200':
                    # M200_low = 10**(cbins[ii] - 10)
                    # M200_hi = 10**(cbins[ii + 1] - 10)
                    M200_mid = 10**((cbins[ii] + cbins[ii + 1])/2 - 10)
                    out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                                                 contracted=True, measured_R=True)
                    out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS/7, z=z, time=time,
                                                contracted=True, measured_R=True)

                    plt.errorbar(np.log10(radii), np.log10(out_low[1]),
                                 ls='-.', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_low[3]),
                                 ls='-.', c='C0', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[1]),
                                 ls=':', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[3]),
                                 ls=(0, (0.8, 0.8)), c='C1', linewidth=2, path_effects=path_effect)

            if ykey == 'sigma_tot':
                if ckey == 'm200':
                    # M200_low = 10**(cbins[ii] - 10)
                    # M200_hi = 10**(cbins[ii + 1] - 10)
                    M200_mid = 10**((cbins[ii] + cbins[ii + 1])/2 - 10)
                    out_low = get_heated_profile(radii, M200_mid, MassDM=DM_MASS, z=z, time=time,
                                                 contracted=True, measured_R=True)
                    out_hi = get_heated_profile(radii, M200_mid, MassDM=DM_MASS/7, z=z, time=time,
                                                contracted=True, measured_R=True)

                    out_low_tot = np.sqrt(out_low[3]**2 + out_low[4]**2 + out_low[5]**2)
                    out_hi_tot = np.sqrt(out_hi[3]**2 + out_hi[4]**2 + out_hi[5]**2)

                    plt.errorbar(np.log10(radii), np.log10(out_low[1] * np.sqrt(3)),
                                 ls='-.', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_low_tot),
                                 ls='-.', c='C0', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi[1] * np.sqrt(3)),
                                 ls=':', c='k', linewidth=2, path_effects=path_effect)
                    plt.errorbar(np.log10(radii), np.log10(out_hi_tot),
                                 ls=(0, (0.8, 0.8)), c='C1', linewidth=2, path_effects=path_effect)

        #decorations
        title = 'z=' + snap_str[7:].replace('p', '.')
        if len(cbins) > 2:
            title += '\n' + label_dict[ckey] + ': ' + str(round(cbins[ii], 1)) + ' - ' + str(round(cbins[ii+1], 1))
        if cent_only:
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),],
                       [r'Stars [$1\times$DM]',
                        r'Stars [$7\times$DM]'],
                       title=title, ncol=2,)
            # l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
            #                 (lines.Line2D([0, 1], [0, 1], color='C5', ls='--', linewidth=3)),
            #                 (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
            #                 (lines.Line2D([0, 1], [0, 1], color='C4', ls='-', linewidth=3)),],
            #            [r'Stars [$1\times$DM]',
            #             r'DM [$1\times$DM]',
            #             r'Stars [$7\times$DM]',
            #             r'DM [$7\times$DM]'],
            #            title=title, ncol=2,)
        else:
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C3', ls='-.', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C2', ls=(0,(0.8,0.8)), linewidth=3))],
                           [r'$1\times$DM Centrals', r'$1\times$DM Satellites',
                            r'$7\times$DM Centrals', r'$7\times$DM Satellites'],
                           title=title, ncol=2,)
        l.set_zorder(-10)
        l.get_title().set_multialignment('center')

        if ii == 0:
            plt.ylabel(label_dict[ykey])
        else:
            ax.set_yticklabels([])

        plt.xlabel(r'$\log \, r / $pkpc')

        plt.ylim(lims_dict[ykey])
        plt.xlim(r_lims)

    if save:
        filename = '../results/full_'
        filename += filename_dict[ykey]
        filename += '_profile'
        filename += '_' + filename_dict[comp]
        if c0key != None:
            filename += '_' + filename_dict[c0key]
        if cent_sat_split:
            filename += '_' + snap_str
            if not cent_only:
                filename += '_cent_sat'
        elif four_z:
            filename += '_' + sim_str
        else:
            filename += '_' + sim_str + '_' + snap_str
        if cent_only:
            filename += '_cent'
        if len(cbins) > 2:
            filename += f'_{len(cbins)-1}bins'
        filename += '.png'
        plt.savefig(filename, bbox_inches='tight')
        print(filename)
        plt.close()

    return


if __name__ == '__main__':

    start_time = time.time()

    save = True

    # file locations
    combined_name_list = [
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5',
    ]

    combined_name_pairs = [
        # ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi_narrow.hdf5',
        # '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi_narrow.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5'],
        # ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5',
        # '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5']
    ]

    combined_name_redshifts = [
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5', ],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5',
         '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5', ],
    ]

    matched_groups_files = [
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_groups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_groups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_groups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_groups_only_25.hdf5'
    ]
    matched_subs_files = [
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_subgroups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_subgroups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_subgroups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_subgroups_only_25.hdf5'
    ]

    # plots to make
    xkeys = ['m200']  # ['mstar']
    ykeys = ['jtot'] #['sigma_z_v200'] # ['R12']  #
    rkeys = ['eq_R12']
    ckeys = ['age']
    cbins = [[0, 2, 4, 6, 8, 10, 12, UNIVERSE_AGE], ] #[[0, UNIVERSE_AGE],] #
    comps = ['Star']

    for xkey in xkeys:
        for ykey in ykeys:
            for rkey in rkeys:
                for comp in comps:
                    for cbin in cbins:
                        # for ckey in ckeys:
                        #     # for pair in combined_name_pairs:
                        #     pair = combined_name_pairs[0]
                        #     with h5.File(pair[0], 'r') as raw_data0:
                        #         with h5.File(pair[1], 'r') as raw_data1:
                        #             make_figure_with_models(raw_data0, raw_data1, None, None,
                        #                                     xkey, ykey, rkey, comp, ckey,
                        #                                     cent_sat_split=True,
                        #                                     # cent_only= xkey != 'mstar',
                        #                                     cent_only=True,
                        #                                     four_z=False,
                        #                                     snap_str=pair[0][53:65], sim_str=pair[0][41:50], save=save,
                        #                                     cbins=cbin, model=True)

                        # for sim in combined_name_redshifts:
                        #     with h5.File(sim[0], 'r') as raw_data0:
                        #         with h5.File(sim[1], 'r') as raw_data1:
                        #             with h5.File(sim[2], 'r') as raw_data2:
                        #                 with h5.File(sim[3], 'r') as raw_data3:
                        #
                        #                     make_figure_with_models(raw_data0, raw_data1, raw_data2, raw_data3,
                        #                                      xkey, ykey, rkey, comp, ckey,
                        #                                      cent_sat_split=False,
                        #                                      cent_only=True, four_z=True,
                        #                                      snap_str=sim[0][53-2*(sim[0][55]=='z'):65-2*(sim[0][55]=='z')],
                        #                                      sim_str=sim[0][41:50], save=save,
                        #                                      cbins=cbin)

                        # # for run in combined_name_list:
                        # for run in combined_name_list[0::4]:
                        #     with h5.File(run, 'r') as raw_data0:
                        #         make_figure_with_models(raw_data0, None, None, None,
                        #                          xkey, ykey, rkey, comp, ckey,
                        #                          cent_sat_split=False,
                        #                          cent_only=True, four_z=False,
                        #                          snap_str=run[
                        #                                   53 - 2 * (run[55] == 'z'):65 - 2 * (
                        #                                               run[55] == 'z')],
                        #                          sim_str=run[41:50], save=True,
                        #                          cbins=cbin)

                        pass

    # cbins = [[11.5, 12, 13, 15]] # [np.linspace(10, 14, 9)]  # [np.linspace(10, 14, 9)] #[np.linspace(8, 14, 13)]
    cbins = [np.linspace(10, 13, 4)] #[np.linspace(10, 14, 9)]
    # cbins = [np.linspace(10, 12.5, 11)] #[np.linspace(8, 10.5, 16)] 25

    for cbin in cbins:
        # for pair in combined_name_pairs:
            pair = combined_name_pairs[0]
            with h5.File(pair[0], 'r') as raw_data0:
                with h5.File(pair[1], 'r') as raw_data1:
                    make_profile_with_models(raw_data0, raw_data1, None, None,
                                             snap_str=pair[0][53:65], sim_str=pair[0][41:50],
                                             save=save,
                                             cbins=cbin, N_ress=[-1])


    plt.show()

    print(round(time.time() - start_time))
    print('Done.')

    pass
