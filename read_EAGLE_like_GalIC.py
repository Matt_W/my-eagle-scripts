#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#TODO change back to snap_010.hdf5
"""
Created on Thu Apr 22 14:20:42 2021

@author: matt
"""

import numpy as np

import h5py as h5

from scipy.integrate         import quad
from scipy.optimize          import brentq
from scipy.spatial.transform import Rotation
from scipy.stats             import binned_statistic

def load_stars(pwd, name):
  '''Read the test files and unitify the outputs
  '''
  #TODO change back to snap_010.hdf5
  file_name = pwd + name + '/snap_000.hdf5'
  #TODO change back to snap_010.hdf5

  #load galaxy
  with h5.File(file_name,"r") as fs:

    CompStars = [0, 0] #[number of disk, number of bluge]

    MassDMs = fs['PartType1/Masses'][()] #don't really need to load all
    PosDMs  = fs['PartType1/Coordinates'][()]
    VelDMs  = fs['PartType1/Velocities'][()]

    try:

      MassStars    = fs['PartType2/Masses'][()] #don't really need to load all
      PosStars     = fs['PartType2/Coordinates'][()]
      VelStars     = fs['PartType2/Velocities'][()]
      CompStars[0] = len(fs['PartType2/Masses'])

      try:

        MassStars    = np.concatenate((MassStars, fs['PartType3/Masses'][()]))
        PosStars     = np.concatenate((PosStars,  fs['PartType3/Coordinates'][()]))
        VelStars     = np.concatenate((VelStars,  fs['PartType3/Velocities'][()]))
        CompStars[1] = len(fs['PartType3/Masses'])

        if fs['PartType2/Masses'][0] != fs['PartType3/Masses'][0]:
          print("Warning, star components are different masses: {0} != {1}".format(
                fs['PartType2/Masses'].value[0], fs['PartType3/Masses'][0]))
          #more interested in disks
          # MassStar = fs['PartType3/Masses'][0]

      except KeyError:

        PosStars = np.array(PosStars)
        VelStars = np.array(VelStars)
        pass

    except KeyError:

      MassStars = fs['PartType3/Masses'][()]
      PosStars  = fs['PartType3/Coordinates'][()]
      VelStars  = fs['PartType3/Velocities'][()]
      CompStars[1] = len(fs['PartType3/Masses'])

      # PosStars = np.array(PosStars)
      # VelStars = np.array(VelStars)

  gas_pos  = np.zeros((0,3))
  gas_mass = np.zeros(0)

  dm_pos   = PosDMs
  dm_mass  = MassDMs

  pos  = PosStars
  vel  = VelStars
  mass = MassStars
  form_a = np.zeros(len(mass))

  # constant_200 = get_200_constant(H=HubbleExpansion, a=ExpansionFactor, z=Redshift,
  #                                 om=om, h=HubbleParam, H_0=100*HubbleParam)
  constant_200_z0 = get_200_constant(H=None, a=None, z=0,
                                     om=[0.307,0.693], h=0.6777, H_0=67.77
                                     ) #* ExpansionFactor**3
  # thirty_ckpc = HubbleParam * 30 / ExpansionFactor * 1e-3

  (R200, M200) = get_crit_200(dm_mass, dm_pos, gas_mass, gas_pos, mass, pos,
                              constant_200_z0)

  R200 = R200 #kpc
  M200 = M200 * 1e10 #Msun

  (pos, vel) = align(pos, vel, mass, apature=30)

  #cylindrical coords
  (R, phi, z, v_R, v_phi, v_z
   ) = get_cylindrical(pos, vel)

  r = np.linalg.norm(pos, axis=1)

  # # j_z = np.cross(R, v_phi)
  # j_z = pos[:,0] * vel[:,1] - pos[:,1] * vel[:,0]
  # j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

  # arg_bind = np.argsort(bind)
  # arg_arg  = np.argsort(arg_bind)

  # #TODO check
  # j_E = max_within_50(j_tot[arg_bind])[arg_arg]
  # j_zonc = j_z / j_E
  j_zonc = np.zeros(len(mass))

  # #shape parameters
  # (a,b,c) = find_abc(pos, mass)
  # axis_a[halo_i] = a
  # axis_b[halo_i] = b
  # axis_c[halo_i] = c

  # #calculate profiles
  # lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
  # log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)

  #aparures
  (R_half, R_quarter, R3quarter) = get_radii_that_are_interesting(R, mass)
  (r_half, r_quarter, r3quarter) = get_radii_that_are_interesting(r, mass)

  z_half = np.median(np.abs(z[r < 30]))
  # #calculate
  # apature_30     = get_kinematic_apature(30,       pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
  # apature_r200   = get_kinematic_apature(R200,     pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
  # apature_half   = get_kinematic_apature(r_half,   pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
  # apature2half   = get_kinematic_apature(2*r_half, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
  # apature_quater = get_kinematic_apature(r_quater, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
  # apature3quater = get_kinematic_apature(r3quater, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)

  # #dm profiles
  # dm_r = np.linalg.norm(dpos, axis=1)

  # dm_N[halo_i, 0:n_lin] = np.histogram(dm_r, bins=lin_bin_edges)[0]

  # dm_sigma_x[halo_i, 0:n_lin] = binned_statistic(dm_r, dvel[:,0], bins=lin_bin_edges, statistic='std')[0]
  # dm_sigma_y[halo_i, 0:n_lin] = binned_statistic(dm_r, dvel[:,1], bins=lin_bin_edges, statistic='std')[0]
  # dm_sigma_z[halo_i, 0:n_lin] = binned_statistic(dm_r, dvel[:,2], bins=lin_bin_edges, statistic='std')[0]

  # dm_N[halo_i, n_lin:(n_lin+n_log)] = np.histogram(dm_r, bins=log_bin_edges)[0]

  # dm_sigma_x[halo_i, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,0], bins=log_bin_edges, statistic='std')[0]
  # dm_sigma_y[halo_i, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,1], bins=log_bin_edges, statistic='std')[0]
  # dm_sigma_z[halo_i, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,2], bins=log_bin_edges, statistic='std')[0]

  # dm_N[halo_i, (n_lin+n_log):n_dim] = [np.sum(dm_r<30), np.sum(dm_r<R200), np.sum(dm_r<r_half),
  #                                         np.sum(dm_r<2*r_half), np.sum(dm_r<r_quater), np.sum(dm_r<r3quater)]

  # dm_sigma_x[halo_i, (n_lin+n_log):n_dim] = [np.std(dvel[dm_r<30,      0]), np.std(dvel[dm_r<R200,    0]),
  #                                               np.std(dvel[dm_r<r_half,  0]), np.std(dvel[dm_r<2*r_half,0]),
  #                                               np.std(dvel[dm_r<r_quater,0]), np.std(dvel[dm_r<r3quater,0])]
  # dm_sigma_y[halo_i, (n_lin+n_log):n_dim] = [np.std(dvel[dm_r<30,      1]), np.std(dvel[dm_r<R200,    1]),
  #                                               np.std(dvel[dm_r<r_half,  1]), np.std(dvel[dm_r<2*r_half,1]),
  #                                               np.std(dvel[dm_r<r_quater,1]), np.std(dvel[dm_r<r3quater,1])]
  # dm_sigma_z[halo_i, (n_lin+n_log):n_dim] = [np.std(dvel[dm_r<30,      2]), np.std(dvel[dm_r<R200,    2]),
  #                                               np.std(dvel[dm_r<r_half,  2]), np.std(dvel[dm_r<2*r_half,2]),
  #                                               np.std(dvel[dm_r<r_quater,2]), np.std(dvel[dm_r<r3quater,2])]

  return {
    'R200': R200,
    'M200': M200,
    'R200z0crit': R200,
    'M200z0crit': M200,
    'StellarHalfMassRadiusProj':     R_half,
    'Rquarter':  R_quarter,
    'R3quarter': R3quarter,
    'StellarHalfMassRadius':     r_half,
    'rquarter':  r_quarter,
    'r3quarter': r3quarter,
    'zHalf':     z_half
    }

def get_cylindrical(PosStars, VelStars):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(np.square(PosStars[:,0]) + np.square(PosStars[:,1]))
  varphi   = np.arctan2(PosStars[:,1], PosStars[:,0])
  z        = PosStars[:,2]

  v_rho    = VelStars[:,0] * np.cos(varphi) + VelStars[:,1] * np.sin(varphi)
  v_varphi = -VelStars[:,0]* np.sin(varphi) + VelStars[:,1] * np.cos(varphi)
  v_z      = VelStars[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  R = find_rotaton_matrix(galaxy_anular_momentum)
  pos = R.apply(pos)
  '''
  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

def align(pos, vel, mass, apature=30):
  '''Aligns the z cartesian direction with the direction of angular momentum.
  Can use an apature.
  '''
  #direction to align
  if apature != None:
    r = np.linalg.norm(pos, axis=1)
    mask = r < apature

    j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

  else:
    j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

  #find rotation
  rotation = find_rotaion_matrix(j_tot)

  #rotate stars
  pos = rotation.apply(pos)
  vel = rotation.apply(vel)

  return(pos, vel)

def get_radii_that_are_interesting(r, mass):
  '''Find stellar half mass, quater mass and 3 quater mass.
  '''
  arg_order = np.argsort(r)
  cum_mass = np.cumsum(mass[arg_order])
  #fraction
  cum_mass /= cum_mass[-1]

  half   = r[arg_order][np.where(cum_mass > 0.50)[0][0]]
  quater = r[arg_order][np.where(cum_mass > 0.25)[0][0]]
  three  = r[arg_order][np.where(cum_mass > 0.75)[0][0]]

  return(half, quater, three)

def max_within_50(array):
  '''returns the largest value of array within 50 indices
  probably can be faster
  '''
  list_len = np.size(array)

  max_array = np.zeros(list_len)

  for i in range(list_len):
    low = np.amax((0, i-50))
    hi  = np.amin((i+50, list_len))

    max_array[i] = np.amax(array[low:hi])

  return(max_array)

def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
  '''Calculates the reduced inertia tensor
  M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
  Itterative selection is done in the other function.
  '''
  norm = m_p / e2_p

  m = np.zeros((3,3))

  for i in range(3):
    for j in range(3):
      m[i,j] = np.sum(norm * r_p[:, i] * r_p[:, j])

  m /= np.sum(norm)

  return(m)

def process_tensor(m):
  '''
  '''
  #DO NOT use np.linalg.eigh(m)
  #looks like there is a bug

  (eigan_values, eigan_vectors) = np.linalg.eig(m)

  order = np.flip(np.argsort(eigan_values))

  eigan_values  = eigan_values[order]
  eigan_vectors = eigan_vectors[order]

  return(eigan_values, eigan_vectors)

def defined_particles(pos, mass, eigan_values, eigan_vectors):
  '''Assumes eigan values are sorted
  '''
  #projection along each axis
  projected_a = (pos[:,0] * eigan_vectors[0,0] + pos[:,1] * eigan_vectors[0,1] +
                 pos[:,2] * eigan_vectors[0,2])
  projected_b = (pos[:,0] * eigan_vectors[1,0] + pos[:,1] * eigan_vectors[1,1] +
                 pos[:,2] * eigan_vectors[1,2])
  projected_c = (pos[:,0] * eigan_vectors[2,0] + pos[:,1] * eigan_vectors[2,1] +
                 pos[:,2] * eigan_vectors[2,2])

  #ellipse distance #Thob et al. 2019 eqn 4.
  ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1]/eigan_values[0]) +
                      np.square(projected_c) / (eigan_values[2]/eigan_values[0]) )

  #ellipse radius #Thob et al. 2019 eqn 4.
  ellipse_radius = np.power(np.square(eigan_values[0]) / (eigan_values[1] * eigan_values[2]), 1/3
                            ) * np.square(30)

  #Thob et al. 2019 eqn 4.
  inside_mask = ellipse_distance <= ellipse_radius

  return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])

def find_abc(pos, mass):
  '''Finds the major, intermediate and minor axes.
  Follows Thob et al. 2019 using quadrupole moment of mass to bias towards
  particles closer to the centre
  '''
  #no clue why this isn't working
  #try except
  try:
    n_ini = len(mass)

    #start off speherical
    r2 = np.square(np.linalg.norm(pos, axis=1))

    #stop problems with r=0
    po  = pos[r2 != 0]
    mas = mass[r2 != 0]
    r2  = r2[r2 != 0]

    #mass tensor of particles
    m = reduced_quadrupole_moments_of_mass_tensor(po, mas, r2)

    #linear algebra stuff
    (eigan_values, eigan_vectors) = process_tensor(m)

    #to see convergeance
    cona = np.sqrt(eigan_values[2] / eigan_values[0])
    bona = np.sqrt(eigan_values[1] / eigan_values[0])

    # done = False
    for i in range(100):

      #redefine particles, calculate ellipse distance
      (po, mas, ellipse_r2) = defined_particles(po, mas, eigan_values, eigan_vectors)

      if len(mas) == 0:
        #if galaxy is small, error is expected so don't print error
        if n_ini > 50:
          print('Warning: No particles left when finding shape')

        return(np.zeros(3, dtype=np.float64))

      #mass tensor of new particles
      m = reduced_quadrupole_moments_of_mass_tensor(po, mas, ellipse_r2)


      if np.any(np.isnan(m)) or np.any(np.isinf(m)):
        if n_ini > 50:
          print('Warning: Found a nan / inf when finding shape')

        (eigan_values, eigan_vectors) = (np.zeros(3, dtype=np.float64),
                                         np.identity(3, dtype=np.float64))

      else:
        #linear algebra stuff
        (eigan_values, eigan_vectors) = process_tensor(m)

      if (1 - np.sqrt(eigan_values[2] / eigan_values[0]) / cona < 0.01) and (
          1 - np.sqrt(eigan_values[1] / eigan_values[0]) / bona < 0.01):
        #converged
        # done = True
        break

      else:
        cona = np.sqrt(eigan_values[2] / eigan_values[0])
        bona = np.sqrt(eigan_values[1] / eigan_values[0])

    #some warnings
    # if not done:
    #   print('Warning: Shape did not converge.')

    # if len(mas) < 100:
    #   print('Warning: Defining shape with <100 particles.')

  except ValueError:
    return(np.zeros(3, dtype=np.float64))

  return(np.sqrt(eigan_values))

###############################################################################

def get_200_constant(H=None, a=None, z=None, om=[0.307,0.693], h=0.6777, H_0=67.77):
  '''rho < 200 rho_c(z)
  M(<R200) / ((4/3)pi R200^3) < 200 (3/(8pi)) H^2 /G
  M(<R200) / R200^3 < 100 H^2 / G
  H in EAGLE units.
  H_0 in km/s/Mpc
  '''
  # G = 4.301e1 #Mpc (km/s)^2 / (10^10 Msun) (source wikipedia)
  G = 4.301e4 #kpc (km/s)^2 / (10^10 Msun) (source wikipedia)

  if H != None and a != None:
    H *= 3.085678E24 * 1e-5 #to km/s/Mpc

    constant = 100 * a**3 * np.square(H * 1e-3) / G #10^10 Msun / kcp^3

  if z != None:
    H_z = np.sqrt( np.square(H_0 * 1e-3) * (om[0] * (1+z)**3 + om[1]) )
    a   = 1/(1 + z)

    const_z = 100 * a**3 * np.square(H_z) / G #10^10 Msun * a^3 / (kcp^3)

  if H != None and a != None and z != None:
    if np.abs(constant - const_z) / constant > 0.001:
      print('Warning: Box cosmology inconsistant: H:', H, '\t H(z)', H_z)

  if H == None or a == None:
    constant = const_z

  #Will raise an error if neither H nor z is defined

  return(constant)

def get_crit_200(dm_mass, dm_pos, gas_mass, gas_pos, star_mass, star_pos,
                 constant_200):
  '''Assume pos in h*Mpc/a and mass_dm and mass in 10^10 Msun*h.
  Returns R200 in h*Mpc/a and M200 in 10^10Msun*h
  '''
  #Could also include bh, but should be negligable...
  dm_r   = np.linalg.norm(dm_pos,   axis=1)
  gas_r  = np.linalg.norm(gas_pos,  axis=1)
  star_r = np.linalg.norm(star_pos, axis=1)

  #TODO speed up by log(R200) or something
  #Should be faster than sorting points / using cumsum
  # M / 4/3 r_200^3 = 200 * 3/(8 pi) * H^2/G
  # M = 100 * r_200^3 * H^2 / G)
  zero = lambda R200 : (np.sum(dm_mass[dm_r < R200]) + np.sum(gas_mass[gas_r < R200]) +
    np.sum(star_mass[star_r < R200])) - constant_200 * R200 * R200 * R200

  #this could take a little while with large N
  try:
    # R200 = brentq(zero, (np.amin(dm_r)+1e-3), 100 * np.amax(dm_r))
    R200 = brentq(zero, (np.amin(dm_r)+1e-3), 100 * np.amax(dm_r))
  except ValueError:
    print('Warning: f(a), f(b) signs. Assume halo is less dense than rho_crit so R200=0')
    print(zero(np.amin(dm_r)+1e-3), zero(100 * np.amax(dm_r)))
    R200 = 0

  M200 = np.sum(dm_mass[dm_r < R200]) + np.sum(gas_mass[gas_r < R200]) + np.sum(star_mass[star_r < R200])

  return(R200, M200)

###############################################################################

def my_binned_statistic(x, value, bins, statistic='sum'):
  '''Calls binned statistic, but returns zeros when x and value are empty.
  '''
  if len(x)==0 and len(value)==0:
    out = np.zeros(len(bins)-1)
  else:
    out = binned_statistic(x, value, bins=bins, statistic=statistic)[0]

  return(out)

def my_already_binned_statistic(value, digitized, bin_edges, statistic='sum'):
  '''Calculates the statistic in bins if binned_statistic has already digitized
  the data set.
  Should be faster than calling binned stastic a bunch of times ......
  '''
  n = len(bin_edges) - 1
  out_array = np.zeros(n, dtype=np.float64)

  if statistic == 'sum':
    func = np.sum
  elif statistic == 'median':
    func = np.median
  else:
    raise ValueError('Statistic ' + str(statistic) + ' not understood.')

  #would be faster without a for loop
  for i in range(n):
    out_array[i] = func(value[digitized == i+1])

  return(out_array)

def get_kinematic_profiles(bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a):
  '''Given galaxy particle data and bins, calculates kenematic profiles.
  See Genel et al. 2015, Lagos et al. 2017, Correra et al. 2017, Wilkinson et al. 2021
  Probably not super efficent to call binned statistic a bunch of times, can use
  the third output to mask bins. Too lazy / will be more confusing to code though.
  '''
  #binned
  (bin_mass,_,bin_number) = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
  bin_N    = np.histogram(R, bins=bin_edges)[0]

  number_of_bins = len(bin_edges) - 1

  #j_zonc
  #I don't like this. Needs to be done though
  fjzjc10 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc09 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc08 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc07 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc06 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc05 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc04 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc03 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc02 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc01 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc00 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm1 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm2 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm3 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm4 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm5 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm6 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm7 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm8 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm9 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm0 = np.zeros(number_of_bins, dtype=np.float64)

  #calculate kappas
  K_tot = np.zeros(number_of_bins, dtype=np.float64)
  K_rot = np.zeros(number_of_bins, dtype=np.float64)
  K_co  = np.zeros(number_of_bins, dtype=np.float64)

  #sigmas
  mean_v_R     = np.zeros(number_of_bins, dtype=np.float64)
  mean_v_phi   = np.zeros(number_of_bins, dtype=np.float64)
  median_v_phi = np.zeros(number_of_bins, dtype=np.float64)

  sigma_z    = np.zeros(number_of_bins, dtype=np.float64)
  sigma_R    = np.zeros(number_of_bins, dtype=np.float64)
  sigma_phi  = np.zeros(number_of_bins, dtype=np.float64)

  z_half = np.zeros(number_of_bins, dtype=np.float64)

  form_a_median = np.zeros(number_of_bins, dtype=np.float64)

  #would be faster without a for loop
  for i in range(number_of_bins):
    mask = bin_number == i+1

    bin_massi = 1 / bin_mass[i]

    mmass = mass[mask]
    mj_zonc = j_zonc[mask]
    mv_phi = v_phi[mask]

    #j_zonc
    fjzjc10[i] = np.sum(mmass[mj_zonc >= 1.0]) * bin_massi
    fjzjc09[i] = np.sum(mmass[mj_zonc >= 0.9]) * bin_massi
    fjzjc08[i] = np.sum(mmass[mj_zonc >= 0.8]) * bin_massi
    fjzjc07[i] = np.sum(mmass[mj_zonc >= 0.7]) * bin_massi
    fjzjc06[i] = np.sum(mmass[mj_zonc >= 0.6]) * bin_massi
    fjzjc05[i] = np.sum(mmass[mj_zonc >= 0.5]) * bin_massi
    fjzjc04[i] = np.sum(mmass[mj_zonc >= 0.4]) * bin_massi
    fjzjc03[i] = np.sum(mmass[mj_zonc >= 0.3]) * bin_massi
    fjzjc02[i] = np.sum(mmass[mj_zonc >= 0.2]) * bin_massi
    fjzjc01[i] = np.sum(mmass[mj_zonc >= 0.1]) * bin_massi
    fjzjc00[i] = np.sum(mmass[mj_zonc >= 0.0]) * bin_massi
    fjzjcm1[i] = np.sum(mmass[mj_zonc >=-0.1]) * bin_massi
    fjzjcm2[i] = np.sum(mmass[mj_zonc >=-0.2]) * bin_massi
    fjzjcm3[i] = np.sum(mmass[mj_zonc >=-0.3]) * bin_massi
    fjzjcm4[i] = np.sum(mmass[mj_zonc >=-0.4]) * bin_massi
    fjzjcm5[i] = np.sum(mmass[mj_zonc >=-0.5]) * bin_massi
    fjzjcm6[i] = np.sum(mmass[mj_zonc >=-0.6]) * bin_massi
    fjzjcm7[i] = np.sum(mmass[mj_zonc >=-0.7]) * bin_massi
    fjzjcm8[i] = np.sum(mmass[mj_zonc >=-0.8]) * bin_massi
    fjzjcm9[i] = np.sum(mmass[mj_zonc >=-0.9]) * bin_massi
    fjzjcm0[i] = np.sum(mmass[mj_zonc >=-1.0]) * bin_massi

    #calculate kappas
    K_tot[i] = np.sum(mmass * np.square(np.linalg.norm(vel[mask], axis=1)))
    K_rot[i] = np.sum(mmass * np.square(mv_phi))

    co = (mv_phi > 0)
    K_co[i] = np.sum(mmass[co] * np.square(mv_phi[co]))

    #sigmas
    mean_v_R[i]     = np.sum(mmass * v_R[mask]) * bin_massi
    mean_v_phi[i]   = np.sum(mmass * mv_phi) * bin_massi
    median_v_phi[i] = np.median(mv_phi)

    sigma_z[i]    = np.sum(mmass * np.square(v_z[mask])) * bin_massi
    sigma_R[i]    = np.sum(mmass * np.square(v_R[mask])) * bin_massi
    sigma_phi[i]  = np.sum(mmass * np.square(mv_phi - mean_v_phi[i])) * bin_massi

    #TODO replace with mass weighted median
    z_half[i] = np.median(np.abs(z[mask]))

    #TODO replace with mass weighted median
    #TODO add initial mass weighted median
    #probably doesn't need to be a profile
    form_a_median[i] = np.median(form_a)

  kappa_rot = K_rot / K_tot
  kappa_co  = K_co  / K_tot

  return(bin_mass, bin_N,
         fjzjc10, fjzjc09, fjzjc08, fjzjc07, fjzjc06, fjzjc05, fjzjc04, fjzjc03, fjzjc02, fjzjc01, fjzjc00,
         fjzjcm1, fjzjcm2, fjzjcm3, fjzjcm4, fjzjcm5, fjzjcm6, fjzjcm7, fjzjcm8, fjzjcm9, fjzjcm0,
         kappa_rot, kappa_co,
         mean_v_R, mean_v_phi, median_v_phi,
         sigma_z, sigma_R, sigma_phi,
         z_half, form_a_median)

def get_kinematic_apature(apature, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a):
  '''Same as get_kinematic_profile, except apature rather than profile.
  '''
  #mask
  mask = (r < apature)

  pos  = pos[mask]
  vel  = vel[mask]
  mass = mass[mask]
  (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])
  j_zonc = j_zonc[mask]
  form_a = form_a[mask]

  bin_mass = np.sum(mass)
  bin_N    = np.size(mass)

  bin_massi = 1/bin_mass

  #jz/jc
  fjzjc10 = np.sum(mass[j_zonc >=1.0]) * bin_massi
  fjzjc09 = np.sum(mass[j_zonc > 0.9]) * bin_massi
  fjzjc08 = np.sum(mass[j_zonc > 0.8]) * bin_massi
  fjzjc07 = np.sum(mass[j_zonc > 0.7]) * bin_massi
  fjzjc06 = np.sum(mass[j_zonc > 0.6]) * bin_massi
  fjzjc05 = np.sum(mass[j_zonc > 0.5]) * bin_massi
  fjzjc04 = np.sum(mass[j_zonc > 0.4]) * bin_massi
  fjzjc03 = np.sum(mass[j_zonc > 0.3]) * bin_massi
  fjzjc02 = np.sum(mass[j_zonc > 0.2]) * bin_massi
  fjzjc01 = np.sum(mass[j_zonc > 0.1]) * bin_massi
  fjzjc00 = np.sum(mass[j_zonc > 0.0]) * bin_massi
  fjzjcm1 = np.sum(mass[j_zonc >-0.1]) * bin_massi
  fjzjcm2 = np.sum(mass[j_zonc >-0.2]) * bin_massi
  fjzjcm3 = np.sum(mass[j_zonc >-0.3]) * bin_massi
  fjzjcm4 = np.sum(mass[j_zonc >-0.4]) * bin_massi
  fjzjcm5 = np.sum(mass[j_zonc >-0.5]) * bin_massi
  fjzjcm6 = np.sum(mass[j_zonc >-0.6]) * bin_massi
  fjzjcm7 = np.sum(mass[j_zonc >-0.7]) * bin_massi
  fjzjcm8 = np.sum(mass[j_zonc >-0.8]) * bin_massi
  fjzjcm9 = np.sum(mass[j_zonc >-0.9]) * bin_massi
  fjzjcm0 = np.sum(mass[j_zonc >-1.0]) * bin_massi

  #calculate kappas
  K_tot = np.sum(mass * np.square(np.linalg.norm(vel,axis=1)))
  K_rot = np.sum(mass * np.square(v_phi))
  K_co  = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

  kappa_rot = K_rot / K_tot
  kappa_co  = K_co  / K_tot

  #sigmas
  mean_v_R   = np.sum(mass * v_R)   * bin_massi
  mean_v_phi = np.sum(mass * v_phi) * bin_massi
  median_v_phi = np.median(v_phi)

  sigma_z    = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
  sigma_R    = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
  sigma_phi  = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

  z_half = np.median(np.abs(z))

  form_a_median = np.median(form_a)

  return(bin_mass, bin_N,
         fjzjc10, fjzjc09, fjzjc08, fjzjc07, fjzjc06, fjzjc05, fjzjc04, fjzjc03, fjzjc02, fjzjc01, fjzjc00,
         fjzjcm1, fjzjcm2, fjzjcm3, fjzjcm4, fjzjcm5, fjzjcm6, fjzjcm7, fjzjcm8, fjzjcm9, fjzjcm0,
         kappa_rot, kappa_co,
         mean_v_R, mean_v_phi, median_v_phi,
         sigma_z, sigma_R, sigma_phi,
         z_half, form_a_median)