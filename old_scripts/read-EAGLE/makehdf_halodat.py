#script recieved from Aditiua Manuwal (UWA)

import ReadEagleSubfind_halo
import h5py                   as h5
import os

print('\nFor HYDRO ...')

#Where the data is
Base = '/home/matt/Documents/UWA/EAGLE/'
DList   = ['L0012N0188/EAGLE_REFERENCE/data']
#os.chdir('/home/matt/Documents/UWA/EAGLE/results')
ident = '_12Mpc_188'

#which snapshots to look at in simfiles
#simfiles in scripts folder
files=open('simfiles','r')
lines=files.readlines()
s=len(lines)

#do for all sizes for all times
for DirList in DList:
  for i in range(s):
    
    #pretty sure splitting like this isnt't necessary ...
    line=lines[i].split('\n')[0].split('_')
    Num=line[0]
    fend='_'+line[1]
    exts         = Num.zfill(3)
    fpart        = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'
    fsub         = Base+DirList+'/groups_'      +exts+fend+'/'+'eagle_subfind_tab_'      +exts+fend+'.0.hdf5'
    
    if (os.path.exists(fpart)==True & (os.path.exists(fsub)==True)):
        # Load halo catalogs into "HaloData" Structure
        HaloData     = ReadEagleSubfind_halo.ReadEagleSubfind_halo(Base, DirList, fend,exts)

#        PartMassDM   = PartData['PartMassDM']
        
        print('\nCreating the hdf5 file for the simulation box...')
        
        fn = Base+'processed_data/'+exts+fend+ident+'_halodat.hdf5'
      
        output  = h5.File(fn, "w")
        grp0    = output.create_group("Header")
        grp1    = output.create_group("HaloData")

#        dset    = grp0.create_dataset('h', data = PartData['HubbleParam'])
        dset    = grp0.create_dataset('Omega',    data = HaloData['Omega'])
        dset    = grp0.create_dataset('Redshift', data = HaloData['Redshift'])
        dset    = grp0.create_dataset('BoxSize',  data = HaloData['BoxSize'])
        dset    = grp0.create_dataset('Ngrps',    data = HaloData['TotNgroups'])
        dset    = grp0.create_dataset('Nsbgrps',  data = HaloData['TotNsubgroups'])
        dset    = grp0.create_dataset('Nsubs',    data = HaloData['NumOfSubhalos'])
        dset    = grp0.create_dataset('H(z)',     data = HaloData['H(z)'])

        dset    = grp1.create_dataset('GroupPos',        data = HaloData['GroupPos'])
        dset    = grp1.create_dataset('SubPos',          data = HaloData['SubPos'])
        dset    = grp1.create_dataset('FirstSub',        data = HaloData['FirstSub'])
        dset    = grp1.create_dataset('Group_M_Crit200', data = HaloData['Group_M_Crit200'])
        dset    = grp1.create_dataset('Group_R_Crit200', data = HaloData['Group_R_Crit200'])
        dset    = grp1.create_dataset('MainGrpNum',      data = HaloData['GroupNumber'])
        dset    = grp1.create_dataset('SubGrpNum',       data = HaloData['SubGroupNumber'])
        dset    = grp1.create_dataset('HaloSFR',         data = HaloData['SFRate'])
        dset    = grp1.create_dataset('Vbulk',           data = HaloData['Vbulk'])
        dset    = grp1.create_dataset('Vmax',            data = HaloData['Vmax'])
        dset    = grp1.create_dataset('Rmax',            data = HaloData['Rmax'])

        output.close()
        print('\nOutput:',fn)