#script recieved from Aditiua Manuwal (UWA)

import h5py  as h5
import os
import numpy as np

def ReadEagleParticles_Gas(Base,DirList,fend,exts):
    fn        = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'
    print(' __') ; print(' Particles:',Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend,' ...')
    fs            = h5.File(fn,"r")
    RuntimePars   = fs['RuntimePars'].attrs
    Header        = fs['Header'].attrs
    #Units         = fs['Units'].attrs
    #Constants     = fs['Constants'].attrs
    FNumPerSnap   = RuntimePars['NumFilesPerSnapshot']
    NumParts      = Header['NumPart_ThisFile']
    NumPartTot    = Header['NumPart_Total']

    fs.close()

    if NumPartTot[0]>0:
        PosGas         = np.empty((NumPartTot[0],3),dtype=float)
        VelGas         = np.empty((NumPartTot[0],3),dtype=float)
        DensGas        = np.empty(NumPartTot[0],    dtype=float)
        HSML_Gas       = np.empty(NumPartTot[0],    dtype=float)
        #fHAll          = np.empty(NumPartTot[0],    dtype=float)
        TempGas        = np.empty(NumPartTot[0],    dtype=float)
        SFR            = np.empty(NumPartTot[0],    dtype=float)
        MassGas        = np.empty(NumPartTot[0],    dtype=float)
        #fHall          = np.empty(NumPartTot[0],    dtype=float)
        fHSall         = np.empty(NumPartTot[0],    dtype=float)
        #GasZ           = np.empty(NumPartTot[0],    dtype=float)
        GasSZ          = np.empty(NumPartTot[0],    dtype=float)
        GrpNum_Gas     = np.empty(NumPartTot[0],    dtype=int)
        SubNum_Gas     = np.empty(NumPartTot[0],    dtype=int)

    NGas_c             = 0

    for ifile in range(0,FNumPerSnap):
        if os.path.isdir(Base+DirList+'data/'):
            fn                 = Base+DirList+'data/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.'+str(ifile)+'.hdf5'
        else:
            fn                 = Base+DirList+'/particledata_'    +exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.'+str(ifile)+'.hdf5'
        fs                     = h5.File(fn,"r")
        Header                 = fs['Header'].attrs
        NumParts               = Header['NumPart_ThisFile']

        if NumParts[0]>0:
          
            PosGas[NGas_c:NGas_c+NumParts[0],:]       = fs["PartType0/Coordinates"].value
            VelGas[NGas_c:NGas_c+NumParts[0],:]       = fs["PartType0/Velocity"].value
            DensGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Density"].value
            #GasZ[NGas_c:NGas_c+NumParts[0]]           = fs["PartType0/Metallicity"].value
            GasSZ[NGas_c:NGas_c+NumParts[0]]          = fs["PartType0/SmoothedMetallicity"].value
            HSML_Gas[NGas_c:NGas_c+NumParts[0]]       = fs["PartType0/SmoothingLength"].value
            SFR[NGas_c:NGas_c+NumParts[0]]            = fs["PartType0/StarFormationRate"].value
            TempGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Temperature"].value
            MassGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Mass"].value
            #fHall[NGas_c:NGas_c+NumParts[0]]          = fs["PartType0/ElementAbundance/Hydrogen"].value
            fHSall[NGas_c:NGas_c+NumParts[0]]         = fs["PartType0/SmoothedElementAbundance/Hydrogen"].value
            GrpNum_Gas[NGas_c:NGas_c+NumParts[0]]     = fs["PartType0/GroupNumber"].value
            SubNum_Gas[NGas_c:NGas_c+NumParts[0]]     = fs["PartType0/SubGroupNumber"].value
            

        NGas_c             += NumParts[0]

        fs.close()

    if(NumPartTot[0]>0): 
        return {'PosGas':PosGas, 'HSML_Gas':HSML_Gas, 
                'DensGas':DensGas, 'SFR':SFR,
                'MassGas':MassGas, 'fHSall':fHSall, #'fHSall':fHSall,
                'VelGas':VelGas,#'Star_aform':Star_aform,              
                'TempGas':TempGas,#'BindingEnergy':BindingEnergy,
                'GrpNum_Gas':GrpNum_Gas, 'SubNum_Gas':SubNum_Gas, 'GasSZ':GasSZ#, 'GasSZ':GasSZ
               }
