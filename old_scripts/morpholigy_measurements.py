#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 15:07:52 2020

@author: matt
"""
import os
import h5py  as h5

import numpy as np

import matplotlib.pyplot as plt
import splotch as splt

import astropy.units as u
import astropy.constants as cst

from scipy.optimize import brentq

###############################################################################
#kappa corotating
#finds the proportion of angular momentum corotating with the galaxy within 30kpc

def kappa_co_camila(subhalo,partsDATA,boxSize,h):
  '''Camila Correa's code somewhat modified for calculating corotaing kappa
  '''
  # subhalo contain subhalo data and is strutured as follow
  # [ (0:3)CentreOfPotential[Mpc]: (0)X  | (1)Y  | (2)Z  | 
  #   (3:6)Velocity[km/s]:         (3)Vx | (4)Vy | (5)Vz | 
  #   (6)R200c[Mpc] ]
  
  # partsDATA contains particles data and is structured as follow
  #should be stars only
  # [ (0:3)Position[Mpc]:  (0)X  | (1)Y  | (2)Z  | 
  #   (3:6)Velocity[km/s]: (4)Vx | (5)Vy | (6)Vz | 
  #   (6)Mass[e10Msun] ]
  
  # Center positions, unwrap space & normalize by R200c
  
  particlesDATA = np.array(partsDATA).copy()
  
  particlesDATA[:,:3]-=subhalo[:3].astype('float')-boxSize/(2*h)
  particlesDATA[:,:3]%=(boxSize/h)
  particlesDATA[:,:3]-=boxSize/(2*h)
  
  #normalising dimensionned coordinates by FOF virial radius, for dimensionless coordinates
  particlesDATA[:,:3]/=subhalo[6]
  
  # Center velocities
  #center velocities on the subhalo CoM velocity: the right center is done later after computing in-aperture CoM velocity
  particlesDATA[:,3:6]-=subhalo[3:6].astype('float')
  
  # Compute distances
  distancesDATA = np.linalg.norm(particlesDATA[:,:3],axis=1)
  
  # Restrict particles
  #dimensionless equivalent to 30kpc aperture
  aperture = 0.03/subhalo[6]
  
  extract = (distancesDATA<aperture)
  
  particles = particlesDATA[extract].copy()

  #not sure why this second one is needed
  distances = distancesDATA[extract].copy()
  
  Mstar = np.sum(particles[:,6])
  
  # Compute 30kpc CoM to Sub CoM velocty offset & recenter
  #compute deviation between the in-aperture 30kpc CoM velocity and the current velocity center (i.e [0,0,0] but representing the subhalo CoM velocity)
  dvVmass = np.sum(particles[:,6][:,np.newaxis]*particles[:,3:6],axis=0)/Mstar
  
  #re-center to correct velocity center (30kpc CoM velocity)
  particles[:,3:6]-=dvVmass
  
  # Compute momentum
  #compute specific momentums per particle (positions x velocities)
  smomentums = np.cross(particles[:,:3],particles[:,3:6])
  #compute in-aperture angular momentum (sum of [mass smomentums])
  momentum = np.sum(particles[:,6][:,np.newaxis]*smomentums,axis=0)
  
  # Compute rotational velocities
  #compute z-projection of specific momentums vectors per particle onto the angular momentum direction (smomentums.(momentum/|momentum|))
  smomentumz = np.sum(momentum*smomentums/np.linalg.norm(momentum),axis=1)
  
  #compute cylindrical radiuses to {CoP,momentum} axis (sqrt of [distances^2 - (positions.(momentum/|momentum|))^2])
  cyldistances = np.sqrt(distances**2-np.sum(momentum*particles[:,:3]/np.linalg.norm(momentum),axis=1)**2)
  
  #compute the cylindrical tangent component of each particles velocities via z-projection of specific momentums divided by cylindrical radiuses
  vrots = smomentumz/cyldistances
  
  # Compute kappa_co
  #compute the double (i.e. no half) of the subhalo kinetic energy invested in co-rotational motion
  Mvrot2 = np.sum((particles[:,6]*vrots**2)[vrots>0])
  
  #compute kappa_corotating by dividing Mvrot2 by the double (i.e. no half) of the subhalo total kinetic energy
  kappa_co = Mvrot2/np.sum(particles[:,6]*(np.linalg.norm(particles[:,3:6],axis=1))**2)
  
  return(kappa_co)

def call_kappa_co_camila(haloID):
  '''Load everything needed to make kappa_co work
  '''
  
  #settings
  Base  = '/home/matt/Documents/UWA/EAGLE/'
  Spec  = 'processed_data/028_z000p000'
  ident = '_12Mpc_188'
  
  #read processed hdf5 files
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
  
  PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
  MassStars = fs['PartData/MassStar'].value[tuple(mask)]
  VelStars  = fs['PartData/VelStar'].value[tuple(mask)]
  
  fs.close()
  
  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")
  
  #Group
  CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]
  R200c             = fs['HaloData/Group_R_Crit200'].value[haloID-1]
  M200c             = fs['HaloData/Group_M_Crit200'].value[haloID-1]
  #Subgroup
  FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
  BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
  
  BoxSize           = fs['Header/BoxSize'].value
  Redshift          = fs['Header/Redshift'].value
  a                 = 1 / (1 + Redshift)
  h = 0.6777
  
  fs.close()
  
  #units
  CentreOfPotential *= a / h
  BulkVelocity      *= 1
  R200c             *= a / h
  
  PosStars          *= a / h
  VelStars          *= np.sqrt(a)
  MassStars         *= 1 / h
  
  #make inputs
  subhalo = np.concatenate((CentreOfPotential, BulkVelocity, [R200c]))
  partsDATA = np.concatenate((PosStars, VelStars, np.transpose([MassStars])), axis=1)
  
  kappa = kappa_co_camila(subhalo, partsDATA, BoxSize, h)
  
  return(M200c, kappa)

def plot_kappa_co():

  kappas = []
  for i in range(200):
    kappas.append(call_kappa_co_camila(i))
    
  M200c, kappa = np.transpose(kappas)
  
  plt.figure()
  plt.errorbar(np.log10(M200c) + 10, kappa, ls='', fmt='.')
  plt.xlabel(r'$\log(M_{200})$')
  plt.ylabel(r'$\kappa_{co}$')
  #the most important part
#  plt.savefig('../results/kappa_co_12Mpc_188_z000p000')
  return

###############################################################################
###############################################################################
###############################################################################


def kappa(subhalo,partsDATA,boxSize,h):
  '''Camila Correa's code somewhat modified for calculating corotaing kappa
  '''
  # subhalo contain subhalo data and is strutured as follow
  # [ (0:3)CentreOfPotential[Mpc]: (0)X  | (1)Y  | (2)Z  | 
  #   (3:6)Velocity[km/s]:         (3)Vx | (4)Vy | (5)Vz | 
  #   (6)R200c[Mpc] ]
  
  # partsDATA contains particles data and is structured as follow
  #should be stars only
  # [ (0:3)Position[Mpc]:  (0)X  | (1)Y  | (2)Z  | 
  #   (3:6)Velocity[km/s]: (4)Vx | (5)Vy | (6)Vz | 
  #   (6)Mass[e10Msun] ]
  
  # Center positions, unwrap space & normalize by R200c
  
  particlesDATA = np.array(partsDATA).copy()
  
  particlesDATA[:,:3]-=subhalo[:3].astype('float')-boxSize/(2*h)
  particlesDATA[:,:3]%=(boxSize/h)
  particlesDATA[:,:3]-=boxSize/(2*h)
  
  #normalising dimensionned coordinates by FOF virial radius, for dimensionless coordinates
  particlesDATA[:,:3]/=subhalo[6]
  
  # Center velocities
  #center velocities on the subhalo CoM velocity: the right center is done later after computing in-aperture CoM velocity
  particlesDATA[:,3:6]-=subhalo[3:6].astype('float')
  
  # Compute distances
  distancesDATA = np.linalg.norm(particlesDATA[:,:3],axis=1)
  
  # Restrict particles
  #dimensionless equivalent to 30kpc aperture
  aperture = 0.03/subhalo[6]
  
  extract = (distancesDATA<aperture)
  
  particles = particlesDATA[extract].copy()

  #not sure why this second one is needed
  distances = distancesDATA[extract].copy()
  
  Mstar = np.sum(particles[:,6])
  
  # Compute 30kpc CoM to Sub CoM velocty offset & recenter
  #compute deviation between the in-aperture 30kpc CoM velocity and the current velocity center (i.e [0,0,0] but representing the subhalo CoM velocity)
  dvVmass = np.sum(particles[:,6][:,np.newaxis]*particles[:,3:6],axis=0)/Mstar
  
  #re-center to correct velocity center (30kpc CoM velocity)
  particles[:,3:6]-=dvVmass
  
  # Compute momentum
  #compute specific momentums per particle (positions x velocities)
  smomentums = np.cross(particles[:,:3],particles[:,3:6])
  #compute in-aperture angular momentum (sum of [mass smomentums])
  momentum = np.sum(particles[:,6][:,np.newaxis]*smomentums,axis=0)
  
  # Compute rotational velocities
  #compute z-projection of specific momentums vectors per particle onto the angular momentum direction (smomentums.(momentum/|momentum|))
  smomentumz = np.sum(momentum*smomentums/np.linalg.norm(momentum),axis=1)
  
  #compute cylindrical radiuses to {CoP,momentum} axis (sqrt of [distances^2 - (positions.(momentum/|momentum|))^2])
  cyldistances = np.sqrt(distances**2-np.sum(momentum*particles[:,:3]/np.linalg.norm(momentum),axis=1)**2)
  
  #compute the cylindrical tangent component of each particles velocities via z-projection of specific momentums divided by cylindrical radiuses
  vrots = smomentumz/cyldistances
  
  # Compute kappa_co
  #compute the double (i.e. no half) of the subhalo kinetic energy invested in co-rotational motion
  Mvrot2 = np.sum((particles[:,6]*vrots**2)[vrots>0])
  
  #compute kappa_corotating by dividing Mvrot2 by the double (i.e. no half) of the subhalo total kinetic energy
  kappa_co = Mvrot2/np.sum(particles[:,6]*(np.linalg.norm(particles[:,3:6],axis=1))**2)
  
  return(kappa_co)

def call_kappa(haloID):
  '''Load everything needed to make kappa_co work
  '''
  
  #settings
  Base  = '/home/matt/Documents/UWA/EAGLE/'
  Spec  = 'processed_data/028_z000p000'
  ident = '_12Mpc_188'
  
  #read processed hdf5 files
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
  
  PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
  MassStars = fs['PartData/MassStar'].value[tuple(mask)]
  VelStars  = fs['PartData/VelStar'].value[tuple(mask)]
  
  fs.close()
  
  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")
  
  #Group
  CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]
  R200c             = fs['HaloData/Group_R_Crit200'].value[haloID-1]
  M200c             = fs['HaloData/Group_M_Crit200'].value[haloID-1]
  #Subgroup
  FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
  BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
  
  BoxSize           = fs['Header/BoxSize'].value
  Redshift          = fs['Header/Redshift'].value
  a                 = 1 / (1 + Redshift)
  h = 0.6777
  
  fs.close()
  
  #units
  CentreOfPotential *= a / h
  BulkVelocity      *= 1
  R200c             *= a / h
  
  PosStars          *= a / h
  VelStars          *= np.sqrt(a)
  MassStars         *= 1 / h
  
  #make inputs
  subhalo = np.concatenate((CentreOfPotential, BulkVelocity, [R200c]))
  partsDATA = np.concatenate((PosStars, VelStars, np.transpose([MassStars])), axis=1)
  
  kappa = kappa_co_camila(subhalo, partsDATA, BoxSize, h)
  
  return(M200c, kappa)

def plot_kappa():

  kappas = []
  for i in range(200):
    kappas.append(call_kappa_co_camila(i))
    
  M200c, kappa = np.transpose(kappas)
  
  plt.figure()
  plt.errorbar(np.log10(M200c) + 10, kappa, ls='', fmt='.')
  plt.xlabel(r'$\log(M_{200})$')
  plt.ylabel(r'$\kappa_{co}$')
  #the most important part
#  plt.savefig('../results/kappa_co_12Mpc_188_z000p000')
  return


###############################################################################
###############################################################################
###############################################################################
#j_z/j_z
#Finds the proportion of star particle energy that is in angular momentum by 
#looking at some NFW moddeling
  
###############################################################################
#nfw stuff
#units are needed because I use astropy.constants.G for the gravitational const

def nfw_mass(rho_0, R_s, r):
  '''mass at a given radii given central potential and scale radius.
  R_s and r shoud be in the same units
  Result in units of rho_0 * R_s^3
  May be numerical errors for small r
  '''
  M = 4*np.pi*rho_0*R_s**3 * (np.log((R_s+r)/R_s) - r/(R_s+r))
  return(M)

def nfw_velocity_circ(rho_0, R_s, r):
  '''potential at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.Mpc
  if not isinstance(r, u.Quantity):
    r     *= u.Mpc
    
  mass = nfw_mass(rho_0, R_s, r)
  v_circ = np.sqrt(cst.G * mass / r)
  v_circ = v_circ.to(u.km/u.s)
  
  return(v_circ)

def nfw_potential(rho_0, R_s, r):
  '''potential at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  Might not work for r=0.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.Mpc
  if not isinstance(r, u.Quantity):
    r     *= u.Mpc
    
  phi = -4*np.pi*cst.G*rho_0*R_s**3/r * np.log(1+r/R_s)
  return(phi)
  
def nfw_params_from_vels(V_max, R_v_max):
  '''Calculates rho_0 and R_s given V_max and R_v_max.
  If not given astropy units, assumes km/s and Mpc.
  '''
  if not isinstance(V_max,   u.Quantity):
    V_max   *= u.km / u.s
  if not isinstance(R_v_max, u.Quantity):
    R_v_max *= u.Mpc
  
  R_s   = R_v_max / 2.1625815870646
  rho_0 = 1/cst.G * (V_max / (R_s * 1.6483500453640))**2
  rho_0 = rho_0.to(u.kg/u.m**3)
  
  return(rho_0, R_s)

def nfw_total_circular_specific_energy(rho_0, R_s, r):
  '''total specific energy (kinetic + potential)/mass for a particle in a 
  circular orbit at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  
  kinetic   = 1/2 * nfw_velocity_circ(rho_0, R_s, r)**2
  potential = nfw_potential(rho_0, R_s, r)
  
  total = kinetic + potential
  
  return(total)
  
def nfw_total_specific_energy(rho_0, R_s, r_vector, v_vector):
  '''total specific energy (kinetic + potential)/mass for a particle in an 
  arbitrary orbit at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(r_vector, u.Quantity):
    r_vector *= u.Mpc
  if not isinstance(v_vector, u.Quantity):
    v_vector *= u.km / u.s
    
  kinetic   = 1/2 * np.linalg.norm(v_vector, axis=1)**2
  potential = nfw_potential(rho_0, R_s, np.linalg.norm(r_vector, axis=1))
  
  total = kinetic + potential
  
  return(total)
  
def nfw_energy_helper(r, E, rho_0, R_s):
  '''Function designed to be used in scipy.optimize.brentq()
  '''
  r *= u.Mpc
  E_off_by = nfw_total_circular_specific_energy(rho_0, R_s, r) - E
  
  return(E_off_by.value)
  
def nfw_radius_circular_orbit_energy(rho_0, R_s, E):
  '''radius of a circular orbit for a given specific energy given central 
  potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  #messing with bounds may improve speed but also make bouds errors (ValueError)
  low = 1e-8
  hi  = 1e3
  #E is iterable
  #Pretty sure this can't be done with vector operations
  try:
    out = np.empty(len(E))
    
    for i, e in enumerate(E):
      value = brentq(nfw_energy_helper, low, hi, args=(e, rho_0, R_s))
      out[i] = value
    
    out *= u.Mpc
    
  #E is one number
  except TypeError:
    out = brentq(nfw_energy_helper, low, hi, args=(E, rho_0, R_s))
    out *= u.Mpc
  
  return(out)
  
def nfw_specific_angular_momentum(rho_0, R_s, r):
  '''specific angular momentum for a particle in a circular orbut at a given 
  radii given cental potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  Returns only the magnitude of the vector.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.Mpc
  if not isinstance(r, u.Quantity):
    r     *= u.Mpc
    
  v_circ = nfw_velocity_circ(rho_0, R_s, r)
  j_c    = v_circ * r
  
  return(j_c)

###############################################################################
#general transforms

def centre_on_galaxy_with_apature(r_vector, v_vector, masses, 
                                  CenterOfPotential, BulkVelocity, boxSize, h):
  '''Centers particles onto the centre of potential, cuts out a chosen apature
  adjusts the velocities to the galaxy frame.
  Probably doesn't need the bulk velocity step(s)
  centre_on_galaxy_with_apature(np.array(3,n), np.array(3,n), np.array(1,n), 
                                np.array(3), np.array(3), float, float) 
                                -> np.array(3,m), np.array(3,m), np.array(1,m)
  '''
  rs = np.array(r_vector.copy())
  vs = np.array(v_vector.copy())
  
  rs -=CenterOfPotential-boxSize/(2*h)
  rs %=(boxSize/h)
  rs -=boxSize/(2*h)
  
  # Center velocities
  #center velocities on the subhalo CoM velocity: the right center is done 
  #later after computing in-aperture CoM velocity
  vs -= BulkVelocity
  
  # Compute distances
  distances = np.linalg.norm(rs ,axis=1)
  
  # Restrict particles
  #dimensionfull equivalent to 30kpc aperture
  aperture = 0.03
  
  extract = (distances<aperture)
  
  r = rs[extract].copy()
  v = vs[extract].copy()
  m = masses[extract].copy()
  
  Mstar = np.sum(masses)
  
  # Compute 30kpc CoM to Sub CoM velocty offset & recenter
  #compute deviation between the in-aperture 30kpc CoM velocity and the current 
  #velocity center (i.e [0,0,0] but representing the subhalo CoM velocity)
  dvVmass = np.sum(np.dot(m,v),axis=0)/Mstar
  
  #re-center to correct velocity center (30kpc CoM velocity)
  v -= dvVmass
  
  return(r, v, m)

def total_angular_momentum(r_vector, v_vector, masses):
  '''total angular momentum of given particles. Units shouldn't matter?
  Useful for parties.
  '''
  angular_momentum = np.sum(np.cross(r_vector, v_vector) * masses, axis=0)
  
  return(angular_momentum)
  
def z_projection_specific_angular_momentum(r_vector, v_vector, j_vector):
  '''magnitude of a particle's the angular momentum projected onto the z 
  direction of total angular momentum.
  '''
  if not isinstance(r_vector, u.Quantity):
    r_vector *= u.Mpc
  if not isinstance(v_vector, u.Quantity):
    v_vector *= u.km / u.s
    
  #individual specific angular momentums
  js = np.cross(r_vector, v_vector)
  
  #projection of specific angular momentum in the j_total direction
  j_z = np.dot(js, j_vector) / np.linalg.norm(j_vector)
  
  return(j_z)

###############################################################################
#load and manage data

def call_j_circ(haloID):
  '''Load everything needed to make j_circ work
  '''
  #if already created file
  if os.path.isfile('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc.npy'):
    j_zonc = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc.npy')
    
  else:
  #if file needs creating
    #settings
    Base  = '/home/matt/Documents/UWA/EAGLE/'
    Spec  = 'processed_data/028_z000p000'
    ident = '_12Mpc_188'
    
    #read processed hdf5 files
    fn = Base+Spec+ident+'_Star.hdf5'
    fs = h5.File(fn,"r")
  
    mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
    
    PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
    MassStars = fs['PartData/MassStar'].value[tuple(mask)]
    VelStars  = fs['PartData/VelStar'].value[tuple(mask)]
    
    fs.close()
    
    fn = Base+Spec+ident+'_halodat.hdf5'
    fs = h5.File(fn,"r")
    
    #Group
    CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]
    
    #Subgroup
    FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
    BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
    Vmax              = fs['HaloData/Vmax'].value[FirstSub]
    Rmax              = fs['HaloData/Rmax'].value[FirstSub]
    
    BoxSize           = fs['Header/BoxSize'].value
    Redshift          = fs['Header/Redshift'].value
    a                 = 1 / (1 + Redshift)
    h = 0.6777
    
    fs.close()
    
    #simulation units to Mpc, km/s or 10^10M_sun
    CentreOfPotential *= a / h
    BulkVelocity      *= 1
    Vmax              *= 1
    Rmax              *= a / h
    
    PosStars          *= a / h
    VelStars          *= np.sqrt(a)
    MassStars         *= 1 / h
    
    #do the tricky stuff
    #Center, apature, velocities
    PosStars, VelStars, MassStars = centre_on_galaxy_with_apature(PosStars, VelStars, MassStars, 
                                                    CentreOfPotential, BulkVelocity, BoxSize, h)
    
    #get galaxy angular momentum
    galaxy_angular_momentum = total_angular_momentum(PosStars, VelStars, np.transpose([MassStars]))
    
    #galaxy NFW properties
    rho_0, R_s = nfw_params_from_vels(Vmax, Rmax)
    
    #projected specific angular momentum of each particle
    j_zs = z_projection_specific_angular_momentum(PosStars, VelStars, galaxy_angular_momentum)
    
    #specific energies of each particle
    energies = nfw_total_specific_energy(rho_0, R_s, PosStars, VelStars)
    
    #circular radii from specific energy
    circular_radii = nfw_radius_circular_orbit_energy(rho_0, R_s, energies)
    
    #circular (maximum) specific angular momentum for particles with the same energy
    j_cs = nfw_specific_angular_momentum(rho_0, R_s, circular_radii)
    
    #j_z / j_c
    j_zonc = j_zs / j_cs
    j_zonc = j_zonc.value
    
    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc', j_zonc)
  
  return(j_zonc)

def plot_j_zonc(haloID):
  '''
  '''
  j_zonc = call_j_circ(haloID)
  
  nstars = len(j_zonc)
  nbins  = 50 #int(np.round(nstars/40)) + 1
  
  #Robin and Matias' package
#  splt.hist(j_zonc, hist_type='smooth', bins=int(np.round(len(j_zonc)/40)))
  
  plt.figure()
  plt.hist(j_zonc, bins=nbins)
  
  plt.legend(['Group number={0}\nnStarParticlesWithin30kpc={1}'.format(haloID, nstars)])
  plt.xlabel(r'$j_z / j_c$')
  plt.ylabel(r'Star particle count')
  plt.show()
  
  plt.savefig('../results/galaxy'+str(haloID)+'j_zonc.png')
  
  return

if __name__ == '__main__':
  
  print("This script is depreciated.")
  
  
  galIDs = np.concatenate((np.arange(1,17), np.arange(18,24,2)))
  
  pass