#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 14 09:09:22 2020

@author: matt
"""
import os

import numpy as np

import matplotlib.pyplot as plt
import splotch as splt

import sklearn.mixture
import save_gmm_data


print("Now (17/3/20) depreciated. See my_gmm.py in test-galaxies")


def _random_data(points=10000, seed=1):
  '''
  '''
  np.random.seed(seed)
  
  #weights of the gaussians
  alpha = [0.2, 0.3, 0.2, 0.3]
  
  #gaussian means
  mu_0 = np.array([1, 1])
  mu_1 = np.array([1, 1])
  mu_2 = np.array([0, 0])
  mu_3 = np.array([0, 1])
  
  mu = np.concatenate((mu_0[np.newaxis,:], mu_1[np.newaxis,:], 
                       mu_2[np.newaxis,:], mu_3[np.newaxis,:]), axis=0)
#  print(mu)
  
  #gaussian standard deviation
  sigma_0 = np.array([[0.2, 0.1], [0.1, 0.2]])
  sigma_1 = np.array([[1, -0.3], [-0.3, 1]])
  sigma_2 = np.array([[0.3, 0.2], [0.2, 0.4]])
  sigma_3 = np.array([[0.7, -0.2], [-0.2, 0.2]])
  
  sigma = np.concatenate((sigma_0[np.newaxis,:], sigma_1[np.newaxis,:], 
                          sigma_2[np.newaxis,:], sigma_3[np.newaxis,:]), axis=0)
#  print(sigma)
  
  data = []
  
  for i in range(len(alpha)):
    
    generated = np.random.multivariate_normal(mu[i], sigma[i], int(alpha[i]*points))
    
    if i == 0:
      data = generated
    else:
      data = np.concatenate((data, generated))
  return(data)


def gmm_BIC(gmm, data, verbose=False):
  '''Calculate and return Bayesian Information Criterion (BIC) for GMM with 
  full covariance. Same results as as gmm.bic() but has not been implimented
  into the BayesianGaussianMixture object.
  '''
  #the number of free parameters in the model
  n_comp          = len(gmm.weights_)
  n_points, n_dim = np.shape(data)
  #triangle number for free Gaussian
  cov_params = n_comp * n_dim * (n_dim + 1) / 2.
  mean_params = n_dim * n_comp
  #n_comp for the weights
  free_param_dim = int(cov_params + mean_params + n_comp - 1)
  
  if verbose:
    print('log likelihood:', -2 * n_points * gmm.score(data), 
          '\tparameter penalty:', free_param_dim * np.log(n_points))
  
  bic = -2 * n_points * gmm.score(data) + free_param_dim * np.log(n_points)
  
  if verbose:
    print('BIC =', bic)
  return(bic)

def _select_above_cutoff(array, cutoff):
  '''Find and return the index of the first element of the array that is larger
  than the cutoff. Returns -1 if could not be found.
  '''
  found = False
  n_selected = -1
  for i, a in enumerate(array):
    if not found:
      if a < cutoff:
        n_selected = i
        found = True
  
  if n_selected == -1:
    print('Warning, something went wrong in selecton.')
  
  return(n_selected)

def gmm_my_autoGMM(data, name='galaxy', cutoff=0.3, n_init=10, baysian=True, plot=False, save=False):
  '''My implimentation of Du etal 2019's algorithm using 
  \Delta\hat{BIC} = BIC/n_data - average_for_large_n_comp(BIC/n_data)
  which asymptotes to 0 for for large n_comp. The smallest number of components
  which results in dBIC > cutoff is the chosen model.
  Might try to do some fancy things to improve fit, solution finding and/or 
  speed.
  '''
  bics = np.zeros(16)
  
  bic_min = 0
  for n_comp in np.arange(11,16):
    
    if not (os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_bic.npy'.format(n_comp)) and
        os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_weights.npy'.format(n_comp)) and
        os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_means.npy'.format(n_comp))   and
        os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_covariances.npy'.format(n_comp))):
      
      print('Calculating n_comp =', n_comp)
      save_gmm_data.run_and_save_gmm(data, n_comp, name, n_init, baysian=False)
    
    bics[n_comp] = np.load('../processed_data/'+name+'_gmm_n{:02d}_bic.npy'.format(n_comp))
    bic_min += bics[n_comp]/5
    
  print('Done large asymptotic BIC')
  
  #find the best number above a threshold (1%)
  for n_comp in np.arange(2,11):
    
    if not (os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_bic.npy'.format(n_comp)) and
        os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_weights.npy'.format(n_comp)) and
        os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_means.npy'.format(n_comp))   and
        os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_covariances.npy'.format(n_comp))):
    
      print('Calculating n_comp =', n_comp)
      save_gmm_data.run_and_save_gmm(data, n_comp, name, n_init, baysian=True)
      
    bics[n_comp] = np.load('../processed_data/'+name+'_gmm_n{:02d}_bic.npy'.format(n_comp))
    
  print('Found all BICs')
  
  #normalize
  bics = (bics - bic_min) / np.shape(data)[0]
  #find first n_comps < cutoff
  n_selected = _select_above_cutoff(bics[2:11], cutoff) + 2
  
  print('n_comp decided =', n_selected)
    
  if plot:
    plt.figure()
    plt.plot(np.arange(2,16), bics[2:16])
    plt.hlines(cutoff, 2, 15, ls='--')
    plt.plot(n_selected, bics[n_selected], marker='*')
    
    plt.xlabel('Number of components')
    plt.ylabel(r'$\Delta\hat{BIC}$')
    
    if save:
      plt.savefig('../results/'+name+'_bics.png')
    
  return(n_selected, np.arange(2,16), bics[2:16])
  
def gmm_call_gmm(data, n_comp, name='galaxy', n_init=10, baysian=True, save=True, max_iter=600):
  '''
  '''
  if not (os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_bic.npy'.format(n_comp)) and
      os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_weights.npy'.format(n_comp)) and
      os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_means.npy'.format(n_comp))   and
      os.path.isfile('../processed_data/'+name+'_gmm_n{:02d}_covariances.npy'.format(n_comp))):
    
    if save:
      print('Calculating n_comp =', n_comp)
      save_gmm_data.run_and_save_gmm(data, n_comp, name, n_init, baysian)
    
    else:
      print('Warning: result not saved.')
      if baysian:
        gmm = sklearn.mixture.BayesianGaussianMixture(n_components=n_comp, 
                                                      n_init=n_init, max_iter=max_iter).fit(data)
      else:
        print('Warning: not baysian')
        gmm = sklearn.mixture.GaussianMixture(n_components=n_comp, 
                                              n_init=n_init, max_iter=max_iter).fit(data)
        
      if not gmm.converged_:
        print('Warning: gmm not converged.')
        
      #done straight up gmm
      return(gmm)
  
  print('Loading from file')
  gmm = gmm_result(name, n_comp)
  
  print('Done')
  return(gmm)
  
class gmm_result(object):
  def __init__(self, name, n_comp):
    self._name = name
    self.n_comp = n_comp
    self.weights_     = np.load('../processed_data/'+name+'_gmm_n{:02d}_weights.npy'.format(n_comp))
    self.means_       = np.load('../processed_data/'+name+'_gmm_n{:02d}_means.npy'.format(n_comp))
    self.covariances_ = np.load('../processed_data/'+name+'_gmm_n{:02d}_covariances.npy'.format(n_comp))
  
  
def old_gmm_my_autoGMM(data, galID=0, cutoff=0.3, n_init=2, baysian=True, plot=False, save=False):
  '''My implimentation of Du etal 2019's algorithm using 
  \Delta\hat{BIC} = BIC/n_data - average_for_large_n_comp(BIC/n_data)
  which asymptotes to 0 for for large n_comp. The smallest number of components
  which results in dBIC > cutoff is the chosen model.
  Might try to do some fancy things to improve fit, solution finding and/or 
  speed.
  '''
  
  print('this function is depreciated')
  
  comps_start  = 1
  comps_end    = 10
  comps_length = comps_end - comps_start + 1
  n_comps = np.arange(comps_start,comps_end + 6)
  bics = np.empty(comps_length + 5)
#  n_gaussians_above_threshold = np.ones(np.shape(n_comps))
  
  #if already created file
  if os.path.isfile('../processed_data/galaxy' + str(galID) + 'bic_ninit' + str(n_init) + 
                    's' + str(comps_start) + 's' + str(comps_end) + '.npy'):
    
    bics = np.load('../processed_data/galaxy' + str(galID) + 'bic_ninit' + str(n_init) + 
                   's' + str(comps_start) + 's' + str(comps_end) + '.npy')
    
    n_selected = _select_above_cutoff(bics[:comps_length], cutoff) + comps_start
  
  else:
    #calculate bic_min  
    bic_min = 0
    for i, n_comp in enumerate(np.arange(comps_end+1,comps_end+6)):
      converged = False
      max_iter = 400
      while not converged:
        gmm = sklearn.mixture.GaussianMixture(n_components=n_comp, 
                                              n_init=n_init, max_iter=max_iter).fit(data)
        if gmm.converged_:
          converged=True
        else:
          max_iter += 100
        
      bic = gmm_BIC(gmm, data)
      
      bics[i + comps_length] = bic
      bic_min += bic/5
    print('Done large asymptotic BIC')
    
    #find the best number above a threshold (1%)
    for i, n_comp in enumerate(n_comps[:comps_length]):
      
      converged = False
      max_iter = 400
      
      while not converged:
        if baysian:
          #the most important step is here
          #needs to be Baysian
          gmm = sklearn.mixture.BayesianGaussianMixture(n_components=n_comp, 
                                                        n_init=n_init, max_iter=max_iter).fit(data)
        else:
          gmm = sklearn.mixture.GaussianMixture(n_components=n_comp, 
                                                n_init=n_init, max_iter=max_iter).fit(data)
        if gmm.converged_:
          converged=True
        else:
          max_iter += 100
          
      print('Done', n_comp)
      
      bics[i] = gmm_BIC(gmm, data)
    print('Found all BICs')
    
  #  min_bic     = np.amin(bics[:comps_end])
  #  arg_min_bic = np.argmin(bics[:comps_end]) + comps_start
    
    #normalize
    bics = (bics - bic_min) / np.shape(data)[0]
    #find first n_comps < cutoff
    n_selected = _select_above_cutoff(bics[:comps_length], cutoff) + comps_start
    
    print('n_comp decided =', n_selected)
    
    if save:
      np.save('../processed_data/galaxy' + str(galID) + 'bic_ninit' + str(n_init) + 
              's' + str(comps_start) + 's' + str(comps_end), bics)
    
  if plot:
    plt.figure()
    plt.plot(n_comps, bics)
    plt.hlines(cutoff, comps_start, comps_end+5, ls='--')
    plt.plot(n_selected, bics[n_selected - comps_start], marker='*')
    
    plt.xlabel('Number of components')
    plt.ylabel(r'$\Delta\hat{BIC}$')
    
    if save:
      plt.savefig('../results/galaxy'+str(galID) + 'bic_ncomp_ninit' + str(n_init) + 
                  's' + str(comps_start) + 's' + str(comps_end) + '.png')
    
  return(n_selected, n_comps, bics)

def gmm_random_points(gmm, data, seed=314159):
  '''The method in GMM only asigns points to their most likely halo. This 
  function uses the proportions calculated by the GMM to assign a point to a 
  component with the correct probabilites. Returns an ordered array with the 
  component number each data point was randomly assigned to and the chances
  of each data point being in each halo.
  gmm_random_points(sklearn.mixture._gaussian_mixture.GaussianMixture or
                    sklearn.mixture._gaussian_mixture.BayesianGaussianMixture, 
                    np.array(n_points))
                    -> np.array(n_points), np.array(n_comp, n_points)
  '''
  np.random.seed(seed)
  
  n_comp   = len(gmm.weights_)
  n_points = np.shape(data)[0]
  
  weighted = np.empty(n_comp, dtype=list)
  
  #calculate the pdf and weighted pdf at each data point for each component
  for i in range(n_comp):
  
    #don't do the same calcs 1000+ times
    sigma_inv = np.linalg.inv(gmm.covariances_[i])
    const     = (2*np.pi)**(-2/2) * np.linalg.det(gmm.covariances_[i])**(-1/2)
    
    #fast holder
    middle = np.zeros(np.amax(np.shape(data)))
    mean   = gmm.means_[i]
    
    #I couldn't figure out how to vectorize the matrix multiplication
    for n, dat in enumerate(data):
      mean_diff = dat-mean
      
      #Full covariance calculation
      first_mul = np.matmul(np.transpose(mean_diff), sigma_inv)
      final_mul = np.matmul(first_mul, mean_diff)
      middle[n] = final_mul
    
    pdf = const * np.exp(-1/2 * middle)
    
    #important that the weights are correct. Not sure if there is anything I 
    #can do to improve the accuracy.
    weighted[i] = pdf * gmm.weights_[i]
  
  chances = np.empty(n_comp, dtype=list)
  
  #draw selection
  #does no calculations
  for i in range(n_comp):
    
    chances[i] = weighted[i] / np.sum(weighted, axis=0)
  
  #the random variable
  draw = np.random.rand(n_points)
  
  #zero gets counteded
  choice = -np.ones(n_points)
  
  #add 1 for eac time that a random number is > the sum of the pdfs before
  for i in range(n_comp):
    
    #add probabilities
    thing = np.zeros(n_points)
    for j in range(i):
      thing += weighted[j]
    
    #normalize
    thing /= np.sum(weighted, axis=0)
    
    #add u[]
    choice += draw > thing
  
  return(choice, chances)

def plot_gmm_selection(gmm, data, verbose=True):
  '''Makes plots of the likelihood of each point being in each component.
  Also simulates randomly picking points from each component and shows the
  resulting Gaussian.
  '''
  if verbose:
    print('Weights:')
    print(gmm.weights_)
    print('Means')
    print(gmm.means_)
    print('Covariances')
    print(gmm.covariances_)
  
  n_comp = len(gmm.weights_) 
  
  fig, ax = plt.subplots(nrows=2, ncols=n_comp, sharex=True, sharey=True, figsize=(n_comp*3, 6))
  
  (choice, chances) = gmm_random_points(gmm, data, seed=314159)
  
  #draw selection
  for i in range(n_comp):
    
    splt.scatter(data[:,0], data[:,1], c=chances[i], clim=[0,1], s=1,
                 ax=ax[0,i])
    
    splt.scatter(data[choice==i,0], data[choice==i,1], s=1,
                 ax=ax[1,i])
  
  return

if __name__ == '__main__':
  
  data = _random_data()
  gmm = gmm_my_autoGMM(data, plot=True)
  
#  n_comp = 2
#  gmm = sklearn.mixture.GaussianMixture(n_components=n_comp, 
#                                                  n_init=4, max_iter=500).fit(data) 
#  
#  plot_gmm_selection(gmm, data)
  
    
  pass
