#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 16:42:39 2020

@author: matt
"""

import h5py  as h5
import numpy as np

#
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D
import matplotlib._color_data as mcd

def velocity_units(velocities, scale_factor=1.0):
  '''EAGLE (GADGET) has velocities in units of km/s a^(1/2)
  Convert velocities in simulation units to proper units
  
  a = 1/(1+z)
  
  velocity_units(np.array(n,3), float) -> np.array(n,3)
  '''
  
  return(scale_factor**(1/2) * velocities)

def velocity_Hubble_flow(velocities, CentreOfPotential, a_dot):
  '''Subtract the hubble flow of the galaxy from velocities. 
  Does not look at individual particle's hubble flows.
  
  This does not seem to be correct. 
  Use velocity_bulk_flow only for rest frame velocities.
  a_dot = H * a #in km/s
  
  velocity_Hubble_flow(np.array(n,3), np.array(3)) -> np.array(n,3)
  '''
  #Hubble flow velocity
  HubbleFlowSpeed    = a_dot * np.linalg.norm(CentreOfPotential)
  HubbleFlowVelocity = HubbleFlowSpeed * CentreOfPotential / np.linalg.norm(CentreOfPotential)
  
  print('Hubble flow velocity correction = ', HubbleFlowVelocity, 'km/s')
  
  return(velocities-HubbleFlowVelocity)


def velocitiy_bulk_flow(velocities):
  '''Subtract bulk movement from local velocities.
  
  velocity_bulk_flow(np.array(n,3)) -> np.array(n,3)
  '''
  
  return(velocities)
  
def get_stars_in_frame(Base,Spec,ident, haloID):
  '''Returns positions, masses and velocitis in proper units for all star
  particles in the center of momentum frame within 30kpc of a chosen galaxy.
  
  get_stars_in_frame(str,str,str, int) -> np.array(7,n)
  '''
  
  #read processed hdf5 files
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
  
  PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
  MassStars = fs['PartData/MassStar'].value[tuple(mask)]
  VelStars  = fs['PartData/VelStar'].value[tuple(mask)]
  
  fs.close()
  
  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")
  
  CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]
  Redshift          = fs['Header/Redshift'].value
  Hubble            = fs['Header/H(z)'].value
  BoxSize           = fs['Header/BoxSize'].value
  
  fs.close()
  
  #unit conversions
  Hubble     *= 3.08567758128e19            #from 1/s to km/s/Mpc
  ScaleFactor = 1 / (1 + Redshift)          #unitless
  
  BoxSize           *= 1/0.6777             #Mpc
  CentreOfPotential *= ScaleFactor / 0.6777 #Mpc
  PosStars          *= ScaleFactor / 0.6777 #Mpc
  MassStars         *= 1/0.6777             #10^10M_\odot
  VelStars          *= np.sqrt(ScaleFactor) #km/s
  
  #Seems wrong and superseeded by average momentum correction
#  #####################
#  a_dot       = Hubble * ScaleFactor        #km/s
#  #Hubble flow velocity
#  HubbleFlowSpeed    = a_dot * np.linalg.norm(CentreOfPotential)
#  HubbleFlowVelocity = HubbleFlowSpeed * CentreOfPotential / np.linalg.norm(CentreOfPotential)
#  
#  print('Hubble flow velocity correction = ', HubbleFlowVelocity, 'km/s')
#  
#  #Velocities without hubble flow
#  VelStars -= HubbleFlowVelocity
#  #####################
  
  #Centre Stars and wrap box
  PosStars += -CentreOfPotential + BoxSize/2
  PosStars %= BoxSize
  PosStars -= BoxSize/2
  
  StarDistances = np.linalg.norm(PosStars,axis=1)
  
  #Restric to 30kpc
  DistanceMask = (StarDistances < 0.03)
  
  PosStars  = PosStars[DistanceMask]
  MassStars = MassStars[DistanceMask]
  VelStars  = VelStars[DistanceMask]
  
  #Average momentum
  AverageStarMomentum = np.sum(np.multiply(MassStars, VelStars.transpose()), axis=1) / np.sum(MassStars)
  
  print('Total momentum of stars = ', AverageStarMomentum, ' 10^10M_sum km/s')
  
  VelStars -= AverageStarMomentum
  
  StarData = np.concatenate((PosStars.transpose(), [MassStars], VelStars.transpose()))
   
  return(StarData)

  
def plot_Star_xyz(haloID):
  '''
  
  plot_Star_xyz(int) -> graph
  '''  
  
  Base  = '/home/matt/Documents/UWA/EAGLE/'
  Spec  = 'processed_data/028_z000p000'
  ident = '_12Mpc_188'
  
  Stars = get_stars_in_frame(Base,Spec,ident, haloID) 
  Stars[:3] *= 1000
  
  #plotting here  
  fig = plt.figure()
  ax1 = fig.add_subplot(111, projection='3d')
  
  ax1.scatter(Stars[0], Stars[1], Stars[2])  
  ax1.quiver(Stars[0], Stars[1], Stars[2], 
             Stars[4], Stars[5], Stars[6], length=3e-2)
  
  ax1.set_xlabel('X [kpc]')
  ax1.set_ylabel('Y [kpc]')
  ax1.set_zlabel('Z [kpc]')
  
  #NonImplementedError
#  ax1.set_aspect('equal')

  max_sep = np.amax(np.amax(Stars[:3], axis=1) - np.amin(Stars[:3], axis=1)) / 2
  centers = np.mean(Stars[:3], axis=1)
  
  axes = plt.gca()
  axes.set_xlim([centers[0]-max_sep,centers[0]+max_sep])
  axes.set_ylim([centers[1]-max_sep,centers[1]+max_sep])
  axes.set_zlim([centers[2]-max_sep,centers[2]+max_sep])
  
if __name__ == '__main__':
  plot_Star_xyz(11)