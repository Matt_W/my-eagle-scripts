#script recieved from Aditiua Manuwal (UWA)

import ReadEagleParticles_DM
import h5py                  as h5
import os

print('\nFor HYDRO ...')

# #Where the data is
# Base = '/home/matt/Documents/UWA/EAGLE/'
# DList   = ['L0012N0188/EAGLE_REFERENCE/data']
# #os.chdir('/home/matt/Documents/UWA/EAGLE/results')
# ident = '_12Mpc_188'

# #which snapshots to look at in simfiles
# #simfiles in scripts folder
# files=open('simfiles','r')
# lines=files.readlines()
# s=len(lines)


#Where the data is
Base = '/fred/oz009/mwilkinson/EAGLE/'
DList   = ['REFERENCE']
#os.chdir('/home/matt/Documents/UWA/EAGLE/results')
ident = '_100Mpc_1504'
snap = '028_z000p000'

#which snapshots to look at
lines=['028_z000p000']

#do for all sizes for all times
for DirList in DList:
  for i in range(len(lines)):

    #pretty sure splitting like this isnt't necessary ...
    line=lines[i].split('\n')[0].split('_')
    Num=line[0]
    fend='_'+line[1]
    exts         = Num.zfill(3)
    fpart        = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'
    fsub         = Base+DirList+'/groups_'      +exts+fend+'/'+'eagle_subfind_tab_'      +exts+fend+'.0.hdf5'

    if os.path.exists(fpart)==True:
      # Load particle data into "PartData" Structure
      PartData = ReadEagleParticles_DM.ReadEagleParticles_DM(Base, DirList, fend, exts)

      print('\nCreating the hdf5 file for the simulation box...')

      fn = Base+'processed_data/'+exts+fend+ident+'_DM.hdf5'

      output  = h5.File(fn, "w")
      grp0    = output.create_group("Header")
      grp2    = output.create_group("PartData")

      dset    = grp0.create_dataset('h',          data = PartData['HubbleParam'])
      dset    = grp0.create_dataset('PartMassDM', data = PartData['PartMassDM'])
      dset    = grp0.create_dataset('z',          data = PartData['Redshift'])

      dset    = grp2.create_dataset('PosDM', data = PartData['PosDM'])
      dset    = grp2.create_dataset('IDsDM', data = PartData['IDsDM'])
      dset    = grp2.create_dataset('VelDM', data = PartData['VelDM'])
      dset    = grp2.create_dataset('GrpNum_DM', data = PartData['GrpNum_DM'])
      dset    = grp2.create_dataset('SubNum_DM', data = PartData['SubNum_DM'])

      output.close()
      print('\nOutput:',fn)
