#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 14:52:05 2021

@author: matt
"""

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
# from scipy.interpolate import interp1d
# from scipy.interpolate import interpn

from scipy.integrate   import quad

# from scipy.optimize    import minimize
# from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.stats       import binned_statistic

import matplotlib
matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt
from matplotlib.cm     import Greys
from matplotlib.cm     import Purples
from matplotlib.cm     import Blues
from matplotlib.cm     import Greens
from matplotlib.cm     import Oranges
from matplotlib.cm     import Reds

# from matplotlib.colors import LogNorm
# from matplotlib.colors import ListedColormap
# from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               # AutoMinorLocator)
# from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import ReadEagleSubfind_halo

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

grav_const   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)


def my_nonzero_nan_quantile(array, q):
  '''
  '''
  copy = array[np.logical_not(np.isnan(array))]
  copy = copy[copy!=0]
  if len(copy) == 0:
    return(0)

  return(np.quantile(copy, q))

def my_nonzero_nan_median(array):
  return(my_nonzero_nan_quantile(array, 0.5))
def my_nonzero_nan_psigma(array):
  return(my_nonzero_nan_quantile(array, 0.84))
def my_nonzero_nan_msigma(array):
  return(my_nonzero_nan_quantile(array, 0.16))

def plot_stellar_mass_stellar_r_half(raw_data, snap_i=0):
  '''
  '''
  #subhalo stellar mass in M_sun
  x_data = raw_data['MassType'][:, 4] * 1e10 / raw_data['HubbleParam']
  #subhalo stellar half mass radii pkpc
  y_data = raw_data['HalfMassRadius'][:, 4] * 1000 * raw_data['ExpansionFactor'] / raw_data['HubbleParam']

  x_bin_edges   = np.logspace(7, 13, 13)
  x_bin_middles = np.logspace(7.25, 12.75, 12)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started rad')

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e13))
  plt.ylim((5e-1, 5e1))

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(12, 0.9, snap_list[snap_i][4:], ha='right')

  plt.xlabel(r'$M_\star / $M$_\odot $')
  plt.ylabel(r'$r_{1/2}$')

  plt.savefig(pwd + 'mass_r_half' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  #subhalo stellar mass in M_sun
  x_data = raw_data['MassType'][:, 4] * 1e10 / raw_data['HubbleParam']
  #subhalo stellar half mass radii pkpc
  y_data = raw_data['HalfMassRadius'][:, 4] * 1000 * raw_data['ExpansionFactor'] / raw_data['HubbleParam']

  x_bin_edges   = np.logspace(7, 13, 13)
  x_bin_middles = np.logspace(7.25, 12.75, 12)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started rad proj')

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e13))
  plt.ylim((5e-1, 5e1))

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(12, 0.9, snap_list[snap_i][4:], ha='right')

  plt.xlabel(r'$M_\star / $M$_\odot $')
  plt.ylabel(r'$r_{1/2}$')

  plt.savefig(pwd + 'mass_r_half_proj' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  return

if __name__ == '__main__':

  #Where the data is
  Base0 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/Jacks_7x376dm/data'
  pwd0  = '/home/mwilkinson/EAGLE/L0025N0376/Jacks_7x376dm'

  Base1 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/Jacks_7x376dm_Restart_z=1/data'
  pwd1  = '/home/mwilkinson/EAGLE/L0025N0376/Jacks_7x376dm_Restart_z=1'

  Base2 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/REFERENCE/data'
  pwd2  = '/home/mwilkinson/EAGLE/L0025N0376/REFERENCE'

  Base3 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/REFERENCE_Restart_z=1/data'
  pwd3  = '/home/mwilkinson/EAGLE/L0025N0376/REFERENCE_Restart_z=1'

  Base_list = [Base0, Base1, Base2, Base3]
  pwd_list  = [pwd0,  pwd1,  pwd2,  pwd3]

  #which snapshots to look at
  # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
  #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
  #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
  #              '016_z001p737', '015_z002p012']
  snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']


  for j in range(4):
    Base = Base_list[j]
    pwd  = pwd_list[j]

    #do for all sizes for all times
    for i in range(len(snap_list)):

      #pretty sure splitting like this isnt't necessary ...
      line=snap_list[i].split('\n')[0].split('_')
      Num=line[0]
      fend='_'+line[1]
      exts         = Num.zfill(3)

      data = ReadEagleSubfind_halo.ReadEagleSubfind_halo(Base, '', fend, exts)

  pass