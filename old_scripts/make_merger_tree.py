#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 15:08:13 2021

@author: matt

This assumes that each group has its own hdf5 file that were made with
makehdf_groups.py
"""

import h5py as h5
import os
import numpy as np


def find_halo_in_next_snap(base, data_location, data_destination, identity,
                           current_snap, previous_snap, previous_merger_groups):
  '''The meat of the alogrithm
  '''
  merger_group_array = [0]

  #number of halo files
  current_halos = np.sum([identity in file_name for file_name
                          in os.listdir(path = data_destination + current_snap)])

  #list to hold current halo ids
  current_halo_id_array = []

  previous_dm_id_array = [None] * (np.amax(previous_merger_groups) + 1)

  #load all current dm particles
  for i in range(current_halos):
    #Subfind groups start from 1
    if i != 0:

      current_group = h5.File(data_destination + current_snap + '/' + identity
                              + '.' + str(i) + '.hdf5', 'r')

      current_dm_ids = current_group['PartType1/ParticleIDs'][()]

      current_group.close()

  #load all previous dm particles
  for i in previous_merger_groups:
    #Subfind groups start from 1
    previous_group = h5.File(data_destination + current_snap + '/' + identity
                             + '.' + str(i) + '.hdf5', 'r')

    previous_dm_ids = previous_group['PartType1/ParticleIDs'][()]

    previous_dm_id_array[i] = previous_dm_ids

    previous_group.close()


      # for previous_dm_ids in range(len(previous_dm_id_array)):
      #   number_in = np.sum(np.isin(search_ids, previous_dm_ids))



  raise SyntaxError('invalid syntax')

  return merger_group_array


def make_merger_tree(base, data_location, data_destination, identity, snap_list):
  '''
  '''
  #TODO might want to have a cut on number of halos
  snap_list.sort()


  #dictionary of lists
  #lists index is the subfind group number and the value is my merger id
  subfind_group_to_merger_group_array = {snap: [] for snap in snap_list}

  #number of halo files
  inital_halos = np.sum([identity in file_name for file_name
                         in os.listdir(path = data_destination + snap_list[0])])

  subfind_group_to_merger_group_array[snap_list[0]] = np.arange(inital_halos)

  for (current_snap, previous_snap) in zip(snap_list[1:], snap_list[:-1]):

    merger_group_array = find_halo_in_next_snap(
      base, data_location, data_destination, identity,
      current_snap, previous_snap, subfind_group_to_merger_group_array[previous_snap])

    subfind_group_to_merger_group_array[current_snap] = merger_group_array

  print(subfind_group_to_merger_group_array)

  return


if __name__ == '__main__':

  #TODO should make this argv argc inputs
  #file stuff
  base             = '/home/matt/Documents/UWA/EAGLE/'
  data_location    = base + 'L0012N0188/EAGLE_REFERENCE/data/'
  data_destination = base + 'processed_data/'

  #name to postpend to files
  identity = '_12Mpc_188'

  #which snapshots to look at
  # snap_list = ['019_z001p004', '023_z000p503', '028_z000p000']
  snap_list = ['019_z001p004', '023_z000p503']

  make_merger_tree(base, data_location, data_destination, identity, snap_list)