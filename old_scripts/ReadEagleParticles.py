#script recieved from Aditiua Manuwal (UWA)

import h5py  as h5
import os
from   numpy import *

def ReadEagleParticles(Base,DirList,fend,exts):
    fn        = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'
    print(' __'); print(' Particles:',Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend,' ...')
    fs            = h5.File(fn,"r")
    RuntimePars   = fs['RuntimePars'].attrs
    Header        = fs['Header'].attrs
    #Units         = fs['Units'].attrs
    #Constants     = fs['Constants'].attrs
    HubbleParam = Header['HubbleParam']
    PartMassDM = Header['MassTable'][1]
    FNumPerSnap   = RuntimePars['NumFilesPerSnapshot']
    NumParts      = Header['NumPart_ThisFile']
    NumPartTot    = Header['NumPart_Total']

    fs.close()

    PosDM              = empty((NumPartTot[1],3),dtype=float)
    VelDM              = empty((NumPartTot[1],3),dtype=float)
    IDsDM              = empty(NumPartTot[1],    dtype=float)
    GrpNum_DM          = empty(NumPartTot[1],    dtype=int)
    SubNum_DM          = empty(NumPartTot[1],    dtype=int)

    if NumPartTot[0]>0:
        PosGas         = empty((NumPartTot[0],3),dtype=float)
        VelGas         = empty((NumPartTot[0],3),dtype=float)
        DensGas        = empty(NumPartTot[0],    dtype=float)
        HSML_Gas       = empty(NumPartTot[0],    dtype=float)
        #fHAll          = empty(NumPartTot[0],    dtype=float)
        TempGas        = empty(NumPartTot[0],    dtype=float)
        #SFR            = empty(NumPartTot[0],    dtype=float)
        MassGas        = empty(NumPartTot[0],    dtype=float)
        HAbundance    = empty(NumPartTot[0],    dtype=float)
        HSPAbundance    = empty(NumPartTot[0],    dtype=float)
        #GasMetallicity = empty(NumPartTot[0],    dtype=float)
        GrpNum_Gas     = empty(NumPartTot[0],    dtype=int)
        SubNum_Gas     = empty(NumPartTot[0],    dtype=int)

    if NumPartTot[4]>0:
        PosStar        = empty((NumPartTot[4],3),dtype=float)
        VelStar        = empty((NumPartTot[4],3),dtype=float)
        MassStar       = empty(NumPartTot[4],    dtype=float)
        #BirthDensity   = empty(NumPartTot[4],    dtype=float)
        #Metallicity    = empty(NumPartTot[4],    dtype=float)
        Star_aform     = empty(NumPartTot[4],    dtype=float)
        #BindingEnergy  = empty(NumPartTot[4],    dtype=float)
        #HSML_Star      = empty(NumPartTot[4],    dtype=float)
        GrpNum_Star    = empty(NumPartTot[4],    dtype=int)
        SubNum_Star    = empty(NumPartTot[4],    dtype=int)

    NDM_c              = 0
    NGas_c             = 0
    NStar_c            = 0
    for ifile in range(0,FNumPerSnap):
        if os.path.isdir(Base+DirList+'data/'):
            fn                 = Base+DirList+'data/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.'+str(ifile)+'.hdf5'
        else:
            fn                 = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.'+str(ifile)+'.hdf5'
        fs                     = h5.File(fn,"r")
        Header                 = fs['Header'].attrs
        NumParts               = Header['NumPart_ThisFile']

        PosDM[NDM_c:NDM_c+NumParts[1],:]              = fs["PartType1/Coordinates"].value
        VelDM[NDM_c:NDM_c+NumParts[1],:]              = fs["PartType1/Velocity"].value
        IDsDM[NDM_c:NDM_c+NumParts[1]]                = fs["PartType1/ParticleIDs"].value
        GrpNum_DM[NDM_c:NDM_c+NumParts[1]]            = fs["PartType1/GroupNumber"].value
        SubNum_DM[NDM_c:NDM_c+NumParts[1]]            = fs["PartType1/SubGroupNumber"].value

        if NumParts[0]>0:
            PosGas[NGas_c:NGas_c+NumParts[0],:]       = fs["PartType0/Coordinates"].value
            VelGas[NGas_c:NGas_c+NumParts[0],:]       = fs["PartType0/Velocity"].value
            DensGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Density"].value
            #GasMetallicity[NGas_c:NGas_c+NumParts[0]] = fs["PartType0/SmoothedMetallicity"].value
            HSML_Gas[NGas_c:NGas_c+NumParts[0]]       = fs["PartType0/SmoothingLength"].value
            #fHAll[NGas_c:NGas_c+NumParts[0]]          = fs["PartType0/ElementAbundance/Hydrogen"].value
            #SFR[NGas_c:NGas_c+NumParts[0]]            = fs["PartType0/StarFormationRate"].value
            TempGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Temperature"].value
            MassGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Mass"].value
            HAbundance[NGas_c:NGas_c+NumParts[0]]    = fs["PartType0/ElementAbundance/Hydrogen"]
            HSPAbundance[NGas_c:NGas_c+NumParts[0]]  = fs["PartType0/SmoothedElementAbundance/Hydrogen"]
            GrpNum_Gas[NGas_c:NGas_c+NumParts[0]]     = fs["PartType0/GroupNumber"].value
            SubNum_Gas[NGas_c:NGas_c+NumParts[0]]     = fs["PartType0/SubGroupNumber"].value

        if NumParts[4]>0:
            PosStar[NStar_c:NStar_c+NumParts[4],:]    = fs["PartType4/Coordinates"].value
            VelStar[NStar_c:NStar_c+NumParts[4],:]    = fs["PartType4/Velocity"].value
            MassStar[NStar_c:NStar_c+NumParts[4]]     = fs["PartType4/Mass"].value
            #BirthDensity[NStar_c:NStar_c+NumParts[4]] = fs["PartType4/BirthDensity"].value
            #Metallicity[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/SmoothedMetallicity"].value
            Star_aform[NStar_c:NStar_c+NumParts[4]]   = fs["PartType4/StellarFormationTime"].value
            #BindingEnergy[NStar_c:NStar_c+NumParts[4]]= fs["PartType4/ParticleBindingEnergy"].value
            #HSML_Star[NStar_c:NStar_c+NumParts[4]]    = fs["PartType4/SmoothingLength"].value
            GrpNum_Star[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/GroupNumber"].value
            SubNum_Star[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/SubGroupNumber"].value

        NDM_c              += NumParts[1]
        NGas_c             += NumParts[0]
        NStar_c            += NumParts[4]

        fs.close()

    if ((NumPartTot[0]>0) & (NumPartTot[4]>0)):
        return {'PartMassDM':PartMassDM, 'HubbleParam':HubbleParam, 'IDsDM':IDsDM,                'GrpNum_DM':GrpNum_DM,      'PosDM':PosDM,     
                'PosStar':PosStar, 'GrpNum_Star':GrpNum_Star, 'MassStar':MassStar,
                'PosGas':PosGas, 'HSML_Gas':HSML_Gas, 
                'DensGas':DensGas,#'BirthDensity':BirthDensity,'Metallicity':Metallicity, 
                'MassGas':MassGas, 'GrpNum_Gas':GrpNum_Gas, 'VelGas':VelGas,'SFT':Star_aform,
                'VelStar':VelStar, 'VelDM':VelDM,     #'SFR':SFR,              
                'TempGas':TempGas,#'BindingEnergy':BindingEnergy,          'fHAll':fHAll,
                'SubNum_Star':SubNum_Star,    'SubNum_DM':SubNum_DM,      'SubNum_Gas':SubNum_Gas,
                'HAbundance':HAbundance, 'HSPAbundance':HSPAbundance
                #'NDM':NDM_c,                  'NGas':NGas_c,              'NStar':NStar_c,
                #'GasMetallicity':GasMetallicity
               }
    else:
        return {'PartMassDM':PartMassDM, 'HubbleParam':HubbleParam, 'IDsDM':IDsDM, 'GrpNum_DM':GrpNum_DM, 'PosDM':PosDM,'VelDM':VelDM,
                'SubNum_DM':SubNum_DM,'VelDM':VelDM
                #'NDM':NDM_c, 'NGas':NGas_c, 'NStar':NStar_c
               }

