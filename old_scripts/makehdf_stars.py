#script recieved from Aditiua Manuwal (UWA)

import ReadEagleParticles_Star
import h5py                   as h5
import os

print('\nFor HYDRO ...')

#Where the data is
Base = '/fred/oz009/mwilkinson/EAGLE/'
DList   = ['REFERENCE']
#os.chdir('/home/matt/Documents/UWA/EAGLE/results')
ident = '_100Mpc_1504'
snap = '028_z000p000'

#which snapshots to look at
lines=['028_z000p000']

#do for all sizes for all times
for DirList in DList:
  for i in range(len(lines)):

    #pretty sure splitting like this isnt't necessary ...
    line=lines[i].split('\n')[0].split('_')
    Num=line[0]
    fend='_'+line[1]
    exts         = Num.zfill(3)
    fpart        = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'

    if os.path.exists(fpart)==True:
      # Load particle data into "PartData" Structure
      PartData = ReadEagleParticles_Star.ReadEagleParticles_Star(Base, DirList, fend, exts)

      print('\nCreating the hdf5 file for the simulation box...')

      fn = Base+'processed_data/'+exts+fend+ident+'_Star.hdf5'

      output  = h5.File(fn, "w")
      grp2    = output.create_group("PartData")

      dset    = grp2.create_dataset('PosStar', data = PartData['PosStar'])
      dset    = grp2.create_dataset('VelStar', data = PartData['VelStar'])
      dset    = grp2.create_dataset('MassStar', data = PartData['MassStar'])
      dset    = grp2.create_dataset('SmoothedMetalicity', data = PartData['Z'])
      dset    = grp2.create_dataset('StellarFormationTime', data = PartData['SFa'])
      dset    = grp2.create_dataset('BindingEnergy', data = PartData['BindingEnergy'])
      dset    = grp2.create_dataset('ParticleIDs', data = PartData['ParticleIDs'])
      dset    = grp2.create_dataset('GrpNum_Star', data = PartData['GrpNum_Star'])
      dset    = grp2.create_dataset('SubNum_Star', data = PartData['SubNum_Star'])

      output.close()
      print('\nOutput:',fn)
