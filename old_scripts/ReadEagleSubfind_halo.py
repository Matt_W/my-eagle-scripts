#script recieved from Aditiua Manuwal (UWA)

import h5py  as h5
import os
import numpy as np

def ReadEagleSubfind_halo(Base,DirList,fend,exts):
  fn            = Base+DirList+'/groups_'+exts+fend+'/'+'eagle_subfind_tab_'+exts+fend+'.0.hdf5'
  print(' __'); print(' Directory:',Base+DirList+'/groups_'+exts+fend+'/'+'eagle_subfind_tab_'+exts+fend+' ...')
  with h5.File(fn,"r") as fs:
    Header           = fs['Header'].attrs

    Ntask            = Header['NTask']
    TotNgroups       = Header['TotNgroups']
    TotNsubgroups    = Header['TotNsubgroups']

    HubbleParam     = Header['HubbleParam']
    # HubbleH         = Header['H(z)']
    ExpansionFactor = Header['ExpansionFactor']
    # PartMassDM      = Header['PartMassDM']
    Redshift        = Header['Redshift']
    # Om              = [Header['Omega0'],Header['OmegaLambda'],Header['OmegaBaryon']]
    # BoxSize         = Header['BoxSize']

  # --- Define Group Arrays
  # if TotNgroups > 0:
  Group_M_Crit200 = np.zeros(TotNgroups,     dtype=np.float64)
  Group_R_Crit200 = np.zeros(TotNgroups,     dtype=np.float64)
  # Group_R_Mean200 = np.zeros(TotNgroups,     dtype=np.float64)
  # GroupMass       = np.zeros(TotNgroups,     dtype=np.float64)
  GroupPos        = np.zeros((TotNgroups,3), dtype=np.float64)
  # GroupLen        = np.zeros(TotNgroups,     dtype=np.int64)
  FirstSub        = np.zeros(TotNgroups,    dtype=np.int64)
  # NumOfSubhalos   = np.zeros(TotNgroups,    dtype=np.int64)

# --- Define Subhalo Arrays
  # HalfMassRad       = np.zeros((TotNsubgroups,6), dtype=np.float64)
  # HalfMassProjRad   = np.zeros((TotNsubgroups,6), dtype=np.float64)
  # MassType          = np.zeros((TotNsubgroups,6), dtype=np.float64)
  # Pcom              = np.zeros((TotNsubgroups,3), dtype=np.float64)
  SubPos            = np.zeros((TotNsubgroups,3), dtype=np.float64)
  # SubLenType        = np.zeros((TotNsubgroups,6), dtype=np.int64)
  # Vbulk             = np.zeros((TotNsubgroups,3), dtype=np.float64)
  # Vmax              = np.zeros(TotNsubgroups,     dtype=np.float64)
  # VmaxRadius        = np.zeros(TotNsubgroups,     dtype=np.float64)
  # SFRate            = np.zeros(TotNsubgroups,     dtype=np.float64)
  # BHMass            = np.zeros(TotNsubgroups,     dtype=np.float64)
  # StellarMass       = np.zeros(TotNsubgroups,     dtype=np.float64)
  # SubMass           = np.zeros(TotNsubgroups,     dtype=np.float64)
  # SubLen            = np.zeros(TotNsubgroups,     dtype=np.int64)
  # SubOffset         = np.zeros(TotNsubgroups,     dtype=np.int64)
  SubGroupNumber    = np.zeros(TotNsubgroups,     dtype=np.int64)
  GroupNumber       = np.zeros(TotNsubgroups,     dtype=np.int64)
  # MostBoundID       = np.zeros(TotNsubgroups,     dtype=np.int64)

  # subtab_index      = np.zeros(TotNsubgroups,     dtype=np.int64)
  # sub_location      = np.zeros(TotNsubgroups,     dtype=np.int64)

  NGrp_c = 0
  NSub_c = 0

  for ifile in range(Ntask):
    if os.path.isdir(Base+DirList+'data/'):
      fn = Base+DirList+'data/groups_'+exts+fend+'/'+'eagle_subfind_tab_'+exts+fend+'.'+str(ifile)+'.hdf5'
    else:
      fn = Base+DirList+'/groups_'+exts+fend+'/'+'eagle_subfind_tab_'+exts+fend+'.'+str(ifile)+'.hdf5'

    with h5.File(fn,"r") as fs:

      Header          = fs['Header'].attrs

      Ngroups         = Header['Ngroups']
      Nsubgroups      = Header['Nsubgroups']

      if Ngroups > 0:
        Group_M_Crit200[NGrp_c:NGrp_c+Ngroups]     = fs["FOF/Group_M_Crit200"][()]
        Group_R_Crit200[NGrp_c:NGrp_c+Ngroups]     = fs["FOF/Group_R_Crit200"][()]
        # Group_R_Mean200[NGrp_c:NGrp_c+Ngroups]      = fs["FOF/Group_R_Mean200"][()]
        # GroupLen[NGrp_c:NGrp_c+Ngroups]             = fs["FOF/GroupLength"][()]
        # GroupMass[NGrp_c:NGrp_c+Ngroups]            = fs["FOF/GroupMass"][()]
        GroupPos[NGrp_c:NGrp_c+Ngroups]            = fs["FOF/GroupCentreOfPotential"][()]
        FirstSub[NGrp_c:NGrp_c+Ngroups]            = fs["FOF/FirstSubhaloID"][()]
        # NumOfSubhalos[NGrp_c:NGrp_c+Ngroups]        = fs["FOF/NumOfSubhalos"][()]

        NGrp_c += Ngroups

      # if Nsubgroups > 0:
      #   HalfMassRad[NSub_c:NSub_c+Nsubgroups,:]     = fs["Subhalo/HalfMassRad"][()]
      #   HalfMassProjRad[NSub_c:NSub_c+Nsubgroups,:] = fs["Subhalo/HalfMassProjRad"][()]
      #   SubLenType[NSub_c:NSub_c+Nsubgroups,:]      = fs["Subhalo/SubLengthType"][()]
      #   MassType[NSub_c:NSub_c+Nsubgroups,:]        = fs["Subhalo/MassType"][()]
        SubPos[NSub_c:NSub_c+Nsubgroups,:]          = fs["Subhalo/CentreOfPotential"][()]
      #   Pcom[NSub_c:NSub_c+Nsubgroups,:]            = fs["Subhalo/CentreOfMass"][()]
      #   Vbulk[NSub_c:NSub_c+Nsubgroups]             = fs["Subhalo/Velocity"][()]
      #   Vmax[NSub_c:NSub_c+Nsubgroups]              = fs["Subhalo/Vmax"][()]
      #   VmaxRadius[NSub_c:NSub_c+Nsubgroups]        = fs["Subhalo/VmaxRadius"][()]
      #   SubLen[NSub_c:NSub_c+Nsubgroups]            = fs["Subhalo/SubLength"][()]
      #   SFRate[NSub_c:NSub_c+Nsubgroups]            = fs["Subhalo/StarFormationRate"][()]
      #   BHMass[NSub_c:NSub_c+Nsubgroups]            = fs["Subhalo/BlackHoleMass"][()]
      #   StellarMass[NSub_c:NSub_c+Nsubgroups]       = fs["Subhalo/Stars/Mass"][()]
      #   SubMass[NSub_c:NSub_c+Nsubgroups]           = fs["Subhalo/Mass"][()]
      #   SubOffset[NSub_c:NSub_c+Nsubgroups]         = fs["Subhalo/SubOffset"][()]
        SubGroupNumber[NSub_c:NSub_c+Nsubgroups]    = fs["Subhalo/SubGroupNumber"][()]
        GroupNumber[NSub_c:NSub_c+Nsubgroups]       = fs["Subhalo/GroupNumber"][()]
      #   MostBoundID[NSub_c:NSub_c+Nsubgroups]       = fs["Subhalo/IDMostBound"][()]

      #   subtab_index[NSub_c:NSub_c+Nsubgroups].fill(ifile)
      #   sub_location[NSub_c:NSub_c+Nsubgroups]      = arange(0,Nsubgroups)

        NSub_c += Nsubgroups

  return {##Box properties
          # 'TotNgroups'         :TotNgroups,
          # 'TotNsubgroups'      :TotNsubgroups,
          # 'HubbleParam'        :HubbleParam,
          # 'H(z)'               :HubbleH
          # 'ExpansionFactor'    :ExpansionFactor,
          # 'PartMassDM'         :PartMassDM,
          # 'Redshift'           :Redshift,
          # 'Omega':Om,
          # 'BoxSize'            :BoxSize,
          ##Group properties
          'Group_M_Crit200'    :Group_M_Crit200,
          'Group_R_Crit200'    :Group_R_Crit200,
          # 'Group_R_Mean200'    :Group_R_Mean200,
          # 'GroupMass'          :GroupMass,
          'GroupPos'           :GroupPos,
          # 'GroupLen'           :GroupLen,
          'FirstSub'           :FirstSub,
          # 'NumOfSubhalos'      :NumOfSubhalos,
          ##Subgroup properties
          # 'HalfMassRadius'     :HalfMassRad,
          # 'HalfMassProjRadius' :HalfMassProjRad,
          'SubPos'             :SubPos,
          # 'Vbulk'              :Vbulk,
          # 'Vmax'               :Vmax,
          # 'Rmax'               :VmaxRadius,
          # 'SFRate'             :SFRate,
          # 'MassType'           :MassType,
          # 'SubMass'            :SubMass,
          # 'SubLen'             :SubLen,
          # 'SubOffset'          :SubOffset,
          'GroupNumber'        :GroupNumber,
          'SubGroupNumber'     :SubGroupNumber,
          # 'MostBoundID'        :MostBoundID,
          # 'subtab_index'       :subtab_index,
          # 'sub_location'       :sub_location,
          }