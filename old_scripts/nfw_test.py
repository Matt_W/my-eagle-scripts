
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 09:46:14 2020

@author: matt
"""
import os
import h5py  as h5

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.patches as mpl
import splotch as splt

import astropy.units as u
import astropy.constants as cst

from scipy.optimize import brentq
import scipy.optimize

import my_gmm


print("Plotting scripts are now (17/3/20) depreciated. See plot-script.py in test-galaxies")


###############################################################################
#test functions
def ugly_mf(r):
  '''Result of d v_c / dr with 4piGrho_0R_s^3/2 = 1 and R_s = 1
  ugly "mass function"
  '''
  R_s = 1
  this_part = np.log((R_s + r)/R_s) - r/(R_s + r)
  total = (1/(R_s+r)**2 - this_part/r**2) / np.sqrt(this_part/r)
  return(total)

def find_r():
  '''Finds radius of maximum velocity in terms of R_s
  '''
  R_s = 1
  out = brentq(ugly_mf, 0.01, 1000)
  print(out / R_s)
  return(out)

# R_v_max = 2.1625815870646 R_s
###############################################################################
def find_v():
  '''find v_circ_max in terms of R_s and rho_0
  '''
  rho_0 = np.random.rand() * u.kg / u.m**3
  R_s   = np.random.rand() *u.kpc
  r     = 2.1625815870646 * R_s

  V_max = nfw_velocity_circ(rho_0, R_s, r)

  return((V_max / (R_s * np.sqrt(cst.G * rho_0))).to(''))

#
###############################################################################
#nfw stuff
#units are needed because I use astropy.constants.G for the gravitational const

def nfw_density(rho_0, R_s, r):
  '''density at a given radii given central potential and scale radius.
  R_s and r shoud be in the same units
  Result in same units as rho_0
  '''
  rho = rho_0 / (r/R_s * (1 + r/R_s)**2)
  return(rho)

def nfw_mass(rho_0, R_s, r):
  '''mass at a given radii given central potential and scale radius.
  R_s and r shoud be in the same units
  Result in units of rho_0 * R_s^3
  May be numerical errors for small r
  '''
  M = 4*np.pi*rho_0*R_s**3 * (np.log((R_s+r)/R_s) - r/(R_s+r))
  return(M)

def nfw_velocity_circ(rho_0, R_s, r):
  '''potential at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.Mpc
  if not isinstance(r, u.Quantity):
    r     *= u.Mpc

  mass = nfw_mass(rho_0, R_s, r)
  v_circ = np.sqrt(cst.G * mass / r)
  v_circ = v_circ.to(u.km/u.s)

  return(v_circ)

def nfw_potential(rho_0, R_s, r):
  '''potential at a given radii given central density and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  Might not work for r=0.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.Mpc
  if not isinstance(r, u.Quantity):
    r     *= u.Mpc

  phi = -4*np.pi*cst.G*rho_0*R_s**3/r * np.log(1+r/R_s)

  return(phi)

def nfw_potential_maximum(rho_0, R_s):
  '''potential at r=0 given central density and scale radius.
  nfw_potential will give nan and RuntimeWarnings
  '''
  phi_0 = -4*np.pi*cst.G*rho_0*R_s**2

  return(phi_0)

def nfw_params_from_vels(V_max, R_v_max):
  '''Calculates rho_0 and R_s given V_max and R_v_max.
  If not given astropy units, assumes km/s and Mpc.
  '''
  if not isinstance(V_max,   u.Quantity):
    V_max   *= u.km / u.s
  if not isinstance(R_v_max, u.Quantity):
    R_v_max *= u.Mpc

  R_s   = R_v_max / 2.1625815870646
  rho_0 = 1/cst.G * (V_max / (R_s * 1.6483500453640))**2
  rho_0 = rho_0.to(u.kg/u.m**3)

  return(rho_0, R_s)

def nfw_vels_from_params(rho_0, R_s):
  '''Calculates V_max and R_v_max given rho_0 and R_s.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s,   u.Quantity):
    R_s   *= u.Mpc

  R_v_max = R_s * 2.1625815870646
  V_max   = R_s * 1.6483500453640 * np.sqrt(cst.G * rho_0)
  V_max   = V_max.to(u.km/u.s)

  return(V_max, R_v_max)

def nfw_rho_0(conc, M_vir, R_s):
  '''Calculates the initial NFW density given the concentration, virial mass,
  and scale radius.
  Result in units of M_vir / R_s^3.
  '''
  rho_0 = M_vir / (4*np.pi * R_s**3 * (np.log(1+conc) - conc/(1+conc)))

  return(rho_0)

def nfw_total_circular_specific_energy(rho_0, R_s, r):
  '''total specific energy (kinetic + potential)/mass for a particle in a
  circular orbit at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''

  kinetic   = 1/2 * nfw_velocity_circ(rho_0, R_s, r)**2
  potential = nfw_potential(rho_0, R_s, r)

  total = kinetic + potential

  return(total)

def nfw_total_specific_energy(rho_0, R_s, r_vector, v_vector):
  '''total specific energy (kinetic + potential)/mass for a particle in an
  arbitrary orbit at a given radii given central potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(r_vector, u.Quantity):
    r_vector *= u.Mpc
  if not isinstance(v_vector, u.Quantity):
    v_vector *= u.km / u.s

  kinetic   = 1/2 * np.linalg.norm(v_vector, axis=1)**2
  potential = nfw_potential(rho_0, R_s, np.linalg.norm(r_vector, axis=1))

  total = kinetic + potential

  return(total)

def nfw_energy_helper(r, E, rho_0, R_s):
  '''Function designed to be used in scipy.optimize.brentq()
  '''
  r *= u.Mpc
  E_off_by = nfw_total_circular_specific_energy(rho_0, R_s, r) - E

  return(E_off_by.value)

def nfw_energy_helper_2(r, E, rho_0, R_s):
  '''Function designed to be used in scipy.optimize.brentq()
  Is thw total equation that nfw_energy_helper is doing but has no impact at
  all on speed of the root finder :(
  output is in Mpc**2/s**2 rather than km**2/s**2
  '''
  r *= u.Mpc

  new_E_off_by = -2*np.pi*cst.G*rho_0*R_s**3 * (np.log((R_s+r)/R_s) + r/(R_s+r)) /r - E

  return(new_E_off_by.value)

def nfw_radius_circular_orbit_energy(rho_0, R_s, E):
  '''radius of a circular orbit for a given specific energy given central
  potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s,   u.Quantity):
    R_s   *= u.Mpc

  e_low = np.min(E)
  #make sure only do bound particles
  e_hi  = np.max(E[E < 0])

  low = 1e-4
  hi  = 1e-2

  resolution = int(1e7 + 1)

  low_bad = True
  while(low_bad):
    if e_low > nfw_total_circular_specific_energy(rho_0, R_s, low * u.Mpc):
      #bounds are good
      low_bad = False
    else:
      #reduce by half an order of magnitude and try again
      low *= 0.316227766

  hi_bad = True
  while(hi_bad):
    if e_hi < nfw_total_circular_specific_energy(rho_0, R_s, hi * u.Mpc):
      #bounds are good
      hi_bad = False

    elif hi > 10 - 1e-10:
      #some particles are unbound
      hi_bad = False

    else:
      #increase by half an order of magnitude and try again
      hi *= 3.16227766

  radii = np.logspace(np.log10(low), np.log10(hi), resolution) * u.Mpc
  energies = -2*np.pi*cst.G*rho_0*R_s**3 * (np.log((R_s+radii)/R_s) + radii/(R_s+radii)) /radii

  out = np.interp(E, energies, radii)

  return(out)

def old_nfw_radius_circular_orbit_energy(rho_0, R_s, E):
  '''radius of a circular orbit for a given specific energy given central
  potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  '''
#  #messing with bounds may improve speed but also make bouds errors (ValueError)
  low = 1e-8                  #Mpc most bound particle
  hi  = 0.03                  #Mpc the apature
  retract   = 0.999           #ratio incase of numbers exactly equal
  lin_scale = 0.001           #Mpc needed for central particles
  exp_scale = 1.0 + 10/len(E) #maximum ratio to search for next particle
  #E is iterable
  #Pretty sure this can't be done with vector operations

  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s,   u.Quantity):
    R_s   *= u.Mpc

  try:
    #where the output goes
    out = np.empty(len(E))

    #needed to start off the loop
    out[-1] = retract * scipy.optimize.brentq(nfw_energy_helper, low, hi, args=(np.min(E), rho_0, R_s))

    #sorting allows smaller range to be root finded
    args = np.argsort(E)
    undo_args = np.argsort(args)

    #trying to be faster / fancy. Does not use paralelization for the root finder
    for i, e in enumerate(E[args]):
      finding_value = True
      while(finding_value):

        try:
          value = scipy.optimize.brenth(nfw_energy_helper,
                                        out[i-1]*retract, out[i-1]*exp_scale + lin_scale,
                                        args=(e, rho_0, R_s))
          finding_value=False
#          exp_scale -= 0.001

        except ValueError:
          finding_value=True
          exp_scale += 0.01

#          print('i   =', i)
#          print('a   =', out[i-1]*retract)
#          print('f(a)=', nfw_energy_helper(out[i-1]*retract, e, rho_0, R_s))
#          print('b   =', out[i-1]*exp_scale + lin_scale)
#          print('f(b)=', nfw_energy_helper(out[i-1]*exp_scale + lin_scale, e, rho_0, R_s))
#          raise ValueError('f(a) and f(b) must have different signs')

      out[i] = value

    out = out[undo_args]
    out *= u.Mpc

  #E is one number
  except TypeError:
    out = brentq(nfw_energy_helper, low, hi, args=(E, rho_0, R_s))
    out *= u.Mpc

  return(out)

def nfw_specific_angular_momentum(rho_0, R_s, r):
  '''specific angular momentum for a particle in a circular orbut at a given
  radii given cental potential and scale radius.
  If not given astropy units, assumes kg/m^3 and Mpc.
  Returns only the magnitude of the vector.
  '''
  if not isinstance(rho_0, u.Quantity):
    rho_0 *= u.kg / u.m**3
  if not isinstance(R_s, u.Quantity):
    R_s   *= u.Mpc
  if not isinstance(r, u.Quantity):
    r     *= u.Mpc

  v_circ = nfw_velocity_circ(rho_0, R_s, r)
  j_c    = v_circ * r

  return(j_c)

###############################################################################
#nonhalo calculations

def centre_on_galaxy_with_apature(r_vector, v_vector, masses,
                                  CenterOfPotential, BulkVelocity, boxSize):
  '''Centers particles onto the centre of potential, cuts out a chosen apature
  adjusts the velocities to the galaxy frame.
  Probably doesn't need the bulk velocity step(s)
  centre_on_galaxy_with_apature(np.array(3,n), np.array(3,n), np.array(1,n),
                                np.array(3), np.array(3), float)
                                -> np.array(3,m), np.array(3,m), np.array(1,m)
  '''
  rs = np.array(r_vector.copy())
  vs = np.array(v_vector.copy())

  rs -=CenterOfPotential-boxSize/2
  rs %=boxSize
  rs -=boxSize/2

  # Center velocities
  #center velocities on the subhalo CoM velocity: the right center is done
  #later after computing in-aperture CoM velocity
  vs -= BulkVelocity

  # Compute distances
  distances = np.linalg.norm(rs ,axis=1)

  # Restrict particles
  #dimensionfull equivalent to 30kpc aperture
  aperture = 0.03

  extract = (distances<aperture)

  r = rs[extract].copy()
  v = vs[extract].copy()
  m = masses[extract].copy()

  Mstar = np.sum(masses)

  # Compute 30kpc CoM to Sub CoM velocty offset & recenter
  #compute deviation between the in-aperture 30kpc CoM velocity and the current
  #velocity center (i.e [0,0,0] but representing the subhalo CoM velocity)
  dvVmass = np.sum(np.dot(m,v),axis=0)/Mstar

  #re-center to correct velocity center (30kpc CoM velocity)
  v -= dvVmass

  return(r, v, m)

def centre_on_galaxy_with_apature_gas(r_vector, v_vector, masses,
                                      r_gas,    v_gas,    m_gas,
                                      CenterOfPotential, BulkVelocity, boxSize):
  '''Centers particles onto the centre of potential, cuts out a chosen apature
  adjusts the velocities to the galaxy frame. Intendd to be used to put the gas
  into the same frame as the stars. The velocities are fixed to the average
  star velocity.
  '''
  rs = np.array(r_vector.copy())
  vs = np.array(v_vector.copy())

  r_gs = np.array(r_gas.copy())
  v_gs = np.array(v_gas.copy())

  rs   -=CenterOfPotential-boxSize/2
  rs   %=boxSize
  rs   -=boxSize/2

  r_gs -=CenterOfPotential-boxSize/2
  r_gs %=boxSize
  r_gs -=boxSize/2

  # Center velocities
  #center velocities on the subhalo CoM velocity: the right center is done
  #later after computing in-aperture CoM velocity
  vs   -= BulkVelocity
  v_gs -= BulkVelocity

  # Compute distances
  distances = np.linalg.norm(rs ,axis=1)
  dist_gas  = np.linalg.norm(r_gs ,axis=1)

  # Restrict particles
  #dimensionfull equivalent to 30kpc aperture
  aperture = 0.03
  apt_gas  = 0.07

  extract = (distances<aperture)
  ext_gas = (dist_gas < apt_gas)
#  print('Passed=', np.sum(ext_gas), ' Failed=', np.sum(1-np.array(ext_gas)))

  v = vs[extract].copy()
  m = masses[extract].copy()

  r_g = r_gs[ext_gas].copy()
  v_g = v_gs[ext_gas].copy()
  m_g = m_gas[ext_gas].copy()

  Mstar = np.sum(masses)

  # Compute 30kpc CoM to Sub CoM velocty offset & recenter
  #compute deviation between the in-aperture 30kpc CoM velocity and the current
  #velocity center (i.e [0,0,0] but representing the subhalo CoM velocity)
  dvVmass = np.sum(np.dot(m,v),axis=0)/Mstar

  #re-center to correct velocity center (30kpc CoM velocity)
  v_g -= dvVmass

  return(r_g, v_g, m_g)

def total_angular_momentum(r_vector, v_vector, masses):
  '''total angular momentum of given particles. Units shouldn't matter?
  Useful for parties.
  '''
  angular_momentum = np.sum(np.cross(r_vector, v_vector) * masses, axis=0)

  return(angular_momentum)

def z_projection_specific_angular_momentum(r_vector, v_vector, j_vector):
  '''magnitude of a particle's the angular momentum projected onto the z
  direction of total angular momentum.
  '''
  if not isinstance(r_vector, u.Quantity):
    r_vector *= u.Mpc
  if not isinstance(v_vector, u.Quantity):
    v_vector *= u.km / u.s

  #individual specific angular momentums
  js = np.cross(r_vector, v_vector)

  #projection of specific angular momentum in the j_total direction
  j_z = np.dot(js, j_vector) / np.linalg.norm(j_vector)

  return(j_z)

def perpendicular_specific_angular_momentum(r_vector, v_vector, j_vector):
  '''magnitude of a particle's the angular momentum minus the z projected
  angular momentum.
  j_p = j - j_z
  '''
  if not isinstance(r_vector, u.Quantity):
    r_vector *= u.Mpc
  if not isinstance(v_vector, u.Quantity):
    v_vector *= u.km / u.s

  z_norm = j_vector / np.linalg.norm(j_vector)

  #individual specific angular momentums
  js = np.cross(r_vector, v_vector)

  #specific angular momentum in the j_total direction
  #magnitude
  mag_j_z = np.dot(js, z_norm)
  #direction (n,) -> (n,1) * (3,) -> (1,3) = (n,3)
  j_z = mag_j_z[:,np.newaxis] * z_norm[np.newaxis,:]

  j_p = js - j_z

  mag_j_p = np.linalg.norm(j_p, axis=1)

  return(mag_j_p)

###############################################################################
#load and manage data

def calculate_star_phase_space(PosStars, VelStars, MassStars,
                               CentreOfPotential, BulkVelocity,
                               BoxSize, Vmax, Rmax):
  '''Does all the nfw calculations for the phase space of stars.
  '''
  #do the tricky stuff
  #Center, apature, velocities
  PosStars, VelStars, MassStars = centre_on_galaxy_with_apature(PosStars, VelStars, MassStars,
                                                  CentreOfPotential, BulkVelocity, BoxSize)

  #get galaxy angular momentum
  galaxy_angular_momentum = total_angular_momentum(PosStars, VelStars, np.transpose([MassStars]))

  #galaxy NFW properties
  rho_0, R_s = nfw_params_from_vels(Vmax, Rmax)

  #- particle binding energy / maximum binding energy
  e_onmax = -nfw_potential(rho_0, R_s, np.linalg.norm(PosStars, axis=1)) / nfw_potential_maximum(rho_0, R_s)
  e_onmax = e_onmax.to('').value

  #projected specific angular momentum of each particle
  j_zs = z_projection_specific_angular_momentum(PosStars, VelStars, galaxy_angular_momentum)

  #perpendicular specific angular momentum of each particle
  j_ps = perpendicular_specific_angular_momentum(PosStars, VelStars, galaxy_angular_momentum)

  #specific energies of each particle
  energies = nfw_total_specific_energy(rho_0, R_s, PosStars, VelStars)

  #circular radii from specific energy
  #this is the slow part. needs a for loop
  circular_radii = nfw_radius_circular_orbit_energy(rho_0, R_s, energies)

  #circular (maximum) specific angular momentum for particles with the same energy
  j_cs = nfw_specific_angular_momentum(rho_0, R_s, circular_radii)

  #j_z / j_c
  j_zonc = j_zs / j_cs
  j_zonc = j_zonc.value

  #j_p / j_c
  j_ponc = j_ps / j_cs
  j_ponc = j_ponc.value

  return(j_zonc, j_ponc, e_onmax)

def call_j_circ(haloID):
  '''Load everything needed to make j_circ work
  '''
  #if already created file
  if os.path.isfile('../processed_data/L0012N0188/galaxy'
                    +str(haloID)+'j_zonc.npy') and os.path.isfile(
                    '../processed_data/L0012N0188/galaxy'
                    +str(haloID)+'j_ponc.npy') and os.path.isfile(
                    '../processed_data/L0012N0188/galaxy'
                    +str(haloID)+'e_onmax.npy'):

    j_zonc  = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc.npy')
    j_ponc  = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_ponc.npy')
    e_onmax = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'e_onmax.npy')

  else:
  #if file needs creating
    #settings
    Base  = '/home/matt/Documents/UWA/EAGLE/'
    Spec  = 'processed_data/028_z000p000'
    ident = '_12Mpc_188'

    #read processed hdf5 files
    fn = Base+Spec+ident+'_Star.hdf5'
    fs = h5.File(fn,"r")

    mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]

    PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
    MassStars = fs['PartData/MassStar'].value[mask]
    VelStars  = fs['PartData/VelStar'].value[tuple(mask)]

    fs.close()

    fn = Base+Spec+ident+'_halodat.hdf5'
    fs = h5.File(fn,"r")

    #Group
    CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]

    #Subgroup
    FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
    BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
    Vmax              = fs['HaloData/Vmax'].value[FirstSub]
    Rmax              = fs['HaloData/Rmax'].value[FirstSub]

    BoxSize           = fs['Header/BoxSize'].value
    Redshift          = fs['Header/Redshift'].value
    a                 = 1 / (1 + Redshift)
    h = 0.6777

    fs.close()

    #simulation units to Mpc, km/s or 10^10M_sun
    CentreOfPotential *= a / h
    BulkVelocity      *= 1
    Vmax              *= 1
    Rmax              *= a / h
    BoxSize           *= 1 / h

    PosStars          *= a / h
    VelStars          *= np.sqrt(a)
    MassStars         *= 1 / h

    j_zonc, j_ponc, e_onmax = calculate_star_phase_space(PosStars, VelStars, MassStars,
                               CentreOfPotential, BulkVelocity,
                               BoxSize, Vmax, Rmax)

    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc', j_zonc)
    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_ponc', j_ponc)
    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'e_onmax', e_onmax)

  return(j_zonc, j_ponc, e_onmax)

def call_j_circ_gas(haloID):
  '''Load everything needed to make j_circ work
  '''
  #if already created file
  if os.path.isfile('../processed_data/L0012N0188/galaxy'
                    +str(haloID)+'j_zonc_gas.npy') and os.path.isfile(
                    '../processed_data/L0012N0188/galaxy'
                    +str(haloID)+'j_ponc_gas.npy') and os.path.isfile(
                    '../processed_data/L0012N0188/galaxy'
                    +str(haloID)+'e_onmax_gas.npy'):

    j_zonc  = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc_gas.npy')
    j_ponc  = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_ponc_gas.npy')
    e_onmax = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'e_onmax_gas.npy')

  else:
  #if file needs creating
    #settings
    Base  = '/home/matt/Documents/UWA/EAGLE/'
    Spec  = 'processed_data/028_z000p000'
    ident = '_12Mpc_188'

    #read processed hdf5 files
    #stars
    fn = Base+Spec+ident+'_Star.hdf5'
    fs = h5.File(fn,"r")

    mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]

    PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
    MassStars = fs['PartData/MassStar'].value[mask]
    VelStars  = fs['PartData/VelStar'].value[tuple(mask)]

    fs.close()

    #gas
    fn = Base+Spec+ident+'_Gas.hdf5'
    fs = h5.File(fn,"r")

    mask = [haloID == np.abs(fs['PartData/GrpNum_Gas'].value)]

    PosGas  = fs['PartData/PosGas'].value[tuple(mask)]
    MassGas = fs['PartData/MassGas'].value[mask]
    VelGas  = fs['PartData/VelGas'].value[tuple(mask)]
    TempGas = fs['PartData/TempGas'].value[mask]

    fs.close()

    #halo
    fn = Base+Spec+ident+'_halodat.hdf5'
    fs = h5.File(fn,"r")

    #Group
    CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]

    #Subgroup
    FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
    BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
    Vmax              = fs['HaloData/Vmax'].value[FirstSub]
    Rmax              = fs['HaloData/Rmax'].value[FirstSub]

    BoxSize           = fs['Header/BoxSize'].value
    Redshift          = fs['Header/Redshift'].value
    a                 = 1 / (1 + Redshift)
    h = 0.6777

    fs.close()

    #simulation units to Mpc, km/s or 10^10M_sun
    CentreOfPotential *= a / h
    BulkVelocity      *= 1
    Vmax              *= 1
    Rmax              *= a / h
    BoxSize           *= 1 / h

    PosStars          *= a / h
    VelStars          *= np.sqrt(a)
    MassStars         *= 1 / h

    PosGas            *= a / h
    VelGas            *= np.sqrt(a)
    MassGas           *= 1 / h
    TempGas           *= 1

    #only include gas modeled well
    mask = [TempGas < 10**4.5]
#    print('Passed=', np.sum(mask), ' Failed=', np.sum(1-np.array(mask)))

    PosGas  = PosGas[tuple(mask)]
    VelGas  = VelGas[tuple(mask)]
    MassGas = MassGas[mask]

    #do the tricky stuff
    #Center, apature, velocities
    PosStars, VelStars, MassStars = centre_on_galaxy_with_apature(PosStars, VelStars, MassStars,
                                                    CentreOfPotential, BulkVelocity, BoxSize)

    PosGas, VelGas, MassGas = centre_on_galaxy_with_apature_gas(PosStars, VelStars, MassStars,
                                                    PosGas, VelGas, MassGas,
                                                    CentreOfPotential, BulkVelocity, BoxSize)

    #get star angular momentum
    galaxy_angular_momentum = total_angular_momentum(PosStars, VelStars, np.transpose([MassStars]))

    #galaxy NFW properties
    rho_0, R_s = nfw_params_from_vels(Vmax, Rmax)

    #- particle binding energy / maximum binding energy
    e_onmax = -nfw_potential(rho_0, R_s, np.linalg.norm(PosGas, axis=1)) / nfw_potential_maximum(rho_0, R_s)
    e_onmax = e_onmax.value

    #projected specific angular momentum of each particle
    j_zs = z_projection_specific_angular_momentum(PosGas, VelGas, galaxy_angular_momentum)

    #perpendicular specific angular momentum of each particle
    j_ps = perpendicular_specific_angular_momentum(PosGas, VelGas, galaxy_angular_momentum)

    #specific energies of each particle
    energies = nfw_total_specific_energy(rho_0, R_s, PosGas, VelGas)

    #reject particles that are unbound to the halo
    #probably not the most elegant way to do this
#    max_energy = nfw_total_specific_energy(rho_0, R_s, [[70, 0, 0]]*u.kpc, [[0, nfw_velocity_circ(rho_0, R_s, 70*u.Mpc).value, 0]])
    mask = [energies < 0]
#    print('Passed=', np.sum(mask), ' Failed=', np.sum(1-np.array(mask)))

    energies = energies[mask]
    e_onmax  = e_onmax[mask]
    j_zs     = j_zs[mask]
    j_ps     = j_ps[mask]

#    PosGas   = PosGas[mask]
#    VelGas   = VelGas[mask]

    #circular radii from specific energy
    #this is the slow part. needs a for loop
    circular_radii = nfw_radius_circular_orbit_energy(rho_0, R_s, energies)

    #circular (maximum) specific angular momentum for particles with the same energy
    j_cs = nfw_specific_angular_momentum(rho_0, R_s, circular_radii)

    #j_z / j_c
    j_zonc = j_zs / j_cs
    j_zonc = j_zonc.value

    #j_p / j_c
    j_ponc = j_ps / j_cs
    j_ponc = j_ponc.value

    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_zonc_gas', j_zonc)
    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'j_ponc_gas', j_ponc)
    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'e_onmax_gas', e_onmax)

  return(j_zonc, j_ponc, e_onmax)

def get_star_formation(haloID):
  '''Get star formation redshift with the same mask.
  '''
  Base  = '/home/matt/Documents/UWA/EAGLE/'
  Spec  = 'processed_data/028_z000p000'
  ident = '_12Mpc_188'

  #read processed hdf5 files
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]

  PosStars         = fs['PartData/PosStar'].value[tuple(mask)]
  StellarFormation = fs['PartData/StellarFormationTime'].value[tuple(mask)]

  fs.close()

  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")

  #Group
  CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]

  #Subgroup
  BoxSize           = fs['Header/BoxSize'].value
  Redshift          = fs['Header/Redshift'].value
  a                 = 1 / (1 + Redshift)
  h = 0.6777

  fs.close()

  #units
  CentreOfPotential *= a / h
  PosStars          *= a / h

  StarFormation_z   = 1/StellarFormation - 1

  ######################################
  #do the mask
  rs = PosStars
  #unfold the dimeions
  rs -=CentreOfPotential-BoxSize/(2*h)
  rs %=(BoxSize/h)
  rs -=BoxSize/(2*h)
  # Compute distances
  distances = np.linalg.norm(rs ,axis=1)
  # Restrict particles to 30kpc aperture
  aperture = 0.03
  extract = (distances<aperture)
  ######################################

  StarFormation_z = StarFormation_z[extract]

  return(StarFormation_z)

def get_star_mass(haloID):
  '''Get star particle mass with the same mask.
  '''
  Base  = '/home/matt/Documents/UWA/EAGLE/'
  Spec  = 'processed_data/028_z000p000'
  ident = '_12Mpc_188'

  #read processed hdf5 files
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]

  PosStars         = fs['PartData/PosStar'].value[tuple(mask)]
  MassStars = fs['PartData/MassStar'].value[mask]

  fs.close()

  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")

  #Group
  CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]

  #Subgroup
  BoxSize           = fs['Header/BoxSize'].value
  Redshift          = fs['Header/Redshift'].value
  a                 = 1 / (1 + Redshift)
  h = 0.6777

  fs.close()

  #units
  CentreOfPotential *= a / h
  PosStars          *= a / h
  MassStars         *= 1 / h

  ######################################
  #do the mask
  rs = PosStars
  #unfold the dimeions
  rs -=CentreOfPotential-BoxSize/(2*h)
  rs %=(BoxSize/h)
  rs -=BoxSize/(2*h)
  # Compute distances
  distances = np.linalg.norm(rs ,axis=1)
  # Restrict particles to 30kpc aperture
  aperture = 0.03
  extract = (distances<aperture)
  ######################################

  MassStars = MassStars[extract]

  return(MassStars)


def get_gas_mass(haloID):
  '''Get gas particle mass with the same mask.
  '''
  #settings
  Base  = '/home/matt/Documents/UWA/EAGLE/'
  Spec  = 'processed_data/028_z000p000'
  ident = '_12Mpc_188'

  #read processed hdf5 files
  #stars
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]

  PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
  MassStars = fs['PartData/MassStar'].value[mask]
  VelStars  = fs['PartData/VelStar'].value[tuple(mask)]

  fs.close()

  #gas
  fn = Base+Spec+ident+'_Gas.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Gas'].value)]

  PosGas  = fs['PartData/PosGas'].value[tuple(mask)]
  MassGas = fs['PartData/MassGas'].value[mask]
  VelGas  = fs['PartData/VelGas'].value[tuple(mask)]
  TempGas = fs['PartData/TempGas'].value[mask]

  fs.close()

  #halo
  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")

  #Group
  CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]

  #Subgroup
  FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
  BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
  Vmax              = fs['HaloData/Vmax'].value[FirstSub]
  Rmax              = fs['HaloData/Rmax'].value[FirstSub]

  BoxSize           = fs['Header/BoxSize'].value
  Redshift          = fs['Header/Redshift'].value
  a                 = 1 / (1 + Redshift)
  h = 0.6777

  fs.close()

  #simulation units to Mpc, km/s or 10^10M_sun
  CentreOfPotential *= a / h
  BulkVelocity      *= 1
  Vmax              *= 1
  Rmax              *= a / h
  BoxSize           *= 1 / h

  PosStars          *= a / h
  VelStars          *= np.sqrt(a)
  MassStars         *= 1 / h

  PosGas            *= a / h
  VelGas            *= np.sqrt(a)
  MassGas           *= 1 / h
  TempGas           *= 1

  #only include gas modeled well
  mask = [TempGas < 10**4.5]
  #    print('Passed=', np.sum(mask), ' Failed=', np.sum(1-np.array(mask)))

  PosGas  = PosGas[tuple(mask)]
  VelGas  = VelGas[tuple(mask)]
  MassGas = MassGas[mask]

  #do the tricky stuff
  #Center, apature, velocities
  PosStars, VelStars, MassStars = centre_on_galaxy_with_apature(PosStars, VelStars, MassStars,
                                                  CentreOfPotential, BulkVelocity, BoxSize)

  PosGas, VelGas, MassGas = centre_on_galaxy_with_apature_gas(PosStars, VelStars, MassStars,
                                                  PosGas, VelGas, MassGas,
                                                  CentreOfPotential, BulkVelocity, BoxSize)

  #galaxy NFW properties
  rho_0, R_s = nfw_params_from_vels(Vmax, Rmax)

  #specific energies of each particle
  energies = nfw_total_specific_energy(rho_0, R_s, PosGas, VelGas)

  #reject particles that are unbound to the halo
  #probably not the most elegant way to do this
  #    max_energy = nfw_total_specific_energy(rho_0, R_s, [[70, 0, 0]]*u.kpc, [[0, nfw_velocity_circ(rho_0, R_s, 70*u.Mpc).value, 0]])
  mask = [energies < 0]
  #    print('Passed=', np.sum(mask), ' Failed=', np.sum(1-np.array(mask)))

  MassGas = MassGas[mask]

  return(MassGas)

################################################################################
#plotting scripts are out of date. See test-galaxies

def plot_j_zonc(haloID):
  '''
  '''
  j_zonc,_,_ = call_j_circ(haloID)
  j_zonc_gas,_,_ = call_j_circ_gas(haloID)

  m_star = get_star_mass(haloID)
  m_gas  = get_gas_mass(haloID)
  mass = np.sum(m_star)

  nstars = len(j_zonc)
  ngas   = len(j_zonc_gas)

  #fix binning
  j_zonc     = np.concatenate((j_zonc, [-1,1]))
  j_zonc_gas = np.concatenate((j_zonc_gas, [-1,1]))
  m_star     = np.concatenate((m_star, [1e-100, 1e-100]))
  m_gas      = np.concatenate((m_gas, [1e-100, 1e-100]))

  nbins  = np.max((np.min((int(np.round(nstars/40)) + 1, 50)),
                   np.min((int(np.round(ngas/40)) + 1, 50))))

  #Robin and Matias' package
#  splt.hist(j_zonc, hist_type='smooth', bins=int(np.round(len(j_zonc)/40)))

  plt.figure()
  plt.hist(j_zonc,     bins=nbins, weights=m_star, alpha=0.5)
  plt.hist(j_zonc_gas, bins=nbins, weights=m_gas, alpha=0.5)

  plt.legend(['Group number={0}\nnStarParticlesWithin30kpc={1}'.format(haloID, nstars),
              'nGasParticlesWithin70kpc={0}\n$M_*$={1} $* 10^{{10}} M_\odot$'.format(ngas, np.round(mass, 3))])
  plt.xlabel(r'$j_z / j_c$')
  plt.ylabel(r'Mass [$10^{10} M_\odot$]')
  plt.show()

  plt.savefig('../results/galaxy'+str(haloID)+'j_zonc.png')

def plot_hists(haloID, j_zonc, j_ponc, e_onmax, weights=1, alpha=1, save=False, for_gmm=False):
  '''
  '''
#  j_zonc, j_ponc, e_onmax = call_j_circ(haloID)

  nstars = len(j_ponc)
  if isinstance(weights, u.quantity.Quantity):
    weights = weights.to(10**10 * u.Msun).value

  #individual distributions
  fig, ax = plt.subplots(nrows=3, ncols=3, sharex='col')

  heights,edge_j_zonc = splt.hist(j_zonc, dens=False, scale=1/weights, c='grey',
                                  ax=ax[0,0], output=True,
                                  plabel='Total particles ='+str(len(j_zonc)))
#  ax[0,0].set_yticks([])
  ax[0,0].set_xlim([-1,1])
  ax[0,0].set_ylim([0,np.max(heights)*1.1])

  heights,edge_j_ponc = splt.hist(j_ponc, dens=False, scale=1/weights, c='grey',
                                  ax=ax[1,1], output=True)
#  ax[1,1].set_yticks([])
  ax[1,1].set_xlim([0,1])
  ax[1,1].set_ylim([0,np.max(heights)*1.1])

  heights,edge_e_onmax = splt.hist(e_onmax, dens=False, scale=1/weights, c='grey',
                                   ax=ax[2,2], output=True)
#  ax[2,2].set_yticks([])
  ax[2,2].set_ylim([0,np.max(heights)*1.1])

  ax[2,2].set_xlabel(r'$e / |e|_{max}$')

  #joint distributions
  #invisible plots to find correct normalization
  heights0,_,_ = splt.hist2D(j_zonc, j_ponc, clog=True, dens=False, scale=1/weights,
                           ax=ax[0,2], output=True, alpha=0)
  heights1,_,_ = splt.hist2D(j_zonc, e_onmax, clog=True, dens=False, scale=1/weights,
                           ax=ax[0,2], output=True, alpha=0)
  heights2,_,_ = splt.hist2D(j_ponc, e_onmax, clog=True, dens=False, scale=1/weights,
                           ax=ax[0,2], output=True, alpha=0)

  cmax = np.max((heights0, heights1, heights2))

  if not for_gmm:
    splt.hist2D(j_zonc, j_ponc, clog=True, dens=False, scale=1/weights,
                clim=[1,cmax], ax=ax[1,0], alpha=alpha)
  ax[1,0].set_ylabel(r'$j_p / j_c$')

  if not for_gmm:
    splt.hist2D(j_zonc, e_onmax, clog=True, dens=False, scale=1/weights,
                clim=[1,cmax], ax=ax[2,0], alpha=alpha)
  ax[2,0].set_xlabel(r'$j_z / j_c$')
  ax[2,0].set_ylabel(r'$e / |e|_{max}$')

  if not for_gmm:
    splt.hist2D(j_ponc, e_onmax, clog=True, dens=False, scale=1/weights,
                clim=[1,cmax], ax=ax[2,1], alpha=alpha)
  ax[2,1].set_yticks([])
  ax[2,1].set_xlabel(r'$j_p / j_c$')

  ax[0,0].legend(['Group number={0}\nnStarParticlesWithin30kpc={1}'.format(haloID, nstars)],
                 bbox_to_anchor=(1.5, 0., 0., 1.))

  fig.delaxes(ax[0,1])
  fig.delaxes(ax[0,2])
  fig.delaxes(ax[1,2])

  #just needs to go here
  ax[2,2].set_xlim([-1,np.max(edge_e_onmax)])

#  data = np.transpose(np.vstack([j_zonc, j_ponc, e_onmax]))
#  splt.cornerplot(data, pair_kw=dict(smooth=0.5))
#  splt.cornerplot(data, nsamples=500, pair_kw=dict(smooth=0.5))

  if save:
    plt.savefig('../results/'+str(haloID)+'_phase_space.png')

  if for_gmm:
    return(ax,edge_j_zonc,edge_j_ponc,edge_e_onmax)
  else:
    return

def plot_ellipses(ax, color, weights, means, covars):

#  for n in range(means.shape[0]):
    n=0
    weights=[weights]
    means  =[means]
    covars =[covars]

    eig_vals, eig_vecs = np.linalg.eigh(covars[n])
    unit_eig_vec = eig_vecs[0] / np.linalg.norm(eig_vecs[0])
    angle = np.arctan2(unit_eig_vec[1], unit_eig_vec[0])
    # Ellipse needs degrees
    angle = 180 * angle / np.pi
    # eigenvector normalization
    eig_vals = 2 * np.sqrt(2) * np.sqrt(eig_vals)
    ell = mpl.Ellipse(means[n], eig_vals[0], eig_vals[1],
                      180 + angle, edgecolor='black')
    ell.set_clip_box(ax.bbox)
#    ell.set_alpha(weights[n])
    ell.set_alpha(0.3)
    ell.set_facecolor(color)
    ax.add_artist(ell)

def plot_hists_components(haloID, comp_stars, j_zonc, j_ponc, e_onmax, weights=1, save=False):
  '''Same as plot hists but include the gmm visalization.
  '''
  #set up plots
  ax, edge_j_zonc,edge_j_ponc,edge_e_onmax = plot_hists(
      haloID, j_zonc, j_ponc, e_onmax, weights=weights, for_gmm=True)

  if isinstance(weights, u.quantity.Quantity):
    weights = weights.to(10**10 * u.Msun).value

  #keep axes
  x_0 = ax[0,0].get_xlim()
  y_0 = ax[0,0].get_ylim()
  x_1 = ax[1,1].get_xlim()
  y_1 = ax[1,1].get_ylim()
  x_2 = ax[2,2].get_xlim()
  y_2 = ax[2,2].get_ylim()

#  data = np.transpose(np.array([j_zonc, j_ponc, e_onmax]))
  choice = np.concatenate((np.zeros(comp_stars[0]), np.ones(comp_stars[1])))

  prop_cycle = plt.rcParams['axes.prop_cycle']
  colors = prop_cycle.by_key()['color']

  for i in range(len(comp_stars)):

    mask = choice==i
    col = colors[i]

    splt.hist(j_zonc[mask], dens=False, scale=1/weights,  bins=edge_j_zonc,  c=col,
              ax=ax[0,0], output=True, plabel='Component '+str(i)+' particles ='+str(sum(mask)))
    splt.hist(j_ponc[mask], dens=False, scale=1/weights,  bins=edge_j_ponc,  c=col,
              ax=ax[1,1], output=True)
    splt.hist(e_onmax[mask], dens=False, scale=1/weights, bins=edge_e_onmax, c=col,
              ax=ax[2,2], output=True)

    splt.contourp(j_zonc[mask], j_ponc[mask],  c=col, ax=ax[1,0], smooth=0.5)
    splt.contourp(j_zonc[mask], e_onmax[mask], c=col, ax=ax[2,0], smooth=0.5)
    splt.contourp(j_ponc[mask], e_onmax[mask], c=col, ax=ax[2,1], smooth=0.5)


  ax[0,0].legend(bbox_to_anchor=(1.5, 0., 0., 1.))

  #keep axes
  ax[0,0].set_xlim(x_0)
  ax[0,0].set_ylim(y_0)
  ax[1,1].set_xlim(x_1)
  ax[1,1].set_ylim(y_1)
  ax[2,2].set_xlim(x_2)
  ax[2,2].set_ylim(y_2)

  if save:
    plt.savefig('../results/'+str(haloID)+'_phase_space_input.png')

  return

def plot_hists_with_gmm(haloID, gmm, j_zonc, j_ponc, e_onmax, weights=1, save=False):
  '''Same as plot hists but include the gmm visalization.
  '''
  #set up plots
  ax, edge_j_zonc,edge_j_ponc,edge_e_onmax = plot_hists(
      haloID, j_zonc, j_ponc, e_onmax, weights=weights, for_gmm=True)

  if isinstance(weights, u.quantity.Quantity):
    weights = weights.to(10**10 * u.Msun).value

  #keep axes
  x_0 = ax[0,0].get_xlim()
  y_0 = ax[0,0].get_ylim()
  x_1 = ax[1,1].get_xlim()
  y_1 = ax[1,1].get_ylim()
  x_2 = ax[2,2].get_xlim()
  y_2 = ax[2,2].get_ylim()

  data = np.transpose(np.array([j_zonc, j_ponc, e_onmax]))
  choice, chances = my_gmm.gmm_random_points(gmm, data)

  prop_cycle = plt.rcParams['axes.prop_cycle']
  colors = prop_cycle.by_key()['color']

  for i in range(len(gmm.means_)):

    mask = choice==i
    col = colors[i]

    plot_ellipses(ax[1,0], col, gmm.weights_,
                  gmm.means_[i,0:2], gmm.covariances_[i,0:2,0:2])
    plot_ellipses(ax[2,0], col, gmm.weights_,
                  gmm.means_[i,[0,2]], gmm.covariances_[i,[0,2],0:3:2])
    plot_ellipses(ax[2,1], col, gmm.weights_,
                  gmm.means_[i,1:3], gmm.covariances_[i,1:3,1:3])

    splt.hist(j_zonc[mask], dens=False, scale=1/weights,  bins=edge_j_zonc,  c=col,
              ax=ax[0,0], output=True, plabel='Component '+str(i)+' particles ='+str(sum(mask)))
    splt.hist(j_ponc[mask], dens=False, scale=1/weights,  bins=edge_j_ponc,  c=col,
              ax=ax[1,1], output=True)
    splt.hist(e_onmax[mask], dens=False, scale=1/weights, bins=edge_e_onmax, c=col,
              ax=ax[2,2], output=True)

    splt.contourp(j_zonc[mask], j_ponc[mask],  c=col, ax=ax[1,0], smooth=0.5)
    splt.contourp(j_zonc[mask], e_onmax[mask], c=col, ax=ax[2,0], smooth=0.5)
    splt.contourp(j_ponc[mask], e_onmax[mask], c=col, ax=ax[2,1], smooth=0.5)


  ax[0,0].legend(bbox_to_anchor=(1.5, 0., 0., 1.))

  #keep axes
  ax[0,0].set_xlim(x_0)
  ax[0,0].set_ylim(y_0)
  ax[1,1].set_xlim(x_1)
  ax[1,1].set_ylim(y_1)
  ax[2,2].set_xlim(x_2)
  ax[2,2].set_ylim(y_2)

  if save:
    plt.savefig('../results/'+str(haloID)+'_phase_space_gmm.png')

  return

def plot_projection(haloID, gmm, data, PosStars, apature=True, save=False):
  '''
  '''
  choice, chances = my_gmm.gmm_random_points(gmm, data)

  n_comps = len(gmm.means_)
  choice, chances = my_gmm.gmm_random_points(gmm, data, seed=314159)

  if apature:
    distances = np.linalg.norm(PosStars ,axis=1)
    aperture = 0.03 * u.Mpc
    extract = (distances<aperture)
    PosStars = PosStars[extract].copy()
  PosStars = PosStars.value

  fig, ax = plt.subplots(nrows=2, ncols=n_comps, sharex=True, sharey=True, figsize=(n_comps*2, 4))

  bins = np.linspace(-0.03,0.03, 80)

  max_height = 0
  #find normalization
  for i in range(n_comps):

    heights,_,_ = splt.hist2D(PosStars[choice==i,0], PosStars[choice==i,2], bins=bins,
                          clog=True, ax=ax[0,i], output=True, alpha=0.00)

    if np.max(heights) > max_height:
      max_height = np.max(heights)

  #draw selection
  for i in range(n_comps):

    splt.hist2D(PosStars[choice==i,0], PosStars[choice==i,1], bins=bins,
                clog=True, clim=[10,max_height],
                ax=ax[0,i])
    splt.hist2D(PosStars[choice==i,0], PosStars[choice==i,2], bins=bins,
                clog=True, clim=[10,max_height],
                ax=ax[1,i])

  ax[0,0].set_ylabel('y [Mpc]')
  ax[1,0].set_ylabel('z [Mpc]')
  ax[1,0].set_xlabel('x [Mpc]')

  cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
  plt.colorbar(cax=cbar_ax)

  if save:
    plt.savefig('../results/'+str(haloID)+'_projection_gmm.png')

  return

def plot_ages(haloID):
  '''
  '''
  j_zonc,_,_ = call_j_circ(haloID)
  z_form = get_star_formation(haloID)

  nstars = len(j_zonc)

  plt.figure()
  splt.hist2D(z_form, j_zonc, clog=True, xlabel=r'$z_{formation}$', ylabel=r'$j_z / j_c$',
              title='Group number={0}\nN Star particles within 30kpc={1}'.format(haloID, nstars),
              clabel='Star particle count', dens=False)

  plt.savefig('../results/galaxy'+str(haloID)+'j_z_age.png')

if __name__ == '__main__':
  #the galaxis with n>1000
#  galIDs = np.concatenate((np.arange(1,17), np.arange(18,24,2)))

#  for i in np.arange(1,50):
#    plot_j_zonc(i)
#  galID = 7
#  plot_j_zonc(galID)

#  galID = 6
#  j_zonc, j_ponc, e_onmax = call_j_circ(galID)
#
#  gal_data = np.concatenate((j_ponc[:,np.newaxis], e_onmax[:,np.newaxis], j_zonc[:,np.newaxis]), axis=1)
#
#  my_gmm.gmm_and_selection(gal_data, 10)

  pass
