#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 14:04:14 2020

@author: matt
"""
import os
import h5py  as h5

import numpy as np

import matplotlib.pyplot as plt
import splotch as splt

import astropy.units as u
import astropy.constants as cst

from scipy.optimize import brentq

###############################################################################
###############################################################################
###############################################################################
#kappa
#Finds the proportion of kinetic energy that is rotating
#not done in the literature

def cylindrical_distance(r_vector, z_vector):
  '''cylindrical distance of r_vector in a plane perpendicular to z_vector.
  Result is in units of r_vector (hopefully)
  '''
  #normalized z vector
  z_hat  = z_vector / np.linalg.norm(z_vector)
  
  #(r dot z_hat) * z_hat. The projction of r onto z
  proj_z = np.matmul(np.dot(r_vector, z_hat).reshape(len(r_vector), 1), z_hat.reshape(1,3))
  
  #r - r projected onto z give a result in the x-y plane of the galaxy
  xy_vector = r_vector - proj_z
  
  Rs = np.linalg.norm(xy_vector, axis=1)
  
  return(Rs)
  
def specific_kinetic_energy(v_vector):
  '''specific kinetic energy
  '''
  k = 1/2 * np.linalg.norm(v_vector, axis=1) **2
  return(k)

###############################################################################
#general functions already made

def centre_on_galaxy_with_apature(r_vector, v_vector, masses, 
                                  CenterOfPotential, BulkVelocity, boxSize, h):
  '''Centers particles onto the centre of potential, cuts out a chosen apature
  adjusts the velocities to the galaxy frame.
  Probably doesn't need the bulk velocity step(s)
  centre_on_galaxy_with_apature(np.array(3,n), np.array(3,n), np.array(1,n), 
                                np.array(3), np.array(3), float, float) 
                                -> np.array(3,m), np.array(3,m), np.array(1,m)
  '''
  rs = np.array(r_vector.copy())
  vs = np.array(v_vector.copy())
  
  rs -=CenterOfPotential-boxSize/(2*h)
  rs %=(boxSize/h)
  rs -=boxSize/(2*h)
  
  # Center velocities
  #center velocities on the subhalo CoM velocity: the right center is done 
  #later after computing in-aperture CoM velocity
  vs -= BulkVelocity
  
  # Compute distances
  distances = np.linalg.norm(rs ,axis=1)
  
  # Restrict particles
  #dimensionfull equivalent to 30kpc aperture
  aperture = 0.03
  
  extract = (distances<aperture)
  
  r = rs[extract].copy()
  v = vs[extract].copy()
  m = masses[extract].copy()
  
  Mstar = np.sum(masses)
  
  # Compute 30kpc CoM to Sub CoM velocty offset & recenter
  #compute deviation between the in-aperture 30kpc CoM velocity and the current 
  #velocity center (i.e [0,0,0] but representing the subhalo CoM velocity)
  dvVmass = np.sum(np.dot(m,v),axis=0)/Mstar
  
  #re-center to correct velocity center (30kpc CoM velocity)
  v -= dvVmass
  
  return(r, v, m)

def total_angular_momentum(r_vector, v_vector, masses):
  '''total angular momentum of given particles. Units shouldn't matter?
  Useful for parties.
  '''
  angular_momentum = np.sum(np.cross(r_vector, v_vector) * masses, axis=0)
  
  return(angular_momentum)
  
def z_projection_specific_angular_momentum(r_vector, v_vector, j_vector):
  '''magnitude of a particle's the angular momentum projected onto the z 
  direction of total angular momentum.
  '''
  if not isinstance(r_vector, u.Quantity):
    r_vector *= u.Mpc
  if not isinstance(v_vector, u.Quantity):
    v_vector *= u.km / u.s
    
  #individual specific angular momentums
  js = np.cross(r_vector, v_vector)
  
  #projection of specific angular momentum in the j_total direction
  j_z = np.dot(js, j_vector) / np.linalg.norm(j_vector)
  
  return(j_z)

###############################################################################
#load and manage data

def call_kappa(haloID):
  '''Load everything needed to make kappa work
  '''
  #if already created file
  if os.path.isfile('../processed_data/L0012N0188/galaxy'+str(haloID)+'kappa.npy'):
    kappa = np.load('../processed_data/L0012N0188/galaxy'+str(haloID)+'kappa.npy')
    
  else:
  #if file needs creating
    #settings
    Base  = '/home/matt/Documents/UWA/EAGLE/'
    Spec  = 'processed_data/028_z000p000'
    ident = '_12Mpc_188'
    
    #read processed hdf5 files
    fn = Base+Spec+ident+'_Star.hdf5'
    fs = h5.File(fn,"r")
  
    mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
    
    PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
    MassStars = fs['PartData/MassStar'].value[tuple(mask)]
    VelStars  = fs['PartData/VelStar'].value[tuple(mask)]
    
    fs.close()
    
    fn = Base+Spec+ident+'_halodat.hdf5'
    fs = h5.File(fn,"r")
    
    #Group
    CentreOfPotential = fs['HaloData/GroupPos'].value[haloID-1]
    
    #Subgroup
    FirstSub          = fs['HaloData/FirstSub'].value[haloID-1]
    BulkVelocity      = fs['HaloData/Vbulk'].value[FirstSub]
    Vmax              = fs['HaloData/Vmax'].value[FirstSub]
    Rmax              = fs['HaloData/Rmax'].value[FirstSub]
    
    BoxSize           = fs['Header/BoxSize'].value
    Redshift          = fs['Header/Redshift'].value
    a                 = 1 / (1 + Redshift)
    h = 0.6777
    
    fs.close()
    
    #simulation units to Mpc, km/s or 10^10M_sun
    CentreOfPotential *= a / h
    BulkVelocity      *= 1
    Vmax              *= 1
    Rmax              *= a / h
    
    PosStars          *= a / h
    VelStars          *= np.sqrt(a)
    MassStars         *= 1 / h
    
    #do the tricky stuff
    #Center, apature, velocities
    PosStars, VelStars, MassStars = centre_on_galaxy_with_apature(PosStars, VelStars, MassStars, 
                                                    CentreOfPotential, BulkVelocity, BoxSize, h)
    
    #get galaxy angular momentum
    galaxy_angular_momentum = total_angular_momentum(PosStars, VelStars, np.transpose([MassStars]))
    
    #projected specific angular momentum of each particle
    j_zs = z_projection_specific_angular_momentum(PosStars, VelStars, galaxy_angular_momentum)
    
    #units
    Rs   = cylindrical_distance(PosStars *u.Mpc, galaxy_angular_momentum)
    
    #kinetic energy
    ks   = specific_kinetic_energy(VelStars *u.km /u.s)

    #numerator    
    k_rot = (j_zs / Rs)**2 / 2 #* np.sign(j_zs)
    
    kappa = k_rot / ks
    kappa = kappa.value
    
#    np.save('../processed_data/L0012N0188/galaxy'+str(haloID)+'kappa', kappa)
  
  return(kappa)

def plot_kappa(haloID):
  '''
  '''
  kappa = call_kappa(haloID)
  
  nstars = len(kappa)
  nbins  = np.amin((50, int(np.round(nstars/40)) + 1))
  
  #Robin and Matias' package
#  splt.hist(j_zonc, hist_type='smooth', bins=int(np.round(len(j_zonc)/40)))
  
  plt.figure()
  plt.hist(kappa, bins=nbins)
  
  plt.legend(['Group number={0}\nnStarParticlesWithin30kpc={1}'.format(haloID, nstars)])
  plt.xlabel(r'$\kappa$')
  plt.ylabel(r'Star particle count')
  plt.show()
  
  plt.savefig('../results/galaxy'+str(haloID)+'kappa.png')

if __name__ == '__main__':
  
  plot_kappa(5)
  
#  galIDs = np.concatenate((np.arange(1,17), np.arange(18,24,2)))
  
#  for haloID in galIDs:
#    plot_j_zonc(haloID)
  
  pass