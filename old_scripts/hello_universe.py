#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 11:21:58 2020

@author: matt
"""

import h5py  as h5
import numpy as np

import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D
import matplotlib._color_data as mcd

Base  = '/home/matt/Documents/UWA/EAGLE/'
Spec  = 'processed_data/028_z000p000'
ident = '_12Mpc_188'

def plot_xy(haloID=100):

  #read processed hdf5 files
  #DM
  fn = Base+Spec+ident+'_DM.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_DM'].value)]
  PosDMs = fs['PartData/PosDM'].value[tuple(mask)].transpose()
  
  fs.close()
  
  #Gas
  fn = Base+Spec+ident+'_Gas.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Gas'].value)]
  PosGass = fs['PartData/PosGas'].value[tuple(mask)].transpose()
  
  fs.close()
  
  #Star
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
  PosStars = fs['PartData/PosStar'].value[tuple(mask)].transpose()
  
  fs.close()
  
  #plot
  plt.errorbar(PosDMs[0],   PosDMs[1],   ls='', fmt='.', c='k', label='DM')
  plt.errorbar(PosGass[0],  PosGass[1],  ls='', fmt='.', c='g', label='Gas')
  plt.errorbar(PosStars[0], PosStars[1], ls='', fmt='.', c='r', label='Star')
  
  plt.legend()
  plt.xlabel('X')
  plt.ylabel('Y')
  
def plot_Star_xyz(haloID=300, HubbleParam=67.77, ScaleFactor=1):
  '''
  
  plot_Star_xyz(int) -> graph
  '''
  
  #read processed hdf5 files
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
  
  PosStars  = fs['PartData/PosStar'].value[tuple(mask)]
  VelStars  = fs['PartData/VelStar'].value[tuple(mask)].transpose()
  MassStars = fs['PartData/MassStar'].value[tuple(mask)]
  
  fs.close()
  
  fn = Base+Spec+ident+'_halodat.hdf5'
  fs = h5.File(fn,"r")

  #only central galaxies
  CentrePotential = fs['HaloData/GroupPos'].value[haloID-1]
  
  print('Centre of potential = ', CentrePotential)
  
  #Average momentum
  print('Total momentum of stars = ', np.sum(np.multiply(MassStars, VelStars), axis=1))
  
  
  #Restrict to 30kpc
  PosStars = np.subtract(PosStars, CentrePotential)
  
  StarDistances = np.linalg.norm(PosStars,axis=1)
  
  DistanceMask = (StarDistances < 0.03 * 0.6777 / ScaleFactor)
  
  PosStars  = PosStars[DistanceMask].transpose()
  VelStars  = VelStars.transpose()[DistanceMask].transpose()
  MassStars = MassStars[DistanceMask]
  
#  VelStars = np.subtract(VelStars.transpose(), 10 * CentrePotential).transpose()
  
  #Corrected momentum
#  print('Corrected momentum = ', np.sum(np.multiply(MassStars, VelStars), axis=1))
  
  fs.close()
  
  #plotting here  
  fig = plt.figure()
  ax1 = fig.add_subplot(111, projection='3d')
  
  ax1.scatter(PosStars[0], PosStars[1], PosStars[2])  
  ax1.quiver(PosStars[0], PosStars[1], PosStars[2], 
             VelStars[0], VelStars[1], VelStars[2], length=3e-5)
  
  ax1.set_xlabel('X')
  ax1.set_ylabel('Y')
  ax1.set_zlabel('Z')
  
  #NonImplementedError
#  ax1.set_aspect('equal')

  max_sep = np.amax(np.amax(PosStars, axis=1) - np.amin(PosStars, axis=1)) / 2
  centers = np.mean(PosStars, axis=1)
  
  axes = plt.gca()
  axes.set_xlim([centers[0]-max_sep,centers[0]+max_sep])
  axes.set_ylim([centers[1]-max_sep,centers[1]+max_sep])
  axes.set_zlim([centers[2]-max_sep,centers[2]+max_sep])
  
def plot_DM_subhalo_3d(haloID=50):

  #read processed hdf5 files
  fn = Base+Spec+ident+'_DM.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_DM'].value)]
  
  subhalos = fs['PartData/SubNum_DM'].value[tuple(mask)]
  
  PosDMs = fs['PartData/PosDM'].value[tuple(mask)].transpose()
  
  fs.close()
  
  #plotting here  
  fig = plt.figure()
  ax1 = fig.add_subplot(111, projection='3d')
  
  for subnum in np.unique(subhalos):
    
    ax1.scatter(PosDMs[0][subhalos == subnum], 
                PosDMs[1][subhalos == subnum], 
                PosDMs[2][subhalos == subnum], label=str(subnum))
  
  ax1.legend()
  ax1.set_xlabel('X')
  ax1.set_ylabel('Y')
  ax1.set_zlabel('Z')
  
  #NonImplementedError
#  ax1.set_aspect('equal')

  max_sep = np.amax(np.amax(PosDMs, axis=1) - np.amin(PosDMs, axis=1)) / 2
  centers = np.mean(PosDMs, axis=1)
  
  axes = plt.gca()
  axes.set_xlim([centers[0]-max_sep,centers[0]+max_sep])
  axes.set_ylim([centers[1]-max_sep,centers[1]+max_sep])
  axes.set_zlim([centers[2]-max_sep,centers[2]+max_sep])
  
def plot_subhalo(haloID=50, L=8.47125):

  #read processed hdf5 files
  #DM
  fn = Base+Spec+ident+'_DM.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_DM'].value)]
  
  PosDMs = fs['PartData/PosDM'].value[tuple(mask)].transpose()
  subhaloDM = fs['PartData/SubNum_DM'].value[tuple(mask)]
  
  #Gas
  fn = Base+Spec+ident+'_Gas.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Gas'].value)]
  
  PosGass = fs['PartData/PosGas'].value[tuple(mask)].transpose()
  subhaloGas = fs['PartData/SubNum_Gas'].value[tuple(mask)]
  
  #Star
  fn = Base+Spec+ident+'_Star.hdf5'
  fs = h5.File(fn,"r")

  mask = [haloID == np.abs(fs['PartData/GrpNum_Star'].value)]
  
  PosStars = fs['PartData/PosStar'].value[tuple(mask)].transpose()
  subhaloStar = fs['PartData/SubNum_Star'].value[tuple(mask)]
  
  fs.close()
  
  #Choice
  x=0
  y=1
  
  #Plotting
  plt.figure(figsize=(8,9))
  colour_list = list(mcd.XKCD_COLORS)
  
  #DM
  ax00 = plt.subplot(321)
#  ax00.scatter(PosDMs[x][subhaloDM == 0] - L*np.array([PosDMs[x][subhaloDM == 0] > L/2]),
#               PosDMs[y][subhaloDM == 0] - L*np.array([PosDMs[y][subhaloDM == 0] > L/2]),
#               s=1)
  ax00.scatter(PosDMs[x][subhaloDM == 0], PosDMs[y][subhaloDM == 0], s=1)
  ax00.set_aspect('equal')
  ax00.set_ylabel('Dark Matter Particles')
  
  ax01 = plt.subplot(322, sharex=ax00, sharey=ax00)
  for subnum in np.flip(np.unique(subhaloDM)[1:]):
#    ax01.scatter(PosDMs[x][subhaloDM == subnum] - L*np.array([PosDMs[x][subhaloDM == subnum] > L/2]),
#                 PosDMs[y][subhaloDM == subnum] - L*np.array([PosDMs[y][subhaloDM == subnum] > L/2]), 
#                 label=str(subnum), s=1, c=colour_list[subnum%949])
    ax01.scatter(PosDMs[x][subhaloDM == subnum], PosDMs[y][subhaloDM == subnum], 
                 label=str(subnum), s=1, c=colour_list[subnum%949])
  
  ax01.legend(bbox_to_anchor=(1.7, -.75), loc='center right')
  ax01.set_aspect('equal')
  
  #Gas
  ax10 = plt.subplot(323, sharex=ax00, sharey=ax00)
#  ax10.scatter(PosGass[x][subhaloGas == 0] - L*np.array([PosGass[x][subhaloGas == 0] > L/2]),
#               PosGass[y][subhaloGas == 0] - L*np.array([PosGass[y][subhaloGas == 0] > L/2]),
#               s=1)
  ax10.scatter(PosGass[x][subhaloGas == 0], PosGass[y][subhaloGas == 0], s=1)
  ax10.set_aspect('equal')
  ax10.set_ylabel('Gas Particles')
  
  ax11 = plt.subplot(324, sharex=ax00, sharey=ax00)
  for subnum in np.flip(np.unique(subhaloGas)[1:]):
#    ax11.scatter(PosGass[x][subhaloGas == subnum] - L*np.array([PosGass[x][subhaloGas == subnum] > L/2]),
#                 PosGass[y][subhaloGas == subnum] - L*np.array([PosGass[y][subhaloGas == subnum] > L/2]), 
#                 label=str(subnum), s=1, c=colour_list[subnum%949])
    ax11.scatter(PosGass[x][subhaloGas == subnum], PosGass[y][subhaloGas == subnum], 
                 label=str(subnum), s=1, c=colour_list[subnum%949])
  
  ax11.set_aspect('equal')
  
  #Stars
  ax20 = plt.subplot(325, sharex=ax00, sharey=ax00)
#  ax20.scatter(PosStars[x][subhaloStar == 0] - L*np.array([PosStars[x][subhaloStar == 0] > L/2]),
#               PosStars[y][subhaloStar == 0] - L*np.array([PosStars[y][subhaloStar == 0] > L/2]),
#               s=1)
  ax20.scatter(PosStars[x][subhaloStar == 0], PosStars[y][subhaloStar == 0], s=1)
  ax20.set_aspect('equal')
  ax20.set_xlabel('Primary Subgroup')
  ax20.set_ylabel('Star Particles')
  
  ax21 = plt.subplot(326, sharex=ax00, sharey=ax00)
  for subnum in np.flip(np.unique(subhaloStar)[1:]):
#    ax21.scatter(PosStars[x][subhaloStar == subnum] - L*np.array([PosStars[x][subhaloStar == subnum] > L/2]),
#                 PosStars[y][subhaloStar == subnum] - L*np.array([PosStars[y][subhaloStar == subnum] > L/2]), 
     ax21.scatter(PosStars[x][subhaloStar == subnum], PosStars[y][subhaloStar == subnum], 
                 label=str(subnum), s=1, c=colour_list[subnum%949])
  
  ax21.set_aspect('equal')
  ax21.set_xlabel('Other Subgroup')
  
  plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
  
  plt.savefig('../results/galaxy'+str(haloID)+'.png')
  
if __name__ == '__main__':
  
  plot_subhalo(23)
  plot_Star_xyz(23)