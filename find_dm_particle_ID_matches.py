#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

import h5py as h5

# from scipy.integrate         import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

from scipy.optimize import linprog

from scipy.spatial import Delaunay

import ctypes

import multiprocessing as mp

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
BOX_SIZE = 33.885 #50 * 0.6777 #kpc
BOX_HALF = 16.9425
INTERPARTICLE_SPACE = 0.04505984042553191 # 33.885 / 752
# SCALE_A  = 1
# DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

NO_GROUP_NUMBER = 1073741824 #2**30

PEANO_SO_LOC = '/home/mwilkinson/EAGLE/scripts/'
# PEANO_SO_LOC = '/home/mwilkins/EAGLE/scripts/'

VERBOSE = True

def my_read(particle_data_location, halo_data_location, kdtree_location, snap, bits):

    fn = f'{halo_data_location}groups_{snap}/eagle_subfind_tab_{snap}.0.hdf5'
    start_time = time.time()
    if VERBOSE: print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    # Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    # GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    # FirstSub = np.zeros(TotNgroups, dtype=np.int32)

    # Subhalo arrays
    GroupNumber = np.zeros(TotNsubgroups, dtype=np.int32)
    SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int32)
    SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    NGrp_c = 0
    NSub_c = 0

    if VERBOSE: print('TotNGroups:', TotNgroups)
    if VERBOSE: print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = f'{particle_data_location}groups_{snap}/eagle_subfind_tab_{snap}.{str(ifile)}.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                # Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]
                # GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                # FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            if Nsubgroups > 0:
                GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
                SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]
                SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]

                NSub_c += Nsubgroups

    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    if VERBOSE: print('Loaded halos')

    #particles
    fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.0.hdf5'
    start_time = time.time()
    if VERBOSE: print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs
        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    if VERBOSE: print('NumPartTot:', NumPartTot)

    # PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    IDDM = np.zeros(NumPartTot[1], dtype=np.uint64)
    GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    SubNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    BindingEnergyDM = np.zeros(NumPartTot[1], dtype=np.float32)

    NDM_c = 0

    for ifile in range(FNumPerSnap):

        fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.{str(ifile)}.hdf5'
        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[1] > 0:

                # PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"][()]
                SubNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/SubGroupNumber"][()]
                IDDM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/ParticleIDs"][()]
                BindingEnergyDM[NDM_c:NDM_c + NumParts[1]] = fs["/PartType1/ParticleBindingEnergy"][()]

                NDM_c += NumParts[1]

    if VERBOSE: print(NDM_c)

    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    if VERBOSE: print('loaded particles')

    #t=0 positions from IDs
    #needs snap so that order matches
    IC_positions_decoded_file_name = f'{kdtree_location}{snap}_ID_decoded_positions.hdf5'
    start_time = time.time()
    if VERBOSE: print('IC positions decoded name ', IC_positions_decoded_file_name)

    if os.path.exists(IC_positions_decoded_file_name):
        if VERBOSE: print('Loading')

        with h5.File(IC_positions_decoded_file_name, "r") as fs:
            PosDM_ICs = fs["PartType1/ICCoordinates"][()]
            IC_IDDMs = fs["PartType1/ParticleIDs"][()]

        # if not np.all(IC_IDDMs == IDDM):
        #     raise ValueError('IDs do not mactch!')

        if VERBOSE: print('Opened IC positions')

    else:
        if VERBOSE: print('Calculating')
        #make tree
        PosDM_ICs = decode_peano_ids(IDDM, bits)
        if VERBOSE: print('Decoded IC positions in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with h5.File(IC_positions_decoded_file_name, "w") as fs:
            quant = fs.create_group('PartType1')
            quant.create_dataset('ICCoordinates', data=PosDM_ICs)
            quant.create_dataset('ParticleIDs', data=IDDM)
        if VERBOSE: print('Saved dm decoded IC positions.')
    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    #t=0 kdtree
    #needs snap so that order matches
    dm_tree_file_name = f'{kdtree_location}{snap}_t0_dm_tree.pickle'
    start_time = time.time()
    if VERBOSE: print('DM tree file name ', dm_tree_file_name)

    if os.path.exists(dm_tree_file_name):
        if VERBOSE: print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            t0_dm_kd_tree = pickle.load(dm_tree_file)
        if VERBOSE: print('Opened dm kdtree')

    else:
        if VERBOSE: print('Calculating')
        #make tree
        t0_dm_kd_tree = cKDTree(PosDM_ICs, leafsize=10, boxsize=BOX_SIZE)
        if VERBOSE: print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(t0_dm_kd_tree, dm_tree_file, protocol=4)
        if VERBOSE: print('Saved dm kdtree.')
    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    if VERBOSE: print('Done t=0 kdtree')

    #t=N kdtree
    dm_tree_file_name = f'{kdtree_location}{snap}_dm_tree.pickle'
    start_time = time.time()
    if VERBOSE: print('DM tree file name ', dm_tree_file_name)

    if os.path.exists(dm_tree_file_name):
        if VERBOSE: print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            tn_dm_kd_tree = pickle.load(dm_tree_file)
        print('Opened dm kdtree')

    else:
        if VERBOSE: print('Calculating')
        #load positions in EAGLE units
        PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
        NDM_c = 0
        for ifile in range(FNumPerSnap):
            fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.{str(ifile)}.hdf5'
            with h5.File(fn, "r") as fs:
                Header = fs['Header'].attrs
                NumParts = Header['NumPart_ThisFile']
                if NumParts[1] > 0:
                    PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                    NDM_c += NumParts[1]
        #make tree
        tn_dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
        if VERBOSE: print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(tn_dm_kd_tree, dm_tree_file, protocol=4)
        if VERBOSE: print('Saved dm kdtree.')
    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    if VERBOSE: print('Done t=N kdtree')

    if VERBOSE: print('loaded eveything')

    return (TotNgroups, Group_R_Crit200,
            TotNsubgroups, GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
            tn_dm_kd_tree, t0_dm_kd_tree,
            IDDM, GrpNum_DM, SubNum_DM, BindingEnergyDM, PosDM_ICs)


def decode_peano_ids(ids, bits, BOX_SIZE=BOX_SIZE, so_loc=PEANO_SO_LOC):

    n = len(ids)

    key = (ctypes.c_uint64 * n)(*ids)

    c_peano_hilbert_key_inverse = ctypes.CDLL(f'{so_loc}peano_for_py.so').peano_hilbert_key_inverse_

    x_array = (ctypes.c_int64 * n)(*[0] * n)
    y_array = (ctypes.c_int64 * n)(*[0] * n)
    z_array = (ctypes.c_int64 * n)(*[0] * n)

    c_peano_hilbert_key_inverse(ctypes.byref(key), ctypes.c_int64(bits),
                                ctypes.byref(x_array), ctypes.byref(y_array), ctypes.byref(z_array),
                                ctypes.c_int64(n))

    x_np = np.array(x_array)
    y_np = np.array(y_array)
    z_np = np.array(z_array)

    pos_ics = np.vstack((x_np, y_np, z_np)).T / (2 ** bits) * BOX_SIZE

    pos_ics = pos_ics.astype(np.float32)

    return pos_ics


def vec_pacman_dist2(u, v):
    dx = np.abs(u[:, 0] - v[:, 0])
    dy = np.abs(u[:, 1] - v[:, 1])
    dz = np.abs(u[:, 2] - v[:, 2])
    dxm = dx > BOX_HALF
    dym = dy > BOX_HALF
    dzm = dz > BOX_HALF
    dx[dxm] = BOX_SIZE - dx[dxm]
    dy[dym] = BOX_SIZE - dy[dym]
    dz[dzm] = BOX_SIZE - dz[dzm]

    return dx * dx + dy * dy + dz * dz


def my_compare_ids(TotNgroups0, Group_R_Crit200_0,
                   TotNsubgroups0, GroupNumber0, SubGroupNumber0, SubGroupCentreOfPotential0,
                   tn_dm_kd_tree0, t0_dm_kd_tree0,
                   IDDM0, GrpNum_DM0, SubNum_DM0, BindingEnergyDM0, PosDM_ICs0,
                   TotNgroups1, Group_R_Crit200_1,
                   TotNsubgroups1, GroupNumber1, SubGroupNumber1, SubGroupCentreOfPotential1,
                   tn_dm_kd_tree1, t0_dm_kd_tree1,
                   IDDM1, GrpNum_DM1, SubNum_DM1, BindingEnergyDM1, PosDM_ICs1,
                   nbound=100, nmin=50):

    group_number_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    group_number_one  = np.zeros(TotNsubgroups1, dtype=np.int32)

    sub_number_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    sub_number_one  = np.zeros(TotNsubgroups1, dtype=np.int32)

    most_matching_ids_one_to_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    most_matching_ids_zero_to_one = np.zeros(TotNsubgroups1, dtype=np.int32)

    sub_matching_ids_one_to_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    sub_matching_ids_zero_to_one = np.zeros(TotNsubgroups1, dtype=np.int32)

    numb_matching_ids_one_to_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    numb_matching_ids_zero_to_one = np.zeros(TotNsubgroups1, dtype=np.int32)

    numb_sub_matching_ids_one_to_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    numb_sub_matching_ids_zero_to_one = np.zeros(TotNsubgroups1, dtype=np.int32)

    numb_particle_match_fails_zero = np.zeros(TotNsubgroups0, dtype=np.int32)
    numb_particle_match_fails_one  = np.zeros(TotNsubgroups1, dtype=np.int32)

    for sub_index in range(TotNsubgroups0):

        group_number_zero[sub_index] = GroupNumber0[sub_index]
        sub_number_zero[sub_index] = SubGroupNumber0[sub_index]

        r200 = Group_R_Crit200_0[group_number_zero[sub_index]-1]  # eagle units
        centre_of_potential = SubGroupCentreOfPotential0[sub_index]

        dm_index_mask = tn_dm_kd_tree0.query_ball_point(x=centre_of_potential, r=min(r200, 0.1))
        dm_index_mask = np.array(dm_index_mask)

        best_group = NO_GROUP_NUMBER
        best_sub = NO_GROUP_NUMBER

        #halo has more than nmin DM particles
        if len(dm_index_mask) <= nmin:
            pass

        else:
            sub_mask = dm_index_mask[SubNum_DM0[dm_index_mask] == sub_number_zero[sub_index]]

            if len(sub_mask) <= nmin:
                pass

            #do the calc
            else:
                PosDMs0 = PosDM_ICs0[sub_mask, :]

                if nbound is not None:
                    EnergyDMs = BindingEnergyDM0[sub_mask]
                    PosDMs0 = PosDMs0[np.argsort(EnergyDMs)[:min(nbound, len(sub_mask))], :]

                #group and subgroup numbers of the closest matched particles particles
                group_numbers = NO_GROUP_NUMBER * np.ones(np.shape(PosDMs0)[0], dtype=np.int32)
                sub_numbers = NO_GROUP_NUMBER * np.ones(np.shape(PosDMs0)[0], dtype=np.int32)

                for i_particle, particle_in_halo0 in enumerate(PosDMs0):
                    close_mask = t0_dm_kd_tree1.query_ball_point(x=particle_in_halo0, r=INTERPARTICLE_SPACE)

                    if len(close_mask) < 1:
                        group_numbers[i_particle] = NO_GROUP_NUMBER
                        sub_numbers[i_particle] = NO_GROUP_NUMBER
                        numb_particle_match_fails_zero[sub_index] += 1

                    else:
                        ClosePosDMs1 = PosDM_ICs1[close_mask, :]

                        dist2s = vec_pacman_dist2(particle_in_halo0[np.newaxis, :], ClosePosDMs1)
                        small_r_arg = np.argmin(dist2s)

                        group_numbers[i_particle] = GrpNum_DM1[close_mask[small_r_arg]]
                        sub_numbers[i_particle] = SubNum_DM1[close_mask[small_r_arg]]

                if VERBOSE:
                    if (sub_index % 100 == 0 or sub_index < 10) and len(PosDMs0) > 0:
                        print(f'N within interparticle space {len(close_mask)}')

                gns, counts = np.unique(np.abs(group_numbers), return_counts=True)

                if VERBOSE:
                    if sub_index % 100 == 0 or sub_index < 10:
                        print(gns)
                        print(counts)

                #remove ungrouped partices
                if NO_GROUP_NUMBER in gns:
                    gns = gns[:-1]
                    counts = counts[:-1]

                if len(counts) > 0:
                    best_group = gns[np.argmax(counts)]
                    numb_matching_ids_one_to_zero[sub_index] = np.amax(counts)
                else:
                    best_group = NO_GROUP_NUMBER

                sns, counts = np.unique(np.abs(sub_numbers[np.abs(group_numbers) == best_group]), return_counts=True)

                if VERBOSE:
                    if sub_index % 100 == 0 or sub_index < 10:
                        print(sns)
                        print(counts)

                if len(counts) > 0:
                    best_sub = sns[np.argmax(counts)]
                    numb_sub_matching_ids_one_to_zero[sub_index] = np.amax(counts)
                else:
                    best_sub = NO_GROUP_NUMBER

        if VERBOSE:
            if sub_index % 100 == 0 or sub_index < 10:
                print(best_group, best_sub)

        most_matching_ids_one_to_zero[sub_index] = best_group
        sub_matching_ids_one_to_zero[sub_index] = best_sub

                # except Exception as e:
        #     print('Exception was thrown calculating halo properties for group: ', sub_index)
        #     print(e)
        #     pass

        if VERBOSE:
            if sub_index % 100 == 0 or sub_index < 10:
                print(f'{group_number_zero[sub_index]}, {sub_number_zero[sub_index]}.')
                print(f'0 {sub_index} / {TotNsubgroups0}.')

    #the other way
    for sub_index in range(TotNsubgroups1):
        group_number_one[sub_index] = GroupNumber1[sub_index]
        sub_number_one[sub_index] = SubGroupNumber1[sub_index]

        r200 = Group_R_Crit200_1[group_number_one[sub_index]-1]  # eagle units
        centre_of_potential = SubGroupCentreOfPotential1[sub_index]

        dm_index_mask = tn_dm_kd_tree1.query_ball_point(x=centre_of_potential, r=min(r200, 0.1))
        dm_index_mask = np.array(dm_index_mask)

        best_group = NO_GROUP_NUMBER
        best_sub = NO_GROUP_NUMBER

        if len(dm_index_mask) <= nmin:
            pass

        else:
            sub_mask = dm_index_mask[SubNum_DM1[dm_index_mask] == sub_number_one[sub_index]]

            if len(sub_mask) <= nmin:
                pass

            #do the calc
            else:
                PosDMs1 = PosDM_ICs1[sub_mask, :]

                if nbound is not None:
                    EnergyDMs = BindingEnergyDM1[sub_mask]
                    PosDMs1 = PosDMs1[np.argsort(EnergyDMs)[:min(nbound, len(sub_mask))], :]

                #group and subgroup numbers of the closest matched particles particles
                group_numbers = NO_GROUP_NUMBER * np.ones(np.shape(PosDMs1)[0], dtype=np.int32)
                sub_numbers = NO_GROUP_NUMBER * np.ones(np.shape(PosDMs1)[0], dtype=np.int32)

                for i_particle, particle_in_halo1 in enumerate(PosDMs1):
                    close_mask = t0_dm_kd_tree0.query_ball_point(x=particle_in_halo1, r=INTERPARTICLE_SPACE)

                    if len(close_mask) < 1:
                        group_numbers[i_particle] = NO_GROUP_NUMBER
                        sub_numbers[i_particle] = NO_GROUP_NUMBER
                        numb_particle_match_fails_one[sub_index] += 1

                    else:
                        ClosePosDMs0 = PosDM_ICs0[close_mask, :]

                        dist2s = vec_pacman_dist2(particle_in_halo1[np.newaxis, :], ClosePosDMs0)
                        small_r_arg = np.argmin(dist2s)

                        group_numbers[i_particle] = GrpNum_DM0[close_mask[small_r_arg]]
                        sub_numbers[i_particle] = SubNum_DM0[close_mask[small_r_arg]]

                if VERBOSE:
                    if (sub_index % 100 == 0 or sub_index < 10) and len(PosDMs1) > 0:
                        print(f'N within interparticle space {len(close_mask)}')

                gns, counts = np.unique(np.abs(group_numbers), return_counts=True)

                if VERBOSE:
                    if sub_index % 100 == 0 or sub_index < 10:
                        print(gns)
                        print(counts)

                #remove ungrouped partices
                if NO_GROUP_NUMBER in gns:
                    gns = gns[:-1]
                    counts = counts[:-1]

                if len(counts) > 0:
                    best_group = gns[np.argmax(counts)]
                    numb_matching_ids_zero_to_one[sub_index] = np.amax(counts)
                else:
                    best_group = NO_GROUP_NUMBER

                sns, counts = np.unique(np.abs(sub_numbers[np.abs(group_numbers) == best_group]), return_counts=True)

                if VERBOSE:
                    if sub_index % 100 == 0 or sub_index < 10:
                        print(sns)
                        print(counts)

                if len(counts) > 0:
                    best_sub = sns[np.argmax(counts)]
                    numb_sub_matching_ids_zero_to_one[sub_index] = np.amax(counts)
                else:
                    best_sub = NO_GROUP_NUMBER

        if VERBOSE:
            if sub_index % 100 == 0 or sub_index < 10:
                print(best_group, best_sub)

        most_matching_ids_zero_to_one[sub_index] = best_group
        sub_matching_ids_zero_to_one[sub_index] = best_sub

                # except Exception as e:
        #     print('Exception was thrown calculating halo properties for group: ', sub_index)
        #     print(e)
        #     pass

        if VERBOSE:
            if sub_index % 100 == 0 or sub_index < 10:
                print(f'{group_number_one[sub_index]}, {sub_number_one[sub_index]}.')
                print(f'1 {sub_index} / {TotNsubgroups1}.')

    return (group_number_zero, sub_number_zero, most_matching_ids_one_to_zero, sub_matching_ids_one_to_zero,
            numb_matching_ids_one_to_zero, numb_sub_matching_ids_one_to_zero, numb_particle_match_fails_zero,
            group_number_one,  sub_number_one,  most_matching_ids_zero_to_one, sub_matching_ids_zero_to_one,
            numb_matching_ids_zero_to_one, numb_sub_matching_ids_zero_to_one, numb_particle_match_fails_one)


def my_write(output_data_location, snap,
             group_number_zero, sub_number_zero, most_matching_ids_one_to_zero, sub_matching_ids_one_to_zero,
             numb_matching_ids_one_to_zero, numb_sub_matching_ids_one_to_zero, numb_particle_match_fails_zero,
             group_number_one, sub_number_one, most_matching_ids_zero_to_one, sub_matching_ids_zero_to_one,
             numb_matching_ids_zero_to_one, numb_sub_matching_ids_zero_to_one, numb_particle_match_fails_one,
             nbound):

    file_name = f'{output_data_location}{snap}_matched_haloes_{nbound}.hdf5'
    if VERBOSE: print('Output file ', file_name)
    # profile file
    #TODO come up with better labels
    with h5.File(file_name, 'w') as output:

        matched_groups = output.create_group('MatchedHaloes')
        matched_subs = output.create_group('MatchedSubHaloes')
        ngs = output.create_group('GroupNumbers')
        sngs = output.create_group('SubGroupNumbers')
        matched_gN = output.create_group('NParticlesMatchedToGroup')
        matched_sN = output.create_group('NParticlesMatchedToSub')
        failN = output.create_group('NOrphanParticles')

        matched_groups.create_dataset('OneToZero', data=most_matching_ids_one_to_zero)
        matched_groups.create_dataset('ZeroToOne', data=most_matching_ids_zero_to_one)

        matched_subs.create_dataset('OneToZero', data=sub_matching_ids_one_to_zero)
        matched_subs.create_dataset('ZeroToOne', data=sub_matching_ids_zero_to_one)

        ngs.create_dataset('Zero', data = group_number_zero)
        ngs.create_dataset('One', data = group_number_one)

        sngs.create_dataset('Zero', data = sub_number_zero)
        sngs.create_dataset('One', data = sub_number_one)

        matched_gN.create_dataset('OneToZero', data=numb_matching_ids_one_to_zero)
        matched_gN.create_dataset('ZeroToOne', data=numb_matching_ids_zero_to_one)

        matched_sN.create_dataset('OneToZero', data=numb_sub_matching_ids_one_to_zero)
        matched_sN.create_dataset('ZeroToOne', data=numb_sub_matching_ids_zero_to_one)

        failN.create_dataset('Zero', data=numb_particle_match_fails_zero)
        failN.create_dataset('One', data=numb_particle_match_fails_one)

    if VERBOSE: print('File written')

    return


def do_everything(particle_data_location0, halo_data_location0, kdtree_location0,
                  particle_data_location1, halo_data_location1, kdtree_location1,
                  snap, output_data_location, N_match):

    start_time = time.time()
    if '7x' in particle_data_location0: bits = 21
    else: bits = 14
    data0 = my_read(particle_data_location0, halo_data_location0, kdtree_location0, snap, bits)

    if '7x' in particle_data_location1: bits = 21
    else: bits = 14
    data1 = my_read(particle_data_location1, halo_data_location1, kdtree_location1, snap, bits)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    dave_data = my_compare_ids(*data0, *data1, nbound=N_match)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    my_write(output_data_location, snap, *dave_data, nbound=N_match)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    with h5.File(f'{output_data_location}{snap}_matched_haloes_{N_match}.hdf5', 'r') as matches:
        full_match_stats(matches, N_match=N_match//2 + 1, const=10_000_000, VERBOSE=VERBOSE,
                         output_data_location=output_data_location, snap=snap)
    if VERBOSE: print('Finished snap in ' + str(np.round(time.time() - start_time, 1)) + 's')

    return


def full_match_stats(matches, N_match=25, const = 10_000_000, VERBOSE=True,
                     output_data_location=None, snap=None):
    # 0 is 7x
    # 1 is 1x

    print(f'N match = {N_match}')

    gn_0 = matches['GroupNumbers/Zero'][()].astype(np.int64)
    gn_1 = matches['GroupNumbers/One'][()].astype(np.int64)
    sn_0 = matches['SubGroupNumbers/Zero'][()].astype(np.int64)
    sn_1 = matches['SubGroupNumbers/One'][()].astype(np.int64)

    best_group_0 = matches['MatchedHaloes/OneToZero'][()].astype(np.int64)
    best_group_1 = matches['MatchedHaloes/ZeroToOne'][()].astype(np.int64)

    best_sub_0 = matches['MatchedSubHaloes/OneToZero'][()].astype(np.int64)
    best_sub_1 = matches['MatchedSubHaloes/ZeroToOne'][()].astype(np.int64)

    N_group_0 = matches['NParticlesMatchedToGroup/OneToZero'][()]
    N_group_1 = matches['NParticlesMatchedToGroup/ZeroToOne'][()]

    N_sub_0 = matches['NParticlesMatchedToSub/OneToZero'][()]
    N_sub_1 = matches['NParticlesMatchedToSub/ZeroToOne'][()]

    if np.amax(best_sub_0) < const or np.amax(best_sub_1) < const:
        print('Warning: const might be too small')

    n_0 = const * gn_0 + sn_0
    n_1 = const * gn_1 + sn_1

    best_0 = const * best_group_0 + best_sub_0
    best_1 = const * best_group_1 + best_sub_1

    in_0 = np.isin(best_0, n_1)
    in_1 = np.isin(best_1, n_0)

    if VERBOSE: print('started fixing 0')
    # best_0[np.logical_not(in_0)] = NO_GROUP_NUMBER
    # fix group numbers
    for i0, g0 in enumerate(best_0):
        if not i0 % 100_000:
            if VERBOSE: print(f'{i0} / {len(best_0)}')

        if not in_0[i0]:
            best_0[i0] = NO_GROUP_NUMBER

        if N_sub_0[i0] < N_match:
            best_0[i0] = NO_GROUP_NUMBER

        m01 = g0 == n_1
        nz01 = np.nonzero(m01)[0]

        if len(nz01) > 0:
            best_0[i0] = nz01
        else:
            best_0[i0] = NO_GROUP_NUMBER

    best_0[best_0 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started fixing 1')
    # best_1[np.logical_not(in_1)] = NO_GROUP_NUMBER
    # fix group numbers
    for i1, g1 in enumerate(best_1):
        if not i1 % 10_000:
            if VERBOSE: print(f'{i1} / {len(best_1)}')

        if not in_1[i1]:
            best_1[i1] = NO_GROUP_NUMBER

        if N_sub_1[i1] < N_match:
            best_1[i1] = NO_GROUP_NUMBER

        m10 = g1 == n_0
        nz10 = np.nonzero(m10)[0]

        if len(nz10) > 0:
            best_1[i1] = nz10
        else:
            best_1[i1] = NO_GROUP_NUMBER

    best_1[best_1 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started matching 0')
    match_0 = np.zeros(len(n_0), dtype=bool)
    for j, i in enumerate(best_0):
        if i < len(best_1):
            if best_1[i] == j:
                match_0[j] = True
                continue

    if VERBOSE: print('started matching 1')
    match_1 = np.zeros(len(n_1), dtype=bool)
    for j, i in enumerate(best_1):
        if i < len(best_0):
            if best_0[i] == j:
                match_1[j] = True
                continue

    if VERBOSE: print('started best aug')
    best_aug_1 = np.argsort(best_1[match_1])

    #centrals
    #centrals
    best_central_0 = const * best_group_0 #+ best_sub_0
    best_central_1 = const * best_group_1 #+ best_sub_1

    n_0 = const * gn_0 #+ sn_0
    n_1 = const * gn_1 #+ sn_1

    in_0 = sn_0 == 0
    in_1 = sn_1 == 0

    if VERBOSE: print('started fixing central 0')
    # best_central_0[np.logical_not(in_0)] = NO_GROUP_NUMBER
    # fix group numbers
    for i0, g0 in enumerate(best_central_0):
        if not i0 % 100_000:
            if VERBOSE: print(f'{i0} / {len(best_central_0)}')

        if not in_0[i0]:
            best_central_0[i0] = NO_GROUP_NUMBER

        if N_group_0[i0] < N_match:
            best_central_0[i0] = NO_GROUP_NUMBER

        m01 = g0 == n_1
        nz01 = np.nonzero(m01)[0]

        if len(nz01) > 1:
            best_central_0[i0] = nz01[0]
        elif len(nz01) > 0:
            best_central_0[i0] = nz01
        else:
            best_central_0[i0] = NO_GROUP_NUMBER

    best_central_0[best_central_0 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started fixing central 1')
    # best_central_1[np.logical_not(in_1)] = NO_GROUP_NUMBER
    # fix group numbers
    for i1, g1 in enumerate(best_central_1):
        if not i1 % 10_000:
            if VERBOSE: print(f'{i1} / {len(best_central_1)}')

        if not in_1[i1]:
            best_central_1[i1] = NO_GROUP_NUMBER

        if N_group_1[i1] < N_match:
            best_central_1[i1] = NO_GROUP_NUMBER

        m10 = g1 == n_0
        nz10 = np.nonzero(m10)[0]

        if len(nz10) > 1:
            best_central_1[i1] = nz10[0]
        elif len(nz10) > 0:
            best_central_1[i1] = nz10
        else:
            best_central_1[i1] = NO_GROUP_NUMBER

    best_central_1[best_central_1 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started matching central 0')
    match_central_0 = np.zeros(len(n_0), dtype=bool)
    for j, i in enumerate(best_central_0):
        if i < len(best_central_1):
            if best_central_1[i] == j:
                match_central_0[j] = True
                continue

    if VERBOSE: print('started matching central 1')
    match_central_1 = np.zeros(len(n_1), dtype=bool)
    for j, i in enumerate(best_central_1):
        if i < len(best_central_0):
            if best_central_0[i] == j:
                match_central_1[j] = True
                continue

    if VERBOSE: print('started best aug central')
    best_aug_central_1 = np.argsort(best_central_1[match_central_1])

    #TODO sort out file names
    if output_data_location is not None and N_min == 0:
        group_file_name = f'{output_data_location}{snap}_matched_groups_only_{N_match}.hdf5'

        print('Group file ', group_file_name)
        with h5.File(group_file_name, 'w') as output:

            group = output.create_group('GroupBijectiveMatches')

            group.create_dataset('MatchedGroupNumber7x', data = gn_0[match_central_0])
            group.create_dataset('MatchedGroupNumber1x', data = gn_1[match_central_1][best_aug_central_1])


        sub_file_name = f'{output_data_location}{snap}_matched_subgroups_only_{N_match}.hdf5'

        print('Subgroup file ', sub_file_name)
        with h5.File(sub_file_name, 'w') as output:

            group = output.create_group('SubGroupBijectiveMatches')

            group.create_dataset('MatchedGroupNumber7x', data = gn_0[match_0])
            group.create_dataset('MatchedGroupNumber1x', data = gn_1[match_1][best_aug_1])

            group.create_dataset('MatchedSubGroupNumber7x', data = sn_0[match_0])
            group.create_dataset('MatchedSubGroupNumber1x', data = sn_1[match_1][best_aug_1])

    if VERBOSE: print('started diagnostics')

    return


if __name__ == '__main__':
    _, snap_index, N_match = sys.argv

    snap_index = int(snap_index)
    N_match = int(N_match)

    #hyades
    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    #ozstar
    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location0 = particle_data_location_list[snap_index // 4]
    particle_data_location1 = particle_data_location_list[1 + (snap_index // 4)]
    halo_data_location0 = halo_data_location_list[snap_index // 4]
    halo_data_location1 = halo_data_location_list[1 + (snap_index // 4)]
    output_data_location0 =  output_data_location_list[snap_index // 4]
    output_data_location1 =  output_data_location_list[1 + (snap_index // 4)]
    kdtree_location0 = output_data_location0
    kdtree_location1 = output_data_location1
    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]
    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    do_everything(particle_data_location1, halo_data_location1, kdtree_location1,
                  particle_data_location0, halo_data_location0, kdtree_location0,
                  snap, output_data_location0, N_match)

    if VERBOSE: print('Done all')

    pass
