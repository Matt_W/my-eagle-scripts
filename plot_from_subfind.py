#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 14:52:05 2021

@author: matt
"""

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
# from scipy.interpolate import interp1d
# from scipy.interpolate import interpn

from scipy.integrate   import quad

# from scipy.optimize    import minimize
# from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.stats       import binned_statistic

import matplotlib
matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt
from matplotlib.cm     import Greys
from matplotlib.cm     import Purples
from matplotlib.cm     import Blues
from matplotlib.cm     import Greens
from matplotlib.cm     import Oranges
from matplotlib.cm     import Reds

# from matplotlib.colors import LogNorm
# from matplotlib.colors import ListedColormap
# from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               # AutoMinorLocator)
# from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import ReadEagleSubfind_halo

import read_EAGLE_like_GalIC

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

grav_const   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)

def my_nonzero_nan_quantile(array, q):
  '''
  '''
  copy = array[np.logical_not(np.isnan(array))]
  copy = copy[copy!=0]
  if len(copy) == 0:
    return(0)

  return(np.quantile(copy, q))

def my_nonzero_nan_median(array):
  return(my_nonzero_nan_quantile(array, 0.5))
def my_nonzero_nan_psigma(array):
  return(my_nonzero_nan_quantile(array, 0.84))
def my_nonzero_nan_msigma(array):
  return(my_nonzero_nan_quantile(array, 0.16))

def plot_stellar_mass_stellar_r_half(raw_data, snap_i=0, pwd=''):
  '''
  '''
  # centrals = (raw_data['SubGroupNumber'][:] == 0)
  #for some reason the most obvious thing is wrong .....
  centrals = raw_data['FirstSub']

  #subhalo stellar mass in M_sun
  x_data = raw_data['Group_M_Crit200'] * 1e10 / raw_data['HubbleParam']
  #subhalo stellar half mass radii pkpc
  y_data = raw_data['HalfMassRadius'][:, 4] * 1000
  y_data = y_data[centrals]

  x_bin_edges   = np.logspace(7, 14, 15)
  x_bin_middles = np.logspace(7.25, 13.75, 14)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started rad')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((3e-1, 3e1))

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e13, 3e0, snap_list[snap_i][4:], ha='right')

  plt.xlabel(r'$M_{200} $[M$_\odot $]')
  plt.ylabel(r'$r_{1/2}$ [kpc]')

  plt.savefig(pwd + 'mass200_r_half' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  #subhalo stellar mass in M_sun
  x_data = raw_data['MassType'][:, 1] * 1e10 / raw_data['HubbleParam']
  #subhalo stellar half mass radii pkpc
  y_data = raw_data['HalfMassRadius'][:, 4] * 1000

  x_bin_edges   = np.logspace(7, 14, 15)
  x_bin_middles = np.logspace(7.25, 13.75, 14)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started rad')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((3e-1, 3e1))

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e13, 3e0, snap_list[snap_i][4:], ha='right')

  plt.xlabel(r'$M_{DM} $[M$_\odot $]')
  plt.ylabel(r'$r_{1/2}$ [kpc]')

  plt.savefig(pwd + 'massDM_r_half' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  return

def plot_stellar_mass_stellar_r_half_comparison(Base_list, col_list, lab_list, pwd, snap_i):
  '''
  '''
  start_time = time.time()

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((3e-1, 3e1))

  plt.text(3e13, 3e0, snap_list[snap_i][4:], ha='right')

  for j in range(len(Base_list)):
    Base = Base_list[j]

    #pretty sure splitting like this isnt't necessary ...
    line=snap_list[snap_i].split('\n')[0].split('_')
    Num=line[0]
    fend='_'+line[1]
    exts         = Num.zfill(3)

    try:
      raw_data = ReadEagleSubfind_halo.ReadEagleSubfind_halo(Base, '', fend, exts)
    except:
      continue

    #for some reason the most obvious thing is wrong .....
    centrals = raw_data['FirstSub']

    #subhalo stellar mass in M_sun
    # x_data = raw_data['MassType'][:, 4] * 1e10 / raw_data['HubbleParam']
    x_data = raw_data['Group_M_Crit200'] * 1e10 / raw_data['HubbleParam']
    #subhalo stellar half mass radii pkpc
    y_data = raw_data['HalfMassRadius'][:, 4] * 1000
    try:
      y_data = y_data[centrals]
    except IndexError:
      print(len(centrals))
      print(len(x_data))
      print(len(y_data))
      print(max(centrals))

      for i in range(len(centrals)):
        if centrals[i] >= len(y_data):
          print('adjusting halo')
          centrals[i] = len(centrals) - 1

      y_data = y_data[centrals]

    # print(len(centrals))
    # # print(sum(raw_data['SubGroupNumber'][raw_data['FirstSub']] == 0))
    # print(len(raw_data['SubGroupNumber'][raw_data['FirstSub']]))

    x_bin_edges   = np.logspace(7, 14, 15)
    x_bin_middles = np.logspace(7.25, 13.75, 14)

    medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
    psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
    msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

    for i in range(len(medians)):
      print(x_bin_middles[i], ' \t', psigmas[i], ' \t', medians[i], ' \t', msigmas)

    plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
    plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

  plt.legend(ncol=2)

  plt.xlabel(r'$M_{200} $[M$_\odot $]')
  plt.ylabel(r'$r_{1/2}$ [kpc]')

  plot_name = pwd + 'mass200_r_half_comparison' + snap_list[snap_i]
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  #do m dm
  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((3e-1, 3e1))

  plt.text(3e13, 3e0, snap_list[snap_i][4:], ha='right')

  for j in range(len(Base_list)):
    Base = Base_list[j]

    #pretty sure splitting like this isnt't necessary ...
    line=snap_list[snap_i].split('\n')[0].split('_')
    Num=line[0]
    fend='_'+line[1]
    exts         = Num.zfill(3)

    try:
      raw_data = ReadEagleSubfind_halo.ReadEagleSubfind_halo(Base, '', fend, exts)
    except:
      continue

    #subhalo stellar mass in M_sun
    # x_data = raw_data['MassType'][:, 4] * 1e10 / raw_data['HubbleParam']
    x_data = raw_data['MassType'][:, 1] * 1e10 / raw_data['HubbleParam']
    #subhalo stellar half mass radii pkpc
    y_data = raw_data['HalfMassRadius'][:, 4] * 1000

    x_bin_edges   = np.logspace(7, 14, 15)
    x_bin_middles = np.logspace(7.25, 13.75, 14)

    medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
    psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
    msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

    plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
    plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

  plt.legend(ncol=2)

  plt.xlabel(r'$M_{DM} $[M$_\odot $]')
  plt.ylabel(r'$r_{1/2}$ [kpc]')

  plot_name = pwd + 'massDM_r_half_comparison' + snap_list[snap_i]
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  print('Done 2 plots in ', np.round(time.time() - start_time))

  return

def plot_box_comparison(xseries, yseries, xlabel, ylabel, save_name,
                        kin_list, col_list, lab_list, pwd, snap_i):
  '''
  '''
  start_time = time.time()

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((1e-1, 3e1))

  plt.text(3e13, 3e-1, snap_list[snap_i][4:], ha='right')

  for j in range(len(Base_list)):
    Base = kin_list[j]

    line = snap_list[snap_i].split('\n')[0].split('_')
    Num  = line[0]
    fend = '_'+line[1]
    exts = Num.zfill(3)
    file_name = Base + exts + fend + '_galaxy_kinematic_profile.hdf5'
    # print(file_name)

    try:
      with h5.File(file_name, 'r') as raw_data:

        x_data = raw_data[xseries][()]
        y_data = raw_data[yseries][()]

        if len(np.shape(y_data)) == 2:
          thrity_bin = 43
          y_data = y_data[:, thrity_bin]

        finite_mask = np.logical_and(np.isfinite(x_data), np.isfinite(y_data))

        x_data = x_data[finite_mask]
        y_data = y_data[finite_mask]

        x_bin_edges   = np.logspace(7.25, 14.25, 15)
        x_bin_middles = np.logspace(7.50, 14.00, 14)

        medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
        psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
        msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

        for i in range(len(medians)):
          print(np.round(np.log10(x_bin_middles[i]),4), ' \t', np.round(psigmas[i],4), ' \t',
                np.round(medians[i],4), ' \t', np.round(msigmas[i],4))

        plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
        plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

    except OSError:
      print('No file ', file_name)
      continue

  plt.legend(ncol=2, loc='upper left')

  plt.xlabel(xlabel)
  plt.ylabel(ylabel)

  plot_name = pwd + save_name
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  print('Done plot in ', np.round(time.time() - start_time))

  return

def plot_box_kinematic_comparison(kin_list, col_list, lab_list, pwd, snap_i):
  '''
  '''
  start_time = time.time()

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.semilogx()

  plt.xlim((1e8, 1e14))
  plt.ylim((0, 1))

  plt.text(3e13, 0.2, snap_list[snap_i][4:], ha='right')

  for j in range(len(Base_list)):
    Base = kin_list[j]

    line = snap_list[snap_i].split('\n')[0].split('_')
    Num  = line[0]
    fend = '_'+line[1]
    exts = Num.zfill(3)
    file_name = Base + exts + fend + '_galaxy_kinematic_profile.hdf5'
    # print(file_name)

    try:
      with h5.File(file_name, 'r') as raw_data:

        x_data = raw_data['GalaxyQuantities/M200'][()]
        y_data = raw_data['GalaxyProfiles/fjzjc00'][()]
        if len(np.shape(y_data)) == 2:
          thrity_bin = 43
          y_data = y_data[:, thrity_bin]
        y_data = 2 * (1 - y_data)

        finite_mask = np.logical_and(np.isfinite(x_data), np.isfinite(y_data))

        x_data = x_data[finite_mask]
        y_data = y_data[finite_mask]

        x_bin_edges   = np.logspace(7.25, 14.25, 15)
        x_bin_middles = np.logspace(7.50, 14.00, 14)

        medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
        psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
        msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

        for i in range(len(medians)):
          print(np.round(np.log10(x_bin_middles[i]),4), ' \t', np.round(psigmas[i],4), ' \t',
                np.round(medians[i],4), ' \t', np.round(msigmas[i],4))

        plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
        plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

    except OSError:
      print('No file ', file_name)
      continue

  plt.legend(ncol=2, loc='lower left')

  plt.xlabel(r'$M_{200}$ [M$_\odot$]')
  plt.ylabel('S/T')

  plot_name = pwd + 'M200_StoT' + snap_list[snap_i]
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  print('Done plot in ', np.round(time.time() - start_time))
  start_time = time.time()

  ##v/sigma

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((6e-1, 3e0))

  plt.text(3e13, 7e-1, snap_list[snap_i][4:], ha='right')

  for j in range(len(Base_list)):
    Base = kin_list[j]

    line = snap_list[snap_i].split('\n')[0].split('_')
    Num  = line[0]
    fend = '_'+line[1]
    exts = Num.zfill(3)
    file_name = Base + exts + fend + '_galaxy_kinematic_profile.hdf5'
    # print(file_name)

    try:
      with h5.File(file_name, 'r') as raw_data:

        x_data = raw_data['GalaxyQuantities/M200'][()]
        sigma = np.sqrt(np.square(raw_data['GalaxyProfiles/sigmaz'][:, 43]) +
                        np.square(raw_data['GalaxyProfiles/sigmaR'][:, 43]) +
                        np.square(raw_data['GalaxyProfiles/sigmaphi'][:, 43]))
        y_data = raw_data['GalaxyProfiles/MeanVphi'][:, 43] / sigma / np.sqrt(3)

        y_data = 2 * (1 - y_data)

        if len(np.shape(y_data)) == 2:
          thrity_bin = 43
          y_data = y_data[:, thrity_bin]

        finite_mask = np.logical_and(np.isfinite(x_data), np.isfinite(y_data))

        x_data = x_data[finite_mask]
        y_data = y_data[finite_mask]

        x_bin_edges   = np.logspace(7.25, 14.25, 15)
        x_bin_middles = np.logspace(7.50, 14.00, 14)

        medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
        psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
        msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

        for i in range(len(medians)):
          print(np.round(np.log10(x_bin_middles[i]),4), ' \t', np.round(psigmas[i],4), ' \t',
                np.round(medians[i],4), ' \t', np.round(msigmas[i],4))

        plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
        plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

    except OSError:
      print('No file ', file_name)
      continue

  plt.legend(ncol=2, loc='upper left')

  plt.xlabel(r'$M_{200}$ [M$_\odot$]')
  plt.ylabel(r'$\overline{v_\phi} / (\sqrt{3} \sigma_{TOT})$')

  plot_name = pwd + 'M200_vonsigma' + snap_list[snap_i]
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  print('Done plot in ', np.round(time.time() - start_time))

  return

def plot_ideal_comparison(xseries, yseries, xlabel, ylabel, save_name,
                          kin_list, col_list, lab_list, marker_list, pwd, snap_i):
  '''
  '''
  start_time = time.time()

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((1e-1, 3e1))

  plt.text(3e13, 3e-1, snap_list[snap_i][4:], ha='right')

  #Median evolution
  for j in range(len(Base_list)):
    Base = kin_list[j]

    line = snap_list[snap_i].split('\n')[0].split('_')
    Num  = line[0]
    fend = '_'+line[1]
    exts = Num.zfill(3)
    file_name = Base + exts + fend + '_galaxy_kinematic_profile.hdf5'
    # print(file_name)

    try:
      with h5.File(file_name, 'r') as raw_data:

        x_data = raw_data[xseries][()]
        y_data = raw_data[yseries][()]

        if len(np.shape(y_data)) == 2:
          thrity_bin = 43
          y_data = y_data[:, thrity_bin]

        finite_mask = np.logical_and(np.isfinite(x_data), np.isfinite(y_data))

        x_data = x_data[finite_mask]
        y_data = y_data[finite_mask]

        x_bin_edges   = np.logspace(7.25, 14.25, 15)
        x_bin_middles = np.logspace(7.50, 14.00, 14)

        medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
        psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
        msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

        for i in range(len(medians)):
          print(np.round(np.log10(x_bin_middles[i]),4), ' \t', np.round(psigmas[i],4), ' \t',
                np.round(medians[i],4), ' \t', np.round(msigmas[i],4))

        plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
        plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

    except OSError:
      print('No file ', file_name)
      continue

  plt.legend(ncol=2, loc='upper left')

  #ideal galaxies
  data_pwd = '/mnt/su3ctm/mwilkinson/GalIC/EAGLE_like/'

  for k, beta in enumerate([0, 1]):
    for j, x in enumerate([1, 7]):
      for logM200 in [8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5]:

        model_name = 'Model_M200_%d_beta%d_x%d'%(int(logM200*10), beta, x)

        try:
          data = read_EAGLE_like_GalIC.load_stars(data_pwd, model_name)
        except:
          continue

        x_data = data[xseries.split('/')[1]][()]
        y_data = data[yseries.split('/')[1]][()]

        # if len(np.shape(y_data)) == 2:
        #   thrity_bin = 43
        #   y_data = y_data[:, thrity_bin]

        finite_mask = np.logical_and(np.isfinite(x_data), np.isfinite(y_data))
        x_data = x_data[finite_mask]
        y_data = y_data[finite_mask]

        print(np.log10(x_data[0]), ' \t', y_data[0])

        plt.errorbar(x_data, y_data, c=col_list[j], fmt=marker_list[k],
                     markersize=15, alpha=0.9)

  # plt.legend(ncol=2, loc='upper left')

  plt.xlabel(xlabel)
  plt.ylabel(ylabel)

  plot_name = pwd + save_name
  print(plot_name)
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  print('Done plot in ', np.round(time.time() - start_time))

  return

def plot_number_of_stars(save_name,
                         kin_list, col_list, lab_list, pwd, snap_i):
  '''
  '''
  start_time = time.time()

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.loglog()

  plt.xlim((1e8, 1e14))
  plt.ylim((1e0, 1e5))

  plt.text(3e13, 3e0, snap_list[snap_i][4:], ha='right')

  for j in range(len(Base_list)):
    Base = kin_list[j]

    line = snap_list[snap_i].split('\n')[0].split('_')
    Num  = line[0]
    fend = '_'+line[1]
    exts = Num.zfill(3)
    file_name = Base + exts + fend + '_galaxy_kinematic_profile.hdf5'
    # print(file_name)

    try:
      with h5.File(file_name, 'r') as raw_data:

        x_data = raw_data['GalaxyQuantities/M200z0crit'][()]
        y_data = raw_data['GalaxyProfiles/BinN'][()]

        if len(np.shape(y_data)) == 2:
          thirty_bin = 43
          y_data = y_data[:, thirty_bin]

        finite_mask = np.logical_and(np.isfinite(x_data), np.isfinite(y_data))

        x_data = x_data[finite_mask]
        y_data = y_data[finite_mask]

        x_bin_edges   = np.logspace(7.25, 14.25, 15)
        x_bin_middles = np.logspace(7.50, 14.00, 14)

        medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
        psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
        msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

        # for i in range(len(medians)):
        #   print(np.round(np.log10(x_bin_middles[i]),8), ' \t', np.round(psigmas[i],8), ' \t',
        #         np.round(medians[i],8), ' \t', np.round(msigmas[i],8))

        plt.errorbar(x_bin_middles, medians, c=col_list[j], label=lab_list[j])
        plt.fill_between(x_bin_middles, psigmas, msigmas, color=col_list[j], alpha=0.3)

    except OSError:
      print('No file ', file_name)
      continue

  plt.legend(ncol=2, loc='upper left')

  plt.xlabel(r'$M_{200}$ $(\rho_{crit}(z=0))$')
  plt.ylabel(r'$N_\star$')

  plot_name = pwd + save_name
  plt.savefig(plot_name, bbox_inches='tight')
  plt.close()

  print('Done plot in ', np.round(time.time() - start_time))

  return

if __name__ == '__main__':

  #Where the data is
  Base0 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/Jacks_7x376dm/data'
  pwd0  = '/home/mwilkinson/EAGLE/L0025N0376/Jacks_7x376dm/'

  Base1 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/Jacks_7x376dm_Restart_z=1/data'
  pwd1  = '/home/mwilkinson/EAGLE/L0025N0376/Jacks_7x376dm_Restart_z=1/'

  Base2 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/REFERENCE/data'
  pwd2  = '/home/mwilkinson/EAGLE/L0025N0376/REFERENCE/'

  Base3 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/REFERENCE_Restart_z=1/data'
  pwd3  = '/home/mwilkinson/EAGLE/L0025N0376/REFERENCE_Restart_z=1/'

  kin_name0 = '/mnt/su3ctm/mwilkinson/EAGLE/Jacks_7x376dm/'
  kin_name1 = '/mnt/su3ctm/mwilkinson/EAGLE/Jacks_7x376dm_Restart_z=1/'
  kin_name2 = '/mnt/su3ctm/mwilkinson/EAGLE/REFERENCE/'
  kin_name3 = '/mnt/su3ctm/mwilkinson/EAGLE/REFERENCE_Restart_z=1/'

  Base_list = [Base0, Base2, Base1, Base3]
  pwd_list  = [pwd0,  pwd2,  pwd1,  pwd3]
  col_list  = ['C0',  'C2', 'C1', 'C3']
  lab_list  = ['7x',  '1x', 'grav-only 7x', 'grav-only 1x']
  kin_list  = [kin_name0, kin_name2, kin_name1, kin_name3]

  marker_list = ['*', '.']

  #which snapshots to look at
  # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
  #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
  #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
  #              '016_z001p737', '015_z002p012']
  snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']

  pwd  = '/home/mwilkinson/EAGLE/L0025N0376/'

  #comparison plots
  for i in range(len(snap_list)):
    plot_box_kinematic_comparison(kin_list, col_list, lab_list, pwd, i)

  # plot_box_comparison('GalaxyQuantities/M200', 'GalaxyQuantities/StellarHalfMassRadius',
  #                     r'$M_{200}$ [M$_\odot$]', r'$r_{1/2}$ [kpc]',
  #                     'M200_rhalf_comparison_' + snap_list[i],
  #                     kin_list, col_list, lab_list, pwd, i)

  # plot_ideal_comparison('GalaxyQuantities/M200z0crit', 'GalaxyQuantities/StellarHalfMassRadius',
  #                       r'$M_{200}$ $(\rho_{crit}(z=0))$ [M$_\odot$]', r'$r_{1/2}$ [kpc]',
  #                       'M200z0_rhalf_ideal_' + snap_list[3],
  #                       kin_list, col_list, lab_list, marker_list, pwd, 3)

  # plot_ideal_comparison('GalaxyQuantities/M200z0crit', 'GalaxyQuantities/StellarHalfMassRadiusProj',
  #                       r'$M_{200}$ $(\rho_{crit}(z=0))$ [M$_\odot$]', r'$R_{1/2}$ [kpc]',
  #                       'M200z0_Rhalfproj_ideal_' + snap_list[3],
  #                       kin_list, col_list, lab_list, marker_list, pwd, 3)

  # plot_box_comparison('GalaxyQuantities/M200', 'GalaxyQuantities/StellarQuarterMassRadius',
  #                     r'$M_{200}$ [M$_\odot$]', r'$r_{1/4}$ [kpc]',
  #                     'M200_rquater_comparison_' + snap_list[i],
  #                     kin_list, col_list, lab_list, pwd, i)

  # plot_box_comparison('GalaxyQuantities/M200', 'GalaxyQuantities/Stellar3QuarterMassRadius',
  #                     r'$M_{200}$ [M$_\odot$]', r'$r_{3/4}$ [kpc]',
  #                     'M200_r3quarter_comparison_' + snap_list[i],
  #                     kin_list, col_list, lab_list, pwd, i)

  # plot_box_comparison('GalaxyQuantities/M200', 'GalaxyProfiles/zHalf',
  #                     r'$M_{200}$ [M$_\odot$]', r'$z_{1/2}$ [kpc]',
  #                     'M200_zhalf_comparison_' + snap_list[i],
  #                     kin_list, col_list, lab_list, pwd, i)

  # plot_ideal_comparison('GalaxyQuantities/M200z0crit', 'GalaxyProfiles/zHalf',
  #                       r'$M_{200}$ $(\rho_{crit}(z=0))$ [M$_\odot$]', r'$z_{1/2}$ [kpc]',
  #                       'M200z0_zhalf_ideal_' + snap_list[3],
  #                       kin_list, col_list, lab_list, marker_list, pwd, 3)

  # plot_baryon_comparison('M200z0_bary_frac_comparison_' + snap_list[i],
  #                         kin_list, col_list, lab_list, pwd, i)

  # for i in range(len(snap_list)):
  #   plot_number_of_stars('M200z0_Nstars_comparison_' + snap_list[i],
  #                        kin_list, col_list, lab_list, pwd, i)

  # print('done ', i)

  # #individual plots
  # for j in range(len(Base_list)):
  #   Base = Base_list[j]
  #   pwd  = pwd_list[j]

  #   #do for all sizes for all times
  #   for i in range(len(snap_list)):

  #     #pretty sure splitting like this isnt't necessary ...
  #     line=snap_list[i].split('\n')[0].split('_')
  #     Num=line[0]
  #     fend='_'+line[1]
  #     exts= Num.zfill(3)

  #     try:
  #       data = ReadEagleSubfind_halo.ReadEagleSubfind_halo(Base, '', fend, exts)

  #       file_name = Base + exts + fend + '_galaxy_kinematic_profile.hdf5'
  #       with h5.File(file_name, 'r') as raw_data:
  #         x_data = raw_data['GalaxyQuantities/M200'][()]

  #     except:
  #       continue

  #     plot_stellar_mass_stellar_r_half(data, i, pwd)

  pass