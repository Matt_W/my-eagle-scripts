#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 15:57:08 2021

@author: matt
"""
import numpy as np

FORTY_SEVEN = 48

def read_merger_csv(data_location, csv_name):
  '''Warning: empty doc string
  '''

  with open(data_location + csv_name) as my_file:
    n_lines = sum(1 for _ in my_file)

  n_lines -= FORTY_SEVEN

  #ids
  GalaxyID       = np.zeros(n_lines, dtype=np.int64)
  LastProgID     = np.zeros(n_lines, dtype=np.int64)
  TopLeafID      = np.zeros(n_lines, dtype=np.int64)
  DescendantID   = np.zeros(n_lines, dtype=np.int64)
  GroupNumber    = np.zeros(n_lines, dtype=np.int64)
  SubGroupNumber = np.zeros(n_lines, dtype=np.int64)

  #other stuff
  BlackHoleMass              = np.zeros(n_lines, dtype=np.float64)
  BlackHoleMassAccretionRate = np.zeros(n_lines, dtype=np.float64)
  CentreOfPotential          = np.zeros((n_lines, 3), dtype=np.float64)

  Mass          = np.zeros(n_lines, dtype=np.float64)
  MassType_DM   = np.zeros(n_lines, dtype=np.float64)
  MassType_Gas  = np.zeros(n_lines, dtype=np.float64)
  MassType_Star = np.zeros(n_lines, dtype=np.float64)
  MassType_BH   = np.zeros(n_lines, dtype=np.float64)

  with open(data_location + csv_name) as csv_file:

    for i, row in enumerate(csv_file, -FORTY_SEVEN):

      if i >= 0:
        row_list = row.split(',')

        GalaxyID[i]       = np.int64(row_list[0])
        LastProgID[i]     = np.int64(row_list[1])
        TopLeafID[i]      = np.int64(row_list[2])
        DescendantID[i]   = np.int64(row_list[3])
        GroupNumber[i]    = np.int64(row_list[4])
        SubGroupNumber[i] = np.int64(row_list[5])

        #other stuff
        BlackHoleMass[i]              = np.float64(row_list[6])
        BlackHoleMassAccretionRate[i] = np.float64(row_list[7])
        CentreOfPotential[i,0]        = np.float64(row_list[8])
        CentreOfPotential[i,1]        = np.float64(row_list[9])
        CentreOfPotential[i,2]        = np.float64(row_list[10])

        Mass[i]          = np.float64(row_list[11])
        MassType_DM[i]   = np.float64(row_list[12])
        MassType_Gas[i]  = np.float64(row_list[13])
        MassType_Star[i] = np.float64(row_list[14])
        MassType_BH[i]   = np.float64(row_list[15])

  return({'GalaxyID':GalaxyID, 'LastProgID':LastProgID, 'TopLeafID':TopLeafID, 'DescendantID':DescendantID,
          'GroupNumber':GroupNumber, 'SubGroupNumber':SubGroupNumber,
          'BlackHoleMass':BlackHoleMass, 'BlackHoleMassAccretionRate':BlackHoleMassAccretionRate,
          'CentreOfPotential':CentreOfPotential,
          'Mass':Mass, 'MassType_DM':MassType_DM, 'MassType_Gas':MassType_Gas,
          'MassType_Star':MassType_Star, 'MassType_BH':MassType_BH})

if __name__ == '__main__':


  data_location = '/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/'

  csv_name = 'Merger_028.csv'

  out = read_merger_csv(data_location, csv_name)

  pass