#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

# import matplotlib
# matplotlib.use('Agg')

import h5py as h5

from scipy.integrate import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

import traceback as tb

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
BOX_SIZE = 33.885 #50 * 0.6777 #kpc
# SCALE_A  = 1
DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

n_jzjc_bins = 21 #np.linspace(-1,1,n_jzjc_bins)
n_formation_time_intervals = 4 #[0.25, 0.5, 1, 2] Myr
formation_age_percentiles = np.array([0.9, 0.75, 0.5, 0.25, 0.1])
n_formation_age_percentiles = len(formation_age_percentiles)

def my_read(particle_data_location, halo_data_location, output_data_location, kdtree_location, snap):
    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    FirstSub = np.zeros(TotNgroups, dtype=np.int64)

    # Subhalo arrays
    GroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    NGrp_c = 0
    NSub_c = 0

    print('TotNGroups:', TotNgroups)
    print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = particle_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            if Nsubgroups > 0:
                GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
                SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]

                SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]

                # SubhaloMass[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/Mass"][()]

                NSub_c += Nsubgroups

    print('Loaded halos')

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    # NumPartTot = [0, NumPartTot[1], 0, 0, 0]

    # if NumPartTot[4] > 0:
    PosStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    VelStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    MassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    InitialMassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    # BirthDensity   = np.zeros(NumPartTot[4],    dtype=np.float32)
    # Metallicity = np.zeros(NumPartTot[4], dtype=np.float32)
    Star_aform = np.zeros(NumPartTot[4], dtype=np.float32)
    # Star_tform     = np.zeros(NumPartTot[4],    dtype=np.float32)
    BindingEnergyStar = np.zeros(NumPartTot[4], dtype=np.float32)
    # HSML_Star      = np.zeros(NumPartTot[4],    dtype=np.float32)
    # ParticleIDs = np.zeros(NumPartTot[4], dtype=np.int32)
    GrpNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)
    SubNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)

    PosGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    VelGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    MassGas = np.zeros(NumPartTot[0], dtype=np.float32)
    BindingEnergyGas = np.zeros(NumPartTot[0], dtype=np.float32)
    GrpNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)
    SubNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)

    PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    VelDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    SubNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)

    PosBH = np.zeros((NumPartTot[5], 3), dtype=np.float32)
    # VelBH = np.zeros((NumPartTot[5], 3), dtype=np.float32)
    GravMassBH = np.zeros(NumPartTot[5], dtype=np.float32)
    MassBH = np.zeros(NumPartTot[5], dtype=np.float32)
    GrpNum_BH = np.zeros(NumPartTot[5], dtype=np.int32)
    SubNum_BH = np.zeros(NumPartTot[5], dtype=np.int32)

    NStar_c = 0
    NGas_c = 0
    NDM_c = 0
    NBH_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[4] > 0:
                PosStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Coordinates"][()]
                VelStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Velocity"][()]
                MassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Mass"][()]
                InitialMassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/InitialMass"][()]
                # BirthDensity[NStar_c:NStar_c+NumParts[4]] = fs["PartType4/BirthDensity"][()]
                # Metallicity[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SmoothedMetallicity"][()]
                Star_aform[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/StellarFormationTime"][()]
                # Star_tform[NStar_c:NStar_c+NumParts[4]]   = lbt(Star_aform[NStar_c:NStar_c+NumParts[4]])
                BindingEnergyStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleBindingEnergy"][()]
                # HSML_Star[NStar_c:NStar_c+NumParts[4]]    = fs["PartType4/SmoothingLength"][()]
                # ParticleIDs[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleIDs"][()]
                GrpNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/GroupNumber"][()]
                SubNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SubGroupNumber"][()]

                NStar_c += NumParts[4]

            if NumParts[0] > 0:
                PosGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Coordinates"][()]
                VelGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Velocity"][()]
                MassGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/Mass"][()]
                BindingEnergyGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/ParticleBindingEnergy"][()]
                GrpNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/GroupNumber"][()]
                SubNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/SubGroupNumber"][()]

                NGas_c += NumParts[0]

            if NumParts[1] > 0:
                PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                VelDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Velocity"][()]
                GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"][()]
                SubNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/SubGroupNumber"][()]

                NDM_c += NumParts[1]

            if NumParts[5] > 0:
                PosBH[NBH_c:NBH_c + NumParts[5], :] = fs["PartType5/Coordinates"][()]
                # VelBH[NBH_c:NBH_c + NumParts[5], :] = fs["PartType5/Velocity"][()]
                GravMassBH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/Mass"][()]
                MassBH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/BH_Mass"][()]
                GrpNum_BH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/GroupNumber"][()]
                SubNum_BH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/SubGroupNumber"][()]

                NBH_c += NumParts[5]

    print('loaded particles')

    start_time = time.time()
    star_kd_tree = cKDTree(PosStar, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated star kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
    start_time = time.time()
    gas_kd_tree  = cKDTree(PosGas, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated gas kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
    start_time = time.time()
    bh_kd_tree = cKDTree(PosBH, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated bh kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')

    # #tree is too large to fit into ram
    # if not(kdtree_location == '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/' and
    #        snap == '028_z000p000'):
    dm_tree_file_name = kdtree_location + snap + '_dm_tree.pickle'
    start_time = time.time()
    print('DM tree file name ' + dm_tree_file_name)
    if os.path.exists(dm_tree_file_name):
        print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            dm_kd_tree = pickle.load(dm_tree_file)
        print('Opened dm kdtree')

    else:
        print('Calculating')
        dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
        print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(dm_kd_tree, dm_tree_file, protocol=4)
        print('Saved dm kdtree.')
    print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    # else:
    #     print('skipped DM tree')

    print('Done all kdtrees.')

    start_time = time.time()
    print('Calculating stellar ages')
    # Star_tform = lbt(Star_aform)
    Star_tform = lbt_interp(Star_aform)
    print('Calculated ages in ' + str(np.round(time.time() - start_time, 1)) + 's')

    print('loaded everything')

    return (output_data_location, kdtree_location, snap,
            star_kd_tree, gas_kd_tree, dm_kd_tree, bh_kd_tree,
            TotNgroups, TotNsubgroups,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
            GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
            PosStar, VelStar, MassStar, InitialMassStar, Star_tform,
            BindingEnergyStar,
            GrpNum_Star, SubNum_Star,
            PosGas, VelGas, MassGas,
            BindingEnergyGas,
            GrpNum_Gas, SubNum_Gas,
            PosDM, VelDM,
            GrpNum_DM, SubNum_DM,
            PosBH, MassBH, GravMassBH,
            GrpNum_BH, SubNum_BH)


def my_calculate(output_data_location, kdtree_location, snap,
                 star_kd_tree, gas_kd_tree, dm_kd_tree, bh_kd_tree,
                 TotNgroups, TotNsubgroups,
                 Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
                 GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
                 PosStar, VelStar, MassStar, InitialMassStar, Star_tform,
                 BindingEnergyStar,
                 GrpNum_Star, SubNum_Star,
                 PosGas, VelGas, MassGas,
                 BindingEnergyGas,
                 GrpNum_Gas, SubNum_Gas,
                 PosDM, VelDM,
                 GrpNum_DM, SubNum_DM,
                 PosBH, MassBH, GravMassBH,
                 GrpNum_BH, SubNum_BH):

    dm_mass = DM_MASS
    if '7x' in output_data_location:
        dm_mass /= 7

    # set up arrays to save values
    group_ids = np.zeros(TotNsubgroups, dtype=np.int32)
    subgroup_ids = np.zeros(TotNsubgroups, dtype=np.int32)

    star_half_mass_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    star_quater_mass_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    star3quater_mass_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    star_half_mass_proj_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    star_quater_mass_proj_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    star3quater_mass_proj_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    gas_half_mass_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    gas_quater_mass_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    gas3quater_mass_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    gas_half_mass_proj_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    gas_quater_mass_proj_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    gas3quater_mass_proj_radius = np.zeros(TotNsubgroups, dtype=np.float32)
    galaxy_r200 = np.zeros(TotNsubgroups, dtype=np.float32)
    galaxy_m200 = np.zeros(TotNsubgroups, dtype=np.float32)
    group_cops = np.zeros((TotNsubgroups, 3), dtype=np.float32)
    sub_cops = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    star_J_box = np.zeros((TotNsubgroups, 3), dtype=np.float32)
    gas_J_box = np.zeros((TotNsubgroups, 3), dtype=np.float32)
    dm_J_box = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    galaxy_has_stars = np.ones(TotNsubgroups, dtype=bool)

    #BH
    bh_bin_mass  = np.zeros(TotNsubgroups, dtype=np.float32)
    bh_bin_grav_mass  = np.zeros(TotNsubgroups, dtype=np.float32)
    bh_mass  = np.zeros(TotNsubgroups, dtype=np.float32)
    bh_grav_mass  = np.zeros(TotNsubgroups, dtype=np.float32)
    bh_bin_N     = np.zeros(TotNsubgroups, dtype=np.int32)

    lin_bin_edges = np.linspace(0, 30, 1)  # *2 for proj and 3d
    log_bin_edges = np.logspace(-1, 3, 4*4+1)  # *2 for proj and 3d
    # + r<30kpc, r<r200, r<r_half, r<5*r_half, R=R_half, r=0.2rs
    # R=R_half, r<r200
    #   star ages: 0-2, 2-4, 4-6, 6-8, 8-10, 10-13.8

    # change if bins change
    n_lin = len(lin_bin_edges) - 1 #6
    n_log = len(log_bin_edges) - 1 #3
    n_scale = 8
    n_star_extra = 7*2
    n_dim = 2 * n_lin + n_log + n_lin + n_scale

    # a_t0 = 1
    # a_t2 = 0.8661993957678878
    # a_t4 = 0.7399616968593654
    # a_t6 = 0.6181186878735284
    # a_t8 = 0.49663550562470427
    # a_t10 = 0.3693965454245891
    # a_t12 = 0.22364939244663354
    # a_t14 = 0
    # a_bin_edges = np.array([a_t0, a_t2, a_t4, a_t6, a_t8, a_t10, a_t12, a_t14])
    t_t0 = 0
    t_t2 = 2
    t_t4 = 4
    t_t6 = 6
    t_t8 = 8
    t_t10 = 10
    t_t12 = 12
    t_t14 = 14
    t_bin_edges = np.array([t_t0, t_t2, t_t4, t_t6, t_t8, t_t10, t_t12, t_t14])

    lower_bin_edges = np.hstack((['r%.5f'%e for e in lin_bin_edges[:-1]], ['r%.5f'%e for e in log_bin_edges[:-1]],
                                 ['R%.5f'%e for e in lin_bin_edges[:-1]], ['R%.5f'%e for e in lin_bin_edges[:-1]],
                                 0, 0, 0, 0, '0.8 R1/2', '0.8 r1/2', '0.8 0.2 3/4 Rs', '0.8 0.2 rs',
                                 'rh 0-2', 'rh 2-4', 'rh 4-6', 'rh 6-8', 'rh 8-10', 'rh 10-12', 'rh 12-14',
                                 'r200 0-2', 'r200 2-4', 'r200 4-6', 'r200 6-8', 'r200 8-10', 'r200 10-12', 'r200 12-14')
                                ).astype(dtype=np.string_)
    upper_bin_edges = np.hstack((['r%.5f'%e for e in lin_bin_edges[1:]], ['r%.5f'%e for e in log_bin_edges[1:]],
                                 ['R%.5f'%e for e in lin_bin_edges[1:]], ['R%.5f'%e for e in lin_bin_edges[1:]],
                                 'r30', 'r200', 'r1/2', '5r1/2', '1.25 R1/2', '1.25 r1/2', '1.25 0.2 3/4 Rs', '1.25 0.2 rs',
                                 'rh 0-2', 'rh 2-4', 'rh 4-6', 'rh 6-8', 'rh 8-10', 'rh 10-12', 'rh 12-14',
                                 'r200 0-2', 'r200 2-4', 'r200 4-6', 'r200 6-8', 'r200 8-10', 'r200 10-12', 'r200 12-14')
                                ).astype(dtype=np.string_)

    # stars
    star_bin_mass = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_bin_N = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.int32)

    fjzjc = np.zeros((TotNsubgroups, n_dim + n_star_extra, n_jzjc_bins), dtype=np.float32)

    star_kappa_rot = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_kappa_co = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    star_mean_v_R = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_mean_v_phi = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_median_v_phi = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    star_sigma_z = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_sigma_R = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_sigma_phi = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    star_z_half = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    star_axis_a = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_axis_b = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_axis_c = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    star_J_z   = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)
    star_J_tot = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    star_formation_time_intervals = np.zeros((TotNsubgroups, n_dim + n_star_extra, n_formation_time_intervals), dtype=np.float32)
    star_formation_age_percentiles = np.zeros((TotNsubgroups, n_dim + n_star_extra, n_formation_age_percentiles), dtype=np.float32)

    star_r_12 = np.zeros((TotNsubgroups, n_dim + n_star_extra), dtype=np.float32)

    # gas
    gas_bin_mass  = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_bin_N     = np.zeros((TotNsubgroups,n_dim), dtype=np.int32)

    gas_kappa_rot = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_kappa_co  = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    gas_mean_v_R     = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_mean_v_phi   = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_median_v_phi = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    gas_sigma_z   = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_sigma_R   = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_sigma_phi = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    gas_z_half = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    gas_axis_a = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_axis_b = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    gas_axis_c = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    gas_J_z   = np.zeros((TotNsubgroups, n_dim), dtype=np.float32)
    gas_J_tot = np.zeros((TotNsubgroups, n_dim), dtype=np.float32)

    gas_r_12 = np.zeros((TotNsubgroups, n_dim), dtype=np.float32)

    #DM
    dm_N = np.zeros((TotNsubgroups,n_dim), dtype=np.int32)
    dm_bin_mass = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    dm_axis_a = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    dm_axis_b = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    dm_axis_c = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    dm_sigma_x = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    dm_sigma_y = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)
    dm_sigma_z = np.zeros((TotNsubgroups,n_dim), dtype=np.float32)

    dm_J_z   = np.zeros((TotNsubgroups, n_dim), dtype=np.float32)
    dm_J_tot = np.zeros((TotNsubgroups, n_dim), dtype=np.float32)

    start_time = time.time()

    for sub_index in range(TotNsubgroups):
        try:

            # extract ids
            # group_ids[sub_index] = sub_index +1
            # subgroup_ids[sub_index] = 0
            group_ids[sub_index] = GroupNumber[sub_index]
            subgroup_ids[sub_index] = SubGroupNumber[sub_index]

            # r200 = Group_R_Crit200[sub_index-1]  # eagle units
            r200 = Group_R_Crit200[group_ids[sub_index]-1]  # eagle units
            # centre_of_potential = GroupCentreOfPotential[sub_index]
            centre_of_potential = SubGroupCentreOfPotential[sub_index]

            # galaxy_m200[sub_index] = Group_M_Crit200[sub_index] / LITTLE_H  # 10^10 M_sun
            galaxy_m200[sub_index] = Group_M_Crit200[group_ids[sub_index]-1] / LITTLE_H  # 10^10 M_sun

            sub_cops[sub_index] = centre_of_potential
            group_cops[sub_index] = GroupCentreOfPotential[group_ids[sub_index]-1]

            pr200 = r200 * SCALE_A / LITTLE_H * 1000  # kcp
            galaxy_r200[sub_index] = pr200

            conc = concentration_ludlow(1/SCALE_A - 1, galaxy_m200[sub_index])
            rs = pr200 / conc

        except Exception as e:
            print('Exception was thrown calculating halo properties for group: ', sub_index)
            print(''.join(tb.format_exception(None, e, e.__traceback__)))
            pass

        try:

            # use the tree
            star_index_mask = star_kd_tree.query_ball_point(x=centre_of_potential,
                                                            r=r200)  # in Mpc*h/a
            # star_index_mask = star_kd_tree.query_ball_point(x=centre_of_potential,
            #                                                 r=30 / SCALE_A * LITTLE_H / 1000)  # in Mpc*h/a
            star_index_mask = np.array(star_index_mask)

            if len(star_index_mask) > 0:
                star_index_mask = star_index_mask[(np.abs(SubNum_Star[star_index_mask]) == SubGroupNumber[sub_index])]
                # star_index_mask = star_index_mask[(np.abs(SubNum_Star[star_index_mask]) == 0)]

            if len(star_index_mask) != 0:

                # mask relevant
                pos = PosStar[star_index_mask]
                vel = VelStar[star_index_mask]
                mass = MassStar[star_index_mask]
                initial_mass = InitialMassStar[star_index_mask]
                pot = BindingEnergyStar[star_index_mask]
                form_t = Star_tform[star_index_mask]

                age_bin = np.digitize(form_t, t_bin_edges)

                # real units and centre and everything
                pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
                pos %= SCALE_A * BOX_SIZE
                pos -= SCALE_A * BOX_SIZE / 2
                pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

                r = np.linalg.norm(pos, axis=1)
                mask = (r < 30)

                vel *= np.sqrt(SCALE_A)  # km/s
                mass /= LITTLE_H  # 10^10 M_sun
                initial_mass /= LITTLE_H  # 10^10 M_sun

                vel_offset = np.sum(mass[mask, np.newaxis] * vel[mask, :], axis=0) / np.sum(mass[mask])
                vel -= vel_offset

                # pot *= SCALE_A #(km/s)^2

                star_J_box[sub_index, :] = np.sum(mass[:, np.newaxis] * np.cross(pos, vel), axis=0)

                (pos, vel, rotation) = align(pos, vel, mass, apature=30)

                # cylindrical coords
                (R, phi, z, v_R, v_phi, v_z
                 ) = get_cylindrical(pos, vel)
                # r = np.linalg.norm(pos, axis=1)

                # j_z = np.cross(R, v_phi)[:, 2]
                j_z = pos[:, 0] * vel[:, 1] - pos[:, 1] * vel[:, 0]
                j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

                kin = 0.5 * (vel[:,0]**2 + vel[:,1]**2 + vel[:,2]**2)

                arg_energy = np.argsort(pot + kin)
                arg_arg = np.argsort(arg_energy)

                # TODO check
                j_c_E = max_within_50(j_tot[arg_energy])[arg_arg]
                j_zonc = j_z / j_c_E

                # aparures
                (r_half, r_quater, r3quater) = get_radii_that_are_interesting(r, mass)
                (R_half, R_quater, R3quater) = get_radii_that_are_interesting(R, mass)

                star_half_mass_radius[sub_index] = r_half
                star_quater_mass_radius[sub_index] = r_quater
                star3quater_mass_radius[sub_index] = r3quater
                star_half_mass_proj_radius[sub_index] = R_half
                star_quater_mass_proj_radius[sub_index] = R_quater
                star3quater_mass_proj_radius[sub_index] = R3quater

                # calculate profiles
                lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                proj_lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                           form_t, initial_mass)
                proj_log_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                           form_t, initial_mass)

                # calculate
                apature_30 = get_kinematic_apature(30, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                apature_r200 = get_kinematic_apature(pr200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                apature1half = get_kinematic_apature(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                apature_quater = get_kinematic_apature(5*r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)

                apature3quater = get_kinematic_annuli(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                annuli_half = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)

                apature_rs = get_kinematic_annuli(0.2*rs, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                apature_proj_Rs = get_kinematic_annuli(0.2*0.75*rs, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)

                # save profiles
                # linear
                star_bin_mass[sub_index, 0:n_lin] = lin_profiles[0]
                star_bin_mass[sub_index, n_lin:(n_lin + n_log)] = log_profiles[0]
                star_bin_mass[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[0]
                star_bin_mass[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[0]
                star_bin_mass[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[0], apature_r200[0],
                                                                          apature1half[0], apature_quater[0], apature3quater[0],
                                                                          annuli_half[0], apature_proj_Rs[0], apature_rs[0]]

                star_bin_N[sub_index, 0:n_lin] = lin_profiles[1]
                star_bin_N[sub_index, n_lin:(n_lin + n_log)] = log_profiles[1]
                star_bin_N[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[1]
                star_bin_N[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[1]
                star_bin_N[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[1], apature_r200[1],
                                                                       apature1half[1], apature_quater[1], apature3quater[1],
                                                                       annuli_half[1], apature_proj_Rs[1], apature_rs[1]]

                fjzjc[sub_index, 0:n_lin] = lin_profiles[2]
                fjzjc[sub_index, n_lin:(n_lin + n_log)] = log_profiles[2]
                fjzjc[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[2]
                fjzjc[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[2]
                fjzjc[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[2], apature_r200[2],
                                                                  apature1half[2], apature_quater[2], apature3quater[2],
                                                                  annuli_half[2], apature_proj_Rs[2], apature_rs[2]]

                star_kappa_rot[sub_index, 0:n_lin] = lin_profiles[3]
                star_kappa_rot[sub_index, n_lin:(n_lin + n_log)] = log_profiles[3]
                star_kappa_rot[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[3]
                star_kappa_rot[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[3]
                star_kappa_rot[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[3], apature_r200[3],
                                                                           apature1half[3], apature_quater[3],
                                                                           apature3quater[3],
                                                                           annuli_half[3], apature_proj_Rs[3], apature_rs[3]]
                star_kappa_co[sub_index, 0:n_lin] = lin_profiles[4]
                star_kappa_co[sub_index, n_lin:(n_lin + n_log)] = log_profiles[4]
                star_kappa_co[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[4]
                star_kappa_co[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[4]
                star_kappa_co[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[4], apature_r200[4],
                                                                            apature1half[4], apature_quater[4],
                                                                            apature3quater[4],
                                                                            annuli_half[4], apature_proj_Rs[4], apature_rs[4]]

                star_mean_v_R[sub_index, 0:n_lin] = lin_profiles[5]
                star_mean_v_R[sub_index, n_lin:(n_lin + n_log)] = log_profiles[5]
                star_mean_v_R[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[5]
                star_mean_v_R[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[5]
                star_mean_v_R[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[5], apature_r200[5],
                                                                            apature1half[5], apature_quater[5],
                                                                            apature3quater[5],
                                                                            annuli_half[5], apature_proj_Rs[5], apature_rs[5]]
                star_mean_v_phi[sub_index, 0:n_lin] = lin_profiles[6]
                star_mean_v_phi[sub_index, n_lin:(n_lin + n_log)] = log_profiles[6]
                star_mean_v_phi[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[6]
                star_mean_v_phi[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[6]
                star_mean_v_phi[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[6], apature_r200[6],
                                                                            apature1half[6], apature_quater[6],
                                                                            apature3quater[6],
                                                                            annuli_half[6], apature_proj_Rs[6], apature_rs[6]]
                star_median_v_phi[sub_index, 0:n_lin] = lin_profiles[7]
                star_median_v_phi[sub_index, n_lin:(n_lin + n_log)] = log_profiles[7]
                star_median_v_phi[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[7]
                star_median_v_phi[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[7]
                star_median_v_phi[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[7], apature_r200[7],
                                                                              apature1half[7], apature_quater[7],
                                                                              apature3quater[7],
                                                                              annuli_half[7],
                                                                           apature_proj_Rs[7], apature_rs[7]]

                star_sigma_z[sub_index, 0:n_lin] = lin_profiles[8]
                star_sigma_z[sub_index, n_lin:(n_lin + n_log)] = log_profiles[8]
                star_sigma_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[8]
                star_sigma_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[8]
                star_sigma_z[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[8], apature_r200[8],
                                                                         apature1half[8], apature_quater[8], apature3quater[8],
                                                                         annuli_half[8], apature_proj_Rs[8], apature_rs[8]]
                star_sigma_R[sub_index, 0:n_lin] = lin_profiles[9]
                star_sigma_R[sub_index, n_lin:(n_lin + n_log)] = log_profiles[9]
                star_sigma_R[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[9]
                star_sigma_R[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[9]
                star_sigma_R[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[9], apature_r200[9],
                                                                         apature1half[9], apature_quater[9], apature3quater[9],
                                                                         annuli_half[9], apature_proj_Rs[9], apature_rs[9]]
                star_sigma_phi[sub_index, 0:n_lin] = lin_profiles[10]
                star_sigma_phi[sub_index, n_lin:(n_lin + n_log)] = log_profiles[10]
                star_sigma_phi[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[10]
                star_sigma_phi[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[10]
                star_sigma_phi[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[10], apature_r200[10],
                                                                           apature1half[10], apature_quater[10],
                                                                           apature3quater[10],
                                                                           annuli_half[10], apature_proj_Rs[10],
                                                                        apature_rs[10]]

                star_z_half[sub_index, 0:n_lin] = lin_profiles[11]
                star_z_half[sub_index, n_lin:(n_lin + n_log)] = log_profiles[11]
                star_z_half[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[11]
                star_z_half[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[11]
                star_z_half[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[11], apature_r200[11],
                                                                        apature1half[11], apature_quater[11],
                                                                        apature3quater[11],
                                                                        annuli_half[11], apature_proj_Rs[11],
                                                                     apature_rs[11]]

                star_axis_a[sub_index, 0:n_lin] = lin_profiles[12]
                star_axis_a[sub_index, n_lin:(n_lin + n_log)] = log_profiles[12]
                star_axis_a[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[12]
                star_axis_a[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[12]
                star_axis_a[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[12], apature_r200[12],
                                                                        apature1half[12], apature_quater[12],
                                                                        apature3quater[12],
                                                                        annuli_half[12], apature_proj_Rs[12], apature_rs[12]]
                star_axis_b[sub_index, 0:n_lin] = lin_profiles[13]
                star_axis_b[sub_index, n_lin:(n_lin + n_log)] = log_profiles[13]
                star_axis_b[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[13]
                star_axis_b[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[13]
                star_axis_b[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[13], apature_r200[13],
                                                                        apature1half[13], apature_quater[13],
                                                                        apature3quater[13],
                                                                        annuli_half[13], apature_proj_Rs[13], apature_rs[13]]
                star_axis_c[sub_index, 0:n_lin] = lin_profiles[14]
                star_axis_c[sub_index, n_lin:(n_lin + n_log)] = log_profiles[14]
                star_axis_c[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[14]
                star_axis_c[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[14]
                star_axis_c[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[14], apature_r200[14],
                                                                        apature1half[14], apature_quater[14],
                                                                        apature3quater[14],
                                                                        annuli_half[14], apature_proj_Rs[14], apature_rs[14]]
                star_J_z[sub_index, 0:n_lin] = lin_profiles[15]
                star_J_z[sub_index, n_lin:(n_lin + n_log)] = log_profiles[15]
                star_J_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[15]
                star_J_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[15]
                star_J_z[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[15], apature_r200[15],
                                                                        apature1half[15], apature_quater[15],
                                                                        apature3quater[15],
                                                                        annuli_half[15], apature_proj_Rs[15],
                                                                  apature_rs[15]]
                star_J_tot[sub_index, 0:n_lin] = lin_profiles[16]
                star_J_tot[sub_index, n_lin:(n_lin + n_log)] = log_profiles[16]
                star_J_tot[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[16]
                star_J_tot[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[16]
                star_J_tot[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[16], apature_r200[16],
                                                                        apature1half[16], apature_quater[16],
                                                                        apature3quater[16],
                                                                        annuli_half[16], apature_proj_Rs[16],
                                                                    apature_rs[16]]

                star_formation_time_intervals[sub_index, 0:n_lin] = lin_profiles[17]
                star_formation_time_intervals[sub_index, n_lin:(n_lin + n_log)] = log_profiles[17]
                star_formation_time_intervals[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[17]
                star_formation_time_intervals[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[17]
                star_formation_time_intervals[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[17], apature_r200[17],
                                                                        apature1half[17], apature_quater[17],
                                                                        apature3quater[17],
                                                                        annuli_half[17], apature_proj_Rs[17],
                                                                                       apature_rs[17]]

                star_formation_age_percentiles[sub_index, 0:n_lin] = lin_profiles[18]
                star_formation_age_percentiles[sub_index, n_lin:(n_lin + n_log)] = log_profiles[18]
                star_formation_age_percentiles[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[18]
                star_formation_age_percentiles[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[18]
                star_formation_age_percentiles[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[18], apature_r200[18],
                                                                        apature1half[18], apature_quater[18],
                                                                        apature3quater[18],
                                                                        annuli_half[18], apature_proj_Rs[18], apature_rs[18]]

                star_r_12[sub_index, 0:n_lin] = lin_profiles[19]
                star_r_12[sub_index, n_lin:(n_lin + n_log)] = log_profiles[19]
                star_r_12[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[19]
                star_r_12[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[19]
                star_r_12[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[19], apature_r200[19],
                                                                        apature1half[19], apature_quater[19],
                                                                        apature3quater[19],
                                                                        annuli_half[19], apature_proj_Rs[19], apature_rs[19]]

                #age bins
                for i in range(len(t_bin_edges)-1):
                    if np.sum(age_bin == i+1) > 0:
                        a_pos = pos[age_bin == i+1]
                        a_vel = vel[age_bin == i+1]
                        a_mass = mass[age_bin == i+1]
                        a_r = r[age_bin == i+1]
                        a_z = z[age_bin == i+1]
                        a_v_R = v_R[age_bin == i+1]
                        a_v_phi = v_phi[age_bin == i+1]
                        a_v_z = v_z[age_bin == i+1]
                        a_j_zonc = j_zonc[age_bin == i+1]
                        a_form_t = form_t[age_bin == i+1]
                        a_initial_mass = initial_mass[age_bin == i+1]

                        a_annuli_half = get_kinematic_annuli(r_half, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                             a_j_zonc, a_form_t, a_initial_mass)
                        a_apature_r200 = get_kinematic_apature(pr200, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                               a_j_zonc, a_form_t, a_initial_mass)

                        star_bin_mass[sub_index, n_dim+i], star_bin_mass[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[0], a_apature_r200[0]]
                        star_bin_N[sub_index, n_dim+i], star_bin_N[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[1], a_apature_r200[1]]

                        fjzjc[sub_index, n_dim+i], fjzjc[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[2], a_apature_r200[2]]
                        star_kappa_rot[sub_index, n_dim+i], star_kappa_rot[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[3], a_apature_r200[3]]
                        star_kappa_co[sub_index, n_dim+i], star_kappa_co[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[4], a_apature_r200[4]]

                        star_mean_v_R[sub_index, n_dim+i], star_mean_v_R[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[5], a_apature_r200[5]]
                        star_mean_v_phi[sub_index, n_dim+i], star_mean_v_phi[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[6], a_apature_r200[6]]
                        star_median_v_phi[sub_index, n_dim+i], star_median_v_phi[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[7], a_apature_r200[7]]

                        star_sigma_z[sub_index, n_dim+i], star_sigma_z[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[8], a_apature_r200[8]]
                        star_sigma_R[sub_index, n_dim+i], star_sigma_R[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[9], a_apature_r200[9]]
                        star_sigma_phi[sub_index, n_dim+i], star_sigma_phi[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[10], a_apature_r200[10]]

                        star_z_half[sub_index, n_dim+i], star_z_half[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[11], a_apature_r200[11]]

                        star_axis_a[sub_index, n_dim+i], star_axis_a[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[12], a_apature_r200[12]]
                        star_axis_b[sub_index, n_dim+i], star_axis_b[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[13], a_apature_r200[13]]
                        star_axis_c[sub_index, n_dim+i], star_axis_c[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[14], a_apature_r200[14]]

                        star_J_z[sub_index, n_dim+i], star_J_z[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[15], a_apature_r200[15]]
                        star_J_tot[sub_index, n_dim+i], star_J_tot[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[16], a_apature_r200[16]]

                        star_formation_time_intervals[sub_index, n_dim+i], star_formation_time_intervals[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[17], a_apature_r200[17]]
                        star_formation_age_percentiles[sub_index, n_dim+i], star_formation_age_percentiles[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[18], a_apature_r200[18]]

                        star_r_12[sub_index, n_dim+i], star_r_12[sub_index, n_dim+i+n_star_extra//2] = [a_annuli_half[19], a_apature_r200[19]]


                # done stars

                try:
                    #only do dm profiles for centrals
                    if SubGroupNumber[sub_index] == 0:

                        # DM stuff
                        dm_index_mask = dm_kd_tree.query_ball_point(x=centre_of_potential,
                                                                    r=r200)
                        # dm_index_mask = dm_kd_tree.query_ball_point(x=centre_of_potential,
                        #                                             r = 30 / SCALE_A * LITTLE_H / 1000)
                        dm_index_mask = np.array(dm_index_mask)

                        if len(dm_index_mask) > 0:
                            dm_index_mask = dm_index_mask[(np.abs(SubNum_DM[dm_index_mask]) == SubGroupNumber[sub_index])]
                            # dm_index_mask = dm_index_mask[(np.abs(SubNum_DM[dm_index_mask]) == 0)]

                        if len(dm_index_mask) != 0:

                            dpos = PosDM[dm_index_mask]
                            dvel = VelDM[dm_index_mask]

                            # real units and centre and everything
                            dpos = dpos - centre_of_potential - SCALE_A * BOX_SIZE / 2
                            dpos %= SCALE_A * BOX_SIZE
                            dpos -= SCALE_A * BOX_SIZE / 2
                            dpos *= 1000 * SCALE_A / LITTLE_H  # to kpc

                            dvel *= np.sqrt(SCALE_A)  # km/s
                            dvel -= vel_offset

                            dm_J_box[sub_index, :] = np.sum(np.cross(dpos, dvel), axis=0) * dm_mass

                            dpos = rotation.apply(dpos)
                            dvel = rotation.apply(dvel)

                            # dm profiles
                            dm_r = np.linalg.norm(dpos, axis=1)
                            # dm_R = np.sqrt(dpos[:, 0] ** 2 + dpos[:, 1] ** 2)

                            dm_half_mask = np.logical_and(10 ** -0.1 < dm_r / R_half, dm_r / R_half < 10 ** 0.1)
                            dm_02rs_mask = np.logical_and(10 ** -0.1 < dm_r / r_half, dm_r / r_half < 10 ** 0.1)

                            dm_rs_mask = np.logical_and(10 ** -0.1 < dm_r / (0.2*rs), dm_r / (0.2*rs) < 10 ** 0.1)
                            dm_proj_rs_mask = np.logical_and(10 ** -0.1 < dm_r / (0.2*0.75*rs),
                                                             dm_r / (0.2*0.75*rs) < 10 ** 0.1)

                            # dm_quarter_mask = np.logical_and(10 ** -0.1 < dm_r / R_quater, dm_r / R_quater < 10 ** 0.1)
                            # dm3quarter_mask = np.logical_and(10 ** -0.1 < dm_r / R3quater, dm_r / R3quater < 10 ** 0.1)

                            # dm_N[sub_index, 0:n_lin] = np.histogram(dm_r, bins=lin_bin_edges)[0]
                            # dm_N[sub_index, n_lin:(n_lin+n_log)] = np.histogram(dm_r, bins=log_bin_edges)[0]
                            # dm_N[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # dm_N[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # dm_N[sub_index, (3 * n_lin + n_log):n_dim] = [np.sum(dm_r < 30), np.sum(dm_r < pr200),
                            #                                                  np.sum(dm_r < r_half), np.sum(dm_r < 5*r_half),
                            #                                                  np.sum(dm_02rs_mask),
                            #                                                  np.sum(dm_half_mask)]


                            # dm_bin_mass[sub_index, 0:n_lin] = np.histogram(dm_r, bins=lin_bin_edges)[0]
                            dm_bin_mass[sub_index, n_lin:(n_lin+n_log)] = np.histogram(dm_r, bins=log_bin_edges)[0]
                            # dm_bin_mass[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # dm_bin_mass[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            dm_bin_mass[sub_index, (3 * n_lin + n_log):n_dim] = [np.sum(dm_r < 30), np.sum(dm_r < pr200),
                                                                             np.sum(dm_r < r_half), np.sum(dm_r < 5*r_half),
                                                                             np.sum(dm_02rs_mask), np.sum(dm_half_mask),
                                                                             np.sum(dm_proj_rs_mask), np.sum(dm_rs_mask)]
                            dm_bin_mass[sub_index] *= dm_mass

                            # dm_sigma_x[sub_index, 0:n_lin] = binned_statistic(dm_r, dvel[:,0], bins=lin_bin_edges, statistic='std')[0]
                            # dm_sigma_y[sub_index, 0:n_lin] = binned_statistic(dm_r, dvel[:,1], bins=lin_bin_edges, statistic='std')[0]
                            # dm_sigma_z[sub_index, 0:n_lin] = binned_statistic(dm_r, dvel[:,2], bins=lin_bin_edges, statistic='std')[0]

                            dm_sigma_x[sub_index, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,0], bins=log_bin_edges, statistic='std')[0]
                            dm_sigma_y[sub_index, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,1], bins=log_bin_edges, statistic='std')[0]
                            dm_sigma_z[sub_index, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,2], bins=log_bin_edges, statistic='std')[0]

                            # dm_sigma_x[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = binned_statistic(dm_R, dvel[:,0], bins=lin_bin_edges, statistic='std')[0]
                            # dm_sigma_y[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = binned_statistic(dm_R, dvel[:,1], bins=lin_bin_edges, statistic='std')[0]
                            # dm_sigma_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = binned_statistic(dm_R, dvel[:,2], bins=lin_bin_edges, statistic='std')[0]

                            # dm_sigma_x[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = binned_statistic(dm_R, dvel[:,0], bins=log_bin_edges, statistic='std')[0]
                            # dm_sigma_y[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = binned_statistic(dm_R, dvel[:,1], bins=log_bin_edges, statistic='std')[0]
                            # dm_sigma_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = binned_statistic(dm_R, dvel[:,2], bins=log_bin_edges, statistic='std')[0]

                            dm_sigma_x[sub_index, (3 * n_lin + n_log):n_dim] = [np.std(dvel[dm_r < 30, 0]),
                                                                                   np.std(dvel[dm_r < pr200, 0]),
                                                                                   np.std(dvel[dm_r < r_half, 0]),
                                                                                   np.std(dvel[dm_r < 5*r_half, 0]),
                                                                                   np.std(dvel[dm_02rs_mask, 0]),
                                                                                   np.std(dvel[dm_half_mask, 0]),
                                                                                   np.std(dvel[dm_proj_rs_mask, 0]),
                                                                                   np.std(dvel[dm_rs_mask, 0])]
                            dm_sigma_y[sub_index, (3 * n_lin + n_log):n_dim] = [np.std(dvel[dm_r < 30, 1]),
                                                                                   np.std(dvel[dm_r < pr200, 1]),
                                                                                   np.std(dvel[dm_r < r_half, 1]),
                                                                                   np.std(dvel[dm_r < 5*r_half, 1]),
                                                                                   np.std(dvel[dm_02rs_mask, 1]),
                                                                                   np.std(dvel[dm_half_mask, 1]),
                                                                                   np.std(dvel[dm_proj_rs_mask, 1]),
                                                                                   np.std(dvel[dm_rs_mask, 1])]
                            dm_sigma_z[sub_index, (3 * n_lin + n_log):n_dim] = [np.std(dvel[dm_r < 30, 2]),
                                                                                   np.std(dvel[dm_r < pr200, 2]),
                                                                                   np.std(dvel[dm_r < r_half, 2]),
                                                                                   np.std(dvel[dm_r < 5*r_half, 2]),
                                                                                   np.std(dvel[dm_02rs_mask, 2]),
                                                                                   np.std(dvel[dm_half_mask, 2]),
                                                                                   np.std(dvel[dm_proj_rs_mask, 2]),
                                                                                   np.std(dvel[dm_rs_mask, 2])]

                            # j_is = np.cross(dpos, dvel)
                            # dm_J_x = np.zeros(n_dim)
                            # dm_J_y = np.zeros(n_dim)
                            #
                            # # dm_J_z[sub_index, 0:n_lin] = np.histogram(dm_r, bins=lin_bin_edges)[0]
                            # dm_J_z[sub_index, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, j_is[:, 2], bins=log_bin_edges,
                            #                                                             statistic=np.sum)[0]
                            # # dm_J_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # # dm_J_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # dm_J_z[sub_index, (3 * n_lin + n_log):n_dim] = [np.sum(j_is[dm_r < 30, 2]), np.sum(j_is[dm_r < pr200, 2]),
                            #                                                  np.sum(j_is[dm_r < r_half, 2]), np.sum(j_is[dm_r < 5*r_half, 2]),
                            #                                                  np.sum(j_is[dm_02rs_mask, 2]),
                            #                                                  np.sum(j_is[dm_half_mask, 2])]
                            #
                            # # dm_J_z[sub_index, 0:n_lin] = np.histogram(dm_r, bins=lin_bin_edges)[0]
                            # dm_J_x[n_lin:(n_lin+n_log)] = binned_statistic(dm_r, j_is[:, 0], bins=log_bin_edges,
                            #                                                             statistic=np.sum)[0]
                            # # dm_J_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # # dm_J_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # dm_J_x[(3 * n_lin + n_log):n_dim] = [np.sum(j_is[dm_r < 30, 0]), np.sum(j_is[dm_r < pr200, 0]),
                            #                                                  np.sum(j_is[dm_r < r_half, 0]), np.sum(j_is[dm_r < 5*r_half, 0]),
                            #                                                  np.sum(j_is[dm_02rs_mask, 0]),
                            #                                                  np.sum(j_is[dm_half_mask, 0])]
                            #
                            # dm_J_y[n_lin:(n_lin+n_log)] = binned_statistic(dm_r, j_is[:, 1], bins=log_bin_edges,
                            #                                                             statistic=np.sum)[0]
                            # # dm_J_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # # dm_J_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = np.histogram(dm_R, bins=lin_bin_edges)[0]
                            # dm_J_y[(3 * n_lin + n_log):n_dim] = [np.sum(j_is[dm_r < 30, 1]), np.sum(j_is[dm_r < pr200, 1]),
                            #                                                  np.sum(j_is[dm_r < r_half, 1]), np.sum(j_is[dm_r < 5*r_half, 1]),
                            #                                                  np.sum(j_is[dm_02rs_mask, 1]),
                            #                                                  np.sum(j_is[dm_half_mask, 1])]
                            #
                            # dm_J_tot[sub_index] = np.sqrt(dm_J_z[sub_index]**2 + dm_J_x**2 + dm_J_y**2)
                            #
                            # dm_J_z[sub_index] *= dm_mass
                            # dm_J_tot[sub_index] *= dm_mass

                        else:
                            # print('Warning: group', str(GroupNumber[sub_index]),
                            #       'subgroup', str(SubGroupNumber[sub_index]), 'has no DM particles.')
                            print(f'Warning: subgroup {sub_index} has no DM particles.')
                            # pass

                except Exception as e:
                    print(f'Exception was thrown calculating DM for subgroup: {sub_index}')
                    print(''.join(tb.format_exception(None, e, e.__traceback__)))

                # done DM

                try:
                    # bh stuff
                    bh_index_mask = bh_kd_tree.query_ball_point(x=centre_of_potential,
                                                                r=r200)
                    bh_index_mask = np.array(bh_index_mask)
                    #TODO only use the most massive BH

                    if len(bh_index_mask) > 0:
                        bh_index_mask = bh_index_mask[(np.abs(SubNum_BH[bh_index_mask]) == SubGroupNumber[sub_index])]

                    if len(bh_index_mask) != 0:

                        bh_bin_mass[sub_index] = np.sum(MassBH[bh_index_mask]) / LITTLE_H
                        bh_bin_grav_mass[sub_index] = np.sum(GravMassBH[bh_index_mask]) / LITTLE_H
                        bh_bin_N[sub_index] = len(bh_index_mask)

                        bh_mass[sub_index] = np.amax(MassBH[bh_index_mask]) / LITTLE_H
                        bh_grav_mass[sub_index] = np.amax(GravMassBH[bh_index_mask]) / LITTLE_H


                    else:
                        # print(f'Warning: subgroup {sub_index} has no BH.')
                        pass

                except Exception as e:
                    print(f'Exception was thrown calculating BH for group: {sub_index}')
                    print(''.join(tb.format_exception(None, e, e.__traceback__)))

                try:

                    # use the tree
                    gas_index_mask = gas_kd_tree.query_ball_point(x=centre_of_potential,
                                                                  r=r200)
                    # gas_index_mask = gas_kd_tree.query_ball_point(x=centre_of_potential,
                    #                                               r = 30 / SCALE_A * LITTLE_H / 1000)
                    gas_index_mask = np.array(gas_index_mask)

                    if len(gas_index_mask) > 0:
                        gas_index_mask = gas_index_mask[(np.abs(SubNum_Gas[gas_index_mask]) == SubGroupNumber[sub_index])]
                        # gas_index_mask = gas_index_mask[(np.abs(SubNum_Gas[gas_index_mask]) == 0)]

                    if len(gas_index_mask) != 0:

                        # mask relevant
                        pos = PosGas[gas_index_mask]
                        vel = VelGas[gas_index_mask]
                        mass = MassGas[gas_index_mask]
                        pot = BindingEnergyGas[gas_index_mask]
                        initial_mass = np.zeros(len(mass))
                        form_t = np.zeros(len(mass))

                        # real units and centre and everything
                        pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
                        pos %= SCALE_A * BOX_SIZE
                        pos -= SCALE_A * BOX_SIZE / 2
                        pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

                        r = np.linalg.norm(pos, axis=1)
                        # mask = (r < 30)

                        vel *= np.sqrt(SCALE_A)  # km/s
                        mass /= LITTLE_H  # 10^10 M_sun

                        vel -= vel_offset

                        # pot *= SCALE_A #(km/s)^2

                        gas_J_box[sub_index, :] = np.sum(mass[:, np.newaxis] * np.cross(pos, vel), axis=0)

                        # (pos, vel, rotation) = align(pos, vel, mass, apature=30)
                        pos = rotation.apply(pos)
                        vel = rotation.apply(vel)

                        # cylindrical coords
                        (R, phi, z, v_R, v_phi, v_z
                         ) = get_cylindrical(pos, vel)
                        # r = np.linalg.norm(pos, axis=1)

                        # j_z = np.cross(R, v_phi)
                        # j_z = pos[:, 0] * vel[:, 1] - pos[:, 1] * vel[:, 0]
                        # j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

                        # arg_pot = np.argsort(pot)
                        # arg_arg = np.argsort(arg_pot)

                        # j_c_E = max_within_50(j_tot[arg_pot])[arg_arg]
                        # j_zonc = j_z / j_c_E
                        j_zonc = np.zeros(len(mass))

                        (gas_r_half, gas_r_quater, gas_r3quater) = get_radii_that_are_interesting(r, mass)
                        (gas_R_half, gas_R_quater, gas_R3quater) = get_radii_that_are_interesting(R, mass)

                        gas_half_mass_radius[sub_index] = gas_r_half
                        gas_quater_mass_radius[sub_index] = gas_r_quater
                        gas3quater_mass_radius[sub_index] = gas_r3quater
                        gas_half_mass_proj_radius[sub_index] = gas_R_half
                        gas_quater_mass_proj_radius[sub_index] = gas_R_quater
                        gas3quater_mass_proj_radius[sub_index] = gas_R3quater

                        # calculate profiles
                        lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z,
                                                              j_zonc, form_t, initial_mass)
                        log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z,
                                                              j_zonc, form_t, initial_mass)
                        proj_lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z,
                                                                   j_zonc, form_t, initial_mass)
                        proj_log_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z,
                                                                   j_zonc, form_t, initial_mass)

                        # calculate
                        apature_30 = get_kinematic_apature(30, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                        apature_r200 = get_kinematic_apature(pr200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                                             form_t, initial_mass)

                        # if (r_half, r_quater, r3quater) == (0, 0, 0) and (R_half, R_quater, R3quater) == (0, 0, 0):
                        #     # aparures
                        #     (r_half, r_quater, r3quater) = get_radii_that_are_interesting(r, mass)
                        #     (R_half, R_quater, R3quater) = get_radii_that_are_interesting(R, mass)
                        #
                        #     star_half_mass_radius[sub_index] = r_half
                        #     star_quater_mass_radius[sub_index] = r_quater
                        #     star3quater_mass_radius[sub_index] = r3quater
                        #     star_half_mass_proj_radius[sub_index] = R_half
                        #     star_quater_mass_proj_radius[sub_index] = R_quater
                        #     star3quater_mass_proj_radius[sub_index] = R3quater

                        apature1half = get_kinematic_apature(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                                             form_t, initial_mass)
                        apature_quater = get_kinematic_apature(5*r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                                               form_t, initial_mass)

                        # apature3quater = get_kinematic_annuli(0.2*rs, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                        #                                        form_t, initial_mass)
                        apature3quater = get_kinematic_annuli(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                                               form_t, initial_mass)
                        annuli_half = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                           form_t, initial_mass)

                        apature_rs = get_kinematic_annuli(0.2 * rs, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                                          form_t, initial_mass)
                        apature_proj_Rs = get_kinematic_annuli(0.2 * 0.75 * rs, pos, vel, mass, R, z, v_R, v_phi, v_z,
                                                               j_zonc, form_t, initial_mass)

                        # annuli_quarter = get_kinematic_annuli(R_quater, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                        #                                       form_t, initial_mass)
                        # annuli3quarter = get_kinematic_annuli(R3quater, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                        #                                       form_t, initial_mass)
                        # annuli_half   = get_kinematic_apature(r_half,   pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                        # apature_quater   = get_kinematic_apature(2*r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                        # annuli_quarter = get_kinematic_apature(r_quater, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)
                        # annuli3quarter = get_kinematic_apature(r3quater, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass)

                        # save profiles
                        # linear
                        gas_bin_mass[sub_index, 0:n_lin] = lin_profiles[0]
                        gas_bin_mass[sub_index, n_lin:(n_lin + n_log)] = log_profiles[0]
                        gas_bin_mass[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[0]
                        gas_bin_mass[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[0]
                        gas_bin_mass[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[0], apature_r200[0],
                                                                                 apature1half[0], apature_quater[0],
                                                                                 apature3quater[0],
                                                                                 annuli_half[0], apature_proj_Rs[0],
                                                                              apature_rs[0]]

                        gas_bin_N[sub_index, 0:n_lin] = lin_profiles[1]
                        gas_bin_N[sub_index, n_lin:(n_lin + n_log)] = log_profiles[1]
                        gas_bin_N[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[1]
                        gas_bin_N[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[1]
                        gas_bin_N[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[1], apature_r200[1],
                                                                              apature1half[1], apature_quater[1],
                                                                              apature3quater[1],
                                                                              annuli_half[1], apature_proj_Rs[1],
                                                                           apature_rs[1]]

                        gas_kappa_rot[sub_index, 0:n_lin] = lin_profiles[3]
                        gas_kappa_rot[sub_index, n_lin:(n_lin + n_log)] = log_profiles[3]
                        gas_kappa_rot[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[3]
                        gas_kappa_rot[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[3]
                        gas_kappa_rot[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[3], apature_r200[3],
                                                                                  apature1half[3], apature_quater[3],
                                                                                  apature3quater[3],
                                                                                  annuli_half[3], apature_proj_Rs[3],
                                                                               apature_rs[3]]
                        gas_kappa_co[sub_index, 0:n_lin] = lin_profiles[4]
                        gas_kappa_co[sub_index, n_lin:(n_lin + n_log)] = log_profiles[4]
                        gas_kappa_co[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[4]
                        gas_kappa_co[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[4]
                        gas_kappa_co[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[4], apature_r200[4],
                                                                                 apature1half[4], apature_quater[4],
                                                                                 apature3quater[4],
                                                                                 annuli_half[4], apature_proj_Rs[4],
                                                                              apature_rs[4]]

                        gas_mean_v_R[sub_index, 0:n_lin] = lin_profiles[5]
                        gas_mean_v_R[sub_index, n_lin:(n_lin + n_log)] = log_profiles[5]
                        gas_mean_v_R[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[5]
                        gas_mean_v_R[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[5]
                        gas_mean_v_R[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[5], apature_r200[5],
                                                                                 apature1half[5], apature_quater[5],
                                                                                 apature3quater[5],
                                                                                 annuli_half[5], apature_proj_Rs[5],
                                                                              apature_rs[5]]
                        gas_mean_v_phi[sub_index, 0:n_lin] = lin_profiles[6]
                        gas_mean_v_phi[sub_index, n_lin:(n_lin + n_log)] = log_profiles[6]
                        gas_mean_v_phi[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[6]
                        gas_mean_v_phi[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[6]
                        gas_mean_v_phi[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[6], apature_r200[6],
                                                                                   apature1half[6], apature_quater[6],
                                                                                   apature3quater[6],
                                                                                   annuli_half[6], apature_proj_Rs[6],
                                                                                apature_rs[6]]
                        gas_median_v_phi[sub_index, 0:n_lin] = lin_profiles[7]
                        gas_median_v_phi[sub_index, n_lin:(n_lin + n_log)] = log_profiles[7]
                        gas_median_v_phi[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[7]
                        gas_median_v_phi[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[7]
                        gas_median_v_phi[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[7], apature_r200[7],
                                                                                     apature1half[7], apature_quater[7],
                                                                                     apature3quater[7],
                                                                                     annuli_half[7], apature_proj_Rs[7],
                                                                                  apature_rs[7]]

                        gas_sigma_z[sub_index, 0:n_lin] = lin_profiles[8]
                        gas_sigma_z[sub_index, n_lin:(n_lin + n_log)] = log_profiles[8]
                        gas_sigma_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[8]
                        gas_sigma_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[8]
                        gas_sigma_z[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[8], apature_r200[8],
                                                                                apature1half[8], apature_quater[8],
                                                                                apature3quater[8],
                                                                                annuli_half[8], apature_proj_Rs[8],
                                                                             apature_rs[8]]
                        gas_sigma_R[sub_index, 0:n_lin] = lin_profiles[9]
                        gas_sigma_R[sub_index, n_lin:(n_lin + n_log)] = log_profiles[9]
                        gas_sigma_R[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[9]
                        gas_sigma_R[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[9]
                        gas_sigma_R[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[9], apature_r200[9],
                                                                                apature1half[9], apature_quater[9],
                                                                                apature3quater[9],
                                                                                annuli_half[9], apature_proj_Rs[9],
                                                                             apature_rs[9]]
                        gas_sigma_phi[sub_index, 0:n_lin] = lin_profiles[10]
                        gas_sigma_phi[sub_index, n_lin:(n_lin + n_log)] = log_profiles[10]
                        gas_sigma_phi[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[10]
                        gas_sigma_phi[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[10]
                        gas_sigma_phi[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[10], apature_r200[10],
                                                                                  apature1half[10], apature_quater[10],
                                                                                  apature3quater[10],
                                                                                  annuli_half[10], apature_proj_Rs[10],
                                                                               apature_rs[10]]

                        gas_z_half[sub_index, 0:n_lin] = lin_profiles[11]
                        gas_z_half[sub_index, n_lin:(n_lin + n_log)] = log_profiles[11]
                        gas_z_half[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[11]
                        gas_z_half[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[11]
                        gas_z_half[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[11], apature_r200[11],
                                                                               apature1half[11], apature_quater[11],
                                                                               apature3quater[11],
                                                                               annuli_half[11], apature_proj_Rs[11],
                                                                            apature_rs[11]]

                        gas_axis_a[sub_index, 0:n_lin] = lin_profiles[12]
                        gas_axis_a[sub_index, n_lin:(n_lin + n_log)] = log_profiles[12]
                        gas_axis_a[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[12]
                        gas_axis_a[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[12]
                        gas_axis_a[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[12], apature_r200[12],
                                                                               apature1half[12], apature_quater[12],
                                                                               apature3quater[12],
                                                                               annuli_half[12], apature_proj_Rs[12],
                                                                            apature_rs[12]]
                        gas_axis_b[sub_index, 0:n_lin] = lin_profiles[13]
                        gas_axis_b[sub_index, n_lin:(n_lin + n_log)] = log_profiles[13]
                        gas_axis_b[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[13]
                        gas_axis_b[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[13]
                        gas_axis_b[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[13], apature_r200[13],
                                                                               apature1half[13], apature_quater[13],
                                                                               apature3quater[13],
                                                                               annuli_half[13], apature_proj_Rs[13],
                                                                            apature_rs[13]]
                        gas_axis_c[sub_index, 0:n_lin] = lin_profiles[14]
                        gas_axis_c[sub_index, n_lin:(n_lin + n_log)] = log_profiles[14]
                        gas_axis_c[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[14]
                        gas_axis_c[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[14]
                        gas_axis_c[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[14], apature_r200[14],
                                                                               apature1half[14], apature_quater[14],
                                                                               apature3quater[14],
                                                                               annuli_half[14], apature_proj_Rs[14],
                                                                            apature_rs[14]]
                        gas_J_z[sub_index, 0:n_lin] = lin_profiles[15]
                        gas_J_z[sub_index, n_lin:(n_lin + n_log)] = log_profiles[15]
                        gas_J_z[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[15]
                        gas_J_z[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[15]
                        gas_J_z[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[15], apature_r200[15],
                                                                               apature1half[15], apature_quater[15],
                                                                               apature3quater[15],
                                                                               annuli_half[15], apature_proj_Rs[15],
                                                                         apature_rs[15]]
                        gas_J_tot[sub_index, 0:n_lin] = lin_profiles[16]
                        gas_J_tot[sub_index, n_lin:(n_lin + n_log)] = log_profiles[16]
                        gas_J_tot[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[16]
                        gas_J_tot[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[16]
                        gas_J_tot[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[16], apature_r200[16],
                                                                               apature1half[16], apature_quater[16],
                                                                               apature3quater[16],
                                                                               annuli_half[16], apature_proj_Rs[16],
                                                                           apature_rs[16]]

                        gas_r_12[sub_index, 0:n_lin] = lin_profiles[19]
                        gas_r_12[sub_index, n_lin:(n_lin + n_log)] = log_profiles[19]
                        gas_r_12[sub_index, (n_lin + n_log):(2 * n_lin + n_log)] = proj_lin_profiles[19]
                        gas_r_12[sub_index, (2 * n_lin + n_log):(3 * n_lin + n_log)] = proj_log_profiles[19]
                        gas_r_12[sub_index, (3 * n_lin + n_log):n_dim] = [apature_30[19], apature_r200[19],
                                                                           apature1half[19], apature_quater[19],
                                                                           apature3quater[19],
                                                                           annuli_half[19], apature_proj_Rs[19],
                                                                           apature_rs[19]]

                    else:
                        # print('Warning: group', str(GroupNumber[sub_index]),
                        #       'subgroup', str(SubGroupNumber[sub_index]), 'has no gas particles.')
                        pass

                except Exception as e:
                    print(f'Exception was thrown calculating gas for subgroup: {sub_index}')
                    print(''.join(tb.format_exception(None, e, e.__traceback__)))

            else:
                # if galaxy_m200[sub_index] > 0.3 and SubGroupNumber[sub_index] == 1:  # > ~10^9.5 M_sun #all halos should be occupied? Tinker?
                #     print('Warning: group', GroupNumber[sub_index], 'has no star particles with subhalo==0.')

                galaxy_has_stars[sub_index] = False

                # vel_offset = np.zeros(3)
                #
                # r_half, r_quater, r3quater = 0, 0, 0
                # R_half, R_quater, R3quater = 0, 0, 0
                # # pass

        except Exception as e:
            print(f'Exception was thrown calculating stars for subgroup: {sub_index}')
            print(''.join(tb.format_exception(None, e, e.__traceback__)))


        if sub_index % 100 == 0 or sub_index < 10:

            print('done ' + str(sub_index) + ' in ' + str(round(time.time() - start_time, 1)) + 's. ',
                  end='')

            if sub_index < 99:
                this_time = time.time()

                print()

            else:
                last_hundred_time = time.time() - this_time

                time_to_finish = last_hundred_time / 100 * (TotNsubgroups - sub_index)

                this_time = time.time()

                print(str(round(time_to_finish, -2))[:-2] + 's to finish at this rate.')

    print('Done all calculations.')

    group_ids = group_ids[galaxy_has_stars]
    subgroup_ids = subgroup_ids[galaxy_has_stars]

    star_half_mass_radius = star_half_mass_radius[galaxy_has_stars]
    star_quater_mass_radius = star_quater_mass_radius[galaxy_has_stars]
    star3quater_mass_radius = star3quater_mass_radius[galaxy_has_stars]
    star_half_mass_proj_radius = star_half_mass_proj_radius[galaxy_has_stars]
    star_quater_mass_proj_radius = star_quater_mass_proj_radius[galaxy_has_stars]
    star3quater_mass_proj_radius = star3quater_mass_proj_radius[galaxy_has_stars]
    gas_half_mass_radius = gas_half_mass_radius[galaxy_has_stars]
    gas_quater_mass_radius = gas_quater_mass_radius[galaxy_has_stars]
    gas3quater_mass_radius = gas3quater_mass_radius[galaxy_has_stars]
    gas_half_mass_proj_radius = gas_half_mass_proj_radius[galaxy_has_stars]
    gas_quater_mass_proj_radius = gas_quater_mass_proj_radius[galaxy_has_stars]
    gas3quater_mass_proj_radius = gas3quater_mass_proj_radius[galaxy_has_stars]
    galaxy_r200 = galaxy_r200[galaxy_has_stars]
    galaxy_m200 = galaxy_m200[galaxy_has_stars]
    group_cops = group_cops[galaxy_has_stars]
    sub_cops = sub_cops[galaxy_has_stars]

    star_J_box = star_J_box[galaxy_has_stars]
    gas_J_box = gas_J_box[galaxy_has_stars]
    dm_J_box = dm_J_box[galaxy_has_stars]

    # stars
    star_bin_mass = star_bin_mass[galaxy_has_stars]
    star_bin_N = star_bin_N[galaxy_has_stars]

    fjzjc = fjzjc[galaxy_has_stars]

    star_kappa_rot = star_kappa_rot[galaxy_has_stars]
    star_kappa_co = star_kappa_co[galaxy_has_stars]

    star_mean_v_R = star_mean_v_R[galaxy_has_stars]
    star_mean_v_phi = star_mean_v_phi[galaxy_has_stars]
    star_median_v_phi = star_median_v_phi[galaxy_has_stars]

    star_sigma_z = star_sigma_z[galaxy_has_stars]
    star_sigma_R = star_sigma_R[galaxy_has_stars]
    star_sigma_phi = star_sigma_phi[galaxy_has_stars]

    star_z_half = star_z_half[galaxy_has_stars]

    star_axis_a = star_axis_a[galaxy_has_stars]
    star_axis_b = star_axis_b[galaxy_has_stars]
    star_axis_c = star_axis_c[galaxy_has_stars]

    star_J_z   = star_J_z[galaxy_has_stars]
    star_J_tot = star_J_tot[galaxy_has_stars]

    star_formation_time_intervals = star_formation_time_intervals[galaxy_has_stars]
    star_formation_age_percentiles = star_formation_age_percentiles[galaxy_has_stars]

    star_r_12 = star_r_12[galaxy_has_stars]

    # gas
    gas_bin_mass  = gas_bin_mass[galaxy_has_stars]
    gas_bin_N     = gas_bin_N[galaxy_has_stars]

    gas_kappa_rot = gas_kappa_rot[galaxy_has_stars]
    gas_kappa_co  = gas_kappa_co[galaxy_has_stars]

    gas_mean_v_R     = gas_mean_v_R[galaxy_has_stars]
    gas_mean_v_phi   = gas_mean_v_phi[galaxy_has_stars]
    gas_median_v_phi = gas_median_v_phi[galaxy_has_stars]

    gas_sigma_z   = gas_sigma_z[galaxy_has_stars]
    gas_sigma_R   = gas_sigma_R[galaxy_has_stars]
    gas_sigma_phi = gas_sigma_phi[galaxy_has_stars]

    gas_z_half = gas_z_half[galaxy_has_stars]

    gas_axis_a = gas_axis_a[galaxy_has_stars]
    gas_axis_b = gas_axis_b[galaxy_has_stars]
    gas_axis_c = gas_axis_c[galaxy_has_stars]

    gas_J_z   = gas_J_z[galaxy_has_stars]
    gas_J_tot = gas_J_tot[galaxy_has_stars]

    gas_r_12 = gas_r_12[galaxy_has_stars]

    #DM
    dm_bin_mass = dm_bin_mass[galaxy_has_stars]

    dm_sigma_x = dm_sigma_x[galaxy_has_stars]
    dm_sigma_y = dm_sigma_y[galaxy_has_stars]
    dm_sigma_z = dm_sigma_z[galaxy_has_stars]

    dm_J_z = dm_J_z[galaxy_has_stars]
    dm_J_tot = dm_J_tot[galaxy_has_stars]

    #BH
    bh_bin_mass = bh_bin_mass[galaxy_has_stars]
    bh_bin_grav_mass = bh_bin_grav_mass[galaxy_has_stars]
    bh_bin_N = bh_bin_N[galaxy_has_stars]

    bh_mass = bh_mass[galaxy_has_stars]
    bh_grav_mass = bh_grav_mass[galaxy_has_stars]

    print('Removed haloes with no stars')
    print('Removed', len(galaxy_has_stars) - np.sum(galaxy_has_stars), 'haloes. ', np.sum(galaxy_has_stars) ,'haloes remaining.')

    return (output_data_location, snap,
            lower_bin_edges, upper_bin_edges,
            group_ids, subgroup_ids,
            galaxy_r200, galaxy_m200, group_cops, sub_cops,
            star_half_mass_proj_radius, star_quater_mass_proj_radius, star3quater_mass_proj_radius,
            star_half_mass_radius, star_quater_mass_radius, star3quater_mass_radius,
            gas_half_mass_proj_radius, gas_quater_mass_proj_radius, gas3quater_mass_proj_radius,
            gas_half_mass_radius, gas_quater_mass_radius, gas3quater_mass_radius,
            star_J_box, gas_J_box, dm_J_box,
            star_bin_mass, star_bin_N, fjzjc,
            star_kappa_rot, star_kappa_co,
            star_mean_v_R, star_mean_v_phi, star_median_v_phi,
            star_sigma_z, star_sigma_R, star_sigma_phi,
            star_z_half, star_axis_a, star_axis_b, star_axis_c,
            star_J_z, star_J_tot,
            star_formation_time_intervals, star_formation_age_percentiles, star_r_12,
            gas_bin_mass, gas_bin_N,
            gas_kappa_rot, gas_kappa_co,
            gas_mean_v_R, gas_mean_v_phi, gas_median_v_phi,
            gas_sigma_z, gas_sigma_R, gas_sigma_phi,
            gas_z_half, gas_axis_a, gas_axis_b, gas_axis_c,
            gas_J_z, gas_J_tot, gas_r_12,
            dm_bin_mass,
            dm_sigma_x, dm_sigma_y, dm_sigma_z,
            dm_J_z, dm_J_tot,
            bh_bin_mass, bh_bin_grav_mass, bh_bin_N,
            bh_mass, bh_grav_mass)


def my_write(output_data_location, snap,
             lower_bin_edges, upper_bin_edges,
             group_ids, subgroup_ids,
             galaxy_r200, galaxy_m200, group_cops, sub_cops,
             star_half_mass_proj_radius, star_quater_mass_proj_radius, star3quater_mass_proj_radius,
             star_half_mass_radius, star_quater_mass_radius, star3quater_mass_radius,
             gas_half_mass_proj_radius, gas_quater_mass_proj_radius, gas3quater_mass_proj_radius,
             gas_half_mass_radius, gas_quater_mass_radius, gas3quater_mass_radius,
             star_J_box, gas_J_box, dm_J_box,
             star_bin_mass, star_bin_N, fjzjc,
             star_kappa_rot, star_kappa_co,
             star_mean_v_R, star_mean_v_phi, star_median_v_phi,
             star_sigma_z, star_sigma_R, star_sigma_phi,
             star_z_half, star_axis_a, star_axis_b, star_axis_c,
             star_J_z, star_J_tot,
             star_formation_time_intervals, star_formation_age_percentiles, star_r_12,
             gas_bin_mass, gas_bin_N,
             gas_kappa_rot, gas_kappa_co,
             gas_mean_v_R, gas_mean_v_phi, gas_median_v_phi,
             gas_sigma_z, gas_sigma_R, gas_sigma_phi,
             gas_z_half, gas_axis_a, gas_axis_b, gas_axis_c,
             gas_J_z, gas_J_tot, gas_r_12,
             dm_bin_mass,
             dm_sigma_x, dm_sigma_y, dm_sigma_z,
             dm_J_z, dm_J_tot,
             bh_bin_mass, bh_bin_grav_mass, bh_bin_N,
             bh_mass, bh_grav_mass):

    file_name = output_data_location + snap + '_galaxy_kinematic_profile.hdf5'
    print('Output file ', file_name)
    # profile file
    with h5.File(file_name, 'w') as output:

        #TODO put bins etc. in header
        header = output.create_group('Header')
        header.create_dataset('LowerBinEdges', data=lower_bin_edges)
        header.create_dataset('UpperBinEdges', data=upper_bin_edges)
        dm_mass = DM_MASS
        if '7x' in file_name:
            dm_mass /= 7
        header.create_dataset('MDMReciprocal', data=np.array([1 / dm_mass]))
        header.create_dataset('MDM', data=np.array([dm_mass]))
        header.create_dataset('scalefactor', data=np.array([SCALE_A]))

        quant = output.create_group('GalaxyQuantities')
        prof = output.create_group('GalaxyProfiles')
        star_prof = prof.create_group('Star')
        gas_prof = prof.create_group('Gas')
        dm_prof = prof.create_group('DM')
        bh_prof = prof.create_group('BH')

        quant.create_dataset('GroupNumber',    data = group_ids)
        quant.create_dataset('SubGroupNumber', data = subgroup_ids)

        quant.create_dataset('R200_crit', data=galaxy_r200)
        quant.create_dataset('M200_crit', data=galaxy_m200)
        quant.create_dataset('GroupCOP', data=group_cops)
        quant.create_dataset('SubGroupCOP', data=sub_cops)

        quant.create_dataset('StellarHalfMassRadius', data=star_half_mass_radius)
        quant.create_dataset('StellarQuaterMassRadius', data=star_quater_mass_radius)
        quant.create_dataset('Stellar3QuaterMassRadius', data=star3quater_mass_radius)
        quant.create_dataset('StellarHalfMassProjRadius', data=star_half_mass_proj_radius)
        quant.create_dataset('StellarQuaterMassProjRadius', data=star_quater_mass_proj_radius)
        quant.create_dataset('Stellar3QuaterMassProjRadius', data=star3quater_mass_proj_radius)
        quant.create_dataset('GasHalfMassRadius', data=gas_half_mass_radius)
        quant.create_dataset('GasQuaterMassRadius', data=gas_quater_mass_radius)
        quant.create_dataset('Gas3QuaterMassRadius', data=gas3quater_mass_radius)
        quant.create_dataset('GasHalfMassProjRadius', data=gas_half_mass_proj_radius)
        quant.create_dataset('GasQuaterMassProjRadius', data=gas_quater_mass_proj_radius)
        quant.create_dataset('Gas3QuaterMassProjRadius', data=gas3quater_mass_proj_radius)

        quant.create_dataset('JboxStar', data=star_J_box)
        quant.create_dataset('JboxGas', data=gas_J_box)
        quant.create_dataset('JboxDM', data=dm_J_box)

        # bh
        quant.create_dataset('BHBinMass', data=bh_bin_mass)
        quant.create_dataset('BHBinGravMass', data=bh_bin_grav_mass)
        quant.create_dataset('BHN', data=bh_bin_N)
        quant.create_dataset('BHMass', data=bh_mass)
        quant.create_dataset('BHGravMass', data=bh_grav_mass)

        # star
        star_prof.create_dataset('BinMass', data=star_bin_mass)
        star_prof.create_dataset('BinN', data=star_bin_N)

        star_prof.create_dataset('fjzjc', data=fjzjc)

        star_prof.create_dataset('kappaRot', data=star_kappa_rot)
        star_prof.create_dataset('kappaCo', data=star_kappa_co)

        star_prof.create_dataset('MeanVR', data=star_mean_v_R)
        star_prof.create_dataset('MeanVphi', data=star_mean_v_phi)
        star_prof.create_dataset('MedianVphi', data=star_median_v_phi)

        star_prof.create_dataset('sigmaz', data=star_sigma_z)
        star_prof.create_dataset('sigmaR', data=star_sigma_R)
        star_prof.create_dataset('sigmaphi', data=star_sigma_phi)

        star_prof.create_dataset('zHalf', data=star_z_half)

        star_prof.create_dataset('Axisa', data=star_axis_a)
        star_prof.create_dataset('Axisb', data=star_axis_b)
        star_prof.create_dataset('Axisc', data=star_axis_c)

        star_prof.create_dataset('Jz',   data=star_J_z)
        star_prof.create_dataset('Jtot', data=star_J_tot)

        star_prof.create_dataset('StarFormationRates',   data=star_formation_time_intervals)
        star_prof.create_dataset('PercentileStellarAge', data=star_formation_age_percentiles)

        star_prof.create_dataset('rHalf', data=star_r_12)

        # gas
        gas_prof.create_dataset('BinMass', data=gas_bin_mass)
        gas_prof.create_dataset('BinN', data=gas_bin_N)

        gas_prof.create_dataset('kappaRot', data=gas_kappa_rot)
        gas_prof.create_dataset('kappaCo', data=gas_kappa_co)

        gas_prof.create_dataset('MeanVR', data=gas_mean_v_R)
        gas_prof.create_dataset('MeanVphi', data=gas_mean_v_phi)
        gas_prof.create_dataset('MedianVphi', data=gas_median_v_phi)

        gas_prof.create_dataset('sigmaz', data=gas_sigma_z)
        gas_prof.create_dataset('sigmaR', data=gas_sigma_R)
        gas_prof.create_dataset('sigmaphi', data=gas_sigma_phi)

        gas_prof.create_dataset('zHalf', data=gas_z_half)

        gas_prof.create_dataset('Axisa', data=gas_axis_a)
        gas_prof.create_dataset('Axisb', data=gas_axis_b)
        gas_prof.create_dataset('Axisc', data=gas_axis_c)

        gas_prof.create_dataset('Jz',   data=gas_J_z)
        gas_prof.create_dataset('Jtot', data=gas_J_tot)

        gas_prof.create_dataset('rHalf', data=gas_r_12)

        # dm
        dm_prof.create_dataset('BinMass', data=dm_bin_mass)

        dm_prof.create_dataset('DMsigmax', data=dm_sigma_x)
        dm_prof.create_dataset('DMsigmay', data=dm_sigma_y)
        dm_prof.create_dataset('DMsigmaz', data=dm_sigma_z)

        dm_prof.create_dataset('Jz',   data=dm_J_z)
        dm_prof.create_dataset('Jtot', data=dm_J_tot)

    print('File written.')

    return

####

#to get star particle age
def lbt(a=None,omm=0.307,oml=0.693,h=0.6777):
    z = 1.0/a-1
    t = np.zeros(len(z))

    for i in range(len(z)):
        if a[i] == 0:
            t[i] = 13.82968685
        else:
            t[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100*h)) * quad(
                lambda z: 1 / ( (1+z) * np.sqrt(omm*(1+z)**3 + oml) ), 0, z[i])[0] #in billion years
    return(t)


def lbt_interp(a, omm=0.307, oml=0.693, h=0.6777, n=10_001):
    a_interp = np.linspace(0,1, n)
    z_interp = 1 / a_interp - 1
    t_interp = np.zeros(len(a_interp))

    for i in range(len(z_interp)):
        if a_interp[i] == 0:
            t_interp[i] = 13.82968685
        else:
            t_interp[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100 * h)) * quad(
                lambda z: 1 / ((1 + z) * np.sqrt(omm * (1 + z) ** 3 + oml)), 0, z_interp[i])[0] #Gyr

    return np.interp(a, a_interp, t_interp)


def concentration_ludlow(z, M200):
    '''Ludlow et al. 2016. Appendix B
    M200 in 10^10 M_sun
    '''
    delta_sc = 1.686

    c_0 = 3.395 * (1 + z)**(-0.215)

    beta = 0.307 * (1 + z)**0.540

    gamma_1 = 0.628 * (1 + z)**(-0.047)

    gamma_2 = 0.317 * (1 + z)**(-0.893)

    omega_l0 = 0.693
    omega_m0 = 0.307
    little_h = 0.6777

    omega_l = omega_l0 / (omega_l0 + omega_m0 * (1 + z)**3)
    omega_m = 1 - omega_l

    psi  = omega_m**(4/7) - omega_l + (1 + omega_m / 2) * (1 + omega_l/70)
    psi0 = omega_m0**(4/7) - omega_l0 + (1 + omega_m0 / 2) * (1 + omega_l0/70)

    a = (1 + z)**(-1.0)

    Dz = omega_m / omega_m0 * psi0 / psi * (1 + z)**(-1.0)

    nu_0 = (4.135 - 0.564 * a**(-1.0) - 0.210 * a**(-2.0) +
          0.0557 * a**(-3.0) - 0.00348 * a**(-4.0)) * Dz**(-1.0)

    #TODO check if don't need little h if units are in 10^10 Msun
    # xi = (M200 / little_h)**(-1.0)
    xi = (M200)**(-1.0)

    sigma = Dz * 22.26 * xi**0.292 / (1 + 1.53 * xi**0.275 + 3.36 * xi**0.198)

    nu = delta_sc / sigma

    c = c_0 * (nu / nu_0)**(-gamma_1) * (1 + (nu / nu_0)**(1/beta) )**(-beta*(gamma_2 - gamma_1))

    return(c)


def get_cylindrical(PosStars, VelStars):
    """Calculates cylindrical coordinates.
    """
    rho = np.sqrt(np.square(PosStars[:, 0]) + np.square(PosStars[:, 1]))
    varphi = np.arctan2(PosStars[:, 1], PosStars[:, 0])
    z = PosStars[:, 2]

    v_rho = VelStars[:, 0] * np.cos(varphi) + VelStars[:, 1] * np.sin(varphi)
    v_varphi = -VelStars[:, 0] * np.sin(varphi) + VelStars[:, 1] * np.cos(varphi)
    v_z = VelStars[:, 2]

    return (rho, varphi, z, v_rho, v_varphi, v_z)


def find_rotaion_matrix(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0
    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)

    if j_tot[2] < 0:
        x += 180

    return Rotation.from_euler('yx', [y, x], degrees=True)


def align(pos, vel, mass, apature=30):
    """Aligns the z cartesian direction with the direction of angular momentum.
    Can use an apature.
    """
    # direction to align
    if apature is not None:
        r = np.linalg.norm(pos, axis=1)
        mask = r < apature

        j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

    else:
        j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

    # find rotation
    rotation = find_rotaion_matrix(j_tot)

    # rotate stars
    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    return pos, vel, rotation


def get_radii_that_are_interesting(R, mass):
    """Find star half mass, quater mass and 3 quater mass.
    """
    arg_order = np.argsort(R)
    cum_mass = np.cumsum(mass[arg_order])
    # fraction
    cum_mass /= cum_mass[-1]

    half = R[arg_order][np.where(cum_mass > 0.50)[0][0]]
    quater = R[arg_order][np.where(cum_mass > 0.25)[0][0]]
    three = R[arg_order][np.where(cum_mass > 0.75)[0][0]]

    return half, quater, three


def max_within_50(array, fifty=50):
    """returns the largest value of array within 50 indices
    probably can be faster
    """
    list_len = np.size(array)

    max_array = np.zeros(list_len)

    for i in range(list_len):
        low = np.amax((0, i - fifty))
        hi = np.amin((i + fifty, list_len))

        max_array[i] = np.amax(array[low:hi])

    return max_array


def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
    """Calculates the reduced inertia tensor
    M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
    Itterative selection is done in the other function.
    """
    norm = m_p / e2_p
    s_n = np.sum(norm)

    m = np.zeros((3, 3))

    if s_n != 0:
        for i in range(3):
            for j in range(3):
                m[i, j] = np.sum(norm * r_p[:, i] * r_p[:, j])

        m /= np.sum(norm)

    return m


def process_tensor(m):
    """
    """
    # I think I messed np.linalg.eigh(m) up when I was first writing thins
    if np.any(np.isnan(m)) or np.any(np.isinf(m)):
        # print('Found a nan or inf', m)
        return np.zeros(3, dtype=np.float32), np.identity(3, dtype=np.float32)

    (eigan_values, eigan_vectors) = np.linalg.eig(m)

    order = np.flip(np.argsort(eigan_values))

    eigan_values = eigan_values[order]
    eigan_vectors = eigan_vectors[:, order]

    return eigan_values, eigan_vectors


def defined_particles(pos, mass, eigan_values, eigan_vectors):
    """Assumes eigan values are sorted
    """
    # projection along each axis
    projected_a = (pos[:, 0] * eigan_vectors[0, 0] + pos[:, 1] * eigan_vectors[1, 0] +
                   pos[:, 2] * eigan_vectors[2, 0])
    projected_b = (pos[:, 0] * eigan_vectors[0, 1] + pos[:, 1] * eigan_vectors[1, 1] +
                   pos[:, 2] * eigan_vectors[2, 1])
    projected_c = (pos[:, 0] * eigan_vectors[0, 2] + pos[:, 1] * eigan_vectors[1, 2] +
                   pos[:, 2] * eigan_vectors[2, 2])

    # ellipse distance #Thob et al. 2019 eqn 4.
    ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1] / eigan_values[0]) +
                        np.square(projected_c) / (eigan_values[2] / eigan_values[0]))

    # #this seems to make almost no difference.
    # #I'm not convinced that the method in Thob is the correct way to do this
    # #ellipse radius #Thob et al. 2019 eqn 4.
    # ellipse_radius = np.power((eigan_values[1] * eigan_values[2]) / np.square(eigan_values[0]), 1/3
    #                           ) * 900
    # #Thob et al. 2019 eqn 4.
    # inside_mask = ellipse_distance <= ellipse_radius
    # return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])

    return pos, mass, ellipse_distance


def find_abc(pos, mass, converge_tol2=0.0001, max_iter=100):
    """Finds the major, intermediate and minor axes.
    Follows Thob et al. 2019 using quadrupole moment of mass to bias towards
    particles closer to the centre
    """
    # no clue why this isn't working
    # try except
    try:
        # start off speherical
        r2 = np.square(np.linalg.norm(pos, axis=1))

        # stop problems with r=0
        pos = pos[r2 != 0]
        mass = mass[r2 != 0]
        r2 = r2[r2 != 0]

        # mass tensor of particles
        m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, r2)

        # linear algebra stuff
        (eigan_values, eigan_vectors) = process_tensor(m)

        # to see convergeance
        cona = np.sqrt(eigan_values[2] / eigan_values[0])
        bona = np.sqrt(eigan_values[1] / eigan_values[0])

        # done = False
        for i in range(max_iter):

            # redefine particles, calculate ellipse distance
            (pos, mass, ellipse_r2) = defined_particles(pos, mass, eigan_values, eigan_vectors)

            if len(mass) == 0:
                # print('Warning: No particles left when finding shape')
                return (np.zeros(3, dtype=np.float32))

            # mass tensor of new particles
            m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, ellipse_r2)

            # linear algebra stuff
            (eigan_values, eigan_vectors) = process_tensor(m)

            if (1 - eigan_values[2] / eigan_values[0] / cona < converge_tol2) and (
                    1 - eigan_values[1] / eigan_values[0] / bona < converge_tol2):
                # converged
                # done = True
                break

            else:
                cona = np.sqrt(eigan_values[2] / eigan_values[0])
                bona = np.sqrt(eigan_values[1] / eigan_values[0])

        # some warnings
        # if not done:
        #   print('Warning: Shape did not converge.')

        # if len(mass) < 100:
        #   print('Warning: Defining shape with <100 particles.')

    except (ValueError, TypeError) as e:
        return (np.zeros(3, dtype=np.float32))

    return np.sqrt(eigan_values)


def my_binned_statistic(x, value, bins, statistic='sum'):
    """Calls binned statistic, but returns zeros when x and value are empty.
    """
    if len(x) == 0 and len(value) == 0:
        out = np.zeros(len(bins) - 1)
    else:
        out = binned_statistic(x, value, bins=bins, statistic=statistic)[0]

    return out


def my_already_binned_statistic(value, digitized, bin_edges, statistic='sum'):
    """Calculates the statistic in bins if binned_statistic has already digitized
    the data set.
    Should be faster than calling binned stastic a bunch of times ......
    """
    n = len(bin_edges) - 1
    out_array = np.zeros(n, dtype=np.float32)

    if statistic == 'sum':
        func = np.sum
    elif statistic == 'median':
        func = np.median
    else:
        raise ValueError('Statistic ' + str(statistic) + ' not understood.')

    # would be faster without a for loop
    for i in range(n):
        out_array[i] = func(value[digitized == i + 1])

    return out_array


def my_weighted_quartile(data, quartile, weights=None):
    if weights is None:
        # return np.nanquantile(data, quartile)
        weights = np.ones(len(data))

    if np.size(data) == 1:
        return np.repeat(data, len(quartile))
    elif np.size(data) == 0:
        return np.nan * np.ones(len(quartile))

    arg_order = np.argsort(data)

    ordered_data = data[arg_order]
    ordered_weights = weights[arg_order]

    normed_ordered_weights = np.cumsum(ordered_weights)
    normed_ordered_weights /= normed_ordered_weights[-1]

    return np.interp(quartile, normed_ordered_weights, ordered_data)


def get_kinematic_profiles(bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass):
    """Given galaxy particle data and bins, calculates kenematic profiles.
    See Genel et al. 2015, Lagos et al. 2017, Correra et al. 2017, Wilkinson et al. 2021
    Probably not super efficent to call binned statistic a bunch of times, can use
    the third output to mask bins. Too lazy / will be more confusing to code though.
    """
    number_of_bins = len(bin_edges) - 1

    # binned
    if number_of_bins > 1:
        (bin_mass, _, bin_number) = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
    #this needed to stop it from breaking
    else:
        return(np.zeros(0), np.zeros(0), np.zeros((0, n_jzjc_bins)),
               np.zeros(0), np.zeros(0), np.zeros(0), np.zeros(0), np.zeros(0),
               np.zeros(0), np.zeros(0), np.zeros(0), np.zeros(0),
               np.zeros(0), np.zeros(0), np.zeros(0), np.zeros(0), np.zeros(0),
               np.zeros((0, n_formation_time_intervals)), np.zeros((0, n_formation_age_percentiles)),
               np.zeros(0))

    bin_N = np.histogram(R, bins=bin_edges)[0]

    # j_zonc
    # I don't like this. Needs to be done though
    fjzjc = np.zeros((number_of_bins, n_jzjc_bins), dtype=np.float32)

    # calculate kappas
    K_tot = np.zeros(number_of_bins, dtype=np.float32)
    K_rot = np.zeros(number_of_bins, dtype=np.float32)
    K_co = np.zeros(number_of_bins, dtype=np.float32)

    # sigmas
    mean_v_R = np.zeros(number_of_bins, dtype=np.float32)
    mean_v_phi = np.zeros(number_of_bins, dtype=np.float32)
    median_v_phi = np.zeros(number_of_bins, dtype=np.float32)

    sigma_z = np.zeros(number_of_bins, dtype=np.float32)
    sigma_R = np.zeros(number_of_bins, dtype=np.float32)
    sigma_phi = np.zeros(number_of_bins, dtype=np.float32)

    z_half = np.zeros(number_of_bins, dtype=np.float32)

    a_axis = np.zeros(number_of_bins, dtype=np.float32)
    b_axis = np.zeros(number_of_bins, dtype=np.float32)
    c_axis = np.zeros(number_of_bins, dtype=np.float32)

    J_z   = np.zeros(number_of_bins, dtype=np.float32)
    J_tot = np.zeros(number_of_bins, dtype=np.float32)

    star_formation_time_intervals = np.zeros((number_of_bins, n_formation_time_intervals), dtype=np.float32)
    star_formation_age_percentiles = np.zeros((number_of_bins, n_formation_age_percentiles), dtype=np.float32)

    r_12 = np.zeros(number_of_bins, dtype=np.float32)

    # would be faster without a for loop
    for i in range(number_of_bins):
        if bin_mass[i] > 0:

            mask = (bin_number == i + 1)

            bin_massi = 1 / bin_mass[i]

            mmass = mass[mask]
            mj_zonc = j_zonc[mask]
            mv_phi = v_phi[mask]

            minitial_mass = initial_mass[mask]
            mform_t = form_t[mask]

            # j_zonc
            for j, X in enumerate(np.linspace(-1, 1, n_jzjc_bins)):
                fjzjc[i, j] = np.sum(mmass[mj_zonc >= X]) * bin_massi

            # calculate kappas
            K_tot[i] = np.sum(mmass * np.square(np.linalg.norm(vel[mask], axis=1)))
            K_rot[i] = np.sum(mmass * np.square(mv_phi))

            co = (mv_phi > 0)
            K_co[i] = np.sum(mmass[co] * np.square(mv_phi[co]))

            # sigmas
            mean_v_R[i] = np.sum(mmass * v_R[mask]) * bin_massi
            mean_v_phi[i] = np.sum(mmass * mv_phi) * bin_massi
            median_v_phi[i] = np.median(mv_phi)

            sigma_z[i] = np.sum(mmass * np.square(v_z[mask])) * bin_massi
            sigma_R[i] = np.sum(mmass * np.square(v_R[mask])) * bin_massi
            sigma_phi[i] = np.sum(mmass * np.square(mv_phi - mean_v_phi[i])) * bin_massi

            # TODO replace with mass weighted median
            z_half[i] = np.median(np.abs(z[mask]))

            # TODO replace with mass weighted median
            # TODO add initial mass weighted median

            if bin_N[i] > 10:
                try:
                    a_axis[i], b_axis[i], c_axis[i] = find_abc(pos[mask], mmass)
                except Exception as e:
                    # a_axis[i], b_axis[i], c_axis[i] = 0., 0., 0.
                    pass
                    # print(e)
            # else:
                # a_axis[i], b_axis[i], c_axis[i] = 0., 0., 0.

            vec_J = np.sum(np.cross(pos[mask], vel[mask]) * mmass[:, np.newaxis], axis=0)
            J_z[i] = vec_J[2]
            J_tot[i] = np.linalg.norm(vec_J)

            r_12[i] = my_weighted_quartile(R[mask], np.array([0.5]), weights=mmass)

            #sf
            for j in range(n_formation_time_intervals):
                time_interval = 2**(-(n_formation_time_intervals-1)//2) *  2 ** j #[0.25, 0.5, 1, 2]
                min_t = CURRENT_LBT + time_interval

                star_formation_time_intervals[i, j] = np.sum(minitial_mass[mform_t <= min_t]) / time_interval #10^10 Mstar / Gyr

            star_formation_age_percentiles[i] = my_weighted_quartile(mform_t, formation_age_percentiles, weights=mmass)

    kappa_rot = K_rot / K_tot
    kappa_co = K_co / K_tot

    sigma_z = np.sqrt(sigma_z)
    sigma_R = np.sqrt(sigma_R)
    sigma_phi = np.sqrt(sigma_phi)

    return (bin_mass, bin_N,
            fjzjc,
            kappa_rot, kappa_co,
            mean_v_R, mean_v_phi, median_v_phi,
            sigma_z, sigma_R, sigma_phi,
            z_half, a_axis, b_axis, c_axis,
            J_z, J_tot,
            star_formation_time_intervals, star_formation_age_percentiles,
            r_12)


def get_kinematic_apature(apature, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass):
    """Same as get_kinematic_profile, except apature rather than profile.
    """
    # mask
    mask = (R < apature)

    if np.sum(mask) > 0:

        pos = pos[mask]
        vel = vel[mask]
        mass = mass[mask]
        (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])
        j_zonc = j_zonc[mask]

        initial_mass = initial_mass[mask]
        form_t = form_t[mask]

        bin_mass = np.sum(mass)
        bin_N = np.size(mass)

        bin_massi = 1 / bin_mass

        # jz/jc
        fjzjc = np.zeros(n_jzjc_bins, dtype=np.float32)
        for j, X in enumerate(np.linspace(-1, 1, n_jzjc_bins)):
            fjzjc[j] = np.sum(mass[j_zonc >= X]) * bin_massi

        # calculate kappas
        K_tot = np.sum(mass * np.square(np.linalg.norm(vel, axis=1)))
        K_rot = np.sum(mass * np.square(v_phi))
        K_co = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

        kappa_rot = K_rot / K_tot
        kappa_co = K_co / K_tot

        # sigmas
        mean_v_R = np.sum(mass * v_R) * bin_massi
        mean_v_phi = np.sum(mass * v_phi) * bin_massi
        median_v_phi = np.median(v_phi)

        sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
        sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
        sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

        z_half = np.median(np.abs(z))

        a_axis, b_axis, c_axis = find_abc(pos, mass)

        vec_J = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)
        J_z = vec_J[2]
        J_tot = np.linalg.norm(vec_J)

        # sf
        star_formation_time_intervals = np.zeros(n_formation_time_intervals, dtype=np.float32)
        for j in range(n_formation_time_intervals):
            time_interval = 2 ** (-(n_formation_time_intervals - 1) // 2) * 2 ** j  # [0.25, 0.5, 1, 2]
            min_t = CURRENT_LBT + time_interval

            star_formation_time_intervals[j] = np.sum(initial_mass[form_t <= min_t]) / time_interval  # 10^10 Mstar / Gyr

        star_formation_age_percentiles = my_weighted_quartile(form_t, formation_age_percentiles, weights=mass)

        r_12 = my_weighted_quartile(R, np.array([0.5]), weights=mass)[0]

        return (bin_mass, bin_N,
                fjzjc,
                kappa_rot, kappa_co,
                mean_v_R, mean_v_phi, median_v_phi,
                sigma_z, sigma_R, sigma_phi,
                z_half, a_axis, b_axis, c_axis,
                J_z, J_tot,
                star_formation_time_intervals, star_formation_age_percentiles,
                r_12)

    else:
        return(0, 0,
               np.zeros(n_jzjc_bins),
               0, 0,
               0, 0, 0,
               0, 0, 0,
               0, 0, 0, 0,
               0, 0,
               np.zeros(n_formation_time_intervals), np.zeros(n_formation_age_percentiles),
               0)


def get_kinematic_annuli(apature, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass):
    """Same as get_kinematic_profile, except apature rather than profile.
    """
    # mask
    dex = 0.2
    mask = np.logical_and(10 ** (-dex / 2) < R / apature, R / apature < 10 ** (dex / 2))

    if np.sum(mask) > 0:

        pos = pos[mask]
        vel = vel[mask]
        mass = mass[mask]
        (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])
        j_zonc = j_zonc[mask]

        initial_mass = initial_mass[mask]
        form_t = form_t[mask]

        bin_mass = np.sum(mass)
        bin_N = np.size(mass)

        bin_massi = 1 / bin_mass

        # jz/jc
        fjzjc = np.zeros(n_jzjc_bins, dtype=np.float32)
        for j, X in enumerate(np.linspace(-1, 1, n_jzjc_bins)):
            fjzjc[j] = np.sum(mass[j_zonc >= X]) * bin_massi

        # calculate kappas
        K_tot = np.sum(mass * np.square(np.linalg.norm(vel, axis=1)))
        K_rot = np.sum(mass * np.square(v_phi))
        K_co = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

        kappa_rot = K_rot / K_tot
        kappa_co = K_co / K_tot

        # sigmas
        mean_v_R = np.sum(mass * v_R) * bin_massi
        mean_v_phi = np.sum(mass * v_phi) * bin_massi
        median_v_phi = np.median(v_phi)

        sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
        sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
        sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

        z_half = np.median(np.abs(z))

        a_axis, b_axis, c_axis = find_abc(pos, mass)

        vec_J = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)
        J_z = vec_J[2]
        J_tot = np.linalg.norm(vec_J)

        # sf
        star_formation_time_intervals = np.zeros(n_formation_time_intervals, dtype=np.float32)
        for j in range(n_formation_time_intervals):
            time_interval = 2 ** (-(n_formation_time_intervals - 1) // 2) * 2 ** j  # [0.25, 0.5, 1, 2]
            min_t = CURRENT_LBT + time_interval

            star_formation_time_intervals[j] = np.sum(initial_mass[form_t <= min_t]) / time_interval  # 10^10 Mstar / Gyr

        star_formation_age_percentiles = my_weighted_quartile(form_t, formation_age_percentiles, weights=mass)

        r_12 = my_weighted_quartile(R, np.array([0.5]), weights=mass)[0]

        return (bin_mass, bin_N,
                fjzjc,
                kappa_rot, kappa_co,
                mean_v_R, mean_v_phi, median_v_phi,
                sigma_z, sigma_R, sigma_phi,
                z_half, a_axis, b_axis, c_axis,
                J_z, J_tot,
                star_formation_time_intervals, star_formation_age_percentiles,
                r_12)

    else:
        return(0, 0,
               np.zeros(n_jzjc_bins),
               0, 0,
               0, 0, 0,
               0, 0, 0,
               0, 0, 0, 0,
               0, 0,
               np.zeros(n_formation_time_intervals), np.zeros(n_formation_age_percentiles),
               0)


def read_calculate_write(args):
    # time
    start_time = time.time()

    raw_data = my_read(*args)
    print(str(np.round(time.time() - start_time, 1)))

    processed_data = my_calculate(*raw_data)
    print(str(np.round(time.time() - start_time, 1)))

    my_write(*processed_data)
    print('Finished snap in ' + str(np.round(time.time() - start_time, 1)) + 's')

    return


if __name__ == '__main__':
    _, snap_index = sys.argv

    snap_index = int(snap_index)

    #hyades
    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    #ozstar
    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location = particle_data_location_list[snap_index // 4]
    halo_data_location = halo_data_location_list[snap_index // 4]
    output_data_location =  output_data_location_list[snap_index // 4]

    kdtree_location = output_data_location

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]

    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    current_lbt_list = [0, 5.22014126, 7.96152142, 10.55063966]

    SCALE_A = SCALE_A_list[snap_index % 4]
    CURRENT_LBT = current_lbt_list[snap_index % 4]

    #TODO change medians to mass weighted median!

    # do the thing
    read_calculate_write((particle_data_location, halo_data_location, output_data_location, kdtree_location, snap))

    pass
