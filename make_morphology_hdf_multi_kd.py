#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

# import matplotlib
# matplotlib.use('Agg')

import h5py as h5

from scipy.integrate import quad
from scipy.optimize import brentq
from scipy.optimize import minimize
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

from scipy.spatial import cKDTree

import multiprocessing as mp

import traceback as tb
import warnings

# ignore divide by zero
np.seterr(divide='ignore', invalid='ignore')
# warnings.filterwarnings('ignore')

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
# BOX_SIZE = 33.885 #50 * 0.6777 #Mpc / h
# BOX_HALF = 16.9425 #50 / 2 * 0.6777 #Mpc / h
# DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun
cosmo_to_cgs_desnity_units = 6.76991e-31 #g cm^-3 / 10^10 M_sun Mpc^-3

MSUN_ON_KG = 1.988e30 #M_sun/kg
# BOLTZMANN_K = 1.380649e-23 #J / K = kg m^2 / s^2 / k Wiki
BOLTZMANN_K = 1.380649e-23 * MSUN_ON_KG / 10**10 * 1e-6 #10^10 M_sun km^2 / s^2 / k Wiki

STAR_FRAME_APERTURE_PHYSICAL = 30 #kpc #TODO should be 50?
GAS_FRAME_APERTURE_PHYSICAL = 70 #kpc #TODO should be 50?

CCSNAGE = 0.03 #Gyr Dalla Vecchia and Scaye 2012 3*10^7 yr = 30 Myr = 0.03 Gyrs

DM_SUBSAMPLE_N = 3_000_000 #if halo has more than this many particles, subsample. for speed reasons

n_jzjc_bins = 21 #np.linspace(-1,1,n_jzjc_bins)
n_formation_time_intervals = 4 #[0.25, 0.5, 1, 2] Myr
formation_age_percentiles = np.array([0.9, 0.75, 0.5, 0.25, 0.1])
n_formation_age_percentiles = len(formation_age_percentiles)
rho_gas_percentiles = np.array([0.80, 0.50, 0.20])
n_rho_gas_percentiles = len(rho_gas_percentiles)
n_age_bins = 7

lin_bin_edges = np.linspace(0, STAR_FRAME_APERTURE_PHYSICAL, 1)
# log_bin_edges = np.logspace(-1, 3, 8*4+1)
log_bin_edges = np.logspace(-1, 3, 5*4+1)
log_bin_edges[0] = 0

# change if bins change
n_lin = len(lin_bin_edges) - 1
n_log = len(log_bin_edges) - 1
n_scale = 8
n_star_extra = n_age_bins*2 # + n_age_bins*n_log + 1
n_dim = 2 * n_lin + n_log + n_lin + n_scale

t_t0 = 0
t_t2 = 2
t_t4 = 4
t_t6 = 6
t_t8 = 8
t_t10 = 10
t_t12 = 12
t_t14 = 14
t_bin_edges = np.array([t_t0, t_t2, t_t4, t_t6, t_t8, t_t10, t_t12, t_t14])


#awful hack for arrays with wrong dimensions
def my_ndstack(arr):
    if len(np.shape(arr[0])) == 1:
        return np.hstack(arr)
    return np.vstack(arr)


def NO_BINNED_PARTICLES_RETURN(n_bins=0):
    return [np.zeros(n_bins), np.zeros(n_bins), np.zeros((n_bins, n_jzjc_bins)),
            np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins),
            np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins),
            np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins),
            np.zeros((n_bins, n_formation_time_intervals)), np.zeros((n_bins, n_formation_age_percentiles)),
            np.zeros((n_bins, n_rho_gas_percentiles)),
            np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins), np.zeros(n_bins)]

#TODO all these zeros should be nans
NO_PARTICLE_RETURN = [0, 0,
                      np.zeros(n_jzjc_bins),
                      0, 0,
                      0, 0, 0,
                      0, 0, 0,
                      0,
                      0, 0, 0,
                      0, 0,
                      np.zeros(n_formation_time_intervals), np.zeros(n_formation_age_percentiles),
                      np.zeros(n_rho_gas_percentiles),
                      0, 0,
                      0, 0, 0]
NO_HALO_RETURN = [[0,0,0,0,np.zeros(3),0]]
# NO_STAR_RETURN = [[np.zeros(3), 0,0,0, 0,0,0],
#                   [NO_BINNED_PARTICLES_RETURN(n_log),
#                    *([NO_PARTICLE_RETURN] * (n_scale + n_star_extra))]]

NO_STAR_RETURN = [[np.zeros(3), 0,0,0, 0,0,0],
                  [NO_BINNED_PARTICLES_RETURN(n_log),
                   *([NO_PARTICLE_RETURN] * n_scale),
                   NO_PARTICLE_RETURN,
                   NO_BINNED_PARTICLES_RETURN(n_log),
                   *([NO_PARTICLE_RETURN] * 2 * n_age_bins),
                   *[NO_BINNED_PARTICLES_RETURN(n_log) for _ in range(n_age_bins)]]]

NO_GAS_RETURN = [[np.zeros(3), 0,0,0, 0,0,0],
                 [NO_BINNED_PARTICLES_RETURN(n_log),
                  *([NO_PARTICLE_RETURN] * n_scale),
                  NO_PARTICLE_RETURN,
                  NO_BINNED_PARTICLES_RETURN(n_log),]]
# NO_DM_RETURN = [[np.zeros(3)],
#                 [[*np.zeros((7, n_log))],
#                  [*np.zeros((7, n_scale))]]]
NO_DM_RETURN = [[np.zeros(3), 0,0,0, 0,0,0],
                [NO_BINNED_PARTICLES_RETURN(n_log),
                 *([NO_PARTICLE_RETURN] * n_scale)]]
NO_BH_RETURN = [0,0,0, 0,0]
NO_DATA_RETURN = [[0], *[i for i in NO_HALO_RETURN[0]], *[i for i in NO_STAR_RETURN[0]],
                  *[i for i in NO_GAS_RETURN[0]], *[i for i in NO_GAS_RETURN[0]],
                  *[i for i in NO_DM_RETURN[0]], *[i for i in NO_BH_RETURN],
                  *[my_ndstack(([d[i] for d in NO_STAR_RETURN[1]])) for i in range(len(NO_STAR_RETURN[1][0]))],
                  *[my_ndstack(([d[i] for d in NO_GAS_RETURN[1]])) for i in range(len(NO_GAS_RETURN[1][0]))],
                  *[my_ndstack(([d[i] for d in NO_GAS_RETURN[1]])) for i in range(len(NO_GAS_RETURN[1][0]))],
                  *[my_ndstack(([d[i] for d in NO_DM_RETURN[1]])) for i in range(len(NO_DM_RETURN[1][0]))]]

#TODO read/make data in multi processing way
def my_read(particle_data_location, halo_data_location, output_data_location, kdtree_location, snap):
    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    FirstSub = np.zeros(TotNgroups, dtype=np.int64)

    # Subhalo arrays
    GroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)
    SubGroupCentreOfMass = np.zeros((TotNsubgroups, 3), dtype=np.float32)
    #0: gas, 1: DM, 4: Stars, 5: BH
    SubMassType = np.zeros((TotNsubgroups, 6), dtype=np.float32)

    NGrp_c = 0
    NSub_c = 0

    print('TotNGroups:', TotNgroups)
    print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = particle_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            if Nsubgroups > 0:
                GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
                SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]

                SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]
                SubGroupCentreOfMass[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfMass"][()]

                SubMassType[NSub_c:NSub_c + Nsubgroups, :] = fs["Subhalo/MassType"][()]

                # SubhaloMass[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/Mass"][()]

                NSub_c += Nsubgroups

    print('Loaded halos')
    # print(np.sum(Group_M_Crit200 > 1)) #number M_200 > 10^10 / h
    print(np.sum(Group_M_Crit200 > 1 * LITTLE_H)) #number M_200 > 10^10

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    # NumPartTot = [0, NumPartTot[1], 0, 0, 0]

    # if NumPartTot[4] > 0:
    PosStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    VelStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    MassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    InitialMassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    # BirthDensity   = np.zeros(NumPartTot[4],    dtype=np.float32)
    # Metallicity = np.zeros(NumPartTot[4], dtype=np.float32)
    Star_aform = np.zeros(NumPartTot[4], dtype=np.float32)
    # Star_tform     = np.zeros(NumPartTot[4],    dtype=np.float32)
    BindingEnergyStar = np.zeros(NumPartTot[4], dtype=np.float32)
    # HSML_Star      = np.zeros(NumPartTot[4],    dtype=np.float32)
    # ParticleIDs = np.zeros(NumPartTot[4], dtype=np.int32)
    GrpNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)
    SubNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)

    PosGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    VelGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    MassGas = np.zeros(NumPartTot[0], dtype=np.float32)
    onEOSGas = np.zeros(NumPartTot[0], dtype=bool)
    SFRGas = np.zeros(NumPartTot[0], dtype=np.float32)
    DensityGas = np.zeros(NumPartTot[0], dtype=np.float32)
    TemperatureGas = np.zeros(NumPartTot[0], dtype=np.float32)
    BindingEnergyGas = np.zeros(NumPartTot[0], dtype=np.float32)
    GrpNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)
    SubNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)

    PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    VelDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    BindingEnergyDM = np.zeros(NumPartTot[1], dtype=np.float32)
    GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    SubNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)

    PosBH = np.zeros((NumPartTot[5], 3), dtype=np.float32)
    # VelBH = np.zeros((NumPartTot[5], 3), dtype=np.float32)
    GravMassBH = np.zeros(NumPartTot[5], dtype=np.float32)
    MassBH = np.zeros(NumPartTot[5], dtype=np.float32)
    GrpNum_BH = np.zeros(NumPartTot[5], dtype=np.int32)
    SubNum_BH = np.zeros(NumPartTot[5], dtype=np.int32)

    NStar_c = 0
    NGas_c = 0
    NDM_c = 0
    NBH_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[4] > 0:
                PosStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Coordinates"][()]
                VelStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Velocity"][()]
                MassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Mass"][()]
                InitialMassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/InitialMass"][()]
                # BirthDensity[NStar_c:NStar_c+NumParts[4]] = fs["PartType4/BirthDensity"][()]
                # Metallicity[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SmoothedMetallicity"][()]
                Star_aform[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/StellarFormationTime"][()]
                # Star_tform[NStar_c:NStar_c+NumParts[4]]   = lbt(Star_aform[NStar_c:NStar_c+NumParts[4]])
                BindingEnergyStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleBindingEnergy"][()]
                # HSML_Star[NStar_c:NStar_c+NumParts[4]]    = fs["PartType4/SmoothingLength"][()]
                # ParticleIDs[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleIDs"][()]
                GrpNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/GroupNumber"][()]
                SubNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SubGroupNumber"][()]

                NStar_c += NumParts[4]

            if NumParts[0] > 0:
                PosGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Coordinates"][()]
                VelGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Velocity"][()]
                MassGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/Mass"][()]
                onEOSGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/OnEquationOfState"][()]
                SFRGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/StarFormationRate"][()]
                DensityGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/Density"][()]
                TemperatureGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/Temperature"][()]
                BindingEnergyGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/ParticleBindingEnergy"][()]
                GrpNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/GroupNumber"][()]
                SubNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/SubGroupNumber"][()]

                NGas_c += NumParts[0]

            if NumParts[1] > 0:
                PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                VelDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Velocity"][()]
                BindingEnergyDM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/ParticleBindingEnergy"][()]
                GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"][()]
                SubNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/SubGroupNumber"][()]

                NDM_c += NumParts[1]

            if NumParts[5] > 0:
                PosBH[NBH_c:NBH_c + NumParts[5], :] = fs["PartType5/Coordinates"][()]
                # VelBH[NBH_c:NBH_c + NumParts[5], :] = fs["PartType5/Velocity"][()]
                GravMassBH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/Mass"][()]
                MassBH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/BH_Mass"][()]
                GrpNum_BH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/GroupNumber"][()]
                SubNum_BH[NBH_c:NBH_c + NumParts[5]] = fs["PartType5/SubGroupNumber"][()]

                NBH_c += NumParts[5]

    print('loaded particles')

    start_time = time.time()
    star_kd_tree = cKDTree(PosStar, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated star kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
    start_time = time.time()
    gas_kd_tree  = cKDTree(PosGas, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated gas kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
    start_time = time.time()
    bh_kd_tree = cKDTree(PosBH, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated bh kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')

    # #tree is too large to fit into ram
    # if not(kdtree_location == '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/' and
    #        snap == '028_z000p000'):
    dm_tree_file_name = kdtree_location + snap + '_dm_tree.pickle'
    start_time = time.time()
    print('DM tree file name ' + dm_tree_file_name)
    if os.path.exists(dm_tree_file_name):
        print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            dm_kd_tree = pickle.load(dm_tree_file)
        print('Opened dm kdtree')

    else:
        print('Calculating')
        dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
        print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(dm_kd_tree, dm_tree_file, protocol=4)
        print('Saved dm kdtree.')
    print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    # else:
    #     print('skipped DM tree')

    print('Done all kdtrees.')

    start_time = time.time()
    print('Calculating stellar ages')
    # Star_tform = lbt(Star_aform)
    Star_tform = lbt_interp(Star_aform)
    print('Calculated ages in ' + str(np.round(time.time() - start_time, 1)) + 's')

    print('loaded everything')

    return (output_data_location, kdtree_location, snap,
            star_kd_tree, gas_kd_tree, dm_kd_tree, bh_kd_tree,
            TotNgroups, TotNsubgroups,
            SubMassType,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
            GroupNumber, SubGroupNumber, SubGroupCentreOfPotential, SubGroupCentreOfMass,
            PosStar, VelStar, MassStar, InitialMassStar, Star_tform,
            BindingEnergyStar,
            GrpNum_Star, SubNum_Star,
            PosGas, VelGas, MassGas,
            onEOSGas, SFRGas, DensityGas, TemperatureGas,
            BindingEnergyGas,
            GrpNum_Gas, SubNum_Gas,
            PosDM, VelDM,
            BindingEnergyDM,
            GrpNum_DM, SubNum_DM,
            PosBH, MassBH, GravMassBH,
            GrpNum_BH, SubNum_BH)


def calculate_stars(sub_index, centre_of_potential, cr200, subgroup_number, rs):
    # use global kdtree
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    star_index_mask = star_kd_tree.query_ball_point(x=centre_of_potential,
                                                    r=cr200)  # in Mpc*h/a
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    star_index_mask = np.array(star_index_mask)

    if len(star_index_mask) > 0:
        #use global star subgroup number
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
        star_index_mask = star_index_mask[np.abs(SubNum_Star[star_index_mask]) == subgroup_number]
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    # masked evetything
    if len(star_index_mask) == 0:
        return False, None, None, None, None, NO_STAR_RETURN
        # return False, Rotation.from_euler('xy', [0,0]), np.zeros(3), 0, 0, NO_STAR_RETURN

    r200 = cr200 * SCALE_A / LITTLE_H * 1000  # kcp

    # read global arrays and mask params relevant for calculation
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    pos = PosStar[star_index_mask]
    vel = VelStar[star_index_mask]
    mass = MassStar[star_index_mask]
    initial_mass = InitialMassStar[star_index_mask]
    pot = BindingEnergyStar[star_index_mask]
    form_t = Star_tform[star_index_mask]
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    #fix galaxy
    #centre galaxy + physical units
    pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
    pos %= SCALE_A * BOX_SIZE
    pos -= SCALE_A * BOX_SIZE / 2
    pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

    r = np.linalg.norm(pos, axis=1)
    mask = (r < STAR_FRAME_APERTURE_PHYSICAL)

    vel *= np.sqrt(SCALE_A)  # km/s
    mass /= LITTLE_H  # 10^10 M_sun
    initial_mass /= LITTLE_H  # 10^10 M_sun

    vel_offset = np.sum(mass[mask, np.newaxis] * vel[mask, :], axis=0) / np.sum(mass[mask])
    vel -= vel_offset

    # TODO cgs conversion factior is 1.989e+53 ???
    # this is actually binding energy (potential + kinetic energy)
    pot /= LITTLE_H  # (km/s)^2

    # halo star J
    star_J = np.sum(mass[:, np.newaxis] * np.cross(pos, vel), axis=0)

    #box orientation
    (box_R, box_phi, box_z, box_v_R, box_v_phi, box_v_z
     ) = get_cylindrical(pos, vel)

    # galaxy sizes
    (box_R_half, box_R_quarter, box_R3quarter) = get_radii_that_are_interesting(box_R, mass)

    box_pos = pos.copy()
    box_vel = vel.copy()
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    # the orientation of the galaxy
    (pos, vel, rotation) = align(pos, vel, mass, aperture=STAR_FRAME_APERTURE_PHYSICAL)
    # galaxy orientated

    # calculate everything else
    # cylindrical coords
    (R, phi, z, v_R, v_phi, v_z
     ) = get_cylindrical(pos, vel)
    # r = np.linalg.norm(pos, axis=1)

    # j_z = np.cross(R, v_phi)[:, 2]
    j_z = pos[:, 0] * vel[:, 1] - pos[:, 1] * vel[:, 0]
    j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

    kin = 0.5 * (vel[:, 0] ** 2 + vel[:, 1] ** 2 + vel[:, 2] ** 2)

    arg_energy = np.argsort(pot)
    arg_arg = np.argsort(arg_energy)

    # TODO this doesn't work for < 100ish star galaxies
    j_c_E = max_within_50(j_tot[arg_energy])[arg_arg]
    j_zonc = j_z / j_c_E

    age_bin = np.digitize(form_t, t_bin_edges)
    yet_to_CC_mask = (form_t - CURRENT_LBT) < CCSNAGE

    onEOS = np.zeros(len(mass), dtype=bool)
    density = np.zeros(len(mass))
    temperature = np.zeros(len(mass))

    # galaxy sizes
    (r_half, r_quarter, r3quarter) = get_radii_that_are_interesting(r, mass)
    (R_half, R_quarter, R3quarter) = get_radii_that_are_interesting(R[r < STAR_FRAME_APERTURE_PHYSICAL],
                                                                    mass[r < STAR_FRAME_APERTURE_PHYSICAL])

    # calculate profiles
    # lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
    #                                       initial_mass)
    log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                          initial_mass, pot, onEOS, density, temperature, stars=True)
    # proj_lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
    #                                            form_t, initial_mass)
    # proj_log_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
    #                                            form_t, initial_mass)

    # calculate values
    aperture_30 = get_kinematic_aperture(STAR_FRAME_APERTURE_PHYSICAL, pos, vel, mass, r, z, v_R, v_phi,
                                         v_z, j_zonc, form_t, initial_mass, pot, onEOS, density, temperature, stars=True)
    aperture_r200 = get_kinematic_aperture(r200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                           initial_mass, pot, onEOS, density, temperature, stars=True)
    aperture_3d_r12 = get_kinematic_aperture(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                             initial_mass, pot, onEOS, density, temperature, stars=True)
    aperture_3d_5r12 = get_kinematic_aperture(5 * r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                              initial_mass, pot, onEOS, density, temperature, stars=True)
    # aperture_3d_5r12 = get_kinematic_aperture(r200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
    #                                           initial_mass, pot, onEOS, density, temperature, stars=True, reorient=True)

    annuli_3d_r12 = get_kinematic_annuli(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                         initial_mass, pot, onEOS, density, temperature, stars=True)
    annuli_face_R12 = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t,
                                           initial_mass, pot, onEOS, density, temperature, stars=True)

    annuli_3d_rs = get_kinematic_annuli(0.2 * rs, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                        initial_mass, pot, onEOS, density, temperature, stars=True)

    apature_box_R12_box = get_kinematic_aperture(box_R_half, box_pos, box_vel, mass, box_R, box_z, box_v_R, box_v_phi,
                                                 box_v_z, j_zonc, form_t, initial_mass, pot, onEOS, density, temperature, stars=True)

    # age bins
    age_kinematics_half = []
    age_kinematics_r200 = []
    age_log_profiles = []
    for i in range(len(t_bin_edges) - 1):
        if np.sum(age_bin == i + 1) > 0:
            a_mask = age_bin == i + 1
            a_pos = pos[a_mask]
            a_vel = vel[a_mask]
            a_mass = mass[a_mask]
            a_r = r[a_mask]
            a_z = z[a_mask]
            a_v_R = v_R[a_mask]
            a_v_phi = v_phi[a_mask]
            a_v_z = v_z[a_mask]
            a_j_zonc = j_zonc[a_mask]
            a_form_t = form_t[a_mask]
            a_initial_mass = initial_mass[a_mask]
            a_pot = pot[a_mask]
            a_onEOS = onEOS[a_mask]
            a_density = density[a_mask]
            a_temperature = temperature[a_mask]

            a_annuli_half = get_kinematic_annuli(0.2 * rs, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                 a_j_zonc, a_form_t, a_initial_mass, a_pot, a_onEOS, a_density, a_temperature,
                                                 stars=True)
            a_aperture_r200 = get_kinematic_aperture(r200, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                     a_j_zonc, a_form_t, a_initial_mass, a_pot, a_onEOS, a_density, a_temperature,
                                                     stars=True)

            a_log_profiles = get_kinematic_profiles(log_bin_edges, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                    a_j_zonc, a_form_t, a_initial_mass, a_pot, a_onEOS, a_density, a_temperature,
                                                    stars=True)

            age_kinematics_half.append(a_annuli_half)
            age_kinematics_r200.append(a_aperture_r200)
            age_log_profiles.append(a_log_profiles)

        else:
            age_kinematics_half.append(NO_PARTICLE_RETURN)
            age_kinematics_r200.append(NO_PARTICLE_RETURN)
            age_log_profiles.append(NO_BINNED_PARTICLES_RETURN(n_log))


    if np.sum(yet_to_CC_mask) > 0:
        a_pos = pos[yet_to_CC_mask]
        a_vel = vel[yet_to_CC_mask]
        a_mass = mass[yet_to_CC_mask]
        a_r = r[yet_to_CC_mask]
        a_z = z[yet_to_CC_mask]
        a_v_R = v_R[yet_to_CC_mask]
        a_v_phi = v_phi[yet_to_CC_mask]
        a_v_z = v_z[yet_to_CC_mask]
        a_j_zonc = j_zonc[yet_to_CC_mask]
        a_form_t = form_t[yet_to_CC_mask]
        a_initial_mass = initial_mass[yet_to_CC_mask]
        a_pot = pot[yet_to_CC_mask]
        a_onEOS = onEOS[yet_to_CC_mask]
        a_density = density[yet_to_CC_mask]
        a_temperature = temperature[yet_to_CC_mask]

        yet_to_CC_r200 = get_kinematic_aperture(r200, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                a_j_zonc, a_form_t, a_initial_mass, a_pot, a_onEOS, a_density, a_temperature,
                                                stars=True)

        yet_to_CC_profile = get_kinematic_profiles(log_bin_edges, a_pos, a_vel, a_mass, a_r, a_z, a_v_R, a_v_phi, a_v_z,
                                                   a_j_zonc, a_form_t, a_initial_mass, a_pot, a_onEOS, a_density, a_temperature,
                                                   stars=True)

    else:
        yet_to_CC_r200 = NO_PARTICLE_RETURN
        yet_to_CC_profile = NO_BINNED_PARTICLES_RETURN(n_log)

    return (True, rotation, vel_offset, r_half, R_half,
            [[star_J,
              r_half, r_quarter, r3quarter,
              R_half, R_quarter, R3quarter],
             # [lin_profiles, log_profiles, proj_lin_profiles, proj_log_profiles,
             [log_profiles,
              aperture_30,  aperture_r200, aperture_3d_r12, aperture_3d_5r12, annuli_3d_r12,
              annuli_face_R12, annuli_3d_rs, apature_box_R12_box,
              yet_to_CC_r200, yet_to_CC_profile,
              *age_kinematics_half, *age_kinematics_r200,
              *age_log_profiles]])


def calculate_gas(sub_index, centre_of_potential, cr200, subgroup_number, rotation, vel_offset, rs, r_half, R_half,
                  SFing_only=False):
    # use global kdtree
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    gas_index_mask = gas_kd_tree.query_ball_point(x=centre_of_potential,
                                                  r=cr200)
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    gas_index_mask = np.array(gas_index_mask)

    if len(gas_index_mask) > 0:
        #use global gas subgroup number
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
        gas_index_mask = gas_index_mask[(np.abs(SubNum_Gas[gas_index_mask]) == subgroup_number)]
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    if len(gas_index_mask) == 0:
        return NO_GAS_RETURN

    r200 = cr200 * SCALE_A / LITTLE_H * 1000  # kcp

    # read global arrays and mask params relevant for calculation
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    pos = PosGas[gas_index_mask]
    vel = VelGas[gas_index_mask]
    mass = MassGas[gas_index_mask]
    pot = BindingEnergyGas[gas_index_mask]
    #onEOS = onEOSGas[gas_index_mask]
    SFR = SFRGas[gas_index_mask]
    density = DensityGas[gas_index_mask]
    temperature = TemperatureGas[gas_index_mask]
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    initial_mass = np.zeros(len(mass))
    form_t = np.zeros(len(mass))
    # j_zonc = np.zeros(len(mass))

    # SFRonEOS = SFR > 0
    # if np.any(onEOS != SFRonEOS):
    #     print(f'Warning: subgroup {sub_index} has {np.sum(onEOS != SFRonEOS)} SFR>0, EOS mismatches')
    # onEOS = SFRonEOS
    onEOS = SFR > 0

    # real units and centre and everything
    pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
    pos %= SCALE_A * BOX_SIZE
    pos -= SCALE_A * BOX_SIZE / 2
    pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

    vel *= np.sqrt(SCALE_A)  # km/s
    mass /= LITTLE_H  # 10^10 M_sun

    density *= (LITTLE_H ** 2) * (SCALE_A ** -3) * cosmo_to_cgs_desnity_units

    vel -= vel_offset

    if SFing_only:
        if np.sum(onEOS) == 0:
            return NO_GAS_RETURN
        pos = pos[onEOS]
        vel = vel[onEOS]
        mass = mass[onEOS]
        pot = pot[onEOS]
        SFR = SFR[onEOS]
        density = density[onEOS]
        temperature = temperature[onEOS]
        initial_mass = initial_mass[onEOS]
        form_t = form_t[onEOS]
        onEOS = onEOS[onEOS]

    r = np.linalg.norm(pos, axis=1)
    # mask = (r < 30)

    #halo gas J
    gas_J = np.sum(mass[:, np.newaxis] * np.cross(pos, vel), axis=0)

    #box orientation
    (box_R, box_phi, box_z, box_v_R, box_v_phi, box_v_z
     ) = get_cylindrical(pos, vel)

    # galaxy sizes
    (box_R_half, box_R_quarter, box_R3quarter) = get_radii_that_are_interesting(box_R, mass)

    box_pos = pos.copy()
    box_vel = vel.copy()
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    # the orientation of the galaxy
    # (pos, vel, rotation) = align(pos, vel, mass, aperture=30)
    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    # cylindrical coords
    (R, phi, z, v_R, v_phi, v_z
     ) = get_cylindrical(pos, vel)

    #gas orientation
    if SFing_only:
        (gas_pos, gas_vel, _) = align(pos, vel, mass*SFR, GAS_FRAME_APERTURE_PHYSICAL, r)
    else:
        (gas_pos, gas_vel, _) = align(pos, vel, mass, GAS_FRAME_APERTURE_PHYSICAL, r)

    (gas_R, gas_phi, gas_z, gas_v_R, gas_v_phi, gas_v_z) = get_cylindrical(gas_pos, gas_vel)

    # j_z = np.cross(R, v_phi)[:, 2]
    j_z = pos[:, 0] * vel[:, 1] - pos[:, 1] * vel[:, 0]
    j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

    kin = 0.5 * (vel[:, 0] ** 2 + vel[:, 1] ** 2 + vel[:, 2] ** 2)

    arg_energy = np.argsort(pot + kin)
    arg_arg = np.argsort(arg_energy)

    # TODO this doesn't work for < 100ish star galaxies
    j_c_E = max_within_50(j_tot[arg_energy])[arg_arg]
    j_zonc = j_z / j_c_E

    #halo gas size
    if SFing_only:
        (gas_r_half, gas_r_quarter, gas_r3quarter) = get_radii_that_are_interesting(r, SFR*mass)
        (gas_R_half, gas_R_quarter, gas_R3quarter) = get_radii_that_are_interesting(R[r < STAR_FRAME_APERTURE_PHYSICAL],
                                                                                    SFR[r < STAR_FRAME_APERTURE_PHYSICAL] *
                                                                                    mass[r < STAR_FRAME_APERTURE_PHYSICAL])
    else:
        (gas_r_half, gas_r_quarter, gas_r3quarter) = get_radii_that_are_interesting(r, mass)
        (gas_R_half, gas_R_quarter, gas_R3quarter) = get_radii_that_are_interesting(R[r < STAR_FRAME_APERTURE_PHYSICAL],
                                                                                    mass[r < STAR_FRAME_APERTURE_PHYSICAL])

    # calculate profiles
    # lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z,
    #                                       j_zonc, form_t, initial_mass)
    log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z,
                                          j_zonc, form_t, initial_mass, pot, onEOS, density, temperature,
                                          stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    # proj_lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z,
    #                                            j_zonc, form_t, initial_mass)
    # proj_log_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z,
    #                                            j_zonc, form_t, initial_mass)

    # calculate
    aperture_30 = get_kinematic_aperture(STAR_FRAME_APERTURE_PHYSICAL, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_t,
                                         initial_mass, pot, onEOS, density, temperature,
                                         stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    aperture_r200 = get_kinematic_aperture(r200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                           form_t, initial_mass, pot, onEOS, density, temperature,
                                           stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)

    aperture_3d_r12 = get_kinematic_aperture(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                             form_t, initial_mass, pot, onEOS, density, temperature,
                                             stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    aperture_3d_5r12 = get_kinematic_aperture(5 * r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                              form_t, initial_mass, pot, onEOS, density, temperature,
                                              stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    # aperture_3d_5r12 = get_kinematic_aperture(r200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
    #                                           form_t, initial_mass, pot, onEOS, density, temperature,
    #                                           stars=False, gas=True, reorient=True, SF_weight=SFing_only, SFR=SFR)

    annuli_3d_r12 = get_kinematic_annuli(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                         form_t, initial_mass, pot, onEOS, density, temperature,
                                         stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    annuli_face_R12 = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                           form_t, initial_mass, pot, onEOS, density, temperature,
                                           stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    # annuli_face_R12 = get_kinematic_annuli(gas_r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
    #                                        form_t, initial_mass, pot, onEOS, density, stars=False, dex=0.5)

    annuli_3d_rs = get_kinematic_annuli(0.2 * rs, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                        form_t, initial_mass, pot, onEOS, density, temperature,
                                        stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)

    apature_box_R12_box = get_kinematic_aperture(box_R_half, box_pos, box_vel, mass, box_R, box_z, box_v_R, box_v_phi,
                                                 box_v_z, j_zonc, form_t, initial_mass, pot, onEOS, density, temperature,
                                                 stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)

    gas_log_profiles = get_kinematic_profiles(log_bin_edges, gas_pos, gas_vel, mass, r, gas_z, gas_v_R, gas_v_phi, gas_v_z,
                                              j_zonc, form_t, initial_mass, pot, onEOS, density, temperature,
                                              stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)
    gas_aperture_3d_star_r12 = get_kinematic_aperture(r_half, gas_pos, gas_vel, mass, r, gas_z, gas_v_R, gas_v_phi, gas_v_z,
                                                      j_zonc, form_t, initial_mass, pot, onEOS, density, temperature,
                                                      stars=False, gas=True, SF_weight=SFing_only, SFR=SFR)

    return [[gas_J,
             gas_r_half, gas_r_quarter, gas_r3quarter,
             gas_R_half, gas_R_quarter, gas_R3quarter],
            # [lin_profiles, log_profiles, proj_lin_profiles, proj_log_profiles,
            [log_profiles,
             aperture_30,  aperture_r200, aperture_3d_r12, aperture_3d_5r12, annuli_3d_r12,
             annuli_face_R12, annuli_3d_rs, apature_box_R12_box,
             gas_aperture_3d_star_r12, gas_log_profiles]]


def calculate_dm(sub_index, centre_of_potential, cr200, subgroup_number, rotation, vel_offset, rs, r_half, R_half):
    #use global kdtree
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    dm_index_mask = dm_kd_tree.query_ball_point(x=centre_of_potential,
                                                r=cr200)
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    dm_index_mask = np.array(dm_index_mask)

    if len(dm_index_mask) > 0:
        #use global dm subgroup number
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
        dm_index_mask = dm_index_mask[np.abs(SubNum_DM[dm_index_mask]) == subgroup_number]
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
        # dm_index_mask = dm_index_mask[(np.abs(SubNum_DM[dm_index_mask]) == 0)]

    if len(dm_index_mask) == 0:
        print(f'Warning: subgroup {sub_index} has no DM particles.')
        return NO_DM_RETURN

    r200 = cr200 * SCALE_A / LITTLE_H * 1000  # kcp

    #use global dm positions and velocities
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    pos = PosDM[dm_index_mask]
    vel = VelDM[dm_index_mask]
    pot = BindingEnergyDM[dm_index_mask]
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    #TODO subsample extremely massive haloes
    if len(pot) > DM_SUBSAMPLE_N:
        sub_sample_mask = np.random.choice(np.arange(len(pot)), DM_SUBSAMPLE_N, replace=False)

        mass = len(pot) / DM_SUBSAMPLE_N * DM_MASS * np.ones(DM_SUBSAMPLE_N)

        pos = pos[sub_sample_mask]
        vel = vel[sub_sample_mask]
        pot = pot[sub_sample_mask]

    else:
        mass = DM_MASS * np.ones(len(pot))  # 10^10 M_sun

    form_t = np.zeros(len(pot))
    initial_mass = np.zeros(len(pot))
    onEOS = np.zeros(len(pot), dtype=bool)
    density = np.zeros(len(pot))
    temperature = np.zeros(len(pot))
    j_zonc = np.zeros(len(pot))

    # real units and centre and everything
    pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
    pos %= SCALE_A * BOX_SIZE
    pos -= SCALE_A * BOX_SIZE / 2
    pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

    r = np.linalg.norm(pos, axis=1)

    vel *= np.sqrt(SCALE_A)  # km/s
    vel -= vel_offset

    DM_J = np.sum(np.cross(pos, vel), axis=0)

    #box orientation
    (box_R, box_phi, box_z, box_v_R, box_v_phi, box_v_z
     ) = get_cylindrical(pos, vel)

    (box_R_half, box_R_quarter, box_R3quarter) = get_radii_that_are_interesting(box_R, mass)

    box_pos = pos.copy()
    box_vel = vel.copy()

    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    # cylindrical coords
    (R, phi, z, v_R, v_phi, v_z
     ) = get_cylindrical(pos, vel)

    #halo gas size
    (DM_r_half, DM_r_quarter, DM_r3quarter) = get_radii_that_are_interesting(r, mass)
    frame_apature = r < STAR_FRAME_APERTURE_PHYSICAL
    (DM_R_half, DM_R_quarter, DM_R3quarter) = get_radii_that_are_interesting(R[frame_apature],
                                                                             mass[frame_apature])

    log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                          form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)

    # calculate
    aperture_30 = get_kinematic_aperture(STAR_FRAME_APERTURE_PHYSICAL, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                         form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)
    aperture_r200 = get_kinematic_aperture(r200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                           form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)

    aperture_3d_r12 = get_kinematic_aperture(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                             form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)
    aperture_3d_5r12 = get_kinematic_aperture(5 * r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                              form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)
    # aperture_3d_5r12 = get_kinematic_aperture(r200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
    #                                           form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True, reorient=True)

    annuli_3d_r12 = get_kinematic_annuli(r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                         form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)
    annuli_face_R12 = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                       form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)

    annuli_3d_rs = get_kinematic_annuli(0.2 * rs, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc,
                                        form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)

    apature_box_R12_box = get_kinematic_aperture(box_R_half, box_pos, box_vel, mass, box_R, box_z, box_v_R, box_v_phi,
                                                 box_v_z, j_zonc, form_t, initial_mass, pot, onEOS, density, temperature, stars=False, DM=True)

    return [[DM_J,
             DM_r_half, DM_r_quarter, DM_r3quarter,
             DM_R_half, DM_R_quarter, DM_R3quarter],
            # [lin_profiles, log_profiles, proj_lin_profiles, proj_log_profiles,
            [log_profiles,
             aperture_30,  aperture_r200, aperture_3d_r12, aperture_3d_5r12, annuli_3d_r12,
             annuli_face_R12, annuli_3d_rs, apature_box_R12_box,]]


def calculate_bh(sub_index, centre_of_potential, cr200, subgroup_number):
    # use global kdtree
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    bh_index_mask = bh_kd_tree.query_ball_point(x=centre_of_potential,
                                                r=cr200)
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    bh_index_mask = np.array(bh_index_mask)

    if len(bh_index_mask) > 0:
        #use global bh subgroup number
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
        bh_index_mask = bh_index_mask[np.abs(SubNum_BH[bh_index_mask]) == subgroup_number]
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    if len(bh_index_mask) == 0:
        return NO_BH_RETURN

    # r200 = cr200 * SCALE_A / LITTLE_H * 1000  # kcp

    # read global arrays and mask params relevant for calculation
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
    mass = MassBH[bh_index_mask]
    grav_mass = GravMassBH[bh_index_mask]
    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

    bh_bin_mass = np.sum(mass) / LITTLE_H
    bh_bin_grav_mass = np.sum(grav_mass) / LITTLE_H
    bh_bin_N = len(bh_index_mask)

    bh_mass = np.amax(mass) / LITTLE_H
    bh_grav_mass = np.amax(grav_mass) / LITTLE_H

    return([bh_bin_mass, bh_bin_grav_mass, bh_bin_N, bh_mass, bh_grav_mass])


def iterable_calculation(sub_index):
    #to stop stuff cloging up error out
    np.seterr(divide='ignore', invalid='ignore')
    warnings.filterwarnings('ignore', 'Casting complex values to real discards the imaginary part')
    warnings.filterwarnings('ignore', 'Degrees of freedom <= 0 for slice')
    warnings.filterwarnings('ignore', 'Values in x were outside bounds during a minimize step, clipping to bounds')

    #this should never break
    try:
        # read global arrays and mask params relevant for calculation
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####
        group_number = GroupNumber[sub_index]
        subgroup_number = SubGroupNumber[sub_index]

        cr200 = Group_R_Crit200[group_number - 1] # eagle units
        em200 = Group_M_Crit200[group_number - 1] # eagle units

        centre_of_potential = SubGroupCentreOfPotential[sub_index]
        centre_offset = pacman_dist(SubGroupCentreOfPotential[sub_index],
                                    SubGroupCentreOfMass[sub_index])
        ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####    ####

        m200 = em200 / LITTLE_H  #10^10 M_sun
        r200 = cr200 * SCALE_A / LITTLE_H * 1000  # kcp

        conc = concentration_ludlow(1 / SCALE_A - 1, m200)
        rs = r200 / conc

        halo_data = [[group_number, subgroup_number,
                      r200, m200, centre_of_potential, centre_offset]]

    except Exception as e:
        print(f'Exception was thrown calculating halo properties for group: {sub_index}')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        return NO_DATA_RETURN

    #stars
    try:
        (contains_stars, rotation, vel_offset, r_half, R_half, star_data
         ) = calculate_stars(sub_index, centre_of_potential, cr200, subgroup_number, rs)

    except Exception as e:
        print(f'Exception was thrown calculating stars for subgroup: {sub_index} ({group_number}, {subgroup_number})')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        return NO_DATA_RETURN

    if not contains_stars:
        print(f'Warning: No stars found after masking out galaxies with no stars for group: {sub_index} ({group_number}, {subgroup_number})')
        return NO_DATA_RETURN
        # return [[sub_index], *[i for i in halo_data[0]], *[i for i in NO_STAR_RETURN[0]],
        #         *[i for i in NO_GAS_RETURN[0]], *[i for i in NO_DM_RETURN[0]], *[i for i in NO_BH_RETURN],
        #         *[my_ndstack(([d[i] for d in NO_STAR_RETURN[1]])) for i in range(len(NO_STAR_RETURN[1][0]))],
        #         *[my_ndstack(([d[i] for d in NO_GAS_RETURN[1]])) for i in range(len(NO_GAS_RETURN[1][0]))],
        #         *[my_ndstack(([d[i] for d in NO_DM_RETURN[1]])) for i in range(len(NO_DM_RETURN[1][0]))]]

    # done stars

    #gas
    try:
        if not sub_has_gas[sub_index]:
            gas_data = NO_GAS_RETURN

        else:
            gas_data = calculate_gas(sub_index, centre_of_potential, cr200, subgroup_number, rotation, vel_offset,
                                     rs, r_half, R_half)

    except Exception as e:
        print(f'Exception was thrown calculating gas for subgroup: {sub_index} ({group_number}, {subgroup_number})')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        #TODO put properties calculated up till now here
        return NO_DATA_RETURN

    #star forming gas
    try:
        if not sub_has_gas[sub_index]:
            SFgas_data = NO_GAS_RETURN

        else:
            SFgas_data = calculate_gas(sub_index, centre_of_potential, cr200, subgroup_number, rotation, vel_offset,
                                     rs, r_half, R_half, SFing_only=True)

    except Exception as e:
        print(f'Exception was thrown calculating gas for subgroup: {sub_index} ({group_number}, {subgroup_number})')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        #TODO put properties calculated up till now here
        return NO_DATA_RETURN
    #done gas

    #DM
    try:
########################################################################################################################
        #TODO add this back in to speed up!
        # # only do dm profiles for centrals
        # if SubGroupNumber[sub_index] != 0:
        #     dm_data = NO_DM_RETURN

        # elif not sub_has_DM[sub_index]:
########################################################################################################################
        if not sub_has_DM[sub_index]:
            dm_data = NO_DM_RETURN

        else:
            dm_data = calculate_dm(sub_index, centre_of_potential, cr200, subgroup_number, rotation, vel_offset,
                                   rs, r_half, R_half)

    except Exception as e:
        print(f'Exception was thrown calculating DM for subgroup: {sub_index} ({group_number}, {subgroup_number})')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        #TODO put stars here
        return NO_DATA_RETURN
    # done DM

    #bh
    try:
        bh_data = calculate_bh(sub_index, centre_of_potential, cr200, subgroup_number)

    except Exception as e:
        print(f'Exception was thrown calculating BH for group: {sub_index} ({group_number}, {subgroup_number})')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        #TODO put properties calculated up till now here
        return NO_DATA_RETURN
    # done BH

    #record calculation progress
    if sub_index % 200 == 0 or sub_index < 64:
        print(f'done {sub_index} ({group_number}, {subgroup_number}) in {round(time.time() - multi_start_time, 1)}s. ',
              end='')
        # if sub_index < 99:
        #     print('')
        # else:
        time_to_finish = (time.time() - multi_start_time) / (sub_index + 1) * (HasStarsNsubgroups - sub_index)
        print(f'{str(round(time_to_finish, -2))[:-2]}s to finish at this rate.')

    out = [[sub_index],
           *[i for i in halo_data[0]], *[i for i in star_data[0]],
           *[i for i in gas_data[0]], *[i for i in SFgas_data[0]],
           *[i for i in dm_data[0]], *[i for i in bh_data],
           *[my_ndstack(([d[i] for d in star_data[1]])) for i in range(len(star_data[1][0]))],
           *[my_ndstack(([d[i] for d in gas_data[1]])) for i in range(len(gas_data[1][0]))],
           *[my_ndstack(([d[i] for d in SFgas_data[1]])) for i in range(len(SFgas_data[1][0]))],
           *[my_ndstack(([d[i] for d in dm_data[1]])) for i in range(len(dm_data[1][0]))]]

    #return calculation
    return out


def my_calculate(output_data_location, kdtree_location, snap,
                 star_kd_tree, gas_kd_tree, dm_kd_tree, bh_kd_tree,
                 TotNgroups, TotNsubgroups,
                 SubMassType,
                 Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
                 GroupNumber, SubGroupNumber, SubGroupCentreOfPotential, SubGroupCentreOfMass,
                 PosStar, VelStar, MassStar, InitialMassStar, Star_tform,
                 BindingEnergyStar,
                 GrpNum_Star, SubNum_Star,
                 PosGas, VelGas, MassGas,
                 onEOSGas, SFRGas, DensityGas, TemperatureGas,
                 BindingEnergyGas,
                 GrpNum_Gas, SubNum_Gas,
                 PosDM, VelDM,
                 BindingEnergyDM,
                 GrpNum_DM, SubNum_DM,
                 PosBH, MassBH, GravMassBH,
                 GrpNum_BH, SubNum_BH):

    # global HasStarsNsubgroups
    # HasStarsNsubgroups = TotNsubgroups

    #use Subhalo/MassType to mask subhaloes with stars
    sub_has_stars = SubMassType[:, 4] > 0

    # #keep all subhalos with stars and all central of hales with M_200 >10^10 Msun
    # keep_data = np.logical_or(sub_has_stars, np.logical_and(Group_M_Crit200 > 1/LITTLE_H, SubGroupNumber == 0))

    keep_data = sub_has_stars

    global HasStarsNsubgroups
    HasStarsNsubgroups = np.sum(keep_data)

    print(f'HasStarsNsubgroups: {HasStarsNsubgroups}') #z=0 fid. res. 41301

    GroupNumber = GroupNumber[keep_data]
    SubGroupNumber = SubGroupNumber[keep_data]

    SubGroupCentreOfPotential = SubGroupCentreOfPotential[keep_data]
    SubGroupCentreOfMass = SubGroupCentreOfMass[keep_data]

    SubMassType = SubMassType[keep_data]

    global sub_has_gas, sub_has_DM
    sub_has_gas = SubMassType[:, 0] > 0
    sub_has_DM = SubMassType[:, 1] > 0
    # sub_has_BH = SubMassType[:, 5] > 0

    lower_bin_edges = np.hstack((['r%.5f'%e for e in lin_bin_edges[:-1]], ['r%.5f'%e for e in log_bin_edges[:-1]],
                                 ['R%.5f'%e for e in lin_bin_edges[:-1]], ['R%.5f'%e for e in lin_bin_edges[:-1]],
                                 0, 0, 0, 0, '0.8 R1/2', '0.8 r1/2', '0.8 0.2 rs', '0',
                                 'rh 0-2', 'rh 2-4', 'rh 4-6', 'rh 6-8', 'rh 8-10', 'rh 10-12', 'rh 12-14',
                                 'r200 0-2', 'r200 2-4', 'r200 4-6', 'r200 6-8', 'r200 8-10', 'r200 10-12', 'r200 12-14')
                                ).astype(dtype=np.string_)
    upper_bin_edges = np.hstack((['r%.5f'%e for e in lin_bin_edges[1:]], ['r%.5f'%e for e in log_bin_edges[1:]],
                                 ['R%.5f'%e for e in lin_bin_edges[1:]], ['R%.5f'%e for e in lin_bin_edges[1:]],
                                 'r30', 'r200', 'r1/2', '5r1/2', '1.25 R1/2', '1.25 r1/2', '1.25 0.2 rs', 'box R1/2',
                                 'rh 0-2', 'rh 2-4', 'rh 4-6', 'rh 6-8', 'rh 8-10', 'rh 10-12', 'rh 12-14',
                                 'r200 0-2', 'r200 2-4', 'r200 4-6', 'r200 6-8', 'r200 8-10', 'r200 10-12', 'r200 12-14')
                                ).astype(dtype=np.string_)

    print('Putting all data in global scope')
    start_time = time.time()
    globals()['star_kd_tree'] = star_kd_tree
    globals()['gas_kd_tree'] = gas_kd_tree
    globals()['dm_kd_tree'] = dm_kd_tree
    globals()['bh_kd_tree'] = bh_kd_tree
    globals()['SubMassType'] = SubMassType
    globals()['Group_M_Crit200'] = Group_M_Crit200
    globals()['Group_R_Crit200'] = Group_R_Crit200
    globals()['GroupCentreOfPotential'] = GroupCentreOfPotential
    globals()['GroupNumber'] = GroupNumber
    globals()['SubGroupNumber'] = SubGroupNumber
    globals()['SubGroupCentreOfPotential'] = SubGroupCentreOfPotential
    globals()['SubGroupCentreOfMass'] = SubGroupCentreOfMass

    globals()['PosStar'] = PosStar
    globals()['VelStar'] = VelStar
    globals()['MassStar'] = MassStar
    globals()['InitialMassStar'] = InitialMassStar
    globals()['Star_tform'] = Star_tform
    globals()['BindingEnergyStar'] = BindingEnergyStar
    globals()['GrpNum_Star'] = GrpNum_Star
    globals()['SubNum_Star'] = SubNum_Star
    globals()['PosGas'] = PosGas
    globals()['VelGas'] = VelGas
    globals()['MassGas'] = MassGas
    globals()['onEOSGas'] = onEOSGas
    globals()['SFRGas'] = SFRGas
    globals()['DensityGas'] = DensityGas
    globals()['TemperatureGas'] = TemperatureGas
    globals()['BindingEnergyGas'] = BindingEnergyGas
    globals()['GrpNum_Gas'] = GrpNum_Gas
    globals()['SubNum_Gas'] = SubNum_Gas
    globals()['PosDM'] = PosDM
    globals()['VelDM'] = VelDM
    globals()['BindingEnergyDM'] = BindingEnergyDM
    globals()['GrpNum_DM'] = GrpNum_DM
    globals()['SubNum_DM'] = SubNum_DM
    globals()['PosBH'] = PosBH
    globals()['MassBH'] = MassBH
    globals()['GravMassBH'] = GravMassBH
    globals()['GrpNum_BH'] = GrpNum_BH
    globals()['SubNum_BH'] = SubNum_BH

    global multi_start_time
    multi_start_time = time.time()

    print('Starting multiprocessing and calculation')
    with mp.Pool(processes=31) as pool:
        out = pool.map(iterable_calculation, range(HasStarsNsubgroups), chunksize=1)

        # # TODO revert back
        # out = pool.map(iterable_calculation, range(51_000, 53_000), chunksize=1)

    print(f'Done in {round(time.time() - multi_start_time, 1)}s')

    # print(out)
    # print(np.shape(out))
    # print(np.shape(out[0]))
    # [print(np.shape(o), end=', ') for o in out[0]]
    # [print([np.shape(out[i][param]) for i in range(len(out))])
    #  for param in range(len(out[0]))]

    print('Attempting to make output arrays')

    try:
        (_, group_ids, subgroup_ids,
         galaxy_r200, galaxy_m200, sub_cops, centre_offset,
         star_J_box,
         star_half_mass_radius, star_quarter_mass_radius, star3quarter_mass_radius,
         star_half_mass_proj_radius, star_quarter_mass_proj_radius, star3quarter_mass_proj_radius,
         gas_J_box,
         gas_half_mass_radius, gas_quarter_mass_radius, gas3quarter_mass_radius,
         gas_half_mass_proj_radius, gas_quarter_mass_proj_radius, gas3quarter_mass_proj_radius,
         sfgas_J_box,
         sfgas_half_mass_radius, sfgas_quarter_mass_radius, sfgas3quarter_mass_radius,
         sfgas_half_mass_proj_radius, sfgas_quarter_mass_proj_radius, sfgas3quarter_mass_proj_radius,
         dm_J_box,
         dm_half_mass_radius, dm_quarter_mass_radius, dm3quarter_mass_radius,
         dm_half_mass_proj_radius, dm_quarter_mass_proj_radius, dm3quarter_mass_proj_radius,
         bh_bin_mass, bh_bin_grav_mass, bh_bin_N,
         bh_mass, bh_grav_mass,
         star_bin_mass, star_bin_N, fjzjc,
         star_kappa_rot, star_kappa_co,
         star_mean_v_R, star_mean_v_phi, star_median_v_phi,
         star_sigma_z, star_sigma_R, star_sigma_phi,
         star_z_half, star_axis_a, star_axis_b, star_axis_c,
         star_J_z, star_J_tot,
         star_formation_time_intervals, star_formation_age_percentiles,
         _, star_r_12, star_r_max,
         star_kinetic_energy, star_potential_energy, _,
         gas_bin_mass, gas_bin_N, gas_fjzjc,
         gas_kappa_rot, gas_kappa_co,
         gas_mean_v_R, gas_mean_v_phi, gas_median_v_phi,
         gas_sigma_z, gas_sigma_R, gas_sigma_phi,
         gas_z_half, gas_axis_a, gas_axis_b, gas_axis_c,
         gas_J_z, gas_J_tot, _, _,
         gas_sf_density_percentiles, gas_r_12, gas_r_max,
         gas_kinetic_energy, gas_potential_energy, gas_mass_temp,
         sfgas_bin_mass, sfgas_bin_N, sfgas_fjzjc,
         sfgas_kappa_rot, sfgas_kappa_co,
         sfgas_mean_v_R, sfgas_mean_v_phi, sfgas_median_v_phi,
         sfgas_sigma_z, sfgas_sigma_R, sfgas_sigma_phi,
         sfgas_z_half, sfgas_axis_a, sfgas_axis_b, sfgas_axis_c,
         sfgas_J_z, sfgas_J_tot, _, _,
         sfgas_sf_density_percentiles, sfgas_r_12, sfgas_r_max,
         sfgas_kinetic_energy, sfgas_potential_energy, sfgas_mass_temp,
         # dm_bin_mass,
         # dm_sigma_x, dm_sigma_y, dm_sigma_z,
         # dm_J_z, dm_J_tot,
         # dm_kinetic_energy,
         dm_bin_mass, dm_bin_N, _,
         dm_kappa_rot, dm_kappa_co,
         dm_mean_v_R, dm_mean_v_phi, dm_median_v_phi,
         dm_sigma_z, dm_sigma_R, dm_sigma_phi,
         dm_z_half, _,_,_,
         dm_J_z, dm_J_tot, _, _,
         _, dm_r_12, dm_r_max,
         dm_kinetic_energy, dm_potential_energy, _,
         ) = [np.array([out[i][param] for i in range(len(out))]) for param in range(len(out[0]))]

        # #group_ids = 0 if there is no stars
        yes_data = np.logical_or(group_ids != 0, np.logical_and(galaxy_m200 > 1, subgroup_ids == 0))
        print(f'There should be {np.sum(yes_data)} galaxies recorded.')

        SubMassType = SubMassType[yes_data]
        # #TODO revert back
        # SubMassType = SubMassType[51_000:53_000][yes_data]

        group_ids = group_ids[yes_data]
        subgroup_ids = subgroup_ids[yes_data]
        galaxy_r200 = galaxy_r200[yes_data]
        galaxy_m200 = galaxy_m200[yes_data]
        sub_cops = sub_cops[yes_data]
        centre_offset = centre_offset[yes_data]
        star_half_mass_proj_radius = star_half_mass_proj_radius[yes_data]
        star_quarter_mass_proj_radius = star_quarter_mass_proj_radius[yes_data]
        star3quarter_mass_proj_radius = star3quarter_mass_proj_radius[yes_data]
        star_half_mass_radius = star_half_mass_radius[yes_data]
        star_quarter_mass_radius = star_quarter_mass_radius[yes_data]
        star3quarter_mass_radius = star3quarter_mass_radius[yes_data]
        gas_half_mass_proj_radius = gas_half_mass_proj_radius[yes_data]
        gas_quarter_mass_proj_radius = gas_quarter_mass_proj_radius[yes_data]
        gas3quarter_mass_proj_radius = gas3quarter_mass_proj_radius[yes_data]
        gas_half_mass_radius = gas_half_mass_radius[yes_data]
        gas_quarter_mass_radius = gas_quarter_mass_radius[yes_data]
        gas3quarter_mass_radius = gas3quarter_mass_radius[yes_data]
        sfgas_half_mass_proj_radius = sfgas_half_mass_proj_radius[yes_data]
        sfgas_quarter_mass_proj_radius = sfgas_quarter_mass_proj_radius[yes_data]
        sfgas3quarter_mass_proj_radius = sfgas3quarter_mass_proj_radius[yes_data]
        sfgas_half_mass_radius = sfgas_half_mass_radius[yes_data]
        sfgas_quarter_mass_radius = sfgas_quarter_mass_radius[yes_data]
        sfgas3quarter_mass_radius = sfgas3quarter_mass_radius[yes_data]
        star_J_box = star_J_box[yes_data]
        gas_J_box = gas_J_box[yes_data]
        sfgas_J_box = sfgas_J_box[yes_data]
        dm_J_box = dm_J_box[yes_data]
        star_bin_mass = star_bin_mass[yes_data]
        star_bin_N = star_bin_N[yes_data]
        fjzjc = fjzjc[yes_data]
        star_kappa_rot = star_kappa_rot[yes_data]
        star_kappa_co = star_kappa_co[yes_data]
        star_mean_v_R = star_mean_v_R[yes_data]
        star_mean_v_phi = star_mean_v_phi[yes_data]
        star_median_v_phi = star_median_v_phi[yes_data]
        star_sigma_z = star_sigma_z[yes_data]
        star_sigma_R = star_sigma_R[yes_data]
        star_sigma_phi = star_sigma_phi[yes_data]
        star_z_half = star_z_half[yes_data]
        star_axis_a = star_axis_a[yes_data]
        star_axis_b = star_axis_b[yes_data]
        star_axis_c = star_axis_c[yes_data]
        star_J_z = star_J_z[yes_data]
        star_J_tot = star_J_tot[yes_data]
        star_formation_time_intervals = star_formation_time_intervals[yes_data]
        star_formation_age_percentiles = star_formation_age_percentiles[yes_data]
        star_r_12 = star_r_12[yes_data]
        star_r_max = star_r_max[yes_data]
        star_kinetic_energy = star_kinetic_energy[yes_data]
        star_potential_energy = star_potential_energy[yes_data]
        gas_bin_mass = gas_bin_mass[yes_data]
        gas_bin_N = gas_bin_N[yes_data]
        gas_fjzjc = gas_fjzjc[yes_data]
        gas_kappa_rot = gas_kappa_rot[yes_data]
        gas_kappa_co = gas_kappa_co[yes_data]
        gas_mean_v_R = gas_mean_v_R[yes_data]
        gas_mean_v_phi = gas_mean_v_phi[yes_data]
        gas_median_v_phi = gas_median_v_phi[yes_data]
        gas_sigma_z = gas_sigma_z[yes_data]
        gas_sigma_R = gas_sigma_R[yes_data]
        gas_sigma_phi = gas_sigma_phi[yes_data]
        gas_z_half = gas_z_half[yes_data]
        gas_axis_a = gas_axis_a[yes_data]
        gas_axis_b = gas_axis_b[yes_data]
        gas_axis_c = gas_axis_c[yes_data]
        gas_J_z = gas_J_z[yes_data]
        gas_J_tot = gas_J_tot[yes_data]
        gas_sf_density_percentiles = gas_sf_density_percentiles[yes_data]
        gas_r_12 = gas_r_12[yes_data]
        gas_r_max = gas_r_max[yes_data]
        gas_kinetic_energy = gas_kinetic_energy[yes_data]
        gas_potential_energy = gas_potential_energy[yes_data]
        gas_mass_temp = gas_mass_temp[yes_data]
        sfgas_bin_mass = sfgas_bin_mass[yes_data]
        sfgas_bin_N = sfgas_bin_N[yes_data]
        sfgas_fjzjc = sfgas_fjzjc[yes_data]
        sfgas_kappa_rot = sfgas_kappa_rot[yes_data]
        sfgas_kappa_co = sfgas_kappa_co[yes_data]
        sfgas_mean_v_R = sfgas_mean_v_R[yes_data]
        sfgas_mean_v_phi = sfgas_mean_v_phi[yes_data]
        sfgas_median_v_phi = sfgas_median_v_phi[yes_data]
        sfgas_sigma_z = sfgas_sigma_z[yes_data]
        sfgas_sigma_R = sfgas_sigma_R[yes_data]
        sfgas_sigma_phi = sfgas_sigma_phi[yes_data]
        sfgas_z_half = sfgas_z_half[yes_data]
        sfgas_axis_a = sfgas_axis_a[yes_data]
        sfgas_axis_b = sfgas_axis_b[yes_data]
        sfgas_axis_c = sfgas_axis_c[yes_data]
        sfgas_J_z = sfgas_J_z[yes_data]
        sfgas_J_tot = sfgas_J_tot[yes_data]
        sfgas_sf_density_percentiles = sfgas_sf_density_percentiles[yes_data]
        sfgas_r_12 = sfgas_r_12[yes_data]
        sfgas_r_max = sfgas_r_max[yes_data]
        sfgas_kinetic_energy = sfgas_kinetic_energy[yes_data]
        sfgas_potential_energy = sfgas_potential_energy[yes_data]
        sfgas_mass_temp = sfgas_mass_temp[yes_data]
        dm_bin_mass = dm_bin_mass[yes_data]
        # dm_sigma_x = dm_sigma_x[yes_data]
        # dm_sigma_y = dm_sigma_y[yes_data]
        # dm_sigma_z = dm_sigma_z[yes_data]
        dm_kappa_rot = dm_kappa_rot[yes_data]
        dm_kappa_co = dm_kappa_co[yes_data]
        dm_mean_v_R = dm_mean_v_R[yes_data]
        dm_mean_v_phi = dm_mean_v_phi[yes_data]
        dm_median_v_phi = dm_median_v_phi[yes_data]
        dm_sigma_z = dm_sigma_z[yes_data]
        dm_sigma_R = dm_sigma_R[yes_data]
        dm_sigma_phi = dm_sigma_phi[yes_data]
        dm_J_z = dm_J_z[yes_data]
        dm_J_tot = dm_J_tot[yes_data]
        bh_bin_mass = bh_bin_mass[yes_data]
        bh_bin_grav_mass = bh_bin_grav_mass[yes_data]
        bh_bin_N = bh_bin_N[yes_data]
        bh_mass = bh_mass[yes_data]
        bh_grav_mass = bh_grav_mass[yes_data]

    except Exception as e:
        print(f'Exception was thrown converting output to arrays.')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))
        raise e

    print('Done all calculations.')

    return (output_data_location, snap,
            lower_bin_edges, upper_bin_edges,
            SubMassType,
            group_ids, subgroup_ids,
            galaxy_r200, galaxy_m200, sub_cops, centre_offset,
            star_half_mass_proj_radius, star_quarter_mass_proj_radius, star3quarter_mass_proj_radius,
            star_half_mass_radius, star_quarter_mass_radius, star3quarter_mass_radius,
            gas_half_mass_proj_radius, gas_quarter_mass_proj_radius, gas3quarter_mass_proj_radius,
            gas_half_mass_radius, gas_quarter_mass_radius, gas3quarter_mass_radius,
            sfgas_half_mass_proj_radius, sfgas_quarter_mass_proj_radius, sfgas3quarter_mass_proj_radius,
            sfgas_half_mass_radius, sfgas_quarter_mass_radius, sfgas3quarter_mass_radius,
            star_J_box, gas_J_box, sfgas_J_box, dm_J_box,
            star_bin_mass, star_bin_N, fjzjc,
            star_kappa_rot, star_kappa_co,
            star_mean_v_R, star_mean_v_phi, star_median_v_phi,
            star_sigma_z, star_sigma_R, star_sigma_phi,
            star_z_half, star_axis_a, star_axis_b, star_axis_c,
            star_J_z, star_J_tot,
            star_formation_time_intervals, star_formation_age_percentiles, star_r_12, star_r_max,
            star_kinetic_energy, star_potential_energy,
            gas_bin_mass, gas_bin_N, gas_fjzjc,
            gas_kappa_rot, gas_kappa_co,
            gas_mean_v_R, gas_mean_v_phi, gas_median_v_phi,
            gas_sigma_z, gas_sigma_R, gas_sigma_phi,
            gas_z_half, gas_axis_a, gas_axis_b, gas_axis_c,
            gas_J_z, gas_J_tot,
            gas_sf_density_percentiles, gas_r_12, gas_r_max,
            gas_kinetic_energy, gas_potential_energy, gas_mass_temp,
            sfgas_bin_mass, sfgas_bin_N, sfgas_fjzjc,
            sfgas_kappa_rot, sfgas_kappa_co,
            sfgas_mean_v_R, sfgas_mean_v_phi, sfgas_median_v_phi,
            sfgas_sigma_z, sfgas_sigma_R, sfgas_sigma_phi,
            sfgas_z_half, sfgas_axis_a, sfgas_axis_b, sfgas_axis_c,
            sfgas_J_z, sfgas_J_tot,
            sfgas_sf_density_percentiles, sfgas_r_12, sfgas_r_max,
            sfgas_kinetic_energy, sfgas_potential_energy, sfgas_mass_temp,
            dm_bin_mass,
            dm_kappa_rot, dm_kappa_co,
            dm_mean_v_R, dm_mean_v_phi, dm_median_v_phi,
            dm_sigma_z, dm_sigma_R, dm_sigma_phi,
            dm_J_z, dm_J_tot,
            bh_bin_mass, bh_bin_grav_mass, bh_bin_N,
            bh_mass, bh_grav_mass)


def my_write(output_data_location, snap,
            lower_bin_edges, upper_bin_edges,
            SubMassType,
            group_ids, subgroup_ids,
            galaxy_r200, galaxy_m200, sub_cops, centre_offset,
            star_half_mass_proj_radius, star_quarter_mass_proj_radius, star3quarter_mass_proj_radius,
            star_half_mass_radius, star_quarter_mass_radius, star3quarter_mass_radius,
            gas_half_mass_proj_radius, gas_quarter_mass_proj_radius, gas3quarter_mass_proj_radius,
            gas_half_mass_radius, gas_quarter_mass_radius, gas3quarter_mass_radius,
            sfgas_half_mass_proj_radius, sfgas_quarter_mass_proj_radius, sfgas3quarter_mass_proj_radius,
            sfgas_half_mass_radius, sfgas_quarter_mass_radius, sfgas3quarter_mass_radius,
            star_J_box, gas_J_box, sfgas_J_box, dm_J_box,
            star_bin_mass, star_bin_N, fjzjc,
            star_kappa_rot, star_kappa_co,
            star_mean_v_R, star_mean_v_phi, star_median_v_phi,
            star_sigma_z, star_sigma_R, star_sigma_phi,
            star_z_half, star_axis_a, star_axis_b, star_axis_c,
            star_J_z, star_J_tot,
            star_formation_time_intervals, star_formation_age_percentiles, star_r_12, star_r_max,
            star_kinetic_energy, star_potential_energy,
            gas_bin_mass, gas_bin_N, gas_fjzjc,
            gas_kappa_rot, gas_kappa_co,
            gas_mean_v_R, gas_mean_v_phi, gas_median_v_phi,
            gas_sigma_z, gas_sigma_R, gas_sigma_phi,
            gas_z_half, gas_axis_a, gas_axis_b, gas_axis_c,
            gas_J_z, gas_J_tot,
            gas_sf_density_percentiles, gas_r_12, gas_r_max,
            gas_kinetic_energy, gas_potential_energy, gas_mass_temp,
            sfgas_bin_mass, sfgas_bin_N, sfgas_fjzjc,
            sfgas_kappa_rot, sfgas_kappa_co,
            sfgas_mean_v_R, sfgas_mean_v_phi, sfgas_median_v_phi,
            sfgas_sigma_z, sfgas_sigma_R, sfgas_sigma_phi,
            sfgas_z_half, sfgas_axis_a, sfgas_axis_b, sfgas_axis_c,
            sfgas_J_z, sfgas_J_tot,
            sfgas_sf_density_percentiles, sfgas_r_12, sfgas_r_max,
            sfgas_kinetic_energy, sfgas_potential_energy, sfgas_mass_temp,
            dm_bin_mass,
            dm_kappa_rot, dm_kappa_co,
            dm_mean_v_R, dm_mean_v_phi, dm_median_v_phi,
            dm_sigma_z, dm_sigma_R, dm_sigma_phi,
            dm_J_z, dm_J_tot,
            bh_bin_mass, bh_bin_grav_mass, bh_bin_N,
            bh_mass, bh_grav_mass):

    #convert arrays to 32 bit numbers
    SubMassType = SubMassType.astype(np.float32)
    group_ids = group_ids.astype(np.int32)
    subgroup_ids = subgroup_ids.astype(np.int32)
    galaxy_r200 = galaxy_r200.astype(np.float32)
    galaxy_m200 = galaxy_m200.astype(np.float32)
    sub_cops = sub_cops.astype(np.float32)
    centre_offset = centre_offset.astype(np.float32)
    star_half_mass_proj_radius = star_half_mass_proj_radius.astype(np.float32)
    star_quarter_mass_proj_radius = star_quarter_mass_proj_radius.astype(np.float32)
    star3quarter_mass_proj_radius = star3quarter_mass_proj_radius.astype(np.float32)
    star_half_mass_radius = star_half_mass_radius.astype(np.float32)
    star_quarter_mass_radius = star_quarter_mass_radius.astype(np.float32)
    star3quarter_mass_radius = star3quarter_mass_radius.astype(np.float32)
    gas_half_mass_proj_radius = gas_half_mass_proj_radius.astype(np.float32)
    gas_quarter_mass_proj_radius = gas_quarter_mass_proj_radius.astype(np.float32)
    gas3quarter_mass_proj_radius = gas3quarter_mass_proj_radius.astype(np.float32)
    gas_half_mass_radius = gas_half_mass_radius.astype(np.float32)
    gas_quarter_mass_radius = gas_quarter_mass_radius.astype(np.float32)
    gas3quarter_mass_radius = gas3quarter_mass_radius.astype(np.float32)
    sfgas_half_mass_proj_radius = sfgas_half_mass_proj_radius.astype(np.float32)
    sfgas_quarter_mass_proj_radius = sfgas_quarter_mass_proj_radius.astype(np.float32)
    sfgas3quarter_mass_proj_radius = sfgas3quarter_mass_proj_radius.astype(np.float32)
    sfgas_half_mass_radius = sfgas_half_mass_radius.astype(np.float32)
    sfgas_quarter_mass_radius = sfgas_quarter_mass_radius.astype(np.float32)
    sfgas3quarter_mass_radius = sfgas3quarter_mass_radius.astype(np.float32)
    star_J_box = star_J_box.astype(np.float32)
    gas_J_box = gas_J_box.astype(np.float32)
    dm_J_box = dm_J_box.astype(np.float32)
    star_bin_mass = star_bin_mass.astype(np.float32)
    star_bin_N = star_bin_N.astype(np.int32)
    fjzjc = fjzjc.astype(np.float32)
    star_kappa_rot = star_kappa_rot.astype(np.float32)
    star_kappa_co = star_kappa_co.astype(np.float32)
    star_mean_v_R = star_mean_v_R.astype(np.float32)
    star_mean_v_phi = star_mean_v_phi.astype(np.float32)
    star_median_v_phi = star_median_v_phi.astype(np.float32)
    star_sigma_z = star_sigma_z.astype(np.float32)
    star_sigma_R = star_sigma_R.astype(np.float32)
    star_sigma_phi = star_sigma_phi.astype(np.float32)
    star_z_half = star_z_half.astype(np.float32)
    star_axis_a = star_axis_a.astype(np.float32)
    star_axis_b = star_axis_b.astype(np.float32)
    star_axis_c = star_axis_c.astype(np.float32)
    star_J_z = star_J_z.astype(np.float32)
    star_J_tot = star_J_tot.astype(np.float32)
    star_formation_time_intervals = star_formation_time_intervals.astype(np.float32)
    star_formation_age_percentiles = star_formation_age_percentiles.astype(np.float32)
    star_r_12 = star_r_12.astype(np.float32)
    star_r_max = star_r_max.astype(np.float32)
    star_kinetic_energy = star_kinetic_energy.astype(np.float32)
    star_potential_energy = star_potential_energy.astype(np.float32)
    gas_bin_mass = gas_bin_mass.astype(np.float32)
    gas_bin_N = gas_bin_N.astype(np.int32)
    gas_fjzjc = gas_fjzjc.astype(np.float32)
    gas_kappa_rot = gas_kappa_rot.astype(np.float32)
    gas_kappa_co = gas_kappa_co.astype(np.float32)
    gas_mean_v_R = gas_mean_v_R.astype(np.float32)
    gas_mean_v_phi = gas_mean_v_phi.astype(np.float32)
    gas_median_v_phi = gas_median_v_phi.astype(np.float32)
    gas_sigma_z = gas_sigma_z.astype(np.float32)
    gas_sigma_R = gas_sigma_R.astype(np.float32)
    gas_sigma_phi = gas_sigma_phi.astype(np.float32)
    gas_z_half = gas_z_half.astype(np.float32)
    gas_axis_a = gas_axis_a.astype(np.float32)
    gas_axis_b = gas_axis_b.astype(np.float32)
    gas_axis_c = gas_axis_c.astype(np.float32)
    gas_J_z = gas_J_z.astype(np.float32)
    gas_J_tot = gas_J_tot.astype(np.float32)
    gas_sf_density_percentiles = gas_sf_density_percentiles.astype(np.float32)
    gas_r_12 = gas_r_12.astype(np.float32)
    gas_r_max = gas_r_max.astype(np.float32)
    gas_kinetic_energy = gas_kinetic_energy.astype(np.float32)
    gas_potential_energy = gas_potential_energy.astype(np.float32)
    gas_mass_temp = gas_mass_temp.astype(np.float32)
    sfgas_bin_mass = sfgas_bin_mass.astype(np.float32)
    sfgas_bin_N = sfgas_bin_N.astype(np.int32)
    sfgas_fjzjc = sfgas_fjzjc.astype(np.float32)
    sfgas_kappa_rot = sfgas_kappa_rot.astype(np.float32)
    sfgas_kappa_co = sfgas_kappa_co.astype(np.float32)
    sfgas_mean_v_R = sfgas_mean_v_R.astype(np.float32)
    sfgas_mean_v_phi = sfgas_mean_v_phi.astype(np.float32)
    sfgas_median_v_phi = sfgas_median_v_phi.astype(np.float32)
    sfgas_sigma_z = sfgas_sigma_z.astype(np.float32)
    sfgas_sigma_R = sfgas_sigma_R.astype(np.float32)
    sfgas_sigma_phi = sfgas_sigma_phi.astype(np.float32)
    sfgas_z_half = sfgas_z_half.astype(np.float32)
    sfgas_axis_a = sfgas_axis_a.astype(np.float32)
    sfgas_axis_b = sfgas_axis_b.astype(np.float32)
    sfgas_axis_c = sfgas_axis_c.astype(np.float32)
    sfgas_J_z = sfgas_J_z.astype(np.float32)
    sfgas_J_tot = sfgas_J_tot.astype(np.float32)
    sfgas_sf_density_percentiles = sfgas_sf_density_percentiles.astype(np.float32)
    sfgas_r_12 = sfgas_r_12.astype(np.float32)
    sfgas_r_max = sfgas_r_max.astype(np.float32)
    sfgas_kinetic_energy = sfgas_kinetic_energy.astype(np.float32)
    sfgas_potential_energy = sfgas_potential_energy.astype(np.float32)
    sfgas_mass_temp = sfgas_mass_temp.astype(np.float32)
    dm_bin_mass = dm_bin_mass.astype(np.float32)
    dm_kappa_rot = dm_kappa_rot.astype(np.float32)
    dm_kappa_co = dm_kappa_co.astype(np.float32)
    dm_mean_v_R = dm_mean_v_R.astype(np.float32)
    dm_mean_v_phi = dm_mean_v_phi.astype(np.float32)
    dm_median_v_phi = dm_median_v_phi.astype(np.float32)
    dm_sigma_z = dm_sigma_z.astype(np.float32)
    dm_sigma_R = dm_sigma_R.astype(np.float32)
    dm_sigma_phi = dm_sigma_phi.astype(np.float32)
    dm_J_z = dm_J_z.astype(np.float32)
    dm_J_tot = dm_J_tot.astype(np.float32)
    bh_bin_mass = bh_bin_mass.astype(np.float32)
    bh_bin_grav_mass = bh_bin_grav_mass.astype(np.float32)
    bh_bin_N = bh_bin_N.astype(np.int32)
    bh_mass = bh_mass.astype(np.float32)
    bh_grav_mass = bh_grav_mass.astype(np.float32)

    file_name = output_data_location + snap + '_galaxy_kinematic_profile_multi.hdf5'
    print('Output file ', file_name)
    # profile file
    with h5.File(file_name, 'w') as output:

        #TODO put bins etc. in header
        header = output.create_group('Header')
        header.create_dataset('LowerBinEdges', data=lower_bin_edges)
        header.create_dataset('UpperBinEdges', data=upper_bin_edges)
        header.create_dataset('MDMReciprocal', data=np.array([1 / DM_MASS]))
        header.create_dataset('MDM', data=np.array([DM_MASS]))
        header.create_dataset('ScaleFactor', data=np.array([SCALE_A]))

        quant = output.create_group('GalaxyQuantities')
        prof = output.create_group('GalaxyProfiles')
        star_prof = prof.create_group('Star')
        gas_prof = prof.create_group('Gas')
        sfgas_prof = prof.create_group('SFGas')
        dm_prof = prof.create_group('DM')
        # bh_prof = prof.create_group('BH')

        quant.create_dataset('GroupNumber',    data = group_ids)
        quant.create_dataset('SubGroupNumber', data = subgroup_ids)

        quant.create_dataset('SubMassType', data = SubMassType)

        quant.create_dataset('R200_crit', data=galaxy_r200)
        quant.create_dataset('M200_crit', data=galaxy_m200)
        # quant.create_dataset('GroupCOP', data=group_cops)
        quant.create_dataset('SubGroupCOP', data=sub_cops)
        quant.create_dataset('SubGroupOffset', data=centre_offset)

        quant.create_dataset('StellarHalfMassRadius', data=star_half_mass_radius)
        quant.create_dataset('StellarQuarterMassRadius', data=star_quarter_mass_radius)
        quant.create_dataset('Stellar3QuarterMassRadius', data=star3quarter_mass_radius)
        quant.create_dataset('StellarHalfMassProjRadius', data=star_half_mass_proj_radius)
        quant.create_dataset('StellarQuarterMassProjRadius', data=star_quarter_mass_proj_radius)
        quant.create_dataset('Stellar3QuarterMassProjRadius', data=star3quarter_mass_proj_radius)
        quant.create_dataset('GasHalfMassRadius', data=gas_half_mass_radius)
        quant.create_dataset('GasQuarterMassRadius', data=gas_quarter_mass_radius)
        quant.create_dataset('Gas3QuarterMassRadius', data=gas3quarter_mass_radius)
        quant.create_dataset('GasHalfMassProjRadius', data=gas_half_mass_proj_radius)
        quant.create_dataset('GasQuarterMassProjRadius', data=gas_quarter_mass_proj_radius)
        quant.create_dataset('Gas3QuarterMassProjRadius', data=gas3quarter_mass_proj_radius)
        quant.create_dataset('SFGasHalfMassRadius', data=sfgas_half_mass_radius)
        quant.create_dataset('SFGasQuarterMassRadius', data=sfgas_quarter_mass_radius)
        quant.create_dataset('SFGas3QuarterMassRadius', data=sfgas3quarter_mass_radius)
        quant.create_dataset('SFGasHalfMassProjRadius', data=sfgas_half_mass_proj_radius)
        quant.create_dataset('SFGasQuarterMassProjRadius', data=sfgas_quarter_mass_proj_radius)
        quant.create_dataset('SFGas3QuarterMassProjRadius', data=sfgas3quarter_mass_proj_radius)

        quant.create_dataset('JboxStar', data=star_J_box)
        quant.create_dataset('JboxGas', data=gas_J_box)
        quant.create_dataset('JboxSFGas', data=sfgas_J_box)
        quant.create_dataset('JboxDM', data=dm_J_box)

        # bh
        quant.create_dataset('BHBinMass', data=bh_bin_mass)
        quant.create_dataset('BHBinGravMass', data=bh_bin_grav_mass)
        quant.create_dataset('BHN', data=bh_bin_N)
        quant.create_dataset('BHMass', data=bh_mass)
        quant.create_dataset('BHGravMass', data=bh_grav_mass)

        # star
        star_prof.create_dataset('BinMass', data=star_bin_mass)
        star_prof.create_dataset('BinN', data=star_bin_N)

        star_prof.create_dataset('fjzjc', data=fjzjc)

        star_prof.create_dataset('kappaRot', data=star_kappa_rot)
        star_prof.create_dataset('kappaCo', data=star_kappa_co)

        star_prof.create_dataset('MeanVR', data=star_mean_v_R)
        star_prof.create_dataset('MeanVphi', data=star_mean_v_phi)
        star_prof.create_dataset('MedianVphi', data=star_median_v_phi)

        star_prof.create_dataset('sigmaz', data=star_sigma_z)
        star_prof.create_dataset('sigmaR', data=star_sigma_R)
        star_prof.create_dataset('sigmaphi', data=star_sigma_phi)

        star_prof.create_dataset('zHalf', data=star_z_half)

        star_prof.create_dataset('Axisa', data=star_axis_a)
        star_prof.create_dataset('Axisb', data=star_axis_b)
        star_prof.create_dataset('Axisc', data=star_axis_c)

        star_prof.create_dataset('Jz',   data=star_J_z)
        star_prof.create_dataset('Jtot', data=star_J_tot)

        star_prof.create_dataset('StarFormationRates',   data=star_formation_time_intervals)
        star_prof.create_dataset('PercentileStellarAge', data=star_formation_age_percentiles)

        star_prof.create_dataset('rHalf', data=star_r_12)
        star_prof.create_dataset('rMax', data=star_r_max)
        star_prof.create_dataset('KineticEnergy', data=star_kinetic_energy)
        star_prof.create_dataset('BindingEnergy', data=star_potential_energy)

        # gas
        gas_prof.create_dataset('BinMass', data=gas_bin_mass)
        gas_prof.create_dataset('BinN', data=gas_bin_N)

        gas_prof.create_dataset('fjzjc', data=gas_fjzjc)

        gas_prof.create_dataset('kappaRot', data=gas_kappa_rot)
        gas_prof.create_dataset('kappaCo', data=gas_kappa_co)

        gas_prof.create_dataset('MeanVR', data=gas_mean_v_R)
        gas_prof.create_dataset('MeanVphi', data=gas_mean_v_phi)
        gas_prof.create_dataset('MedianVphi', data=gas_median_v_phi)

        gas_prof.create_dataset('sigmaz', data=gas_sigma_z)
        gas_prof.create_dataset('sigmaR', data=gas_sigma_R)
        gas_prof.create_dataset('sigmaphi', data=gas_sigma_phi)

        gas_prof.create_dataset('zHalf', data=gas_z_half)

        gas_prof.create_dataset('Axisa', data=gas_axis_a)
        gas_prof.create_dataset('Axisb', data=gas_axis_b)
        gas_prof.create_dataset('Axisc', data=gas_axis_c)

        gas_prof.create_dataset('Jz',   data=gas_J_z)
        gas_prof.create_dataset('Jtot', data=gas_J_tot)

        gas_prof.create_dataset('PercentileSFDensity', data=gas_sf_density_percentiles)

        gas_prof.create_dataset('rHalf', data=gas_r_12)
        gas_prof.create_dataset('rMax', data=gas_r_max)

        gas_prof.create_dataset('KineticEnergy', data=gas_kinetic_energy)
        gas_prof.create_dataset('BindingEnergy', data=gas_potential_energy)
        gas_prof.create_dataset('MassTemperature', data=gas_mass_temp)

        # sfgas
        sfgas_prof.create_dataset('BinMass', data=sfgas_bin_mass)
        sfgas_prof.create_dataset('BinN', data=sfgas_bin_N)

        sfgas_prof.create_dataset('fjzjc', data=sfgas_fjzjc)

        sfgas_prof.create_dataset('kappaRot', data=sfgas_kappa_rot)
        sfgas_prof.create_dataset('kappaCo', data=sfgas_kappa_co)

        sfgas_prof.create_dataset('MeanVR', data=sfgas_mean_v_R)
        sfgas_prof.create_dataset('MeanVphi', data=sfgas_mean_v_phi)
        sfgas_prof.create_dataset('MedianVphi', data=sfgas_median_v_phi)

        sfgas_prof.create_dataset('sigmaz', data=sfgas_sigma_z)
        sfgas_prof.create_dataset('sigmaR', data=sfgas_sigma_R)
        sfgas_prof.create_dataset('sigmaphi', data=sfgas_sigma_phi)

        sfgas_prof.create_dataset('zHalf', data=sfgas_z_half)

        sfgas_prof.create_dataset('Axisa', data=sfgas_axis_a)
        sfgas_prof.create_dataset('Axisb', data=sfgas_axis_b)
        sfgas_prof.create_dataset('Axisc', data=sfgas_axis_c)

        sfgas_prof.create_dataset('Jz',   data=sfgas_J_z)
        sfgas_prof.create_dataset('Jtot', data=sfgas_J_tot)

        sfgas_prof.create_dataset('PercentileSFDensity', data=sfgas_sf_density_percentiles)

        sfgas_prof.create_dataset('rHalf', data=sfgas_r_12)
        sfgas_prof.create_dataset('rMax', data=sfgas_r_max)

        sfgas_prof.create_dataset('KineticEnergy', data=sfgas_kinetic_energy)
        sfgas_prof.create_dataset('BindingEnergy', data=sfgas_potential_energy)
        sfgas_prof.create_dataset('MassTemperature', data=sfgas_mass_temp)

        # dm
        dm_prof.create_dataset('BinMass', data=dm_bin_mass)

        # dm_prof.create_dataset('DMsigmax', data=dm_sigma_x)
        # dm_prof.create_dataset('DMsigmay', data=dm_sigma_y)
        # dm_prof.create_dataset('DMsigmaz', data=dm_sigma_z)

        dm_prof.create_dataset('kappaRot', data=dm_kappa_rot)
        dm_prof.create_dataset('kappaCo', data=dm_kappa_co)

        dm_prof.create_dataset('MeanVR', data=dm_mean_v_R)
        dm_prof.create_dataset('MeanVphi', data=dm_mean_v_phi)
        dm_prof.create_dataset('MedianVphi', data=dm_median_v_phi)

        dm_prof.create_dataset('sigmaz', data=dm_sigma_z)
        dm_prof.create_dataset('sigmaR', data=dm_sigma_R)
        dm_prof.create_dataset('sigmaphi', data=dm_sigma_phi)

        dm_prof.create_dataset('Jz',   data=dm_J_z)
        dm_prof.create_dataset('Jtot', data=dm_J_tot)

    print('File written.')

    return

####

#to get star particle age
def lbt(a=None,omm=0.307,oml=0.693,h=0.6777):
    z = 1.0/a-1
    t = np.zeros(len(z))

    for i in range(len(z)):
        if a[i] == 0:
            t[i] = 13.82968685
        else:
            t[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100*h)) * quad(
                lambda z: 1 / ( (1+z) * np.sqrt(omm*(1+z)**3 + oml) ), 0, z[i])[0] #in Gyr
    return t


def lbt_interp(a, omm=0.307, oml=0.693, h=0.6777, n=10_001):
    a_interp = np.linspace(0,1, n)
    z_interp = 1 / a_interp - 1
    t_interp = np.zeros(len(a_interp))

    for i in range(len(z_interp)):
        if a_interp[i] == 0:
            t_interp[i] = 13.82968685
        else:
            t_interp[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100 * h)) * quad(
                lambda z: 1 / ((1 + z) * np.sqrt(omm * (1 + z) ** 3 + oml)), 0, z_interp[i])[0] #Gyr

    return np.interp(a, a_interp, t_interp)


def concentration_ludlow(z, M200):
    '''Ludlow et al. 2016. Appendix B
    M200 in 10^10 M_sun
    '''
    delta_sc = 1.686

    c_0 = 3.395 * (1 + z)**(-0.215)

    beta = 0.307 * (1 + z)**0.540

    gamma_1 = 0.628 * (1 + z)**(-0.047)

    gamma_2 = 0.317 * (1 + z)**(-0.893)

    omega_l0 = 0.693
    omega_m0 = 0.307
    little_h = 0.6777

    omega_l = omega_l0 / (omega_l0 + omega_m0 * (1 + z)**3)
    omega_m = 1 - omega_l

    psi  = omega_m**(4/7) - omega_l + (1 + omega_m / 2) * (1 + omega_l/70)
    psi0 = omega_m0**(4/7) - omega_l0 + (1 + omega_m0 / 2) * (1 + omega_l0/70)

    a = (1 + z)**(-1.0)

    Dz = omega_m / omega_m0 * psi0 / psi * (1 + z)**(-1.0)

    nu_0 = (4.135 - 0.564 * a**(-1.0) - 0.210 * a**(-2.0) +
          0.0557 * a**(-3.0) - 0.00348 * a**(-4.0)) * Dz**(-1.0)

    #TODO check if don't need little h if units are in 10^10 Msun
    # xi = (M200 / little_h)**(-1.0)
    xi = (M200)**(-1.0)

    sigma = Dz * 22.26 * xi**0.292 / (1 + 1.53 * xi**0.275 + 3.36 * xi**0.198)

    nu = delta_sc / sigma

    c = c_0 * (nu / nu_0)**(-gamma_1) * (1 + (nu / nu_0)**(1/beta) )**(-beta*(gamma_2 - gamma_1))

    return(c)


def get_cylindrical(PosStars, VelStars):
    """Calculates cylindrical coordinates.
    """
    rho = np.sqrt(np.square(PosStars[:, 0]) + np.square(PosStars[:, 1]))
    varphi = np.arctan2(PosStars[:, 1], PosStars[:, 0])
    z = PosStars[:, 2]

    v_rho = VelStars[:, 0] * np.cos(varphi) + VelStars[:, 1] * np.sin(varphi)
    v_varphi = -VelStars[:, 0] * np.sin(varphi) + VelStars[:, 1] * np.cos(varphi)
    v_z = VelStars[:, 2]

    return (rho, varphi, z, v_rho, v_varphi, v_z)


def find_rotaion_matrix(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0

    #brentq default tolerances: xtol = 2e-12, rtol = 8.881784197001252e-16
    #there also seems to be a rounding error in the rotation matrixes
    #already aligned
    if (np.log10(np.abs(j_vector[2])) - np.log10(np.abs(j_vector[0])) > 7 and
        np.log10(np.abs(j_vector[2])) - np.log10(np.abs(j_vector[1])) > 7):
        return Rotation.from_euler('x', [0])

    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    try:
        y = brentq(fy, 0, 180)
    except ValueError as e:
        print('Exception was trown when trying to find rotation matrix x')
        print(np.log10(np.abs(j_vector[2])) - np.log10(np.abs(j_vector[0])),
              np.log10(np.abs(j_vector[1])) - np.log10(np.abs(j_vector[0])),
              j_vector, fy(0), fy(180))
        print('Assuming no rotation is appropriate')
        # print(''.join(tb.format_exception(None, e, e.__traceback__)))

        return Rotation.from_euler('x', [0])

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    try:
        x = brentq(fx, 0, 180)
    except ValueError as e:
        print('Exception was trown when trying to find rotation matrix y')
        print(np.log10(np.abs(j_vector[2])) - np.log10(np.abs(j_vector[0])),
              np.log10(np.abs(j_vector[1])) - np.log10(np.abs(j_vector[0])),
              j_vector, fx(0), fx(180), y, fy(y))
        print('Assuming no rotation is appropriate')
        print(''.join(tb.format_exception(None, e, e.__traceback__)))

        return Rotation.from_euler('x', [0])

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)

    if j_tot[2] < 0:
        x += 180

    return Rotation.from_euler('yx', [y, x], degrees=True)


# def find_rotaion_matrix(j_vector):
#     """Returns a scipy.spatial.transform.Rotation object.
#     R = find_rotaton_matrix(galaxy_anular_momentum)
#     pos = R.apply(pos)
#     """
#     # rotate until x coord = 0
#     fy = lambda y: Rotation.from_euler('y', y, degrees=False).apply(j_vector)[0]
#     y = brentq(fy, 0, np.pi)
#
#     # rotate until y coord = 0
#     fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=False).apply(j_vector)[1]
#     x = brentq(fx, 0, np.pi)
#
#     # check it isn't upsidedown
#     j_tot = Rotation.from_euler('yx', [y, x], degrees=False).apply(j_vector)
#
#     if j_tot[2] < 0:
#         x += np.pi
#
#     return Rotation.from_euler('yx', [y, x], degrees=False)


def find_velocity_max(m, x, v):

    def to_min(args):
        rot = Rotation.from_euler('yx', [args[1], args[0]])

        x_dash = rot.apply(x)
        v_dash = rot.apply(v)

        v_max = np.sum(m * (x_dash[:, 0] * v_dash[:, 1] - x_dash[:, 1] * v_dash[:, 0]) /
                       np.sqrt(x_dash[:, 0] ** 2 + x_dash[:, 1] ** 2))

        return -v_max

    x_angles = np.linspace(0, np.pi * 7/4, 8)
    y_angles = np.linspace(0, np.pi * 7/4, 8)

    best = 0
    x_best = 0
    y_best = 0
    for x_angle in x_angles:
        for y_angle in y_angles:
            guess = to_min((y_angle, x_angle))
            if guess < best:
                best = guess
                x_best = x_angle
                y_best = y_angle

    out = minimize(to_min, np.array([y_best,x_best]), method='SLSQP', bounds=((y_best - 0.6, y_best + 0.6),
                                                                              (x_best - 0.6, x_best + 0.6)))

    rot = Rotation.from_euler('yx', [out.x[1], out.x[0]])

    x_dash = rot.apply(x)
    v_dash = rot.apply(v)

    phi = np.arctan2(x_dash[:, 1], x_dash[:, 0])
    v_phi = -v_dash[:, 0] * np.sin(phi) + v_dash[:, 1] * np.cos(phi)
    v_max = np.sum(m * v_phi) / np.sum(m)

    return v_max


def align(pos, vel, mass, aperture=STAR_FRAME_APERTURE_PHYSICAL, r=None):
    """Aligns the z cartesian direction with the direction of angular momentum.
    Can use an aperture.
    """
    # direction to align
    if aperture is not None:
        if r is None:
            r = np.linalg.norm(pos, axis=1)

        mask = r < aperture
        if np.sum(mask) == 0:
            return pos, vel, Rotation.from_euler('x', [0])

        j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

    else:
        j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

    # find rotation
    rotation = find_rotaion_matrix(j_tot)

    # rotate stars
    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    return pos, vel, rotation


# def vec_pacman_dist(u, v):
#     '''vectorized periodic seperation in Mpc / h
#     '''
#
#     dx = np.abs(u[:, 0] - v[:, 0])
#     dy = np.abs(u[:, 1] - v[:, 1])
#     dz = np.abs(u[:, 2] - v[:, 2])
#     dxm = dx > BOX_HALF
#     dym = dy > BOX_HALF
#     dzm = dz > BOX_HALF
#     dx[dxm] = BOX_SIZE - dx[dxm]
#     dy[dym] = BOX_SIZE - dy[dym]
#     dz[dzm] = BOX_SIZE - dz[dzm]
#
#     return np.sqrt(dx * dx + dy * dy + dz * dz)


def pacman_dist(u, v):
    '''vectorized periodic seperation in Mpc / h
    '''

    dx = np.abs(u[0] - v[0])
    dy = np.abs(u[1] - v[1])
    dz = np.abs(u[2] - v[2])
    dxm = dx > BOX_HALF
    dym = dy > BOX_HALF
    dzm = dz > BOX_HALF
    if dxm: dx = BOX_SIZE - dx
    if dym: dy = BOX_SIZE - dy
    if dzm: dz = BOX_SIZE - dz

    return np.sqrt(dx * dx + dy * dy + dz * dz)


def get_radii_that_are_interesting(R, mass):
    """Find star half mass, quarter mass and 3 quarter mass.
    """
    # arg_order = np.argsort(R)
    # cum_mass = np.cumsum(mass[arg_order])
    # # fraction
    # cum_mass /= cum_mass[-1]
    #
    # half = R[arg_order][np.where(cum_mass > 0.50)[0][0]]
    # quarter = R[arg_order][np.where(cum_mass > 0.25)[0][0]]
    # three = R[arg_order][np.where(cum_mass > 0.75)[0][0]]

    # return half, quarter, three

    return my_weighted_quartile(R, [0.5, 0.25, 0.75], mass)


# TODO this doesn't work for < 100ish star galaxies
def max_within_50(array, fifty=50):
    """returns the largest value of array within 50 indices
    probably can be faster
    """
    list_len = np.size(array)

    max_array = np.zeros(list_len)

    for i in range(list_len):
        low = np.amax((0, i - fifty))
        hi = np.amin((i + fifty, list_len))

        max_array[i] = np.amax(array[low:hi])

    return max_array


def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
    """Calculates the reduced inertia tensor
    M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
    Itterative selection is done in the other function.
    """
    norm = m_p / e2_p
    s_n = np.sum(norm)

    m = np.zeros((3, 3))

    if s_n != 0:
        for i in range(3):
            for j in range(3):
                m[i, j] = np.sum(norm * r_p[:, i] * r_p[:, j])

        m /= np.sum(norm)

    return m


def process_tensor(m):
    """
    """
    # I think I messed np.linalg.eigh(m) up when I was first writing thins
    if np.any(np.isnan(m)) or np.any(np.isinf(m)):
        # print('Found a nan or inf', m)
        return np.zeros(3, dtype=np.float32), np.identity(3, dtype=np.float32)

    (eigan_values, eigan_vectors) = np.linalg.eig(m)

    order = np.flip(np.argsort(eigan_values))

    eigan_values = eigan_values[order]
    eigan_vectors = eigan_vectors[:, order]

    return eigan_values, eigan_vectors


def defined_particles(pos, mass, eigan_values, eigan_vectors):
    """Assumes eigan values are sorted
    """
    # projection along each axis
    projected_a = (pos[:, 0] * eigan_vectors[0, 0] + pos[:, 1] * eigan_vectors[1, 0] +
                   pos[:, 2] * eigan_vectors[2, 0])
    projected_b = (pos[:, 0] * eigan_vectors[0, 1] + pos[:, 1] * eigan_vectors[1, 1] +
                   pos[:, 2] * eigan_vectors[2, 1])
    projected_c = (pos[:, 0] * eigan_vectors[0, 2] + pos[:, 1] * eigan_vectors[1, 2] +
                   pos[:, 2] * eigan_vectors[2, 2])

    # ellipse distance #Thob et al. 2019 eqn 4.
    ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1] / eigan_values[0]) +
                        np.square(projected_c) / (eigan_values[2] / eigan_values[0]))

    # #this seems to make almost no difference.
    # #I'm not convinced that the method in Thob is the correct way to do this
    # #ellipse radius #Thob et al. 2019 eqn 4.
    # ellipse_radius = np.power((eigan_values[1] * eigan_values[2]) / np.square(eigan_values[0]), 1/3
    #                           ) * 900
    # #Thob et al. 2019 eqn 4.
    # inside_mask = ellipse_distance <= ellipse_radius
    # return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])

    return pos, mass, ellipse_distance


def find_abc(pos, mass, converge_tol2=0.0001, max_iter=20, max_particles=100_000):
    """Finds the major, intermediate and minor axes.
    Follows Thob et al. 2019 using quadrupole moment of mass to bias towards
    particles closer to the centre
    """
    if np.shape(pos)[0] > max_particles:
        choice = np.random.choice(np.shape(pos)[0], max_particles, replace=False)
        pos = pos[choice]
        mass = mass[choice]

    try:
        # start off speherical
        r2 = np.square(np.linalg.norm(pos, axis=1))

        # stop problems with r=0
        pos = pos[r2 != 0]
        mass = mass[r2 != 0]
        r2 = r2[r2 != 0]

        # mass tensor of particles
        m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, r2)

        # linear algebra stuff
        (eigan_values, eigan_vectors) = process_tensor(m)

        # to see convergeance
        cona = np.sqrt(eigan_values[2] / eigan_values[0])
        bona = np.sqrt(eigan_values[1] / eigan_values[0])

        # done = False
        for i in range(max_iter):

            # redefine particles, calculate ellipse distance
            (pos, mass, ellipse_r2) = defined_particles(pos, mass, eigan_values, eigan_vectors)

            if len(mass) == 0:
                # print('Warning: No particles left when finding shape')
                return (np.zeros(3, dtype=np.float32))

            # mass tensor of new particles
            m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, ellipse_r2)

            # linear algebra stuff
            (eigan_values, eigan_vectors) = process_tensor(m)

            if (1 - eigan_values[2] / eigan_values[0] / cona < converge_tol2) and (
                    1 - eigan_values[1] / eigan_values[0] / bona < converge_tol2):
                # converged
                # done = True
                break

            else:
                cona = np.sqrt(eigan_values[2] / eigan_values[0])
                bona = np.sqrt(eigan_values[1] / eigan_values[0])

        # some warnings
        # if not done:
        #   print('Warning: Shape did not converge.')

        # if len(mass) < 100:
        #   print('Warning: Defining shape with <100 particles.')

    except (ValueError, TypeError) as e:
        return (np.zeros(3, dtype=np.float32))

    return np.sqrt(eigan_values).astype(np.float32)


def my_binned_statistic(x, value, bins, statistic='sum'):
    """Calls binned statistic, but returns zeros when x and value are empty.
    """
    if len(x) == 0 and len(value) == 0:
        out = np.zeros(len(bins) - 1)
    else:
        out = binned_statistic(x, value, bins=bins, statistic=statistic)[0]

    return out


def my_already_binned_statistic(value, digitized, bin_edges, statistic=np.sum, dtype=np.float32):
    """Calculates the statistic in bins if binned_statistic has already digitized
    the data set.
    Should be faster than calling binned statistic a bunch of times ......
    """
    n = len(bin_edges) - 1
    out_array = np.zeros(n, dtype=dtype)

    # if statistic == 'sum':
    #     func = np.sum
    # elif statistic == 'median':
    #     func = np.median
    # else: #elif type(statistic) == type(lambda : None):
    #     func = statistic
    # # else:
    # #     raise ValueError('Statistic ' + str(statistic) + ' not understood.')

    func = statistic

    # would be faster without a for loop
    for i in range(n):
        out_array[i] = func(value[digitized == i + 1])

    return out_array


def my_weighted_quartile(data, quartile, weights=None):
    size = np.size(data)
    if size == 1:
        return np.repeat(data, len(quartile))
    elif size == 0:
        return np.nan * np.ones(len(quartile))

    if weights is None:
        # return np.nanquantile(data, quartile)
        weights = np.ones(len(data))

    arg_order = np.argsort(data)

    ordered_data = data[arg_order]
    ordered_weights = weights[arg_order]

    normed_ordered_weights = np.cumsum(ordered_weights)
    normed_ordered_weights /= normed_ordered_weights[-1]

    return np.interp(quartile, normed_ordered_weights, ordered_data)


def get_kinematic_profiles(bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass, potential,
                           onEOS, density, temperature,
                           stars=True, gas=False, DM=False, reorient=False, SF_weight=False, SFR=None):
    """Given galaxy particle data and bins, calculates kenematic profiles.
    See Genel et al. 2015, Lagos et al. 2017, Correra et al. 2017, Wilkinson et al. 2021
    Probably not super efficent to call binned statistic a bunch of times, can use
    the third output to mask bins. Too lazy / will be more confusing to code though.
    """
    number_of_bins = len(bin_edges) - 1

    # binned
    if number_of_bins < 1:
        return(NO_BINNED_PARTICLES_RETURN(0))

    FRAME_APERTURE = STAR_FRAME_APERTURE_PHYSICAL
    if gas:
        FRAME_APERTURE = GAS_FRAME_APERTURE_PHYSICAL

    if SF_weight:
        mass = mass * SFR

    # (bin_mass, _, bin_number) = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
    bin_number = np.digitize(R, bins=bin_edges)
    bin_mass = my_already_binned_statistic(mass, bin_number, bin_edges, np.sum)

    # bin_N = np.histogram(R, bins=bin_edges)[0]
    bin_N = np.bincount(bin_number, minlength=len(bin_edges))[1:len(bin_edges)]

    # j_zonc
    # I don't like this. Needs to be done though
    fjzjc = np.zeros((number_of_bins, n_jzjc_bins), dtype=np.float32)

    # calculate kappas
    K_tot = np.zeros(number_of_bins, dtype=np.float32)
    K_rot = np.zeros(number_of_bins, dtype=np.float32)
    K_co = np.zeros(number_of_bins, dtype=np.float32)

    # sigmas
    mean_v_R = np.zeros(number_of_bins, dtype=np.float32)
    mean_v_phi = np.zeros(number_of_bins, dtype=np.float32)
    median_v_phi = np.zeros(number_of_bins, dtype=np.float32)

    sigma_z = np.zeros(number_of_bins, dtype=np.float32)
    sigma_R = np.zeros(number_of_bins, dtype=np.float32)
    sigma_phi = np.zeros(number_of_bins, dtype=np.float32)

    z_half = np.zeros(number_of_bins, dtype=np.float32)

    a_axis = np.zeros(number_of_bins, dtype=np.float32)
    b_axis = np.zeros(number_of_bins, dtype=np.float32)
    c_axis = np.zeros(number_of_bins, dtype=np.float32)

    J_z   = np.zeros(number_of_bins, dtype=np.float32)
    J_tot = np.zeros(number_of_bins, dtype=np.float32)

    star_formation_time_intervals = np.zeros((number_of_bins, n_formation_time_intervals), dtype=np.float32)
    star_formation_age_percentiles = np.zeros((number_of_bins, n_formation_age_percentiles), dtype=np.float32)

    density_gas_percentiles = np.zeros((number_of_bins, n_rho_gas_percentiles), dtype=np.float32)

    r_12 = np.zeros(number_of_bins, dtype=np.float32)
    r_max = np.zeros(number_of_bins, dtype=np.float32)

    potential_energy = np.zeros(number_of_bins, dtype=np.float32)

    mass_temp = np.zeros(number_of_bins, dtype=np.float32)

    # would be faster without a for loop
    for i in range(number_of_bins):
        if bin_mass[i] == 0:
            continue

        mask = (bin_number == i + 1)

        bin_massi = 1 / bin_mass[i]

        mmass = mass[mask]
        mpos = pos[mask]
        mvel = vel[mask]
        mR = R[mask]

        #reorient each annuli
        if reorient:
            #trust the R is spherical radii
            if np.any(mR < FRAME_APERTURE):
                (mpos, mvel, _) = align(mpos, mvel, mmass, FRAME_APERTURE, mR)
            else:
                (mpos, mvel, _) = align(mpos, mvel, mmass, None, mR)

            _, _, mz, mv_R, mv_phi, mv_z = get_cylindrical(mpos, mvel)

        else:
            (mz, mv_R, mv_phi, mv_z) = (z[mask], v_R[mask], v_phi[mask], v_z[mask])

        # j_zonc
        # if stars:
        mj_zonc = j_zonc[mask]
        for j, X in enumerate(np.linspace(-1, 1, n_jzjc_bins)):
            fjzjc[i, j] = np.sum(mmass[mj_zonc >= X]) * bin_massi

        # calculate kappas
        K_tot[i] = np.sum(mmass * np.square(np.linalg.norm(mvel, axis=1)))
        K_rot[i] = np.sum(mmass * np.square(mv_phi))

        co = (mv_phi > 0)
        K_co[i] = np.sum(mmass[co] * np.square(mv_phi[co]))

        # sigmas
        # mean_v_R[i] = np.sum(mmass * mv_R) * bin_massi
        mean_v_R[i] = find_velocity_max(mmass, mpos, mvel)
        mean_v_phi[i] = np.sum(mmass * mv_phi) * bin_massi
        # fast_mask = mv_phi**2 > (mvel[:, 0]**2 + mvel[:, 1]**2 + mvel[:, 2]**2) * 0.9
        # fmmass = mmass[fast_mask]
        # median_v_phi[i] = np.sum(fmmass * mv_phi[fast_mask]) / np.sum(fmmass)
        # median_v_phi[i] = np.sqrt(np.sum(mmass * mv_phi**2) * bin_massi)
        median_v_phi[i] = my_weighted_quartile(mv_phi, np.array([0.5]), weights=mmass)

        sigma_z[i] = np.sum(mmass * np.square(mv_z)) * bin_massi
        sigma_R[i] = np.sum(mmass * np.square(mv_R)) * bin_massi
        sigma_phi[i] = np.sum(mmass * np.square(mv_phi - mean_v_phi[i])) * bin_massi

        # TODO replace with mass weighted median
        # np.median(np.abs(mz))
        z_half[i] = my_weighted_quartile(np.abs(mz), np.array([0.5]), weights=mmass)

        if not DM:
            if bin_N[i] > 10:
                try:
                    a_axis[i], b_axis[i], c_axis[i] = find_abc(mpos, mmass)
                except Exception as e:
                    # a_axis[i], b_axis[i], c_axis[i] = 0., 0., 0.
                    # print(e)
                    pass
            # else:
                # a_axis[i], b_axis[i], c_axis[i] = 0., 0., 0.

        vec_J = np.sum(np.cross(mpos, mvel) * mmass[:, np.newaxis], axis=0)
        J_z[i] = vec_J[2]
        J_tot[i] = np.linalg.norm(vec_J)

        r_12[i] = my_weighted_quartile(mR, np.array([0.5]), weights=mmass)
        r_max[i] = np.amax(mR)

        potential_energy[i] = np.sum(mmass * potential[mask])

        #sf
        if stars:
            minitial_mass = initial_mass[mask]
            mform_t = form_t[mask]
            for j in range(n_formation_time_intervals):
                time_interval = 2**(-(n_formation_time_intervals-1)//2) *  2 ** j #[0.25, 0.5, 1, 2]
                min_t = CURRENT_LBT + time_interval

                star_formation_time_intervals[i, j] = np.sum(minitial_mass[mform_t <= min_t]) / time_interval #10^10 Mstar / Gyr

            star_formation_age_percentiles[i] = my_weighted_quartile(mform_t, formation_age_percentiles, weights=mmass)

        if gas:
            monEOS = onEOS[mask]
            sf_mmass = mmass[monEOS]
            sf_mdensity = density[mask][monEOS]
            density_gas_percentiles[i] = my_weighted_quartile(sf_mdensity, rho_gas_percentiles, weights=sf_mmass)
            mass_temp[i] = np.sum(mmass * temperature[mask])

    kappa_rot = K_rot / K_tot
    kappa_co = K_co / K_tot

    sigma_z = np.sqrt(sigma_z)
    sigma_R = np.sqrt(sigma_R)
    sigma_phi = np.sqrt(sigma_phi)

    return [bin_mass, bin_N,
            fjzjc,
            kappa_rot, kappa_co,
            mean_v_R, mean_v_phi, median_v_phi,
            sigma_z, sigma_R, sigma_phi,
            z_half, a_axis, b_axis, c_axis,
            J_z, J_tot,
            star_formation_time_intervals, star_formation_age_percentiles,
            density_gas_percentiles,
            r_12, r_max, K_tot / 2, potential_energy, mass_temp]


def get_kinematic_aperture(aperture, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass, pot,
                           onEOS, density, temperature,
                           stars=True, gas=False, DM=False, reorient=False, SF_weight=False, SFR=None):
    """Same as get_kinematic_profile, except aperture rather than profile.
    """
    # mask
    mask = (R < aperture)

    if np.sum(mask) == 0:
        return NO_PARTICLE_RETURN

    return get_kinematic_bin(mask, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass, pot,
                             onEOS, density, temperature,
                             stars, gas, DM, reorient, SF_weight, SFR)


def get_kinematic_annuli(aperture, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass, pot,
                         onEOS, density, temperature,
                         stars=True, gas=False, DM=False, dex = 0.2, reorient=False, SF_weight=False, SFR=None):
    """Same as get_kinematic_profile, except aperture rather than profile.
    """
    # mask
    mask = np.logical_and(10 ** (-dex / 2) < R / aperture, R / aperture < 10 ** (dex / 2))

    return get_kinematic_bin(mask, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass, pot,
                             onEOS, density, temperature, stars, gas, DM, reorient, SF_weight, SFR)


def get_kinematic_bin(mask, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_t, initial_mass, potential,
                      onEOS, density, temperature,
                      stars=True, gas=False, DM=False, reorient=False, SF_weight=False, SFR=None):

    if np.sum(mask) == 0:
        return NO_PARTICLE_RETURN

    FRAME_APERTURE = STAR_FRAME_APERTURE_PHYSICAL
    if gas:
        FRAME_APERTURE = GAS_FRAME_APERTURE_PHYSICAL

    pos = pos[mask]
    vel = vel[mask]
    mass = mass[mask]
    if SF_weight:
        mass = mass * SFR[mask]
    R = R[mask]
    potential = potential[mask]

    if reorient:
        #trust the R is spherical radii
        if np.any(R < FRAME_APERTURE):
            (pos, vel, _) = align(pos, vel, mass, FRAME_APERTURE, R)
        else:
            (pos, vel, _) = align(pos, vel, mass, None, R)

        _, _, z, v_R, v_phi, v_z = get_cylindrical(pos, vel)

    else:
        (z, v_R, v_phi, v_z) = (z[mask], v_R[mask], v_phi[mask], v_z[mask])

    bin_mass = np.sum(mass)
    bin_N = np.size(mass)

    density_gas_percentiles = np.zeros(n_rho_gas_percentiles, dtype=np.float32)
    star_formation_age_percentiles = np.zeros(n_formation_age_percentiles, dtype=np.float32)
    mass_temp = 0

    bin_massi = 1 / bin_mass

    # jz/jc
    fjzjc = np.zeros(n_jzjc_bins, dtype=np.float32)
    if stars or gas:
        j_zonc = j_zonc[mask]
        for j, X in enumerate(np.linspace(-1, 1, n_jzjc_bins)):
            fjzjc[j] = np.sum(mass[j_zonc >= X]) * bin_massi

    # calculate kappas
    K_tot = np.sum(mass * np.square(np.linalg.norm(vel, axis=1)))
    K_rot = np.sum(mass * np.square(v_phi))
    K_co = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

    kappa_rot = K_rot / K_tot
    kappa_co = K_co / K_tot

    # sigmas
    # mean_v_R = np.sum(mass * v_R) * bin_massi
    mean_v_R = find_velocity_max(mass, pos, vel)
    mean_v_phi = np.sum(mass * v_phi) * bin_massi
    # fast_mask = v_phi ** 2 > (vel[:, 0] ** 2 + vel[:, 1] ** 2 + vel[:, 2] ** 2) * 0.9
    # fmass = mass[fast_mask]
    # median_v_phi = np.sum(fmass * v_phi[fast_mask]) / np.sum(fmass)
    # median_v_phi = np.sqrt(np.sum(mass * v_phi ** 2) * bin_massi)
    median_v_phi = my_weighted_quartile(v_phi, np.array([0.5]), weights=mass)[0]

    sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
    sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
    sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

    z_half = my_weighted_quartile(np.abs(z), np.array([0.5]), weights=mass)[0]

    if not DM:
        a_axis, b_axis, c_axis = find_abc(pos, mass)
    else:
        a_axis, b_axis, c_axis = (0,0,0)

    vec_J = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)
    J_z = vec_J[2]
    J_tot = np.linalg.norm(vec_J)

    potential_energy = np.sum(mass * potential)

    # sf
    star_formation_time_intervals = np.zeros(n_formation_time_intervals, dtype=np.float32)
    if stars:
        initial_mass = initial_mass[mask]
        form_t = form_t[mask]
        for j in range(n_formation_time_intervals):
            time_interval = 2 ** (-(n_formation_time_intervals - 1) // 2) * 2 ** j  # [0.25, 0.5, 1, 2]
            min_t = CURRENT_LBT + time_interval

            star_formation_time_intervals[j] = np.sum(initial_mass[form_t <= min_t]) / time_interval  # 10^10 Mstar / Gyr

        star_formation_age_percentiles = my_weighted_quartile(form_t, formation_age_percentiles, weights=mass)

    if gas:
        onEOS = onEOS[mask]
        sf_mass = mass[onEOS]
        sf_density = density[mask][onEOS]
        density_gas_percentiles = my_weighted_quartile(sf_density, rho_gas_percentiles, weights=sf_mass)
        mass_temp = np.sum(mass * temperature[mask])

    r_12 = my_weighted_quartile(R, np.array([0.5]), weights=mass)[0]
    r_max = np.amax(R)

    return [bin_mass, bin_N,
            fjzjc,
            kappa_rot, kappa_co,
            mean_v_R, mean_v_phi, median_v_phi,
            sigma_z, sigma_R, sigma_phi,
            z_half, a_axis, b_axis, c_axis,
            J_z, J_tot,
            star_formation_time_intervals, star_formation_age_percentiles,
            density_gas_percentiles,
            r_12, r_max, K_tot / 2, potential_energy, mass_temp]


def read_calculate_write(args):
    # time
    start_time = time.time()

    raw_data = my_read(*args)
    print(str(np.round(time.time() - start_time, 1)))

    processed_data = my_calculate(*raw_data)
    print(str(np.round(time.time() - start_time, 1)))

    my_write(*processed_data)
    print('Finished snap in ' + str(np.round(time.time() - start_time, 1)) + 's')

    return


if __name__ == '__main__':
    _, snap_index = sys.argv

    snap_index = int(snap_index)

    print('Warning: The code currently calculates DM profiles for subhaloes')

    #hyades
    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/',
                                   '/mnt/su3ctm/mwilkinson/EAGLE/L0025N0752/RECAL/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/',
                                '/mnt/su3ctm/mwilkinson/EAGLE/L0025N0752/RECAL/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0025N752/RECAL/']

    #ozstar
    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location = particle_data_location_list[snap_index // 4]
    halo_data_location = halo_data_location_list[snap_index // 4]
    output_data_location =  output_data_location_list[snap_index // 4]

    DM_MASS = [6.570332889156362E-4 / 0.6777, 6.570332889156362E-4 / 0.6777 / 7,
               6.570332889156362E-4 / 0.6777 / 8][snap_index // 4] # 10^10 Msun

    kdtree_location = output_data_location

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]

    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    current_lbt_list = [0, 5.198223, 7.945284, 10.531461] #Gyr #[0, 5.22014126, 7.96152142, 10.55063966]

    SCALE_A = SCALE_A_list[snap_index % 4]
    CURRENT_LBT = current_lbt_list[snap_index % 4]

    #TODO this should be read in
    BOX_SIZE = [33.885, 16.9425][snap_index // 8]  # 50 * 0.6777 #Mpc / h
    BOX_HALF = [16.9425, 8.47125][snap_index // 8]  # 50 / 2 * 0.6777 #Mpc / h

    print('particle_data_location:', particle_data_location)
    print('halo_data_location:', halo_data_location)
    print('output_data_location:', output_data_location)
    print('kdtree_location:', kdtree_location)
    print('snap:', snap)

    # do the thing
    read_calculate_write((particle_data_location, halo_data_location, output_data_location, kdtree_location, snap))

    pass
