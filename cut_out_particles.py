#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

# import matplotlib
# matplotlib.use('Agg')

import h5py as h5

from scipy.integrate import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
BOX_SIZE = 33.885 #50 * 0.6777 #kpc
# SCALE_A  = 1
DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun


def lbt_interp(a, omm=0.307, oml=0.693, h=0.6777, n=10_001):
    a_interp = np.linspace(0,1, n)
    z_interp = 1 / a_interp - 1
    t_interp = np.zeros(len(a_interp))

    for i in range(len(z_interp)):
        if a_interp[i] == 0:
            t_interp[i] = 13.82968685
        else:
            t_interp[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100 * h)) * quad(
                lambda z: 1 / ((1 + z) * np.sqrt(omm * (1 + z) ** 3 + oml)), 0, z_interp[i])[0] #Gyr

    return np.interp(a, a_interp, t_interp)


def my_read(particle_data_location, halo_data_location, output_data_location, kdtree_location, snap, group_number):
    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    FirstSub = np.zeros(TotNgroups, dtype=np.int64)

    # Subhalo arrays
    GroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    NGrp_c = 0
    NSub_c = 0

    print('TotNGroups:', TotNgroups)
    print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = particle_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            if Nsubgroups > 0:
                GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
                SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]

                SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]

                NSub_c += Nsubgroups

    print('Loaded halos')

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    # NumPartTot = [0, NumPartTot[1], 0, 0, 0]

    # if NumPartTot[4] > 0:
    PosStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    VelStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    MassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    Star_aform = np.zeros(NumPartTot[4], dtype=np.float32)
    Metallicity = np.zeros(NumPartTot[4], dtype=np.float32)
    IDsStar = np.zeros(NumPartTot[4], dtype=np.int64)
    GrpNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)
    SubNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)

    PosGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    VelGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    MassGas = np.zeros(NumPartTot[0], dtype=np.float32)
    IDsGas = np.zeros(NumPartTot[0], dtype=np.int64)
    GrpNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)
    SubNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)

    PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    VelDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    IDsDM = np.zeros(NumPartTot[1], dtype=np.int64)
    GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    SubNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)

    NStar_c = 0
    NGas_c = 0
    NDM_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[0] > 0:
                PosGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Coordinates"][()]
                VelGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Velocity"][()]
                MassGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/Mass"][()]
                IDsGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/ParticleIDs"][()]
                GrpNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/GroupNumber"][()]
                SubNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/SubGroupNumber"][()]

                NGas_c += NumParts[0]

            if NumParts[1] > 0:
                PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                VelDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Velocity"][()]
                IDsDM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/ParticleIDs"][()]
                GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"][()]
                SubNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/SubGroupNumber"][()]

                NDM_c += NumParts[1]

            if NumParts[4] > 0:
                PosStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Coordinates"][()]
                VelStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Velocity"][()]
                MassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Mass"][()]
                Star_aform[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/StellarFormationTime"][()]
                Metallicity[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Metallicity"][()]
                IDsStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleIDs"][()]
                GrpNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/GroupNumber"][()]
                SubNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SubGroupNumber"][()]

                NStar_c += NumParts[4]

    print('loaded particles')

    start_time = time.time()
    star_kd_tree = cKDTree(PosStar, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated star kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
    start_time = time.time()
    gas_kd_tree  = cKDTree(PosGas, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated gas kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')

    # #tree is too large to fit into ram
    # if not(kdtree_location == '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/' and
    #        snap == '028_z000p000'):
    dm_tree_file_name = kdtree_location + snap + '_dm_tree.pickle'
    start_time = time.time()
    print('DM tree file name ' + dm_tree_file_name)
    if os.path.exists(dm_tree_file_name):
        print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            dm_kd_tree = pickle.load(dm_tree_file)
        print('Opened dm kdtree')

    else:
        print('Calculating')
        dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
        print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(dm_kd_tree, dm_tree_file, protocol=4)
        print('Saved dm kdtree.')
    print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    # else:
    #     print('skipped DM tree')

    print('Done all kdtrees.')

    Star_tform = lbt_interp(Star_aform)

    print('loaded everything')

    return (output_data_location, kdtree_location, snap, group_number,
            star_kd_tree, gas_kd_tree, dm_kd_tree,
            TotNgroups, TotNsubgroups,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
            GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
            PosStar, VelStar, MassStar, Star_tform, Metallicity,
            GrpNum_Star, SubNum_Star, IDsStar,
            PosGas, VelGas, MassGas,
            GrpNum_Gas, SubNum_Gas, IDsGas,
            PosDM, VelDM,
            GrpNum_DM, SubNum_DM, IDsDM)


def my_calculate(output_data_location, kdtree_location, snap, group_number,
                star_kd_tree, gas_kd_tree, dm_kd_tree,
                TotNgroups, TotNsubgroups,
                Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
                GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
                PosStar, VelStar, MassStar, Star_tform, Metallicity,
                GrpNum_Star, SubNum_Star, IDsStar,
                PosGas, VelGas, MassGas,
                GrpNum_Gas, SubNum_Gas, IDsGas,
                PosDM, VelDM,
                GrpNum_DM, SubNum_DM, IDsDM):

    r200 = Group_R_Crit200[group_number - 1]
    centre_of_potential = GroupCentreOfPotential[group_number - 1]

    Star_index_mask = star_kd_tree.query_ball_point(x=centre_of_potential,
                                                    r=2 * r200)
    Star_index_mask = np.array(Star_index_mask)
    if len(Star_index_mask) > 0:
        PosStar = PosStar[Star_index_mask]
        VelStar = VelStar[Star_index_mask]
        MassStar = MassStar[Star_index_mask]
        Star_tform = Star_tform[Star_index_mask]
        Metallicity = Metallicity[Star_index_mask]
        GrpNum_Star = GrpNum_Star[Star_index_mask]
        SubNum_Star = SubNum_Star[Star_index_mask]
        IDsStar = IDsStar[Star_index_mask]

    Gas_index_mask = gas_kd_tree.query_ball_point(x=centre_of_potential,
                                                  r=2 * r200)
    Gas_index_mask = np.array(Gas_index_mask)
    if len(Gas_index_mask) > 0:
        PosGas = PosGas[Gas_index_mask]
        VelGas = VelGas[Gas_index_mask]
        MassGas = MassGas[Gas_index_mask]
        GrpNum_Gas = GrpNum_Gas[Gas_index_mask]
        SubNum_Gas = SubNum_Gas[Gas_index_mask]
        IDsGas = IDsGas[Gas_index_mask]

    DM_index_mask = dm_kd_tree.query_ball_point(x=centre_of_potential,
                                                r=2 * r200)
    DM_index_mask = np.array(DM_index_mask)
    if len(DM_index_mask) > 0:
        PosDM = PosDM[DM_index_mask]
        VelDM = VelDM[DM_index_mask]
        GrpNum_DM = GrpNum_DM[DM_index_mask]
        SubNum_DM = SubNum_DM[DM_index_mask]
        IDsDM = IDsDM[DM_index_mask]

    return (output_data_location, snap, group_number,
            PosStar, VelStar, MassStar, Star_tform, Metallicity,
            GrpNum_Star, SubNum_Star, IDsStar,
            PosGas, VelGas, MassGas,
            GrpNum_Gas, SubNum_Gas, IDsGas,
            PosDM, VelDM,
            GrpNum_DM, SubNum_DM, IDsDM)


def my_write(output_data_location, snap, group_number,
            PosStar, VelStar, MassStar, Star_tform, Metallicity,
            GrpNum_Star, SubNum_Star, IDsStar,
            PosGas, VelGas, MassGas,
            GrpNum_Gas, SubNum_Gas, IDsGas,
            PosDM, VelDM,
            GrpNum_DM, SubNum_DM, IDsDM):

    file_name = f'{output_data_location}{snap}_group{group_number}_cutout.hdf5'
    print('Output file ', file_name)
    # profile file
    with h5.File(file_name, 'w') as output:
        PartType0 = output.create_group('PartType0')
        PartType1 = output.create_group('PartType1')
        PartType4 = output.create_group('PartType4')

        PartType0.create_dataset('Coordinates', data=PosGas)
        PartType0.create_dataset('Velocity', data=VelGas)
        PartType0.create_dataset('Mass', data=MassGas)
        PartType0.create_dataset('ParticleIDs', data=GrpNum_Gas)
        PartType0.create_dataset('GroupNumber', data=SubNum_Gas)
        PartType0.create_dataset('SubGroupNumber', data=IDsGas)

        PartType1.create_dataset('Coordinates', data=PosDM)
        PartType1.create_dataset('Velocity', data=VelDM)
        PartType1.create_dataset('ParticleIDs', data=GrpNum_DM)
        PartType1.create_dataset('GroupNumber', data=SubNum_DM)
        PartType1.create_dataset('SubGroupNumber', data=IDsDM)

        PartType4.create_dataset('Coordinates', data=PosStar)
        PartType4.create_dataset('Velocity', data=VelStar)
        PartType4.create_dataset('Mass', data=MassStar)
        PartType4.create_dataset('StellarFormationTime', data=Star_tform)
        PartType4.create_dataset('Metallicity', data=Metallicity)
        PartType4.create_dataset('ParticleIDs', data=GrpNum_Star)
        PartType4.create_dataset('GroupNumber', data=SubNum_Star)
        PartType4.create_dataset('SubGroupNumber', data=IDsStar)

    print('File written.')

    return

####


def read_calculate_write(args):
    # time
    start_time = time.time()

    raw_data = my_read(*args)
    print(str(np.round(time.time() - start_time, 1)))

    processed_data = my_calculate(*raw_data)
    print(str(np.round(time.time() - start_time, 1)))

    my_write(*processed_data)
    print('Finished snap in ' + str(np.round(time.time() - start_time, 1)) + 's')

    return


if __name__ == '__main__':
    _, snap_index, group_number = sys.argv

    snap_index = int(snap_index)

    group_number = int(group_number)

    #hyades
    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location = particle_data_location_list[snap_index // 4]
    halo_data_location = halo_data_location_list[snap_index // 4]
    output_data_location =  output_data_location_list[snap_index // 4]

    kdtree_location = output_data_location

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]

    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    #TODO change medians to mass weighted median!

    # do the thing
    read_calculate_write((particle_data_location, halo_data_location, output_data_location, kdtree_location, snap, group_number))

    pass
