#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time

import numpy as np

import h5py as h5

np.seterr(divide='ignore', invalid='ignore')

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
BOX_SIZE = 33.885 #50 * 0.6777 #kpc
# SCALE_A  = 1

def my_read(particle_data_location, halo_data_location, output_data_location, kdtree_location, snap):
    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    # FirstSub = np.zeros(TotNgroups, dtype=np.int64)

    # Subhalo arrays
    GroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    SubMDM = np.zeros(TotNsubgroups, dtype=np.float32)
    SubMgas = np.zeros(TotNsubgroups, dtype=np.float32)
    SubMstar = np.zeros(TotNsubgroups, dtype=np.float32)
    SubMBH = np.zeros(TotNsubgroups, dtype=np.float32)

    NGrp_c = 0
    NSub_c = 0

    print('TotNGroups:', TotNgroups)
    print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = particle_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                # FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            if Nsubgroups > 0:
                GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
                SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]

                SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]

                SubMgas[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/MassType"][:, 0]
                SubMDM[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/MassType"][:, 1]
                SubMstar[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/MassType"][:, 4]
                SubMBH[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/MassType"][:, 5]

                NSub_c += Nsubgroups

    print('Loaded halos')

    print('loaded everything')

    return (output_data_location, snap,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
            GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
            SubMgas, SubMDM, SubMstar, SubMBH)


def my_write(output_data_location, snap,
             Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
             GroupNumber, SubGroupNumber, SubGroupCentreOfPotential,
             SubMgas, SubMDM, SubMstar, SubMBH):

    file_name = output_data_location + snap + '_some_subhalo_properties.hdf5'
    print('Output file ', file_name)
    # profile file
    with h5.File(file_name, 'w') as output:

        group = output.create_group('Group')
        sub = output.create_group('SubGroup')

        group.create_dataset('Group_M_Crit200', data = Group_M_Crit200)
        group.create_dataset('Group_R_Crit200', data = Group_R_Crit200)
        group.create_dataset('GroupCentreOfPotential', data = GroupCentreOfPotential)

        sub.create_dataset('GroupNumber', data = GroupNumber)
        sub.create_dataset('SubgroupNumber', data = SubGroupNumber)
        sub.create_dataset('CentreOfPotential', data = SubGroupCentreOfPotential)

        sub.create_dataset('MassType0', data = SubMgas)
        sub.create_dataset('MassType1', data = SubMDM)
        sub.create_dataset('MassType4', data = SubMstar)
        sub.create_dataset('MassType5', data = SubMBH)

    print('File written.')

    return


def read_calculate_write(args):
    # time
    start_time = time.time()

    raw_data = my_read(*args)
    print(str(np.round(time.time() - start_time, 1)))

    my_write(*raw_data)
    print('Finished snap in ' + str(np.round(time.time() - start_time, 1)) + 's')

    return


if __name__ == '__main__':
    _, snap_index = sys.argv

    snap_index = int(snap_index)

    # # ozstar 7xdm
    # particle_data_location = '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/'
    # halo_data_location = '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/'
    # output_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/'

    # # ozstar fiducial 50mpc
    # particle_data_location = '/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/'
    # halo_data_location = '/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/'
    # output_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/'

    # # hyades 7xdm
    # particle_data_location = '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/'
    # halo_data_location = '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/'
    # output_data_location = '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm'

    particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
                                   '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
                               '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location = particle_data_location_list[snap_index // 4]
    halo_data_location = halo_data_location_list[snap_index // 4]
    output_data_location =  output_data_location_list[snap_index // 4]

    kdtree_location = output_data_location

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]

    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    #TODO change medians to mass weighted median!

    # do the thing
    read_calculate_write((particle_data_location, halo_data_location, output_data_location, kdtree_location, snap))

    pass
