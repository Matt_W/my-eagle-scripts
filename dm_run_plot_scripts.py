#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

from functools import lru_cache

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d

from scipy.integrate   import quad

from scipy.optimize    import minimize
from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.special import kn, iv
from scipy.special import erf
from scipy.special import lambertw
from scipy.special import spence #aka dilogarithm

from scipy.stats import binned_statistic
import scipy.stats

import warnings

import matplotlib
# matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt

import matplotlib.lines as lines
import matplotlib.patheffects as path_effects

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 12#20
rcParams['xtick.labelsize'] = 12#20
rcParams['ytick.labelsize'] = 12#20
rcParams['axes.labelsize'] = 12#22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

n_jzjc_bins = 21

NRUN = 14 #13

from my_galaxy_models import *
from morphology_hdf_keys import *


def my_better_violin_plot(axs,
                          x, y, xbin_edges, ybin_edges,
                          quartiles=None,
                          global_scale=True, local_scale=False, fixed_scale=False, density_scale=True, bin_scale=1,
                          alpha=0.8, color='C0', label=''):

    if quartiles is None:
        quartiles = []

    #the main slow part
    x_bin_index = np.digitize(x, bins=xbin_edges)

    # quartile_data = np.zeros((len(quartiles), len(xbin_edges)-1))
    # mean_data = np.zeros(len(xbin_edges)-1)
    # disp_data = np.zeros(len(xbin_edges)-1)
    median_data = np.zeros(len(xbin_edges)-1)
    sig_plus_data = np.zeros(len(xbin_edges)-1)
    sig_minu_data = np.zeros(len(xbin_edges)-1)

    if global_scale:

        max_height = 0

        for i in range(len(xbin_edges)-1):

            ydata = y[x_bin_index==(i+1)]
            ydata = ydata[~np.isnan(ydata)]
            hist_height,_ = np.histogram(ydata, bins=ybin_edges, density=False)

            if density_scale:
                max_height = np.amax(max_height, np.sum(hist_height))
            else:
                max_height = np.amax(max_height, hist_height)


        scale = bin_scale / max_height

    for i in range(len(xbin_edges)-1):

        ydata = y[x_bin_index==(i+1)]
        ydata = ydata[~np.isnan(ydata)]
        hist_height,y_bin_edges = np.histogram(ydata, bins=ybin_edges, density=False)

        if fixed_scale:
            scale = bin_scale
        elif local_scale:
            if density_scale:
                max_height = np.sum(hist_height)
            else:
                max_height = np.amax(hist_height)

            if max_height == 0:
                scale = 0
            else:
                scale = bin_scale / max_height

        if i==0:
            axs.fill_betweenx(np.repeat(y_bin_edges, 2)[1:-1],
                            np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) *
                                      np.ones(len(y_bin_edges)-1), 2),
                            np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) +
                                      hist_height * scale, 2),
                            where=(np.repeat(hist_height, 2) != 0),
                            label=label,
                            alpha=alpha, color=color, ec='k', zorder=10)

        else:
            axs.fill_betweenx(np.repeat(y_bin_edges, 2)[1:-1],
                            np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) *
                                      np.ones(len(y_bin_edges)-1), 2),
                            np.repeat(0.5*(xbin_edges[i] + xbin_edges[i+1]) +
                                      hist_height * scale, 2),
                            where=(np.repeat(hist_height, 2) != 0),
                            alpha=alpha, color=color, ec='k', zorder=10)

        # for j in range(len(quartiles)):
        #   quartile_data[j, i] = my_quantile(ydata, quartiles[j])

        if len(ydata) > 0:
            # mean_data[i] = np.mean(ydata)
            # disp_data[i] = np.std(ydata)
            median_data[i] = np.nanmedian(ydata)
            sig_plus_data[i] = np.nanquantile(ydata, 0.16)
            sig_minu_data[i] = np.nanquantile(ydata, 0.84)
        else:
            # mean_data[i] = np.nan
            # disp_data[i] = np.nan
            median_data[i] = np.nan
            sig_plus_data[i] = np.nan
            sig_minu_data[i] = np.nan

    # for j in range(len(quartiles)):
    # if j == 0 and label != '':
    #   plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), quartile_data[j],
    #            color='C9', linewidth=3, label='Quantiles')
    # else:
    #   plt.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), quartile_data[j],
    #            color='C9', linewidth=3)

    if label != '':
        axs.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), median_data,
             color=color, linewidth=3, label=label)
    else:
        axs.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), median_data,
                 color=color, linewidth=3)
    axs.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), sig_plus_data,
           color=color, linewidth=3, ls='--')
    axs.plot(0.5*(xbin_edges[1:] + xbin_edges[:-1]), sig_minu_data,
           color=color, linewidth=3, ls='--')

    return


###############################################################################


def my_bootstrap(data, n=1000, statistic=np.median, quantiles=[0.16, 0.50, 0.84]):
    estimates = np.zeros(n)
    ld = len(data)
    for i in range(n):
        estimates[i] = statistic(data[np.random.randint(0, ld, ld)])

    return [np.quantile(estimates, q) for q in quantiles]


def plot_x_vs_y(raw_data0,
                xkey, ykey, rkey, comp, ii, mask0,
                c0key=None, c0column=None, clims=None,
                quartiles=False, N_run=NRUN,
                colour='C0', linestyle='-', marker='.', cmap='turbo',
                bootstrap=True, shade=True, N_ress=None, nan_to_x=-10):

    xlims = lims_dict[xkey]
    ylims = lims_dict[ykey]

    if type(N_run) == int:
        N_run = np.linspace(*xlims, N_run)
    if xkey == 'age':
        # N_run = np.linspace(0, 14, 2*7+1)
        N_run = np.linspace(-1, 15, 8+1)

    if ykey == 'mbh':
        print('Removing high mass bins')
        N_run = N_run[:-2]

    if clims == None:
        clims = [None, None]

    if N_ress == None:
        # N_ress = [100, 1]
        N_ress = [-1] #[100, 1]
        # N_ress = [1]

    if np.sum(mask0) < 1:
        print('Masked all values')
        return None, None, None, None, None

    x0 = return_array_from_keys(raw_data0, xkey, rkey, comp, mask0, ii)
    y0 = return_array_from_keys(raw_data0, ykey, rkey, comp, mask0, ii)

    N0 = return_array_from_keys(raw_data0, 'Npart', rkey, comp, mask0, ii)
    # N0 = return_array_from_keys(raw_data0, 'Npart', 'r200', comp, mask0, ii)

    if nan_to_x is not None:
        x0[~np.isfinite(y0)] = nan_to_x
        y0[~np.isfinite(y0)] = nan_to_x

    not_crazy_mask = np.logical_and(np.isfinite(x0), np.isfinite(y0))

    # if np.sum(not_crazy_mask) == 0:
    #     continue
        # return

    if quartiles:
        x1 = x0[not_crazy_mask]
        y1 = y0[not_crazy_mask]

        x_n, xedges = np.histogram(x1, bins=N_run)
        bin_is = np.digitize(x1, bins=xedges)

        y_nx = np.histogram2d(y1, x1, bins=(quartiles, N_run))[0]

        y_quarts = y_nx / x_n
        y_cum = 1 - np.cumsum(y_quarts, axis=0)

        path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]

        quart_lims = [np.amin(quartiles[1:-1]), np.amax(quartiles[1:-1])]

        for i in range(np.shape(y_cum)[0]-1):
            color_frac = (quartiles[i+1] - quart_lims[0]
                          ) / (quart_lims[1] - quart_lims[0])
            # if color_frac <= 1:
            c = matplotlib.cm.get_cmap(cmap)(color_frac)
            # else:
            #     c='k'

            plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), y_cum[i],
                         c=c, ls=linestyle, linewidth=2, zorder=10, path_effects=path_effect)

            if bootstrap:
                stat = lambda data: np.histogram(data, bins=[quartiles[i+1], quartiles[-1]])[0] / len(data)

                boots = np.zeros((len(xedges)-1, 3))
                for j in range(len(xedges)-1):
                    boots[j] = my_bootstrap(y1[bin_is == j + 1], statistic=stat, n=100)

                hatches = {'-': 'xxx', '--': '+++', '-.': r'\\\\ ', (0, (0.8, 0.8)): '...'}

                # plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 1],
                #              c=colour, ls=linestyle, linewidth=2, zorder=9.7, path_effects=path_effect)
                plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 0], boots[:, 2],
                                 color=c, alpha=0.3, edgecolor='k', zorder=9.3, hatch=hatches[linestyle])

        return None, None, None, None, None

    for s, N_res in enumerate(N_ress):
        N_mask = N0 > N_res

        not_crazy_mask = np.logical_and(np.logical_and(np.isfinite(x0), np.isfinite(y0)), N_mask)

        if np.sum(not_crazy_mask) == 0:
            print('Masked out all values.')
            continue

        ymedians, xedges, bin_is = binned_statistic(x0[not_crazy_mask], y0[not_crazy_mask],
                                                    statistic=np.nanmedian, bins=N_run)
        yplus, xedges, _ = binned_statistic(x0[not_crazy_mask], y0[not_crazy_mask],
                                            statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
        yminus, xedges, _ = binned_statistic(x0[not_crazy_mask], y0[not_crazy_mask],
                                             statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        xcentres = 0.5 * (xedges[1:] + xedges[:-1])

        # #
        # n_per_bin, _ = np.histogram(x0, bins=N_run)
        # print(colour)
        # print(n_per_bin)

        if bootstrap and s < 1:
            boots = np.zeros((len(xedges) - 1, 3))
            for i in range(len(xedges) - 1):
                boots[i] = my_bootstrap(y0[not_crazy_mask][bin_is == i + 1], statistic=np.nanmedian)

            hatches = {'-': 'xxx', '--': '+++', '-.': r'\\\\ ', (0, (0.8, 0.8)): '...'}

            # plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 1],
            #              c=colour, ls=linestyle, linewidth=2, zorder=9.7, path_effects=path_effect)
            plt.fill_between(xcentres, boots[:, 0], boots[:, 2],
                             color=colour, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle])

        else:
            boots = None

        if c0key is None:
            plt.scatter(x0[not_crazy_mask], y0[not_crazy_mask],
                        color=colour, marker=marker, s=3-2*s, alpha=1-0.4*2*s)

            if linestyle is not None:
                path_effect = [path_effects.Stroke(linewidth=4-s, foreground='black', alpha=1-0.4*s),
                               path_effects.Normal()]
                plt.errorbar(xcentres, ymedians,
                             c=colour, ls=linestyle, linewidth=2-s/2, zorder=10-s, alpha=1-0.2*s,
                             path_effects=path_effect)

                if s == 0 and shade:
                    plt.fill_between(xcentres, yminus, yplus,
                                     color=colour, alpha=0.5, edgecolor='k', zorder=9)

        else:
            plt.errorbar(xcentres, ymedians,
                         c=colour, ls=linestyle, linewidth=2-s/2, zorder=11)
            if s == 0 and shade:
                plt.errorbar(xcentres, yminus,
                             c=colour, ls=linestyle, linewidth=2-s/2, zorder=11)
                plt.errorbar(xcentres, yplus,
                             c=colour, ls=linestyle, linewidth=2-s/2, zorder=11)

            if s == len(N_ress) -1:
                c0 = return_array_from_keys(raw_data0, c0key, rkey, comp, mask0, ii)

                if len(c0[not_crazy_mask]) > 2:

                    plt.scatter(x0[not_crazy_mask], y0[not_crazy_mask], c=c0[not_crazy_mask],
                                marker=marker, s=6, alpha=1,
                                cmap=cmap, vmin=clims[0], vmax=clims[1], zorder=9)

                    # #TODO work out why this breaks
                    # plt.hexbin(x0[not_crazy_mask], y0[not_crazy_mask], C=c0[not_crazy_mask],
                    #            cmap=cmap, vmin=clims[0], vmax=clims[1],
                    #            reduce_C_function=np.nanmedian, mincnt=0, edgecolors='none',
                    #            gridsize=(int((np.amax(x0[not_crazy_mask]) - np.amin(x0[not_crazy_mask])) /
                    #                          (xlims[1] - xlims[0]) * len(N_run)*2),
                    #                      int((np.amax(y0[not_crazy_mask]) - np.amin(y0[not_crazy_mask])) /
                    #                          (ylims[1] - ylims[0]) * len(N_run)*2 / np.sqrt(3))))

    return #xcentres, ymedians, yminus, yplus, boots


def plot_x_vs_y_binned(raw_data0,
                       xkey, ykey, rkey, comp, binkey, mask0,
                       N_run=NRUN,
                       colour='C0', linestyle='-', marker='.', cmap='turbo',
                       bootstrap=True, shade=True, N_ress=None, nan_to_x=np.nan):
    if np.sum(mask0) < 1:
        print('Masked all values')
        return None, None, None, None, None

    xlims = lims_dict[xkey]
    ylims = lims_dict[ykey]

    if xkey == 'age':
        xcentres = np.linspace(1,13,7)

    if N_ress == None:
        N_ress = [20, 1]
        # N_ress = [1000, 100, 1]
        # N_ress = [1]

    for s, N_res in enumerate(N_ress):
        ymedians = np.zeros(n_star_extra//2)
        ypluss = np.zeros(n_star_extra//2)
        yminuss = np.zeros(n_star_extra//2)

        boots = np.zeros((n_star_extra//2, 3))

        for ii in range(n_star_extra//2):

            x0 = return_array_from_keys(raw_data0, xkey, rkey, comp, mask0, ii)
            y0 = return_array_from_keys(raw_data0, ykey, rkey, comp, mask0, ii)

            N0 = return_array_from_keys(raw_data0, 'Npart', rkey, comp, mask0, ii)
            # N0 = return_array_from_keys(raw_data0, 'Npart', 'r200', comp, mask0, ii)

            if nan_to_x is not None:
                x0[~np.isfinite(y0)] = nan_to_x
                y0[~np.isfinite(y0)] = nan_to_x

            N_mask = N0 > N_res
            not_crazy_mask = np.logical_and(np.logical_and(np.isfinite(x0), np.isfinite(y0)), N_mask)

            if np.sum(not_crazy_mask) == 0:
                # print('Masked out all values.')
                # continue
                pass

            ymedians[ii] = np.nanmedian(y0[not_crazy_mask])
            ypluss[ii] = np.nanquantile(y0[not_crazy_mask], 0.16)
            yminuss[ii] = np.nanquantile(y0[not_crazy_mask], 0.84)

            if bootstrap and s < 1:
                boots[ii] = my_bootstrap(y0[not_crazy_mask], statistic=np.nanmedian)

            plt.scatter(x0[not_crazy_mask], y0[not_crazy_mask],
                        color=colour, marker=marker, s=3 - 2*s, alpha=1 - 0.4 * 2*s)

        if bootstrap and shade and s < 1:
            hatches = {'-': 'xxx', '--': '+++', '-.': r'\\\\ ', (0, (0.8, 0.8)): '...'}

            # plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 1],
            #              c=colour, ls=linestyle, linewidth=2, zorder=9.7, path_effects=path_effect)

            if True: #np.sum(np.isfinite(np.log10(ymedians))) > len(xcentres) * 3 / 4:
                plt.fill_between(xcentres, boots[:, 0], boots[:, 2],
                                 color=colour, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle])

        if linestyle is not None:
            path_effect = [path_effects.Stroke(linewidth=4 - s, foreground='black', alpha=1 - 0.4 * s),
                           path_effects.Normal()]

            if True: #np.sum(np.isfinite(np.log10(ymedians))) > len(xcentres) * 3 / 4:
                plt.errorbar(xcentres, ymedians,
                             c=colour, ls=linestyle, linewidth=2 - s / 2, zorder=10 - s, alpha=1 - 0.2 * s,
                             path_effects=path_effect)

                if s == 0 and shade:
                    plt.fill_between(xcentres, yminuss, ypluss,
                                     color=colour, alpha=0.5, edgecolor='k', zorder=9)

        # if ?

    return


def plot_r_vs_y(raw_data0,
                radial_bin_mask, radial_bin_centres,
                ykey, comp, mask0,
                c0key=None, c0column=None, clims=None,
                colour='C0', linestyle='-', marker='.', cmap='turbo',
                show_all=False, shade=True, N_ress=None, nan_to_x=-10):

    ylims = lims_dict[ykey]

    if clims == None:
        clims = [None, None]

    if N_ress == None:
        N_ress = [1000, 100, 1]

    if np.sum(mask0) < 1:
        print('Masked all profiles')
        return

    if 'cum_' in ykey:
        #cumulative
        #meant for jz and Jz
        radial_y0 = 10**return_arrays_from_keys(raw_data0, ykey[4:], comp, radial_bin_mask, mask0)

        if ykey[4:] in ['jz']:

            if comp == 'Star':
                m0 = 10**return_arrays_from_keys(raw_data0, 'mstarr', comp, radial_bin_mask, mask0) #Msun
            elif comp == 'Gas':
                m0 = 10**return_arrays_from_keys(raw_data0, 'mgasr', comp, radial_bin_mask[:n_dim], mask0) #Msun
            elif comp == 'DM':
                m0 = 10**return_arrays_from_keys(raw_data0, 'mdmr', comp, radial_bin_mask[:n_dim], mask0) #Msun

            y0 = np.log10(np.nancumsum(m0 * radial_y0, axis=1) / np.nancumsum(m0, axis=1))

        elif ykey[4:] in ['ek', 'DMek']:

            if comp == 'Star':
                N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask, mask0) #Msun
            elif comp == 'Gas':
                N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask[:n_dim], mask0) #Msun
            elif comp == 'DM':
                N0 = return_arrays_from_keys(raw_data0, 'ndm', comp, radial_bin_mask[:n_dim], mask0) #Msun

            y0 = np.log10(np.nancumsum(N0 * radial_y0, axis=1))

        else:
            y0 = np.log10(np.nancumsum(radial_y0, axis=1))

    elif ykey in ['mean_v_phi_v_circ', 'mean_v_R_v_circ']:
        mstar = 10**return_arrays_from_keys(raw_data0, 'mstarr', comp, radial_bin_mask, mask0) # Msun
        mgas = 10**return_arrays_from_keys(raw_data0, 'mgasr', comp, radial_bin_mask[:n_dim], mask0) # Msun
        mdm = 10**return_arrays_from_keys(raw_data0, 'mdmr', comp, radial_bin_mask[:n_dim], mask0) # Msun

        mstar[~np.isfinite(mstar)] = 0
        mgas[~np.isfinite(mgas)] = 0
        mdm[~np.isfinite(mdm)] = 0

        x0 = np.sqrt(GRAV_CONST * np.cumsum(mstar + mgas + mdm, axis=1) / 10**10 / 10**radial_bin_centres)

        y0 = 10**return_arrays_from_keys(raw_data0, ykey[:-7], comp, radial_bin_mask, mask0)

        y0 = transform_dict['mean_v_phi_v_circ'](y0, x0)

    elif ykey in ['v_circ', 'DMv_circ']:
        if comp == 'Star':
            m0 = return_arrays_from_keys(raw_data0, 'mstarr', comp, radial_bin_mask, mask0)
            # m0[~np.isfinite(m0)] = 0
            N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask, mask0)
            y0 = transform_dict['v_circ'](
                np.sqrt(GRAV_CONST * np.cumsum(10**m0, axis=1) / 10**10 / 10**radial_bin_centres))
        elif comp == 'Gas':
            m0 = return_arrays_from_keys(raw_data0, 'mgasr', comp, radial_bin_mask[:n_dim], mask0)
            # m0[~np.isfinite(m0)] = 0
            N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask[:n_dim], mask0)
            y0 = transform_dict['v_circ'](
                np.sqrt(GRAV_CONST * np.cumsum(10**m0, axis=1) / 10**10 / 10**radial_bin_centres))
        elif comp == 'DM':
            m0 = return_arrays_from_keys(raw_data0, 'mdmr', comp, radial_bin_mask[:n_dim], mask0)
            # m0[~np.isfinite(m0)] = 0
            N0 = return_arrays_from_keys(raw_data0, 'ndm', comp, radial_bin_mask[:n_dim], mask0)
            y0 = transform_dict['v_circ'](
                np.sqrt(GRAV_CONST * np.cumsum(10**m0, axis=1) / 10**10 / 10**radial_bin_centres))

        elif comp == 'ALL':
            mstar = 10**return_arrays_from_keys(raw_data0, 'mstarr', 'Star', radial_bin_mask, mask0) # Msun
            mgas = 10**return_arrays_from_keys(raw_data0, 'mgasr', 'Gas', radial_bin_mask[:n_dim], mask0) # Msun
            mdm = 10**return_arrays_from_keys(raw_data0, 'mdmr', 'DM', radial_bin_mask[:n_dim], mask0) # Msun

            mstar[~np.isfinite(mstar)] = 0
            mgas[~np.isfinite(mgas)] = 0
            mdm[~np.isfinite(mdm)] = 0

            m0 = mstar + mgas + mdm
            m0 = np.cumsum(m0, axis=1)

            Nstar = return_arrays_from_keys(raw_data0, 'Npart', 'Star', radial_bin_mask, mask0)
            Ngas = return_arrays_from_keys(raw_data0, 'Npart', 'Gas', radial_bin_mask[:n_dim], mask0)
            Ndm = return_arrays_from_keys(raw_data0, 'ndm', 'DM', radial_bin_mask[:n_dim], mask0)
            N0 = Nstar + Ngas + Ndm

            y0 = transform_dict['v_circ'](
                np.sqrt(GRAV_CONST * m0 / 10**10 / 10**radial_bin_centres))

        N0 = np.cumsum(N0, axis=1)

        y0[~np.isfinite(y0)] = nan_to_x
        y0[m0 == 0] = -10 #np.nan

    elif ykey in ['mstarr', 'mgasr', 'mdmr']:
        if comp == 'Star':
            m0 = return_arrays_from_keys(raw_data0, ykey, comp, radial_bin_mask, mask0)
            N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask, mask0)
            y0 = m0
        elif comp == 'Gas':
            m0 = return_arrays_from_keys(raw_data0, ykey, comp, radial_bin_mask[:n_dim], mask0)
            N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask[:n_dim], mask0)
            y0 = m0
        elif comp == 'DM':
            m0 = return_arrays_from_keys(raw_data0, ykey, comp, radial_bin_mask[:n_dim], mask0)
            N0 = return_arrays_from_keys(raw_data0, 'ndm', comp, radial_bin_mask[:n_dim], mask0)
            y0 = m0

        elif comp == 'ALL':
            mstar = 10 ** return_arrays_from_keys(raw_data0, 'mstarr', 'Star', radial_bin_mask, mask0)  # Msun
            mgas = 10 ** return_arrays_from_keys(raw_data0, 'mgasr', 'Gas', radial_bin_mask[:n_dim], mask0)  # Msun
            mdm = 10 ** return_arrays_from_keys(raw_data0, 'mdmr', 'DM', radial_bin_mask[:n_dim], mask0)  # Msun

            mstar[~np.isfinite(mstar)] = 0
            mgas[~np.isfinite(mgas)] = 0
            mdm[~np.isfinite(mdm)] = 0

            m0 = mstar + mgas + mdm
            y0 = np.log10(m0)

            Nstar = return_arrays_from_keys(raw_data0, 'Npart', 'Star', radial_bin_mask, mask0)
            Ngas = return_arrays_from_keys(raw_data0, 'Npart', 'Gas', radial_bin_mask[:n_dim], mask0)
            Ndm = return_arrays_from_keys(raw_data0, 'ndm', 'DM', radial_bin_mask[:n_dim], mask0)
            N0 = Nstar + Ngas + Ndm

        N0 = np.cumsum(N0, axis=1)

        y0[~np.isfinite(y0)] = nan_to_x
        y0[m0 == 0] = -10  # np.nan

    elif ykey in ['st', 'dt']:
        y0 = return_array_from_keys(raw_data0, ykey, comp, radial_bin_mask, mask0)

    else:
        y0 = return_arrays_from_keys(raw_data0, ykey, comp, radial_bin_mask, mask0)

    if ykey in ['v_circ', 'DMv_circ', 'mstarr', 'mgasr', 'mdmr']:
        pass

    elif nan_to_x is not None:
        if comp == 'Star':
            m0 = 10 ** return_arrays_from_keys(raw_data0, 'mstarr', comp, radial_bin_mask, mask0)
            N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask, mask0)
        elif comp == 'Gas':
            m0 = 10 ** return_arrays_from_keys(raw_data0, 'mgasr', comp, radial_bin_mask[:n_dim], mask0)
            N0 = return_arrays_from_keys(raw_data0, 'Npart', comp, radial_bin_mask[:n_dim], mask0)
        elif comp == 'DM':
            m0 = 10 ** return_arrays_from_keys(raw_data0, 'mdmr', comp, radial_bin_mask[:n_dim], mask0)
            N0 = return_arrays_from_keys(raw_data0, 'ndm', comp, radial_bin_mask[:n_dim], mask0)

        elif comp == 'ALL':
            mstar = 10**return_arrays_from_keys(raw_data0, 'mstarr', 'Star', radial_bin_mask, mask0)
            mgas = 10**return_arrays_from_keys(raw_data0, 'mgasr', 'Gas', radial_bin_mask[:n_dim], mask0)
            mdm = 10**return_arrays_from_keys(raw_data0, 'mdmr', 'DM', radial_bin_mask[:n_dim], mask0)
            m0 = mstar + mgas + mdm

            Nstar = return_arrays_from_keys(raw_data0, 'Npart', 'Star', radial_bin_mask, mask0)
            Ngas = return_arrays_from_keys(raw_data0, 'Npart', 'Gas', radial_bin_mask[:n_dim], mask0)
            Ndm = return_arrays_from_keys(raw_data0, 'ndm', 'DM', radial_bin_mask[:n_dim], mask0)
            N0 = Nstar + Ngas + Ndm

        y0[~np.isfinite(y0)] = nan_to_x
        y0[m0 == 0] = np.nan

    else:
        N0 = return_array_from_keys(raw_data0, 'Npart', 'r200', 'Star', mask0)

    # y0 = np.log10(y0)

    jn = len(radial_bin_mask) #np.sum(radial_bin_mask)
    ymedians = np.zeros(jn)
    yplus  = np.zeros(jn)
    yminus = np.zeros(jn)

    for s, N_res in enumerate(N_ress):
        N_mask = N0 > N_res

        #no particles measured is actually meaningful for v_circ
        if ykey in ['v_circ', 'DMv_circ', 'mstar', 'mgasr', 'mdmr']:
            N_mask = N0 > -1

        for j in range(jn):
            ys = y0[:, j][N_mask[:, j]] #y0[N_mask, j]
            ymedians[j] = np.nanmedian(ys)
            # ymedians[j] = np.nanmean(ys)
            yplus[j]  = np.nanquantile(ys, 0.84)
            yminus[j] = np.nanquantile(ys, 0.16)

        # print('Warning: quartile range not 16-84.')

        ymedians[ymedians < -1] = np.nan
        yplus[np.isnan(ymedians)] = np.nan
        yminus[np.isnan(ymedians)] = np.nan

        yplus[yplus > ymedians + 3] = np.nan
        yminus[yminus < ymedians - 6] = np.nan

        # ymedians = np.log10(ymedians)
        # yplus = np.log10(yplus)
        # yminus = np.log10(yminus)

        if show_all and s == 0: #np.sum(mask0) < 100 or
            for i in range(np.sum(mask0)):

                plt.errorbar(radial_bin_centres, y0[i],
                             color=colour, linestyle=linestyle, linewidth=1, alpha=0.2)

        path_effect = [path_effects.Stroke(linewidth=4-s, foreground='black', alpha=1-0.4*s),
                       path_effects.Normal()]
        plt.errorbar(radial_bin_centres, ymedians,
                     c=colour, ls=linestyle, linewidth=2-s/2, zorder=10-s, alpha=1-0.2*s,
                     path_effects=path_effect)

        if s == 0 and shade:
            plt.fill_between(radial_bin_centres, yminus, yplus,
                             color=colour, alpha=0.3, edgecolor='k', zorder=9)

    return


def my_nanquantile(*args, **kwargs):
    if len(args[0]) == 0:
        return np.nan
    return np.nanquantile(*arg, **kwargs)


def plot_r_vs_y_percentiles(raw_data0,
                            radial_bin_mask, radial_bin_centres,
                            ykey, comp, mask0,
                            c0key=None, c0column=None, clims=None, percentiles=None,
                            colour='C0', linestyle='-', marker='.', cmap='viridis',
                            show_all=False, shade=True, N_ress = None, nan_to_x=-10):

    ylims = lims_dict[ykey]

    if clims == None:
        clims = [None, None]

    if N_ress == None:
        # N_ress = [1000, 100, 1]
        N_ress = [1]

    if percentiles == None:
        # percentiles = np.linspace(0, 1, 11)
        percentiles = np.linspace(0, 1, 6)

    if np.sum(mask0) < 1:
        print('Masked all profiles')
        return

    if ykey not in ['v_circ', 'DMv_circ']:
        y0 = return_arrays_from_keys(raw_data0, ykey, comp, radial_bin_mask, mask0)

    else:
        if comp == 'Star':
            m0 = return_arrays_from_keys(raw_data0, 'mstarr', comp, radial_bin_mask, mask0)
        elif comp == 'Gas':
            m0 = return_arrays_from_keys(raw_data0, 'mgasr', comp, radial_bin_mask, mask0)
        elif comp == 'DM':
            m0 = return_arrays_from_keys(raw_data0, 'mdmr', comp, radial_bin_mask, mask0)

        y0 = np.sqrt(GRAV_CONST * np.cumsum(10**m0, axis=0) / 10**10 / 10**radial_bin_centres)
        y0 = transform_dict['v_circ'](y0)

    if nan_to_x is not None:
        x0[~np.isfinite(y0)] = nan_to_x
        y0[~np.isfinite(y0)] = nan_to_x

    N0 = return_array_from_keys(raw_data0, 'Npart', 'r200', comp, mask0)
    c0 = return_array_from_keys(raw_data0, c0key, c0column, comp, mask0)

    if nan_to_x is not None:
        x0[~np.isfinite(y0)] = nan_to_x
        y0[~np.isfinite(y0)] = nan_to_x

    jn = np.sum(radial_bin_mask)
    ypctiles = np.zeros((jn, len(percentiles)-1))

    for s, N_res in enumerate(N_ress):
        N_mask = N0 > N_res #np.ones(len(N0), dtype=bool)

        # TODO finish this...
        # order = np.argsort(np.amax(y0[N_mask], axis=0))

        if c0key != None:
            order = np.argsort(np.argsort(c0[N_mask])) / np.sum(N_mask)
            pct_bin = np.digitize(order, percentiles)

        for k, pct in enumerate((percentiles[1:] + percentiles[:-1]) /2):

            for j in range(jn):

                if c0key != None:
                    ypctiles[j, k] = np.nanmedian(y0[N_mask, j][pct_bin == k+1])

                else:
                    #TODO this is probably not the correct thing todo.. should be based on order of vmax of profiles ...
                    ypctiles[j, k] = np.nanquantile(y0[N_mask, j], pct)
                    # ypctiles[j, pct] = my_nanquantile(y0[N_mask, j], pct)

        # TODO color each curve by order
        # if show_all and s == 0:
        #     for i in range(np.sum(mask0)):
        #
        #         plt.errorbar(radial_bin_centres, y0[i],
        #                      color=colour, linestyle=linestyle, linewidth=1, alpha=0.2)

        path_effect = [path_effects.Stroke(linewidth=4-s, foreground='black', alpha=1-0.4*s),
                       path_effects.Normal()]
        for k, pct in enumerate((percentiles[1:] + percentiles[:-1]) /2):
            plt.errorbar(radial_bin_centres, ypctiles[:, k],
                         c=matplotlib.cm.get_cmap(cmap)(pct), ls=linestyle, linewidth=2-s/2, zorder=8-s, alpha=1-0.2*s,
                         path_effects=path_effect)

            # plt.errorbar(np.ones(np.sum(pct_bin == k+1)), c0[N_mask][pct_bin == k+1],
            #              c=matplotlib.cm.get_cmap(cmap)(pct), marker='.', ls='')

        # if shade:
        #     plt.fill_between(radial_bin_centres, yminus, yplus,
        #                      color=colour, alpha=0.3, edgecolor='k', zorder=9)

    return


def btf(b):
    try:
        return np.float64(b[1:])
    except ValueError:
        return b



def plot_data_x_vs_y(raw_data0, raw_data1, raw_data2, raw_data3,
                     xkey, ykey, rkey, comp, binkey,
                     cent_sat_split,
                     cent_only, four_z,
                     snap_str, sim_str, binbins, ii, ii_colour=None, legend=True):

    #binbins should be length 2
    mask1key = binkey
    mask1column = rkey

    mask1transform = lambda data: np.logical_and(binbins[ii] < data, data <= binbins[ii + 1])

    if ii_colour == None:
        C0 = 'C0'
        C1 = 'C1'
        C2 = 'C2'
        C3 = 'C3'
        k = 'k'
    else:
        C0 = ii_colour
        C1 = ii_colour
        C2 = ii_colour
        C3 = ii_colour
        k = ii_colour

    if cent_sat_split:
        if not cent_only:

            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: mask != 0

            mask = lambda raw_data: return_full_mask(raw_data,
                                                     mask0key, mask0column, comp, mask0transform,
                                                     mask1key, mask1column, comp, mask1transform,
                                                     mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                     mask_func_dict[ykey], ii)

            #satellites
            plot_x_vs_y(raw_data0,
                        xkey, ykey, rkey, comp, ii, mask(raw_data0),
                        colour=C2, linestyle=(0,(0.8,0.8)), marker='.',
                        quartiles=quartile_dict[ykey], cmap='viridis')
            plot_x_vs_y(raw_data1,
                        xkey, ykey, rkey, comp, ii, mask(raw_data1),
                        colour=C3, linestyle='-.', marker='.',
                        quartiles=quartile_dict[ykey], cmap='viridis')

        mask0key = 'sn'
        mask0column = None
        mask0transform = lambda mask: mask == 0

        mask = lambda raw_data : return_full_mask(raw_data,
                                                  mask0key, mask0column, comp, mask0transform,
                                                  mask1key, mask1column, comp, mask1transform,
                                                  mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                  mask_func_dict[ykey], ii)

        #centrals
        plot_x_vs_y(raw_data0,
                    xkey, ykey, rkey, comp, ii, mask(raw_data0),
                    colour=C0, linestyle='-', marker='.',
                    quartiles=quartile_dict[ykey], cmap='viridis')
        plot_x_vs_y(raw_data1,
                    xkey, ykey, rkey, comp, ii, mask(raw_data1),
                    colour=C1, linestyle='--', marker='.',
                    quartiles=quartile_dict[ykey], cmap='viridis')

        title = 'z=' + snap_str[7:].replace('p', '.')
        if len(binbins) > 2:
            title += '\n' + label_dict[binkey] + ': ' + str(round(binbins[ii], 1)) + ' - ' + str(round(binbins[ii+1], 1))
        if cent_only:
            if 'quart' in ykey:
                extra_lines = []
                extra_labels = []

                quart_lims = [np.amin(quartile_dict[ykey][1:-1]), np.amax(quartile_dict[ykey][1:-1])]

                for i in range(len(quartile_dict[ykey]) -2):

                    color_frac = (quartile_dict[ykey][i+1] - quart_lims[0]) / (quart_lims[1] - quart_lims[0])

                    c = matplotlib.cm.get_cmap('viridis')(color_frac)
                    extra_lines.append(lines.Line2D([0, 1], [0, 1], color=c, ls='-', linewidth=3))
                    extra_labels.append(r'$\kappa_{\rm co} > $' + f'{quartile_dict[ykey][i+1]}')

                if legend:
                    l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),
                                    (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                                    *extra_lines],
                                   [r'$1\times$DM Centrals',
                                    r'$7\times$DM Centrals',
                                    *extra_labels],
                                   title=title, ncol=1,
                                   loc='upper left')

            else:
                if legend:
                    l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                                    (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)), ],
                                   [r'$1\times$DM Centrals',
                                    r'$7\times$DM Centrals'],
                                   title=title, ncol=1, )

        else:
            if legend:
                l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C3', ls='-.', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C2', ls=(0,(0.8,0.8)), linewidth=3))],
                               [r'$1\times$DM Centrals', r'$1\times$DM Satellites',
                                r'$7\times$DM Centrals', r'$7\times$DM Satellites'],
                               title=title, ncol=1,)
        if legend:
            l.set_zorder(-10)
            l.get_title().set_multialignment('center')

    elif four_z:

        if cent_only:
            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: mask == 0
        else:
            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: True

        mask = lambda raw_data : return_full_mask(raw_data,
                                                  mask0key, mask0column, comp, mask0transform,
                                                  mask1key, mask1column, comp, mask1transform,
                                                  mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                  mask_func_dict[ykey], ii)

        plot_x_vs_y(raw_data3,
                    xkey, ykey, rkey, comp, ii, mask(raw_data3),
                    colour=matplotlib.cm.get_cmap('inferno')(0.5/4), linestyle=(0,(0.8,0.8)), marker='.',
                    quartiles=quartile_dict[ykey])
        plot_x_vs_y(raw_data2,
                    xkey, ykey, rkey, comp, ii, mask(raw_data2),
                    colour=matplotlib.cm.get_cmap('inferno')(1.5/4), linestyle='--', marker='.',
                    quartiles=quartile_dict[ykey])
        plot_x_vs_y(raw_data1,
                    xkey, ykey, rkey, comp, ii, mask(raw_data1),
                    colour=matplotlib.cm.get_cmap('inferno')(2.5/4), linestyle='-.', marker='.',
                    quartiles=quartile_dict[ykey])
        plot_x_vs_y(raw_data0,
                    xkey, ykey, rkey, comp, ii, mask(raw_data0),
                    colour=matplotlib.cm.get_cmap('inferno')(3.5/4), linestyle='-', marker='.',
                    quartiles=quartile_dict[ykey])

        title = sim_str
        if len(binbins) > 2:
            title += '\n' + label_dict[binkey] + ': ' + str(round(binbins[ii], 2)) + ' - ' + str(round(binbins[ii+1], 2))
        l = plt.legend([(lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(3.5/4), ls='-', linewidth=3)),
                        (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(2.5/4), ls='-.', linewidth=3)),
                        (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(1.5/4), ls='--', linewidth=3)),
                        (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(0.5/4), ls=(0,(0.8,0.8)), linewidth=3))],
                       [r'$z=0$', r'$z=0.5$', r'$z=1$', r'$z=2$'],
                       title=title, ncol=2,)
        l.set_zorder(-10)
        l.get_title().set_multialignment('center')

    else:
        if cent_only:
            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: mask == 0
        else:
            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: np.ones(np.shape(mask), dtype=bool)

        mask = lambda raw_data : return_full_mask(raw_data,
                                                  mask0key, mask0column, comp, mask0transform,
                                                  mask1key, mask1column, comp, mask1transform,
                                                  mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                  mask_func_dict[ykey], ii)

        plot_x_vs_y(raw_data0,
                    xkey, ykey, rkey, comp, ii, mask(raw_data0),
                    # c0key=binkey, c0column=rkey, clims=lims_dict[binkey],
                    colour=k, linestyle='-', marker='.',
                    quartiles=quartile_dict[ykey])

        title = sim_str + ', z=' + snap_str[7:].replace('p', '.')
        if len(binbins) > 2 or ii_colour != None:
            title += '\n' + label_dict[binkey] + ': ' + str(binbins[0]) + ' - ' + str(round(binbins[1], 1))
        l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='k', ls='', marker='.')),],
                       [r'Galaxies'],
                       title=title,)
        l.set_zorder(10)
        l.get_title().set_multialignment('center')

    return


def make_full_figure(raw_data0, raw_data1=None, raw_data2=None, raw_data3=None,
                     xkey='m200', ykey='kappa_rot', rkey='r200', comp='Star', binkey=None,
                     cent_sat_split=True,
                     cent_only=False, four_z=False,
                     snap_str='', sim_str='', save=True, binbins=None, model=True,
                     iter_bins_on_panel=False, bins_on_new_panel=True, legend=True):

    if binbins is None:
        binbins = [0, UNIVERSE_AGE]

    if rkey in ['eq_rh_profile', 'r200_profile'] and xkey == 'age':
        iter_bins_on_panel = True

    # #check if radius does nothing
    # if ((column0_dict[xkey][rkey, 0] == column0_dict[xkey]['test', 0]) and
    #     (column1_dict[xkey][rkey, 0] == column1_dict[xkey]['test', 0]) and
    #     (column2_dict[xkey][rkey, 0] == column2_dict[xkey]['test', 0]) and
    #     (column0_dict[ykey][rkey, 0] == column0_dict[ykey]['test', 0]) and
    #     (column1_dict[ykey][rkey, 0] == column1_dict[ykey]['test', 0]) and
    #     (column2_dict[ykey][rkey, 0] == column2_dict[ykey]['test', 0])):
    #     if rkey != 'r200':
    #         return

    if model:
        # models
        if binkey in ['m200', 'mtot']:
            logM200s = (np.array(binbins)[1:] + np.array(binbins)[:-1]) / 2 - 10
        else:
            logM200s = np.linspace(8, 14, 25) - 10  # 10^10 M_sun

        if xkey == 'age':
            time_edges = np.linspace(*lims_dict[xkey], 13)
            times = (time_edges[:-1] + time_edges[1:]) / 2
            # time_edges = np.array(binbins)
            # times = (time_edges[:-1] + time_edges[1:]) / 2
        # elif len(binbins) <= 2:
            # times = np.array([0, 0.5, 1, 2, 4, 6, 8, 10, 12, UNIVERSE_AGE - 1e-3])
        elif 'eq_rh_profile' in rkey:
            if   rkey[-1] == '0': times = np.array([1])
            elif rkey[-1] == '1': times = np.array([3])
            elif rkey[-1] == '2': times = np.array([5])
            elif rkey[-1] == '3': times = np.array([7])
            elif rkey[-1] == '4': times = np.array([9])
            elif rkey[-1] == '5': times = np.array([11])
            elif rkey[-1] == '6': times = np.array([13])
            else:
                times = np.array([1, 3, 5, 7, 9, 11, 13])
        else:
            times = np.array([8])
        # else:
        #     times = np.array([binbins[ii], (binbins[ii] + binbins[ii + 1]) / 2, binbins[ii + 1]])

        z = float(snap_str[7:].replace('p', '.'))
        a = 1 / (1 + z)
        snap_time = lbt(np.array([a]))[0]

        concs = concentration_ludlow(z, 10 ** logM200s)

        # cosmology
        omm = 0.307
        oml = 0.693

        H_z2 = HUBBLE_CONST ** 2 * (oml + omm * np.power(a, -3))
        rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3

        g = 1  # 1 / (np.log1p(concs) - concs / (1 + concs)) #* concs**3 / 3
        rho_200 = 200 * rho_crit

        r200s = np.power(10 ** logM200s * 3 / (4 * np.pi * rho_200 * g), 1 / 3) #kpc
        rs = r200s / concs

        v200s = np.sqrt(GRAV_CONST * 10 ** logM200s / r200s) #km / s

        mdm = DM_MASS #1 / raw_data0['Header/MDMReciprocal'][()][0]

        # nfw_vs_M200 = [[get_heating_at_r12(10 ** logM200, MassDM=mdm,
        #                                    z=z, time=time - snap_time, ic_heat_fraction=0, contracted=True)
        #                 for time in times] for logM200 in logM200s]
        # nfw_vs_M200 = np.array(nfw_vs_M200)

        # analytic_v_circ, analytic_dispersion,
        # theory_best_v, theory_best_z, theory_best_r, theory_best_p
        # [logM200s, times, quantity]

        measured_R = rkey in ['eq_r12', 'eq_R12']

        nfw_vs_M200_raw = [[get_heating_at_r12(10 ** logM200, MassDM=mdm,
                                               z=z, time=time - snap_time, ic_heat_fraction=0,
                                               contracted=True)
                            for time in times] for logM200 in logM200s]
        nfw_vs_M200_raw = np.array(nfw_vs_M200_raw)

        if raw_data1 is not None:
            mdm1 = DM_MASS / 7 #1 / raw_data1['Header/MDMReciprocal'][()][0]

            # nfw_vs_M2001 = [[get_heating_at_r12(10 ** logM200, MassDM=mdm1,
            #                                     z=z, time=time - snap_time, ic_heat_fraction=0)
            #                  for time in times] for logM200 in logM200s]
            # nfw_vs_M2001 = np.array(nfw_vs_M2001)

            nfw_vs_M200_1_raw = [[get_heating_at_r12(10 ** logM200, MassDM=mdm1,
                                                     z=z, time=time - snap_time, ic_heat_fraction=0,
                                                     contracted=True)
                             for time in times] for logM200 in logM200s]
            nfw_vs_M200_1_raw = np.array(nfw_vs_M200_1_raw)

    #plotting starts here
    if bins_on_new_panel:
        fig = plt.figure(figsize=(5.3 * min(len(binbins) - 1, 4), 5.3 * ((len(binbins) + 2) // 4)), constrained_layout=True)
    else:
        fig = plt.figure(figsize=(5.3, 5.3), constrained_layout=True)
    fig.set_constrained_layout_pads(w_pad=0, h_pad=0, hspace=0, wspace=0)

    warnings.filterwarnings("ignore")

    for ii in range(len(binbins)-1):
        if bins_on_new_panel:
            ax = plt.subplot((len(binbins) + 2) // 4, min(len(binbins) - 1, 4), ii+1)
        else:
            ax = plt.subplot(111)

        ax.set_aspect(np.diff(lims_dict[xkey]) / np.diff(lims_dict[ykey]))

        plt.subplots_adjust(wspace=0, hspace=0)

        if iter_bins_on_panel:

            #TODO make this a function

            if bins_on_new_panel:
                ii_colour = ['C0', 'C1', 'C2', 'C3']

            else:
                ii_colour = []
                ii_colour.append(matplotlib.cm.get_cmap('inferno')(
                    (1 - ((binbins[1:] + binbins[:-1]) / 2 - binbins[0]) / (binbins[-1] - binbins[0]))[ii]))
                ii_colour *= 4

            mask1key = binkey
            mask1column = rkey
            mask1transform = lambda data: np.logical_and(binbins[ii] < data, data <= binbins[ii + 1])

            if cent_only:
                mask0key = 'sn'
                mask0column = None
                mask0transform = lambda mask: mask == 0
            else:
                mask0key = 'sn'
                mask0column = None
                mask0transform = lambda mask: True

            mask = lambda raw_data: return_full_mask(raw_data,
                                                     mask0key, mask0column, comp, mask0transform,
                                                     mask1key, mask1column, comp, mask1transform,
                                                     mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                     mask_func_dict[ykey], ii)

            if cent_sat_split:
                plot_x_vs_y_binned(raw_data0,
                                   xkey, ykey, rkey, comp, binkey, mask(raw_data0),
                                   linestyle='-', colour=ii_colour[0], cmap='inferno', shade=True, bootstrap=True)

                plot_x_vs_y_binned(raw_data1,
                                   xkey, ykey, rkey, comp, binkey, mask(raw_data1),
                                   linestyle='--', colour=ii_colour[1], cmap='inferno', shade=True, bootstrap=True)

                title = 'z=' + snap_str[7:].replace('p', '.')
                if legend:
                    if len(binbins) > 2:
                        title += '\n' + label_dict[binkey] + ': ' + str(round(binbins[ii], 1)) + ' - ' + str(
                            round(binbins[ii + 1], 1))
                    if cent_only:
                        l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='--', linewidth=3)),
                                        (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)), ],
                                       [r'$1\times$DM Centrals',
                                        r'$7\times$DM Centrals'],
                                       title=title, ncol=1, )

            elif four_z:
                raise NotImplementedError

            else:
                plot_x_vs_y_binned(raw_data0,
                                   xkey, ykey, rkey, comp, binkey, mask(raw_data0),
                                   colour=ii_colour[0], cmap='inferno', shade=True, bootstrap=True)

                if legend:
                    title = sim_str + ', z=' + snap_str[7:].replace('p', '.')
                    if len(binbins) > 2:
                        title += '\n' + label_dict[binkey] + ': ' + str(round(binbins[ii], 1)) + ' - ' + str(
                            round(binbins[ii + 1], 1))
                    l = plt.legend([], [], #[(lines.Line2D([0, 1], [0, 1], color='k', ls='', marker='.')), ],
                                   #[r'Galaxies'],
                                   title=title, )
            if legend:
                l.set_zorder(10)
                l.get_title().set_multialignment('center')

        else:
            if bins_on_new_panel:
                iii = ii
            else:
                iii = 0

            plot_data_x_vs_y(raw_data0, raw_data1, raw_data2, raw_data3,
                             xkey, ykey, rkey, comp, binkey,
                             cent_sat_split,
                             cent_only, four_z,
                             snap_str, sim_str, binbins, iii, legend=legend)

        #models
        if model:
            path_effect = [path_effects.Stroke(linewidth=3, foreground='black'), path_effects.Normal()]

            colours = []
            for i, time in enumerate(times):
                clims = lims_dict['age']
                colours.append(matplotlib.cm.get_cmap('turbo')((time - clims[0]) / (clims[1] - clims[0])))

            if xkey in ['r12', 'R12']:
                if ykey in ['r12', 'R12']:
                    plt.errorbar([-10,10], [-10,10], color='k', ls=':', zorder=30)
                    plt.errorbar([np.log10(3/4) -10, np.log10(3/4) + 10], [-10,10], color='k', ls=':', zorder=30)
                    plt.errorbar([-np.log10(3/4) -10, -np.log10(3/4) + 10], [-10,10], color='k', ls=':', zorder=30)

            if xkey in ['m200', 'mtot']:

                if ykey in ['jz', 'jtot']:
                    for i in range(30):
                        plt.errorbar([6, 14], [-6 + i * 2 / 3, -6 + 16 / 3 + i * 2 / 3], color='grey', ls=':', linewidth=1,
                                     zorder=11)

                if ykey == 'mstar':
                    y_model = lambda args, x: np.log10(2 * np.power(10, x) * args[0] / (
                            np.power(np.power(10, x - args[3]), -args[1]) + np.power(np.power(10, x - args[3]), args[2])))

                    if z == 0:
                        args_7 = [0.01752821, 0.80010439, 0.52942091, 12.14989909]
                        args_1 = [0.01749592, 0.74689666, 0.50345467, 12.19449142]

                    elif z == 0.503:
                        args_7 = [0.01584741, 0.74680244, 0.58655926, 12.18494499]
                        args_1 = [0.01562641, 0.7324231,  0.51210526, 12.15066235]

                    elif z == 1.004:
                        args_7 = [0.01339274, 0.68658911, 0.72261859, 12.22739973]
                        args_1 = [0.01366547, 0.67812074, 0.59578051, 12.1935998 ]

                    elif z == 2.012:
                        args_7 = [1.10271892e-02, 0.565491719, 1.35982600, 12.5377843]
                        args_1 = [1.05770969e-02, 0.538684119, 1.10020089, 12.5274492]

                    plt.errorbar(logM200s + 10, y_model(args_7, logM200s + 10), ls=':', c='k', lw=2, zorder=20)

                elif ykey == 'sigma_tot':
                    for i, time in enumerate(times):
                        if i == ii:
                            if time - snap_time > 0:
                                sigma_tot = np.sqrt(nfw_vs_M200_raw[:,i,3]**2 + nfw_vs_M200_raw[:,i,4]**2 +
                                                    nfw_vs_M200_raw[:,i,5]**2)

                                plt.errorbar(logM200s + 10, np.log10(sigma_tot),
                                             c=colours[i], ls='--', linewidth=2, path_effects=path_effect)

                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_raw[:, 0, 1] * np.sqrt(3)), c='k', ls='-', linewidth=2)

                elif ykey in ['sigma_tot_v200', 'root_ek_v200']:
                    for i, time in enumerate(times):
                        if i == ii:
                            # if time - snap_time > 0:
                            # sigma_tot = np.sqrt(nfw_vs_M200[:,i,3]**2 + nfw_vs_M200[:,i,4]**2 + nfw_vs_M200[:,i,5]**2)

                            sigma_tot = np.sqrt(nfw_vs_M200_raw[:, i, 3] ** 2 + nfw_vs_M200_raw[:, i, 4] ** 2 +
                                                nfw_vs_M200_raw[:, i, 5] ** 2)

                            # plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                            #              c=colours[i], ls='--', linewidth=2, path_effects=path_effect)
                            plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                                         c='C1', ls=(0,(0.8,0.8)), linewidth=2, path_effects=path_effect, zorder=20)

                            sigma_tot = np.sqrt(nfw_vs_M200_1_raw[:, i, 3] ** 2 + nfw_vs_M200_1_raw[:, i, 4] ** 2 +
                                                nfw_vs_M200_1_raw[:, i, 5] ** 2)

                            # plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                            #              c=colours[i], ls='--', linewidth=2, path_effects=path_effect)
                            plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                                         c='C0', ls='-.', linewidth=2, path_effects=path_effect, zorder=20)

                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_raw[:, 0, 1] * np.sqrt(3) / v200s),
                                 c='k', ls='-', linewidth=2)
                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_1_raw[:, 0, 1] * np.sqrt(3) / v200s),
                                 c='k', ls='-', linewidth=2)

                elif ykey in ['mean_v_phi_v200', 'mean_v_R_v200', 'median_v_phi_v200']:
                    for i, time in enumerate(times):
                        if i == ii:
                            # if time - snap_time > 0:
                            # sigma_tot = np.sqrt(nfw_vs_M200[:,i,3]**2 + nfw_vs_M200[:,i,4]**2 + nfw_vs_M200[:,i,5]**2)

                            mean_v_phi = nfw_vs_M200_raw[:, i, 2]

                            # plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                            #              c=colours[i], ls='--', linewidth=2, path_effects=path_effect)
                            plt.errorbar(logM200s + 10, np.log10(mean_v_phi / v200s),
                                         c='C1', ls=(0,(0.8,0.8)), linewidth=2, path_effects=path_effect, zorder=20)

                            if raw_data1 is not None:
                                mean_v_phi = nfw_vs_M200_1_raw[:, i, 2]

                                # plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                                #              c=colours[i], ls='--', linewidth=2, path_effects=path_effect)
                                plt.errorbar(logM200s + 10, np.log10(mean_v_phi / v200s),
                                             c='C0', ls='-.', linewidth=2, path_effects=path_effect, zorder=20)

                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_raw[:, 0, 0] / v200s),
                                 c='k', ls='--', linewidth=2)
                    if raw_data1 is not None:
                        plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_1_raw[:, 0, 0] / v200s),
                                     c='k', ls='-', linewidth=2)

                elif ykey in ['sigma_z_v200', 'sigma_R_v200', 'sigma_phi_v200']:

                    # plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200[:, 0, 1] / v200s),
                    #              c='darkgreen', ls='-.', linewidth=2, zorder=15)

                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_raw[:, 0, 1] / v200s),
                                 c='k', ls='--', linewidth=2, zorder=15)
                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_1_raw[:, 0, 1] / v200s),
                                 c='k', ls='-', linewidth=2, zorder=15)

                    if 'z' in ykey: k=3
                    elif 'R' in ykey: k=4
                    elif 'phi' in ykey: k=5

                    for i, time in enumerate(times):
                        if i == ii:
                            sigma_tot = nfw_vs_M200_raw[:, i, k]
                            plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                                         c='C1', ls=(0, (0.8, 0.8)), linewidth=2, path_effects=path_effect, zorder=20)

                            sigma_tot = nfw_vs_M200_1_raw[:, i, k]
                            plt.errorbar(logM200s + 10, np.log10(sigma_tot / v200s),
                                         c='C0', ls='-.', linewidth=2, path_effects=path_effect, zorder=20)

                elif ykey == 'mean_v_phi' or ykey == 'median_v_phi':
                    for i, time in enumerate(times):
                        if i == ii:
                            if time - snap_time > 0:
                                mean_v_phi = nfw_vs_M200_raw[:,i,2]

                                plt.errorbar(logM200s + 10, np.log10(mean_v_phi),
                                             c=colours[i], ls='--', linewidth=2, path_effects=path_effect)

                    plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_raw[:, 0, 0]), c='k', ls='-', linewidth=2)

                elif ykey == 'vsigma':
                    for i, time in enumerate(times):
                        # mean_v_phi = nfw_vs_M200[:,i,2]
                        # sigma_1d = np.sqrt((nfw_vs_M200[:,i,3]**2 + nfw_vs_M200[:,i,4]**2 + nfw_vs_M200[:,i,5]**2)/3)
                        # v_on_sigma = mean_v_phi / sigma_1d
                        # plt.errorbar(logM200s + 10, np.log10(v_on_sigma),
                        #              c=colours[i], ls='--', linewidth=2, path_effects=path_effect)

                        mean_v_phi = nfw_vs_M200_raw[:,i,2]
                        sigma_1d = np.sqrt((nfw_vs_M200_raw[:,i,3]**2 + nfw_vs_M200_raw[:,i,4]**2 +
                                            nfw_vs_M200_raw[:,i,5]**2)/3)
                        v_on_sigma = mean_v_phi / sigma_1d

                        plt.errorbar(logM200s + 10, np.log10(v_on_sigma),
                                     c='C1', ls=(0,(0.8,0.8)), linewidth=2, path_effects=path_effect, zorder=20)

                        mean_v_phi_1 = nfw_vs_M200_1_raw[:,i,2]
                        sigma_1d_1 = np.sqrt((nfw_vs_M200_1_raw[:,i,3]**2 + nfw_vs_M200_1_raw[:,i,4]**2 +
                                            nfw_vs_M200_1_raw[:,i,5]**2)/3)
                        v_on_sigma_1 = mean_v_phi_1 / sigma_1d_1

                        plt.errorbar(logM200s + 10, np.log10(v_on_sigma_1),
                                     c='C0', ls='-.', linewidth=2, path_effects=path_effect, zorder=20)

                    # plt.errorbar(logM200s + 10, np.log10(nfw_vs_M200_raw[:, 0, 0] / nfw_vs_M200_raw[:, 0, 1]),
                    #              c='k', ls='-', linewidth=2)

                elif ykey == 'mstarm200':
                    for i in [1, 10, 100, 1_000, 10_000, 100_000, 1_000_000]:
                        m200h = 14
                        m200l = 8
                        baryon_mass = 1.81e6
                        plt.errorbar([m200l, m200h], [np.log10(i * baryon_mass / 10**m200l / 1.8),
                                                      np.log10(i * baryon_mass / 10**m200h / 1.8)],
                                     ls=':', c='grey')
                        # plt.errorbar([m200l, m200h], [10 + np.log10(i * DM_MASS/5.362694300518134 / 10**m200l),
                        #                               10 + np.log10(i * DM_MASS/5.362694300518134 / 10**m200h)],
                        #              ls=':', c='grey')

                        # print([m200l, m200h], [10 + np.log10(i * DM_MASS/5.362694300518134 / 10**m200l),
                        #                        10 + np.log10(i * DM_MASS/5.362694300518134 / 10**m200h)])

                elif ykey == 'kappa_rot' or ykey == 'kappa_rot_disk':
                    for i, time in enumerate(times):
                        if i == ii and rkey != 'r200':
                            if time - snap_time > 0:
                                mean_v_phi2 = (nfw_vs_M200_raw[:,i,2])**2
                                sigma_tot2 = (nfw_vs_M200_raw[:,i,3]**2 + nfw_vs_M200_raw[:,i,4]**2 + nfw_vs_M200_raw[:,i,5]**2)
                                kappa_rot = (mean_v_phi2 + nfw_vs_M200_raw[:,i,5]**2) / (mean_v_phi2 + sigma_tot2)

                                plt.errorbar(logM200s + 10, kappa_rot, #np.log10(kappa_rot),
                                     c='C1', ls=(0,(0.8,0.8)), linewidth=2, path_effects=path_effect, zorder=20)

                                mean_v_phi2_1 = (nfw_vs_M200_1_raw[:,i,2])**2
                                sigma_tot2_1 = (nfw_vs_M200_1_raw[:,i,3]**2 + nfw_vs_M200_1_raw[:,i,4]**2 + nfw_vs_M200_1_raw[:,i,5]**2)
                                kappa_rot_1 = (mean_v_phi2_1 + nfw_vs_M200_1_raw[:,i,5]**2) / (mean_v_phi2_1 + sigma_tot2_1)

                                plt.errorbar(logM200s + 10, kappa_rot_1, #np.log10(kappa_rot_1),
                                     c='C0', ls='-.', linewidth=2, path_effects=path_effect, zorder=20)

                    # nfw_kappa_rot = (nfw_vs_M200[:, 0, 0]**2 + nfw_vs_M200[:, 0, 1]**2) / (nfw_vs_M200[:, 0, 0]**2 + 3 * nfw_vs_M200[:, 0, 1]**2)
                    # plt.errorbar(logM200s + 10, np.log10(nfw_kappa_rot), c='k', ls='-', linewidth=2)

                # elif ykey == 'kappa_co' or ykey == 'kappa_co_disk':
                #     for i, time in enumerate(times):
                #         if time - snap_time > 0:
                #             mean_v_phi = nfw_vs_M200[:,i,2]
                #             sigma_phi = nfw_vs_M200[:,i,5]
                #             sigma_tot2 = (nfw_vs_M200[:,i,3]**2 + nfw_vs_M200[:,i,4]**2 + nfw_vs_M200[:,i,5]**2)
                #
                #             kappa_co = np.zeros(len(logM200s))
                #             for j in range(len(logM200s)):
                #                 v2_int = lambda x: x ** 2 * scipy.stats.norm.pdf(x, loc=mean_v_phi[j], scale=sigma_phi[j])
                #                 v2_out = quad(v2_int, 0, 1_000)[0]
                #
                #                 area_out = 1
                #                 rms2_positive_v_phi = v2_out / area_out  # * m_frac_out
                #
                #                 kappa_co[j] = rms2_positive_v_phi / (mean_v_phi[j]**2 + sigma_tot2[j])
                #
                #             plt.errorbar(logM200s + 10, np.log10(kappa_co),
                #                          c=colours[i], ls='--', linewidth=2, path_effects=path_effect)

                elif ykey == 'lambda_r1bin':
                    for i, time in enumerate(times):
                        if i == ii:
                            if time - snap_time > 0:
                                mean_v_phi = nfw_vs_M200[:,i,2]
                                sigma_1d2 = (nfw_vs_M200[:,i,3]**2 + nfw_vs_M200[:,i,4]**2 + nfw_vs_M200[:,i,5]**2)/3
                                lambda_r = mean_v_phi / np.sqrt(mean_v_phi**2 + sigma_1d2)

                                plt.errorbar(logM200s + 10, np.log10(lambda_r),
                                             c=colours[i], ls='--', linewidth=2, path_effects=path_effect)

                elif ykey == 'st':

                    for i, time in enumerate(times):
                        if i == ii:
                            if time - snap_time > 0:

                                if binbins[ii] <= time and time <= binbins[ii+1]:
                                    mean_v_phi = nfw_vs_M200_raw[:,i,2]
                                    sigma_phi = nfw_vs_M200_raw[:,i,5]

                                    st = 1 + erf( - mean_v_phi / (np.sqrt(2) * sigma_phi))

                                    st =  1 - st

                                    plt.errorbar(logM200s + 10, st,
                                                 c=colours[i], ls='--', linewidth=2, path_effects=path_effect)

                elif ykey == 'r200':
                    plt.errorbar(logM200s + 10, np.log10(r200s), ls=':', c='grey', linewidth=1)

                elif ykey in ['R12', 'r12', 'z12']:

                    for i in range(30):
                        plt.errorbar([6, 14], [-4 + i / 3, -4 + 8 / 3 + i / 3], color='grey', ls=':', linewidth=1,
                                     zorder=11)

                    eps = 0.7 #kpc
                    plt.errorbar([6, 14], [np.log10(eps)]*2, color='grey', ls='--', linewidth=1,
                                 zorder=0)
                    plt.errorbar([6, 14], [np.log10(2.8 * eps)]*2, color='grey', ls='--', linewidth=1,
                                 zorder=0)

                    if ykey != 'z12':
                        # disk
                        R_halfs = 0.2 * rs  # kpc
                        # half = 0.5
                        # R_d = R_halfs * (-lambertw((half - 1) / np.exp(1), -1) - 1).real  # kpc
                        # M_disk = 0  # 10^10 M_sun

                        if ykey == 'R12':
                            #Navarro et al. 2017
                            # R_halfs *= 3/4
                            pass

                        plt.errorbar(logM200s + 10, np.log10(R_halfs), ls=':', c='k', linewidth=4)

                        # disk
                        #fiducial rez
                        # if np.isclose(mdm, 0.0009695046317185129):
                        frac = 0.2
                        if z == 0.0:
                            const = 10**0.55 #10**0.45
                        if z == 0.503:
                            const = 10**0.3
                        if z == 1.004:
                            const = 10**0.15
                        if z == 2.012:
                            const = 10**-0.05

                        if ykey == 'R12':
                            # Navarro et al. 2017
                            # R_halfs *= 3 / 4
                            pass

                        pow = 4
                        R_halfs = np.power(R_halfs ** pow + const ** pow, 1 / pow)

                        plt.errorbar(logM200s + 10, np.log10(R_halfs), ls=':', c='k', linewidth=4)

                        #high rez
                        # elif np.isclose(mdm, 0.00013850066167407329):
                        frac = 0.2
                        if z == 0.0:
                            const = 10**-0.15
                        if z == 0.503:
                            const = 10**-0.30
                        if z == 1.004:
                            const = 10**-0.30
                        if z == 2.012:
                            const = 10**-0.30

                        R_halfs = frac * rs # kpc

                        if ykey == 'R12':
                            #Navarro et al. 2017
                            # R_halfs *= 3/4
                            pass

                        pow = 4
                        R_halfs = np.power(R_halfs**pow + const**pow, 1/pow)

                        plt.errorbar(logM200s + 10, np.log10(R_halfs), ls=':', c='grey', linewidth=2)

                    else: #is z12
                        # R_halfs = 0.2 * rs  # kpc
                        #
                        # R_halfs *= 3/4
                        #
                        # z_halfs = R_halfs / 2
                        #
                        # plt.errorbar(logM200s + 10, np.log10(z_halfs), ls='-', c='k', linewidth=2)
                        pass

                        #
                    # plog10(z_halfs), ls='-', c='k', linewidth=2)

            if xkey == 'r200':
                if ykey in ('R12', 'r12'):
                    for i in range(10):
                        plt.errorbar([0, 4], [-4 + i, 0 + i], color='grey', ls=':', linewidth=1, zorder=11)

                    mdm = 1 / raw_data0['Header/MDMReciprocal'][()][0]

                    # NFW halo
                    conc = concentration_ludlow(z, 10 ** logM200s)

                    r200s = np.power(3 * 10 ** logM200 / (4 * np.pi * rho_200), 1 / 3)  # kpc
                    # v200 = np.sqrt(GRAV_CONST * M200 / r200)  # km/s
                    rs = r200s / conc

                    # disk
                    R_d = 0.2 * rs  # kpc
                    half = 0.5
                    R_halfs = -R_d * (lambertw((half - 1) / np.exp(1), -1) + 1).real  # kpc
                    # M_disk = 0  # 10^10 M_sun

                    plt.errorbar(np.log10(r200s), np.log10(R_halfs), ls='-', c='k', linewidth=2)

            if xkey == 'age':
                if ykey == 'sigma_tot':
                    ax.axhline(np.log10(np.sqrt(3) * nfw_vs_M200_raw[ii, 0, 1]), 0, 1, ls='--', c='k', lw=2)
                    sigma_tot = np.sqrt(nfw_vs_M200_raw[ii, :, 3] ** 2 + nfw_vs_M200_raw[ii, :, 4] ** 2 +
                                        nfw_vs_M200_raw[ii, :, 5] ** 2)
                    plt.errorbar(times, np.log10(sigma_tot), ls=(0,(0.8,0.8)), c='C1', lw=3, path_effects=path_effect)

                    if cent_sat_split == True:
                        ax.axhline(np.log10(np.sqrt(3) * nfw_vs_M200_1_raw[ii, 0, 1]), 0, 1, ls='-', c='k', lw=2)

                        sigma_tot_1 = np.sqrt(nfw_vs_M200_1_raw[ii, :, 3] ** 2 + nfw_vs_M200_1_raw[ii, :, 4] ** 2 +
                                              nfw_vs_M200_1_raw[ii, :, 5] ** 2)
                        plt.errorbar(times, np.log10(sigma_tot_1), ls='-.', c='C4=0', lw=3, path_effects=path_effect)

                elif ykey in ['sigma_tot_v200', 'root_ek_v200']:
                    ax.axhline(np.log10(np.sqrt(3) * nfw_vs_M200_raw[ii, 0, 1] / v200s[ii]), 0, 1, ls='--', c='k', lw=2)
                    sigma_tot = np.sqrt(nfw_vs_M200_raw[ii, :, 3] ** 2 + nfw_vs_M200_raw[ii, :, 4] ** 2 +
                                        nfw_vs_M200_raw[ii, :, 5] ** 2)
                    plt.errorbar(times, np.log10(sigma_tot / v200s[ii]), ls=(0,(0.8,0.8)), c='C1', lw=3, path_effects=path_effect)

                    if cent_sat_split == True:
                        ax.axhline(np.log10(np.sqrt(3) * nfw_vs_M200_1_raw[ii, 0, 1] / v200s[ii]), 0, 1, ls='-', c='k', lw=2)

                        sigma_tot_1 = np.sqrt(nfw_vs_M200_1_raw[ii, :, 3] ** 2 + nfw_vs_M200_1_raw[ii, :, 4] ** 2 +
                                              nfw_vs_M200_1_raw[ii, :, 5] ** 2)
                        plt.errorbar(times, np.log10(sigma_tot_1 / v200s[ii]), ls='-.', c='C0', lw=3, path_effects=path_effect)

                elif ykey in ['mean_v_phi_v200', 'mean_v_R_v200', 'median_v_phi_v200']:
                    ax.axhline(np.log10(nfw_vs_M200_raw[ii, 0, 0] / v200s[ii]), 0, 1,
                               ls='--', c='k', lw=2)
                    plt.errorbar(times, np.log10(nfw_vs_M200_raw[ii, :, 2] / v200s[ii]),
                                 ls=(0,(0.8,0.8)), c='C1', lw=3, path_effects=path_effect)

                    if cent_sat_split == True:
                        ax.axhline(np.log10(nfw_vs_M200_1_raw[ii, 0, 0] / v200s[ii]), 0, 1,
                                   ls='-', c='k', lw=2)
                        plt.errorbar(times, np.log10(nfw_vs_M200_1_raw[ii, :, 2] / v200s[ii]),
                                     ls='-.', c='C0', lw=3, path_effects=path_effect)

                elif ykey in ['sigma_z_v200', 'sigma_R_v200', 'sigma_phi_v200']:
                    if 'z' in ykey: k=3
                    elif 'R' in ykey: k=4
                    elif 'phi' in ykey: k=5

                    ax.axhline(np.log10(nfw_vs_M200_raw[ii, 0, 1] / v200s[ii]), 0, 1,
                               ls='--', c='k', lw=2)
                    plt.errorbar(times, np.log10(nfw_vs_M200_raw[ii, :, k] / v200s[ii]),
                                 ls=(0,(0.8,0.8)), c='C1', lw=3, path_effects=path_effect)

                    if cent_sat_split == True:
                        ax.axhline(np.log10(nfw_vs_M200_1_raw[ii, 0, 1] / v200s[ii]), 0, 1,
                                   ls='-', c='k', lw=2)
                        plt.errorbar(times, np.log10(nfw_vs_M200_1_raw[ii, :, k] / v200s[ii]),
                                     ls='-.', c='C0', lw=3, path_effects=path_effect)

                elif ykey == 'mean_v_phi' or ykey == 'median_v_phi':
                    ax.axhline(np.log10(nfw_vs_M200_raw[ii, 0, 0]), 0, 1,
                               ls='--', c='k', lw=2)
                    plt.errorbar(times, np.log10(nfw_vs_M200_raw[ii, :, 2]),
                                 ls=(0,(0.8,0.8)), c='C1', lw=3, path_effects=path_effect)

                    if cent_sat_split == True:
                        ax.axhline(np.log10(nfw_vs_M200_1_raw[ii, 0, 0]), 0, 1,
                                   ls='-', c='k', lw=2)
                        plt.errorbar(times, np.log10(nfw_vs_M200_1_raw[ii, :, 2]),
                                     ls='-.', c='C0', lw=3, path_effects=path_effect)

            #various guide lines
            if ykey == 'z12R12':
                #spherical distribution
                # plt.axhline(np.log10(1/np.sqrt(3)), 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
                plt.axhline(np.log10(1/2), 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
            elif ykey == 'ca' or ykey == 'ba':
                #spherical distribution
                plt.axhline(np.log10(1), 0, 1, color='grey', ls=':', linewidth=1, zorder=11)

            elif ykey[0] == 'm' and xkey[0] == 'm':
                for i in range(40):
                    plt.errorbar([0, 20], [-10 + i/2, 10 + i/2], color='grey', ls=':', linewidth=1, zorder=11)

            elif ykey == 'kappa_co':
                plt.axhline(np.log10(1), 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
                plt.axhline(np.log10(0.4), 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
                plt.axhline(np.log10(1/6), 0, 1, color='grey', ls=':', linewidth=1, zorder=11)

            elif ykey == 'kappa_rot':
                plt.axhline(1, 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
                plt.axhline(0.7, 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
                plt.axhline(0.5, 0, 1, color='grey', ls=':', linewidth=1, zorder=11)
                plt.axhline(1/3, 0, 1, color='grey', ls=':', linewidth=1, zorder=11)

            # plt.errorbar([6, 12], [0, 6], color='grey', ls=':', linewidth=1, zorder=11)

            elif ykey in ('mstarmtot', 'mgasmtot', 'mbarymtot'):
                omega_m = 0.307
                omega_b = 0.04825
                log_cosmo_bary_frac = np.log10(omega_b / omega_m)
                plt.errorbar([0, 20], [log_cosmo_bary_frac, log_cosmo_bary_frac], color='grey', ls='--', linewidth=1, zorder=11)

                for i in range(6):
                    plt.errorbar([0, 20], [-i, -i], color='grey', ls=':', linewidth=1, zorder=11)

            elif ykey in ('mean_v_phi', 'median_v_phi', 'sigma_z', 'sigma_R', 'sigma_z', 'sigma_tot') and xkey[0] == 'm':
                for i in range(100):
                    plt.errorbar([6, 14], [-4+i/3, -4 + 8/3 +i/3], color='grey', ls=':', linewidth=1, zorder=11)

            elif ykey == 'dt':
                plt.errorbar([6, 14], [0.5, 0.5], color='grey', ls=':', linewidth=1, zorder=11)
                plt.errorbar([6, 14], [0.015, 0.015], color='grey', ls=':', linewidth=1, zorder=11)
            elif ykey == 'st':
                plt.errorbar([6, 14], [0.5, 0.5], color='grey', ls=':', linewidth=1, zorder=11)
                plt.errorbar([6, 14], [1, 1], color='grey', ls=':', linewidth=1, zorder=11)

        #labels and colorbars
        if ii % 4 == 0 or not bins_on_new_panel:
            ylab = label_dict[ykey]

            if ((column0_dict[xkey][rkey, 0] == column0_dict[xkey]['test', 0]) and
                (column1_dict[xkey][rkey, 0] == column1_dict[xkey]['test', 0]) and
                (column2_dict[xkey][rkey, 0] == column2_dict[xkey]['test', 0]) and
                (column0_dict[ykey][rkey, 0] == column0_dict[ykey]['test', 0]) and
                (column1_dict[ykey][rkey, 0] == column1_dict[ykey]['test', 0]) and
                (column2_dict[ykey][rkey, 0] == column2_dict[ykey]['test', 0])):
                pass
            else:
                ylab += ' ' + rkey_label_dict[rkey]
            plt.ylabel(ylab)
        else:
            ax.set_yticklabels([])

        if ii // 4 == (len(binbins) -2) // 4 or not bins_on_new_panel:
            plt.xlabel(label_dict[xkey])

            if xkey in ['m200', 'mtot']:
                ax.set_xticks([10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14])
                ax.set_xticklabels([10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, None])
        else:
            ax.set_xticklabels([])

        plt.xlim(lims_dict[xkey])
        plt.ylim(lims_dict[ykey])

        try:
            if not four_z and not cent_sat_split or not bins_on_new_panel:
                if ii == len(binbins) - 2:

                    Bbox = ax.get_position()
                    cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])

                    axrange = [np.amin(binbins), np.amax(binbins)]
                    x_ = np.linspace(1 - 1e-3, 1e-3, 101)
                    x, _ = np.meshgrid(x_, np.array([0, 1]))
                    plt.imshow(x.T, cmap='inferno' + '_r', aspect=2 / 101 / 0.08)
                    cax.yaxis.set_label_position('right')
                    cax.yaxis.tick_right()
                    cax.set_xticks([])
                    cax.set_yticks( 100 - ((binbins[1:] + binbins[:-1])/2 - binbins[0]) / (binbins[-1] - binbins[0]) * 100 )
                    cax.set_yticklabels((binbins[1:] + binbins[:-1])/2)

                    cax.set_xticklabels([])
                    cax.set_ylabel(label_dict[binkey])

        except Exception:
            print('cbar broke. binbins needs to be array')
            pass

        # if quartile_dict[ykey]:
        #     if ii == len(binbins) - 2:
        #         Bbox = ax.get_position()
        #
        #         cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])
        #
        #         axrange = [np.amin(quartile_dict[ykey][1:-1]), np.amax(quartile_dict[ykey][1:-1])]
        #         x_ = np.linspace(1-1e-3, 1e-3, 101)
        #         x,_ = np.meshgrid(x_, np.array([0,1]))
        #         plt.imshow(x.T, cmap='viridis', aspect= 2 / 101 / 0.08)
        #         cax.yaxis.set_label_position('right')
        #         cax.yaxis.tick_right()
        #         cax.set_yticks(101 * (axrange[1] - np.array(quartile_dict[ykey][1:-1])) / np.diff(axrange) - 0.5)
        #         cax.set_yticklabels([round(q,2) for q in quartile_dict[ykey][1:-1]])
        #         cax.set_xticks([])
        #         cax.set_xticklabels([])
        #         cax.set_ylabel(r'$X$')

    if legend:
        if model and ykey in ['sigma_tot_v200', 'root_ek_v200'] and raw_data1 is not None and xkey in ['m200']:
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='white', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C1', ls=(0,(0.8,0.8)), linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-.', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='k', ls='-', linewidth=3)),],
                           [r'$1\times$DM Centrals',
                            r'$7\times$DM Centrals',
                            ' ',
                            r'$1\times$DM model',
                            r'$7\times$DM model',
                            r'$\sigma_{\rm DM}$ (NFW)',],
                           # title=title,
                           ncol=2, loc='upper center')

        if model and ykey == 'mean_v_phi_v200' and xkey in ['m200']:
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='white', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C1', ls=(0,(0.8,0.8)), linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-.', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='k', ls='-', linewidth=3)),],
                           [r'$1\times$DM Centrals',
                            r'$7\times$DM Centrals',
                            ' ',
                            r'$1\times$DM model',
                            r'$7\times$DM model',
                            r'$V_c$ (NFW)',],
                           # title=title,
                           ncol=2, loc='upper center')

        if model and ykey == 'vsigma' and xkey in ['m200']:
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C1', ls=(0,(0.8,0.8)), linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='C0', ls='-.', linewidth=3)),],
                           [r'$1\times$DM Centrals',
                            r'$7\times$DM Centrals',
                            r'$1\times$DM model',
                            r'$7\times$DM model',],
                           # title=title,
                           ncol=2, loc='upper center')

    if save:
        filename = '../results/'
        filename += filename_dict[xkey]
        filename += '_' + filename_dict[ykey]
        filename += '_' + filename_dict[rkey]
        filename += '_' + filename_dict[comp]
        if binkey != None:
            filename += '_' + filename_dict[binkey]
        if cent_sat_split:
            filename += '_' + snap_str
            if not cent_only:
                filename += '_cent_sat'
        elif four_z:
            filename += '_' + sim_str
        else:
            filename += '_' + sim_str + '_' + snap_str
        if cent_only:
            filename += '_cent'
        if len(binbins) > 2:
            filename += f'_{len(binbins)-1}bins'
        filename += '.png'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def make_profile_figure(raw_data0, raw_data1=None, raw_data2=None, raw_data3=None,
                        ykey='kappa_rot', comp='Star', ckey='age',
                        cent_sat_split=True,
                        cent_only=False, four_z=False,
                        snap_str='', sim_str='', save=True, cbins=[0, UNIVERSE_AGE]):

    if comp == 'Star':
        radial_bin_mask = np.zeros(n_dim + n_star_extra, dtype=bool)
        radial_bin_mask[:n_log] = True

    else:
        radial_bin_mask = np.zeros(n_dim, dtype=bool)
        radial_bin_mask[:n_log] = True

    r_ls = np.array([btf(entry) for entry in columns_l][:n_log])
    r_hs = np.array([btf(entry) for entry in columns_h][:n_log])
    radial_bin_centres = (np.log10(r_ls) + np.log10(r_hs))/2

    r_lims = [-1, 2.5] #ckpc

    # ####
    # if len(cbins) - 1 > 1:
    plt.figure(figsize=(5.3 * (len(cbins) - 1), 5.3), constrained_layout=True)

    # elif cent_sat_split or four_z:
    #     plt.figure(figsize=(6.4,6.4), constrained_layout=True)
    # else:
    #     #need to make space for color bar
    #     plt.figure(figsize=(6.4,5.3), constrained_layout=True)
    # # plt.figure(constrained_layout=True)

    # if len(cbins) -1 > 1:

    for ii in range(len(cbins)-1):
        ax = plt.subplot(1, len(cbins)-1, ii+1)
        ax.set_aspect(np.diff(r_lims) / np.diff(lims_dict[ykey]))

        warnings.filterwarnings("ignore")
        plt.subplots_adjust(wspace=0)

        mask1key = ckey
        mask1column = rkey
        mask1transform = lambda data: np.logical_and(cbins[ii] < data, data <= cbins[ii + 1])

        if cent_sat_split:
            if not cent_only:

                mask0key = 'sn'
                mask0column = None
                mask0transform = lambda mask: mask != 0

                mask = lambda raw_data: return_full_mask(raw_data,
                                                         mask0key, mask0column, comp, mask0transform,
                                                         mask1key, mask1column, comp, mask1transform,
                                                         mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                         mask_func_dict[ykey], ii)

                #satellites
                plot_r_vs_y(raw_data0,
                            radial_bin_mask, radial_bin_centres,
                            ykey, comp, mask(raw_data0),
                            colour='C2', linestyle = '-', marker = '.')
                plot_r_vs_y(raw_data1,
                            radial_bin_mask, radial_bin_centres,
                            ykey, comp, mask(raw_data1),
                            colour='C3', linestyle = '-', marker = '.')

            mask0key = 'sn'
            mask0column = None
            mask0transform = lambda mask: mask == 0

            mask = lambda raw_data : return_full_mask(raw_data,
                                                      mask0key, mask0column, comp, mask0transform,
                                                      mask1key, mask1column, comp, mask1transform,
                                                      mask_dict[ykey][comp], mask_column_dict[ykey][rkey, ii], comp,
                                                      mask_func_dict[ykey], ii)

            #centrals
            plot_r_vs_y(raw_data0,
                        radial_bin_mask, radial_bin_centres,
                        ykey, comp, mask(raw_data0),
                        colour='C0', linestyle = '-', marker = '.')
            plot_r_vs_y(raw_data1,
                        radial_bin_mask, radial_bin_centres,
                        ykey, comp, mask(raw_data1),
                        colour='C1', linestyle = '--', marker = '.')

            title = 'z=' + snap_str[7:].replace('p', '.')
            if len(cbins) > 2:
                title += '\n' + label_dict[ckey] + ': ' + str(cbins[ii]) + ' - ' + str(round(cbins[ii+1], 1))
            if cent_only:
                l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),],
                           [r'$1\times$DM Centrals',
                            r'$7\times$DM Centrals'],
                           title=title, ncol=2,)
            else:
                l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='C1', ls='--', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C3', ls='-.', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=3)),
                                (lines.Line2D([0, 1], [0, 1], color='C2', ls=(0,(0.8,0.8)), linewidth=3))],
                               [r'$1\times$DM Centrals', r'$1\times$DM Satellites',
                                r'$7\times$DM Centrals', r'$7\times$DM Satellites'],
                               title=title, ncol=2,)
            l.set_zorder(-10)
            l.get_title().set_multialignment('center')


        elif four_z:

            if cent_only:
                mask0key = 'GalaxyQuantities/SubGroupNumber'
                mask0column = None
                mask0transform = lambda mask: mask == 0
            else:
                mask0key = 'GalaxyQuantities/SubGroupNumber'
                mask0column = None
                mask0transform = lambda mask: True

            plot_r_vs_y(raw_data3,
                        radial_bin_mask=radial_bin_mask, radial_bin_centres=radial_bin_centres,
                        y0key=key0_dict[ykey][comp], y1key=key1_dict[ykey][comp],
                        y2key=key2_dict[ykey][comp], y3key=key3_dict[ykey][comp],
                        ytransform=transform_dict[ykey], ylims=lims_dict[ykey],
                        mask0key=mask0key, mask0column=mask0column, mask0transform=mask0transform,
                        mask1key=mask1key, mask1column=mask1column, mask1transform=mask1transform,
                        mask2key=mask_dict[ykey][comp], mask2column=mask_column_dict[ykey][rkey, ii],
                        mask2transform=mask_func_dict[ykey],
                        colour=matplotlib.cm.get_cmap('inferno')(0.5/4), linestyle=(0,(0.8,0.8)), marker='.',
                        cmap='viridis')
            plot_r_vs_y(raw_data2,
                        radial_bin_mask=radial_bin_mask, radial_bin_centres=radial_bin_centres,
                        y0key=key0_dict[ykey][comp], y1key=key1_dict[ykey][comp],
                        y2key=key2_dict[ykey][comp], y3key=key3_dict[ykey][comp],
                        ytransform=transform_dict[ykey], ylims=lims_dict[ykey],
                        mask0key=mask0key, mask0column=mask0column, mask0transform=mask0transform,
                        mask1key=mask1key, mask1column=mask1column, mask1transform=mask1transform,
                        mask2key=mask_dict[ykey][comp], mask2column=mask_column_dict[ykey][rkey, ii],
                        mask2transform=mask_func_dict[ykey],
                        colour=matplotlib.cm.get_cmap('inferno')(1.5/4), linestyle='--', marker='.',
                        cmap='viridis')
            plot_r_vs_y(raw_data1,
                        radial_bin_mask=radial_bin_mask, radial_bin_centres=radial_bin_centres,
                        y0key=key0_dict[ykey][comp], y1key=key1_dict[ykey][comp],
                        y2key=key2_dict[ykey][comp], y3key=key3_dict[ykey][comp],
                        ytransform=transform_dict[ykey], ylims=lims_dict[ykey],
                        mask0key=mask0key, mask0column=mask0column, mask0transform=mask0transform,
                        mask1key=mask1key, mask1column=mask1column, mask1transform=mask1transform,
                        mask2key=mask_dict[ykey][comp], mask2column=mask_column_dict[ykey][rkey, ii],
                        mask2transform=mask_func_dict[ykey],
                        colour=matplotlib.cm.get_cmap('inferno')(2.5/4), linestyle='-.', marker='.',
                        cmap='viridis')
            plot_r_vs_y(raw_data0,
                        radial_bin_mask=radial_bin_mask, radial_bin_centres=radial_bin_centres,
                        y0key=key0_dict[ykey][comp], y1key=key1_dict[ykey][comp],
                        y2key=key2_dict[ykey][comp], y3key=key3_dict[ykey][comp],
                        ytransform=transform_dict[ykey], ylims=lims_dict[ykey],
                        mask0key=mask0key, mask0column=mask0column, mask0transform=mask0transform,
                        mask1key=mask1key, mask1column=mask1column, mask1transform=mask1transform,
                        mask2key=mask_dict[ykey][comp], mask2column=mask_column_dict[ykey][rkey, ii],
                        mask2transform=mask_func_dict[ykey],
                        colour=matplotlib.cm.get_cmap('inferno')(3.5/4), linestyle='-', marker='.',
                        cmap='viridis')

            title = sim_str
            if len(cbins) > 2:
                title += '\n' + label_dict[ckey] + ': ' + str(cbins[ii]) + ' - ' + str(round(cbins[ii+1], 1))
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(3.5/4), ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(2.5/4), ls='-.', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(1.5/4), ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('inferno')(0.5/4), ls=(0,(0.8,0.8)), linewidth=3))],
                           [r'$z=0$', r'$z=0.5$', r'$z=1$', r'$z=2$'],
                           title=title, ncol=2,)
            l.set_zorder(-10)
            l.get_title().set_multialignment('center')

        else:

            if cent_only:
                mask0key = 'GalaxyQuantities/SubGroupNumber'
                mask0column = None
                mask0transform = lambda mask: mask == 0
            else:
                mask0key = 'GalaxyQuantities/SubGroupNumber'
                mask0column = None
                mask0transform = lambda mask: True

            plot_r_vs_y(raw_data0,
                        radial_bin_mask=radial_bin_mask, radial_bin_centres=radial_bin_centres,
                        y0key=key0_dict[ykey][comp], y1key=key1_dict[ykey][comp],
                        y2key=key2_dict[ykey][comp], y3key=key3_dict[ykey][comp],
                        ytransform=transform_dict[ykey], ylims=lims_dict[ykey],
                        mask0key=mask0key, mask0column=mask0column, mask0transform=mask0transform,
                        mask1key=mask1key, mask1column=mask1column, mask1transform=mask1transform,
                        mask2key=mask_dict[ykey][comp], mask2column=mask_column_dict[ykey][rkey, ii],
                        mask2transform=mask_func_dict[ykey],
                        c0key=key0_dict[ckey][comp], c0column=column0_dict[ckey][rkey],
                        ctransform=transform_dict[ckey], clims=lims_dict[ckey],
                        colour='k', linestyle='-', marker='.',
                        cmap='viridis')

        #####

        if ii == 0:
            plt.ylabel(label_dict[ykey])
        else:
            ax.set_yticklabels([])

        plt.xlabel(r'$\log \, r / $pkpc')

        plt.ylim(lims_dict[ykey])
        plt.xlim(r_lims)

        try:
            if not four_z and not cent_sat_split:
                if ii == len(cbins) - 2:
                    Bbox = ax.get_position()
                    cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0)*0.08, Bbox.y1 - Bbox.y0])
                    cbar = plt.colorbar(cax=cax)
                    cbar.set_label(label_dict[ckey])
        except:
            pass

    if save:
        filename = '../results/'
        filename += filename_dict[ykey]
        filename += '_profile'
        filename += '_' + filename_dict[comp]
        if ckey != None:
            filename += '_' + filename_dict[ckey]
        if cent_sat_split:
            filename += '_' + snap_str
            if not cent_only:
                filename += '_cent_sat'
        elif four_z:
            filename += '_' + sim_str
        else:
            filename += '_' + sim_str + '_' + snap_str
        if cent_only:
            filename += '_cent'
        if len(cbins) > 2:
            filename += f'_{len(cbins)-1}bins'
        filename += '.png'
        plt.savefig(filename, bbox_inches='tight')
        print(filename)
        plt.close()

    return


def plot_x_vs_jzonc(raw_data0,
                    jcolumn=lt_r200_c,
                    x0key=None, x0column=None,
                    x1key=None, x1column=None,
                    x2key=None, x2column=None,
                    x3key=None, x3column=None,
                    xtransform = lambda y: y,
                    xlims=[None, None],
                    mask0key=None, mask0column=None, mask0transform = lambda mask: True,
                    mask1key=None, mask1column=None, mask1transform = lambda mask: True,
                    mask2key=None, mask2column=None, mask2transform = lambda mask: True,
                    c0key=None, c0column=None,
                    ctransform = lambda mask: mask,
                    clims=[None, None],
                    N_run=NRUN, colour='C0', linestyle='-', marker='.', cmap='turbo',
                    bootstrap=True):

    if type(N_run) == int:
        N_run = np.linspace(*xlims, N_run)

    if mask2key != None:
        mask0 = np.hstack(mask0transform(raw_data0[mask0key][()][:, mask0column]))
        mask1 = np.hstack(mask1transform(raw_data0[mask1key][()][:, mask1column]))
        mask2 = np.hstack(mask2transform(raw_data0[mask2key][()][:, mask2column]))

        mask0 = np.logical_and(np.logical_and(mask0, mask1), mask2)

    elif mask1key != None:
        mask0 = np.hstack(mask0transform(raw_data0[mask0key][()][:, mask0column]))
        mask1 = np.hstack(mask1transform(raw_data0[mask1key][()][:, mask1column]))

        mask0 = np.logical_and(mask0, mask1)

    elif mask0key != None:
        mask0 = np.hstack(mask0transform(raw_data0[mask0key][()][:, mask0column]))
    else:
        mask0 = np.ones(np.shape(raw_data0['GalaxyQuantities/GroupNumber'][()]), dtype=bool)

    if np.sum(mask0) > 1:

        if x3key != None:
            x0data = process_array(raw_data0, x0key, mask0, x3column)
            x1data = process_array(raw_data0, x1key, mask0, x2column)
            x2data = process_array(raw_data0, x2key, mask0, x1column)
            x3data = process_array(raw_data0, x3key, mask0, x0column)
            x0 = xtransform(x0data, x1data, x2data, x3data)
        elif x2key != None:
            x0data = process_array(raw_data0, x0key, mask0, x2column)
            x1data = process_array(raw_data0, x1key, mask0, x1column)
            x2data = process_array(raw_data0, x2key, mask0, x0column)
            x0 = xtransform(x0data, x1data, x2data)
        elif x1key != None:
            x0data = process_array(raw_data0, x0key, mask0, x1column)
            x1data = process_array(raw_data0, x1key, mask0, x0column)
            x0 = xtransform(x0data, x1data)
        else:
            x0data = process_array(raw_data0, x0key, mask0, x0column)
            x0 = xtransform(x0data)

        fjzjcs = raw_data0['GalaxyProfiles/Star/fjzjc'][()][mask0, jcolumn]

        # print(np.shape(x0))
        # print(np.shape(fjzjcs))

        n_jzjc_bins = 21
        j_zonc_edges = np.linspace(-1, 1, n_jzjc_bins)
        j_zonc_centres = 0.5 * (j_zonc_edges[1:] + j_zonc_edges[:-1])

        clevel = lambda level: (1.2 * level + 0.2)
        n_contours = 6
        contours = [0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5]  # np.linspace(0, 0.5, n_contours+1)

        colors = np.array([matplotlib.colors.to_rgba(colour)] * 256)
        colors[:128, :3] = 1 - (1 - colors[:128, :3]) * np.column_stack([np.linspace(0, 1, 128)] * 3)
        colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0, 128)] * 3)
        cmaps = matplotlib.colors.ListedColormap(colors)

        n_galaxies = np.sum(mask0)

        # j_zoncs_cumulative = np.cumsum(j_zoncs[:, _index, :], axis=1)
        # j_zoncs_cumulative /= np.amax(j_zoncs_cumulative, axis=1)[:, np.newaxis]

        x = 0.5
        # xp = 0.84
        # xm = 0.16
        j_zonc_medians = np.zeros(n_galaxies)
        # v_phis_plus = np.zeros(snaps + 1)
        # v_phis_minus = np.zeros(snaps + 1)

        j_zonc_contours = np.zeros((2 * n_contours - 1, n_galaxies))

        for galaxy_index in range(n_galaxies):

            g = interp1d(j_zonc_edges, fjzjcs[galaxy_index, :], kind='linear', fill_value='extrapolate')

            try:
                j_zonc_medians[galaxy_index] = brentq(lambda v: (g(v) - x), -1.2, 1.2, rtol=1e-2, xtol=1e-2)
                # v_phis_plus[ti] = brentq(lambda v: (g(v) - xp), 0, 2)
                # v_phis_minus[ti] = brentq(lambda v: (g(v) - xm), -1, 1.5)
            except ValueError:
                if g(-1.2) - x == 0.5 or g(1.2) - x == -0.5:
                    j_zonc_medians[galaxy_index] = np.nan

                else:
                    print(galaxy_index, x0[galaxy_index])
                    print(g(-1.2) - x, g(1.2) - x)
                    raise ValueError

            for k, level in enumerate(contours[1:-1]):

                try:
                    j_zonc_contours[k, galaxy_index] = brentq(lambda v: (g(v) - level), -1.2, 1.2, rtol=1e-2, xtol=1e-2)
                    j_zonc_contours[2 * n_contours - k - 2, galaxy_index] = brentq(lambda v: (g(v) - (1 - level)), -1.2, 1.2,
                                                                                   rtol=1e-2, xtol=1e-2)
                except ValueError:
                    j_zonc_contours[k, galaxy_index] = np.nan
                    j_zonc_contours[2 * n_contours - k - 2, galaxy_index] = np.nan

        #bin by x
        not_crazy_mask = np.logical_and(np.isfinite(x0), np.isfinite(j_zonc_medians))
        x0 = x0[not_crazy_mask]
        j_zonc_medians = j_zonc_medians[not_crazy_mask]
        j_zonc_contours = j_zonc_contours[:, not_crazy_mask]
        n_galaxies = np.sum(not_crazy_mask)

        j_zonc_median, xedges, bin_is = binned_statistic(x0, j_zonc_medians,
                                                         statistic=np.nanmedian, bins=N_run)

        j_zonc_stacked_contours = np.zeros((2 * n_contours - 1, len(xedges)+1))

        #This is percentile
        for bin_i in np.unique(bin_is):
            j_zonc_stacked_contours[:, bin_i] = np.median(j_zonc_contours[:, bin_is == bin_i], axis=1)

        # #This is average
        # for galaxy_index in range(n_galaxies-1):
        #     j_zonc_stacked_contours[:, bin_is[galaxy_index]] += j_zonc_contours[:, galaxy_index]
        # for bin_i in np.unique(bin_is):
        #     j_zonc_stacked_contours[:, bin_i] /= np.sum(bin_is == bin_i)

        # if bootstrap:
        #     boots = np.zeros((len(xedges)-1, 3))
        #     for i in range(len(xedges)-1):
        #         boots[i] = my_bootstrap(y0[not_crazy_mask][bin_is == i + 1], statistic=np.nanmedian)
        #
        #     hatches = {'-': 'xxx', '--': '+++', '-.': r'\\\\ ', (0, (0.8, 0.8)): '...'}
        #
        #     # plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 1],
        #     #              c=colour, ls=linestyle, linewidth=2, zorder=9.7, path_effects=path_effect)
        #     plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 0], boots[:, 2],
        #                      color=colour, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle])

        # plt.scatter(x0, j_zonc_medians, color=colour, marker=marker, s=4, alpha=0.1)

        path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
        plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), j_zonc_median,
                     c=colour, ls=linestyle, linewidth=2, zorder=10, path_effects=path_effect)
        # plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]), yminus, yplus,
        #                  color=colour, alpha=0.3, edgecolor='k', zorder=9)

        for k, level in enumerate(contours[1:-1]):
            plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]),
                             j_zonc_stacked_contours[k, 1:-1], j_zonc_stacked_contours[2 * n_contours - k - 2, 1:-1],
                             color=cmaps(clevel(level)), edgecolor='k')

        # # scale = 1e-3
        # scale = 1
        # combine_jz_bins = kwargs['jzbins']
        #
        # print(combine_jz_bins)
        # j_zonc_combined = j_zonc_edges[::combine_jz_bins]
        # d_j_zonc = (j_zonc_combined[1] - j_zonc_combined[0]) * scale
        #
        # # plot insert
        # lsss = ['-', (1, (0.5, 0.5)), (1, (3, 0.8))]
        # for k, snap in enumerate(snaps_to_plot):
        #     j_zonc_data = np.sum([j_zoncs[snap, _index, i::combine_jz_bins] for i in range(combine_jz_bins)], axis=0)
        #     total_mass = np.sum(j_zonc_data)
        #     j_zonc_data *= 1 / total_mass * 1 / d_j_zonc
        #
        #     ydobuled = np.concatenate(([0], np.repeat(j_zonc_data, 2), [0]))
        #     plt.plot(np.repeat(j_zonc_combined, 2), np.log10(ydobuled + 1e-5),
        #              c=c, ls=lsss[k], lw=3)

    return

#TODO save this in hdf5 header and load
def plot_stacked_jzoncs(raw_data0, raw_data1=None, raw_data2=None, raw_data3=None,
                        xkey='m200', rkey='r200', comp='Star', ckey='age',
                        cent_sat_split=True,
                        cent_only=False, four_z=False,
                        snap_str='', sim_str='', save=True, cbins=[0, UNIVERSE_AGE]):

    ####
    plt.figure(figsize=(5.3 * (len(cbins) - 1), 5.3), constrained_layout=True)

    for ii in range(len(cbins)-1):
        ax = plt.subplot(1, len(cbins)-1, ii+1)
        ax.set_aspect(np.diff(lims_dict[xkey]) / 2)

        warnings.filterwarnings("ignore")
        plt.subplots_adjust(wspace=0)

        mask1key = key0_dict[ckey][comp]
        # mask1column = lt_r200_c
        mask1column = column0_dict[ckey]['r200']
        mask1transform = lambda mask1: np.logical_and(cbins[ii] < transform_dict[ckey](mask1),
                                                      transform_dict[ckey](mask1) <= cbins[ii + 1])

        if cent_only:
            mask0key = 'GalaxyQuantities/SubGroupNumber'
            mask0column = None
            mask0transform = lambda mask: mask == 0
        else:
            mask0key = 'GalaxyQuantities/SubGroupNumber'
            mask0column = None
            mask0transform = lambda mask: True

        plot_x_vs_jzonc(raw_data0,
                        jcolumn=rkey_to_rcolum_dict[rkey, ii],
                        x0key=key0_dict[xkey][comp], x0column=column0_dict[xkey][rkey, ii],
                        x1key=key1_dict[xkey][comp], x1column=column1_dict[xkey][rkey, ii],
                        x2key=key2_dict[xkey][comp], x2column=column2_dict[xkey][rkey, ii],
                        x3key=key3_dict[xkey][comp], x3column=column3_dict[xkey][rkey, ii],
                        xtransform=transform_dict[xkey], xlims=lims_dict[xkey],
                        mask0key=mask0key, mask0column=mask0column, mask0transform=mask0transform,
                        mask1key=mask1key, mask1column=mask1column, mask1transform=mask1transform,
                        mask2key=mask_dict[ykey][comp], mask2column=mask_column_dict[ykey][rkey, ii],
                        mask2transform=mask_func_dict[ykey],
                        c0key=key0_dict[ckey][comp], c0column=column0_dict[ckey][rkey, ii],
                        ctransform=transform_dict[ckey], clims=lims_dict[ckey],
                        colour='grey', linestyle='-', marker='.',
                        cmap='viridis')

        #####

        if ii == 0:
            plt.ylabel(r'Stacked $j_z / j_c(E)$')

            colors = np.array([matplotlib.colors.to_rgba('grey')] * 256)
            colors[:128, :3] = 1 - (1 - colors[:128, :3]) * np.column_stack([np.linspace(0, 1, 128)] * 3)
            colors[128:, :3] = colors[128:, :3] * np.column_stack([np.linspace(1, 0, 128)] * 3)
            greys = matplotlib.colors.ListedColormap(colors)

            contours = [0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5]  # np.linspace(0, 0.5, n_contours+1)
            clevel = lambda level: (1.2 * level + 0.2)

            plt.legend(reversed([(lines.Line2D([0, 1], [0, 1], color=greys(clevel(level)), ls='-', lw=8))
                                    for level in contours[1:-1]]),
                          reversed([str(int(100 * level)) + r'$\, - \,$' + str(int(100 * (1 - level))) + r'$\, \%$'
                                    for level in contours[1:-1]]),
                          title='Percentiles',)

        elif ii == 1:
            path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
            plt.legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=3, path_effects=path_effect))],
                        ['Median'],)

            ax.set_yticklabels([])
        else:
            ax.set_yticklabels([])

        plt.xlabel(label_dict[xkey])

        plt.ylim([-1, 1])
        plt.xlim(lims_dict[xkey])

    if save:
        filename = '../results/'
        filename += filename_dict[xkey]
        filename += '_jzonc_profile'
        filename += '_' + filename_dict[comp]
        if ckey != None:
            filename += '_' + filename_dict[ckey]
        if cent_sat_split:
            filename += '_' + snap_str
            if not cent_only:
                filename += '_cent_sat'
        elif four_z:
            filename += '_' + sim_str
        else:
            filename += '_' + sim_str + '_' + snap_str
        if cent_only:
            filename += '_cent'
        if len(cbins) > 2:
            filename += f'_{len(cbins)-1}bins'
        filename += '.png'
        plt.savefig(filename, bbox_inches='tight')
        print(filename)
        plt.close()


    return


def age_diversity_plot(raw_data0, rkey, comp, ckey, cbins=None,
                       cent_sat_split=True, cent_only=False,
                       snap_str=None, save=True):




    return


#TODO add check if LBTs already calculated
def add_age_to_hdf5(filename):
    with h5.File(filename, 'a') as file:
        a_array = file['GalaxyProfiles/Star/MedianFormationa'][()]

        t_array = np.zeros(np.shape(a_array))

        for i, a_gal in enumerate(a_array):
            t_array[i] = lbt(a = a_gal)

        if np.shape(a_array) != np.shape(t_array):
            raise ValueError

        star_prof = file['GalaxyProfiles/Star']
        star_prof.create_dataset('FormationLBT', data=t_array)

    print('sucess:', filename)

    return


if __name__ == '__main__':

    # radii = np.logspace(-1, 3, 100) #kpc
    # rs = 20 #kpc
    # c = 10
    # z = 0
    # omm = 0.307
    # oml = 0.693
    # a = 1 / (z + 1)
    # H_z2 = HUBBLE_CONST**2 * (oml + omm * np.power(a, -3))
    # rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST) #10^10 M_sun / kpc^3
    # M200 = 4/3 * np.pi * (rs * c)**3 * 200 * rho_crit #10^10 m_sun
    # v200 = np.sqrt(GRAV_CONST * M200 / (rs * c)) #km/s
    # sigma = np.array([get_analytic_nfw_dispersion(np.array([r]), rs, c, v200) for r in radii])
    # vc = np.array([get_analytic_nfw_circular_velocity(np.array([r]), rs, c, v200) for r in radii])
    # rho = np.array([get_analytic_nfw_density(np.array([r]), M200, c, rs) for r in radii])
    #
    # f_b = 0.03
    # M_h = (1 - f_b) * M200
    # M_d = f_b * M200
    # r_d = 0.2 * rs
    # sigma_disk_only = np.array([get_analytic_nfw_plus_exponential_disk_dispersion(np.array([r]),
    #                             0, rs, c, M_d, r_d) for r in radii])
    # sigma_halo_only = np.array([get_analytic_nfw_plus_exponential_disk_dispersion(np.array([r]),
    #                             M_h, rs, c, 0, r_d) for r in radii])
    # sigma_with_disk = np.array([get_analytic_nfw_plus_exponential_disk_dispersion(np.array([r]),
    #                             M_h, rs, c, M_d, r_d) for r in radii])
    #
    # plt.figure()
    #
    # plt.errorbar(radii, sigma)
    # plt.errorbar(radii, sigma_disk_only)
    # plt.errorbar(radii, sigma_halo_only)
    # plt.errorbar(radii, sigma_with_disk)
    # plt.errorbar(radii, np.sqrt(sigma_disk_only**2 + sigma_halo_only**2))
    # # plt.errorbar(radii, vc)
    # plt.axvline(0.2 * rs)
    # plt.axvline(rs)
    # plt.axvline(rs * c)
    #
    # # plt.errorbar(radii / (rs * c), sigma)
    # # plt.errorbar(radii / (rs * c), vc)
    # # plt.axvline(1/c)
    # # plt.axvline(1)
    #
    # # plt.semilogx()
    # plt.loglog()
    #
    # # plt.figure()
    # #
    # # plt.errorbar(radii, rho)
    # # plt.axvline(rs)
    # #
    # # plt.loglog()
    #
    # plt.show()

    start_time = time.time()

    save=True

    cmap = 'turbo'

    np.seterr(all='ignore')

    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5'
    # combined_name = '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5'

    combined_name_list = [
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile_multi.hdf5',
    ]

    # [add_age_to_hdf5(filename) for filename in combined_name_list]

    # combined_name_list = [
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5'
    # ]

    combined_name_pairs = [
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile_multi.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile_multi.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile_multi.hdf5']
    ]

    combined_name_redshifts = [
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile_multi.hdf5',],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile_multi.hdf5',],
    ]

    # combined_name_pairs = [
    #     ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5'],
    #     ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5'],
    #     ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5'],
    #     ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5',
    #     '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5']
    # ]
    # 'mstar', 'mgas', 'mdm', 'm200', 'mtot', 'mbary', 'mstarr', 'mgasr', 'mdmr', dm_density', 'ndm',
    # 'mstarmtot', 'mgasmtot', 'mbarymtot'
    # 'r12', 'R12', 'z12'  'z12R12', 'ca', 'ba', 'kappa_co', 'kappa_co_disk' 'kappa_rot', 'st', 'dt', 'lambda_r1bin', 'age'
    # 'mean_v_phi', 'median_v_phi', 'sigma_z', 'sigma_R', 'sigma_z', 'sigma_tot', 'vsigma', 'jtot', 'jz'
    # 'quart_kappa_co_disk', 'quart_kappa_co',, 'quart_dt', 'quart_dt_disk'
    # xkeys = ['m200']  #['mstar']
    # ykeys = ['r12_r200r'] #['sigma_tot_v200'] #['r12_r200'] #'mbh', #, 'sigma_tot', 'sigma_z', 'sigma_R', 'sigma_phi',]
    # rkeys = ['r200_profile'] #'eq_rh_profile'] #'r200' #['r200_profile'] #['lt_5r12', 'r200'] #['eq_rh_profile'] # 'r200', 'lt_r12', 'eq_R14', 'eq_R12',  'eq_R34'
    # ckeys = ['age'] #['age'] #['kappa_co']  #None 'age'
    # cbins = [[0,2,4,6,8,10,12,UNIVERSE_AGE], ] #  #[[0, 1/6, 0.4, 1]] #[[0, 1], ] #[[0, UNIVERSE_AGE],]
    # comps = ['Star']  # ['Star', 'Gas', 'DM']

    xkeys = ['mstar']
    ykeys = ['mstarmtotr']#, 'root_ek_v200']
    rkeys = ['lt_30', 'lt_5r12', 'lt_r12'] #['r200'] #['eq_02rs']
    ckeys = ['mstar']
    binkeys = ['age']
    binbins = [[0, UNIVERSE_AGE]]
    comps = ['Star']

    # xkeys = ['age']
    # ykeys = ['mean_v_R_v200'] #['sigma_tot_v200', 'root_ek_v200']
    # rkeys = ['eq_rh_profile'] #['r200_profile']
    # ckeys = [None]
    # binkeys = ['m200']
    # binbins = [np.linspace(10, 13, 25)] #[np.linspace(10, 14, 9)]
    # comps = ['Star']

    # xkeys = ['m200']  #['m200'] #['m200']
    # ykeys = ['mean_v_phi_v200'] #['r12_r200r'] #['mean_v_phi_v200', 'mean_v_R_v200'] #  ['root_ek_v200', 'sigma_tot_v200']
    # rkeys = ['eq_rh_profile']# ['r200_profile']# [f'eq_rh_profile{i}' for i in range(7)]  #'eq_R12' 'eq_02rs' 'r200', 'lt_r12',
    # ckeys = [None]
    # binkeys = ['age']
    # binbins = [[0, 2, 4, 6, 8, 10, 12, UNIVERSE_AGE]] #[np.linspace(10, 14, 9)]
    # comps = ['Star']  # ['Star', 'Gas', 'DM']

    # xkeys = ['m200']  #['m200'] #['m200']
    # ykeys = ['sigma_z_v200'] #, 'sigma_tot', 'sigma_z', 'sigma_R', 'sigma_phi',]
    # rkeys = ['eq_02rs'] # 'r200', 'lt_r12', 'eq_R14', 'eq_R12',  'eq_R34'
    # ckeys = [None]
    # binkeys = ['age'] #['age'] #None 'age'
    # binbins = [[0, UNIVERSE_AGE],] #[[0,3,6,9,UNIVERSE_AGE], ]
    # comps = ['Star']  # ['Star', 'Gas', 'DM']

    for xkey in xkeys:
        for ykey in ykeys:
            for rkey in rkeys:
                for comp in comps:
                    for binbin in binbins:
                        for binkey in binkeys:
                            # for pair in combined_name_pairs:
                                pair = combined_name_pairs[0]
                                with h5.File(pair[0], 'r') as raw_data0:
                                    with h5.File(pair[1], 'r') as raw_data1:

                                        make_full_figure(raw_data0, raw_data1, None, None,
                                                         xkey, ykey, rkey, comp, binkey,
                                                         cent_sat_split=True,
                                                         # cent_only= xkey != 'mstar', four_z=False,
                                                         cent_only=False, four_z=False,
                                                         snap_str=pair[0][53:65], sim_str=pair[0][41:50], save=save,
                                                         binbins=binbin, model=False,
                                                         iter_bins_on_panel=False, bins_on_new_panel=True, legend=True)

                            # for sim in combined_name_redshifts:
                            #     with h5.File(sim[0], 'r') as raw_data0:
                            #         with h5.File(sim[1], 'r') as raw_data1:
                            #             with h5.File(sim[2], 'r') as raw_data2:
                            #                 with h5.File(sim[3], 'r') as raw_data3:
                            #
                            #                     make_full_figure(raw_data0, raw_data1, raw_data2, raw_data3,
                            #                                      xkey, ykey, rkey, comp, binkey,
                            #                                      cent_sat_split=False,
                            #                                      cent_only=True, four_z=True,
                            #                                      snap_str=sim[0][53-2*(sim[0][55]=='z'):65-2*(sim[0][55]=='z')],
                            #                                      sim_str=sim[0][41:50], save=save,
                            #                                      binbin=binbin)

                            # # for run in combined_name_list:
                            # for run in combined_name_list[0::4]:
                            #     with h5.File(run, 'r') as raw_data0:
                            #         make_full_figure(raw_data0, None, None, None,
                            #                          xkey, ykey, rkey, comp, binkey,
                            #                          cent_sat_split=False,
                            #                          cent_only=True, four_z=False,
                            #                          snap_str=run[
                            #                                   53 - 2 * (run[55] == 'z'):65 - 2 * (
                            #                                               run[55] == 'z')],
                            #                          sim_str=run[41:50], save=True,
                            #                          binbins=binbin, model=False, iter_bins_on_panel=True)

                            # pass


    ykeys = ['mean_v_phi'] #['sigma_tot']
    comps = ['Star']  # ['Star', 'Gas', 'DM']
    ckeys = ['m200'] #None 'age'
    cbins = [[11.5, 12, 13, 15]] # [np.linspace(10, 14, 9)] #[np.linspace(8, 14, 13)]

    for ykey in ykeys:
        for comp in comps:
            for cbin in cbins:
                for ckey in ckeys:

                    # # for pair in combined_name_pairs:
                    #     pair = combined_name_pairs[0]
                    #     with h5.File(pair[0], 'r') as raw_data0:
                    #         with h5.File(pair[1], 'r') as raw_data1:
                    #
                    #             make_profile_figure(raw_data0, raw_data1, None, None,
                    #                                 ykey, comp, ckey,
                    #                                 cent_sat_split=True,
                    #                                 cent_only=True, four_z=False,
                    #                                 snap_str=pair[0][53:65], sim_str=pair[0][41:50], save=save,
                    #                                 cbins=cbin)
                    #
                    # for sim in combined_name_redshifts:
                    #     with h5.File(sim[0], 'r') as raw_data0:
                    #         with h5.File(sim[1], 'r') as raw_data1:
                    #             with h5.File(sim[2], 'r') as raw_data2:
                    #                 with h5.File(sim[3], 'r') as raw_data3:
                    #
                    #                     make_profile_figure(raw_data0, raw_data1, raw_data2, raw_data3,
                    #                                         ykey, comp, ckey,
                    #                                         cent_sat_split=False,
                    #                                         cent_only=True, four_z=True,
                    #                                         snap_str=sim[0][53:65],
                    #                                         sim_str=sim[0][41:50], save=save,
                    #                                         cbins=cbin)

                        pass

    xkeys = ['m200']  # ['m200']
    rkeys = ['eq_R12']  # 'r200', 'lt_r12', 'eq_R14', 'eq_R12',  'eq_R34'
    ckeys = ['kappa_co']  # None 'age'
    cbins = [np.log10([0, 1/6, 0.4, 1])] #[[0, UNIVERSE_AGE], ]  # [[0,3,6,9,UNIVERSE_AGE], ] #
    comps = ['Star']  # ['Star', 'Gas', 'DM']

    # for xkey in xkeys:
    #     for rkey in rkeys:
    #         for comp in comps:
    #             for cbin in cbins:
    #                 for ckey in ckeys:
    #
    #                     for run in combined_name_list:
    #                         with h5.File(run, 'r') as raw_data0:
    #
    #                             #TODO add labels back in
    #                             #TODO speed up
    #                             #TODO show multiple runs on the same plot
    #                             plot_stacked_jzoncs(raw_data0, None, None, None,
    #                                                 xkey, rkey, comp, ckey,
    #                                                 cent_sat_split=False,
    #                                                 cent_only=True, four_z=False,
    #                                                 snap_str=run[53-2*(run[55]=='z'):65-2*(run[55]=='z')],
    #                                                 sim_str=run[41:50], save=save,
    #                                                 cbins=cbin)
    #
    #                     pass


    plt.show()

    print(round(time.time() - start_time))
    print('Done.')

    pass