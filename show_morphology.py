#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 14:48:55 2020

@author: matt
"""

import os
# import time

import numpy as np
# import h5py as h5

from scipy.integrate   import quad

from scipy.interpolate import InterpolatedUnivariateSpline

from scipy.optimize    import minimize
from scipy.optimize    import brentq

from scipy.spatial.transform import Rotation

from scipy.special     import gamma
from scipy.special     import lambertw
from scipy.special     import erf

#matplotlib
import matplotlib.pyplot as plt
import splotch as splt

from matplotlib.colors import LogNorm

#my files
import halo_calculations as halo
import load_EAGLE

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = True
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.301e4 #kpc (km/s)^2 / (10^10Msun) (source ??)
HUBBLE_CONST = 0.06766 #km/s/kpc (Planck 2018)
RHO_CRIT = 3 * HUBBLE_CONST**2 / (8 * np.pi * GRAV_CONST) #10^10Msun/kpc^3

PC_ON_M = 3.0857e16 #pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16 #gyr/s

def calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                     thin_disk_threshold, rmin, rmax):
  '''
  '''
  star_mask = np.logical_and((rho > rmin), (rho < rmax))

  #commom masked quantities
  star_mass   = star_mass[star_mask]
  star_j_zonc = star_j_zonc[star_mask]
  v_phi       = v_phi[star_mask]

  #common constant
  star_mass_tot = np.sum(star_mass)

  mean_elipticity_value_out = halo.mass_weighted_mean(star_j_zonc,
                                                        star_mass)
  #jzoncs
  thin_disk_fraction_out    = np.sum(
    star_mass[star_j_zonc > thin_disk_threshold]) / star_mass_tot
  bulge_fraction_out        = 2 * np.sum(
    star_mass[star_j_zonc < 0]) / star_mass_tot

  #calculate kappas
  K_tot = np.sum(star_mass * np.square(
    np.linalg.norm(star_vel[star_mask], axis=1)))
  K_rot = np.sum(star_mass * np.square(v_phi))
  K_co  = np.sum(star_mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

  kappa_value_out    = K_rot / K_tot
  kappa_co_value_out = K_co  / K_tot

  #sigmas
  sigma_v_R_value_out = halo.mass_weighted_std(v_rho[star_mask], star_mass)
  sigma_v_phi_value_out = halo.mass_weighted_std(v_phi, star_mass)
  sigma_v_z_value_out   = halo.mass_weighted_std(v_z[star_mask], star_mass)
  sigma_v_tot_value_out = np.linalg.norm((sigma_v_R_value_out, sigma_v_phi_value_out,
                                          sigma_v_z_value_out))
  mean_v_phi_value_out = halo.mass_weighted_mean(v_phi, star_mass)

  v_on_sigma_value_out  = mean_v_phi_value_out / sigma_v_tot_value_out

  half_abs_z_value_out = halo.mass_weighted_median(
    np.abs(z[star_mask]), star_mass)

  return(mean_elipticity_value_out, thin_disk_fraction_out, bulge_fraction_out,
         kappa_value_out, kappa_co_value_out, v_on_sigma_value_out, half_abs_z_value_out,
         sigma_v_R_value_out, sigma_v_phi_value_out, sigma_v_z_value_out,
         sigma_v_tot_value_out, mean_v_phi_value_out)


def plot_morpholocial_corner(data_destination, snap, identity,
                             n_centrals=20, apature=30, save=True):
  '''
  '''
  r_25 = 4
  r_50 = 7
  r_75 = 11
  p=0.1
  thin_disk_threshold = 0.7

  mean_elipticity_value = np.zeros(n_centrals)
  thin_disk_fraction    = np.zeros(n_centrals)
  bulge_fraction        = np.zeros(n_centrals)
  kappa_value           = np.zeros(n_centrals)
  kappa_co_value        = np.zeros(n_centrals)
  v_on_sigma_value      = np.zeros(n_centrals)
  half_abs_z_value      = np.zeros(n_centrals)

  sigma_v_R_value       = np.zeros(n_centrals)
  sigma_v_phi_value     = np.zeros(n_centrals)
  # sigma_v_tild_value    = np.zeros(n_centrals)
  sigma_v_z_value       = np.zeros(n_centrals)
  sigma_v_tot_value     = np.zeros(n_centrals)
  mean_v_phi_value      = np.zeros(n_centrals)

  mean_elipticity_value_25 = np.zeros(n_centrals)
  thin_disk_fraction_25    = np.zeros(n_centrals)
  bulge_fraction_25        = np.zeros(n_centrals)
  kappa_value_25           = np.zeros(n_centrals)
  kappa_co_value_25        = np.zeros(n_centrals)
  v_on_sigma_value_25      = np.zeros(n_centrals)
  half_abs_z_value_25      = np.zeros(n_centrals)
  sigma_v_R_value_25       = np.zeros(n_centrals)
  sigma_v_phi_value_25     = np.zeros(n_centrals)
  sigma_v_z_value_25       = np.zeros(n_centrals)
  sigma_v_tot_value_25     = np.zeros(n_centrals)
  mean_v_phi_value_25      = np.zeros(n_centrals)

  mean_elipticity_value_50 = np.zeros(n_centrals)
  thin_disk_fraction_50    = np.zeros(n_centrals)
  bulge_fraction_50        = np.zeros(n_centrals)
  kappa_value_50           = np.zeros(n_centrals)
  kappa_co_value_50        = np.zeros(n_centrals)
  v_on_sigma_value_50      = np.zeros(n_centrals)
  sigma_v_R_value_50       = np.zeros(n_centrals)
  sigma_v_phi_value_50     = np.zeros(n_centrals)
  sigma_v_z_value_50       = np.zeros(n_centrals)
  sigma_v_tot_value_50     = np.zeros(n_centrals)
  mean_v_phi_value_50      = np.zeros(n_centrals)
  half_abs_z_value_50      = np.zeros(n_centrals)

  mean_elipticity_value_75 = np.zeros(n_centrals)
  thin_disk_fraction_75    = np.zeros(n_centrals)
  bulge_fraction_75        = np.zeros(n_centrals)
  kappa_value_75           = np.zeros(n_centrals)
  kappa_co_value_75        = np.zeros(n_centrals)
  v_on_sigma_value_75      = np.zeros(n_centrals)
  sigma_v_R_value_75       = np.zeros(n_centrals)
  sigma_v_phi_value_75     = np.zeros(n_centrals)
  sigma_v_z_value_75       = np.zeros(n_centrals)
  sigma_v_tot_value_75     = np.zeros(n_centrals)
  mean_v_phi_value_75      = np.zeros(n_centrals)
  half_abs_z_value_75      = np.zeros(n_centrals)

  for i in range(n_centrals):
    (file_name,
     gas_pos,  gas_vel,  gas_mass,  gas_bind,
     dm_pos,   dm_vel,   dm_mass,   dm_bind,
     star_pos, star_vel, star_mass, star_bind,
     bh_pos,   bh_vel,   bh_mass,   bh_bind,
     j_circ, binding_e, r_bins,
     gas_j_c, dm_j_c, star_j_c, bh_j_c
     ) = load_EAGLE.load_and_calculate(
       data_destination, snap, identity, group=i+1, subgroup=0)

    #some quantities for later
    star_mass_tot = np.sum(star_mass)

    #j_zonc
    star_j_z = np.cross(star_pos, star_vel)[:, 2]
    star_j_zonc = star_j_z / star_j_c

    #cylindrical coords
    (rho, phi, z, v_rho, v_phi, v_z
     ) = halo.get_cylindrical(star_pos, star_vel)

    (mean_elipticity_value[i], thin_disk_fraction[i], bulge_fraction[i],
     kappa_value[i], kappa_co_value[i],
     v_on_sigma_value[i], half_abs_z_value[i],
     sigma_v_R_value[i], sigma_v_phi_value[i], sigma_v_z_value[i],
     sigma_v_tot_value[i], mean_v_phi_value[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin=0, rmax=np.amax(rho)+1)

    #r25
    #make mask
    rmin = r_25 / (1+p)
    rmax = r_25 * (1+p)

    (mean_elipticity_value_25[i], thin_disk_fraction_25[i], bulge_fraction_25[i],
     kappa_value_25[i], kappa_co_value_25[i],
     v_on_sigma_value_25[i], half_abs_z_value_25[i],
     sigma_v_R_value_25[i], sigma_v_phi_value_25[i], sigma_v_z_value_25[i],
     sigma_v_tot_value_25[i], mean_v_phi_value_25[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin, rmax)

    #r50
    #make mask
    rmin = r_50 / (1+p)
    rmax = r_50 * (1+p)

    (mean_elipticity_value_50[i], thin_disk_fraction_50[i], bulge_fraction_50[i],
     kappa_value_50[i], kappa_co_value_50[i],
     v_on_sigma_value_50[i], half_abs_z_value_50[i],
     sigma_v_R_value_50[i], sigma_v_phi_value_50[i], sigma_v_z_value_50[i],
     sigma_v_tot_value_50[i], mean_v_phi_value_50[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin, rmax)

    #r75
    #make mask
    rmin = r_75 / (1+p)
    rmax = r_75 * (1+p)

    (mean_elipticity_value_75[i], thin_disk_fraction_75[i], bulge_fraction_75[i],
     kappa_value_75[i], kappa_co_value_75[i],
     v_on_sigma_value_75[i], half_abs_z_value_75[i],
     sigma_v_R_value_75[i], sigma_v_phi_value_75[i], sigma_v_z_value_75[i],
     sigma_v_tot_value_75[i], mean_v_phi_value_75[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin, rmax)

  #put data togather for easy plotting
  data = np.vstack((mean_elipticity_value, thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value, half_abs_z_value,
                    sigma_v_R_value, sigma_v_phi_value, sigma_v_z_value,
                    sigma_v_tot_value, mean_v_phi_value)).T

  data_25=np.vstack((mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25,
                     half_abs_z_value_25,
                     sigma_v_R_value_25, sigma_v_phi_value_25, sigma_v_z_value_25,
                     sigma_v_tot_value_25, mean_v_phi_value_25)).T

  data_50=np.vstack((mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50,
                     half_abs_z_value_50,
                     sigma_v_R_value_50, sigma_v_phi_value_50, sigma_v_z_value_50,
                     sigma_v_tot_value_50, mean_v_phi_value_50)).T

  data_75=np.vstack((mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75,
                     half_abs_z_value_75,
                     sigma_v_R_value_75, sigma_v_phi_value_75, sigma_v_z_value_75,
                     sigma_v_tot_value_75, mean_v_phi_value_75)).T

  #manual corner plot
  # big_data = (data_25,   data_50,   data_75, data,
  #             theory_25, theory_50, theory_75)
  big_data = (data_25,   data_50,   data_75, data)
  # cs = ('C0', 'C1', 'C2', 'k', 'C0', 'C1', 'C2')
  cs = ('C0', 'C1', 'C2', 'k', 'C0', 'C1', 'C2')

  labels=[r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'S/T',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$z_{50}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,4.999), (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(25,25))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        if data_i > 3:
          axs[axis_y-1, axis_x].errorbar(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            ls=':', fmt='', c=cs[data_i], linewidth=3)
        else:
          axs[axis_y-1, axis_x].scatter(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            s=5, c=cs[data_i], alpha=0.8)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend(['R ='+str(r_25)+'kpc', 'R ='+str(r_50)+'kpc',
                   'R ='+str(r_75)+'kpc', 'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  if save:
    fname = '../results/morphological_corner_n_centreals' + str(n_centrals)
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  data = np.vstack((mean_elipticity_value, thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value)).T

  data_25=np.vstack((mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25)).T

  data_50=np.vstack((mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50)).T

  data_75=np.vstack((mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75)).T

  #manual corner plot
  big_data = (data_25, data_50, data_75, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C0', 'C1', 'C2', 'k')

  labels=[r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'S/T',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(13,13))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  #plot points
  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        axs[axis_y-1, axis_x].scatter(
          big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
          s=5, c=cs[data_i], alpha=0.8)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  #line connecting same galaxy
  for axis_y in range(n):
    for axis_x in range(axis_y):
      for i in range(n_centrals):
        data_x = [big_data[0].T[axis_x][i], big_data[1].T[axis_x][i],
                  big_data[2].T[axis_x][i]]
        data_y = [big_data[0].T[axis_y][i], big_data[1].T[axis_y][i],
                  big_data[2].T[axis_y][i]]
        axs[axis_y-1, axis_x].errorbar(data_x, data_y, ls=':', fmt='', c='k', alpha=0.2)

  #labels
  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  #remove numbers on inside graphs
  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend(['R ='+str(r_25)+'kpc', 'R ='+str(r_50)+'kpc',
                   'R ='+str(r_75)+'kpc', 'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  if save:
    fname = '../results/morphological_corner_only_n_centreals' + str(n_centrals)
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_morpholocial_corner_less(data_destination, snap, identity,
                                  n_centrals=20, apature=30, save=True):
  '''
  '''
  r_25 = 4
  r_50 = 7
  r_75 = 11
  p=0.1
  thin_disk_threshold = 0.7

  mean_elipticity_value = np.zeros(n_centrals)
  thin_disk_fraction    = np.zeros(n_centrals)
  bulge_fraction        = np.zeros(n_centrals)
  kappa_value           = np.zeros(n_centrals)
  kappa_co_value        = np.zeros(n_centrals)
  v_on_sigma_value      = np.zeros(n_centrals)
  half_abs_z_value      = np.zeros(n_centrals)

  sigma_v_R_value       = np.zeros(n_centrals)
  sigma_v_phi_value     = np.zeros(n_centrals)
  # sigma_v_tild_value    = np.zeros(n_centrals)
  sigma_v_z_value       = np.zeros(n_centrals)
  sigma_v_tot_value     = np.zeros(n_centrals)
  mean_v_phi_value      = np.zeros(n_centrals)

  mean_elipticity_value_25 = np.zeros(n_centrals)
  thin_disk_fraction_25    = np.zeros(n_centrals)
  bulge_fraction_25        = np.zeros(n_centrals)
  kappa_value_25           = np.zeros(n_centrals)
  kappa_co_value_25        = np.zeros(n_centrals)
  v_on_sigma_value_25      = np.zeros(n_centrals)
  half_abs_z_value_25      = np.zeros(n_centrals)
  sigma_v_R_value_25       = np.zeros(n_centrals)
  sigma_v_phi_value_25     = np.zeros(n_centrals)
  sigma_v_z_value_25       = np.zeros(n_centrals)
  sigma_v_tot_value_25     = np.zeros(n_centrals)
  mean_v_phi_value_25      = np.zeros(n_centrals)

  mean_elipticity_value_50 = np.zeros(n_centrals)
  thin_disk_fraction_50    = np.zeros(n_centrals)
  bulge_fraction_50        = np.zeros(n_centrals)
  kappa_value_50           = np.zeros(n_centrals)
  kappa_co_value_50        = np.zeros(n_centrals)
  v_on_sigma_value_50      = np.zeros(n_centrals)
  sigma_v_R_value_50       = np.zeros(n_centrals)
  sigma_v_phi_value_50     = np.zeros(n_centrals)
  sigma_v_z_value_50       = np.zeros(n_centrals)
  sigma_v_tot_value_50     = np.zeros(n_centrals)
  mean_v_phi_value_50      = np.zeros(n_centrals)
  half_abs_z_value_50      = np.zeros(n_centrals)

  mean_elipticity_value_75 = np.zeros(n_centrals)
  thin_disk_fraction_75    = np.zeros(n_centrals)
  bulge_fraction_75        = np.zeros(n_centrals)
  kappa_value_75           = np.zeros(n_centrals)
  kappa_co_value_75        = np.zeros(n_centrals)
  v_on_sigma_value_75      = np.zeros(n_centrals)
  sigma_v_R_value_75       = np.zeros(n_centrals)
  sigma_v_phi_value_75     = np.zeros(n_centrals)
  sigma_v_z_value_75       = np.zeros(n_centrals)
  sigma_v_tot_value_75     = np.zeros(n_centrals)
  mean_v_phi_value_75      = np.zeros(n_centrals)
  half_abs_z_value_75      = np.zeros(n_centrals)

  for i in range(n_centrals):
    (file_name,
     gas_pos,  gas_vel,  gas_mass,  gas_bind,
     dm_pos,   dm_vel,   dm_mass,   dm_bind,
     star_pos, star_vel, star_mass, star_bind,
     bh_pos,   bh_vel,   bh_mass,   bh_bind,
     j_circ, binding_e, r_bins,
     gas_j_c, dm_j_c, star_j_c, bh_j_c
     ) = load_EAGLE.load_and_calculate(
       data_destination, snap, identity, group=i+1, subgroup=0)

    #some quantities for later
    star_mass_tot = np.sum(star_mass)

    #j_zonc
    star_j_z = np.cross(star_pos, star_vel)[:, 2]
    star_j_zonc = star_j_z / star_j_c

    #cylindrical coords
    (rho, phi, z, v_rho, v_phi, v_z
     ) = halo.get_cylindrical(star_pos, star_vel)

    (mean_elipticity_value[i], thin_disk_fraction[i], bulge_fraction[i],
     kappa_value[i], kappa_co_value[i],
     v_on_sigma_value[i], half_abs_z_value[i],
     sigma_v_R_value[i], sigma_v_phi_value[i], sigma_v_z_value[i],
     sigma_v_tot_value[i], mean_v_phi_value[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin=0, rmax=np.amax(rho)+1)

    #r25
    rmin = 0
    rmax = r_25 * (1+p)

    (mean_elipticity_value_25[i], thin_disk_fraction_25[i], bulge_fraction_25[i],
     kappa_value_25[i], kappa_co_value_25[i],
     v_on_sigma_value_25[i], half_abs_z_value_25[i],
     sigma_v_R_value_25[i], sigma_v_phi_value_25[i], sigma_v_z_value_25[i],
     sigma_v_tot_value_25[i], mean_v_phi_value_25[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin, rmax)

    #r50
    rmin = 0
    rmax = r_50 * (1+p)

    (mean_elipticity_value_50[i], thin_disk_fraction_50[i], bulge_fraction_50[i],
     kappa_value_50[i], kappa_co_value_50[i],
     v_on_sigma_value_50[i], half_abs_z_value_50[i],
     sigma_v_R_value_50[i], sigma_v_phi_value_50[i], sigma_v_z_value_50[i],
     sigma_v_tot_value_50[i], mean_v_phi_value_50[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin, rmax)

    #r75
    rmin = 0
    rmax = r_75 * (1+p)

    (mean_elipticity_value_75[i], thin_disk_fraction_75[i], bulge_fraction_75[i],
     kappa_value_75[i], kappa_co_value_75[i],
     v_on_sigma_value_75[i], half_abs_z_value_75[i],
     sigma_v_R_value_75[i], sigma_v_phi_value_75[i], sigma_v_z_value_75[i],
     sigma_v_tot_value_75[i], mean_v_phi_value_75[i]
      ) = calculate_values(star_mass, rho, z, star_vel, v_rho, v_phi, v_z, star_j_zonc,
                           thin_disk_threshold, rmin, rmax)

  #put data togather for easy plotting
  data = np.vstack((mean_elipticity_value, thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value, half_abs_z_value,
                    sigma_v_R_value, sigma_v_phi_value, sigma_v_z_value,
                    sigma_v_tot_value, mean_v_phi_value)).T

  data_25=np.vstack((mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25,
                     half_abs_z_value_25,
                     sigma_v_R_value_25, sigma_v_phi_value_25, sigma_v_z_value_25,
                     sigma_v_tot_value_25, mean_v_phi_value_25)).T

  data_50=np.vstack((mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50,
                     half_abs_z_value_50,
                     sigma_v_R_value_50, sigma_v_phi_value_50, sigma_v_z_value_50,
                     sigma_v_tot_value_50, mean_v_phi_value_50)).T

  data_75=np.vstack((mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75,
                     half_abs_z_value_75,
                     sigma_v_R_value_75, sigma_v_phi_value_75, sigma_v_z_value_75,
                     sigma_v_tot_value_75, mean_v_phi_value_75)).T

  #manual corner plot
  # big_data = (data_25,   data_50,   data_75, data,
  #             theory_25, theory_50, theory_75)
  big_data = (data_25,   data_50,   data_75, data)
  # cs = ('C0', 'C1', 'C2', 'k', 'C0', 'C1', 'C2')
  cs = ('C0', 'C1', 'C2', 'k', 'C0', 'C1', 'C2')

  labels=[r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'S/T',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$z_{50}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,4.999), (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(25,25))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        if data_i > 3:
          axs[axis_y-1, axis_x].errorbar(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            ls=':', fmt='', c=cs[data_i], linewidth=3)
        else:
          axs[axis_y-1, axis_x].scatter(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            s=5, c=cs[data_i], alpha=0.8)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'R$<$'+str(r_25)+'kpc', r'R$<$'+str(r_50)+'kpc',
                   r'R$<$'+str(r_75)+'kpc', 'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  if save:
    fname = '../results/morphological_corner_less_n_centreals' + str(n_centrals)
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  data = np.vstack((mean_elipticity_value, thin_disk_fraction, bulge_fraction,
                    kappa_value, kappa_co_value, v_on_sigma_value)).T

  data_25=np.vstack((mean_elipticity_value_25,thin_disk_fraction_25,bulge_fraction_25,
                     kappa_value_25, kappa_co_value_25, v_on_sigma_value_25)).T

  data_50=np.vstack((mean_elipticity_value_50,thin_disk_fraction_50,bulge_fraction_50,
                     kappa_value_50, kappa_co_value_50, v_on_sigma_value_50)).T

  data_75=np.vstack((mean_elipticity_value_75,thin_disk_fraction_75,bulge_fraction_75,
                     kappa_value_75, kappa_co_value_75, v_on_sigma_value_75)).T

  #manual corner plot
  big_data = (data_25, data_50, data_75, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C0', 'C1', 'C2', 'k')

  labels=[r'$\bar{\epsilon}$', r'$F(\epsilon > 0.7)$', r'S/T',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(13,13))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        axs[axis_y-1, axis_x].scatter(
          big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
          s=5, c=cs[data_i], alpha=0.8)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  #line connecting same galaxy
  for axis_y in range(n):
    for axis_x in range(axis_y):
      for i in range(n_centrals):
        data_x = [big_data[0].T[axis_x][i], big_data[1].T[axis_x][i],
                  big_data[2].T[axis_x][i]]
        data_y = [big_data[0].T[axis_y][i], big_data[1].T[axis_y][i],
                  big_data[2].T[axis_y][i]]
        axs[axis_y-1, axis_x].errorbar(data_x, data_y, ls=':', fmt='', c='k', alpha=0.2)

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'R$<$'+str(r_25)+'kpc', r'R$<$'+str(r_50)+'kpc',
                   r'R$<$'+str(r_75)+'kpc', 'Global'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  if save:
    fname = '../results/morphological_corner_less_only_n_centreals' + str(n_centrals)
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return


if __name__ == '__main__':

  base             = '/home/matt/Documents/UWA/EAGLE/'
  # data_location    = base + 'L0012N0188/EAGLE_REFERENCE/data/'
  data_destination = base + 'processed_data/'

  identity         = '_12Mpc_188'
  snap             = '028_z000p000'

  plot_morpholocial_corner(data_destination, snap, identity, save=False)

  # plot_morpholocial_corner_less(data_destination, snap, identity, save=True)

  pass
