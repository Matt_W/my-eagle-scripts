#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 10:38:04 2021

@author: matt
"""

import numpy as np

import matplotlib.pyplot as plt

from scipy.linalg import eig

import time
import timeit

#following description in Thob et al. 2019
#(see e.g. Dubinski & Carlberg 1991; Bett 2012; Schneider et al. 2012)

def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
  '''Calculates the reduced inertia tensor
  M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
  Itterative selection is done in the other function.
  '''
  norm = m_p / e2_p

  m = np.zeros((3,3))

  for i in range(3):
    for j in range(3):
      m[i,j] = np.sum(norm * r_p[:, i] * r_p[:, j])

  # m[0, 0] = np.sum(norm * r_p[:, 0] * r_p[:, 0])
  # m[0, 1] = np.sum(norm * r_p[:, 0] * r_p[:, 1])
  # m[0, 2] = np.sum(norm * r_p[:, 0] * r_p[:, 2])
  # m[1, 0] = np.sum(norm * r_p[:, 1] * r_p[:, 0])
  # m[1, 1] = np.sum(norm * r_p[:, 1] * r_p[:, 1])
  # m[1, 2] = np.sum(norm * r_p[:, 1] * r_p[:, 2])
  # m[2, 0] = np.sum(norm * r_p[:, 2] * r_p[:, 0])
  # m[2, 1] = np.sum(norm * r_p[:, 2] * r_p[:, 1])
  # m[2, 2] = np.sum(norm * r_p[:, 2] * r_p[:, 2])

  #this line does not effect result
  m /= np.sum(norm)

  return(m)

def process_tensor(m):
  '''
  '''
  #I think I messed np.linalg.eigh(m) up when I was first writing thins
  if np.any(np.isnan(m)):
    print('Warning: Nans in tensor. Returning identity instead.')
    return(np.ones(3, dtype=np.float64), np.identity(3, dtype=np.float64))

  # (eigan_values, eigan_vectors) = np.linalg.eig(m)
  (eigan_values, eigan_vectors) = np.linalg.eig(m)

  order = np.flip(np.argsort(eigan_values))

  eigan_values  = eigan_values[order]
  eigan_vectors = eigan_vectors[:, order]

  return(eigan_values, eigan_vectors)

def defined_particles(pos, mass, eigan_values, eigan_vectors):
  '''Assumes eigan values are sorted
  '''
  #projection along each axis
  projected_a = (pos[:,0] * eigan_vectors[0,0] + pos[:,1] * eigan_vectors[1,0] +
                 pos[:,2] * eigan_vectors[2,0])
  projected_b = (pos[:,0] * eigan_vectors[0,1] + pos[:,1] * eigan_vectors[1,1] +
                 pos[:,2] * eigan_vectors[2,1])
  projected_c = (pos[:,0] * eigan_vectors[0,2] + pos[:,1] * eigan_vectors[1,2] +
                 pos[:,2] * eigan_vectors[2,2])

  #ellipse distance #Thob et al. 2019 eqn 4.
  ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1]/eigan_values[0]) +
                      np.square(projected_c) / (eigan_values[2]/eigan_values[0]) )

  # #this seems to make almost no difference. 
  # #I'm not convinced that the method in Thob is the correct way to do this
  # #ellipse radius #Thob et al. 2019 eqn 4.
  # ellipse_radius = np.power((eigan_values[1] * eigan_values[2]) / np.square(eigan_values[0]), 1/3
  #                           ) * 900
  # #Thob et al. 2019 eqn 4.
  # inside_mask = ellipse_distance <= ellipse_radius
  # return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])
  
  return(pos, mass, ellipse_distance)

def find_abc(pos, mass, converge_tol=0.01, max_iter=20):
  '''Finds the major, intermediate and minor axes.
  Follows Thob et al. 2019 using reduced quadrupole moment of mass to bias towards
  particles closer to the centre
  '''
  #start off speherical
  r2 = np.square(np.linalg.norm(pos, axis=1))

  #stop problems with r=0
  pos  = pos[r2 != 0]
  mass = mass[r2 != 0]
  r2   = r2[r2 != 0]

  #mass tensor of particles
  m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, r2)

  #linear algebra stuff
  (eigan_values, eigan_vectors) = process_tensor(m)

  #to see convergeance
  cona = eigan_values[2] / eigan_values[0]
  bona = eigan_values[1] / eigan_values[0]

  done = False
  for i in range(max_iter):

    #redefine particles, calculate ellipse distance
    (pos, mass, ellipse_r2) = defined_particles(pos, mass, eigan_values, eigan_vectors)

    #mass tensor of new particles
    m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, ellipse_r2)

    #linear algebra stuff
    (eigan_values, eigan_vectors) = process_tensor(m)

    if (1 - eigan_values[2] / eigan_values[0] / cona < converge_tol*converge_tol) and (
        1 - eigan_values[1] / eigan_values[0] / bona < converge_tol*converge_tol):
      #converged
      done = True
      break

    else:
      cona = eigan_values[2] / eigan_values[0]
      bona = eigan_values[1] / eigan_values[0]

  if not done:
    print('Warning: Shape did not converge.')

  if len(mass) < 100:
    print('Warning: Defining shape with <100 particles.')

  return(np.sqrt(eigan_values).astype(np.float32))

##############################################################################

from scipy.optimize          import brentq

# def get_200_constant(H=None, a=None, z=None, om=[0.307,0.693], h=0.6777, H_0=67.77):
#   '''Function to get constant to calculate R200. Checks box output and cosmology.
#   rho < 200 rho_c(z)
#   M(<R200) / ((4/3)pi R200^3) < 200 (3/(8pi)) H^2 /G
#   M(<R200) / R200^3 < (100 H^2 / G = constant.
#   H in EAGLE units.
#   H_0 in km/s/Mpc
#   '''
#   # G = 4.301e1 #Mpc (km/s)^2 / (10^10 Msun) (source wikipedia)
#   G = 4.301e-6 #kpc (km/s)^2 / Msun (source wikipedia)

#   if H != None and a != None:
#     H *= 3.085678E24 * 1e-8 #to km/s/kpc

#     constant = 100 * np.square(H) / G #Msun / kcp^3

#   if z != None:
#     H_z = np.sqrt( np.square(H_0 * 1e-3) * (om[0] * (1+z)**3 + om[1]) )
#     a   = 1/(1 + z)

#     const_z = 100 * np.square(H_z) / G #Msun / kcp^3

#   if H != None and a != None and z != None:
#     if np.abs(constant - const_z) / constant > 0.001:
#       print('Warning: Box cosmology inconsistant: H:', H, '\t H(z)', H_z)

#   if H == None or a == None:
#     constant = const_z

#   #Will raise an error if neither H nor z is defined

#   return(constant)

def get_200_constant(H=None, a=None, z=None, om=[0.307,0.693], h=0.6777, H_0=67.77):
  '''rho < 200 rho_c(z)
  M(<R200) / ((4/3)pi R200^3) < 200 (3/(8pi)) H^2 /G
  M(<R200) / R200^3 < 100 H^2 / G
  H in EAGLE units.
  H_0 in km/s/Mpc
  '''
  G = 4.301e1 #Mpc (km/s)^2 / (10^10 Msun) (source wikipedia)

  if H != None and a != None:
    H *= 3.085678E24 * 1e-5 #to km/s/Mpc

    constant = 100 * a**3 * np.square(H / h) / G #10^10 Msun / kcp^3

  if z != None:
    H_z = np.sqrt( np.square(H_0) * (om[0] * (1+z)**3 + om[1]) )
    a   = 1/(1 + z)

    const_z = 100 * a**3 * np.square(H_z / h) / G #10^10 Msun * a^3 / (h^2 * Mcp^3)

  if H != None and a != None and z != None:
    if np.abs(constant - const_z) / constant > 0.001:
      print('Warning: Box cosmology inconsistant: H:', H, '\t H(z)', H_z)

  if H == None or a == None:
    constant = const_z

  #Will raise an error if neither H nor z is defined

  return(constant)

def get_crit_200(dm_mass, dm_pos, gas_mass, gas_pos, star_mass, star_pos,
                 constant_200):
  '''Assume pos in h*Mpc/a and mass_dm and mass in 10^10 Msun*h.
  Returns R200 in h*Mpc/a and M200 in 10^10Msun*h
  '''
  #Could also include bh, but should be negligable...
  dm_r   = np.linalg.norm(dm_pos,   axis=1)
  gas_r  = np.linalg.norm(gas_pos,  axis=1)
  star_r = np.linalg.norm(star_pos, axis=1)

  #TODO speed up by log(R200) or something
  #Should be faster than sorting points / using cumsum
  # M / 4/3 r_200^3 = 200 * 3/(8 pi) * H^2/G
  # M = 100 * r_200^3 * H^2 / G)
  zero = lambda R200 : (np.sum(dm_mass[dm_r < R200]) + np.sum(gas_mass[gas_r < R200]) +
    np.sum(star_mass[star_r < R200])) - constant_200 * R200 * R200 * R200

  #this could take a little while with large N
  try:
    R200 = brentq(zero, (np.amin(dm_r)+1e-3), 100 * np.amax(dm_r))
  except ValueError:
    print('Warning : f(a), f(b) signs. Assume halo is less dense than rho_crit so R200=0')
    R200 = 0

  M200 = np.sum(dm_mass[dm_r < R200]) + np.sum(gas_mass[gas_r < R200]) + np.sum(star_mass[star_r < R200])

  # all_r = np.hstack((dm_r,    gas_r,    star_r))
  # all_m = np.hstack((dm_mass, gas_mass, star_mass))

  # #sort by rs
  # args = np.argsort(all_r)

  # all_r = all_r[args]
  # all_m = all_m[args]

  # cum_m = np.cumsum(all_m)
  # #density within each point
  # rho_comp = cum_m / np.power(all_r, 3)

  # #cut off the inner values
  # burn_in = np.sum(all_r <= 1)

  # r200_index = np.nonzero(rho_comp[all_r > 1] - constant_200 < 0)[0]

  # #check if R200 is less than the maximum radius of the group
  # if len(r200_index) > 0:
  #   r200_index = r200_index[0]

  #   #check if the halo ever is above rho crit
  #   if r200_index != 0:
  #     R200 = all_r[r200_index + burn_in]
  #     M200 = cum_m[r200_index + burn_in]

  #   else:
  #     #never got above rho crit
  #     R200 = 0
  #     M200 = 0

  # else:
  #   M200 = cum_m[-1]
  #   R200 = np.power(M200 / constant_200, 1/3)

  return(R200, M200)

##############################################################################
#everything else is for testing
def nfw_m(r, rs=10):
  '''
  '''
  x = r/rs

  # m = np.log(1 + x) - x / (1 + x)
  #hernquist
  m = np.square(x) / np.square(1 + x)

  return(m)

def nfw_r(m, rs=10):
  '''
  '''
  sqm = np.sqrt(m)

  #hernquist
  x = - sqm / (sqm - 1)

  r = x * rs

  return(r)

def spherical_r(r):
  '''
  '''
  n = len(r)
  u = 2*np.random.rand(n, 3) -1
  u = u / np.linalg.norm(u, axis=1)[:, np.newaxis]

  out = u * r[:, np.newaxis]

  return(out)

def mock_nfw(n=10000, cona=0.8, bona=0.8, rs=10, rmax=1000):
  '''Is actually Hernquist. Couldn't work out nfw inverse cumulate mass profile.
  '''
  mmax = 1.01

  mrand = np.random.rand(n)

  rrand = nfw_r(mrand / mmax, rs)

  pos = spherical_r(rrand)

  pos[:, 1] *= bona
  pos[:, 2] *= cona

  return(pos)

if __name__ == '__main__':

  cona = 0.2
  bona = 0.3

  h = 0.6777
  z = 0
  a = 1 / (1 + z)
  pos  = mock_nfw(int(1e6), cona=cona, bona=bona) #* 1e-3 * h / a
  mass = np.ones(len(pos)) #* h

  # random rotation
  from scipy.spatial.transform import Rotation
  random = Rotation.from_euler('yx', [360*np.random.rand(),360*np.random.rand()], degrees=True)
  pos = random.apply(pos)

  number = 10

  print(timeit.timeit( lambda : find_abc(pos, mass), number=number) / number)

  out = find_abc(pos, mass)

  # print(out)
  print(out[2] / out[0], out[1] / out[0])
  print(cona, bona)

  # rs = np.linalg.norm(pos, axis=1)
  #
  # tot_mass = 1e12 #MW mass
  #
  # const = tot_mass / len(mass) * 1e-10
  # print(np.log10(const))
  #
  # import time
  #
  # start_time = time.time()
  #
  # # const_200 = get_200_constant(H=2.1962758605300003E-18, a=a, z=z, om=[0.307,0.693], h=h, H_0=100*h)
  # const_200 = get_200_constant(H=None, a=a, z=z, om=[0.307,0.693], h=h, H_0=100*h) #* a**3
  # for i in range(1):
  #   out = get_crit_200(const * mass, pos, np.zeros(0), np.zeros((0,3)), np.zeros(0), np.zeros((0,3)),
  #                      const_200)
  #
  # print(time.time() - start_time)
  #
  # print(out[0] * a / h * 1e3, out[1] / h)
  # # print(out[0], out[1] * 1e-10)
  #
  # print(const_200)


  pass
