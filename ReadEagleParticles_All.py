import h5py  as h5
import os
import numpy as np

#TODO fix crazy naming scheme
def ReadEagleParticles_All(Base,DirList,fend,exts):
  fn        = Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.0.hdf5'
  print(' __')
  print(' Particles:',Base+DirList+'/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend,' ...')

  with h5.File(fn,"r") as fs:
    RuntimePars = fs['RuntimePars'].attrs
    Header      = fs['Header'].attrs

    HubbleParam     = Header['HubbleParam'] #h = 0.6777
    HubbleExpansion = Header['H(z)']        #H_0 = 67.77 km/s/Mpc but in EAGLE units
    Redshift        = Header['Redshift']
    ExpansionFactor = Header['ExpansionFactor']
    PartMassDM      = Header['MassTable'][1]
    print(Header['MassTable'])
    #problem with mass table for 7x dm run
    if PartMassDM == 0.0:
      PartMassDM = 9.386189841651946e-05 # = 6.570332889156362E-4 / 7
    BoxSize         = Header['BoxSize']

    FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
    NumPartTot  = Header['NumPart_Total']

    om          = [Header['Omega0'],Header['OmegaLambda'],Header['OmegaBaryon']]

  #set up output arrays
  #Gas  [0]
  PosGas        = np.zeros((NumPartTot[0],3),dtype=np.float64)
  # VelGas        = np.zeros((NumPartTot[0],3),dtype=np.float64)
  MassGas       = np.zeros(NumPartTot[0],    dtype=np.float64)
  # IDsGas        = np.zeros(NumPartTot[0],    dtype=np.float64)
  GrpNum_Gas    = np.zeros(NumPartTot[0],    dtype=np.int64)
  SubNum_Gas    = np.zeros(NumPartTot[0],    dtype=np.int64)

  #DM   [1]
  PosDM         = np.zeros((NumPartTot[1],3),dtype=np.float64)
  VelDM         = np.zeros((NumPartTot[1],3),dtype=np.float64)
  # IDsDM         = np.zeros(NumPartTot[1],    dtype=np.float64)
  GrpNum_DM     = np.zeros(NumPartTot[1],    dtype=np.int64)
  SubNum_DM     = np.zeros(NumPartTot[1],    dtype=np.int64)

  #star [4]
  PosStar       = np.zeros((NumPartTot[4],3),dtype=np.float64)
  VelStar       = np.zeros((NumPartTot[4],3),dtype=np.float64)
  MassStar      = np.zeros(NumPartTot[4],    dtype=np.float64)
  # BirthDensity  = np.zeros(NumPartTot[4],    dtype=np.float64)
  # Metallicity   = np.zeros(NumPartTot[4],    dtype=np.float64)
  Star_aform    = np.zeros(NumPartTot[4],    dtype=np.float64)
  # Star_tform    = np.zeros(NumPartTot[4],    dtype=np.float64)
  BindingEnergy = np.zeros(NumPartTot[4],    dtype=np.float64)
  # IDsDM         = np.zeros(NumPartTot[4],    dtype=np.float64)
  GrpNum_Star   = np.zeros(NumPartTot[4],    dtype=np.int64)
  SubNum_Star   = np.zeros(NumPartTot[4],    dtype=np.int64)

  NGas_c  = 0
  NDM_c   = 0
  NStar_c = 0

  for ifile in range(0,FNumPerSnap):
    if os.path.isdir(Base+DirList+'data/'):
      fn = Base+DirList+'data/particledata_'+exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.'+str(ifile)+'.hdf5'
    else:
      fn = Base+DirList+'/particledata_'    +exts+fend+'/'+'eagle_subfind_particles_'+exts+fend+'.'+str(ifile)+'.hdf5'

    #read file
    with h5.File(fn,'r') as fs:
      Header   = fs['Header'].attrs
      NumParts = Header['NumPart_ThisFile']

      #Gas
      if NumParts[0] > 0:
        PosGas[NGas_c:NGas_c+NumParts[0],:]       = fs["PartType0/Coordinates"][()]
        # VelGas[NGas_c:NGas_c+NumParts[0],:]       = fs["PartType0/Velocity"][()]
        MassGas[NGas_c:NGas_c+NumParts[0]]        = fs["PartType0/Mass"][()]
        # IDsGas[NGas_c:NGas_c+NumParts[0]]         = fs["PartType0/ParticleIDs"][()]
        GrpNum_Gas[NGas_c:NGas_c+NumParts[0]]     = fs["PartType0/GroupNumber"][()]
        SubNum_Gas[NGas_c:NGas_c+NumParts[0]]     = fs["PartType0/SubGroupNumber"][()]

      #DM
      if NumParts[1] > 0:
        PosDM[NDM_c:NDM_c+NumParts[1],:]          = fs["PartType1/Coordinates"][()]
        VelDM[NDM_c:NDM_c+NumParts[1],:]          = fs["PartType1/Velocity"][()]
        # IDsDM[NDM_c:NDM_c+NumParts[1]]            = fs["PartType1/ParticleIDs"][()]
        GrpNum_DM[NDM_c:NDM_c+NumParts[1]]        = fs["PartType1/GroupNumber"][()]
        SubNum_DM[NDM_c:NDM_c+NumParts[1]]        = fs["PartType1/SubGroupNumber"][()]

      #Star
      if NumPartTot[4] > 0:
        PosStar[NStar_c:NStar_c+NumParts[4],:]    = fs["PartType4/Coordinates"][()]
        VelStar[NStar_c:NStar_c+NumParts[4],:]    = fs["PartType4/Velocity"][()]#Check this
        MassStar[NStar_c:NStar_c+NumParts[4]]     = fs["PartType4/Mass"][()]#Check this
        # BirthDensity[NStar_c:NStar_c+NumParts[4]] = fs["PartType4/BirthDensity"][()]
        # Metallicity[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/SmoothedMetallicity"][()]
        Star_aform[NStar_c:NStar_c+NumParts[4]]   = fs["PartType4/StellarFormationTime"][()]
        # Star_tform[NStar_c:NStar_c+NumParts[4]]   = lbt(Star_aform[NStar_c:NStar_c+NumParts[4]])
        BindingEnergy[NStar_c:NStar_c+NumParts[4]]= fs["PartType4/ParticleBindingEnergy"][()]
        # ParticleIDs[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/ParticleIDs"][()]
        GrpNum_Star[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/GroupNumber"][()]
        SubNum_Star[NStar_c:NStar_c+NumParts[4]]  = fs["PartType4/SubGroupNumber"][()]

      NGas_c  += NumParts[0]
      NDM_c   += NumParts[1]
      NStar_c += NumParts[4]

  return {#Global properties
          'HubbleParam':     HubbleParam,
          'HubbleExpansion': HubbleExpansion,
          'Redshift':        Redshift,
          'ExpansionFactor': ExpansionFactor,
          'PartMassDM':      PartMassDM,
          'BoxSize':         BoxSize,
          'om':              om,
          #Gas
          'PosGas':     PosGas,
          # 'VelGas':     VelGas,
          'MassGas':    MassGas,
          # 'IDsGas':     IDsGas,
          'GrpNum_Gas': GrpNum_Gas,
          'SubNum_Gas': SubNum_Gas,
          #DM
          'PosDM':     PosDM,
          'VelDM':     VelDM,
          # 'IDsDM':     IDsDM,
          'GrpNum_DM': GrpNum_DM,
          'SubNum_DM': SubNum_DM,
          #Star
          'PosStar':       PosStar,
          'VelStar':       VelStar,
          'MassStar':      MassStar,
          # 'BirthDensity':  BirthDensity,
          # 'Metallicity':   Metallicity,
          'Star_aform':    Star_aform,
          # 'Star_tform':    Star_tform,
          'BindingEnergy': BindingEnergy,
          # 'IDsDM':         IDsDM,
          'GrpNum_Star':   GrpNum_Star,
          'SubNum_Star':   SubNum_Star
          }