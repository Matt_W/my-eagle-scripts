#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

from functools import lru_cache

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d

from scipy.integrate   import quad

from scipy.optimize    import minimize
from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.special import kn, iv
from scipy.special import erf
from scipy.special import lambertw
from scipy.special import spence #aka dilogarithm

from scipy.stats import binned_statistic
import scipy.stats

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

########################################################################################################################
#Ludlow 2016

def concentration_ludlow(z, M200):
    '''Ludlow et al. 2016. Appendix B
    M200 in 10^10 M_sun
    '''
    delta_sc = 1.686

    c_0 = 3.395 * (1 + z)**(-0.215)

    beta = 0.307 * (1 + z)**0.540

    gamma_1 = 0.628 * (1 + z)**(-0.047)

    gamma_2 = 0.317 * (1 + z)**(-0.893)

    omega_l0 = 0.693
    omega_m0 = 0.307
    little_h = 0.6777

    omega_l = omega_l0 / (omega_l0 + omega_m0 * (1 + z)**3)
    omega_m = 1 - omega_l

    psi  = omega_m**(4/7) - omega_l + (1 + omega_m / 2) * (1 + omega_l/70)
    psi0 = omega_m0**(4/7) - omega_l0 + (1 + omega_m0 / 2) * (1 + omega_l0/70)

    a = (1 + z)**(-1.0)

    Dz = omega_m / omega_m0 * psi0 / psi * (1 + z)**(-1.0)

    nu_0 = (4.135 - 0.564 * a**(-1.0) - 0.210 * a**(-2.0) +
          0.0557 * a**(-3.0) - 0.00348 * a**(-4.0)) * Dz**(-1.0)

    #TODO check if don't need little h if units are in 10^10 Msun
    xi = (M200 / little_h)**(-1.0)

    sigma = Dz * 22.26 * xi**0.292 / (1 + 1.53 * xi**0.275 + 3.36 * xi**0.198)

    nu = delta_sc / sigma

    c = c_0 * (nu / nu_0)**(-gamma_1) * (1 + (nu / nu_0)**(1/beta) )**(-beta*(gamma_2 - gamma_1))

    return c

########################################################################################################################

#to get star particle age
def lbt(a=None,omm=0.307,oml=0.693,h=0.6777):
    z = 1.0/a-1
    t = np.zeros(len(z))
    for i in range(len(z)):
        if a[i] == 0:
            t[i] = 13.82968685
        else:
            t[i] = 1e+3 * 3.086e+16 / (3.154e+7 * 1e+9) * (1.0 / (100*h)) * quad(
                lambda z: 1 / ( (1+z) * np.sqrt(omm*(1+z)**3 + oml) ), 0, z[i])[0] #in billion years
    return(t)

########################################################################################################################
#analytic disk and or halo
#all outputs (except mass) are the value AT that radii (as opposed to some cumulative measurement).

def get_analytic_nfw_mass(r_measure, M200, r200, conc):
    #Lokas and Mamon 2001

    s = r_measure / r200

    g_c = 1 / (np.log1p(conc) - conc/(1+conc))

    M_r = M200 * g_c * (np.log1p(conc * s) - conc * s / (1 + conc * s))

    return M_r


def get_analytic_nfw_density(r_measure, M200, conc, r_s, f_bary=0):
    '''
    '''
    # M_h = M200
    M_h = (1-f_bary) * M200

    g = 1 / (np.log1p(conc) - conc / (1 + conc))

    # binney and tremaine eq 2.64 etc.
    rho = g * M_h / (4 * np.pi * r_s**3) / ((r_measure / r_s) * (1 + r_measure / r_s) ** 2)

    return rho


def get_analytic_nfw_circular_velocity(r_measure, r_s, c, v200=200):
    '''
    '''
    xs = r_measure / (r_s * c)

    v_c = 1 / xs * (np.log1p(c * xs) - (c*xs) / (1 + c*xs)) / (np.log1p(c) - c / (1 + c))

    v_c = np.sqrt(v_c) * v200

    return v_c


def dilog_approx(x):
    # lokas & Mamon 2001
    # return (x * np.power(1 + 1/np.sqrt(10) * np.power(-x, 0.62/0.7), -0.7))
    return spence(1 - x)


def get_analytic_nfw_dispersion(r_measure, r_s, c, v200, beta=0):
    '''
    '''
    disp_1d = np.zeros(np.size(r_measure))

    # to fix if the centre is 0
    xs = r_measure / (r_s * c)

    # e.g. Lokas & Mamon 2001
    g = 1 / (np.log1p(c) - c / (1 + c))

    for i, s in enumerate(xs):

        if beta == 0:
            disp_1d[i] = 1/2 * c**2 * g * s * (1 + c*s)**2 * (
                    np.pi**2 - np.log(c*s) - 1 / (c*s) - 1 / (1 + c*s)**2 - 6 / (1 + c*s) +
                    (1 + 1 / (c*s)**2 - 4 / (c*s) - 2 / (1 + c*s)) * np.log1p(c*s) +
                    3 * np.log1p(c*s)**2 + 6 * dilog_approx(- c*s) )

        else:
            #give up, calculate integral. might break as there's no truncation radii...
            integrand = lambda _s: (np.power(_s, 2 * beta - 3) * np.log1p(c * _s) / (1 + c * _s) ** 2 -
                                    c * np.power(_s, 2 * beta - 2) / (1 + c * _s) ** 3)

            disp_1d[i] = (g * (1 + c * s)**2 * np.power(s, 1 - 2 * beta) *
                          quad(integrand, s, np.inf)[0])

    disp_1d = np.sqrt(disp_1d) * v200

    return disp_1d


def get_analytic_nfw_potential(r_measure, r200, conc, V200):
    #Lokas and Mamon 2001

    s = r_measure / r200

    g_c = 1 / (np.log1p(conc) - conc/(1+conc))

    Phi_r = - V200**2 * g_c * np.log1p(conc * s) / s

    #Hernquist
    # GM_h = 4 * r200 / conc * V200 ** 2
    # Phi_r = - GM_h / (r_measure + r200 / conc)

    return Phi_r

####

def get_analytic_exponential_mass(r_measure, Mstar, r12):
    # easy to work out
    # 3/4 from projection
    proj_const = 3/4
    R_d = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    M_r = Mstar * (1 - (r_measure/R_d + 1) * np.exp(-r_measure / R_d))

    return M_r


def get_analytic_spherical_exponential_mass(r_measure, Mstar, r12):
    '''Razor-thin disk has same altitudally smeared mass profile
    '''
    return get_analytic_exponential_mass(r_measure, Mstar, r12)


def get_analytic_exponential_density(R, M_star, r12):
    '''
    '''
    # 3/4 from projection
    proj_const = 3/4
    R_d = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    rho_0 = M_star / (4 * np.pi * R_d ** 2)
    rho_R = rho_0 * np.exp(-R / R_d) / R

    return rho_R


def get_analytic_spherical_exponential_density(r_measure, M_star, r12):
    '''Razor-thin disk has same altitudally smeared density profile
    '''
    return get_analytic_exponential_density(r_measure, M_star, r12)


def get_analytic_exponential_surface_density(R, M_star, r12):
    '''
    '''
    # 3/4 from projection
    proj_const = 3/4
    R_d = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    sigma_0 = M_star / (2 * np.pi * R_d ** 2)
    sigma_R = sigma_0 * np.exp(-R / R_d)

    return sigma_R


def get_analytic_exponential_disk_velocity(r_measure, M_disk, r12):
    '''Calculates the circular velocity for an analytic thin exponential disk
    '''
    # 3/4 from projection
    proj_const = 3/4
    r_disk = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    y = r_measure / (2 * r_disk)

    v_c2 = GRAV_CONST * M_disk * r_measure ** 2 / (2 * r_disk ** 3) * (iv(0, y) * kn(0, y) - iv(1, y) * kn(1, y))

    return np.sqrt(v_c2)


def get_analytic_spherical_exponential_velocity(r_measure, M_disk, r12):
    '''Calculates the circular velocity for an analytic thin exponential disk
    '''
    # 3/4 from projection
    proj_const = 3/4
    r_disk = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    v_c2 = GRAV_CONST * M_disk * (1 - (r_measure/r_disk + 1) * np.exp(-r_measure/r_disk)) / r_measure

    return np.sqrt(v_c2)


#dispersion? needs potential...
def get_analytic_spherical_exponential_dispersion(r_measure, M_disk, r12, beta=0):
    '''
    '''
    # 3/4 from projection
    proj_const = 3/4
    r_disk = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    disp_1d = np.zeros(np.size(r_measure))

    for i, r in enumerate(r_measure):

        if beta == 0:
            integrand = lambda r: (get_analytic_spherical_exponential_density(r, M_disk, r12) *
                                   get_analytic_spherical_exponential_potential_derivative(r, M_disk, r12))
        else:
            integrand = lambda r: (np.power(r, 2*beta) * get_analytic_spherical_exponential_density(r, M_disk, r12) *
                                   get_analytic_spherical_exponential_potential_derivative(r, M_disk, r12))

        disp_1d[i] = quad(integrand, s, np.inf)[0] / (
                np.power(r, 2*beta) * get_analytic_spherical_exponential_density(r, M_disk, r12))

    disp_1d = np.sqrt(disp_1d)

    return disp_1d


def get_analytic_exponential_potential(r_measure, M_star, r12):
    # 3/4 from projection
    proj_const = 3/4
    r_disk = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    y = r_measure / (2 * r_disk)

    Phi = -GRAV_CONST * M_star * r_measure / (2 * r_disk ** 2) * (iv(0, y) * kn(1, y) - iv(1, y) * kn(0, y))

    return Phi


def get_analytic_spherical_exponential_potential(r_measure, M_star, r12):
    # 3/4 from projection
    proj_const = 3/4
    r_d = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    Phi = - M_star * GRAV_CONST / r_measure * (1 - np.exp(-r_measure / r_d))

    return Phi


def get_analytic_spherical_exponential_potential_derivative(r_measure, M_star, r12):
    # 3/4 from projection
    proj_const = 3/4
    r_d = proj_const * r12 / (-lambertw((0.5 - 1) / np.exp(1), -1) - 1).real  # same units as r12 (should be kpc)

    dPhi_dR = M_star * GRAV_CONST / r_measure**2 * (1 - np.exp(-r_measure / r_d) * (1 + r_measure / r_d))

    return dPhi_dR

####

def get_analytic_hernquist_mass(r_measure, Mstar, r12):
    a = (np.sqrt(2) - 1) * r12

    M_r = Mstar * (r_measure / (r_measure + a))**2

    return M_r


def get_analytic_hernquist_density(R, M_star, r12):
    '''
    '''
    a = (np.sqrt(2) - 1) * r12

    rho_0 = M_star / (2 * np.pi)
    rho_R = rho_0 * (a / (R * (a + R)**3))

    return rho_R


def get_analytic_hernquist_velocity(r_measure, M_disk, r12):
    '''Calculates the circular velocity for an analytic thin exponential disk
    '''
    v_c2 = GRAV_CONST * get_analytic_hernquist_mass(r_measure, Mstar, r12) / r_measure

    return np.sqrt(v_c2)


def get_analytic_hernquist_potential(r_measure, M_star, r12):
    a = (np.sqrt(2) - 1) * r12

    Phi = -GRAV_CONST * M_star / (r_measure + a)

    return Phi


def get_analytic_hernquist_potential_derivative(r_measure, M_star, r12):
    a = (np.sqrt(2) - 1) * r12

    dPhi_dR = M_star * GRAV_CONST / (r_measure + a)**2

    return dPhi_dR


########################################################################################################################
#to set up Wilkinson et al. 2022 equations.

def get_density_scale(r_measure, M200, c, r_s, f_bary=0):
    '''
    '''
    out = get_analytic_nfw_density(r_measure, M200, c, r_s, f_bary)

    return out

def get_velocity_scale(r_measure, r_s, c, v200=200, M_disk=0, r_disk=1,
                       analytic_dispersion=False, analytic_v_c=False):
    '''Get a given velocity. The data is not needed for the analytic velocities.
    '''
    if analytic_dispersion:
        #TODO check if other variables cause a problem
        out = get_analytic_nfw_dispersion(r_measure, r_s, c, v200)

    elif analytic_v_c:
        v_halo = get_analytic_nfw_circular_velocity(r_measure, r_s, c, v200)

        #TODO test this...
        # v_disk = get_analytic_exponential_disk_velocity(r_measure, M_disk, r_disk)
        v_disk = get_analytic_spherical_exponential_velocity(r_measure, M_disk, r_disk)

        # r_h = 10**((np.log10(bin_edges[0::2]) + np.log10(bin_edges[1::2]))/2)
        # v_disk = np.sqrt(GRAV_CONST * M_disk * (1 - np.exp(-r_h / r_disk) * (1 + r_h / r_disk)) / r_h)

        out = np.sqrt(v_halo ** 2 + v_disk ** 2)

    else:
        raise KeyError('Did not select a velocity.')

    return out


####
#TODO remove. This should be unused
def get_analytic_nfw_plus_exponential_disk_dispersion(r_measure, M_h, r_s, c, M_d, r_d, beta=0):
    '''Calculated DM velocity dispersion
    '''
    disp_1d = np.zeros(np.size(r_measure))

    g = 1 / (np.log1p(c) - c / (1 + c))
    rho_h = g * M_h / (4*np.pi * r_s**3)

    rho_nfw = lambda r: rho_h / ((r/r_s) * (1 + r/r_s)**2)
    dPhi_dr_nfw = lambda r: GRAV_CONST * g * M_h * ((r/r_s - (1 + r/r_s)*np.log1p(r/r_s)) / (r**2 * (1 + r/r_s)))

    rho_d = M_d / (2 * np.pi * r_d**3)

    rho_exp = lambda r: rho_d * np.exp(-r/r_d)
    dPhi_dr_exp = lambda r: GRAV_CONST * M_d * (np.exp(-r/r_d)*(1 + r/r_d) - 1) / r**2
    # dPhi_dr_exp = lambda r: GRAV_CONST * M_d * (np.exp(-r/r_d)*(1 + r/r_d + r**2/(2 * r_d**2)) - 1) / r**2

    rho = lambda r: rho_nfw(r)
    dPhi_dr = lambda r: dPhi_dr_nfw(r) + dPhi_dr_exp(r)

    integrand = lambda r: -rho(r) * dPhi_dr(r)

    for i in range(len(disp_1d)):

        disp_1d = np.sqrt( 1 / rho(r_measure[i]) * quad(integrand, r_measure[i], 15*r_s*c)[0])

    return disp_1d

###############################################################################
#Wilkinson et al. 2022

def Wilkinson_least_log_squares_best_fit():
    '''Wilkinson et al. 2022
    '''
    return([78.88383 * 2 * np.sqrt(2) * np.pi,
            20.01508 * np.sqrt(2) * np.pi,
            20.39694 * np.sqrt(2) * np.pi,
            9.65423 * np.sqrt(2) * np.pi],
           [-0.46909, -0.31016, -0.19881, -0.1256],
           [0.0, 0.0, 0.0, 0.0],
           [0.0, 0.0, 0.0, 0.0])

def get_tau_array(time_array, time_scale_array):
    # e.g. Wilkinson et al. eqn. 10
    tau_array = time_array / time_scale_array

    return (tau_array)


def get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                       ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    # e.g. Wilkinson et al. eqn. 9, 13
    tau_heat_array = np.power((ln_Lambda_k_i * np.power(scale_density_ratio_array, alpha_i) *
                               np.power(scale_velocity_ratio_array, -beta_i) / scale_velocity_ratio_array**2), -1.0 / (1 + gamma_i))

    return (tau_heat_array)


def get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array):

    sigma_dm_2 = velocity_scale2_array * scale_velocity_ratio_array**2
    # e.g. Wilkinson et al. eqn. 11, 14
    tau_0_on_tau_heat = np.log(
        sigma_dm_2 / (sigma_dm_2 - inital_velocity2_array))

    return (tau_0_on_tau_heat)


def get_theory_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                               time_array, time_scale_array,
                               scale_density_ratio_array, scale_velocity_ratio_array,
                               ln_Lambda_k_i, alpha_i, beta_i, gamma_i):

    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i)

    tau_0_on_tau_heat = get_tau_zero_array(velocity_scale2_array, inital_velocity2_array, scale_velocity_ratio_array)

    # e.g. Wilkinson et al. eqn. 8, 12
    theory_velocity2_array = velocity_scale2_array * scale_velocity_ratio_array**2 * (1 - np.exp(
        - np.power(((time_array / time_scale_array) / tau_heat_array) +
                   tau_0_on_tau_heat, 1 + gamma_i)))

    return (theory_velocity2_array)


def get_powerlaw_velocity2_array(velocity_scale2_array, inital_velocity2_array,
                                 time_array, time_scale_array,
                                 scale_density_ratio_array, scale_velocity_ratio_array,
                                 ln_Lambda_k_i, alpha_i, beta_i, gamma_i):
    tau_heat_array = get_tau_heat_array(scale_density_ratio_array, scale_velocity_ratio_array,
                                        ln_Lambda_k_i, alpha_i, beta_i, gamma_i) / scale_velocity_ratio_array**2

    # e.g. Wilkinson et al. eqn. 7
    theory_velocity2_array = (velocity_scale2_array - inital_velocity2_array) * (
        np.power(((time_array / time_scale_array) / tau_heat_array),
                 1 + gamma_i)) + inital_velocity2_array

    return (theory_velocity2_array)

###############################################################################

# # disk heights see e.g. Benitez-Llambey et al. 2018, Ludlow et al 2021
# def get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
#     # Ludlow et al. 2021 Equation (11)
#     h_SG = sigma_z ** 2 / (np.pi * GRAV_CONST * Sigma_R_star)
#
#     # #Benitez-Llambey et al. 2018
#     # z_SG = 0.55 * h_SG
#     # me
#     z_SG = np.log(3) / 2 * h_SG
#
#     return (z_SG)
#
#
# def get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
#     # Ludlow et al. 2021 Equation (10)
#     h_NSG = np.sqrt(2) * sigma_z * R / v_c_halo
#
#     # #Benitez-Llambey et al. 2018
#     # z_NSG = 0.477 * h_NSG
#     # me
#     z_NSG = erfinv(0.5) * h_NSG
#
#     return (z_NSG)
#
#
# def get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
#     z_SG = get_thin_disk_SG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)
#
#     z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)
#
#     # Ludlow et al. 2021 Equation (18)
#     z_thin_half = (1 / z_SG ** 2 + 1 / (2 * z_SG * z_NSG) + 1 / z_NSG ** 2) ** (-1 / 2)
#
#     return (z_thin_half)
#
#
# def get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
#     # Ludlow et al. 2021 Equation (20)
#     nu_2inv = v200 * v200 / (sigma_z * sigma_z)
#     rho = lambda z, nu_2inv, R, r200: np.exp(nu_2inv * (1 / (np.sqrt((R / r200) ** 2 + (z / r200) ** 2) + a / r200)
#                                                         - 1 / (R / r200 + a / r200)))
#
#     tot = quad(rho, 0, 200, args=(nu_2inv, R, r200))[0]
#
#     half = lambda z_half, nu_2inv, R, r200: quad(rho, 0, z_half, args=(nu_2inv, R, r200))[0] - tot / 2
#
#     out = brentq(half, 0, 200, args=(nu_2inv, R, r200))
#
#     return (out)
#
#
# def get_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a):
#     z_thin_half = get_thin_disk_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)
#
#     z_thick_NSG = get_thick_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)
#
#     z_NSG = get_thin_disk_NSG_height(sigma_z, R, Sigma_R_star, v_c_halo, v200, r200, a)
#
#     # Ludlow et al. 2021 Equation (21)
#     z_thick_half = z_thin_half + (z_thick_NSG - z_NSG)
#
#     return (z_thick_half)

########################################################################################################################

''' (fitted on 20/3/23)
z=0: 7x, 1x
[ 0.01805803  1.03014287  0.39003484 11.83089355]
[ 0.0178969   0.98558489  0.36901893 11.85473776]
z=0.5: 7x, 1x
[ 0.01677972  0.96921464  0.46347807 11.85997894]
[ 0.01726748  0.92220301  0.45158719 11.90659349]
z=1: 7x, 1x
[ 0.01555471  0.84337933  0.553132   11.99164799]
[ 0.01597337  0.83218904  0.54990353 12.00853445]
z=2: 7x, 1x
[ 0.01323591  0.72708704  0.66739143 12.23070176]
[ 0.01324826  0.69753135  0.68506589 12.23213424]
'''

def mass_function_args(z=0, hr=True):
    if z == 2.012 or z == 2:
        # if hr:
        return [ 0.01323591, 0.72708704, 0.66739143, 12.23070176]
        # else: return [ 0.01324826, 0.69753135, 0.68506589, 12.23213424]
    elif z == 1.004 or z == 1:
        # if hr:
        return [ 0.01555471, 0.84337933, 0.553132,   11.99164799]
        # else: return [ 0.01597337, 0.83218904, 0.54990353, 12.00853445]
    elif z == 0.503 or z == 0.5:
        # if hr:
        return [ 0.01677972, 0.96921464, 0.46347807, 11.85997894]
        # else: return [ 0.01726748, 0.92220301, 0.45158719, 11.90659349]
    else:
        # if hr:
        return [ 0.01805803, 1.03014287, 0.39003484, 11.83089355]
        # else: return [ 0.0178969,  0.98558489, 0.36901893, 11.85473776]

def mass_function(args, x):
    # x is log(M_200 / M_sun)
    return np.log10(2 * np.power(10, x) * args[0] / (
            np.power(np.power(10, x - args[3]), -args[1]) + np.power(np.power(10, x - args[3]), args[2])))

''' (fitted on 5/4/23)
z=0: 7x, 1x
[1.04716, 0.38767, 0.35636, 11.781]
[1.04716, 0.04232, 0.35636, 11.781]
z=0.5: 7x, 1x
[0.94253, 0.47340, 0.291180, 11.1576]
[0.94253, 0.06310, 0.291180, 11.1576]
z=1: 7x, 1x
[0.79859, 0.65058, 0.20914, 10.649]
[0.79859, 0.14679, 0.20914, 10.649]
z=2: 7x, 1x
[0.51521, 0.60397, 0, 10.938]
[0.51521, 0.39764, 0, 10.938]
'''

def size_function_args(z=0., hr=True):
    if z == 2.012 or z == 2:
        if hr:
            return [0.51521, 0.60397, 0, 10.938]
        else: return [0.51521, 0.39764, 0, 10.938]
    elif z == 1.004 or z == 1:
        if hr:
            return [0.79859, 0.65058, 0.20914, 10.649]
        else: return [0.79859, 0.14679, 0.20914, 10.649]
    elif z == 0.503 or z == 0.5:
        if hr:
            return [0.94253, 0.47340, 0.291180, 11.1576]
        else: return [0.94253, 0.06310, 0.291180, 11.1576]
    else:
        if hr:
            return [1.04716, 0.38767, 0.35636, 11.781]
        else: return [1.04716, 0.04232, 0.35636, 11.781]


def size_function(args, x):
    '''e.g. Shen et al. 2003 eqn. (18)
    x is log(M_200 / M_sun)
    '''
    # return np.log10(args[0] * np.power(np.power(10, x), args[1]) *
    #                 np.power( 1 + np.power(10, x) / np.power(10, args[3]), args[2] - args[1]))
    return (args[0] + args[1] * x  + (args[2] - args[1]) * np.log10(1 + np.power(10, x - args[3]))
            - args[1] * 13 - (args[2] - args[1]) * np.log10(1 + np.power(10, 13 - args[3])))

########################################################################################################################
# Heating equaitons from Ludlow et al. 2021 and Wilkinson et al. 2022

def find_contracted_radii(r_DMO, M200, r200, conc, Mstar, r12, accuracy=1e-4, profile='exp'):
    '''Following abadi et al. 2010
    I ignore gas mass / gas profile.
    '''
    a = 0.3
    n = 2

    #following Velani & Paranjape 2023
    q0 = -0.05
    q10 = 1.1
    q11 = 0.5

    m_DM_DMO = get_analytic_nfw_mass(r_DMO, M200, r200, conc)
    m_DMO = m_DM_DMO

    #Reduce Hydro DM mass by baryon fraction
    m_DM_hydro = (1 - Mstar / M200) * m_DMO

    def equation(r_hydro):
        '''Find contracted radii that makes Abadi et al. equation 3 true
        '''
        if profile == 'exp':
            m_star_hydro = get_analytic_exponential_mass(r_hydro, Mstar, r12)
        elif profile == 'hern':
            m_star_hydro = get_analytic_hernquist_mass(r_hydro, Mstar, r12)

        m_hydro = m_DM_hydro + m_star_hydro

        # e.g. Abadi et al. 2010 equation 4
        zero = 1 + a * (np.power( m_DMO / m_hydro, n) - 1) - r_hydro/r_DMO

        # # e.g. Velmani & Paranjape 2023 equation 12
        # zero = (q10 + q11 * np.log10(r_hydro / r200)) * (m_DMO / m_hydro - 1) + q0 + 1 - r_hydro / r_DMO

        return zero

    try:
        r_hydro = brentq(equation, 0.1*r_DMO, 10*r_DMO, xtol=accuracy, rtol=accuracy, maxiter=100)
    except RuntimeError as e:
        print('Error: Did not find correct contracted radii. Inputs are: r_DMO, M200, r200, conc, Mstar, r12')
        print(r_DMO, M200, r200, conc, Mstar, r12)
        raise(e)

    #return (r, M(<r)) pair
    return r_hydro, m_DM_hydro


#TODO improve integrals and derivitives
@lru_cache(maxsize=1_000_000)
def contruct_contracted_mass_profile(M200, r200, conc, Mstar, r12, n_evals=51, log_r200_min=-3, log_r200_max=-1,
                                     disk_disp=True, halo_disp=True, fix_central_pot_norm=False, beta=0, profile='exp'):
    '''Calculates several profiles for "adiabatiaclly" contracted profile.
    Masses and distances need to be in the same units. Which units should be arbitrary.
    '''

    r_DMOs = np.logspace(log_r200_min, log_r200_max, n_evals) * r200 #kpc

    r_hydro = np.zeros(n_evals)
    M_dm_hydro = np.zeros(n_evals)
    rho_dm_hydro = np.zeros(n_evals)
    Phi_dm_hydro = np.zeros(n_evals)
    d_Phi_dr_hydro = np.zeros(n_evals)
    sigma_1D_DM_hydro = np.zeros(n_evals)
    sigma_1D_star_hydro = np.zeros(n_evals)

    #the slow step
    for i, r_DMO in enumerate(r_DMOs):
        r_hydro[i], M_dm_hydro[i] = find_contracted_radii(r_DMO, M200, r200, conc, Mstar, r12, profile=profile)

    #demerucal derive mass profile
    rho_dm_hydro[1:-1] = (M_dm_hydro[2:] - M_dm_hydro[:-2]) / (4 / 3 * np.pi * (r_hydro[2:] ** 3 - r_hydro[:-2] ** 3))

    #fix ends
    # rho_dm_hydro[-1] = get_analytic_nfw_density(r_hydro[-1], M200, conc, r200 / conc)
    # rho_dm_hydro[0]  = get_analytic_nfw_density(r_hydro[0], M200, conc, r200 / conc)
    rho_dm_hydro[-1] = rho_dm_hydro[-2]
    rho_dm_hydro[0]  = rho_dm_hydro[1]

    #numerical integral
    for i in range(n_evals-2):
        Phi_dm_hydro[i+1] += - 4 * np.pi * GRAV_CONST * (1/r_hydro[i+1] *
            np.sum( np.diff(r_hydro[:i+1]) * (r_hydro[:i]**2 * rho_dm_hydro[:i] + r_hydro[1:i+1]**2 * rho_dm_hydro[1:i+1])/2) +
            np.sum( np.diff(r_hydro[i:]) * (r_hydro[i:-1] * rho_dm_hydro[i:-1] + r_hydro[i+1:] * rho_dm_hydro[i+1:])/2))

    # if fix_central_pot_norm:
    #     #correct the centre #this part seems a bit dodgey
    #     g_c = 1 / (np.log1p(conc) - conc / (1 + conc))
    #     v200 = np.sqrt(GRAV_CONST * M200 / r200)
    #     Phi_0_min_r = - g_c * conc * v200**2
    #
    #     Phi_dm_hydro += Phi_0_min_r - Phi_dm_hydro[1]

    # Phi_dm_hydro[-1] = get_analytic_nfw_potential(r_hydro[-1], r200, conc, v200)
    # Phi_dm_hydro[0] = get_analytic_nfw_potential(r_hydro[0], r200, conc, v200)
    Phi_dm_hydro[-1] = Phi_dm_hydro[-2]
    Phi_dm_hydro[0]  = Phi_dm_hydro[1]

    #for solving Jean's equation
    #numerical derivitive
    d_Phi_dr_hydro[1:-1] = (Phi_dm_hydro[2:] - Phi_dm_hydro[:-2]) / (r_hydro[2:] - r_hydro[:-2])
    if profile == 'exp':
        d_Phi_dr_hydro[1:-1] += get_analytic_spherical_exponential_potential_derivative(r_hydro[1:-1], Mstar, r12)
    elif profile == 'hern':
        d_Phi_dr_hydro[1:-1] += get_analytic_hernquist_potential_derivative(r_hydro[1:-1], Mstar, r12)

    d_Phi_dr_hydro[-1] = d_Phi_dr_hydro[-2]
    d_Phi_dr_hydro[0]  = d_Phi_dr_hydro[1]

    if halo_disp:
        #TODO do the integral with beta!=0
        #numerical integral
        for i in range(n_evals-1):
            sigma_1D_DM_hydro[i] = 1 / rho_dm_hydro[i] * np.sum(np.diff(r_hydro[i:]) *
                                                          (rho_dm_hydro[i:-1] * d_Phi_dr_hydro[i:-1] +
                                                           rho_dm_hydro[i+1:] * d_Phi_dr_hydro[i+1:])/2)

        sigma_1D_DM_hydro = np.sqrt(sigma_1D_DM_hydro)

    if disk_disp:
        if profile == 'exp':
            rho_star_hydro = get_analytic_exponential_density(r_hydro, Mstar, r12)
        elif profile == 'hern':
            rho_star_hydro = get_analytic_hernquist_density(r_hydro, Mstar, r12)

        #numerical integral
        for i in range(n_evals-1):
            sigma_1D_star_hydro[i] = 1 / rho_star_hydro[i] * np.sum(np.diff(r_hydro[i:]) *
                                                            (rho_star_hydro[i:-1] * d_Phi_dr_hydro[i:-1] +
                                                             rho_star_hydro[i+1:] * d_Phi_dr_hydro[i+1:])/2)

        sigma_1D_star_hydro = np.sqrt(sigma_1D_star_hydro)

    return r_hydro, M_dm_hydro, rho_dm_hydro, Phi_dm_hydro, sigma_1D_DM_hydro, sigma_1D_star_hydro


def get_heating_at_r12(M200, MassDM=DM_MASS, z=0., time=10., ic_heat_fraction=0.,
                       contracted=True, hr_size=True, Mstar=None, r12=None):
    '''Get all the scaled quantities.
    M200 in 10^10 M_sun
    time in Gyr
    '''
    if np.isclose(MassDM, DM_MASS): hr = False
    else: hr = True

    #cosmology
    omm = 0.307
    oml = 0.693
    a = 1 / (z + 1)
    H_z2 = HUBBLE_CONST**2 * (oml + omm * np.power(a, -3))
    rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST) #10^10 M_sun / kpc^3

    #NFW halo
    conc = concentration_ludlow(z, M200)

    g = 1 #1 / (np.log1p(conc) - conc / (1 + conc))
    rho_200 = 200 * rho_crit

    # #hard code value form ideal study
    # rho_200 = 5.550625776280936e-06

    r200 = np.power(3 * M200 / (4 * np.pi * rho_200), 1/3) #kpc

    v200 = np.sqrt(GRAV_CONST * M200 / r200) #km/s
    rs = r200 / conc

    if r12 == None:
        r12 = 10**size_function(size_function_args(z, hr_size), np.log10(M200) + 10) #kpc
        # r12 = 10**size_function(size_function_args(z, True), np.log10(M200) + 10) #kpc

    if Mstar == None:
        Mstar = 10**(mass_function(mass_function_args(z, hr), np.log10(M200) + 10) - 10) #10^10 M_sun

    if contracted:
        (r_hydro, m_dm_hydro, rho_dm_hydro, Phi_hydro, sigma_1D_dm_hydro, sigma_1D_star_hydro
         ) = contruct_contracted_mass_profile(
            M200, r200, conc, Mstar, r12,
            log_r200_min=np.log10(r12/r200)-2, log_r200_max=np.log10(r12/r200)+2, n_evals=401)

        m_dm_interp = interp1d(r_hydro, m_dm_hydro)
        rho_dm_interp = interp1d(r_hydro, rho_dm_hydro)
        # Phi_interp = interp1d(r_hydro, M_hydro)
        sigma_1D_dm_interp = interp1d(r_hydro, sigma_1D_dm_hydro)
        sigma_1D_star_interp = interp1d(r_hydro, sigma_1D_star_hydro)

        rho_dm = rho_dm_interp(r12)
        # #TODO remove testing
        # rho_dm = get_density_scale(r12, M200, conc, rs, f_bary=0)

        analytic_dispersion = sigma_1D_dm_interp(r12)
        stellar_dispersion = sigma_1D_star_interp(r12)

        analytic_v_circ = np.sqrt(GRAV_CONST * (m_dm_interp(r12) + Mstar/2) / r12)

    else:
        rho_dm = get_density_scale(r12, M200, conc, rs, f_bary=0)

        analytic_dispersion = get_velocity_scale(np.array([r12]), rs, conc, v200, 0, r12,
                                                 analytic_dispersion=True)[0]
        stellar_dispersion = 0
        analytic_v_circ = get_velocity_scale(np.array([r12]), rs, conc, v200, 0, r12,
                                             analytic_v_c=True)[0]

    #calc dimensionless properties
    t_c_dm = v200 ** 3 / (GRAV_CONST**2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M # 1/Gyr

    delta_dm = rho_dm / rho_200
    upsilon_dm = analytic_dispersion / v200
    upsilon_circ = analytic_v_circ / v200

    #ics
    inital_velocity_v = np.sqrt((1 - ic_heat_fraction**2) * analytic_v_circ**2)
    inital_velocity2_z = ic_heat_fraction**2 * (analytic_dispersion)**2
    inital_velocity2_r = ic_heat_fraction**2 * (analytic_dispersion)**2
    inital_velocity2_p = ic_heat_fraction**2 * (analytic_dispersion)**2

    # get my values
    (ln_Lambda_ks, alphas, betas, gammas
     ) = Wilkinson_least_log_squares_best_fit()

    # plug into my equations
    theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ - inital_velocity_v,
                                                time, t_c_dm, delta_dm, np.sqrt(upsilon_circ),
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    theory_best_v = analytic_v_circ - theory_best_v_

    theory_best_z2 = get_theory_velocity2_array(v200**2, inital_velocity2_z,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    theory_best_r2 = get_theory_velocity2_array(v200**2, inital_velocity2_r,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    theory_best_p2 = get_theory_velocity2_array(v200**2, inital_velocity2_p,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    #power law
    power_law_z2 = get_powerlaw_velocity2_array(v200**2, inital_velocity2_z,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    power_law_r2 = get_powerlaw_velocity2_array(v200**2, inital_velocity2_r,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    power_law_p2 = get_powerlaw_velocity2_array(v200**2, inital_velocity2_p,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    theory_best_z = np.sqrt(theory_best_z2)
    theory_best_r = np.sqrt(theory_best_r2)
    theory_best_p = np.sqrt(theory_best_p2)

    power_law_z = np.sqrt(power_law_z2)
    power_law_r = np.sqrt(power_law_r2)
    power_law_p = np.sqrt(power_law_p2)

    #TODO implement this:
    # tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
    #                                  ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
    # # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
    # tau_heat_experiment = (tau_heat_p2 * 0.74987599)
    #
    # tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ - inital_velocity_v,
    #                                        np.sqrt(upsilon_circ))
    #
    # experiment_v_ = v200 * np.sqrt(upsilon_circ) ** 2 * (1 - np.exp(
    #     - np.power(((time / t_c_dm) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))
    #
    # theory_best_v = analytic_v_circ - experiment_v_

    return(analytic_v_circ, analytic_dispersion,
           theory_best_v, theory_best_z, theory_best_r, theory_best_p,
           stellar_dispersion, power_law_z, power_law_r, power_law_p)


def get_heated_profile(radii, M200, MassDM=DM_MASS, z=0., time=10., ic_heat_fraction=0., ic_heat_const=0.,
                       contracted=True, Mstar=None, r12=None, profile='exp', hr_size=True):
    '''Get all the scaled quantities.
    M200 in 10^10 M_sun
    time in Gyr
    ic_heat_const in km/s
    '''
    if np.isclose(MassDM, DM_MASS): hr = False
    else: hr = True

    #cosmology
    omm = 0.307
    oml = 0.693
    a = 1 / (z + 1)
    H_z2 = HUBBLE_CONST**2 * (oml + omm * np.power(a, -3))
    rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST) #10^10 M_sun / kpc^3

    #NFW halo
    conc = concentration_ludlow(z, M200)

    g = 1 #1 / (np.log1p(conc) - conc / (1 + conc))
    rho_200 = 200 * rho_crit

    r200 = np.power(3 * M200 / (4 * np.pi * rho_200 * g), 1/3) #kpc

    v200 = np.sqrt(GRAV_CONST * M200 / r200) #km/s
    rs = r200 / conc

    if r12 == None:
        r12 = 10**size_function(size_function_args(z, hr_size), np.log10(M200) + 10) #kpc
        # r12 = 10**size_function(size_function_args(z, True), np.log10(M200) + 10) #kpc

    # half = 0.5
    # R_d = r12 / (-lambertw((half - 1) / np.exp(1), -1) - 1).real * (3/4) #kpc

    if Mstar == None:
        Mstar = 10**(mass_function(mass_function_args(z, hr), np.log10(M200) + 10) - 10) #10^10 M_sun

    # print(round(np.log10(Mstar)+10, 3), round(r12,3))

    #calc profiles
    if contracted:
        #TODO
        (r_hydro, m_dm_hydro, rho_dm_hydro, Phi_hydro, sigma_1D_dm_hydro, sigma_1D_star_hydro
         ) = contruct_contracted_mass_profile(M200, r200, conc, Mstar, r12, n_evals=401,
            log_r200_min=np.log10(np.amin(radii)/r200)-0.5, log_r200_max=np.log10(np.amax(radii)/r200)+0.5,
                                              profile=profile)

        m_dm_interp = interp1d(r_hydro, m_dm_hydro)
        rho_dm_interp = interp1d(r_hydro, rho_dm_hydro)
        # Phi_interp = interp1d(r_hydro, M_hydro)
        sigma_1D_dm_interp = interp1d(r_hydro, sigma_1D_dm_hydro)
        sigma_1D_star_interp = interp1d(r_hydro, sigma_1D_star_hydro)

        rho_dm = rho_dm_interp(radii)

        analytic_dispersion = sigma_1D_dm_interp(radii)
        stellar_dispersion = sigma_1D_star_interp(radii)

        if profile == 'exp':
            m_tot = m_dm_interp(radii) + get_analytic_exponential_mass(radii, Mstar, r12)
        elif profile == 'hern':
            m_tot = m_dm_interp(radii) + get_analytic_hernquist_mass(radii, Mstar, r12)
        analytic_v_circ = np.sqrt(GRAV_CONST * m_tot / radii)

    else:
        rho_dm = get_density_scale(radii, M200, conc, rs, f_bary= 1 - Mstar / M200)

        analytic_dispersion = get_velocity_scale(radii, rs, conc, v200, 0, r12,
                                                 analytic_dispersion=True)
        stellar_dispersion = np.zeros(len(analytic_dispersion))

        analytic_v_circ = get_velocity_scale(radii, rs, conc, v200, 0, r12,
                                             analytic_v_c=True)

    if profile == 'exp':
        m_star = get_analytic_exponential_mass(radii, Mstar, r12)
    elif profile == 'hern':
        m_star = get_analytic_hernquist_mass(radii, Mstar, r12)
    v_c_star = np.sqrt(GRAV_CONST * m_star / radii)

    #calc dimensionless properties
    t_c_dm = v200 ** 3 / (GRAV_CONST**2 * rho_dm * MassDM) * GYR_ON_S / PC_ON_M  # 1/Gyr

    delta_dm = rho_dm / rho_200
    upsilon_dm = analytic_dispersion / v200
    upsilon_circ = analytic_v_circ / v200

    #ics
    # inital_velocity_v = np.sqrt((1 - ic_heat_fraction**2) * analytic_v_circ**2)
    # inital_velocity2_z = ic_heat_fraction**2 * (analytic_dispersion)**2
    # inital_velocity2_r = ic_heat_fraction**2 * (analytic_dispersion)**2
    # inital_velocity2_p = ic_heat_fraction**2 * (analytic_dispersion)**2

    inital_velocity_v = analytic_v_circ
    inital_velocity2_z = ic_heat_const**2
    inital_velocity2_r = ic_heat_const**2
    inital_velocity2_p = ic_heat_const**2

    # get my values
    (ln_Lambda_ks, alphas, betas, gammas
     ) = Wilkinson_least_log_squares_best_fit()

    # plug into my equations
    theory_best_v_ = get_theory_velocity2_array(v200, analytic_v_circ - inital_velocity_v,
                                                time, t_c_dm, delta_dm, np.sqrt(upsilon_circ),
                                                ln_Lambda_ks[0], alphas[0], betas[0], gammas[0])
    theory_best_v = analytic_v_circ - theory_best_v_ + 1e-14

    theory_best_z2 = get_theory_velocity2_array(v200**2, inital_velocity2_z,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[1], alphas[1], betas[1], gammas[1])
    theory_best_r2 = get_theory_velocity2_array(v200**2, inital_velocity2_r,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    theory_best_p2 = get_theory_velocity2_array(v200**2, inital_velocity2_p,
                                                time, t_c_dm, delta_dm, upsilon_dm,
                                                ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])

    theory_best_z = np.sqrt(theory_best_z2)
    theory_best_r = np.sqrt(theory_best_r2)
    theory_best_p = np.sqrt(theory_best_p2)

    # #
    # tau_heat_r2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
    #                                  ln_Lambda_ks[2], alphas[2], betas[2], gammas[2])
    # tau_heat_experiment = (tau_heat_r2 * 1.06126839)
    # tau_heat_p2 = get_tau_heat_array(delta_dm[0], upsilon_dm[0],
    #                                  ln_Lambda_ks[3], alphas[3], betas[3], gammas[3])
    # tau_heat_experiment = (tau_heat_p2 * 0.74987599)
    #
    # tau_0_on_tau_heat = get_tau_zero_array(v200, analytic_v_circ - inital_velocity_v,
    #                                        np.sqrt(upsilon_circ))
    #
    # experiment_v_ = v200 * np.sqrt(upsilon_circ) ** 2 * (1 - np.exp(
    #     - np.power(((time / t_c_dm) / tau_heat_experiment) + tau_0_on_tau_heat, 1 + gammas[0])))
    #
    # theory_best_v = analytic_v_circ - experiment_v_ + 1e-14

    return (analytic_v_circ, analytic_dispersion,
            theory_best_v, theory_best_z, theory_best_r, theory_best_p,
            stellar_dispersion, v_c_star)



if __name__ == '__main__':
    pass