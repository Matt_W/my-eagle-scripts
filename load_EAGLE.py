#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 14:47:10 2020

@author: matt
"""
import numpy as np
import h5py as h5

import halo_calculations as halo

LITTLE_H = 0.6777
#box size should be saved and read in
BOX_SIZE = 8.47125
SCALE_A  = 1

def load_snapshot(data_destination, snap, identity, group=1, subgroup=0,
                  subgroup_only=True):
  '''
  '''

  file_name = '%s%s/%s.%d.hdf5'%(data_destination, snap, identity, group)

  group = h5.File(file_name,'r')

  #COP
  # centre_of_potential = group['SubGroup/Coordinates'][()][subgroup]
  c_o_p = group['SubGroup/Coordinates'][()][subgroup]

  #surley there is a better way of masking ...
  #gas
  gas_subgroup = group['PartType0/SubGroupNumber'][()]
  if subgroup_only: gas_mask = (gas_subgroup == subgroup)
  else:             gas_mask = np.ones(len(gas_subgroup), dtype=bool)

  gas_pos  = group['PartType0/Coordinates'][()][gas_mask] - c_o_p - BOX_SIZE/2
  gas_pos  %= BOX_SIZE
  gas_pos  -= BOX_SIZE/2
  gas_pos  *= 1000 * SCALE_A / LITTLE_H #kpc
  gas_vel  = group['PartType0/Velocity'][()][gas_mask] * np.sqrt(SCALE_A) #km/s
  gas_mass = group['PartType0/Mass'][()][gas_mask] / LITTLE_H #10^10 M_sun
  gas_bind = group['PartType0/ParticleBindingEnergy'][()][gas_mask] * SCALE_A #(km/s)^2

  #dm
  dm_subgroup = group['PartType1/SubGroupNumber'][()]
  if subgroup_only: dm_mask = (dm_subgroup == subgroup)
  else:             dm_mask = np.ones(len(dm_subgroup), dtype=bool)

  dm_pos  = group['PartType1/Coordinates'][()][dm_mask] - c_o_p - BOX_SIZE/2
  dm_pos  %= BOX_SIZE
  dm_pos  -= BOX_SIZE/2
  dm_pos  *= 1000 * SCALE_A / LITTLE_H #kpc
  dm_vel  = group['PartType1/Velocity'][()][dm_mask] * np.sqrt(SCALE_A) #km/s
  dm_mass = group['PartType1/Mass'][()][dm_mask] / LITTLE_H #10^10 M_sun
  dm_bind = group['PartType1/ParticleBindingEnergy'][()][dm_mask] * SCALE_A #(km/s)^2

  #star
  star_subgroup = group['PartType4/SubGroupNumber'][()]
  if subgroup_only: star_mask = (star_subgroup == subgroup)
  else:             star_mask = np.ones(len(star_subgroup), dtype=bool)

  star_pos  = group['PartType4/Coordinates'][()][star_mask] - c_o_p - BOX_SIZE/2
  star_pos  %= BOX_SIZE
  star_pos  -= BOX_SIZE/2
  star_pos  *= 1000 * SCALE_A / LITTLE_H #kpc
  star_vel  = group['PartType4/Velocity'][()][star_mask] * np.sqrt(SCALE_A) #km/s
  star_mass = group['PartType4/Mass'][()][star_mask] / LITTLE_H #10^10 M_sun
  star_bind = group['PartType4/ParticleBindingEnergy'][()][star_mask] * SCALE_A #(km/s)^2

  #bh
  bh_subgroup = group['PartType5/SubGroupNumber'][()]
  if subgroup_only: bh_mask = (bh_subgroup == subgroup)
  else:             bh_mask = np.ones(len(bh_subgroup), dtype=bool)

  bh_pos  = group['PartType5/Coordinates'][()][bh_mask] - c_o_p - BOX_SIZE/2
  bh_pos  %= BOX_SIZE
  bh_pos  -= BOX_SIZE/2
  bh_pos  *= 1000 * SCALE_A / LITTLE_H #kpc
  bh_vel  = group['PartType5/Velocity'][()][bh_mask] * np.sqrt(SCALE_A) #km/s
  bh_mass = group['PartType5/Mass'][()][bh_mask] / LITTLE_H #10^10 M_sun
  bh_bind = group['PartType5/ParticleBindingEnergy'][()][bh_mask] * SCALE_A #(km/s)^2

  group.close()

  #need to zero velocity
  central_velocity = halo.get_central_velocity(star_pos, star_vel, star_mass,
                                               verbose=False)

  gas_vel  -= central_velocity
  dm_vel   -= central_velocity
  star_vel -= central_velocity
  bh_vel   -= central_velocity

  return(file_name, gas_pos,  gas_vel,  gas_mass,  gas_bind,
                    dm_pos,   dm_vel,   dm_mass,   dm_bind,
                    star_pos, star_vel, star_mass, star_bind,
                    bh_pos,   bh_vel,   bh_mass,   bh_bind)

def load_and_align(data_destination, snap, identity, group=1, subgroup=0,
                   subgroup_only=True):
  '''
  '''
  (file_name, gas_pos,  gas_vel,  gas_mass,  gas_bind,
              dm_pos,   dm_vel,   dm_mass,   dm_bind,
              star_pos, star_vel, star_mass, star_bind,
              bh_pos,   bh_vel,   bh_mass,   bh_bind
              ) = load_snapshot(
                data_destination, snap, identity, group, subgroup,
                subgroup_only)

  #align orientation of disk
  j_tot = halo.total_angular_momentum(star_pos, star_vel, star_mass)
  #find rotation
  rotation = halo.find_rotaion_matrix(j_tot)

  #gas
  gas_pos = rotation.apply(gas_pos)
  gas_vel = rotation.apply(gas_vel)
  #dm
  dm_pos = rotation.apply(dm_pos)
  dm_vel = rotation.apply(dm_vel)
  #star
  star_pos = rotation.apply(star_pos)
  star_vel = rotation.apply(star_vel)
  #bh
  bh_pos = rotation.apply(bh_pos)
  bh_vel = rotation.apply(bh_vel)

  return(file_name,
         gas_pos,  gas_vel,  gas_mass,  gas_bind,
         dm_pos,   dm_vel,   dm_mass,   dm_bind,
         star_pos, star_vel, star_mass, star_bind,
         bh_pos,   bh_vel,   bh_mass,   bh_bind)

def load_and_calculate(data_destination, snap, identity, group=1, subgroup=0,
                       subgroup_only=True):

  (file_name,
   gas_pos,  gas_vel,  gas_mass,  gas_bind,
   dm_pos,   dm_vel,   dm_mass,   dm_bind,
   star_pos, star_vel, star_mass, star_bind,
   bh_pos,   bh_vel,   bh_mass,   bh_bind
   ) = load_and_align(
     data_destination, snap, identity, group, subgroup, subgroup_only)

  (j_circ, binding_e, r_bins
   ) = halo.calculate_binned_circular(
    gas_pos,  gas_vel,  gas_mass,  gas_bind,
    dm_pos,   dm_vel,   dm_mass,   dm_bind,
    star_pos, star_vel, star_mass, star_bind,
    bh_pos,   bh_vel,   bh_mass,   bh_bind)

  # halo.plot_potential_verify(gas_pos,  gas_vel,  gas_mass,  gas_bind,
  #                             dm_pos,   dm_vel,   dm_mass,   dm_bind,
  #                             star_pos, star_vel, star_mass, star_bind,
  #                             bh_pos,   bh_vel,   bh_mass,   bh_bind,
  #                             j_circ, binding_e, r_bins)

  (gas_j_c, dm_j_c, star_j_c, bh_j_c
   ) = halo.calculate_particle_circular(
     gas_pos,  gas_vel,  gas_mass,  gas_bind,
     dm_pos,   dm_vel,   dm_mass,   dm_bind,
     star_pos, star_vel, star_mass, star_bind,
     bh_pos,   bh_vel,   bh_mass,   bh_bind,
     j_circ, binding_e, r_bins,
     do_gas=True, do_dm=True, do_star=True, do_bh=True)

  return(file_name,
         gas_pos,  gas_vel,  gas_mass,  gas_bind,
         dm_pos,   dm_vel,   dm_mass,   dm_bind,
         star_pos, star_vel, star_mass, star_bind,
         bh_pos,   bh_vel,   bh_mass,   bh_bind,
         j_circ, binding_e, r_bins,
         gas_j_c, dm_j_c, star_j_c, bh_j_c)

if __name__ == '__main__':

  base             = '/home/matt/Documents/UWA/EAGLE/'
  # data_location    = base + 'L0012N0188/EAGLE_REFERENCE/data/'
  data_destination = base + 'processed_data/'

  identity         = '_12Mpc_188'
  snap             = '028_z000p000'

  out = load_and_calculate(
    data_destination, snap, identity, group=4, subgroup=0)

  # print(out)

  pass