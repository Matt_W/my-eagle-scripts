#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

from functools import lru_cache

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d

from scipy.integrate   import quad

from scipy.optimize    import minimize
from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.special import kn, iv
from scipy.special import erf
from scipy.special import lambertw
from scipy.special import spence #aka dilogarithm

from scipy.stats import binned_statistic
from scipy.stats import gaussian_kde
import scipy.stats

from awkde import GaussianKDE
import getdist
import getdist.plots

import warnings

import matplotlib
# matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt

import matplotlib.lines as lines
import matplotlib.patches as patches
import matplotlib.patheffects as path_effects
from matplotlib.legend_handler import HandlerTuple, HandlerLine2D

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 12#20
rcParams['xtick.labelsize'] = 12#20
rcParams['ytick.labelsize'] = 12#20
rcParams['axes.labelsize'] = 12#22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s
MSUN_ON_KG = 1.988e30 #M_sun/kg

# BOLTZMANN_K = 1.380649e-23 #J / K = kg m^2 / s^2 / k Wiki
BOLTZMANN_K = 1.380649e-23 * 1e-6 #kg km^2 / s^2 / k Wiki
PROTON_MASS = 1.67262158e-27 #kg

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

Omega_b = 0.04825
Omega_m = 0.307

EAGLE_EPS = 0.7 # kpc

n_jzjc_bins = 21

NRUN = 14 #13

import my_galaxy_models as mgm
import morphology_hdf_keys as keys

path_effect_1 = [path_effects.Stroke(linewidth=1, foreground='black', alpha=1), path_effects.Normal()]
path_effect_2 = [path_effects.Stroke(linewidth=2, foreground='black', alpha=1), path_effects.Normal()]
path_effect_3 = [path_effects.Stroke(linewidth=3, foreground='black', alpha=1), path_effects.Normal()]
path_effect_4 = [path_effects.Stroke(linewidth=4, foreground='black', alpha=1), path_effects.Normal()]
path_effect_8 = [path_effects.Stroke(linewidth=8, foreground='black', alpha=1), path_effects.Normal()]
white_alpha_path_effect_2 = [path_effects.Stroke(linewidth=2, foreground='white', alpha=0.7), path_effects.Normal()]
white_path_effect_2 = [path_effects.Stroke(linewidth=2, foreground='white', alpha=1), path_effects.Normal()]
white_path_effect_3 = [path_effects.Stroke(linewidth=3, foreground='white', alpha=1), path_effects.Normal()]
white_path_effect_4 = [path_effects.Stroke(linewidth=4, foreground='white', alpha=1), path_effects.Normal()]

hatches = {'-': 'xxx', '--': '+++', '-.': r'\\\\ ', (0, (0.8, 0.8)): '...'}

combined_name_pairs = [
    ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
     # '/home/matt/Documents/UWA/EAGLE/L0025N752/RECAL/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
     ],
    ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile_multi.hdf5'],
    ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile_multi.hdf5'],
    ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile_multi.hdf5']
]

# combined_name_pairs = [
#     ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
#      '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
#      # '/home/matt/Documents/UWA/EAGLE/L0025N752/RECAL/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
#      ],
#     ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
#      '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',],
#     ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
#      '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',],
#     ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
#      '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',]
# ]

combined_name_redshifts = [
    ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile_multi.hdf5', ],
    ['/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile_multi.hdf5',
     '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile_multi.hdf5', ],
]

match_files = ['/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_haloes.hdf5',
               '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_haloes.hdf5',
               '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_haloes.hdf5',
               '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_haloes.hdf5']

matched_groups_files = [
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_groups_only_25.hdf5',
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_groups_only_25.hdf5',
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_groups_only_25.hdf5',
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_groups_only_25.hdf5'
]

matched_subs_files = [
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_subgroups_only_25.hdf5',
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_subgroups_only_25.hdf5',
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_subgroups_only_25.hdf5',
    '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_subgroups_only_25.hdf5'
]

''' (fitted on 20/3/23)
z=0: 7x, 1x
[ 0.01805803  1.03014287  0.39003484 11.83089355]
[ 0.0178969   0.98558489  0.36901893 11.85473776]
z=0.5: 7x, 1x
[ 0.01677972  0.96921464  0.46347807 11.85997894]
[ 0.01726748  0.92220301  0.45158719 11.90659349]
z=1: 7x, 1x
[ 0.01555471  0.84337933  0.553132   11.99164799]
[ 0.01597337  0.83218904  0.54990353 12.00853445]
z=2: 7x, 1x
[ 0.01323591  0.72708704  0.66739143 12.23070176]
[ 0.01324826  0.69753135  0.68506589 12.23213424]
'''

def mass_function_args(z=0, hr=True):
    if z == 2.012 or z == 2:
        # if hr:
        return [ 0.01323591, 0.72708704, 0.66739143, 12.23070176]
        # else: return [ 0.01324826, 0.69753135, 0.68506589, 12.23213424]
    elif z == 1.004 or z == 1:
        # if hr:
        return [ 0.01555471, 0.84337933, 0.553132,   11.99164799]
        # else: return [ 0.01597337, 0.83218904, 0.54990353, 12.00853445]
    elif z == 0.503 or z == 0.5:
        # if hr:
        return [ 0.01677972, 0.96921464, 0.46347807, 11.85997894]
        # else: return [ 0.01726748, 0.92220301, 0.45158719, 11.90659349]
    else:
        # if hr:
        return [ 0.01805803, 1.03014287, 0.39003484, 11.83089355]
        # else: return [ 0.0178969,  0.98558489, 0.36901893, 11.85473776]

def mass_function(args, x):
    # output is log(M_star / M_sun), x is log(M_200 / M_sun)
    # return np.log10(2 * np.power(10, x) * args[0] / (
    #         np.power(np.power(10, x - args[3]), -args[1]) + np.power(np.power(10, x - args[3]), args[2])))
    return np.log10(2 * np.power(10, x) * args[0] / (
            np.power(np.power(10, x - args[3]), -args[1]) + np.power(np.power(10, x - args[3]), args[2])))

def old_size_function(args, x, z=0):
    '''This is really good for z=0, not so good for other z.
    x is log(M_200 / M_sun)
    '''
    # args[1] = 4
    # args[2] = 0.2 * 3/4 #0.15

    concs = keys.concentration_ludlow(z, np.power(10, x-10))

    H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(1/(1+z), -3)) # km^2 / s^2 / kpc^2
    rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
    rho_200 = 200 * rho_crit # 10^10 M_sun / kpc^3

    r200s = np.power(3 * np.power(10, x-10) / (4 * np.pi * rho_200), 1 / 3) # kpc
    rs = r200s / concs # kpc
    r_12 = args[2] * rs

    return np.log10(np.power(np.power(r_12, args[1]) + np.power(args[0], args[1]), 1 / args[1])) #log kpc

''' (fitted on 5/4/23)
z=0: 7x, 1x
[1.04716, 0.38767, 0.35636, 11.781]
[1.04716, 0.04232, 0.35636, 11.781]
z=0.5: 7x, 1x
[0.94253, 0.47340, 0.291180, 11.1576]
[0.94253, 0.06310, 0.291180, 11.1576]
z=1: 7x, 1x
[0.79859, 0.65058, 0.20914, 10.649]
[0.79859, 0.14679, 0.20914, 10.649]
z=2: 7x, 1x
[0.51521, 0.60397, 0, 10.938]
[0.51521, 0.39764, 0, 10.938]
'''

def size_function_args(z=0, hr=True):
    if z == 2.012 or z == 2:
        if hr:
            return [0.51521, 0.60397, 0, 10.938]
        else: return [0.51521, 0.39764, 0, 10.938]
    elif z == 1.004 or z == 1:
        if hr:
            return [0.79859, 0.65058, 0.20914, 10.649]
        else: return [0.79859, 0.14679, 0.20914, 10.649]
    elif z == 0.503 or z == 0.5:
        if hr:
            return [0.94253, 0.47340, 0.291180, 11.1576]
        else: return [0.94253, 0.06310, 0.291180, 11.1576]
    else:
        if hr:
            return [1.04716, 0.38767, 0.35636, 11.781]
        else: return [1.04716, 0.04232, 0.35636, 11.781]


def size_function(args, x):
    '''e.g. Shen et al. 2003 eqn. (18)
    out is log(r_1/2/kpc) x is log(M_200 / M_sun)
    '''
    # return np.log10(args[0] * np.power(np.power(10, x), args[1]) *
    #                 np.power( 1 + np.power(10, x) / np.power(10, args[3]), args[2] - args[1]))
    return (args[0] + args[1] * x  + (args[2] - args[1]) * np.log10(1 + np.power(10, x - args[3]))
            - args[1] * 13 - (args[2] - args[1]) * np.log10(1 + np.power(10, 13 - args[3])))


''' (evaluated 19/4/23)
z=0, 7x, 1x,
11.004316932647056
11.538391889695763
z=0.5, 7x, 1x,
10.797432961908406
11.274839365561101
z=1, 7x, 1x,
10.611085228618078
11.07266393992926
z=2, 7x, 1x,
10.355332404370081
10.767682642030607
'''

def m200_heating_intercept(z=0, hr=True):
    if z == 2.012 or z == 2:
        if hr: return 10.355332404370081
        else: return 10.767682642030607
    elif z == 1.004 or z == 1:
        if hr: return 10.611085228618078
        else: return 11.07266393992926
    elif z == 0.503 or z == 0.5:
        if hr: return 10.797432961908406
        else: return 11.274839365561101
    else:
        if hr: return 11.004316932647056
        else: return 11.538391889695763


class HandlerTupleVertical(HandlerTuple):
    def __init__(self, **kwargs):
        HandlerTuple.__init__(self, **kwargs)
        # HandlerLine2D.__init__(self, **kwargs)

    def create_artists(self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans):
        # How many lines are there.
        numlines = len(orig_handle)
        handler_map = legend.get_legend_handler_map()

        # divide the vertical space where the lines will go
        # into equal parts based on the number of lines
        height_y = (height / numlines)

        leglines = []
        for i, handle in enumerate(orig_handle):
            handler = legend.get_legend_handler(handler_map, handle)

            legline = handler.create_artists(legend, handle, xdescent, (2 * i + 1) * height_y, width, 2 * height,
                                             fontsize, trans)
            leglines.extend(legline)

        return leglines


def my_bootstrap(data, n=1000, statistic=np.median, quantiles=[0.16, 0.50, 0.84]):
    estimates = np.zeros(n)
    ld = len(data)
    for i in range(n):
        estimates[i] = statistic(data[np.random.randint(0, ld, ld)])

    return [np.quantile(estimates, q) for q in quantiles]


def matched_fraction_success(name='', save=False, reverse=False, ins=False,
                             sub=False, seperate_panels=True, stellar_masses=False):
    redshift_indexes = [0] #[3,2,1,0]

    if not seperate_panels:
        fig = plt.figure(constrained_layout=True)
        # fig.set_size_inches(3.3 * len(redshift_indexes), 3.3 * 3 + 0.04, forward=True)
        # spec = fig.add_gridspec(ncols=len(redshift_indexes), nrows=3)
        fig.set_size_inches(3.3, 3.3 * 3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=3)

    if len(redshift_indexes) == 1:
        xlims = [8, 14]
        xticks = [8,9,10,11,12,13,14]
    else:
        xlims = [1e-3 + 8, 14 - 1e-3]
        xticks = [9,10,11,12,13]

    ylims0 = [0, 1] #[-0.05, 1.05]
    ylims0_ins = [0.95, 1] #[-0.05, 1.05]
    ylims1 = [-0.18, 0.18]
    # ylims1 = [-0.02, 0.02]
    ylims2 = [1e-3 + 0.2, 3.2 - 1e-3] #[1e-3 + 0.5, 2.5 - 1e-3] #
    ylimss = [ylims0, ylims1, ylims2]

    # N_run = np.linspace(7.75, 14, 26)
    N_run = np.linspace(7.8, 14, 32)
    # N_run = np.linspace(7.5, 14, 11)

    if stellar_masses:
        N_run -= 2
        xlims[0] -= 2
        xlims[1] -= 2

    n_bins = 16
    n_bins = (n_bins, int(n_bins/ np.sqrt(3))) #(int(np.sqrt(3) * n_bins), n_bins) #

    xlabel = r'$\log \, M_{200}^{\rm HR} / $M$_\odot$'
    if stellar_masses:
        xlabel = r'$\log \, M_\star^{\rm HR} / $M$_\odot$'
    ylabels0 = r'Matched Fraction'
    ylabels1 = r'$(M_{200}^{\rm LR}$ - $M_{200}^{\rm HR}$) / $M_{200}^{\rm HR}$'
    # ylabels1 = r'$ \log M_{200}^{\rm LR}$ / $M_{200}^{\rm HR} $'
    if stellar_masses:
        ylabels1 = r'($M_\star^{\rm LR}$ - $M_\star^{\rm HR}$) / $M_\star^{\rm HR}$'
    ylabels2 = r'$\log \, |\Delta \vec{r}_{\rm COP}| / $kpc'
    ylabels = [ylabels0, ylabels1, ylabels2]


    ylabels = [ylabels0, ylabels1, ylabels2]

    cmap='Greys'

    if not seperate_panels:
        axs = []
        axs_ins = []
        for row in range(3):
            axs.append([])
            # for col in range(len(redshift_indexes)):
            for col in range(1):
                axs[row].append(fig.add_subplot(spec[row, col]))
                axs[row][col].set_xlim(xlims)
                axs[row][col].set_ylim(ylimss[row])

                if col == 0:
                    axs[row][col].set_ylabel(ylabels[row])
                else:
                    axs[row][col].set_yticklabels([])

                axs[row][col].set_xticks(xticks)
                if row == 2:
                    axs[row][col].set_xlabel(xlabel)
                else:
                    axs[row][col].set_xticklabels([])

                if ins and row == 0:
                    axs_ins.append(axs[0][col].inset_axes([(10 - xlims[0]) / (xlims[1] - xlims[0]), 0.25,
                                                            4 / (xlims[1] - xlims[0]), 0.5]))

                    axs_ins[col].set_xticks(xticks)
                    # axs_ins[col].set_xlabel(xlabel)
                    # axs_ins[col].set_ylabel(ylabels0)
                    axs_ins[col].set_xlim(xlims0_ins)
                    axs_ins[col].set_ylim(ylims0_ins)

                # axs[row][col].set_aspect(np.diff(xlims) / np.diff(ylimss[row]))

        fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if seperate_panels:
        #separate figures
        fig0 = plt.figure(constrained_layout=True)
        # fig0.set_size_inches(3.3, 3.3, forward=True)
        fig0.set_size_inches(2.5, 2.5, forward=True)
        ax0 = plt.gca()

        fig1 = plt.figure(constrained_layout=True)
        # fig1.set_size_inches(3.3, 3.3, forward=True)
        fig1.set_size_inches(2.5, 2.5, forward=True)
        ax1 = plt.gca()

        fig2 = plt.figure(constrained_layout=True)
        # fig2.set_size_inches(3.3, 3.3, forward=True)
        fig2.set_size_inches(2.5, 2.5, forward=True)
        ax2 = plt.gca()

        axs = [[ax0], [ax1], [ax2]]

        for row in range(3):
            for col in [0] * len(redshift_indexes): #range(len(redshift_indexes)):
                axs[row][col].set_xlim(xlims)
                axs[row][col].set_ylim(ylimss[row])

                if col == 0:
                    axs[row][col].set_ylabel(ylabels[row])
                else:
                    axs[row][col].set_yticklabels([])

                axs[row][col].set_xticks(xticks)
                axs[row][col].set_xlabel(xlabel)

    LARGE = 1_000_000

    #start load data
    for col, z_index in zip([0] * len(redshift_indexes), redshift_indexes): #enumerate(redshift_indexes):
        z = [0, 0.503, 1.004, 2.012][z_index]
        SCALE_A = 1 / (1 + z)

        color = matplotlib.cm.get_cmap('inferno_r')((3 + z_index) / 6)

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
            # centrals only
            if not sub:
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            else:
                sn0 = raw_data0[keys.key0_dict['sn'][None]][:]
                mask0 = np.ones(len(sn0), dtype=bool)
            gn0 = raw_data0[keys.key0_dict['gn'][None]][:]
            gn0 = gn0[mask0]

            if sub:
                unique_sn0 = LARGE * gn0 + sn0

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
            r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]
            cop_0 = raw_data0['/GalaxyQuantities/SubGroupCOP'][:, :]
            MDM_0 = raw_data0['/GalaxyProfiles/DM/BinMass'][:, keys.rkey_to_rcolum_dict['r200', None]]
            Mstar_0 = raw_data0['/GalaxyProfiles/Star/BinMass'][:, keys.rkey_to_rcolum_dict['r200', None]]

            M200_0 = M200_0[mask0] * 1e10
            log_M200_0 = np.log10(M200_0) # log Mstar
            central_log_M200_0 = np.log10(M200_0) # log Mstar
            print(np.sum(central_log_M200_0 > 10))
            cop_0 = cop_0[mask0] * SCALE_A / LITTLE_H * 1000
            MDM_0 = np.log10(MDM_0[mask0]) + 10
            Mstar_0 = Mstar_0[mask0] * 1e10
            log_Mstar_0 = np.log10(Mstar_0) # log Mstar
            central_log_Mstar_0 = np.log10(Mstar_0) # log Mstar

        with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
            # centrals only
            if not sub:
                mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
            else:
                sn1 = raw_data1[keys.key0_dict['sn'][None]][:]
                mask1 = np.ones(len(sn1), dtype=bool)
            gn1 = raw_data1[keys.key0_dict['gn'][None]][:]
            gn1 = gn1[mask1]

            if sub:
                unique_sn1 = LARGE * gn1 + sn1

            # load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
            r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]
            cop_1 = raw_data1['/GalaxyQuantities/SubGroupCOP'][:, :]
            MDM_1 = raw_data1['/GalaxyProfiles/DM/BinMass'][:, keys.rkey_to_rcolum_dict['r200', None]]
            Mstar_1 = raw_data1['/GalaxyProfiles/Star/BinMass'][:, keys.rkey_to_rcolum_dict['r200', None]]

            M200_1 = M200_1[mask1] * 1e10
            log_M200_1 = np.log10(M200_1) # log Mstar
            central_log_M200_1 = np.log10(M200_1)
            print(np.sum(central_log_M200_1 > 10))
            cop_1 = cop_1[mask1] * SCALE_A / LITTLE_H * 1000
            MDM_1 = np.log10(MDM_1[mask1]) + 10
            Mstar_1 = Mstar_1[mask1] * 1e10
            log_Mstar_1 = np.log10(Mstar_1) # log Mstar
            central_log_Mstar_1 = np.log10(Mstar_1) # log Mstar

        if not sub:
            with h5.File(matched_groups_files[z_index], 'r') as match_data:
                matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
                matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

            #not all haloes have stars
            ni0 = np.isin(matched_gn_0, gn0)
            ni1 = np.isin(matched_gn_1, gn1)
            ni = np.logical_and(ni0, ni1)
            matched_gn_0 = matched_gn_0[ni]
            matched_gn_1 = matched_gn_1[ni]

            #not all haloes matched with stars
            in0 = np.isin(gn0, matched_gn_0)
            in1 = np.isin(gn1, matched_gn_1)

            order0 = np.argsort(np.argsort(matched_gn_0))
            order1 = np.argsort(np.argsort(matched_gn_1))

            M200_0 = M200_0[in0][order0]
            log_M200_0 = log_M200_0[in0][order0]
            cop_0 = cop_0[in0][order0]
            gn0 = gn0[in0][order0]
            MDM_0 = MDM_0[in0][order0]
            Mstar_0 = Mstar_0[in0][order0]
            log_Mstar_0 = log_Mstar_0[in0][order0]

            M200_1 = M200_1[in1][order1]
            log_M200_1 = log_M200_1[in1][order1]
            cop_1 = cop_1[in1][order1]
            gn1 = gn1[in1][order1]
            MDM_1 = MDM_1[in1][order1]
            Mstar_1 = Mstar_1[in1][order1]
            log_Mstar_1 = log_Mstar_1[in1][order1]

        if sub:
            with h5.File(matched_subs_files[z_index], 'r') as match_data:
                matched_gn_0 = match_data['SubGroupBijectiveMatches/MatchedGroupNumber7x'][()]
                matched_gn_1 = match_data['SubGroupBijectiveMatches/MatchedGroupNumber1x'][()]
                matched_sn_0 = match_data['SubGroupBijectiveMatches/MatchedSubGroupNumber7x'][()]
                matched_sn_1 = match_data['SubGroupBijectiveMatches/MatchedSubGroupNumber1x'][()]

            matched_unique_sn_0 = LARGE * matched_gn_0 + matched_sn_0
            matched_unique_sn_1 = LARGE * matched_gn_1 + matched_sn_1

            #not all haloes have stars
            ni0 = np.isin(matched_unique_sn_0, unique_sn0)
            ni1 = np.isin(matched_unique_sn_1, unique_sn1)
            ni = np.logical_and(ni0, ni1)
            matched_unique_sn_0 = matched_unique_sn_0[ni]
            matched_unique_sn_1 = matched_unique_sn_1[ni]

            #not all haloes matched with stars
            in0 = np.isin(unique_sn0, matched_unique_sn_0)
            in1 = np.isin(unique_sn1, matched_unique_sn_1)

            order0 = np.argsort(np.argsort(matched_unique_sn_0))
            order1 = np.argsort(np.argsort(matched_unique_sn_1))

            M200_0 = M200_0[in0][order0]
            log_M200_0 = log_M200_0[in0][order0]
            cop_0 = cop_0[in0][order0]
            gn0 = gn0[in0][order0]
            MDM_0 = MDM_0[in0][order0]
            Mstar_0 = Mstar_0[in0][order0]
            log_Mstar_0 = log_Mstar_0[in0][order0]

            M200_1 = M200_1[in1][order1]
            log_M200_1 = log_M200_1[in1][order1]
            cop_1 = cop_1[in1][order1]
            gn1 = gn1[in1][order1]
            MDM_1 = MDM_1[in1][order1]
            Mstar_1 = Mstar_1[in1][order1]
            log_Mstar_1 = log_Mstar_1[in1][order1]

        m200_intercept_0 = m200_heating_intercept(z, 1)
        m200_intercept_1 = m200_heating_intercept(z, 0)

        if stellar_masses:
            M200_0 = Mstar_0
            M200_1 = Mstar_1
            log_M200_0 = log_Mstar_0
            log_M200_1 = log_Mstar_1
            central_log_M200_0 = central_log_Mstar_0
            central_log_M200_1 = central_log_Mstar_1

        #match percent
        ymedians, xedges, bin_is = binned_statistic(central_log_M200_0, in0,
                                                    statistic=np.mean, bins=N_run)
        xcentres = 0.5 * (xedges[1:] + xedges[:-1])
        counts, _ = np.histogram(log_M200_0, bins=N_run)
        # mask = counts >= 1
        mask = counts > 20 * np.diff(N_run)[0]

        # print_cut = np.log10(10**10 / 0.6777)
        print_cut = np.log10(10**10)
        print(round(np.mean(np.hstack((in0[central_log_M200_0 > print_cut],in1[central_log_M200_1 > print_cut]))), 4),
              np.sum(np.hstack((in0[central_log_M200_0 > print_cut], in1[central_log_M200_1 > print_cut]))),
              np.sum(np.hstack((central_log_M200_0 > print_cut, central_log_M200_1 > print_cut))),
              round(np.mean(in0[central_log_M200_0 > print_cut]), 4),
              np.sum(in0[central_log_M200_0 > print_cut]), np.sum(central_log_M200_0 > print_cut),
              round(np.mean(in1[central_log_M200_1 > print_cut]), 4),
              np.sum(in1[central_log_M200_1 > print_cut]), np.sum(central_log_M200_1 > print_cut))

        axs[0][col].errorbar(xcentres[mask], ymedians[mask],
                             c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        if ins:
            axs_ins[col].errorbar(xcentres[mask], ymedians[mask],
                                 c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        if reverse:
            ymedians, xedges, bin_is = binned_statistic(central_log_M200_1, in1,
                                                        statistic=np.mean, bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])
            counts, _ = np.histogram(log_M200_0, bins=N_run)
            # mask = counts >= 1
            mask = counts > 20 * np.diff(N_run)[0]

            axs[0][col].errorbar(xcentres[mask], ymedians[mask],
                                 c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
            if ins:
                axs_ins[col].errorbar(xcentres[mask], ymedians[mask],
                                     c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        #mass consistancy
        y = (M200_1 - M200_0)/M200_0
        # y = np.log10(M200_1 / M200_0)

        #way less secatter in MDMs
        # y = (MDM_1 - MDM_0)/MDM_1
        # log_M200_0 = MDM_1

        ymedians, xedges, bin_is = binned_statistic(log_M200_0, y,
                                                    statistic=np.median, bins=N_run)
        yplus, xedges, _ = binned_statistic(log_M200_0, y,
                                            statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
        yminus, xedges, _ = binned_statistic(log_M200_0, y,
                                             statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        xcentres = 0.5 * (xedges[1:] + xedges[:-1])
        counts, _ = np.histogram(log_M200_0, bins=N_run)
        # mask = counts > 1
        mask = counts > 20 * np.diff(N_run)[0]

        print(xcentres)
        print(yplus)
        print(yminus)

        counts = np.concatenate((counts, [1]))

        if not save:
            axs[1][col].scatter(log_M200_0, y,
                                color=color, marker='o', s=1, alpha=1, rasterized=True)
        axs[1][col].hexbin(log_M200_0, y, C=1/counts[bin_is-1],
                           cmap=cmap,
                           reduce_C_function = lambda x: np.log10(np.nansum(x)),
                           extent=[*xlims, *ylims1], mincnt=0, gridsize=n_bins)
        axs[1][col].errorbar(xcentres[mask], ymedians[mask],
                             c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        axs[1][col].errorbar(xcentres[mask], yplus[mask],
                             c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        axs[1][col].errorbar(xcentres[mask], yminus[mask],
                             c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        if reverse:
            y = (M200_0 - M200_1)/M200_1
            # y = np.log10(M200_0 / M200_1)

            ymedians, xedges, bin_is = binned_statistic(log_M200_0, y,
                                                        statistic=np.median, bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])
            counts, _ = np.histogram(log_M200_0, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]

            axs[1][col].errorbar(xcentres[mask], ymedians[mask],
                                 c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        #pos
        y = np.log10(np.linalg.norm(cop_0 - cop_1, axis=1))

        ymedians, xedges, bin_is = binned_statistic(log_M200_0, y,
                                                    statistic=np.median, bins=N_run)
        yplus, xedges, _ = binned_statistic(log_M200_0, y,
                                            statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
        yminus, xedges, _ = binned_statistic(log_M200_0, y,
                                             statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        xcentres = 0.5 * (xedges[1:] + xedges[:-1])
        counts, _ = np.histogram(log_M200_0, bins=N_run)
        # mask = counts > 1
        mask = counts > 20 * np.diff(N_run)[0]

        counts = np.concatenate((counts, [1]))

        if not save:
            axs[2][col].scatter(log_M200_0, y,
                                color=color, marker='o', s=1, alpha=1, rasterized=True)
        axs[2][col].hexbin(log_M200_0, y, C=1/counts[bin_is-1],
                           cmap=cmap,
                           reduce_C_function = lambda x: np.log10(np.nansum(x)),
                           extent=[*xlims, *ylims2], mincnt=0, gridsize=n_bins)
        axs[2][col].errorbar(xcentres[mask], ymedians[mask],
                             c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        axs[2][col].errorbar(xcentres[mask], yplus[mask],
                             c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        axs[2][col].errorbar(xcentres[mask], yminus[mask],
                             c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        for lm2, dy in zip(log_M200_0, y):
            if lm2 > 11 and dy > 2:
                print(lm2, 10**dy)

        if reverse:
            ymedians, xedges, bin_is = binned_statistic(log_M200_1, y,
                                                        statistic=np.median, bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])
            counts, _ = np.histogram(log_M200_1, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]

            axs[2][col].errorbar(xcentres[mask], ymedians[mask],
                                 c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        #decorations
        axs[1][col].axhline(0, 0, 1,
                            color='k', ls=':', lw=1, zorder=3, path_effects=white_path_effect_3)

        # model
        log_M200s = np.linspace(*xlims, 201)  # log M_sun
        # cosmology
        H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3))  # km^2/s^2 / kpc^2
        rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
        rho_200 = 200 * rho_crit
        r200 = np.power(3 * 10 ** (log_M200s - 10) / (4 * np.pi * rho_200), 1 / 3)  # kpc

        axs[2][col].errorbar(log_M200s, np.log10(r200),
                            color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)
        axs[2][col].text(log_M200s[int(4/8*len(log_M200s))],
                         np.log10(r200[int(4/8*len(log_M200s))]) + 0.005 * (ylims2[1] - ylims2[0]),
                         r'$r_{200}$', rotation_mode='anchor', ha='left', va='bottom',
                         rotation=(np.arctan2(np.diff(xlims) / 3, np.diff(ylims2))*180/np.pi)[0],
                         color='k', path_effects=white_path_effect_3)

        axs[0][col].axvline(np.log10(50 * DM_MASS) + 10, 0, 1,
                            color='k', ls='--', lw=1, zorder=3, path_effects=white_path_effect_3)
        axs[1][col].axvline(np.log10(50 * DM_MASS) + 10, 0, 1,
                            color='k', ls='--', lw=1, zorder=3, path_effects=white_path_effect_3)
        axs[2][col].axvline(np.log10(50 * DM_MASS) + 10, 0, 1,
                            color='k', ls='--', lw=1, zorder=3, path_effects=white_path_effect_3)
        axs[0][col].axvline(np.log10(600 * DM_MASS) + 10, 0, 1,
                            color='k', ls=':', lw=1, zorder=3, path_effects=white_path_effect_3)
        axs[1][col].axvline(np.log10(600 * DM_MASS) + 10, 0, 1,
                            color='k', ls=':', lw=1, zorder=3, path_effects=white_path_effect_3)
        axs[2][col].axvline(np.log10(600 * DM_MASS) + 10, 0, 1,
                            color='k', ls=':', lw=1, zorder=3, path_effects=white_path_effect_3)

        axs[0][col].text(np.log10(50 * DM_MASS) + 10,  ylims0[0] + 0.95 * (ylims0[1] - ylims0[0]),
                         r'$50 \times m_{\rm DM}^{\rm LR}$', va='top', ha='right', rotation='vertical')#,
                         # color='white', path_effects=path_effect_2)
        axs[0][col].text(np.log10(600 * DM_MASS) + 10,  ylims0[0] + 0.05 * (ylims0[1] - ylims0[0]),
                         r'$600 \times m_{\rm DM}^{\rm LR}$', ha='right', rotation='vertical')#,
                         #color='white', path_effects=path_effect_2)

        axs[0][col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims0[0] + 0.05 * (ylims0[1] - ylims0[0]),
                         fr"$z={['0', '0.5', '1', '2'][z_index]}$", ha='right',
                         bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[1][col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims1[0] + 0.05 * (ylims1[1] - ylims1[0]),
                         fr"$z={['0', '0.5', '1', '2'][z_index]}$", ha='right',
                         bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2][col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims2[0] + 0.05 * (ylims2[1] - ylims2[0]),
                         fr"$z={['0', '0.5', '1', '2'][z_index]}$", ha='right',
                         bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save and not seperate_panels:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    if save and seperate_panels:
        fig0.savefig(name[:-4] + '0' + name[-4:], bbox_inches='tight', dpi=200)
        fig1.savefig(name[:-4] + '1' + name[-4:], bbox_inches='tight', dpi=200)
        fig2.savefig(name[:-4] + '2' + name[-4:], bbox_inches='tight', dpi=200)

        plt.close()
        plt.close()
        plt.close()

    return


def stellar_mass_halo_mass_relation(name='', save=False):

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    xlims = [1e-3 + 10, 14 - 1e-3]
    yticks = [9,10,11]
    ylims = [1e-3 + 8, 12 - 1e-3]
    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(10, 14, 9)
    N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, M_\star^{\rm central} / $M$_\odot$'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_yticks(yticks)

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        axs[col].set_aspect('equal')

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]: z = 2.012
        elif '019_z001p004_' in name_pairs[0]: z = 1.004
        elif '023_z000p503_' in name_pairs[0]: z = 0.503
        else: z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8,0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                label = 'LRDM'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                #load
                x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                y0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                #mask and units
                x0 = np.log10(x0[mask0]) + 10
                y0 = np.log10(y0[mask0]) + 10
                #mask
                not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                x = x0[not_crazy_mask0]
                y = y0[not_crazy_mask0]

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]
            max_xbin_centre = xcentres[:-1][np.logical_not(mask)[1:]][0]

            boots = np.zeros((len(xedges) - 1, 3))
            for i in range(len(xedges) - 1):
                boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

            # plot
            axs[col].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                                  color=color, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle])
            # axs[col].scatter(x[x > max_xbin_centre], y[x > max_xbin_centre],
            #                  color=color, marker=marker, s=3, alpha=1)#, rasterized=True)
            axs[col].scatter(x, y,
                             color=color, marker=marker, s=4, alpha=0.5, linewidths=0, rasterized=True)
            axs[col].errorbar(xcentres[mask], ymedians[mask],
                              c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            axs[col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
                                  color=color, alpha=0.5, edgecolor='k', zorder=9)

            #fit
            guess = [0.01, 0.7, 0.7, 12.]
            # mask = np.isfinite(ymedians)
            # to_min = lambda args: np.sum((mass_function(args, xcentres[mask]) - ymedians[mask])**2)
            mask = np.logical_and(np.logical_and(x > 11, np.isfinite(x)), np.isfinite(y))
            to_min = lambda args: np.log10(np.sum((mass_function(args, x[mask]) - y[mask])**2))
            out = minimize(to_min, guess, method='Nelder-Mead')
            print(out.x)
            # print(out.message)

            ''' (fitted on 20/3/23)
            z=0: 7x, 1x
            [ 0.01805803  1.03014287  0.39003484 11.83089355]
            [ 0.0178969   0.98558489  0.36901893 11.85473776]
            z=0.5: 7x, 1x
            [ 0.01677972  0.96921464  0.46347807 11.85997894]
            [ 0.01726748  0.92220301  0.45158719 11.90659349]
            z=1: 7x, 1x
            [ 0.01555471  0.84337933  0.553132   11.99164799]
            [ 0.01597337  0.83218904  0.54990353 12.00853445]
            z=2: 7x, 1x
            [ 0.01323591  0.72708704  0.66739143 12.23070176]
            [ 0.01324826  0.69753135  0.68506589 12.23213424]
            '''

            xs = np.linspace(*xlims, 201)
            #only 1 set of lines for HR
            if pair == 0:
                axs[col].errorbar(xs, mass_function(out.x, xs), ls='-.', c='navy', lw=2, zorder=20)

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            axs[col].axhline(mstar_intercept, 0, 1,
                             color=color2, ls=linestyle2, lw=1, zorder=-3)
            axs[col].axvline(m200_intercept, 0, 1,
                             color=color2, ls=linestyle2, lw=1, zorder=-3)

        #decorations
        axs[col].errorbar(xlims, [np.log10(Omega_b / Omega_m * 10 ** xlims[0]),
                                     np.log10(Omega_b / Omega_m * 10 ** xlims[1])],
                             c='k', ls='-', lw=1, zorder=1, alpha=1)

        for i in range(-10, 10):
            axs[col].errorbar(xlims, [np.log10(Omega_b / Omega_m * 10 ** xlims[0]) + i,
                                      np.log10(Omega_b / Omega_m * 10 ** xlims[1]) + i],
                              c='grey', ls=':', lw=1, zorder=-10)

    axs[0].text(np.mean([*xlims, xlims[0]]), np.mean([*(2*[np.log10(Omega_b/Omega_m * 10**xlims[0])]),
                                                      np.log10(Omega_b/Omega_m * 10**xlims[1])])
                     + 0.005 * (ylims[1] - ylims[0]),
                     r'$M_{\star} / M_{200} = \Omega_b / \Omega_m$',
                     rotation_mode='anchor', ha='center', va='bottom',
                     rotation=(np.arctan(1)*180/np.pi),
                     path_effects=white_path_effect_3)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4),
                    patches.Patch(facecolor='C0', edgecolor='k', alpha=0.5)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4),
                    patches.Patch(facecolor='C1', edgecolor='k', alpha=0.5)),
                   (lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2, path_effects=path_effect_4))
                   ],
                  ['HR DM', 'LR DM', 'HR Fit'],
                  loc='lower right', framealpha=0.9)
    # axs[1].legend([(patches.Patch(facecolor='grey', edgecolor='k', alpha=0.5)),
    #                (patches.Patch(facecolor='grey', edgecolor='k', alpha=0.5, hatch='xxx'))],
    #               ['16th-84th\npercentile', 'Error on\nthe median'],
    #               # loc='upper left', title=r'$z=0.5$')
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='lower right')
    # axs[2].legend([],[], loc='upper left',
    #               title=r'$z=1$')
    # axs[3].legend([],[], loc='upper left',
    #               title=r'$z=2$')

    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.9))
    axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.9))
    axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.9))
    axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.9))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def more_size_mass_relation(name='', save=False, fit=False):

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    xlims = [1e-3 + 10, 14 - 1e-3]
    # xlims = [1e-3 + 9, 14 - 1e-3]
    ylims = [-0.4, 1.6]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(10-1/3, 14, 14)
    N_run = np.linspace(9.5, 14, 10)

    age_bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    # ylabel = r'$\log \, r_{1/2, \star} / $kpc'
    ylabel = r'$\log \, r_{1/2} / $kpc'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        axs[col].set_aspect(2) #should be 3 e.g. Mo, Mao & White

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    # cmap_0 = 'turbo'
    # cmap_1 = 'turbo'

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                cmap = cmap_0
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0,(0.8,0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                cmap = cmap_1

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                #load
                x0 = raw_data0[keys.key0_dict['m200'][None]][:]

                rk2 = keys.rkey_to_rcolum_dict['r200_profile', 0]
                y0 = raw_data0[keys.key0_dict['r12r']['Star']][:, rk2:rk2+len(age_bin_centres)]
                # y0 = raw_data0[keys.key0_dict['r12'][None]][:]

                # y0 = raw_data0[keys.key0_dict['R12'][None]][:]
                #mask and units
                x = np.log10(x0[mask0]) + 10
                y0 = np.log10(y0[mask0])
                #mask
                # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                # x = x0[not_crazy_mask0]
                # y0 = y0[not_crazy_mask0]

            for i in range(len(age_bin_centres)):
                y = y0[:, i]

                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 10
                mask = counts > 50 * np.diff(N_run)[0]
                max_xbin_centre = xcentres[:-1][np.logical_not(mask)[1:]][0]
                axs[col].scatter(x[x > max_xbin_centre], y[x > max_xbin_centre],
                                 color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                                 # edgecolors=color, linewidths=0.5,
                                 marker=marker, s=3, alpha=1, zorder=4)#, rasterized=True)
                # if pair == 0:
                # axs[col].scatter(x, y,
                #                  color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                #                  marker=marker, s=2, alpha=0.5,
                #                  linewidths=0, zorder=2-i - pair*0.5, rasterized=True)
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                                  ls=linestyle, linewidth=2, zorder=10-i, alpha=1)#, path_effects=path_effect_3)
                # axs[col].errorbar(xcentres, ymedians,
                #                   c=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                #                   ls=linestyle, linewidth=2, zorder=10-i, alpha=1)#, path_effects=path_effect_3)

            # #model
            # log_M200s = np.linspace(*xlims, 201) #log M_sun
            # #cosmology
            # H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3)) #km^2/s^2 / kpc^2
            # rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            # rho_200 = 200 * rho_crit
            # r200 = np.power(3 * 10**(log_M200s-10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
            #
            # #NFW
            # conc = mgm.concentration_ludlow(z, 10**(log_M200s-10))
            # r_s = r200 / conc #kpc
            #
            # r12_on_rm2 = 0.15

        #decorations
        frac_size = 8
        axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(EAGLE_EPS),
                       -(xlims[1] - xlims[0]) / frac_size, 0,
                       length_includes_head=True,
                       head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                       head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                       color='k', fc='white', lw=1, zorder=-15, alpha=1)
        axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(2.8*EAGLE_EPS),
                       -(xlims[1] - xlims[0]) / frac_size, 0,
                       length_includes_head=True,
                       head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                       head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                       color='k', fc='white', lw=1, zorder=-15, alpha=1)

        # if col in (0,1):
        if col == 1:
            axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                          np.log10(EAGLE_EPS) + 0.000 * (ylims[1] - ylims[0]),
                        r'$\epsilon$', va='bottom')

            if col == 3:
                axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                              np.log10(2.8 * EAGLE_EPS) - 0.015 * (ylims[1] - ylims[0]),
                            r'$2.8 \times \epsilon$', va='top')
            else:
                axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                              np.log10(2.8 * EAGLE_EPS) + 0.005 * (ylims[1] - ylims[0]),
                            r'$2.8 \times \epsilon$', va='bottom')

        # for i in range(20):
        #     #1/3 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i/3 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) / 3,
        #                               i/3 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        if fit:
            with h5.File(name_pairs[0], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # load
                x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                y0 = raw_data0[keys.key0_dict['r12'][None]][:]
                # y0 = raw_data0[keys.key0_dict['R12'][None]][:]
                # mask and units
                x0 = np.log10(x0[mask0]) + 10
                y0 = np.log10(y0[mask0])
                # mask
                not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                x0 = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

            with h5.File(name_pairs[1], 'r') as raw_data0:
                # centrals only
                mask1 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # load
                x1 = raw_data0[keys.key0_dict['m200'][None]][:]
                y1 = raw_data0[keys.key0_dict['r12'][None]][:]
                # y1 = raw_data0[keys.key0_dict['R12'][None]][:]
                # mask and units
                x1 = np.log10(x1[mask1]) + 10
                y1 = np.log10(y1[mask1])
                # mask
                not_crazy_mask1 = np.logical_and(np.isfinite(x1), np.isfinite(y1))
                x1 = x1[not_crazy_mask1]
                y1 = y1[not_crazy_mask1]

            guess = [0, 0.1, 0.2, 0.4, 11.5]
            if z == 0:
                guess = [1.06, 0.41, 0.05, 0.42, 11.4]
            elif z == 0.503:
                guess = [0.91, 0.52, 0.06, 0.31, 11.0]
            elif z == 1.004:
                guess = [0.74, 0.60, 0.19, 0.22, 10.8]
            elif z == 2.012:
                # guess = [0.36, 0.58, 0.41, -0.07, 11.1]
                guess = [0.36, 0.58, 0.41, 0, 11.1]

            m200_lim = 10
            mask0 = np.logical_and(np.logical_and(x0 > m200_lim, np.isfinite(x0)), np.isfinite(y0))
            mask1 = np.logical_and(np.logical_and(x1 > m200_lim, np.isfinite(x1)), np.isfinite(y1))
            to_min = lambda args: np.log10(np.sum((size_function([args[0], args[1], args[3], args[4]],
                                                                 x0[mask0]) - y0[mask0]) ** 2) +
                                           np.sum((size_function([args[0], args[2], args[3], args[4]],
                                                                 x1[mask1]) - y1[mask1]) ** 2))

            xs = np.linspace(*xlims, 201)

            out0 = mgm.size_function_args(z, True)
            out1 = mgm.size_function_args(z, False)

            axs[col].errorbar(xs, size_function(out0, xs), ls='-.', c='navy', lw=3, zorder=20,
                              )#path_effects=path_effect_4)
            axs[col].errorbar(xs, size_function(out1, xs), ls=(0, (0.8, 0.8)), c='saddlebrown', lw=3, zorder=20,
                              )#path_effects=path_effect_4)

    # #more decorations
    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
    #                ],
    #               ['HR DM\nmedian', 'LR DM\nmedian'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='upper left')
    #
    # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
    #                (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls=(0, (0.8, 0.8)), lw=2)),
    #                ],
    #               ['HR DM\nmodel', 'LR DM\nmodel'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='upper left')
    #
    # axs[2].legend([(patches.Patch(facecolor='grey', edgecolor='k', alpha=0.5)),
    #                (patches.Patch(facecolor='grey', edgecolor='k', alpha=0.5, hatch='xxx'))],
    #               ['16th-84th\npercentile', 'Error on\nthe median'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='upper left')

    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return

def size_mass_relation(name='', save=False, model=True):

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)
    # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
    # spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [-0.4, 1.6]
    # xlims = [1e-3 + 8, 12 - 1e-3]
    # ylims = [0, 1.8]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    # xlabel = r'$\log \, M_\star / $M$_\odot$'
    # ylabel = r'$\log \, r_{1/2, \star} / $kpc'
    ylabel = r'$\log \, r_{1/2} / $kpc'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))

    # for col in range(len([combined_name_pairs[0]])):
    #     axs.append(fig.add_subplot(spec[0]))

        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])

        # axs[col].set_xlabel(xlabel)

        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims)) #should be 3 e.g. Mo, Mao & White

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    # for col, name_pairs in enumerate([combined_name_pairs[0]]):
    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
        # for pair in range(1,2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0,(0.8,0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                #load
                x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                # x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                y0 = raw_data0[keys.key0_dict['r12'][None]][:]
                # y0 = raw_data0[keys.key0_dict['R12'][None]][:]
                #mask and units
                x0 = np.log10(x0[mask0]) + 10
                y0 = np.log10(y0[mask0])
                #mask
                not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                x = x0[not_crazy_mask0]
                y = y0[not_crazy_mask0]

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]
            max_xbin_centre = xcentres[:-1][np.logical_not(mask)[1:]][0]

            xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
            xlin2 = np.linspace(xcentres[mask][0], mstar_intercept)
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            # plot
            axs[col].errorbar(xlin2, ymed_func(xlin2),
                              c=color,
                              ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color,
                              ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            # #####
            # if col == 3:
            #     max_xbin_centre = 10
            # #####

            # # if False:
            # boots = np.zeros((len(xedges) - 1, 3))
            # for i in range(len(xedges) - 1):
            #     boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

            # plot
            # if False:
            # axs[col].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
            #                       color=color, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle])
            axs[col].scatter(x[x > max_xbin_centre], y[x > max_xbin_centre],
                             color=color, marker=marker, s=3, alpha=1, zorder=4)#, rasterized=True)
            axs[col].scatter(x, y,
                             color=color, marker=marker, s=2, alpha=0.5,
                             linewidths=0, zorder=2, rasterized=True)
            # axs[col].errorbar(xcentres[mask], ymedians[mask],
            #                   c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            # axs[col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
            #                       color=color3, alpha=0.5, edgecolor='k', zorder=9)

            m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
            # mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)
            axs[col].axvline(m200_intercept, 0, 1,
                             color=color2, ls=linestyle2, linewidth=1, zorder=3)

        #model
            log_M200s = np.linspace(*xlims, 201) #log M_sun
            #cosmology
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3)) #km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200 = 200 * rho_crit
            r200 = np.power(3 * 10**(log_M200s-10) / (4 * np.pi * rho_200), 1 / 3)  # kpc

            #NFW
            conc = mgm.concentration_ludlow(z, 10**(log_M200s-10))
            r_s = r200 / conc #kpc

            r12_on_rm2 = 0.15

            # axs[col].errorbar(log_M200s, np.log10(r12_on_rm2 * r_s),
            #                   c=color3, ls=linestyle2, linewidth=2, zorder=-1, alpha=1)
            # for i in range(20):
            #     axs[col].errorbar(log_M200s, np.log10(r12_on_rm2 * r_s) + i/3 - 5,
            #                       c=color3, ls=linestyle2, linewidth=2, zorder=-1, alpha=1)

        #decorations
        frac_size = 8
        axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(EAGLE_EPS),
                       -(xlims[1] - xlims[0]) / frac_size, 0,
                       length_includes_head=True,
                       head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                       head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                       color='k', fc='white', lw=1, zorder=-15, alpha=1)
        axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(2.8*EAGLE_EPS),
                       -(xlims[1] - xlims[0]) / frac_size, 0,
                       length_includes_head=True,
                       head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                       head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                       color='k', fc='white', lw=1, zorder=-15, alpha=1)

        # if col in (0,1):
        if col == 0:
            axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                          np.log10(EAGLE_EPS) + 0.000 * (ylims[1] - ylims[0]),
                        r'$\epsilon$', va='bottom')

            if col == 3:
                axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                              np.log10(2.8 * EAGLE_EPS) - 0.015 * (ylims[1] - ylims[0]),
                            r'$2.8 \times \epsilon$', va='top')
            else:
                axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                              np.log10(2.8 * EAGLE_EPS) + 0.005 * (ylims[1] - ylims[0]),
                            r'$2.8 \times \epsilon$', va='bottom')

        # if col==0:
        show_r200 = False
        show_conc = False
        if show_r200:
            for mult in [0.1, 0.01]:
                axs[col].errorbar(log_M200s, np.log10(r200 * mult),
                                color='k', ls=(0,(1.5,1.8)), lw=1, zorder=2, path_effects=white_path_effect_3)
        if show_conc:
            for mult in [1, 0.1]:
                axs[col].errorbar(log_M200s, np.log10(r200 / conc * mult),
                                  color='k', ls=(0,(6,1.8)), lw=1, zorder=3, path_effects=white_path_effect_3)

        if col == 1:
        # if True:
            if show_r200:
                index = int(3.9 / 8 * len(log_M200s))
                slope = (np.diff(np.log10(0.1 * r200)) / np.diff(log_M200s))[index + 4] #-0.01
                axs[col].text(log_M200s[index] - 0.005 * (xlims[1] - xlims[0]),
                            np.log10(0.1 * r200[index]) - 0.02 * (ylims[1] - ylims[0]),
                            r'$0.1 \times r_{200}$', rotation_mode='anchor', ha='left', va='top',
                            rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims)) * 180 / np.pi)[0],
                            color='k', path_effects=white_path_effect_2)
                index = int(5.4 / 8 * len(log_M200s))
                slope = (np.diff(np.log10(0.01 * r200)) / np.diff(log_M200s))[index + 16]
                axs[col].text(log_M200s[index],
                            np.log10(0.01 * r200[index]) - 0.02 * (ylims[1] - ylims[0]),
                            r'$0.01 \times r_{200}$', rotation_mode='anchor', ha='left', va='top',
                            rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims)) * 180 / np.pi)[0],
                            color='k', path_effects=white_path_effect_2)

            if show_conc:
                index = int(1.0 / 8 * len(log_M200s))
                slope = (np.diff(np.log10(r200 / conc)) / np.diff(log_M200s))[index + 4] + 0.03
                axs[col].text(log_M200s[index] - 0.005 * (xlims[1] - xlims[0]),
                            np.log10(r200[index] / conc[index]) + 0.01 * (ylims[1] - ylims[0]),
                            r'$r_{200} / c$', rotation_mode='anchor', ha='left', va='bottom',
                            rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims)) * 180 / np.pi)[0],
                            color='k', path_effects=white_path_effect_2)
                index = int(1.0 / 8 * len(log_M200s))
                slope = (np.diff(np.log10(0.1 * r200 / conc)) / np.diff(log_M200s))[index + 16]
                axs[col].text(log_M200s[index],
                            np.log10(0.1 * r200[index]/conc[index]) - 0.02 * (ylims[1] - ylims[0]),
                            r'$0.1 \times r_{200} / c$', rotation_mode='anchor', ha='left', va='top',
                            rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims)) * 180 / np.pi)[0],
                            color='k', path_effects=white_path_effect_2)

        # for i in range(20):
        #     #1/3 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i/3 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) / 3,
        #                               i/3 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)


        if model:
            # fit
            with h5.File(name_pairs[0], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # load
                x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                y0 = raw_data0[keys.key0_dict['r12'][None]][:]
                # y0 = raw_data0[keys.key0_dict['R12'][None]][:]
                # mask and units
                x0 = np.log10(x0[mask0]) + 10
                y0 = np.log10(y0[mask0])
                # mask
                not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                x0 = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

            with h5.File(name_pairs[1], 'r') as raw_data0:
                # centrals only
                mask1 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # load
                x1 = raw_data0[keys.key0_dict['m200'][None]][:]
                y1 = raw_data0[keys.key0_dict['r12'][None]][:]
                # y1 = raw_data0[keys.key0_dict['R12'][None]][:]
                # mask and units
                x1 = np.log10(x1[mask1]) + 10
                y1 = np.log10(y1[mask1])
                # mask
                not_crazy_mask1 = np.logical_and(np.isfinite(x1), np.isfinite(y1))
                x1 = x1[not_crazy_mask1]
                y1 = y1[not_crazy_mask1]

            guess = [0, 0.1, 0.2, 0.4, 11.5]
            if z == 0:
                guess = [1.06, 0.41, 0.05, 0.42, 11.4]
            elif z == 0.503:
                guess = [0.91, 0.52, 0.06, 0.31, 11.0]
            elif z == 1.004:
                guess = [0.74, 0.60, 0.19, 0.22, 10.8]
            elif z == 2.012:
                # guess = [0.36, 0.58, 0.41, -0.07, 11.1]
                guess = [0.36, 0.58, 0.41, 0, 11.1]

            m200_lim = 10
            mask0 = np.logical_and(np.logical_and(x0 > m200_lim, np.isfinite(x0)), np.isfinite(y0))
            mask1 = np.logical_and(np.logical_and(x1 > m200_lim, np.isfinite(x1)), np.isfinite(y1))
            to_min = lambda args: np.log10(np.sum((size_function([args[0], args[1], args[3], args[4]],
                                                                 x0[mask0]) - y0[mask0]) ** 2) +
                                           np.sum((size_function([args[0], args[2], args[3], args[4]],
                                                                 x1[mask1]) - y1[mask1]) ** 2))

            if False:
                #fix outer slope to 0
                if col == 3:
                    to_min = lambda args: np.log10(np.sum((size_function([args[0], args[1], 0, args[4]],
                                                                         x0[mask0]) - y0[mask0]) ** 2) +
                                                   np.sum((size_function([args[0], args[2], 0, args[4]],
                                                                         x1[mask1]) - y1[mask1]) ** 2))

                out = minimize(to_min, guess, method='Nelder-Mead')
                print(out.x)
                if col != 3:
                    out0 = [out.x[0], out.x[1], out.x[3], out.x[4]]
                    out1 = [out.x[0], out.x[2], out.x[3], out.x[4]]
                else:
                    out0 = [out.x[0], out.x[1], 0, out.x[4]]
                    out1 = [out.x[0], out.x[2], 0, out.x[4]]
                print(out0)
                print(out1)
                print(out.message)

                ''' (fitted on 5/4/23)
                z=0: 7x, 1x
                #R_1/2:[1.0484, 0.40186, 0.42097, 11.720]
                #R_1/2:[1.0484, 0.05105, 0.42097, 11.720]
                [1.04716, 0.38767, 0.35636, 11.781]
                [1.04716, 0.04232, 0.35636, 11.781]
                z=0.5: 7x, 1x
                #R_1/2:[0.90386, 0.51727, 0.31391, 11.017]
                #R_1/2:[0.90386, 0.06164, 0.31391, 11.017]
                [0.94253, 0.47340, 0.291180, 11.1576]
                [0.94253, 0.06310, 0.291180, 11.1576]
                z=1: 7x, 1x
                #R_1/2:[0.73956, 0.59369, 0.21914, 10.804]
                #R_1/2:[0.73956, 0.18824, 0.21914, 10.804]
                [0.79859, 0.65058, 0.20914, 10.649]
                [0.79859, 0.14679, 0.20914, 10.649]
                z=2: 7x, 1x
                #R_1/2:[0.44219, 0.60950, 0, 10.962]
                #R_1/2:[0.44219, 0.41349, 0, 10.962]
                [0.51521, 0.60397, 0, 10.938]
                [0.51521, 0.39764, 0, 10.938]
                '''

            xs = np.linspace(*xlims, 201)

            out0 = mgm.size_function_args(z, True)
            out1 = mgm.size_function_args(z, False)

            axs[col].errorbar(xs, size_function(out0, xs), ls='-.', c='navy', lw=3, zorder=20,
                              )#path_effects=path_effect_4)
            axs[col].errorbar(xs, size_function(out1, xs), ls=(0, (0.8, 0.8)), c='saddlebrown', lw=3, zorder=20,
                              )#path_effects=path_effect_4)

            if False:
                #fix to Mo, Mao & White:
                to_min = lambda args: np.log10(np.sum((size_function([args[0], args[1], 1/3, args[3]],
                                                                     x0[mask0]) - y0[mask0]) ** 2) +
                                               np.sum((size_function([args[0], args[2], 1/3, args[3]],
                                                                     x1[mask1]) - y1[mask1]) ** 2))

                out = minimize(to_min, [guess[0], guess[1], guess[2], guess[4]], method='Nelder-Mead')
                print(out.x)
                out0 = [out.x[0], out.x[1], 1/3, out.x[3]]
                out1 = [out.x[0], out.x[2], 1/3, out.x[3]]
                print(out0)
                print(out1)
                print(out.message)

                axs[col].errorbar(xs, size_function(out0, xs), ls='-.', c='navy', lw=2, zorder=20)
                axs[col].errorbar(xs, size_function(out1, xs), ls=(0, (0.8, 0.8)), c='saddlebrown', lw=2, zorder=20)

                #fix to f * rs
                # cosmology
                H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3))  # km^2/s^2 / kpc^2
                rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                rho_200 = 200 * rho_crit
                r200 = np.power(3 * 10 ** (log_M200s - 10) / (4 * np.pi * rho_200), 1 / 3)  # kpc

                # NFW
                conc = mgm.concentration_ludlow(z, 10 ** (log_M200s - 10))
                r_s = r200 / conc  # kpc

                min_i = np.argmin(np.abs(12 - log_M200s))
                max_i = -1
                slope = (np.log10(r_s[max_i]) - np.log10(r_s[min_i])) / (log_M200s[max_i] - log_M200s[min_i]) #

                to_min = lambda args: np.log10(np.sum((size_function([args[0], args[1], slope, args[3]],
                                                                     x0[mask0]) - y0[mask0]) ** 2) +
                                               np.sum((size_function([args[0], args[2], slope, args[3]],
                                                                     x1[mask1]) - y1[mask1]) ** 2))

                out = minimize(to_min, [guess[0], guess[1], guess[2], guess[4]], method='Nelder-Mead')
                print(out.x)
                out0 = [out.x[0], out.x[1], slope, out.x[3]]
                out1 = [out.x[0], out.x[2], slope, out.x[3]]
                print(out0)
                print(out1)
                print(out.message)

                axs[col].errorbar(xs, size_function(out0, xs), ls='-.', c='cyan', lw=2, zorder=20)
                axs[col].errorbar(xs, size_function(out1, xs), ls=(0, (0.8, 0.8)), c='gold', lw=2, zorder=20)

                #fix to f * rs with contraction
                # cosmology
                H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3))  # km^2/s^2 / kpc^2
                rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                rho_200 = 200 * rho_crit
                r200 = np.power(3 * 10 ** (log_M200s - 10) / (4 * np.pi * rho_200), 1 / 3)  # kpc

                # NFW
                conc = mgm.concentration_ludlow(z, 10 ** (log_M200s - 10))
                r_s = r200 / conc  # kpc
                f_guess = 0.15

                r_DMO = f_guess * r_s

                Mstar = 10 ** (mass_function(mass_function_args(z, True), log_M200s))  # M_sun

                half = 0.5
                R_d = 3/4 * f_guess * r_s / (-lambertw((half - 1) / np.exp(1), -1) - 1).real

                r_estimate = np.array([mgm.find_contracted_radii(r_DMO[i], 10**log_M200s[i], r200[i],
                                                                 conc[i], Mstar[i], R_d[i])[0]
                                       for i in range(len(log_M200s))])

                min_i = np.argmin(np.abs(12 - log_M200s))
                max_i = -1
                slope = (np.log10(r_estimate[max_i]) - np.log10(r_estimate[min_i])) / (log_M200s[max_i] - log_M200s[min_i])

                to_min = lambda args: np.log10(np.sum((size_function([args[0], args[1], slope, args[3]],
                                                                     x0[mask0]) - y0[mask0]) ** 2) +
                                               np.sum((size_function([args[0], args[2], slope, args[3]],
                                                                     x1[mask1]) - y1[mask1]) ** 2))

                out = minimize(to_min, [guess[0], guess[1], guess[2], guess[4]], method='Nelder-Mead')
                print(out.x)
                out0 = [out.x[0], out.x[1], slope, out.x[3]]
                out1 = [out.x[0], out.x[2], slope, out.x[3]]
                print(out0)
                print(out1)
                print(out.message)

                axs[col].errorbar(xs, size_function(out0, xs), ls='-.', c='navy', lw=2, zorder=20)
                axs[col].errorbar(xs, size_function(out1, xs), ls=(0, (0.8, 0.8)), c='darkorange', lw=2, zorder=20)

                for i in range(20):
                    axs[col].errorbar(log_M200s, np.log10(r_estimate) + i/3 - 5,
                                      c='lime', ls=':', linewidth=1, zorder=-1, alpha=1)
                for i in range(20):
                    axs[col].errorbar(log_M200s, np.log10(f_guess * r_s) + i/3 - 5,
                                      c='red', ls=':', linewidth=1, zorder=-1, alpha=1)

        # #more decorations
        # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
        #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
        #                ],
        #               ['HR DM\nmedian', 'LR DM\nmedian'],
        #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
        #               loc='upper left')
        #
        # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
        #                (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls=(0, (0.8, 0.8)), lw=2)),
        #                ],
        #               ['HR DM\nmodel', 'LR DM\nmodel'],
        #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
        #               loc='upper left')
        #
        # axs[2].legend([(patches.Patch(facecolor='grey', edgecolor='k', alpha=0.5)),
        #                (patches.Patch(facecolor='grey', edgecolor='k', alpha=0.5, hatch='xxx'))],
        #               ['16th-84th\npercentile', 'Error on\nthe median'],
        #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
        #               loc='upper left')

    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    if len(axs) > 1:
        axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def stellar_mass_all_good(name='', save=False, show_diff=False):

    z_index = 0

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(3.3 * len(combined_name_pairs), 3*3.3 + 0.05, forward=True)
    # spec = fig.add_gridspec(ncols=len(combined_name_pairs), nrows=3)
    fig.set_size_inches(3.3, 3.3, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [5.1, 9.1]
    if show_diff:
        ylims = [-1.49,1.49]

    # N_run = np.linspace(9.5, 14, 10)
    # N_run = np.linspace(7, 12.5, 12)
    N_run = np.linspace(7.2, 14.2, 31)

    aspect = ['equal']

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    if show_diff:
        xlabel = r'$\log \, M_{200}^{\rm HR} / $M$_\odot$'
    ylabel = r'$\log \, M_{\rm BH} / $M$_\odot$'

    axs = []
    for row in range(1):
        axs.append([])
        for col in range(len([combined_name_pairs[z_index]])):
            #axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row].append(fig.add_subplot(spec[col // 2, col % 2]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if (col % 2) == 0: axs[row][col].set_ylabel(ylabel)
            else: axs[row][col].set_yticklabels([])

            if  (col // 2) == 0: axs[row][col].set_xlabel(xlabel)
            else: axs[row][col].set_xticklabels([])

            axs[row][col].set_aspect(aspect[row])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate([combined_name_pairs[z_index]]):
        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                marker = 'o'
                color = 'C0'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                marker = 'o'
                color = 'C1'
                label = 'LRDM'

            for row in range(1):
                with h5.File(name_pairs[pair], 'r') as raw_data0:
                    # centrals only
                    mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                    #load
                    # x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                    x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                    # y0 = raw_data0[keys.key0_dict['mbh'][None]][:]
                    # y0 = raw_data0[keys.key0_dict['mbh'][None]][:]

                    # y0 = raw_data0['GalaxyQuantities/BHBinGravMass'][()]
                    # y0 = raw_data0['GalaxyQuantities/BHBinMass'][()]
                    # y0 = raw_data0['GalaxyQuantities/BHGravMass'][()]
                    y0 = raw_data0['GalaxyQuantities/BHMass'][()]

                    #mask and units
                    x0 = np.log10(x0[mask0]) + 10
                    y0 = np.log10(y0[mask0]) + 10
                    # if row == 2:
                    #     y0 = 10**y0
                    #mask
                    not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                    x = x0[not_crazy_mask0]
                    y = y0[not_crazy_mask0]

                xd = (x + y)/2
                yd = (y - x)/2

                # calc medians etc.
                ydmedians, xdedges, bin_is = binned_statistic(xd, yd,
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xdcentres = 0.5 * (xdedges[1:] + xdedges[:-1])
                # boots = np.zeros((len(xedges) - 1, 3))
                # for i in range(len(xedges) - 1):
                #     boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

                counts, _ = np.histogram(xd, bins=N_run)
                # mask = counts > 5
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                # axs[row][col].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle],
                #                       label=label+' error on the median')
                axs[row][col].scatter(x, y,
                                 color=color, marker=marker, s=4, alpha=0.5, linewidths=0,
                                      rasterized=True)
                axs[row][col].errorbar(xdcentres[mask] - ydmedians[mask], xdcentres[mask] + ydmedians[mask],
                                  c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
                # axs[row][col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9,
                #                       label=label+' 16th-84th percentile')

                #decorations
                #no idea what this is
                m_gas_min = DM_MASS * (Omega_b / (Omega_m - Omega_b)) * 10**10
                averaged_time = 1
                axs[0][col].errorbar(xlims, np.log10(averaged_time * m_gas_min / 10**np.array(xlims)),
                                     c='k', ls=':', lw=1, zorder=-10)

                #
                axs[row][col].errorbar(xlims, [np.log10(Omega_b / Omega_m * 10 ** xlims[0]),
                                             np.log10(Omega_b / Omega_m * 10 ** xlims[1])],
                                     c='k', ls='-', lw=1, zorder=1, alpha=1)

                for i in range(-10, 10):
                    axs[row][col].errorbar(xlims, [np.log10(Omega_b / Omega_m * 10 ** xlims[0]) + i,
                                              np.log10(Omega_b / Omega_m * 10 ** xlims[1]) + i],
                                      c='grey', ls=':', lw=1, zorder=-10)

    axs[0][0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylimss[0][0] + 0.95 * (ylimss[0][1] - ylimss[0][0]),
    #             r'$z=0.5$', va='top', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylimss[0][0] + 0.95 * (ylimss[0][1] - ylimss[0][0]),
    #             r'$z=1$', va='top', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylimss[0][0] + 0.95 * (ylimss[0][1] - ylimss[0][0]),
    #             r'$z=2$', va='top', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def bh_mass_diff(name='', save=False):

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(3.3 * len(combined_name_pairs), 3*3.3 + 0.05, forward=True)
    # spec = fig.add_gridspec(ncols=len(combined_name_pairs), nrows=3)
    fig.set_size_inches(3.3, 3.3, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [10, 14]
    ylims = [-1.49, 1.49]
    # ylims = [-0.1, 0.1]

    N_run = np.linspace(9.5, 14, 10)
    # N_run = np.linspace(7, 12.5, 12)

    aspect = ['equal']

    xlabel = r'$\log \, M_{200}^{\rm HR} / $M$_\odot$'
    ylabel = r'$\log \, M_{\rm BH}^{\rm LR} / M_{\rm BH}^{\rm HR}$'

    axs = []
    for row in range(1):
        axs.append([])
        for col in range(len([combined_name_pairs[0]])):
            # axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row].append(fig.add_subplot(spec[col // 2, col % 2]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if (col % 2) == 0:
                axs[row][col].set_ylabel(ylabel)
            else:
                axs[row][col].set_yticklabels([])

            if (col // 2) == 0:
                axs[row][col].set_xlabel(xlabel)
            else:
                axs[row][col].set_xticklabels([])

            # axs[row][col].set_aspect(aspect[row])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    z_index = 0

    for col, name_pairs in enumerate([combined_name_pairs[z_index]]):
        for pair in range(1):
            linestyle = '-'
            marker = 'o'
            color = 'limegreen'
            color2 = 'purple'

            for row in range(1):
                with h5.File(name_pairs[pair], 'r') as raw_data0:
                    print(name_pairs[pair])
                    # centrals only
                    mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                    gn0 = raw_data0[keys.key0_dict['gn'][None]][:]
                    gn0 = gn0[mask0]
                    # load
                    # x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                    x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                    # y0 = raw_data0[keys.key0_dict['mbh'][None]][:]

                    gy0 = raw_data0['GalaxyQuantities/BHGravMass'][()] # 10^10 M_sun
                    y0 = raw_data0['GalaxyQuantities/BHMass'][()]  # 10^10 M_sun

                    # mask and units
                    x0 = np.log10(x0[mask0]) + 10
                    y0 = np.log10(y0[mask0]) + 10
                    gy0 = np.log10(gy0[mask0]) + 10

                with h5.File(name_pairs[1-pair], 'r') as raw_data1:
                    print(name_pairs[1-pair])
                    # centrals only
                    mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
                    gn1 = raw_data1[keys.key0_dict['gn'][None]][:]
                    gn1 = gn1[mask1]
                    # load
                    # x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                    x1 = raw_data1[keys.key0_dict['m200'][None]][:]
                    # y1 = raw_data1[keys.key0_dict['mbh'][None]][:]

                    gy1 = raw_data1['GalaxyQuantities/BHGravMass'][()] # 10^10 M_sun
                    y1 = raw_data1['GalaxyQuantities/BHMass'][()]  # 10^10 M_sun

                    # mask and units
                    x1 = np.log10(x1[mask1]) + 10
                    y1 = np.log10(y1[mask1]) + 10
                    gy1 = np.log10(gy1[mask1]) + 10

                with h5.File(matched_groups_files[z_index], 'r') as match_data:
                    print(matched_groups_files[z_index])
                    matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
                    matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

                # not all haloes have stars
                ni0 = np.isin(matched_gn_0, gn0)
                ni1 = np.isin(matched_gn_1, gn1)
                ni = np.logical_and(ni0, ni1)
                matched_gn_0 = matched_gn_0[ni]
                matched_gn_1 = matched_gn_1[ni]

                # not all haloes matched with stars
                in0 = np.isin(gn0, matched_gn_0)
                in1 = np.isin(gn1, matched_gn_1)

                order0 = np.argsort(np.argsort(matched_gn_0))
                order1 = np.argsort(np.argsort(matched_gn_1))

                x0 = x0[in0][order0]
                y0 = y0[in0][order0]
                gy0 = gy0[in0][order0]
                x1 = x1[in1][order1]
                y1 = y1[in1][order1]
                gy1 = gy1[in1][order1]

                #grav
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x0, gy1 - gy0,
                                                            statistic=np.nanmedian, bins=N_run)
                yplus, xedges, _ = binned_statistic(x0, gy1 - gy0,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x0, gy1 - gy0,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])
                # boots = np.zeros((len(xedges) - 1, 3))
                # for i in range(len(xedges) - 1):
                #     boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

                counts, _ = np.histogram(x0, bins=N_run)
                # mask = counts > 10
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                # axs[row][col].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle],
                #                       label=label+' error on the median')
                axs[row][col].scatter(x0, gy1 - gy0,
                                      color=color2, marker=marker, s=3, alpha=0.5, rasterized=True)
                axs[row][col].errorbar(xcentres[mask], ymedians[mask],
                                       c=color2, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                                       path_effects=path_effect_4)
                axs[row][col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
                                           color=color2, alpha=0.5, edgecolor='k', zorder=9)

                #actual
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x0, y1 - y0,
                                                              statistic=np.nanmedian, bins=N_run)
                yplus, xedges, _ = binned_statistic(x0, y1 - y0,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x0, y1 - y0,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])
                # boots = np.zeros((len(xedges) - 1, 3))
                # for i in range(len(xedges) - 1):
                #     boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

                counts, _ = np.histogram(x0, bins=N_run)
                # mask = counts > 10
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                # axs[row][col].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle],
                #                       label=label+' error on the median')
                axs[row][col].scatter(x0, y1 - y0,
                                      color=color, marker=marker, s=3, alpha=0.2, rasterized=True)
                axs[row][col].errorbar(xcentres[mask], ymedians[mask],
                                       c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                                       path_effects=path_effect_4)
                axs[row][col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
                                      color=color, alpha=0.3, edgecolor='k', zorder=9)

                # decorations

    axs[0][0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                   r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylimss[0][0] + 0.95 * (ylimss[0][1] - ylimss[0][0]),
    #             r'$z=0.5$', va='top', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylimss[0][0] + 0.95 * (ylimss[0][1] - ylimss[0][0]),
    #             r'$z=1$', va='top', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylimss[0][0] + 0.95 * (ylimss[0][1] - ylimss[0][0]),
    #             r'$z=2$', va='top', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def stellar_mass_all_well(name='', save=False):

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(3.3 * 6/4, 3.3 * 6/4 + 0.06, forward=True)
    fig.set_size_inches(3.3, 3.3, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    # xlims = [1e-3 + 9, 15 - 1e-3]
    # ylims = [1e-3 + 7, 13 - 1e-3]

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [1e-3 + 8.5, 12.5 - 1e-3]
    # ylims = [1e-3 + 6, 10 - 1e-3]

    N_run = np.linspace(7.5, 13, 12)

    aspect = [1] #[np.diff(xlims)[0] / np.diff(ylims)[0]]

    xlabel = r'$\log \, M_{200} / $M$_\odot$' #r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'$\log \, M_{\rm gas} / $M$_\odot$'

    axs = []
    for row in range(1):
        axs.append([])
        for col in range(len([combined_name_pairs[0]])):
            #axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row].append(fig.add_subplot(spec[col // 2, col % 2]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if (col % 2) == 0: axs[row][col].set_ylabel(ylabel)
            else: axs[row][col].set_yticklabels([])

            if  (col // 2) == 0: axs[row][col].set_xlabel(xlabel)
            else: axs[row][col].set_xticklabels([])

            axs[row][col].set_aspect(aspect[row])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate([combined_name_pairs[0]]):
        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                marker = 'o'
                color = 'C0'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                marker = 'o'
                color = 'C1'
                label = 'LRDM'

            for row in range(1):
                with h5.File(name_pairs[pair], 'r') as raw_data0:
                    # centrals only
                    mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                    #load
                    x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                    y0 = raw_data0[keys.key0_dict['mgas'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                    # y0 = raw_data0['GalaxyProfiles/SFGas/BinMass'][:, keys.rkey_to_rcolum_dict['r200', None]]

                    #mask and units
                    x0 = np.log10(x0[mask0]) + 10
                    y0 = np.log10(y0[mask0])
                    if row == 0:#1:
                        y0 += 10
                    # if row == 2:
                    #     y0 = 10**y0
                    #mask
                    not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                    x = x0[not_crazy_mask0]
                    y = y0[not_crazy_mask0]

                xd = (x + y)/2
                yd = (y - x)/2

                # calc medians etc.
                ydmedians, xdedges, bin_is = binned_statistic(xd, yd,
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xdcentres = 0.5 * (xdedges[1:] + xdedges[:-1])
                # boots = np.zeros((len(xedges) - 1, 3))
                # for i in range(len(xedges) - 1):
                #     boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

                counts, _ = np.histogram(xd, bins=N_run)
                # mask = counts > 10
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                # axs[row][col].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9.3, hatch=hatches[linestyle],
                #                       label=label+' error on the median')
                axs[row][col].scatter(x, y,
                                 color=color, marker=marker, s=4, alpha=0.5, linewidths=0, rasterized=True)
                axs[row][col].errorbar(xdcentres[mask] - ydmedians[mask], xdcentres[mask] + ydmedians[mask],
                                  c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
                # axs[row][col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9,
                #                       label=label+' 16th-84th percentile')

                #decorations
                axs[row][col].errorbar(xlims, [np.log10(Omega_b / Omega_m * 10 ** xlims[0]),
                                             np.log10(Omega_b / Omega_m * 10 ** xlims[1])],
                                     c='k', ls='-', lw=1, zorder=1, alpha=1)

                for i in range(-10, 10):
                    axs[row][col].errorbar(xlims, [np.log10(Omega_b / Omega_m * 10 ** xlims[0]) + i,
                                              np.log10(Omega_b / Omega_m * 10 ** xlims[1]) + i],
                                      c='grey', ls=':', lw=1, zorder=-10)

                axs[0][col].text(np.mean(xlims), np.mean([np.log10(Omega_b/Omega_m * 10**xlims[0]),
                                                          np.log10(Omega_b/Omega_m * 10**xlims[1])])
                                 + 0.005 * (ylims[1] - ylims[0]),
                                 r'$M_{\rm gas} / M_{200} = \Omega_b / \Omega_m$',
                                 rotation_mode='anchor', ha='center', va='bottom',
                                 rotation=(np.arctan(aspect[0])*180/np.pi),
                                 path_effects=white_path_effect_2)


    axs[0][0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
    #             r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
    #             r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[0][3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
    #             r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def dispersion_mass_relation(name='', save=False, sfgas=True,
                             plot_dm_measure=False, ek=False, contracted=True, star_jeans=False, sigma_z=False):
                             # plot_dm_measure=False, ek=True, contracted=False, star_jeans=False, sigma_z=False):
    N_res = 2

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    xlims = [1e-3 + 10, 14 - 1e-3]
    # ylims = [1e-3 + -0.4, 0.35 - 1e-3]
    ylims = [1e-3 + -0.6, 0.4 - 1e-3]
    if sigma_z:
        ylims[0] -= np.log10(np.sqrt(3))
        ylims[1] -= np.log10(np.sqrt(3))

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9+2/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, \sigma_{\rm tot} (r_{1/2}) / V_{200}$'
    if sigma_z:
        ylabel = r'$\log \, \sigma_z (r_{1/2}) / V_{200}$'
    if ek:
        ylabel = r'$\log \, \sqrt{2 E_k} (r_{1/2}) / V_{200}$'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(4)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                color4 = 'chartreuse'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                color4 = 'fuchsia'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                r200 = raw_data0[keys.key0_dict['r200'][None]][:]
                sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                sR = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                sphi = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                Mstar0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                r120 = raw_data0[keys.key0_dict['r12'][None]][:]

                #add kinetic energy because that's what Arron has done
                if ek:
                    vphi = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                if plot_dm_measure:
                    dsz = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                    dsy = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                    dsx = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                if sfgas:
                    sfz = raw_data0[keys.key0_dict['sigma_z']['SFGas']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                    sfR = raw_data0[keys.key0_dict['sigma_R']['SFGas']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                    sfphi = raw_data0[keys.key0_dict['sigma_phi']['SFGas']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                    if ek:
                        vfphi = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                half_age0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['eq_r12', None], 2]

                #mask and units
                Mstar0 = Mstar0[mask0]
                r120 = r120[mask0]

                V200 = np.sqrt(GRAV_CONST * M200 / r200) # km/s
                x0 = np.log10(M200[mask0]) + 10 # log Mstar
                y0 = np.log10(np.sqrt(sz[mask0]**2 + sR[mask0]**2 + sphi[mask0]**2) / V200[mask0])
                if ek:
                    y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2
                                          + vphi[mask0]**2) / V200[mask0])
                if sigma_z:
                    y0 = np.log10(sz[mask0] / V200[mask0])

                if sfgas:
                    sy0 = np.log10(np.sqrt(sfz[mask0]**2 + sfR[mask0]**2 + sfphi[mask0]**2) / V200[mask0])
                    if ek:
                        sy0 = np.log10(np.sqrt(sfz[mask0] ** 2 + sfR[mask0] ** 2 + sfphi[mask0] ** 2
                                              + vfphi[mask0]**2) / V200[mask0])
                    if sigma_z:
                        sy0 = np.log10(sfz[mask0] / V200[mask0])

                N0 = N0[mask0]

                if plot_dm_measure:
                    dmy0 = np.log10(np.sqrt(dsz[mask0]**2 + dsy[mask0]**2 + dsx[mask0]**2) / V200[mask0])
                half_age0 = half_age0[mask0] #Gyr

                #mask
                not_crazy_mask0 = N0 > N_res
                x = x0[not_crazy_mask0]
                y = y0[not_crazy_mask0]
                Mstar = Mstar0[not_crazy_mask0]
                r12 = r120[not_crazy_mask0]
                age = half_age0[not_crazy_mask0] #Gyr

                if plot_dm_measure:
                    dmy = dmy0[not_crazy_mask0]

                if sfgas:
                    sy = sy0[not_crazy_mask0]

            #don't want a fancy model for ages
            agemedians, _, dmbin_is = binned_statistic(x, age,
                                                       statistic=np.nanmedian, bins=N_run)
            agemedians[np.isnan(agemedians)] = agemedians[np.logical_not(np.isnan(agemedians))][-1]

            agemedians = np.median(age[x > 10.5])
            print('age medians', agemedians)
            agemedians = agemedians * np.ones(len(N_run)-1)

            # interp_age = interp1d(0.5 * (N_run[1:] + N_run[:-1]), UNIVERSE_AGE-agemedians, fill_value='extrapolate')
            interp_age = interp1d(0.5 * (N_run[1:] + N_run[:-1]), agemedians - keys.lbt(np.array([1 / (1 + z)]))[0],
                                  fill_value='extrapolate')

            #contraction
            # log_M200s = np.linspace(*xlims, 101) #log M_sun
            log_M200s = np.linspace(*xlims, 21) #log M_sun
            # log_M200s = (N_run[1:] + N_run[:-1])/2 #log M_sun
            #cosmology
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3)) #km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200 = 200 * rho_crit
            r200 = np.power(3 * 10**(log_M200s-10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
            V200 = np.sqrt(GRAV_CONST * 10**(log_M200s-10) / r200)  # km/s

            # Mstars, _,_ = binned_statistic(x, Mstar, statistic=np.nanmedian, bins=N_run)
            # r12s, _,_ = binned_statistic(x, r12, statistic=np.nanmedian, bins=N_run)
            #
            # #To stop things breaking
            # Mstars[np.isnan(Mstars)] = 1
            # r12s[np.isnan(r12s)] = 2.8 * EAGLE_EPS

            if pair: MassDM = DM_MASS
            else: MassDM = DM_MASS / 7

            # #uncontracted (pure NFW)
            # if not contracted:
            #     if pair == 0:
            #         out = np.array([mgm.get_heating_at_r12(10**(log_M200s[i]-10), MassDM=MassDM, z=z,
            #                                                time=interp_age(log_M200s[i]), contracted=not contracted)
            #                         for i in range(len(log_M200s))])
            #         (analytic_v_circ, analytic_dispersion,
            #          theory_best_v, theory_best_z, theory_best_r, theory_best_p,
            #          stellar_dispersion, power_law_z, power_law_r, power_law_p
            #         ) = out.T
            #
            #         if pair == 0:
            #             sqrt3 = np.sqrt(3)
            #             if sigma_z: sqrt3 = 1
            #             axs[col].errorbar(log_M200s, np.log10(analytic_dispersion * sqrt3 / V200),
            #                               c=color3, ls=linestyle2, linewidth=2, zorder=4, alpha=1)

            #contracted
            out = np.array([mgm.get_heating_at_r12(10**(log_M200s[i]-10), MassDM=MassDM, z=z,
                                                   time=interp_age(log_M200s[i]), contracted=contracted,)
                                                   # Mstar = Mstars[i], r12 = r12s[i])
                            for i in range(len(log_M200s))])
            (analytic_v_circ, analytic_dispersion,
             theory_best_v, theory_best_z, theory_best_r, theory_best_p,
             stellar_dispersion, power_law_z, power_law_r, power_law_p
             ) = out.T

            if pair == 0:
                sqrt3 = np.sqrt(3)
                if sigma_z: sqrt3 = 1
                axs[col].errorbar(log_M200s, np.log10(analytic_dispersion * sqrt3 / V200),
                                  c=color2, ls=linestyle2, linewidth=2, zorder=4, alpha=1)

            #This line is interesting although distracts from the point
            if star_jeans:
                sqrt3 = np.sqrt(3)
                if sigma_z: sqrt3 = 1
                axs[col].errorbar(log_M200s, np.log10(stellar_dispersion * sqrt3 / V200),
                                  c=color2, ls=linestyle2, linewidth=2, zorder=4, alpha=1)

            theory_best_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)
            # power_law_tot = np.sqrt(power_law_z**2 + power_law_r**2 + power_law_p**2)
            #TODO This looks really weird for ek...
            # if ek: theory_best_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2 +
            #                                  theory_best_v**2)
            if sigma_z: theory_best_tot = theory_best_z

            axs[col].errorbar(log_M200s, np.log10(theory_best_tot / V200),
                              c=color, ls=linestyle2, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            heating_limit = interp1d(log_M200s, np.log10(theory_best_tot / V200), fill_value='extrapolate')

            sqrt3 = np.sqrt(3)
            if sigma_z: sqrt3 = 1
            nfw_frac = interp1d(log_M200s, theory_best_tot / (analytic_dispersion * sqrt3),
                                fill_value='extrapolate')
            limit = lambda log_M200: nfw_frac(log_M200) - 0.95
            try:
                m200_all_heated = brentq(limit, 9.5, 13)
            except ValueError:
                m200_all_heated = 9.5

            # plot no cut
            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 20 * np.diff(N_run)[0]
            max_xbin_centre = xcentres[:-1][np.logical_not(mask)[1:]][0]

            y_med_interp = interp1d(xcentres[np.isfinite(ymedians)], ymedians[np.isfinite(ymedians)],
                                    fill_value='extrapolate')

            heated_intercept_func = lambda log_M200: y_med_interp(log_M200) - heating_limit(log_M200)
            try:
                m200_intercept = brentq(heated_intercept_func, 10, 14)
            except ValueError:
                m200_intercept = 9.5
            print(m200_intercept)

            '''evaluated 19/4/23
            z=0, 7x, 1x,
            11.004316932647056
            11.538391889695763
            z=0.5, 7x, 1x,
            10.797432961908406
            11.274839365561101
            z=1, 7x, 1x,
            10.611085228618078
            11.07266393992926
            z=2, 7
            10.355332404370081
            10.767682642030607
            '''
            # plot
            x_converged = np.linspace(m200_intercept, max_xbin_centre, 51)
            axs[col].errorbar(x_converged , y_med_interp(x_converged),
                              c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)
            axs[col].errorbar(xcentres[mask], ymedians[mask],
                              c=color, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)

            if col == 0: h = 0.85
            else: h = 0.77
            axs[col].axvline(m200_intercept, 0, h,
                             c=color2, ls=linestyle2, linewidth=1, zorder=4.5, alpha=1)

            #DM
            if plot_dm_measure:
                dmymedians, _, dmbin_is = binned_statistic(x, dmy,
                                                           statistic=np.nanmedian, bins=N_run)
                dmyplus, _, _ = binned_statistic(x, dmy,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                dmyminus, _, _ = binned_statistic(x, dmy,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)

                axs[col].errorbar(xcentres[mask], dmymedians[mask],
                                  c=color2, ls=linestyle, linewidth=2, zorder=6, alpha=1, path_effects=path_effect_4)
                axs[col].scatter(x[x > max_xbin_centre], dmy[x > max_xbin_centre],
                                 color=color2, marker=marker, s=3, alpha=1)#, rasterized=True)
                axs[col].fill_between(xcentres[mask], dmyminus[mask], dmyplus[mask],
                                      color=color2, alpha=0.5, edgecolor='k', zorder=5)

            if sfgas:
                symedians, _, sbin_is = binned_statistic(x, sy,
                                                           statistic=np.nanmedian, bins=N_run)
                smyplus, _, _ = binned_statistic(x, sy,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                smyminus, _, _ = binned_statistic(x, sy,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)

                axs[col].errorbar(xcentres[mask], symedians[mask],
                                  c=color4, ls=linestyle, linewidth=2, zorder=6, alpha=1, path_effects=path_effect_4)

            # plot with cut
            not_heated_mask = np.logical_and(y > heating_limit(x), x > m200_all_heated)
            # not_heated_mask = y > heating_limit(x)

            #plot heated galaxies only
            axs[col].scatter(x[np.logical_not(not_heated_mask)], y[np.logical_not(not_heated_mask)],
                             color=color, marker=marker, s=0.6, alpha=0.3,
                             linewidths=0, zorder=0, rasterized=True)

            x = x[not_heated_mask]
            y = y[not_heated_mask]

            # plot
            axs[col].scatter(x, y,
                             color=color, marker=marker, s=4, alpha=0.5,
                             linewidths=0, zorder=2, rasterized=True)
            # axs[col].errorbar(xcentres[mask], ymedians[mask],
            #                   c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            # axs[col].fill_between(xcentres[mask], yminus[mask], yplus[mask],
            #                       color=color, alpha=0.5, edgecolor='k', zorder=9)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
                   ],
                  [r'$\sigma_{\rm DM}$ model'],
                  loc='upper left')#, frameon=False)

    axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(4),
                                 lw=0, alpha=0.5, mew=0)),
                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(0.6),
                                 lw=0, alpha=0.3, mew=0)),],
                  [r'Converged galaxies', r'Unconverged galaxies'],
                  numpoints = 3,
                  loc='upper left')#, frameon=False

    axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
                   ],
                  ['HR DM median', 'LR DM median'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  loc='upper left')#, frameon=False)

    axs[3].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-.', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls=(0, (0.8, 0.8)), lw=2, path_effects=path_effect_4)),
                   ],
                  ['HR spurious heating', 'LR spurious heating'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  loc='upper left')#, frameon=False)

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def sf_gas_density_mass(name='', save=False, all_z=True, r200_app=True):
    N_res = 2

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
        xlims = [1e-3 + 10, 14 - 1e-3]
        # xlims = [1e-3 + 8, 12 - 1e-3]
        if r200_app:
            ylims = [1e-3 -2.0, 0.5 - 1e-3]
        else:
            ylims = [1e-3 -2.0, 0.5 - 1e-3]
    else:
        # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        fig.set_size_inches(2.5, 2.5 + 0.03, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[0]]
        xlims = [10, 14]
        # xlims = [8, 12]
        if r200_app:
            ylims = [-2.0, 0.5]
        else:
            ylims = [-2.0, 0.5]

    N_run = np.linspace(9.75, 14, 18) #-2
    # N_run = np.linspace(9.8, 14, 22) #-2
    # N_run = np.linspace(8.5, 14, 10) #-2

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    # xlabel = r'$\log \, M_\star / $M$_\odot$'
    # ylabel = r'$\log \, \rho_{\rm SF \, gas} / 10^{10} {\rm M}_\odot {\rm Mpc}^{-3}$'

    if r200_app:
        rkey = 'r200'
        ylabel = r'$\log \, \rho_{\rm SF} / {\rm cm}^{-3}$'
    else:
        # rkey = 'eq_r12'
        rkey = 'eq_R12'
        ylabel = r'$\log \, \rho_{\rm SF} (r = r_{1/2, \rm gas}) / {\rm cm}^{-3}$'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(1/2)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask0 = np.ones(len(raw_data0[keys.key0_dict['sn'][None]][:]), dtype=bool)

                # print(keys.rkey_to_rcolum_dict['r200', None], keys.rkey_to_rcolum_dict[rkey, None])

                #load
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                density_percentiles = raw_data0['GalaxyProfiles/Gas/PercentileSFDensity'][:,
                                                keys.rkey_to_rcolum_dict[rkey, None], :]

                N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, keys.rkey_to_rcolum_dict[rkey, None]]

                #mask and units
                x0 = np.log10(M200[mask0]) + 10 # log Mstar
                # x0 = np.log10(mstar[mask0]) + 10 # log Mstar
                y0 = np.log10(density_percentiles[mask0] * keys.N_H1_PER_g)

                N0 = N0[mask0]

                # #mask
                not_crazy_mask0 = N0 > N_res
                x = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

            # calc medians etc.2
            for percenile_index in range(3):
                # marker = ['^', 'o', 'v'][percenile_index]
                # linestyle = [[(0,(3, 1, 1, 1)), ((0,(2, 1, 2, 1)))],
                #              [(0,(6, 2, 2, 2)), ((0,(4, 2, 4, 2)))],
                #              [(0,(12, 4, 4, 4)), ((0,(8, 4, 8, 4)))]][percenile_index][pair]
                # cmap = ['Blues', 'Oranges'][pair]
                # color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1)/3)

                marker = ['^', 'o', 'v'][percenile_index]
                linestyle = [['-', '--'],
                             ['-', '--'],
                             ['-', '--']][percenile_index][pair]
                color = ['C0', 'C1'][pair]

                y = y0[:, percenile_index]

                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                yplus, xedges, _ = binned_statistic(x, y,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x, y,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 10#2

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color, ls=linestyle, linewidth=2, zorder=2, alpha=1, path_effects=path_effect_4)

                if percenile_index == 1: # and pair == 0:
                    axs[col].scatter(x, y,
                                     color=color, marker=marker, s=4, alpha=0.5,
                                     linewidths=0, zorder=2, rasterized=True)

        # for i in range(20):
        #     #2 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i * 1/3 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 1/3,
        #                               i * 1/3 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
    #                ],
    #               ['HR DM', 'LR DM'],
    #               # handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='upper left')#, frameon=False)


    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    # marker = ['^', 'o', 'v'][percenile_index]
    # linestyle = [[(0, (3, 1, 1, 1)), ((0, (2, 1, 2, 1)))],
    #              [(0, (6, 2, 2, 2)), ((0, (4, 2, 4, 2)))],
    #              [(0, (12, 4, 4, 4)), ((0, (8, 4, 8, 4)))]][percenile_index][pair]

    if all_z: col = 3
    else: col = 0

    # for percenile_index in range(3):
    #     cmap = ['Blues', 'Oranges'][1]
    #     color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1) / 3)
    #     label = [r'20th percentile', r'50th percentile', r'80th percentile'][percenile_index]
    #
    #     dx = 0.97
    #     dy = 0.18 - (0.07 * percenile_index)
    #
    #     x = xlims[0] + dx * (xlims[1] - xlims[0])
    #     y = ylims[0] + dy * (ylims[1] - ylims[0])
    #     xc = xlims[0] + (dx + 0.2) * (xlims[1] - xlims[0])
    #     yc = ylims[0] + (dy + 0.1) * (ylims[1] - ylims[0])
    #     dxc = -5
    #     dyc = -0.0825 * (ylims[1] - ylims[0])
    #
    #     axs[col].text(x, y, label,
    #                 ha='right', c=color, path_effects=path_effect_1)
    #
    #     cmap = ['Blues', 'Oranges'][0]
    #     color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1) / 3)
    #
    #     t = axs[col].text(x, y, label,
    #                     ha='right', c=color, path_effects=path_effect_1)
    #
    #     patch = patches.Rectangle((xc, yc), dxc, dyc, transform=axs[col].transData)
    #     t.set_clip_on(True)
    #     t.set_clip_path(patch)

    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def sf_temp(name='', save=False, all_z=False):
    N_res = 1
    z_index = 0

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        fig.set_size_inches(2.5, 2.5 + 0.03, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[z_index]]

#    xlims = [1e-3 + 10, 14 - 1e-3]
#     xlims = [1e-3 + 8, 12 - 1e-3]
    xlims = [8, 12]

    ylims = [3.5, 4.5]

    # N_run = np.linspace(7.8, 12, 22)
    N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(7.5, 12, 10)

    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    xlabel = r'$\log \, M_\star / $M$_\odot$'
    # ylabel = r'$\log \, \rho_{\rm SF \, gas} / 10^{10} {\rm M}_\odot {\rm Mpc}^{-3}$'

    rkey = 'r200'
    ylabel = r'$\log \, \, \overline{T}_{\rm SF} / {\rm K}$'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xticks([8,9,10,11,12])
        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(1/2)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask0 = np.ones(len(raw_data0[keys.key0_dict['sn'][None]][:]), dtype=bool)

                # print(keys.rkey_to_rcolum_dict['r200', None], keys.rkey_to_rcolum_dict[rkey, None])

                #load
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]

                sfgas_M = raw_data0['GalaxyProfiles/SFGas/BinMass'][:,
                        keys.rkey_to_rcolum_dict[rkey, None]]  # 10^10 M_sun
                sfgas_MT = raw_data0['GalaxyProfiles/SFGas/MassTemperature'][:,
                         keys.rkey_to_rcolum_dict[rkey, None]]  # 10^10 M_sun k

                N0 = raw_data0[keys.key0_dict['Npart']['SFGas']][:, keys.rkey_to_rcolum_dict[rkey, None]]

                #mask and units
                # x0 = np.log10(M200[mask0]) + 10 # log Mstar
                x0 = np.log10(mstar[mask0]) + 10 # log Mstar
                y0 = np.log10(sfgas_MT[mask0] / sfgas_M[mask0])

                N0 = N0[mask0]

                # #mask
                not_crazy_mask0 = N0 > N_res
                x = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

            color = ['C0', 'C1'][pair]
            linestyle = ['-', '--'][pair]
            marker = 'o'
            y = y0

            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 20 * np.diff(N_run)[0]

            # plot
            axs[col].errorbar(xcentres[mask], ymedians[mask],
                              c=color, ls=linestyle, linewidth=2, zorder=2, alpha=1, path_effects=path_effect_4)

            axs[col].scatter(x, y,
                             color=color, marker=marker, s=4, alpha=0.5,
                             linewidths=0, zorder=2, rasterized=True)

            nd_x0 = x[y > ylims[1]]
            axs[col].scatter(nd_x0, np.ones(len(nd_x0)) * (ylims[0] + 0.97 * (ylims[1] - ylims[0])),
                             color=color, marker=r'$\uparrow$', s=6 ** 2, alpha=0.5,
                             # linewidths=0,
                             zorder=1, rasterized=True)

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', va='bottom', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if all_z: col = 3
    else: col = 0

    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', va='bottom', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', va='bottom', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', va='bottom', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300, pad_inches=0.05)
        plt.close()

    return


def formation_time(name='', save=False, all_z=False):
    N_res = 2

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
        # xlims = [1e-3 + 10, 14 - 1e-3]
        xlims = [1e-3 + 8, 12 - 1e-3]
    else:
        # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        fig.set_size_inches(2.5, 2.5 + 0.03, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[0]]
        # xlims = [10, 14]
        xlims = [8, 12]

    ylims = [0, UNIVERSE_AGE]

    # N_run = np.linspace(9.75, 14, 18)
    N_run = np.linspace(9.8, 14, 22) - 2
    # N_run = np.linspace(8.5, 14, 10)

    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    xlabel = r'$\log \, M_\star (z=0) / $M$_\odot$'
    ylabel = r'Stellar age [Gyr]'
    ylabel2 = r'Stellar age $[z]$'
    # ylabel = r'$t \, [{\rm Gyr}]$'
    # ylabel2 = r'$z$'

    if all_z:
        xlabel = r'$\log \, M_\star (z=z) / $M$_\odot$'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (all_z and (col % 2) == 1) or not all_z:
            axs2 = axs[col].twinx()
            #scale factor
            axs2.set_ylim([0,1])
            z_numbers = np.array([0, 0.5, 1, 2, 4, 8])
            z_labels = ['0', '0.5', '1', '2', '4', '8']

            t_norm = mgm.lbt(1 / (1 + z_numbers)) / UNIVERSE_AGE

            axs2.set_yticks(t_norm)
            axs2.set_yticklabels(z_labels)
            axs2.set_ylabel(ylabel2)

        axs[col].set_xticks([8,9,10,11,12])
        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(1/2)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            with h5.File(name_pairs[pair], 'r') as raw_data0:
                print(name_pairs[pair])
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask0 = raw_data0[keys.key0_dict['sn'][None]][:] != 0
                # mask0 = np.ones(len(raw_data0[keys.key0_dict['sn'][None]][:]), dtype=bool)

                #load
                Mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                age_percentiles = raw_data0['GalaxyProfiles/Star/PercentileStellarAge'][:,
                                                keys.rkey_to_rcolum_dict['r200', None], :]

                N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, keys.rkey_to_rcolum_dict['r200', None]]

                #mask and units
                x0 = np.log10(Mstar[mask0]) + 10 # log Mstar
                # x0 = np.log10(M200[mask0]) + 10 # log Mstar
                y0 = age_percentiles[mask0]

                N0 = N0[mask0]

                # #mask
                not_crazy_mask0 = N0 > N_res
                x = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

            # calc medians etc.2
            for percenile_index in range(5):
                # marker = ['d', '^', 'o', 'v', '*'][percenile_index]
                # linestyle = [[(0,(1.5,0.5,0.5,0.5)), ((0,(1,0.5,1,0.5)))],
                #              [(0,(3, 1, 1, 1)), ((0,(2, 1, 2, 1)))],
                #              [(0,(6, 2, 2, 2)), ((0,(4, 2, 4, 2)))],
                #              [(0,(12, 4, 4, 4)), ((0,(8, 4, 8, 4)))],
                #              [(0,(24, 8, 8, 8)), ((0,(16, 8, 16, 8)))]][percenile_index][pair]
                # cmap = ['Blues', 'Oranges'][pair]
                # color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1)/5)

                marker = ['','','o','',''][percenile_index]
                linestyle = ['-', '--'][pair]
                color = ['C0', 'C1'][pair]

                y = y0[:, percenile_index]

                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                yplus, xedges, _ = binned_statistic(x, y,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x, y,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 2
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color, ls=linestyle, linewidth=2, zorder=2, alpha=1, path_effects=path_effect_4)

                axs[col].scatter(x, y,
                                 color=color, marker=marker, s=4, alpha=0.5,
                                 linewidths=0, zorder=2, rasterized=True)

        min_t = mgm.lbt(np.array([1 / (1 + z)]))[0]
        axs[col].axhline(min_t, 0, 1,
                         color='k', ls=':', lw=1, zorder=-5)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
    #                ],
    #               ['HR DM', 'LR DM'],
    #               # handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='upper left')#, frameon=False)

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', va='bottom', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    # for percenile_index in range(5):
    #     cmap = ['Blues', 'Oranges'][1]
    #     color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1) / 5)
    #     if all_z:
    #         label = [r'10th percentile', r'25th percentile', r'50th percentile',
    #                  r'75th percentile', r'90th percentile'][percenile_index]
    #         col=1
    #     else:
    #         label = [r'10th', r'25th', r'50th',
    #                  r'75th', r'90th'][percenile_index]
    #         col=0
    #
    #     dx = 0.97
    #     dy = (0.04 + 0.07*(5-1)) - (0.07 * percenile_index)
    #
    #     x = xlims[0] + dx * (xlims[1] - xlims[0])
    #     y = ylims[0] + dy * (ylims[1] - ylims[0])
    #     xc = xlims[0] + (dx + 0.2) * (xlims[1] - xlims[0])
    #     yc = ylims[0] + (dy + 0.1) * (ylims[1] - ylims[0])
    #     dxc = -5
    #     dyc = -0.0825 * (ylims[1] - ylims[0])
    #
    #     axs[col].text(x, y, label,
    #                 ha='right', c=color, path_effects=path_effect_1)
    #
    #     cmap = ['Blues', 'Oranges'][0]
    #     color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1) / 5)
    #
    #     t = axs[col].text(x, y, label,
    #                     ha='right', c=color, path_effects=path_effect_1)
    #
    #     patch = patches.Rectangle((xc, yc), dxc, dyc, transform=axs[col].transData)
    #     t.set_clip_on(True)
    #     t.set_clip_path(patch)

    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300, pad_inches=0.05)
        plt.close()

    return


def star_formation_rate(name='', save=False, all_z=True):
    N_res = 2

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
        xlims = [1e-3 + 8, 12 - 1e-3]
    else:
        fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[0]]
        xlims = [8, 12]

    ylims = [-3.8, 0.2]

    # N_run = np.linspace(9.75, 14, 18)
    N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(8.5, 14, 10)

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'sSFR [Gyr$^{-1}$]'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(1/2)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                # 2 is 1Gyr (0 is 0.25Gyr, 1 is 0.5Gyr, 3 is 2Gyr)
                ssfr = raw_data0[keys.key0_dict['sfr'][None]][:, keys.rkey_to_rcolum_dict['r200', None], :
                                                ] / mstar[:, np.newaxis]

                N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, keys.rkey_to_rcolum_dict['r200', None]]

                #mask and units
                x0 = np.log10(mstar[mask0]) + 10 # log Mstar
                y0 = np.log10(ssfr[mask0])

                N0 = N0[mask0]

            # calc medians etc.2
            for percenile_index in range(4):
                marker = ['d', '^', 'o', 'v', '*'][percenile_index]
                linestyle = [[(0,(1.5,0.5,0.5,0.5)), ((0,(1,0.5,1,0.5)))],
                             [(0,(3, 1, 1, 1)), ((0,(2, 1, 2, 1)))],
                             [(0,(6, 2, 2, 2)), ((0,(4, 2, 4, 2)))],
                             [(0,(12, 4, 4, 4)), ((0,(8, 4, 8, 4)))],
                             [(0,(24, 8, 8, 8)), ((0,(16, 8, 16, 8)))]][percenile_index][pair]
                cmap = ['Blues', 'Oranges'][pair]
                color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1)/4)

                #Non detections
                nds = np.logical_or(y0[:, percenile_index] == 0, y0[:, percenile_index] < ylims[0])
                nd_x0 = x0[nds]

                #TODO don't think deals with nondetections correctly
                not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0[:, percenile_index]))
                x = x0[not_crazy_mask0]
                y = y0[not_crazy_mask0, percenile_index]

                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                yplus, xedges, _ = binned_statistic(x, y,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x, y,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 2
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color, ls=linestyle, linewidth=2, zorder=2, alpha=1, path_effects=path_effect_4)

                axs[col].scatter(x, y,
                                 color=color, marker=marker, s=4, alpha=0.5,
                                 linewidths=0, zorder=2, rasterized=True)

                print(len(nd_x0))
                axs[col].scatter(nd_x0, np.ones(len(nd_x0)) * (ylims[0] + 0.03 * (ylims[1] - ylims[0])),
                                 color=color, marker=r'$\downarrow$', s=8 ** 2, alpha=0.5, rasterized=True)

        #decorations
        m_gas_min = DM_MASS * (Omega_b / (Omega_m - Omega_b)) * 10**10
        for averaged_time in [0.25, 0.5, 1, 2]:
            axs[col].errorbar(xlims, np.log10(averaged_time * m_gas_min / 10**np.array(xlims)),
                              c='k', ls=':', lw=1, zorder=-10)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
    #                ],
    #               ['HR DM', 'LR DM'],
    #               # handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               loc='upper left')#, frameon=False)

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    for percenile_index in range(4):
        cmap = ['Blues', 'Oranges'][1]
        color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1) / 4)
        if all_z:
            label = [r'0.25 Gyr', r'0.5 Gyr', r'1 Gyr', r'2 Gyr'][percenile_index]
            col=3
        else:
            label = [r'0.25 Gyr', r'0.5 Gyr', r'1 Gyr', r'2 Gyr'][percenile_index]
            col=0

        dx = 0.97
        dy = (0.04 + 0.07*(4-1)) - (0.07 * percenile_index)

        x = xlims[0] + dx * (xlims[1] - xlims[0])
        y = ylims[0] + dy * (ylims[1] - ylims[0])
        xc = xlims[0] + (dx + 0.2) * (xlims[1] - xlims[0])
        yc = ylims[0] + (dy + 0.1) * (ylims[1] - ylims[0])
        dxc = -5
        dyc = -0.0825 * (ylims[1] - ylims[0])

        axs[col].text(x, y, label,
                    ha='right', c=color, path_effects=path_effect_1)

        cmap = ['Blues', 'Oranges'][0]
        color = matplotlib.cm.get_cmap(cmap)((percenile_index + 1) / 5)

        t = axs[col].text(x, y, label,
                        ha='right', c=color, path_effects=path_effect_1)

        patch = patches.Rectangle((xc, yc), dxc, dyc, transform=axs[col].transData)
        t.set_clip_on(True)
        t.set_clip_path(patch)

    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def proj_dispersion_mass_relation(name='', save=False, all_z=True):
    N_res = 2

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[0]]

    xlims = [1e-3 + 8, 12 - 1e-3]
    # yticks = [-1, 0, 1, 2, 3]
    # ylims = [-1, 4]
    ylims = [1e-3 + 1.1, 2.433 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(7.5, 12, 10)

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'$\log \, \sigma_{\rm los} (R < R_{1/2, \rm los}) / $km s$^{-1}$'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        axs[col].set_aspect(3)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['lt_box_R12', None]]

                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['lt_box_R12', None]]

                #mask and units
                x0 = np.log10(mstar[mask0]) + 10 # log Mstar
                y0 = np.log10(sz[mask0])

                N0 = N0[mask0]

                #mask
                not_crazy_mask0 = N0 > N_res
                x = x0[not_crazy_mask0]
                y = y0[not_crazy_mask0]

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]

            # plot
            axs[col].errorbar(xcentres[mask], ymedians[mask],
                              c=color, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            # plot with cut
            not_heated_mask = x > mstar_intercept

            #plot heated galaxies only
            axs[col].scatter(x[np.logical_not(not_heated_mask)], y[np.logical_not(not_heated_mask)],
                             color=color, marker=marker, s=0.6, alpha=0.3,
                             linewidths=0, zorder=0, rasterized=True)

            x = x[not_heated_mask]
            y = y[not_heated_mask]

            xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            # plot
            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            axs[col].scatter(x, y,
                             color=color, marker=marker, s=4, alpha=0.5,
                             linewidths=0, zorder=2, rasterized=True)

            axs[col].axvline(mstar_intercept, 0, 1,
                             color=color2, ls=linestyle2, linewidth=1, zorder=-3)


        for i in range(20):
            #2 Mo, Mao, White style.
            axs[col].errorbar(xlims, [i * 1/3 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 1/3,
                                      i * 1/3 + np.floor(ylims[0])],
                              c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
    #                ],
    #               [r'$\sigma_{\rm DM}$ model'],
    #               loc='upper left')#, frameon=False)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
                   ],
                  ['HR DM', 'LR DM'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  loc='upper left')#, frameon=False)

    if all_z:
        pass
        # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(4),
        #                              lw=0, alpha=0.5, mew=0)),
        #                (lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(0.6),
        #                              lw=0, alpha=0.3, mew=0)), ],
        #               ['Converged galaxies', 'Unconverged galaxies'],
        #               numpoints=3,
        #               loc='lower right')  # , frameon=False

        # axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-.', lw=2, path_effects=path_effect_4)),
        #                (lines.Line2D([0, 1], [0, 1], color='C1', ls=(0, (0.8, 0.8)), lw=2, path_effects=path_effect_4)),
        #                ],
        #               ['HR spurious heating', 'LR spurious heating'],
        #               # handlelength=2, labelspacing=0.2, handletextpad=0.4,
        #               loc='upper left')#, frameon=False)

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def shape_plots(name='', save=False, all_z=False, shade=False, ca=True, ba=False):
    N_res = 2
    rad_key = 'eq_r12' #'lt_r12' #'lt_30' #'eq_r12' #'r200' #

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[0]]

    xlims = [8, 12]
    ylims = [0, 1]
    # ylims = [0, 1.5]

    if all_z:
        xlims[0] += 1e-3
        xlims[1] -= 1e-3
        ylims[0] += 1e-3
        ylims[1] -= 1e-3

    # N_run = np.linspace(7.9, 12, 42)
    # N_run = np.linspace(7.8, 12, 22)
    N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    # N_run = np.linspace(7.5, 12, 10)

    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    xlabel = r'$\log \, M_\star / $M$_\odot$'
    if ba:
        ylabel = r'$(b/a) \, (r < r_{1/2})$'
    if ca:
        ylabel = r'$(c/a) \, (r < r_{1/2})$'
    # ylabel = r'$v / \sigma (r_{1/2})$'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(3)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask_cents = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                mask_sats = raw_data0[keys.key0_dict['sn'][None]][:] != 0

                #load
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                # m0 = raw_data0[keys.key0_dict['m200'][None]][:]

                # comp = 'SFGas'
                comp = 'Star'
                c_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisc'][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                b_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisb'][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                a_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisa'][:, keys.rkey_to_rcolum_dict[rad_key, None]]

                # c_0 = c_0[mask0]
                # b_0 = b_0[mask0]
                # a_0 = a_0[mask0]

                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

                #mask and units
                # x0 = np.log10(m0) + 10 # log Mstar
                x0 = np.log10(mstar) + 10 # log Mstar

                if ba:
                    y0 = b_0 / a_0
                if ca:
                    y0 = c_0 / a_0

                # vphi = np.abs(raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]])
                # sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # sR = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # sphi = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # 
                # y0 = vphi / np.sqrt((sz ** 2 + sR ** 2 + sphi ** 2) / 3)

                # y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # y0 = y0[mask0]

                # N0 = N0[mask0]

                #mask
                not_crazy_mask0 = N0 > N_res
                x0 = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

                mask_cents = mask_cents[not_crazy_mask0]
                mask_sats = mask_sats[not_crazy_mask0]

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            for sat_cen, mask in enumerate([mask_sats, mask_cents]):
                x = x0[mask]
                y = y0[mask]

                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                if shade:
                    yplus, xedges, _ = binned_statistic(x, y,
                                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                    yminus, xedges, _ = binned_statistic(x, y,
                                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 10
                mask_count = counts > 20 * np.diff(N_run)[0]
                
                if shade:
                    boots = np.zeros((len(xcentres), 3))
                    for i in range(len(xcentres)):
                        boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

                if shade:
                    axs[col].fill_between(xcentres[mask_count], boots[mask_count, 0], boots[mask_count, 2],
                                          color=color, alpha=0.5, edgecolor='k', zorder=1, hatch=hatches[linestyle])

                # plot with cut
                not_heated_mask = x > mstar_intercept

                #scatter
                if sat_cen:
                    axs[col].scatter(x, y,
                                     color=color, marker=marker, s=3, alpha=0.3,
                                     linewidths=0, zorder=2, rasterized=True)
                else:
                    axs[col].scatter(x, y,
                                     facecolor='white', edgecolor=color2, marker=marker, s=3-0.8, alpha=0.2,
                                     linewidths=0.4, zorder=1, rasterized=True)

                xlin = np.linspace(mstar_intercept, xcentres[mask_count][-1])
                xlin2 = np.linspace(xcentres[mask_count][0], mstar_intercept)
                ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                #thin lines
                if sat_cen:
                    axs[col].errorbar(xlin2, ymed_func(xlin2),
                                      c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1,
                                      path_effects=path_effect_2)
                else:
                    if ca:
                        axs[col].errorbar(xlin2, ymed_func(xlin2),
                                          c=color2, ls=linestyle, linewidth=2, zorder=2, alpha=1,)
                                          # path_effects=path_effect_2)

                #thick lines
                if sat_cen:
                    axs[col].errorbar(xlin, ymed_func(xlin),
                                      c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1,
                                      path_effects=path_effect_4)
                else:
                    if ca:
                        axs[col].errorbar(xlin, ymed_func(xlin),
                                          c=color2, ls=linestyle, linewidth=4, zorder=4, alpha=1,)
                                        # path_effects=path_effect_4)

            axs[col].axvline(mstar_intercept, 0, 1,
                             color=color2, ls=linestyle2, linewidth=1, zorder=4)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
    #                ],
    #               [r'$\sigma_{\rm DM}$ model'],
    #               loc='upper left')#, frameon=False)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2),
                    (lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1),
                     lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1))),
                   (lines.Line2D([0, 1], [0, 1], color='navy', ls='-', lw=2),
                    (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
                                 markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1),
                     lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
                                 markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1))),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2),
                    (lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                 markeredgewidth=0, marker='o', markersize=3, alpha=1),
                     lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                 markeredgewidth=0, marker='o', markersize=3, alpha=1))), #0.3)),
                   (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls='--', lw=2),
                    (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
                                 markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1),
                     lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
                                 markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1))), #0.2)),
                   ],
                  ['HR centrals','HR satellites',
                   'LR centrals','LR satellites',],
                  handler_map={tuple: HandlerTuple(ndivide=2, pad=0)}, handlelength=3,
                  fontsize=9, labelspacing=0.3,
                  loc='lower left')
                  # loc='upper left')

    if ba:
        axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2),
                        (lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                      markeredgewidth=0, marker='o', markersize=3, alpha=1),
                         lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                      markeredgewidth=0, marker='o', markersize=3, alpha=1))),
                       ((),
                        (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
                                     markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1),
                         lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
                                     markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1))),
                       (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2),
                        (lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                     markeredgewidth=0, marker='o', markersize=3, alpha=1),
                         lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                     markeredgewidth=0, marker='o', markersize=3, alpha=1))), #0.3)),
                       ((),
                        (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
                                     markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1),
                         lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
                                     markeredgewidth=0.4, marker='o', markersize=3-0.8, alpha=1))), #0.2)),
                       ],
                      ['HR centrals','HR satellites',
                       'LR centrals','LR satellites',],
                      handler_map={tuple: HandlerTuple(ndivide=2, pad=0)}, handlelength=3,
                      fontsize=9, labelspacing=0.3,
                      loc='lower left')

    if all_z:
        pass

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def kappa_co_limits_plot(name='', save=False, all_z=False, shade=False, show_sat=False, model=False):
    N_res = 2
    rad_key = 'r200' #'eq_r12'  # 'lt_30' #'eq_r12' #'r200'

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        # spec = fig.add_gridspec(ncols=1, nrows=1)
        # cnp = [combined_name_pairs[0]]

        fig.set_size_inches(3.3*2, 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=1)
        cnp = [combined_name_pairs[0]]

    # xlims = [8, 12]
    xlims = [10, 14]
    ylims = [0, 1.2]

    if all_z:
        xlims[0] += 1e-3
        xlims[1] -= 1e-3
        ylims[0] += 1e-3
        ylims[1] -= 1e-3

    # vlims = [0, UNIVERSE_AGE]
    vmaps = ['Blues', 'Oranges']

    # vlims = [0.5, 4.5]
    vlims = [5, 12]
    vmaps = ['turbo', 'turbo']
    # vmaps = ['viridis_r', 'viridis_r']

    # N_run = np.linspace(9.9, 14, 42)
    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(10-1/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    # N_run -= 2
    # xlabel = r'$\log \, M_\star / $M$_\odot$'
    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, r_{1/2} /$ kpc'
    # ylabel = r'$\kappa_{\rm co} (r_{1/2})$'

    axs = []
    for col in range(len(cnp) + 1):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0:
            axs[col].set_ylabel(ylabel)
        else:
            axs[col].set_yticklabels([])

        if (col // 2) == all_z:
            axs[col].set_xlabel(xlabel)
        else:
            axs[col].set_xticklabels([])
        # axs[col].set_aspect(3)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:  # '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            col = pair

            if pair == 0:  # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                cmap = vmaps[0]
            else:  # 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                cmap = vmaps[1]

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask_cents = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                mask_sats = raw_data0[keys.key0_dict['sn'][None]][:] != 0

                # load
                m200 = raw_data0[keys.key0_dict['m200'][None]][:]
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                half_age0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict[rad_key, None], 2] #4]

                comp = 'Star'
                # c_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisc'][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # b_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisb'][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # a_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisa'][:, keys.rkey_to_rcolum_dict[rad_key, None]]

                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

                # mask and units
                # x0 = np.log10(mstar) + 10  # log Mstar
                x0 = np.log10(m200) + 10 # log Mstar

                # vphi = np.abs(raw_data0[keys.key0_dit['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]])
                # sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # sR = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # sphi = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                #
                # y0 = vphi / np.sqrt((sz ** 2 + sR ** 2 + sphi ** 2) / 3)

                y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                # y0 = y0[mask0]

                # N0 = N0[mask0]

                y0 = np.log10(raw_data0['GalaxyProfiles/Star/rHalf'][:, keys.rkey_to_rcolum_dict['eq_R12', None]])
                # y0 = np.log10(raw_data0['GalaxyProfiles/Star/rMax'][:, keys.rkey_to_rcolum_dict['box', None]])
                # y0 = np.log10(raw_data0['GalaxyProfiles/Star/rMax'][:, keys.rkey_to_rcolum_dict['lt_r12', None]])

                # mask
                not_crazy_mask0 = N0 > N_res
                x0 = x0[not_crazy_mask0]
                y0 = y0[not_crazy_mask0]

                mask_cents = mask_cents[not_crazy_mask0]
                mask_sats = mask_sats[not_crazy_mask0]
                half_age0 = half_age0[not_crazy_mask0]

            m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
            # mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            for sat_cen, mask in enumerate([mask_sats, mask_cents]):
                if not sat_cen and not show_sat:
                    continue

                x = x0[mask]
                y = y0[mask]

                age = half_age0[mask]

                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                if shade:
                    yplus, xedges, _ = binned_statistic(x, y,
                                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                    yminus, xedges, _ = binned_statistic(x, y,
                                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 10
                mask_count = counts > 20 * np.diff(N_run)[0]

                # if shade:
                #     boots = np.zeros((len(xcentres), 3))
                #     for i in range(len(xcentres)):
                #         boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)
                #     axs[col].fill_between(xcentres[mask_count], boots[mask_count, 0], boots[mask_count, 2],
                #                           color=color, alpha=0.5, edgecolor='k', zorder=1, hatch=hatches[linestyle])

                # not_heated_mask = x > m200_intercept
                not_heated_mask = x > 0

                # scatter
                if sat_cen:
                    axs[col].scatter(x, y,
                                     # color=color,
                                     c=age, cmap=cmap, vmin=vlims[0], vmax=vlims[1], ec='k',  #ec=color,
                                     marker=marker, s=4, alpha=1,
                                     linewidths=0.2, zorder=4, rasterized=True)

                    # # ec = ['#1f77b466', '#ff7f0e66'][pair]
                    # axs[col].scatter(x, y,
                    #                  color='#ffffff00', ec=ec, linewidths=0.2,
                    #                  marker=marker, s=4,
                    #                  zorder=5, rasterized=True)

                    # axs[col].scatter(x, y,
                    #                  color='k', marker=marker, s=0.5, linewidths=0,
                    #                  zorder=8, rasterized=True)

                    n_bins = 16 # int(2*len(N_run))
                    hex_bins = np.linspace(xlims[0] - (np.diff(xlims)[0] / n_bins), xlims[1], n_bins // 2)
                    hex_counts, _ = np.histogram(x, bins=hex_bins)
                    n_bins = (n_bins, int(n_bins / np.sqrt(3)))  # (int(np.sqrt(3) * n_bins), n_bins) #
                    axs[col].hexbin(x, y, C=age,
                                    cmap=cmap, vmin=vlims[0], vmax=vlims[1],
                                    reduce_C_function=np.median,
                                    extent=[*xlims, *ylims], mincnt=0, gridsize=n_bins, zorder=3) #zorder=-5)

                    # for xi, xcentre in enumerate(xcentres):
                    #     parts = axs[col].violinplot(dataset = y[bin_is==xi], positions=[xcentre],
                    #                                 widths=np.diff(N_run)[0], showextrema=False)
                    #     parts['bodies'][0].set_facecolor(color)
                    #     parts['bodies'][0].set_edgecolor('k')
                    #     parts['bodies'][0].set_alpha(0.5)
                    #     parts['bodies'][0].set_zorder(6)

                else:
                    axs[col].scatter(x, y,
                                     facecolor='white', edgecolor=color2, marker=marker, s=3 - 0.8, alpha=0.2,
                                     linewidths=0.4, zorder=1, rasterized=True)

                if shade:
                    xlin = np.linspace(m200_intercept, xcentres[mask_count][-1])
                    xlin2 = np.linspace(xcentres[mask_count][0], m200_intercept)
                    ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                    # thin lines
                    if sat_cen:
                        axs[col].errorbar(xlin2, ymed_func(xlin2),
                                          c=color, ls=linestyle, linewidth=1, zorder=4, alpha=1,
                                          path_effects=path_effect_2)
                    else:
                        if show_sat:
                            axs[col].errorbar(xlin2, ymed_func(xlin2),
                                              c=color2, ls=linestyle, linewidth=2, zorder=3, alpha=1, )
                            # path_effects=path_effect_2)

                    # thick lines
                    if sat_cen:
                        axs[col].errorbar(xlin, ymed_func(xlin),
                                          c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1,
                                          path_effects=path_effect_4)
                    else:
                        if show_sat:
                            axs[col].errorbar(xlin, ymed_func(xlin),
                                              c=color2, ls=linestyle, linewidth=3, zorder=4, alpha=1, )
                            # path_effects=path_effect_4)

                if shade:
                    axs[col].errorbar(xcentres[mask_count], yplus[mask_count],
                                      c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1)
                    axs[col].errorbar(xcentres[mask_count], yminus[mask_count],
                                      c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1)

                    axs[col].fill_between(xcentres[mask_count], yminus[mask_count], yplus[mask_count],
                                          color=color, alpha=0.5, zorder=0)


            axs[col].axvline(m200_intercept, 0, 1,
                             color=color2, ls=linestyle2, linewidth=1, zorder=4)

            if model:
                #don't want a fancy model for ages
                agemedians, _, dmbin_is = binned_statistic(x, age,
                                                           statistic=np.nanmedian, bins=N_run)
                agemedians[np.isnan(agemedians)] = agemedians[np.logical_not(np.isnan(agemedians))][-1]
                interp_age = interp1d(0.5 * (N_run[1:] + N_run[:-1]), UNIVERSE_AGE-agemedians, fill_value='extrapolate')

                #contraction
                log_M200s = np.linspace(*xlims, 21) #log M_sun
                #cosmology
                H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3)) #km^2/s^2 / kpc^2
                rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                rho_200 = 200 * rho_crit
                r200 = np.power(3 * 10**(log_M200s-10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
                V200 = np.sqrt(GRAV_CONST * 10**(log_M200s-10) / r200)  # km/s

                if pair: MassDM = DM_MASS
                else: MassDM = DM_MASS / 7

                #contracted
                # for time in [interp_age(log_M200s[i])]:
                for time in vlims:
                    out = np.array([mgm.get_heating_at_r12(10**(log_M200s[i]-10), MassDM=MassDM, z=z,
                                                           ic_heat_fraction=0.3,
                                                           time=time, contracted=True)
                                    for i in range(len(log_M200s))])
                    (analytic_v_circ, analytic_dispersion,
                     theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                     stellar_dispersion, power_law_z, power_law_r, power_law_p
                     ) = out.T


                    theory_best_tot2 = theory_best_z**2 + theory_best_r**2 + theory_best_p**2

                    # rms2_positive_v_phi = np.zeros(len(log_M200s))
                    # for i in range(len(log_M200s)):
                    #     v = theory_best_v[i]
                    #     s = theory_best_p[i]
                    #
                    #     v2_int = lambda x: x ** 2 * scipy.stats.norm.pdf(x, loc=v, scale=s)
                    #     v2_out = quad(v2_int, 0, v + s*5)[0]
                    #
                    #     # m_frac_out = 1-scipy.stats.norm.cdf(0, loc=v, scale=s)
                    #     area_out = 1
                    #     rms2_positive_v_phi[i] = v2_out / area_out  # * m_frac_out
                    #
                    # theory_best_kappa_co = rms2_positive_v_phi / (theory_best_v ** 2 + theory_best_tot2)

                    theory_best_kappa_co = analytic_v_circ**2 / (analytic_v_circ**2 + theory_best_tot2)

                    line_color = matplotlib.cm.get_cmap(cmap)((time - vlims[0])/(vlims[1] - vlims[0]))

                    axs[col].errorbar(log_M200s, theory_best_kappa_co,
                                      c=line_color,
                                      ls=linestyle, lw=1, zorder=9, path_effects=path_effect_2)

                    # axs[col].errorbar(log_M200s, 7/12 - 5/12 * erf(log_M200s - 10.2),
                    #                   c='grey', ls='-', lw=1, zorder=6)
                    # axs[col].errorbar(log_M200s, 1/12 + 1/12 * erf(log_M200s - 10.2),
                    #                   c='grey', ls='-', lw=1, zorder=6)

            #TODO replace with arrows
            # axs[col].axhline(0.65, 0, 1,
            #                  color='grey', ls='--', linewidth=1, alpha=1, zorder=3)
            # axs[col].axhline(0.5, 0, 1,
            #                  color='grey', ls='--', linewidth=1, alpha=1, zorder=3)
            # axs[col].axhline(0.35, 0, 1,
            #                  color='grey', ls='--', linewidth=1, alpha=1, zorder=3)

            # axs[col].axhline(1/6, 0, 1,
            #                  color='grey', ls=':', linewidth=1, alpha=1, zorder=3)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
    #                ],
    #               [r'$\sigma_{\rm DM}$ model'],
    #               loc='upper left')#, frameon=False)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2),
    #                 (lines.Line2D([0, 1], [0, 1], color='C0', ls='',
    #                               markeredgewidth=0, marker='o', markersize=3, alpha=1),
    #                  lines.Line2D([0, 1], [0, 1], color='C0', ls='',
    #                               markeredgewidth=0, marker='o', markersize=3, alpha=1))),
    #                (lines.Line2D([0, 1], [0, 1], color='navy', ls='-', lw=2),
    #                 (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
    #                               markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1),
    #                  lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
    #                               markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1))),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2),
    #                 (lines.Line2D([0, 1], [0, 1], color='C1', ls='',
    #                               markeredgewidth=0, marker='o', markersize=3, alpha=1),
    #                  lines.Line2D([0, 1], [0, 1], color='C1', ls='',
    #                               markeredgewidth=0, marker='o', markersize=3, alpha=1))),  # 0.3)),
    #                (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls='--', lw=2),
    #                 (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
    #                               markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1),
    #                  lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
    #                               markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1))),  # 0.2)),
    #                ],
    #               ['HR centrals', 'HR satellites',
    #                'LR centrals', 'LR satellites', ],
    #               handler_map={tuple: HandlerTuple(ndivide=2, pad=0)}, handlelength=3,
    #               fontsize=9, labelspacing=0.3,
    #               loc='upper left')

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    # Bbox_top = axs[0].get_position()
    # Bbox_bot = axs[0].get_position()

    Bbox_top = axs[1].get_position()
    Bbox_bot = axs[1].get_position()

    width = 0.1
    pad = 0.05

    cax = plt.axes([Bbox_top.x1, Bbox_bot.y0,
                    (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                    (Bbox_top.y1 - Bbox_bot.y0)])

    cax2 = plt.axes([Bbox_top.x1, Bbox_bot.y0,
                     (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                     (Bbox_top.y1 - Bbox_bot.y0)])

    if all_z:
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))


        Bbox_top = axs[1].get_position()
        Bbox_bot = axs[2].get_position()

        width = 0.1
        pad = 0.05

        cax = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                        (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                        (Bbox_top.y1 - Bbox_bot.y0) / 2])

        cax2 = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                         (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                         (Bbox_top.y1 - Bbox_bot.y0) / 2])

    n_col_grad = 101
    x_ = np.linspace(*vlims, n_col_grad)
    x, _ = np.meshgrid(x_, np.array([0, 1]))
    cax.imshow(x.T, cmap=vmaps[0], vmin=vlims[0], vmax=vlims[1],
               aspect = 2 / np.diff(vlims)[0] / width, origin='lower', extent=[0, 1, *vlims])

    cax.set_xticks([])
    cax.set_xticklabels([])
    # cax.set_yticks([0.6, 0.7, 0.8, 0.9, 1])
    cax.set_yticklabels([])
    cax.yaxis.tick_left()


    cax2.imshow(x.T, cmap=vmaps[1], vmin=vlims[0], vmax=vlims[1],
                aspect = 2 / np.diff(vlims)[0] / width, origin='lower', extent=[0, 1, *vlims])

    cax2.set_xticks([])
    cax2.set_xticklabels([])

    cax2.yaxis.set_label_position('right')
    cax2.yaxis.tick_right()

    cax2.set_ylabel(r'Median stellar age [Gyr]')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def disk_fraction_cut_plot(name='', save=False, shade=False):
    N_res = 2
    rad_key = 'r200' #'lt_r12'  # 'lt_30' #'eq_r12' # #

    # DT_cuts = np.arange(-1, 1, 0.1)  # [0.6, 0.7, 0.8, 0.9]
    DT_cuts = [0.6, 0.7, 0.8, 0.9]

    fig = plt.figure(constrained_layout=True)

    # fig.set_size_inches(2.54 * len(DT_cuts), 2.54 + 0.04, forward=True)
    # spec = fig.add_gridspec(ncols=len(DT_cuts), nrows=1)

    fig.set_size_inches(3.33 * 2 + 0.6, 3.33 * 2, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    z_index = 0
    name_pairs = combined_name_pairs[z_index]
    z = [0, 0.503, 1.004, 2.012][z_index]
    strz = ['0', '0.5', '1', '2'][z_index]

    # xlims = [1e-3 + 10, 14 - 1e-3]
    xlims = [1e-3 + 8, 12 - 1e-3]
    # ylims = [0, 1]

    ylimss = [[0, 1], [0,0.8], [0,0.6-1e-3], [0,0.4-1e-3]]

    # N_run = np.linspace(9.9, 14, 42)
    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(10-1/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    N_run -= 2

    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'D/T'

    axs = []
    # for col in range(len(DT_cuts)):
    for col in range(4):
        axs.append(fig.add_subplot(spec[col//2, col%2]))
        axs[col].set_ylim(ylimss[col])
        axs[col].set_ylabel(ylabel)

        axs[col].set_xlim(xlims)
        if col//2 == 0: axs[col].set_xticklabels([])
        else: axs[col].set_xlabel(xlabel)

        axs[col].set_aspect(np.diff(xlims) / np.diff(ylimss[col]))

    fig.subplots_adjust(hspace=0.02, wspace=0.21)

    for col, DT_cut in enumerate(DT_cuts):
        for pair in range(2):
            if pair == 0:  # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
            elif pair == 2:  # 0 (7x)
                linestyle = '-.'
                linestyle2 = ':'
                marker = 'o'
                color = 'C2'
                color2 = 'seagreen'
                color3 = 'lime'
            else:  # 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                # load
                mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                # m0 = raw_data0[keys.key0_dict['m200'][None]][:]

                fjzonc_0 = raw_data0['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict[rad_key, None], :]

                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

                # mask and units
                N0 = N0[mask0]
                x0 = np.log10(mstar[mask0]) + 10  # log Mstar
                # x0 = np.log10(M200[mask0]) + 10 # log Mstar

                fjzonc_0 = fjzonc_0[mask0]
                # n = -round((1 - 0.7) / (2 / (keys.n_jzjc_bins - 1)) + 1)
                n = -round((1 - DT_cut) / (2 / (keys.n_jzjc_bins - 1)) + 1)
                DT_0 = fjzonc_0[:, n]

                # mask
                not_crazy_mask0 = N0 > N_res
                x = x0[not_crazy_mask0]
                y = DT_0[not_crazy_mask0]

            m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            print(np.sum((y > 0.5)[x > xlims[0]]), np.sum(x > xlims[0]))
            print(np.amax(y[x > xlims[0]]))

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, _, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, _, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            yplus2, _, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.02275), bins=N_run)
            yminus2, _, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.97725), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask_count = counts > 20 * np.diff(N_run)[0]

            if shade:
                boots = np.zeros((len(xcentres), 3))
                for i in range(len(xcentres)):
                    boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)
                axs[col].fill_between(xcentres[mask_count], boots[mask_count, 0], boots[mask_count, 2],
                                      color=color, alpha=0.5, edgecolor='k', zorder=1, hatch=hatches[linestyle])

            # scatter
            axs[col].scatter(x, y,
                             color=color, marker=marker, s=3, alpha=0.3,
                             linewidths=0, zorder=-1, rasterized=True)

            # xlin = np.linspace(m200_intercept, xcentres[mask_count][-1])
            # xlin2 = np.linspace(xcentres[mask_count][0], m200_intercept)
            xlin = np.linspace(mstar_intercept, xcentres[mask_count][-1])
            xlin2 = np.linspace(xcentres[mask_count][0], mstar_intercept)
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            axs[col].errorbar(xlin2, ymed_func(xlin2),
                              c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1,
                              path_effects=path_effect_2)
            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1,
                              path_effects=path_effect_4)

            yplus_func = interp1d(xcentres, yplus, fill_value='extrapolate')
            yminus_func = interp1d(xcentres, yminus, fill_value='extrapolate')
            axs[col].errorbar(xlin2, yplus_func(xlin2),
                              c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1)
            axs[col].errorbar(xlin, yplus_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1)
            axs[col].errorbar(xlin2, yminus_func(xlin2),
                              c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1)
            axs[col].errorbar(xlin, yminus_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1)

            axs[col].fill_between(xcentres[mask_count], yminus[mask_count], yplus[mask_count],
                                  color=color, alpha=0.5, zorder=0)

            # #2 sigma lines
            # axs[col].errorbar(xcentres[mask_count], yplus2[mask_count],
            #                   c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1)
            # axs[col].errorbar(xcentres[mask_count], yminus2[mask_count],
            #                   c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1)

            # axs[col].axvline(m200_intercept, 0, 1,
            #                  color=color2, ls=linestyle2, linewidth=1, zorder=0)

            mxy0 = 0.76
            mxy1 = 0.88
            if col == 3:
                mxy0 = 0.54
                mxy1 = 0.66

            axs[col].axvline(mstar_intercept, 0, [mxy0, mxy1][pair],
                             color=color2, ls=linestyle2, linewidth=1, zorder=0)

        dy0 = 0.95
        dy1 = 0.83
        if col == len(DT_cuts) -1:
            dy0 = 0.725
            dy1 = 0.605

        axs[col].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylimss[col][0] + dy0 * (ylimss[col][1] - ylimss[col][0]),
                      # r'D/T$= F(j_z / j_c(E) > $' + str(round(DT_cut, 2)) + ')',
                      # r'D/T with $\varepsilon_{\rm circ} > $' + str(round(DT_cut, 2)),
                      r'$j_z / j_c(E) > $' + str(round(DT_cut, 2)),
                      # r'$\varepsilon_{\rm circ} > $' + str(round(DT_cut, 2)),
                      ha='left', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[col].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylimss[col][0] + dy1 * (ylimss[col][1] - ylimss[col][0]),
                      r'$z=$' + strz,
                      ha='left', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    axs[-1].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2),
                    (lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1),
                     lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1))),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2),
                    (lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1),
                     lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1))),  # 0.3)),
                   ],
                  ['HR galaxies', 'LR galaxies'],
                  handler_map={tuple: HandlerTuple(ndivide=2, pad=0)}, handlelength=3,
                  # fontsize=9, labelspacing=0.3,
                  loc='upper left')


    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def traditional_v_on_s_from_shape(ellipticity, delta, alpha=0):
    '''
    '''
    s = 1 - ellipticity

    # Thob et al. 2019 equation B5
    W_xx_on_W_zz = 1 / (2 * s ** 2) * (np.arccos(s) - s * np.sqrt(1 - s ** 2)) / (
            np.sqrt(1 - s ** 2) / s - np.arccos(s))

    # # Thob et al. 2019 equation B4
    v_2_on_s0_2 = 2 * (1 - delta) * W_xx_on_W_zz - 2

    # # Binney 2005 equation 26
    # v_2_on_s0_2 = ((1 - delta) * W_xx_on_W_zz - 1) / (alpha * (1 - delta) * W_xx_on_W_zz + 1)

    return np.sqrt(v_2_on_s0_2)


def shape_relation_plots(name='', save=False, shade=False):
    N_res = 2
    rad_key = 'lt_r12'  # 'lt_30' #'eq_r12' #'r200' #
    z_index = 0

    boundary_kde = True
    adaptave_kde = False

    harry_plotter = getdist.plots.get_single_plotter()
    plt.close()

    fig = plt.figure(constrained_layout=True)

    # mass_bin_edges = [[8.35,8.65], [9.35,9.65], [10.35,10.65]]
    mass_bin_edges = [[8.25,8.75], [9.25,9.75], [10.25,10.75]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [0+1e-3, 1-1e-3]
    ylims = [0, 2-1e-3]

    xlabel = r'$(1 - c/a) \, (r < r_{1/2})$'
    ylabel = r'$(\overline{v}_\phi / \sigma_{\rm 1D}) \, (r < r_{1/2})$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])
        axs[col].set_xlabel(xlabel)

        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if '015_z002p012_' in combined_name_pairs[z_index][0]:
        z = 2.012
        strz = '2'
    elif '019_z001p004_' in combined_name_pairs[z_index][0]:
        z = 1.004
        strz = '1'
    elif '023_z000p503_' in combined_name_pairs[z_index][0]:
        z = 0.503
        strz = '0.5'
    else:
        z = 0
        strz = '0'

    for pair in range(2):
        if pair == 0:  # 0 (7x)
            linestyle = '-'
            linestyle2 = '-.'
            marker = 'o'
            color = 'C0'
            color2 = 'navy'
            color3 = 'cyan'
        else:  # 1 (1x)
            linestyle = '--'
            linestyle2 = (0, (0.8, 0.8))
            marker = 'o'
            color = 'C1'
            color2 = 'saddlebrown'
            color3 = 'gold'

        with h5.File(combined_name_pairs[z_index][pair], 'r') as raw_data0:
            # centrals only
            mask_cents = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            mask_sats = raw_data0[keys.key0_dict['sn'][None]][:] != 0

            # load
            mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            m200 = raw_data0[keys.key0_dict['m200'][None]][:]

            comp = 'Star'
            c_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisc'][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            b_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisb'][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            a_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisa'][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            # Jtot = raw_data0[keys.key0_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            # Mstar12 = raw_data0[keys.key1_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            # r12 = raw_data0[keys.key0_dict['r12'][None]][:]
            # vphi = Jtot / Mstar12 / r12 * 2
            vphi = np.abs(raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]])
            # vphi = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            sR = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            sphi = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            # y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            # y0 = y0[mask0]

            N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            # mask and units
            log_m200 = np.log10(m200) + 10  # log Mstar
            log_mstar = np.log10(mstar) + 10  # log Mstar

            x0 = c_0 / a_0
            y0 = vphi / np.sqrt((sz**2 + sR**2 + sphi**2)/3)

            # N0 = N0[mask0]
            # mask
            not_crazy_mask0 = N0 > N_res
            x0 = 1 - x0[not_crazy_mask0]
            y0 = y0[not_crazy_mask0]

            mask_cents = mask_cents[not_crazy_mask0]
            mask_sats = mask_sats[not_crazy_mask0]
            log_m200 = log_m200[not_crazy_mask0]
            log_mstar = log_mstar[not_crazy_mask0]

        # m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
        # mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

        for sat_cen, mask in enumerate([mask_sats, mask_cents]):
            # if sat_cen == 0:
            #     continue

            x00 = x0[mask]
            y00 = y0[mask]
            log_m2000 = log_m200[mask]
            log_mstar0 = log_mstar[mask]

            for col, mass_range in enumerate(mass_bin_edges):

                mask = np.logical_and(mass_range[0] < log_mstar0, log_mstar0 < mass_range[1])
                x = x00[mask]
                y = y00[mask]

                # # calc medians etc.
                # ymedians, xedges, bin_is = binned_statistic(x, y,
                #                                             statistic=np.nanmedian, bins=N_run)
                # xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                # counts, _ = np.histogram(x, bins=N_run)
                # # mask = counts > 10
                # mask_count = counts > 20 * np.diff(N_run)[0]

                print(sat_cen, mass_range, pair)
                print(np.sum(y > 1), len(y), np.sum(y > 1) / len(y))

                # scatter
                if sat_cen:
                    axs[col].scatter(x, y,
                                     color=color, marker=marker, s=3, alpha=0.5,#0.3,
                                     linewidths=0, zorder=2, rasterized=True)
                else:
                    axs[col].scatter(x, y,
                                     facecolor='white', edgecolor=color2, marker=marker, s=3 - 0.8, alpha=0.4,#0.2,
                                     linewidths=0.4, zorder=1, rasterized=True)

                values = np.vstack([x, y])

                # #scipy
                if not boundary_kde and not adaptave_kde:
                    kernel = gaussian_kde(values)

                if adaptave_kde and not boundary_kde:
                    kde = GaussianKDE(glob_bw='silverman', alpha=0.1, diag_cov=False)
                    kde.fit(values.T)

                if boundary_kde:
                    samples = getdist.MCSamples(samples=values.T,
                                                names=['x0', 'x1'],
                                                settings = {'mult_bias_correction_order':1,
                                                            'boundary_correction_order':1},
                                                ranges = {'x0':[0, 1], 'x1':[0, 2]},)

                #??? from https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html
                X, Y = np.mgrid[xlims[0]:xlims[1]:101j, ylims[0]:ylims[1]:101j]
                positions = np.vstack([X.ravel(), Y.ravel()])

                #scipy
                if not boundary_kde and not adaptave_kde:
                    W = np.reshape(kernel(positions).T, X.shape)
                    W /= np.sum(W)

                    #scipy
                    zero_int_gtr_level = lambda x, y: np.sum(W[W > x]) - y
                    fracs = [0.95, 0.68] #[0.95, 0.68] #[1-0.68, 1-0.95, 1-0.997]
                    wlevels = []
                    for frac in fracs:
                        wlevels.append(brentq(zero_int_gtr_level, 0, 1, args=frac))

                if adaptave_kde and not boundary_kde:
                    Z = np.reshape(kde.predict(positions.T), X.shape)
                    Z /= np.sum(Z)

                    zero_int_gtr_level = lambda x, y: np.sum(Z[Z > x]) - y
                    fracs = [0.95, 0.68] #[0.95, 0.68] #[1-0.68, 1-0.95, 1-0.997]
                    levels = []
                    for frac in fracs:
                        levels.append(brentq(zero_int_gtr_level, 0, 1, args=frac))


                if sat_cen:
                    #scipy
                    if not boundary_kde and not adaptave_kde:
                        if linestyle == '-': ls1 = '-'
                        else: ls1 = (0, (7.37,7.37*4.385/10))

                        axs[col].contour(X, Y, W, levels=wlevels,
                                         zorder=4,
                                         colors=color, linestyles=[ls1 for wlevel in wlevels], linewidths=1)
                        axs[col].contour(X, Y, W, levels=wlevels,
                                         zorder=3,
                                         colors='k', linestyles=linestyle, linewidths=2)

                    if adaptave_kde and not boundary_kde:
                        axs[col].contour(X, Y, Z, levels=levels,
                                         zorder=4,
                                         colors=color, linestyles=linestyle, linewidths=1)

                    if boundary_kde:
                        if linestyle == '-': ls1 = '-'
                        else: ls1 = (0, (6,4))

                        harry_plotter.plot_2d(samples, 'x0', 'x1', ax=axs[col],
                                              line_args=[{'ls': ls1, 'color': color, 'lw': 1, 'zorder':4},
                                                         {'ls': ls1, 'color': color, 'lw': 1, 'zorder':4}])
                        harry_plotter.plot_2d(samples, 'x0', 'x1', ax=axs[col],
                                              line_args=[{'ls': linestyle, 'color': 'k', 'lw': 2, 'zorder':3},
                                                         {'ls': linestyle, 'color': 'k', 'lw': 2, 'zorder':3}])
                else:
                    pass
                    # axs[col].contour(X, Y, Z, levels=levels,
                    #                  zorder=3,
                    #                  colors=color2, linestyles=linestyle, linewidths=1)

                    # harry_plotter.plot_2d(samples, 'x0', 'x1', ax=axs[col],
                    #                       line_args=[{'ls': linestyle, 'color': color2, 'lw': 1, 'zorder':3},
                    #                                  {'ls': linestyle, 'color': color2, 'lw': 1, 'zorder':3}])

                #decorations

                axs[col].text(xlims[0] + 0.045 * (xlims[1] - xlims[0]), ylims[0] + 0.793 * (ylims[1] - ylims[0]),
                              r'$z=$' + strz,
                              bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

                axs[col].text(xlims[0] + 0.045 * (xlims[1] - xlims[0]), ylims[0] + 0.91 * (ylims[1] - ylims[0]),
                               r'$\log \, M_\star / $M$_\odot \in$[' + str(round(mass_bin_edges[col][0], 2)) + ', ' +
                               str(round(mass_bin_edges[col][1], 2)) + ']',
                               bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

                ellipticities = np.linspace(*xlims, 1001)
                delta = 0

                for delta in [0, 0.2, 0.4, 0.6, 0.8]:
                    v_on_s = traditional_v_on_s_from_shape(ellipticities, delta, alpha=0)
                    v_on_s[np.isnan(v_on_s)] = -0.01
                    axs[col].errorbar(ellipticities, v_on_s, c='grey', ls=':', lw=1, alpha=0.5, zorder=-3)

                    if col == 0:
                        index = np.argmin((v_on_s - 1.6)**2)
                        if delta == 0:
                            axs[col].text(ellipticities[index] - 0.04, v_on_s[index],
                                          r'$\delta = $',
                                          ha='center', c='grey', alpha=0.5, fontsize=9)
                        axs[col].text(ellipticities[index], v_on_s[index],
                                      f'{delta}',
                                      ha='center', c='grey', alpha=0.5, fontsize=9)

    #plotter messes this up
    for col in range(len(mass_bin_edges)):
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0:
            axs[col].set_ylabel(ylabel)
        else:
            axs[col].set_ylabel('')
            axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)

        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)


    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_2),
                    (lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1),
                     lines.Line2D([0, 1], [0, 1], color='C0', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1))),
                   ((),#lines.Line2D([0, 1], [0, 1], color='navy', ls='-', lw=2),
                    (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
                                  markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1),
                     lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='navy', ls='',
                                  markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1))),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_2),
                    (lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1),
                     lines.Line2D([0, 1], [0, 1], color='C1', ls='',
                                  markeredgewidth=0, marker='o', markersize=3, alpha=1))),  # 0.3)),
                   ((),#lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls='--', lw=2),
                    (lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
                                  markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1),
                     lines.Line2D([0, 1], [0, 1], markerfacecolor='white', markeredgecolor='saddlebrown', ls='',
                                  markeredgewidth=0.4, marker='o', markersize=3 - 0.8, alpha=1))),  # 0.2)),
                   ],
                  ['HR centrals', 'HR satellites',
                   'LR centrals', 'LR satellites', ],
                  handler_map={tuple: HandlerTuple(ndivide=2, pad=0)}, handlelength=3,
                  fontsize=9, labelspacing=0.3,
                  loc='upper left', bbox_to_anchor=(0,0.77))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def fraction_of_heated_galaxies(name='', save=False,
                                plot_dm_measure=False, ek=False, uncontracted=False, star_jeans=False):

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 1.65 * 2 + 0.02, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    xlims = [1e-3 + 10, 14 - 1e-3]
    # yticks = [-1, 0, 1, 2, 3]
    # ylims = [-1, 4]
    ylims = [-0.1, 1.1]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.75, 14, 18)
    N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.9, 14, 42)
    # N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'Fraction of heated galaxies'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims) / 2)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                r200 = raw_data0[keys.key0_dict['r200'][None]][:]
                sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                sR = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                sphi = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                #add kinetic energy because that's what Arron has done
                if ek:
                    vphi = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                half_age0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['eq_r12', None], 2]

                #mask and units
                V200 = np.sqrt(GRAV_CONST * M200 / r200) # km/s
                x0 = np.log10(M200[mask0]) + 10 # log Mstar
                y0 = np.log10(np.sqrt(sz[mask0]**2 + sR[mask0]**2 + sphi[mask0]**2) / V200[mask0])

                if ek:
                    y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2
                                          + vphi[mask0]**2) / V200[mask0])
                half_age0 = half_age0[mask0] #Gyr

                #mask
                not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
                x = x0[not_crazy_mask0]
                y = y0[not_crazy_mask0]
                age = half_age0[not_crazy_mask0] #Gyr

            #don't want a fancy model for ages
            agemedians, _, dmbin_is = binned_statistic(x, age,
                                                       statistic=np.nanmedian, bins=N_run)
            agemedians[np.isnan(agemedians)] = agemedians[np.logical_not(np.isnan(agemedians))][-1]
            interp_age = interp1d(0.5 * (N_run[1:] + N_run[:-1]), UNIVERSE_AGE-agemedians, fill_value='extrapolate')

            #contraction
            log_M200s = np.linspace(*xlims, 101) #log M_sun
            #cosmology
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3)) #km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200 = 200 * rho_crit
            r200 = np.power(3 * 10**(log_M200s-10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
            V200 = np.sqrt(GRAV_CONST * 10**(log_M200s-10) / r200)  # km/s

            if pair: MassDM = DM_MASS
            else: MassDM = DM_MASS / 7

            #uncontracted (pure NFW)
            if uncontracted:
                if pair == 0:
                    out = np.array([mgm.get_heating_at_r12(10**(log_M200s[i]-10), MassDM=MassDM, z=z,
                                                           time=interp_age(log_M200s[i]), contracted=False)
                                    for i in range(len(log_M200s))])
                    (analytic_v_circ, analytic_dispersion,
                     theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                     stellar_dispersion, power_law_z, power_law_r, power_law_p
                    ) = out.T

                    # axs[col].errorbar(log_M200s, np.log10(analytic_dispersion * np.sqrt(3) / V200),
                    #                   c=color3, ls=linestyle2, linewidth=2, zorder=4, alpha=1)

            #contracted
            out = np.array([mgm.get_heating_at_r12(10**(log_M200s[i]-10), MassDM=MassDM, z=z,
                                                   time=interp_age(log_M200s[i]), contracted=True)
                            for i in range(len(log_M200s))])
            (analytic_v_circ, analytic_dispersion,
             theory_best_v, theory_best_z, theory_best_r, theory_best_p,
             stellar_dispersion, power_law_z, power_law_r, power_law_p
             ) = out.T

            # if pair == 0:
                # axs[col].errorbar(log_M200s, np.log10(analytic_dispersion * np.sqrt(3) / V200),
                #                   c=color2, ls=linestyle2, linewidth=2, zorder=4, alpha=1)

            theory_best_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)
            # power_law_tot = np.sqrt(power_law_z**2 + power_law_r**2 + power_law_p**2)

            # axs[col].errorbar(log_M200s, np.log10(theory_best_tot / V200),
            #                   c=color, ls=linestyle2, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            heating_limit = interp1d(log_M200s, np.log10(theory_best_tot / V200), fill_value='extrapolate')

            nfw_frac = interp1d(log_M200s, theory_best_tot / (analytic_dispersion * np.sqrt(3)),
                                fill_value='extrapolate')
            limit = lambda log_M200: nfw_frac(log_M200) - 0.95
            m200_all_heated = brentq(limit, 9.5, 13)

            # plot no cut
            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]
            max_xbin_centre = xcentres[:-1][np.logical_not(mask)[1:]][0]

            y_med_interp = interp1d(xcentres, ymedians, fill_value='extrapolate')

            heated_intercept_func = lambda log_M200: y_med_interp(log_M200) - heating_limit(log_M200)
            m200_intercept = brentq(heated_intercept_func, 10, 12)
            print(m200_intercept)

            # plot
            x_converged = np.linspace(m200_intercept, max_xbin_centre, 101)

            axs[col].axvline(m200_intercept, 0, 1,
                             c=color2, ls=linestyle2, linewidth=1, zorder=4.5, alpha=1)

            heated_mask = np.logical_or(y < heating_limit(x), x < m200_all_heated)

            frac_heated, xedges, bin_is = binned_statistic(x, heated_mask,
                                                           statistic=np.nanmean, bins=N_run)

            #plot
            axs[col].errorbar(xcentres[mask], frac_heated[mask],
                              c=color, ls=linestyle, linewidth=2, zorder=2, alpha=1, path_effects=path_effect_4)

        with h5.File(name_pairs[0], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

            # load
            M200 = raw_data0[keys.key0_dict['m200'][None]][:]
            r200 = raw_data0[keys.key0_dict['r200'][None]][:]
            sz = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sR = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sphi = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            # mask and units
            V200 = np.sqrt(GRAV_CONST * M200 / r200)  # km/s
            x0 = np.log10(M200[mask0]) + 10  # log Mstar
            y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2) / V200[mask0])

            # mask
            not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
            x0 = x0[not_crazy_mask0]
            y0 = y0[not_crazy_mask0]

        with h5.File(name_pairs[1], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0

            # load
            M200 = raw_data1[keys.key0_dict['m200'][None]][:]
            r200 = raw_data1[keys.key0_dict['r200'][None]][:]
            sz = raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sR = raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sphi = raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            # mask and units
            V200 = np.sqrt(GRAV_CONST * M200 / r200)  # km/s
            x1 = np.log10(M200[mask1]) + 10  # log Mstar
            y1 = np.log10(np.sqrt(sz[mask1] ** 2 + sR[mask1] ** 2 + sphi[mask1] ** 2) / V200[mask1])

            # mask
            not_crazy_mask1 = np.logical_and(np.isfinite(x1), np.isfinite(y1))
            x1 = x1[not_crazy_mask1]
            y1 = y1[not_crazy_mask1]

        counts0, _ = np.histogram(x0, bins=N_run)
        counts1, _ = np.histogram(x1, bins=N_run)
        mask = np.logical_and(counts0 > 10, counts1 > 10)

        bins_0 = np.digitize(x0, N_run)
        bins_1 = np.digitize(x1, N_run)

        frac_greater = np.nan * np.zeros(len(N_run)-1)

        n_boots = 200
        boot_frac_greater = np.nan * np.zeros((len(N_run)-1, 3))

        for i in range(len(N_run) -1):
            zs = y0[bins_0 == i+1]
            os = y1[bins_1 == i+1]
            if len(zs) > 0 or len(os) > 0:
                #might run out of memory...
                frac_greater[i] = np.sum(np.less(zs[:, np.newaxis], os[np.newaxis, :])) / (len(zs) * len(os))

                #bootstrap (this is a little slow)
                boot = np.zeros(n_boots)
                for j in range(n_boots):
                    zbs = zs[np.random.randint(0, len(zs), len(zs))]
                    obs = os[np.random.randint(0, len(os), len(os))]

                    boot[j] = np.sum(np.less(zbs[:, np.newaxis], obs[np.newaxis, :])) / (len(zbs) * len(obs))

                boot_frac_greater[i] = [np.quantile(boot, q) for q in [0.16, 0.5, 0.84]]

        axs[col].errorbar(xcentres[mask], 2*frac_greater[mask] - 1,
                          c='navy', ls='-.', linewidth=2, zorder=2, alpha=1, path_effects=path_effect_4)

        axs[col].fill_between(xcentres[mask], 2*boot_frac_greater[mask, 0]-1, 2*boot_frac_greater[mask, 2]-1,
                              color='navy', alpha=0.5, edgecolor='k', zorder=9, hatch='xxx')

    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0.5$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=1$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=2$', ha='right', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def dispersion_age(name='', save=False, r12=True, dmsigma=False, ek=False, shade=False, show_all=False, z_index=0,
                   mean_v_phi=False, zoom_bins=False):
    N_res = 2

    fig = plt.figure(constrained_layout=True)

    # mass_bin_edges = [[10.35,10.65], [10.85,11.15], [11.35, 11.65], [11.85, 12.15]]#, [12.35, 12.65]]
    mass_bin_edges = [[10.85,11.15], [11.35, 11.65], [11.85, 12.15]]

    if zoom_bins:
        mass_bin_edges = [[11.4, 11.6], [11.6, 11.8], [11.8, 12]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    # fig.set_size_inches(2.5 * len(mass_bin_edges), 2.5 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [0, UNIVERSE_AGE - 1e-3]
    ylims = [1e-3 - 0.6, 0.4 - 1e-3]
    if mean_v_phi:
        ylims = [1e-3 - 1.6, 0.4 - 1e-3]

    #TODO use measured median age within bin instead
    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    xlabel = r'Stellar age [Gyr]'
    if r12:
        ylabel =r'$\log \, \sigma_{\rm tot} (r_{1/2}) / V_{200}$'
    else:
        ylabel =r'$\log \, \sigma_{\rm tot} (r < r_{200}) / V_{200}$'

    if mean_v_phi:
        if r12:
            ylabel = r'$\log \, \overline{v}_\phi (r_{1/2}) / V_{200}$'
        else:
            ylabel = r'$\log \, \overline{v}_\phi (r < r_{200}) / V_{200}$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)

        if z_index == 0:
            axs[col].set_xlabel('')
            axs[col].set_xticklabels([])

        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if '015_z002p012_' in combined_name_pairs[z_index][0]:
        z = 2.012
        strz = '2'
    elif '019_z001p004_' in combined_name_pairs[z_index][0]:
        z = 1.004
        strz = '1'
    elif '023_z000p503_' in combined_name_pairs[z_index][0]:
        z = 0.503
        strz = '0.5'
    else:
        z = 0
        strz = '0'

    for comp in ['Star']: #['Star', 'Gas']:
        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                label = 'LRDM'

            #z=0 output only
            with h5.File(combined_name_pairs[z_index][pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask1 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]] > 0.4
                # mask0 = np.logical_and(mask0, mask1)

                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'r200'

                # r_slice = keys.rkey_to_rcolum_dict[r_key, 0]:keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                r200 = raw_data0[keys.key0_dict['r200'][None]][:]
                sz = raw_data0[keys.key0_dict['sigma_z'][comp]][:, rsn:rsx]
                sR = raw_data0[keys.key0_dict['sigma_R'][comp]][:, rsn:rsx]
                sphi = raw_data0[keys.key0_dict['sigma_phi'][comp]][:, rsn:rsx]

                szhalf = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                sRhalf = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                sphihalf = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

                t0_half = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['r200', None], 2]

                N0 = raw_data0[keys.key0_dict['Npart'][comp]][:, rsn:rsx]

                #add kinetic energy because that's what Arron has done
                if ek or mean_v_phi:
                    vphi = raw_data0[keys.key0_dict['mean_v_phi'][comp]][:, rsn:rsx]

                if dmsigma:
                    dsz = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    dsy = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    dsx = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                half_age0 = raw_data0[keys.key0_dict['age'][None]][:, rsn:rsx, 2]

                #mask and units
                V200 = np.sqrt(GRAV_CONST * M200 / r200) # km/s
                m0 = np.log10(M200[mask0]) + 10 # log Mstar
                if mean_v_phi:
                    y0 = np.log10(vphi[mask0] / V200[mask0, np.newaxis])
                    #fix counter rotating radii
                    y0[np.isnan(y0)] = 1e-10
                elif ek:
                    y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2 +
                                          vphi[mask0]**2) / V200[mask0, np.newaxis])
                else:
                    y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2) / V200[mask0, np.newaxis])
                    y0_half = np.log10(np.sqrt(szhalf[mask0] ** 2 + sRhalf[mask0] ** 2 + sphihalf[mask0] ** 2) /
                                       V200[mask0])
                t0_half = t0_half[mask0]
                N0 = N0[mask0]
                half_age0 = half_age0[mask0]

                #assume all haloes have enough DM particles
                if dmsigma:
                    dy0 = np.log10(np.sqrt(dsz[mask0] ** 2 + dsy[mask0] ** 2 + dsx[mask0] ** 2) /
                                   V200[mask0])

            with h5.File(combined_name_pairs[z_index][1-pair], 'r') as raw_data0:
                mask1 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                M2001 = raw_data0[keys.key0_dict['m200'][None]][:]
                t1_half = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['r200', None], 2]
                m1 = np.log10(M2001[mask1]) + 10  # log Mstar
                t1_half = t1_half[mask1]

            for col, mass_range in enumerate(mass_bin_edges):

                #theory
                min_t = mgm.lbt(np.array([1 / (1 + z)]))[0]
                ages = np.linspace(min_t, UNIVERSE_AGE)
                axs[col].axvline(UNIVERSE_AGE - min_t, 0, 1,
                                 color='k', ls=':', lw=1, zorder=-5)

                MassDM = DM_MASS
                if pair == 0:
                    MassDM /= 7

                H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
                rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                rho_200 = 200 * rho_crit

                M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
                r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
                V200 = np.sqrt(GRAV_CONST * M200 * 1e-10 / r200) #km/s

                (analytic_v_circ, analytic_dispersion,
                 theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                 stellar_dispersion, power_law_z, power_law_r, power_law_p
                 ) = mgm.get_heating_at_r12(M200 * 1e-10, MassDM=MassDM, z=z, time=ages - min_t, contracted=True)

                if mean_v_phi:
                    sigma_tot = theory_best_v
                elif ek:
                    sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2 + theory_best_v**2)
                else:
                    sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)

                if pair == 0:
                    if mean_v_phi:
                        axs[col].errorbar([0, UNIVERSE_AGE], [np.log10(analytic_v_circ / V200)]*2,
                                          c=color2, ls=linestyle2, linewidth=2, zorder=1, alpha=1)
                    else:
                        axs[col].errorbar([0, UNIVERSE_AGE], [np.log10(analytic_dispersion * np.sqrt(3) / V200)]*2,
                                          c=color2, ls=linestyle2, linewidth=2, zorder=1, alpha=1)

                axs[col].errorbar(ages - min_t, np.log10(sigma_tot / V200),
                                  c=color, ls=linestyle, linewidth=1, zorder=10, alpha=1)#, path_effects=path_effect_4)

                heat_func = interp1d(ages, np.log10(sigma_tot / V200), fill_value=np.nan, bounds_error=False)

                #data
                #mask
                mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
                y = y0[mask]
                y_half = y0_half[mask]
                t_half = t0_half[mask]
                N = N0[mask]
                half_age = half_age0[mask]
                if dmsigma:
                    dy = dy0[mask]

                # calc medians etc.
                iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                ymedians = np.zeros(iiin)
                yplus = np.zeros(iiin)
                yminus = np.zeros(iiin)

                boots = np.zeros((iiin, 3))

                age_medians = np.zeros(iiin)

                N_mask = N > N_res
                print(np.mean(N[N_mask]))
                print(np.median(N[N_mask]))
                for iii in range(iiin):
                    ys = y[:, iii][N_mask[:, iii]]

                    ymedians[iii] = np.nanmedian(ys)
                    # ymedians[iii] = np.nanmean(ys)
                    yplus[iii] = np.nanquantile(ys, 0.84)
                    yminus[iii] = np.nanquantile(ys, 0.16)

                    boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                    ha = half_age[:, iii][N_mask[:, iii]]
                    age_medians[iii] = np.nanmedian(ha)

                #plot
                measure_func = interp1d(age_medians, ymedians, fill_value=np.nan, bounds_error=False)

                axs[col].errorbar(ages[measure_func(ages) >= heat_func(ages)] - min_t,
                                  measure_func(ages)[measure_func(ages) >= heat_func(ages)],
                             c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                             path_effects=path_effect_4)
                if linestyle == '-':
                    axs[col].errorbar(ages - min_t,
                                      measure_func(ages),
                                      c=color, ls=linestyle, linewidth=1, zorder=9, alpha=1,
                                      path_effects=path_effect_2)
                else: #linestyle == '--':
                    axs[col].errorbar(ages[measure_func(ages) <= heat_func(ages)] - min_t,
                                      measure_func(ages)[measure_func(ages) <= heat_func(ages)],
                                      c=color, ls=linestyle, linewidth=1, zorder=9, alpha=1,
                                      path_effects=path_effect_2)
                
                if shade:
                    axs[col].fill_between(age_medians - min_t, yminus, yplus,
                                     color=color, alpha=0.5, edgecolor='k', zorder=8)

                    axs[col].fill_between(age_medians - min_t, boots[:, 0], boots[:, 2],
                                          color=color, alpha=0.5, edgecolor='k', zorder=9, hatch=hatches[linestyle])

                if show_all:  # np.sum(mask0) < 100 or
                    for i in range(np.sum(mask)):
                        axs[col].errorbar(age_medians - min_t, y[i],
                                     color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

                if dmsigma:
                    dymedians = np.nanmedian(dy)
                    # dymedians = np.nanmean(ys)
                    dyplus = np.nanquantile(dy, 0.84)
                    dyminus = np.nanquantile(dy, 0.16)
                    # boots = my_bootstrap(ys, statistic=np.nanmedian)

                    # plot
                    axs[col].errorbar([0, UNIVERSE_AGE], [dymedians]*2,
                                      c=color2, ls=linestyle2, linewidth=2, zorder=10, alpha=1,
                                      path_effects=path_effect_4)

                    if shade:
                        axs[col].fill_between([0, UNIVERSE_AGE], [dyminus]*2, [dyplus]*2,
                                              color=color2, alpha=0.5, edgecolor='k', zorder=8)

                frac_size = 8
                this_t = (np.median(t0_half[m0 > 11]) + np.median(t1_half[m1 > 11]))/2 - min_t
                this_y = np.median(y_half)

                this_x_lim = xlims[1]
                if z_index != 0:
                    this_x_lim = xlims[1] - min_t + (xlims[1] - xlims[0]) / frac_size

                axs[col].arrow(this_x_lim, this_y,
                               -(xlims[1] - xlims[0]) / frac_size, 0,
                               length_includes_head=True,
                               head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                               head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                               color=color, lw=1, zorder=-9, alpha=1)
                # axs[col].arrow(this_t, ylims[0] + 0.26 * (ylims[1] - ylims[0]),
                #                0, (ylims[1] - ylims[0]) / frac_size,
                #                length_includes_head=True,
                #                head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                #                head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                #                color=color, lw=1, zorder=-9, alpha=1)
                if pair == 1:
                    axs[col].arrow(this_t, ylims[0] + 0.26 * (ylims[1] - ylims[0]),
                                   0, (ylims[1] - ylims[0]) / frac_size,
                                   length_includes_head=True,
                                   head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                                   head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                                   color='k', fc='white', lw=1, zorder=-9, alpha=1)

                if col == 0 and z_index == 1:
                    if pair == 1:
                        axs[col].text(this_t + 0.01 * (xlims[1] - xlims[0]), ylims[0] + 0.26 * (ylims[1] - ylims[0]),
                                      r'$\langle t_{50} \rangle$', va='bottom')

                        axs[col].text(this_x_lim - 2 * (xlims[1] - xlims[0]) / frac_size / 3, this_y,
                                      r'$\langle \sigma_{\rm tot}^2 \rangle^{1/2}$', ha='left', va='bottom', color=color)


                # axs[col].arrow(this_t + (xlims[1] - xlims[0]) / frac_size, this_y,
                #                -(xlims[1] - xlims[0]) / frac_size, 0,
                #                length_includes_head=True,
                #                head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                #                head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                #                color=color, lw=1, zorder=-9, alpha=1)
                # axs[col].arrow(this_t, this_y - (ylims[1] - ylims[0]) / frac_size,
                #                0, (ylims[1] - ylims[0]) / frac_size,
                #                length_includes_head=True,
                #                head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                #                head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                #                color=color, lw=1, zorder=-9, alpha=1)

                # print(this_t, this_y)
                # axs[col].scatter(this_t, this_y,
                #                  color=color, ec='k', lw=2, marker='o', s=10**2, zorder=15)
                # print(np.median(t_half) - min_t, np.median(y_half))
                # axs[col].scatter(np.median(t_half) - min_t, np.median(y_half),
                #                  color=color, ec='k', lw=2, marker='s', s=10**2, zorder=15)
                # print(np.median(t_half) - min_t, np.mean(y_half))
                # axs[col].scatter(np.median(t_half) - min_t, np.mean(y_half),
                #                  color=color, ec='k', lw=2, marker='p', s=10**2, zorder=15)


    l1p = 0
    l2p = 1
    if r12 and z_index == 0:
        l1p = 1
        l2p = 2

    if not mean_v_phi and z_index==0:
        axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                       (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                       ],
                      [r'HR stars', r'LR stars'],
                      # labelspacing=0.2, fontsize=11,
                      loc='upper left')

        axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1)),
                       (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1)),
                       ],
                      ['heating by HR DM', 'heating by LR DM'],
                      # labelspacing=0.2, fontsize=11,
                      loc='upper left')

        # axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=4, path_effects=path_effect_8)),
        #                (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=2, path_effects=path_effect_4)),
        #                ],
        #               [r'$\sigma_{\rm tot} > \sigma_{\rm heat}$', r'$\sigma_{\rm tot} < \sigma_{\rm heat}$'],
        #               labelspacing=0.2, fontsize=11,
        #               loc='upper left')

    # axs[2].legend([], [],
    #               loc='upper left')

    # if z_index == 0:
    #     ha = 'right'
    #     dx = 0.95
    # else:
    #     ha = 'left'
    #     dx = 0.05

    ha = 'right'
    dx = 0.95
    if z_index == 0:
        dy = 0.26 #0.32
    else:
        dy = 0.07

    for i in range(len(mass_bin_edges)):
        if z_index == 0:
            axs[i].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                        r'$\log \, M_{200} / $M$_\odot$' + '\n' + r'$\in$' + str(mass_bin_edges[i]),
                        ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)

        axs[i].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                    r'$z=$' + strz,
                    ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)

    # for i in range(0, len(mass_bin_edges)):
    #     axs[i].legend([],[],
    #                   title=r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[i]),
    #                   loc='upper left')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200, pad_inches=0)
        plt.close()

    return


def kappa_co_age_plot(name='', save=False, shade=False):
    N_res = 2
    z_index = 0

    fig = plt.figure(constrained_layout=True)

    # mass_bin_edges = [[10.35,10.65], [10.85,11.15], [11.35, 11.65], [11.85, 12.15]]#, [12.35, 12.65]]
    mass_bin_edges = [[11.35, 11.65], [11.85, 12.15], [12.35, 12.65]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    # fig.set_size_inches(2.5 * len(mass_bin_edges), 2.5 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [0, UNIVERSE_AGE - 1e-3]
    ylims = [0, 1]

    #TODO use measured median age within bin instead
    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    xlabel = r'Stellar age [Gyr]'
    ylabel =r'$\kappa_{\rm co} (r_{1/2})$'

    N_run = np.linspace(0, 14, 8)
    # N_run = np.linspace(0, 14, 15)
    # N_run = np.linspace(0, 14, 29)

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)

        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if '015_z002p012_' in combined_name_pairs[z_index][0]:
        z = 2.012
        strz = '2'
    elif '019_z001p004_' in combined_name_pairs[z_index][0]:
        z = 1.004
        strz = '1'
    elif '023_z000p503_' in combined_name_pairs[z_index][0]:
        z = 0.503
        strz = '0.5'
    else:
        z = 0
        strz = '0'

    r_key = 'eq_r12'

    for pair in range(1):
        if pair == 0: # 0 (7x)
            linestyle = '-'
            linestyle2 = '-.'
            marker = 'o'
            color = 'C0'
            color2 = 'navy'
            label = 'HRDM'
        else:# 1 (1x)
            linestyle = '--'
            linestyle2 = (0, (0.8, 0.8))
            marker = 'o'
            color = 'C1'
            color2 = 'saddlebrown'
            label = 'LRDM'

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][pair], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]

            kappa_co_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[r_key, None]]
            age_0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict[r_key, None], 2]

            N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict[r_key, None]]

            gn0 = gn0[mask0]
            M200_0 = M200_0[mask0] * 1e10
            kappa_co_0 = kappa_co_0[mask0]
            age_0 = age_0[mask0]
            N0 = N0[mask0]

            m0 = np.log10(M200_0)
            x0 = age_0
            y0 = kappa_co_0

        with h5.File(combined_name_pairs[z_index][pair+1], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
            gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

            #load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]

            kappa_co_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[r_key, None]]
            age_1 = raw_data1[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict[r_key, None], 2]

            N1 = raw_data1[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict[r_key, None]]

            gn1 = gn1[mask1]
            M200_1 = M200_1[mask1] * 1e10
            kappa_co_1 = kappa_co_1[mask1]
            age_1 = age_1[mask1]
            N1 = N1[mask1]

            m1 = np.log10(M200_1)
            x1 = age_1
            y1 = kappa_co_1

        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        # not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        # not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0))
        order1 = np.argsort(np.argsort(matched_gn_1))

        my0 = y0[in0][order0]
        mx0 = x0[in0][order0]
        mm0 = m0[in0][order0]

        my1 = y1[in1][order1]
        mx1 = x1[in1][order1]
        mm1 = m1[in1][order1]

        for col, mass_range in enumerate(mass_bin_edges):
            mask = np.logical_and(mass_range[0] < mm0, mm0 < mass_range[1])
            x_0 = mx0[mask]
            x_1 = mx1[mask]
            y_0 = my0[mask]
            y_1 = my1[mask]
            for _x_0, _x_1, _y_0, _y_1 in zip(x_0, x_1, y_0, y_1):
                if _y_0 >= _y_1: c='C0'
                else: c='C1'
                axs[col].errorbar([_x_0, _x_1], [_y_0, _y_1], c=c, lw=0.5, alpha=0.5)

            mask = np.logical_and(mass_range[0] < mm1, mm1 < mass_range[1])
            x_0 = mx0[mask]
            x_1 = mx1[mask]
            y_0 = my0[mask]
            y_1 = my1[mask]
            for _x_0, _x_1, _y_0, _y_1 in zip(x_0, x_1, y_0, y_1):
                if _y_0 >= _y_1: c='C0'
                else: c='C1'
                axs[col].errorbar([_x_0, _x_1], [_y_0, _y_1], c=c, lw=0.5, alpha=0.5)

            #data
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
            y = y0[mask]
            x = x0[mask]

            linestyle = '-'
            linestyle2 = '-.'
            marker = 'o'
            color = 'C0'
            color2 = 'navy'
            label = 'HRDM'

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, _, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, _, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask_count = counts > 2 * np.diff(N_run)[0]

            xlin = np.linspace(xcentres[mask_count][0], xcentres[mask_count][-1])
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1,
                              path_effects=path_effect_4)

            if shade:
                yplus_func = interp1d(xcentres, yplus, fill_value='extrapolate')
                yminus_func = interp1d(xcentres, yminus, fill_value='extrapolate')
                axs[col].errorbar(xlin, yplus_func(xlin),
                                  c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1)
                axs[col].errorbar(xlin, yminus_func(xlin),
                                  c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1)
                axs[col].fill_between(xcentres[mask_count], yminus[mask_count], yplus[mask_count],
                                      color=color, alpha=0.5, zorder=2)
            
            axs[col].scatter(x, y,
                             c=color, marker='o', lw=0, s=5,
                             zorder=1, alpha=1)


            mask = np.logical_and(mass_range[0] < m1, m1 < mass_range[1])
            y = y1[mask]
            x = x1[mask]

            linestyle = '--'
            linestyle2 = (0, (0.8, 0.8))
            marker = 'o'
            color = 'C1'
            color2 = 'saddlebrown'
            label = 'LRDM'

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, _, _ = binned_statistic(x, y,
                                           statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, _, _ = binned_statistic(x, y,
                                            statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask_count = counts > 2 * np.diff(N_run)[0]

            xlin = np.linspace(xcentres[mask_count][0], xcentres[mask_count][-1])
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1,
                              path_effects=path_effect_4)

            if shade:
                yplus_func = interp1d(xcentres, yplus, fill_value='extrapolate')
                yminus_func = interp1d(xcentres, yminus, fill_value='extrapolate')
                axs[col].errorbar(xlin, yplus_func(xlin),
                                  c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1)
                axs[col].errorbar(xlin, yminus_func(xlin),
                                  c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1)
                axs[col].fill_between(xcentres[mask_count], yminus[mask_count], yplus[mask_count],
                                      color=color, alpha=0.5, zorder=2)

            axs[col].scatter(x, y,
                             c=color, marker='o', lw=0, s=5,
                             zorder=1, alpha=1)

    for i in range(len(mass_bin_edges)):
        if z_index == 0:
            axs[i].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                        r'$\log \, M_{200} / $M$_\odot\in$' + str(mass_bin_edges[i]),
                        va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200, pad_inches=0)
        plt.close()

    return


def height_age(name='', save=False, r12=True, shade=True, show_all=False, z_index=0,
               zoom_bins=False):
    N_res = 10

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10.4,10.6], [11.4,11.6], [12.4, 12.6]]
    # mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]
    # mass_bin_edges = [[10.5,10.7], [11,11.2], [11.5,11.7]]
    # mass_bin_edges = [[10.8,11], [11,11.2], [11.2,11.4]]
    # mass_bin_edges = [[10.8, 11], [11,11.2], [11.2,11.4], [11.4, 11.6], [11.6, 11.8]]
    if zoom_bins:
        mass_bin_edges = [[11.4, 11.6], [11.6, 11.8], [11.8, 12]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 0, UNIVERSE_AGE - 1e-3]
    ylims = [1e-3 - 0.6, 1 - 1e-3]

    #TODO use measured median age within bin instead
    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    xlabel = r'$t$ [Gyr]'
    if r12:
        ylabel =r'$\log \, z_{1/2}$'
    else:
        ylabel =r'$\log \, z_{1/2}$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)
        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if '015_z002p012_' in combined_name_pairs[z_index][0]: z = 2.012
    elif '019_z001p004_' in combined_name_pairs[z_index][0]: z = 1.004
    elif '023_z000p503_' in combined_name_pairs[z_index][0]: z = 0.503
    else: z = 0

    for comp in ['Star']: #['Star', 'Gas']:
        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                label = 'LRDM'

            #z=0 output only
            with h5.File(combined_name_pairs[z_index][pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask1 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]] > 0.4
                # mask0 = np.logical_and(mask0, mask1)

                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'r200'

                # r_slice = keys.rkey_to_rcolum_dict[r_key, 0]:keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                r200 = raw_data0[keys.key0_dict['r200'][None]][:]
                z12 = raw_data0[keys.key0_dict['z12'][comp]][:, rsn:rsx]

                N0 = raw_data0[keys.key0_dict['Npart'][comp]][:, rsn:rsx]
                #not as a funciton of age
                kappa_co = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                half_age0 = raw_data0[keys.key0_dict['age'][None]][:, rsn:rsx, 2]

                #mask and units
                V200 = np.sqrt(GRAV_CONST * M200 / r200) # km/s
                m0 = np.log10(M200[mask0]) + 10 # log Mstar
                y0 = np.log10(z12[mask0])
                N0 = N0[mask0]
                half_age0 = half_age0[mask0]
                kappa_co = kappa_co[mask0]

            for col, mass_range in enumerate(mass_bin_edges):

                #mask
                mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
                y = y0[mask]
                N = N0[mask]
                half_age = half_age0[mask]
                kappa = kappa_co[mask]

                mask2 = kappa > 0.4
                y = y[mask2]
                N = N[mask2]
                half_age = half_age[mask2]
                kappa = kappa[mask2]

                # calc medians etc.
                iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                ymedians = np.zeros(iiin)
                yplus = np.zeros(iiin)
                yminus = np.zeros(iiin)

                boots = np.zeros((iiin, 3))

                age_medians = np.zeros(iiin)

                N_mask = N > N_res
                for iii in range(iiin):
                    ys = y[:, iii][N_mask[:, iii]]

                    ymedians[iii] = np.nanmedian(ys)
                    # ymedians[iii] = np.nanmean(ys)
                    yplus[iii] = np.nanquantile(ys, 0.84)
                    yminus[iii] = np.nanquantile(ys, 0.16)

                    boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                    ha = half_age[:, iii][N_mask[:, iii]]
                    age_medians[iii] = np.nanmedian(ha)

                #plot
                axs[col].errorbar(age_medians, ymedians,
                             c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                             path_effects=path_effect_4)

                if shade:
                    axs[col].fill_between(age_medians, yminus, yplus,
                                     color=color, alpha=0.5, edgecolor='k', zorder=8)

                    axs[col].fill_between(age_medians, boots[:, 0], boots[:, 2],
                                          color=color, alpha=0.5, edgecolor='k', zorder=9, hatch=hatches[linestyle])

                if show_all:  # np.sum(mask0) < 100 or
                    for i in range(np.sum(mask)):
                        axs[col].errorbar(age_medians, y[i],
                                     color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

                #theory
                min_t = mgm.lbt(np.array([1 / (1 + z)]))[0]
                ages = np.linspace(min_t,UNIVERSE_AGE)
                axs[col].axvline(min_t, 0, 1,
                                 color='k', ls=':', lw=1, zorder=-5)

                # MassDM = DM_MASS
                # if pair == 0:
                #     MassDM /= 7
                #
                # H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
                # rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                # rho_200 = 200 * rho_crit
                #
                # M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
                # r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
                # V200 = np.sqrt(GRAV_CONST * M200 * 1e-10 / r200) #km/s
                #
                # (analytic_v_circ, analytic_dispersion,
                #  theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                #  stellar_dispersion, power_law_z, power_law_r, power_law_p
                #  ) = mgm.get_heating_at_r12(M200 * 1e-10, MassDM=MassDM, z=z, time=ages - min_t, contracted=True)
                #
                # if ek:
                #     sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2 + theory_best_v**2)
                # else:
                #     sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)
                #
                # if pair == 0:
                #     axs[col].errorbar([0, UNIVERSE_AGE], [np.log10(analytic_dispersion * np.sqrt(3) / V200)]*2,
                #                       c=color2, ls=linestyle2, linewidth=2, zorder=10, alpha=1)
                #
                # axs[col].errorbar(ages, np.log10(sigma_tot / V200),
                #                   c=color2, ls=linestyle2, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

    l1p = 0
    l2p = 1
    if r12 and z_index == 0:
        l1p = 1
        l2p = 2

    axs[l1p].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                   ],
                  [r'HR DM measurements', r'LR DM measurements'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  loc='upper left')

    axs[l2p].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls=(0, (0.8, 0.8)), lw=2,
                                 path_effects=path_effect_4)),
                   ],
                  ['HR spurious heating', 'LR spurious heating'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  loc='upper left')

    # axs[2].legend([], [],
    #               loc='upper left')

    if z_index == 0:
        ha = 'right'
        dx = 0.95
    else:
        ha = 'left'
        dx = 0.05

    axs[0].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[0]),
                ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)
    axs[1].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[1]),
                ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)
    axs[2].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.07 * (ylims[1] - ylims[0]),
                r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[2]),
                ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)

    axs[0].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.18 * (ylims[1] - ylims[0]),
                r'$z=$' + str(z),
                ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)
    axs[1].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.18 * (ylims[1] - ylims[0]),
                r'$z=$' + str(z),
                ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)
    axs[2].text(xlims[0] + dx * (xlims[1] - xlims[0]), ylims[0] + 0.18 * (ylims[1] - ylims[0]),
                r'$z=$' + str(z),
                ha=ha, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=12)

    # for i in range(0, len(mass_bin_edges)):
    #     axs[i].legend([],[],
    #                   title=r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[i]),
    #                   loc='upper left')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def scaling_size_mass(name='', save=False, all_z=False, shade=False):

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 2 * 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[0]]

    xlims = [1e-3 + 8, 12 - 1e-3]
    # xlims = [8.33, 11.67] #Crain et al. 2015
    ylims = [-0.2, 1.8]
    # ylims = [-0.2, 1.2] #Crain et al. 2015

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(7.8, 12, 22)
    N_run = np.linspace(7.5, 12, 10)

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'$\log \, R_{1/2} / $kpc'

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(2)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0,(0.8,0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                # y0 = raw_data0[keys.key0_dict['R12'][None]][:]
                #fudge for sersic fit
                # y0 = 3/4 * raw_data0['GalaxyProfiles/Star/rHalf'][:, keys.rkey_to_rcolum_dict['lt_30', None]]
                # y0 = 2 * raw_data0['GalaxyProfiles/Star/rHalf'][:, keys.rkey_to_rcolum_dict['box', None]]
                y0 = raw_data0['GalaxyProfiles/Star/rMax'][:, keys.rkey_to_rcolum_dict['box', None]]

                # y0 = raw_data0[keys.key0_dict['r12'][None]][:]
                # y0 = 2 * raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['lt_box_R12', None]]

                #mask and units
                M200 = np.log10(M200[mask0]) + 10 # log Mstar
                x = np.log10(x0[mask0]) + 10 # log Mstar
                y = np.log10(y0[mask0]) # log kpc

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 10

            max_xbin_centre = xcentres[:-1][np.logical_not(mask)[1:]][0]

            if shade:
                yplus, xedges, _ = binned_statistic(x, y,
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x, y,
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                axs[col].fill_between(xcentres, yminus, yplus,
                                      color=color, alpha=0.5, edgecolor='k', zorder=2)

            # axs[col].errorbar(xcentres[mask], yplus[mask],
            #                   c=color, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
            # axs[col].errorbar(xcentres[mask], yminus[mask],
            #                   c=color, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            # plot with cut
            not_heated_mask = x > mstar_intercept

            #plot heated galaxies only
            if not shade:
                axs[col].scatter(x[np.logical_not(not_heated_mask)], y[np.logical_not(not_heated_mask)],
                                 color=color, marker=marker, s=0.6, alpha=0.3,
                                 linewidths=0, zorder=0, rasterized=True)

            x = x[not_heated_mask]
            y = y[not_heated_mask]

            xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
            lxlin = np.linspace(xcentres[mask][0], mstar_intercept)
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            # plot
            axs[col].errorbar(lxlin, ymed_func(lxlin),
                              c=color, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            if not shade:
                axs[col].scatter(x, y,
                                 color=color, marker=marker, s=4, alpha=0.5,
                                 linewidths=0, zorder=2, rasterized=True)

            yh=1
            if z == 0:
                if pair == 1: yh = 0.71
                else: yh = 0.59
            axs[col].axvline(mstar_intercept, 0, yh,
                             color=color2, ls=linestyle2, linewidth=1, zorder=-3)

        #decorations
        frac_size = 8
        axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(EAGLE_EPS),
                       -(xlims[1] - xlims[0]) / frac_size, 0,
                       length_includes_head=True,
                       head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                       head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                       color='k', fc='white', lw=1, zorder=-15, alpha=1)
        axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(2.8*EAGLE_EPS),
                       -(xlims[1] - xlims[0]) / frac_size, 0,
                       length_includes_head=True,
                       head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                       head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                       color='k', fc='white', lw=1, zorder=-15, alpha=1)

        # if col in (0,1):
        axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                      np.log10(EAGLE_EPS) + 0.000 * (ylims[1] - ylims[0]),
                    r'$\epsilon$', va='bottom')
        axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
                      np.log10(2.8 * EAGLE_EPS) + 0.005 * (ylims[1] - ylims[0]),
                    r'$2.8 \times \epsilon$', va='bottom')

        # axs[col].text(xlims[1] -1.55*(xlims[1] - xlims[0]) / frac_size,
        #               np.log10(2.8 * EAGLE_EPS) - 0.015 * (ylims[1] - ylims[0]),
        #             r'$2.8 \times \epsilon$', va='top')

        # for i in range(20):
        #     #1/3 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i/3 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) / 3,
        #                               i/3 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        log_M200s = np.linspace(xlims[0]+1, xlims[1]+5, 201) #M_sun

        H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3))
        rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
        rho_200 = 200 * rho_crit

        r200 = np.power(3 * 10**(log_M200s -10) / (4 * np.pi * rho_200), 1 / 3)  # kpc

        log_Mstar = mass_function(mass_function_args(z, True), log_M200s)

        show_r200 = False
        if show_r200:
            for mult in [0.1, 0.01]:
                axs[col].errorbar(log_Mstar, np.log10(r200 * mult),
                                color='k', ls=(0,(1.5,1.8)), lw=1, zorder=2, path_effects=white_path_effect_3)

            index = int(3.5 / 8 * len(log_Mstar))
            slope = (np.diff(np.log10(0.1 * r200)) / np.diff(log_Mstar))[index + 10] #-0.01
            axs[col].text(log_Mstar[index] - 0.005 * (xlims[1] - xlims[0]),
                        np.log10(0.1 * r200[index]) - 0.02 * (ylims[1] - ylims[0]),
                        r'$0.1 \times r_{200}$', rotation_mode='anchor', ha='left', va='top',
                        rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims)) * 180 / np.pi)[0],
                        color='k', path_effects=white_path_effect_2)
            index = int(3.9 / 8 * len(log_Mstar))
            slope = (np.diff(np.log10(0.01 * r200)) / np.diff(log_Mstar))[index + 16]
            axs[col].text(log_Mstar[index],
                        np.log10(0.01 * r200[index]) - 0.02 * (ylims[1] - ylims[0]),
                        r'$0.01 \times r_{200}$', rotation_mode='anchor', ha='left', va='top',
                        rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims)) * 180 / np.pi)[0],
                        color='k', path_effects=white_path_effect_2)

        show_size_mass = False
        if show_size_mass:
            M200s = np.linspace(xlims[0]+1, xlims[1]+5, 201)

            f = 0.75
            xs = mass_function(mass_function_args(z, True), M200s)
            ys = size_function(size_function_args(z, True), M200s)
            ys  = np.log10(f * 10**ys)

            axs[col].errorbar(xs, ys,
                              ls='-.', c='navy', lw=2, zorder=3)


        if z == 0:
            Mstar = np.logspace(8, 12, 201)

            #Shen et al. 2003 eq 18 and 19
            alpha = 0.14
            beta = 0.39
            gamma = 0.10
            M_0 = 3.98 * 10**10
            sigma_1 = 0.47
            sigma_2 = 0.34

            shen_R_n_less_2p5 = gamma * Mstar**alpha * (1 + Mstar/M_0)**(beta - alpha)
            shen_sigma_in_ln = sigma_2 + (sigma_1 - sigma_2) / (1 + (Mstar / M_0)**2)

            axs[col].errorbar(np.log10(Mstar), np.log10(shen_R_n_less_2p5),
                              ls='-', c='grey', lw=1, zorder=3)#    , path_effects=path_effect_2)
            axs[col].errorbar(np.log10(Mstar), np.log10(shen_R_n_less_2p5) + np.log10(np.exp(shen_sigma_in_ln)),
                              ls=(0,(1.2,1.2)), c='grey', lw=1, zorder=3)
            axs[col].errorbar(np.log10(Mstar), np.log10(shen_R_n_less_2p5) - np.log10(np.exp(shen_sigma_in_ln)),
                              ls=(0,(1.2,1.2)), c='grey', lw=1, zorder=3)

            b = 2.88 * 10**-6 #Erratum
            a = 0.56

            shen_R_n_gtr_2p5 = b * Mstar**a

            axs[col].errorbar(np.log10(Mstar), np.log10(shen_R_n_gtr_2p5),
                              ls='--', c='grey', lw=1, zorder=3)#    , path_effects=path_effect_2)
            # axs[col].errorbar(np.log10(Mstar), np.log10(shen_R_n_gtr_2p5) + np.log10(np.exp(shen_sigma_in_ln)),
            #                   ls=(0,(1.2,1.2)), c='grey', lw=1, zorder=3)
            # axs[col].errorbar(np.log10(Mstar), np.log10(shen_R_n_gtr_2p5) - np.log10(np.exp(shen_sigma_in_ln)),
            #                   ls=(0,(1.2,1.2)), c='grey', lw=1, zorder=3)

            #Baldry et al. 2012 #yoinked from graph https://apps.automeris.io/wpd/
            Baldry_red_x = [8.3236, 8.8220, 9.2815, 9.7799, 10.2718, 10.6861, 11.0939]
            Baldry_red_y = np.array([0.06135, 0.26380, 0.26075, 0.34356, 0.4755, 0.64110, 0.85890])
            Baldry_red_plus = np.array([0.34663, 0.45399, 0.47239, 0.54908, 0.66564, 0.86196, 1.02761])
            Baldry_red_minus = np.array([-0.21166, -0.00920, 0.01227, 0.10736, 0.27301, 0.47853, 0.70552])

            Baldry_blue_x = [8.2977, 8.7638, 9.2168, 9.7217, 10.2330, 10.6731, 11.0874]
            Baldry_blue_y = np.array([0.21166, 0.38344, 0.50920, 0.60736, 0.69325, 0.82822, 1.01534])
            Baldry_blue_plus = np.array([0.45092, 0.59202, 0.69018, 0.78221, 0.88650, 1.03067, 1.17485])
            Baldry_blue_minus = np.array([-0.05215, 0.08282, 0.23620, 0.35890, 0.48466, 0.68098, 0.71779])

            axs[col].errorbar(Baldry_red_x, Baldry_red_y, (Baldry_red_plus-Baldry_red_y, Baldry_red_y-Baldry_red_minus),
                              ls='', marker='s', c='white', mec='grey', ecolor='grey', ms=4, lw=1, zorder=3)
            axs[col].errorbar(Baldry_blue_x, Baldry_blue_y, (Baldry_blue_plus-Baldry_blue_y, Baldry_blue_y-Baldry_blue_minus),
                              ls='', marker='o', c='white', mec='grey', ecolor='grey', ms=4, lw=1, zorder=3)

    # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(4),
    #                              lw=0, alpha=0.6, mew=0)),
    #                (lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(0.6),
    #                              lw=0, alpha=0.3, mew=0)),],
    #               ['Converged\ngalaxies', 'Unconverged\ngalaxies'],
    #               numpoints = 3,
    #               loc='upper left')#, frameon=False

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='white', mec='grey', ls='', marker='o', lw=1)),
                   (lines.Line2D([0, 1], [0, 1], color='white', mec='grey', ls='', marker='s', lw=1)),
                   (lines.Line2D([0, 1], [0, 1], color='grey', ls=':', lw=1),
                    lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=1),
                    lines.Line2D([0, 1], [0, 1], color='grey', ls=':', lw=1)),
                   (lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=1)),
                   ],
                  # ['Badry et al. 2012, Blue galaxies',
                  #  'Badry et al. 2012, Red galaxies',
                  #  r'Shen et al. 2003, $n_s < 2.5$',
                  #  r'Shen et al. 2003, $n_s > 2.5$',
                  ['Baldry+ 12, Blue galaxies',
                   'Baldry+ 12, Red galaxies',
                   r'Shen+ 03, $n_s < 2.5$',
                   r'Shen+ 03, $n_s > 2.5$',
                   ],
                  handler_map={tuple: HandlerTupleVertical(ndivide=3)},
                  loc='upper left',
                  # bbox_to_anchor=(-0.03,1.03),
                  fontsize=9,
                  labelspacing=0.3,)
                  # frameon=False)

    # axs[0].text(xlims[0] + 0.38 * (xlims[1] - xlims[0]),
    #             ylims[0] + 0.915 * (ylims[1] - ylims[0]),
    #             'Shen+ 03',
    #             fontsize=10, zorder=20)
    # axs[0].text(xlims[0] + 0.30 * (xlims[1] - xlims[0]),
    #             ylims[0] + 0.79 * (ylims[1] - ylims[0]),
    #             'Badry+ 12',
    #             fontsize=10, zorder=20)


    # Shen_lines = (lines.Line2D([0, 1], [0, 1], color='grey', ls=':', lw=1),
    #               lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=1),
    #               lines.Line2D([0, 1], [0, 1], color='grey', ls=':', lw=1),
    #               lines.Line2D([0, 1], [0, 1], color='grey', ls='', lw=1),
    #               lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=1))
    # Baldry_lines = (lines.Line2D([0, 1], [0, 1], color='white', mec='grey', ls='', marker='o', lw=1),
    #                 lines.Line2D([0, 1], [0, 1], color='white', mec='grey', ls='', marker='s', lw=1))
    #
    # axs[0].legend([Shen_lines, Baldry_lines,
    #                (lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
    #                ],
    #               [# r'Shen et al. 2003',
    #                # r'Badry et al. 2012'
    #                r'$n_s < 2.5$ Shen+03',
    #                'Badry+12 cBlue, Red galaxies',
    #                'HR DM', 'LR DM',
    #               ],
    #               handler_map={Shen_lines: HandlerTupleVertical(ndivide=5),
    #                            Baldry_lines: HandlerTuple(ndivide=None)},
    #               loc='upper left',
    #               labelspacing=0.3, bbox_to_anchor=(-0.03,1.03),
    #               frameon=False)

    axs[0].text(xlims[0] + 0.04 * (xlims[1] - xlims[0]), ylims[0] + 0.67 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    if all_z:
        axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def scaling_vc_mass(name='', save=False):
    uncontracted=False

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [1e-3 + 8, 12 - 1e-3]
    ylims = [1e-3 + 1.4, 2.733 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(7.5, 12, 10)

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'$\log \, V_c (r_{1/2}) / $km s$^{-1}$'

    axs = []
    for col in range(1):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 0: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        axs[col].set_aspect(3)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate([combined_name_pairs[0]]):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                mstar = raw_data0[keys.key0_dict['mstarr'][None]][:, keys.rkey_to_rcolum_dict['lt_r12', None]]
                mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, keys.rkey_to_rcolum_dict['lt_r12', None]]
                mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, keys.rkey_to_rcolum_dict['lt_r12', None]]

                # mbh = raw_data0['GalaxyQuantities/BHMass'][()] # 10^10 M_sun
                mbh = raw_data0['GalaxyQuantities/BHGravMass'][()]  # 10^10 M_sun

                r12 = raw_data0[keys.key0_dict['r12'][None]][:]
                # r12 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

                mtot = mstar + mgas + mdm + mbh # 10^10 M_sun

                vc = np.sqrt(GRAV_CONST * mtot / r12)

                #mask and units
                M200 = np.log10(M200[mask0]) + 10 # log Mstar
                x = np.log10(x0[mask0]) + 10 # log Mstar
                y = np.log10(vc[mask0]) # log km/s

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]

            # plot
            axs[col].errorbar(xcentres[mask], ymedians[mask],
                              c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            # plot with cut
            not_heated_mask = x > mstar_intercept

            #plot heated galaxies only
            axs[col].scatter(x[np.logical_not(not_heated_mask)], y[np.logical_not(not_heated_mask)],
                             color=color, marker=marker, s=0.6, alpha=0.3,
                             linewidths=0, zorder=0, rasterized=True)

            x = x[not_heated_mask]
            y = y[not_heated_mask]

            xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            # plot
            axs[col].errorbar(xlin, ymed_func(xlin),
                              c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            axs[col].scatter(x, y,
                             color=color, marker=marker, s=4, alpha=0.5,
                             linewidths=0, zorder=2, rasterized=True)

            axs[col].axvline(mstar_intercept, 0, 1,
                             color=color2, ls=linestyle2, linewidth=1, zorder=-3)

            #contracted Vc
            if pair - 1:
                MassDM = DM_MASS / 7
            else:
                MassDM = DM_MASS

            log_M200s = np.linspace(8, 15, 51)
            log_Mstars = mass_function(mass_function_args(z, True), log_M200s)  # log M_sun

            out = np.array([mgm.get_heating_at_r12(10 ** (log_M200s[i] - 10), MassDM=MassDM, z=z,
                                                   time=0, contracted=True, hr_size=pair - 1)
                            for i in range(len(log_M200s))])
            (analytic_v_circ, analytic_dispersion,
             theory_best_v, theory_best_z, theory_best_r, theory_best_p,
             stellar_dispersion, power_law_z, power_law_r, power_law_p
             ) = out.T

            axs[col].errorbar(log_Mstars, np.log10(analytic_v_circ),
                              c=color2, ls='-.', linewidth=2, zorder=3, alpha=1)

        #decorations
        for i in range(20):
            #1 Mo, Mao, White style.
            axs[col].errorbar(xlims, [i * 1/3 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 1/3,
                                      i * 1/3 + np.floor(ylims[0])],
                              c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        #NFW
        # if pair: MassDM = DM_MASS
        # else:

        if uncontracted:
            out = np.array([mgm.get_heating_at_r12(10**(log_M200s[i]-10), MassDM=MassDM, z=z,
                                                   time=0, contracted=False)
                            for i in range(len(log_M200s))])
            (analytic_v_circ, analytic_dispersion,
             theory_best_v, theory_best_z, theory_best_r, theory_best_p,
             stellar_dispersion, power_law_z, power_law_r, power_law_p
            ) = out.T

            axs[col].errorbar(log_Mstars, np.log10(analytic_v_circ),
                              c='cyan', ls=':', linewidth=2, zorder=3, alpha=1)

        #contracted
        # if False:

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=1, path_effects=path_effect_2)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=1, path_effects=path_effect_2)),
    #                ],
    #               ['HR DM\nmedian', 'LR DM\nmedian'],
    #               loc='lower right')#, frameon=False)s
    #
    # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(4),
    #                              lw=0, alpha=0.6, mew=0)),
    #                (lines.Line2D([0, 1], [0, 1], color='grey', ls='', marker='o', markersize=np.sqrt(0.6),
    #                              lw=0, alpha=0.3, mew=0)),],
    #               ['Converged\ngalaxies', 'Unconverged\ngalaxies'],
    #               numpoints = 3,
    #               loc='lower right')#, frameon=False

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
                   ],
                  ['Contracted\nNFW halo'],
                  loc='lower right')#, frameon=False

    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
    #             r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
    #             r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    # axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
    #             r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def scaling_am_mass(name='', save=False, split_kappa=True):

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)
    # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
    # spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [1e-3 + 8, 12 - 1e-3]
    ylims = [1e-3 + 0, 4 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    N_run = np.linspace(7.5, 12, 10)

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'$\log \, j_\star / $kpc km s$^{-1}$'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))

    # for col in range(len([combined_name_pairs[0]])):
        # axs.append(fig.add_subplot(spec[0]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_xlabel(xlabel)

        axs[col].set_aspect(1)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(combined_name_pairs):
    # for col, name_pairs in enumerate([combined_name_pairs[0]]):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                cmap = 'Blues'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                cmap = 'Oranges'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                y0 = raw_data0[keys.key0_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
                yq0 = raw_data0[keys.key1_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
                y0 = y0 / yq0

                kappa_co = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

                #mask and units
                M200 = np.log10(M200[mask0]) + 10 # log Mstar
                x = np.log10(x0[mask0]) + 10 # log Mstar
                y = np.log10(y0[mask0]) # log kpc
                kappa_co = kappa_co[mask0]

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, xedges, _ = binned_statistic(x, y,
                                                statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
            yminus, xedges, _ = binned_statistic(x, y,
                                                 statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 20 * np.diff(N_run)[0]

            m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
            mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

            xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
            ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

            not_heated_mask = x > mstar_intercept

            # plot
            # axs[col].errorbar(xcentres[mask], ymedians[mask],
            #                   c=color, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
            # axs[col].errorbar(xlin, ymed_func(xlin),
            #                   c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            if split_kappa:
                #plot heated galaxies only
                axs[col].scatter(x[np.logical_not(not_heated_mask)], y[np.logical_not(not_heated_mask)],
                                 color=color,
                                 # c=kappa_co[np.logical_not(not_heated_mask)], cmap = cmap,
                                 marker=marker, s=0.6, alpha=0.3,
                                 linewidths=0, zorder=0, rasterized=True)
                axs[col].scatter(x[not_heated_mask], y[not_heated_mask],
                                 # color=color,
                                 c=kappa_co[not_heated_mask], cmap=cmap, ec=color, vmin=0, vmax=0.8,
                                 marker=marker, s=4, alpha=1,
                                 linewidths=0.2, zorder=2, rasterized=True)

                axs[col].axvline(mstar_intercept, 0, 1,
                                 color=color2, ls=linestyle2, linewidth=1, zorder=-3)

                # kappas
                ymedians, xedges, bin_is = binned_statistic(x[kappa_co > 0.35], y[kappa_co > 0.35],
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                counts, _ = np.histogram(x[kappa_co > 0.35], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=matplotlib.cm.get_cmap(cmap)(0.6),
                                  ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
                axs[col].errorbar(xlin, ymed_func(xlin),
                                  c=matplotlib.cm.get_cmap(cmap)(0.6),
                                  ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

                ymedians, xedges, bin_is = binned_statistic(x[kappa_co < 0.25], y[kappa_co < 0.25],
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                counts, _ = np.histogram(x[kappa_co < 0.25], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=matplotlib.cm.get_cmap(cmap)(0.25 * 0.8),
                                  ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
                axs[col].errorbar(xlin, ymed_func(xlin),
                                  c=matplotlib.cm.get_cmap(cmap)(0.25 * 0.8),
                                  ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

            else:
                axs[col].scatter(x, y,
                                 color=color,
                                 # c=kappa_co[np.logical_not(not_heated_mask)], cmap = cmap,
                                 marker=marker, s=2, alpha=0.5,
                                 linewidths=0, zorder=0, rasterized=True)
                # axs[col].scatter(x[not_heated_mask], y[not_heated_mask],
                #                  # color=color,
                #                  c=kappa_co[not_heated_mask], cmap=cmap, ec=color, vmin=0, vmax=0.8,
                #                  marker=marker, s=4, alpha=1,
                #                  linewidths=0.2, zorder=2, rasterized=True)

                # axs[col].axvline(mstar_intercept, 0, 1,
                #                  color=color2, ls=linestyle2, linewidth=1, zorder=-3)

                # kappas
                ymedians, xedges, bin_is = binned_statistic(x, y,
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)
                axs[col].errorbar(xlin, ymed_func(xlin),
                                  c=color,
                                  ls=linestyle, linewidth=2, zorder=4, alpha=1, path_effects=path_effect_4)

        #decorations
        # for i in range(20):
        #     #1 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i * 1/2 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 2/3,
        #                               i * 1/2 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        # Sugessed by Mike
        ms = np.linspace(*xlims)
        alpha = 0.67
        j0_d = 3.17
        j0_b = 2.25
        js_d = j0_d + alpha * (ms - 10.5)
        js_b = j0_b + alpha * (ms - 10.5)

        fall_color = 'darkgreen'
        axs[col].errorbar(ms, js_d, ls='-', c=fall_color)
        axs[col].errorbar(ms, js_b, ls='-', c=fall_color)

        if col == 3:
            loc = 0.54
            axs[col].text(ms[int(loc * len(ms))], js_d[int(loc * len(ms))] + 0.25,
                          r'FR18 disc',
                          rotation=np.arctan(alpha) * 180 / np.pi, c=fall_color,)
                          # path_effects=white_alpha_path_effect_2)

            loc = 0.67
            axs[col].text(ms[int(loc * len(ms))], js_b[int(loc * len(ms))] - 0.25,
                          r'FR18 bulge',
                          rotation=np.arctan(alpha) * 180 / np.pi, c=fall_color,)
                          # path_effects=white_alpha_path_effect_2)

        # if col == 3:
        #     loc = 0.2
        #     axs[col].text(ms[int(loc * len(ms))], js_d[int(loc * len(ms))] + 0.6,
        #                   r'Disc galaxies ($\kappa_{\rm co} > 0.35$)',
        #                   rotation=np.arctan(0.4) * 180 / np.pi, path_effects=white_path_effect_3)
        #
        #     loc = 0.2
        #     axs[col].text(ms[int(loc * len(ms))], js_b[int(loc * len(ms))] - 0.1,
        #                   r'Elliptical galaxies ($\kappa_{\rm co} < 0.25$)',
        #                   rotation=np.arctan(0.4) * 180 / np.pi, path_effects=white_path_effect_3)

        # axs[col].text(8.90, 2.202, r'FR18 disc', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)
        # axs[col].text(10.3, 2.22, r'FR18 bulge', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)

    if len(axs) > 1:
        if split_kappa:
            axs[3].legend([(lines.Line2D([0, 1], [0, 1], ls='')),
                           (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Blues')(0.6),
                                         ls='-', lw=2, path_effects=path_effect_4)),
                           (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Oranges')(0.6),
                                         ls='--', lw=2, path_effects=path_effect_4)),
                           (lines.Line2D([0, 1], [0, 1], ls='')),
                           (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Blues')(0.25 * 0.8),
                                          ls='-', lw=2, path_effects=path_effect_4)),
                           (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Oranges')(0.25 * 0.8),
                                          ls='--', lw=2, path_effects=path_effect_4)),
                           ],
                          ['Disc', 'galaxies', r'$\kappa_{\rm co} > 0.35$',
                           'Elliptical', 'galaxies', r'$\kappa_{\rm co} < 0.25$'],
                          # handler_map={tuple: HandlerTupleVertical(ndivide=4)},
                          frameon=False,
                          # markerfirst=False,
                          labelspacing=0,
                          loc='lower right', bbox_to_anchor=(1.03,-0.03))
        else:
            axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C1',
                                         ls='--', lw=2, path_effects=path_effect_4)),
                            (lines.Line2D([0, 1], [0, 1], color='C0',
                                         ls='-', lw=2, path_effects=path_effect_4)),

                           ],
                          ['LoResDM', 'HiResDM'],
                          # handler_map={tuple: HandlerTupleVertical(ndivide=4)},
                          frameon=False,
                          # markerfirst=False,
                          # labelspacing=0,
                          loc='lower right', bbox_to_anchor=(1.03,-0.03))


    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    if len(axs) > 1:
        axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                    r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

        if split_kappa:
            Bbox_top = axs[1].get_position()
            Bbox_bot = axs[2].get_position()

            width = 0.1
            pad = 0.05
            cax = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3*Bbox_bot.y0)/4,
                            (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                            (Bbox_top.y1 - Bbox_bot.y0)/2])

            n_col_grad = 101
            x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
            x, _ = np.meshgrid(x_, np.array([0, 1]))
            cax.imshow(x.T, cmap = 'Blues', vmin = 0, vmax = 0.8,
                       aspect = 2 / width, origin='lower', extent=[0, 1, 0, 1])
            cax.axhline(0.25, 0,1,
                        c='k', ls=(0,(0.8,0.8)))
            cax.axhline(0.35, 0,1,
                        c='k', ls=(0,(0.8,0.8)))

            cax.set_xticks([])
            cax.set_xticklabels([])
            # cax.set_yticks([0.6, 0.7, 0.8, 0.9, 1])
            cax.set_yticklabels([])
            cax.yaxis.tick_left()

            cax2 = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3*Bbox_bot.y0)/4,
                             (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                             (Bbox_top.y1 - Bbox_bot.y0)/2])

            cax2.imshow(x.T, cmap = 'Oranges', vmin = 0, vmax = 0.8,
                        aspect = 2 / width, origin='lower', extent=[0, 1, 0, 1])
            cax2.axhline(0.25, 0,1,
                        c='k', ls=(0,(0.8,0.8)))
            cax2.axhline(0.35, 0,1,
                        c='k', ls=(0,(0.8,0.8)))

            cax2.set_xticks([])
            cax2.set_xticklabels([])

            cax2.yaxis.set_label_position('right')
            cax2.yaxis.tick_right()

            cax2.set_ylabel(r'$\kappa_{\rm co}$')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def more_am_scaling(name='', save=False,
                    stars=True, all_ages=True, gas=False, sfgas=True, CC_stars=False, DM=False,
                    all_z=False, jz=False, flat_scale=True, r12=False, sim_low=1, sim_hi=2):
    N_res = 10
    z_index = 0

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[z_index]]

    xlims = [1e-3 + 8, 12 - 1e-3]
    # xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [1e-3 + 0, 4 - 1e-3]
    if flat_scale:
        ylims = [1e-3 + 1.3, 3.7 - 1e-3]
        # ylims = [1e-3 + 1.7, 3.3 - 1e-3]
        if jz:
            ylims = [1e-3 + 1.3, 3.3 - 1e-3]
            # ylims = [1e-3 + 0.3, 4.3 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    N_run = np.linspace(7.5, 12, 10)
    # N_run += 2
    
    xlabel = r'$\log \, M_\star / $M$_\odot$'
    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, j_{\rm tot} / $kpc km s$^{-1}$'
    if jz:
        ylabel = r'$\log \, j_z / $kpc km s$^{-1}$'
    if flat_scale:
        ylabel = r'$\log \, j_{\rm tot} / M_\star^{2/3} $'+'\n'+r'[kpc km s$^{-1}$ (10$^{10}$M$_\odot)^{-2/3}$]'
        if jz:
            ylabel = r'$\log \, j_z / M_\star^{2/3} $' + '\n' + r'[kpc km s$^{-1}$ (10$^{10}$M$_\odot)^{-2/3}$]'

    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0 and sim_low != 1: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1 or not all_z: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        # axs[col].set_aspect(1)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if all_ages:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

    if jz:
        j_label = 'jz'
    else:
        j_label = 'jtot'

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(sim_low, sim_hi):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                color4 = 'chartreuse'
                cmap = cmap_0
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                color4 = 'fuchsia'
                cmap = cmap_1
                if sim_low != 0:
                    linestyle = '-'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'r200'

                # r_slice = keys.rkey_to_rcolum_dict[r_key, 0]:keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                if stars:
                    y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, rsn:rsx]
                    yq0 = raw_data0[keys.key1_dict[j_label]['Star']][:, rsn:rsx]
                    y0 = y0 / yq0
            
                if CC_stars and not r12:
                    cc_y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]
                    cc_yq0 = raw_data0[keys.key1_dict[j_label]['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]
                    cc_y0 = cc_y0 / cc_yq0
                    cc_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]

                if gas:
                    gas_y0 = raw_data0[keys.key0_dict[j_label]['Gas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    gas_yq0 = raw_data0[keys.key1_dict[j_label]['Gas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    gas_y0 = gas_y0 / gas_yq0

                if sfgas:
                    sfgas_y0 = raw_data0[keys.key0_dict[j_label]['SFGas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    sfgas_yq0 = raw_data0[keys.key1_dict[j_label]['SFGas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    sfgas_y0 = sfgas_y0 / sfgas_yq0

                if DM:
                    DM_y0 = raw_data0[keys.key0_dict[j_label]['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    DM_yq0 = raw_data0[keys.key1_dict[j_label]['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    DM_y0 = DM_y0 / DM_yq0

                kappa_co = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

                #mask and units
                log_M200 = np.log10(M200[mask0]) + 10 # log Mstar
                x = np.log10(x0[mask0]) + 10 # log Mstar
                # x = np.log10(M200[mask0]) + 10 # log Mstar

                if stars:
                    y = y0[mask0] # kpc km /s
                if CC_stars and not r12:
                    cc_y = cc_y0[mask0] # kpc km /s
                    cc_N = cc_N0[mask0]
                if gas:
                    gas_y = gas_y0[mask0] # kpc km /s
                if sfgas:
                    sfgas_y = sfgas_y0[mask0] # kpc km /s
                if DM:
                    DM_y = DM_y0[mask0] # kpc km /s
                if flat_scale:
                    if stars:
                        y = y0[mask0] / x0[mask0, np.newaxis]**(2/3)
                    if CC_stars and not r12:
                        cc_y = cc_y0[mask0] / x0[mask0]**(2/3)
                    if gas:
                        gas_y = gas_y0[mask0] / x0[mask0]**(2/3)
                    if sfgas:
                        sfgas_y = sfgas_y0[mask0] / x0[mask0]**(2/3)
                    if DM:
                        DM_y = DM_y0[mask0] / x0[mask0]**(2/3)

            if stars:
                found_1 = False
                found_2 = False
                for i in range(len(bin_centres)):
                    if not all_ages and found_1 and found_2:
                        continue

                    if np.any(~np.isnan(y[:, i])):
                        if not found_1:
                            print(np.sum(~np.isnan(y[:, i])))
                            found_1 = True
                            # axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
                            #               r'stellar ages$\in$' + str(list(keys.t_bin_edges[i:i + 2])), ha='right',
                            #               bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

                        else:
                            found_2 = True
                            # axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                            #               r'stellar ages$\in$' + str(list(keys.t_bin_edges[i:i + 2])), ha='right',
                            #               bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

                    else:
                        continue

                    mask = y[:, i] != 0

                    # calc medians etc.
                    ymedians, xedges, bin_is = binned_statistic(x[mask], y[mask, i],
                                                                statistic=np.nanmedian, bins=N_run)
                    yplus, xedges, _ = binned_statistic(x[mask], y[mask, i],
                                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                    yminus, xedges, _ = binned_statistic(x[mask], y[mask, i],
                                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                    xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                    counts, _ = np.histogram(x, bins=N_run)
                    mask = counts > 20 * np.diff(N_run)[0]
                    
                    # plot
                    axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=1, zorder=10-i, alpha=1, path_effects=path_effect_2)

                    # m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
                    # mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)
                    # 
                    # xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                    # ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')
                    # axs[col].errorbar(xlin, np.log10(ymed_func(xlin)),
                    #                   # c=color,
                    #                   # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                    #                   c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                    #                   ls=linestyle, linewidth=2, zorder=10-i, alpha=1, path_effects=path_effect_4)

            if CC_stars and not r12:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[cc_y != 0], cc_y[cc_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[cc_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if gas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[gas_y != 0], gas_y[gas_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])
                
                counts, _ = np.histogram(x[gas_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color3,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

                # #set nans to 0
                # gas_y[np.isnan(gas_y)] = -100
                # ymedians, xedges, bin_is = binned_statistic(x, gas_y,
                #                                             statistic=np.nanmedian, bins=N_run)
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]
                # axs[col].errorbar(xcentres[mask], ymedians[mask],
                #                   c=color3,
                #                   ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)
            if sfgas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[sfgas_y != 0], sfgas_y[sfgas_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])
                counts, _ = np.histogram(x[sfgas_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]
                mask2 = counts > 20 * np.diff(N_run)[0]

                # plot
                if not stars:
                    axs[col].errorbar(xcentres[mask2], np.log10(ymedians[mask2]),
                                      c=color, #4,
                                      ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

                    max_xbin_centre = xcentres[:-1][np.logical_not(mask2)[1:]][0]
                    axs[col].scatter(x[x > max_xbin_centre], np.log10(sfgas_y[x > max_xbin_centre]),
                                     color=color, marker='o', s=3, alpha=1,
                                     linewidths=0, zorder=3)#, rasterized=True)
                    axs[col].scatter(x, np.log10(sfgas_y),
                                     color=color, marker=marker, s=2, alpha=0.5,
                                     linewidths=0, zorder=2, rasterized=True)

                    nd_x0 = x[sfgas_y != 0][sfgas_y[sfgas_y != 0] > 10**ylims[1]]
                    axs[col].scatter(nd_x0, np.ones(len(nd_x0)) * (ylims[0] + 0.97 * (ylims[1] - ylims[0])),
                                     color=color, marker=r'$\uparrow$', s=6**2, alpha=0.5,
                                     # linewidths=0,
                                     zorder=1, rasterized=True)

                    nd_x0 = x[sfgas_y != 0][sfgas_y[sfgas_y != 0] < 10**ylims[0]]
                    axs[col].scatter(nd_x0, np.ones(len(nd_x0)) * (ylims[0] + 0.03 * (ylims[1] - ylims[0])),
                                     color=color, marker=r'$\downarrow$', s=6**2, alpha=0.5,
                                     # linewidths=0,
                                     zorder=1, rasterized=True)

                else:
                    axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                      c='k', #color,#4,
                                      ls='--', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)


            if DM:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[DM_y != 0], DM_y[DM_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])
                
                counts, _ = np.histogram(x[DM_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]
                
                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        #decorations
        # for i in range(20):
        #     #1 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i * 1/2 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 2/3,
        #                               i * 1/2 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        #convert halo to stellar mass
        # mh = np.linspace(*xlims)
        # ms = mgm.mass_function(mgm.mass_function_args(z), mh)

        # Sugessed by Mike
        ms = np.linspace(*xlims)
        alpha = 0.67
        j0_d = 3.17
        j0_b = 2.25
        js_d = j0_d + alpha * (ms - 10.5)
        js_b = j0_b + alpha * (ms - 10.5)

        if flat_scale:
            js_d -= 2/3 * (ms - 10)
            js_b -= 2/3 * (ms - 10)

        fall_color = 'darkgreen'
        axs[col].errorbar(ms, js_d, ls='-', c=fall_color)
        axs[col].errorbar(ms, js_b, ls='-', c=fall_color)
        # axs[col].errorbar(mh, js_d, ls='-', c=fall_color)
        # axs[col].errorbar(mh, js_b, ls='-', c=fall_color)

        if col == 1:
            loc = 0.54
            axs[col].text(ms[int(loc * len(ms))], js_d[int(loc * len(ms))] + 0.25,
                          r'FR18 disc',
                          rotation=np.arctan(alpha) * 180 / np.pi, c=fall_color,)
                          # path_effects=white_alpha_path_effect_2)

            loc = 0.67
            axs[col].text(ms[int(loc * len(ms))], js_b[int(loc * len(ms))] - 0.25,
                          r'FR18 bulge',
                          rotation=np.arctan(alpha) * 180 / np.pi, c=fall_color,)
                          # path_effects=white_alpha_path_effect_2)

        # if col == 3:
        #     loc = 0.2
        #     axs[col].text(ms[int(loc * len(ms))], js_d[int(loc * len(ms))] + 0.6,
        #                   r'Disc galaxies ($\kappa_{\rm co} > 0.35$)',
        #                   rotation=np.arctan(0.4) * 180 / np.pi, path_effects=white_path_effect_3)
        #
        #     loc = 0.2
        #     axs[col].text(ms[int(loc * len(ms))], js_b[int(loc * len(ms))] - 0.1,
        #                   r'Elliptical galaxies ($\kappa_{\rm co} < 0.25$)',
        #                   rotation=np.arctan(0.4) * 180 / np.pi, path_effects=white_path_effect_3)

        # axs[col].text(8.90, 2.202, r'FR18 disc', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)
        # axs[col].text(10.3, 2.22, r'FR18 bulge', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)

    # axs[3].legend([(lines.Line2D([0, 1], [0, 1], ls='')),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Blues')(0.6),
    #                              ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Oranges')(0.6),
    #                              ls='--', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], ls='')),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Blues')(0.25 * 0.8),
    #                               ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Oranges')(0.25 * 0.8),
    #                               ls='--', lw=2, path_effects=path_effect_4)),
    #                ],
    #               ['Disc', 'galaxies', r'$\kappa_{\rm co} > 0.35$',
    #                'Elliptical', 'galaxies', r'$\kappa_{\rm co} < 0.25$'],
    #               # handler_map={tuple: HandlerTupleVertical(ndivide=4)},
    #               frameon=False,
    #               # markerfirst=False,
    #               labelspacing=0,
    #               loc='lower right', bbox_to_anchor=(1.03,-0.03))

    if all_z:
        axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    else:
        if z_index == 0:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 1:
            axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 2:
            axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 3:
            axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    # for col in range(len(cnp)):
    #     if r12:
    #         radii_label = r'$r=r_{1/2, \star}$'
    #     else:
    #         radii_label = r'$r<r_{200}$'
    #     axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
    #                   radii_label, ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        
    for col in range(len(cnp)):
        label = ''
        if sim_low == 1:
            label = r'LR'
        if sim_hi == 1:
            label = r'HR'
        if not stars and sfgas:
            # label = 'star forming gas'
            label = 'SF gas'
        if label != '':
            axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
                          label, ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if stars and sim_hi == 2:
        axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=1, path_effects=path_effect_2)),
                       (lines.Line2D([0, 1], [0, 1], color='k', ls='--', lw=2, path_effects=path_effect_4)),
                       ],
                      ['Stars', 'SF gas'],
                      loc='upper right')

    if stars and sim_hi == 2:
        if all_z:
            Bbox_top = axs[1].get_position()
            Bbox_bot = axs[2].get_position()
        else:
            Bbox_top = axs[0].get_position()
            Bbox_bot = axs[0].get_position()

        width = 0.1
        pad = 0.05
        if all_z:
            cax = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3*Bbox_bot.y0)/4,
                            (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                            (Bbox_top.y1 - Bbox_bot.y0)/2])
        else:
            cax = plt.axes([Bbox_top.x1, Bbox_bot.y0,
                            (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                            Bbox_top.y1 - Bbox_bot.y0])

        n_col_grad = 1384  # 101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
        x_ = np.digitize(x_, (keys.t_bin_edges - keys.t_bin_edges[0]) /
                         (UNIVERSE_AGE - keys.t_bin_edges[0])) / (len(keys.t_bin_edges) - 1)

        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x.T, cmap = cmap_0,
                   # vmin = 0, vmax = 1.25,
                   vmin = 0, vmax = 1,
                   aspect = 2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        cax.set_xticks([])
        cax.set_xticklabels([])
        # cax.set_yticklabels([])
        cax.yaxis.tick_left()

        if all_z:
            cax2 = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3*Bbox_bot.y0)/4,
                             (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                             (Bbox_top.y1 - Bbox_bot.y0)/2])
        else:
            # cax2 = plt.axes([Bbox_top.x1, Bbox_bot.y0,
            #                 (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
            #                 Bbox_top.y1 - Bbox_bot.y0])
            cax2 = cax

        cax2.imshow(x.T, cmap = cmap_1,
                    # vmin = 0, vmax = 1.25,
                    vmin = 0, vmax = 1,
                    aspect = 2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        if all_z:
            for age in mgm.lbt(1 / (1 + np.array([0, 0.5, 1, 2]))):
                for _cax in [cax, cax2]:
                    _cax.axhline(age, 0,1,
                                c='k', ls=(0,(0.8,0.8)))
                    _cax.axhline(age, 0,1,
                                c='k', ls=(0,(0.8,0.8)))

        cax2.set_xticks([])
        cax2.set_xticklabels([])

        cax2.yaxis.set_label_position('right')
        cax2.yaxis.tick_right()

        cax2.set_ylabel('Stellar age [Gyr]')

    if save:
        if sim_low != 0 or sim_hi != 2:
            plt.savefig(name, bbox_inches='tight', pad_inches=0, dpi=300)
        else:
            plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def more_size_scaling(name='', save=False,
                      stars=True, all_ages=True, gas=False, sfgas=True, CC_stars=False, DM=False,
                      all_z=False, flat_scale=True, r12=False, sim_low=1, sim_hi=2,
                      swap=False, tcx=False, r200y=False, mass_m200=False):
    N_res = 10
    z_index = 0

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        # if not flat_scale:
        #     fig.set_size_inches(3.3 * 1.52, 3.3 * 1.52 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[z_index]]

    xlims = [1e-3 + 8, 12 - 1e-3]
    if mass_m200: xlims = [1e-3 + 10, 14 - 1e-3]

    if swap:
        xlims = [0, UNIVERSE_AGE]
        if tcx: xlims = [-5, -1]

    # ylims = [1e-3 + -0.16, 1.34 - 1e-3]
    ylims = [-0.4, 1.6]
    if flat_scale: 
        ylims = [1e-3 + 0, 1.5 - 1e-3] #[1e-3 -0.5, 2 - 1e-3]
        if mass_m200:
            ylims = [-0.9, 1.1]
            # ylims = [-0.7, 0.8]
            # ylims = [-0.38, 0.62]
            
    if r200y:
        ylims = [-2.55, -0.55]
        # ylims = [-2.35, -0.85]
        # ylims = [-2.04, -1.04]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    # N_run = np.linspace(7.5, 12, 10)
    # N_run = np.linspace(7.5, 12.5, 11)
    if swap:
        # N_run = np.linspace(8, 12.5, 10)
        N_run = np.linspace(8, 11.5, 8)
        # N_run = np.linspace(6, 11.5, 12)
    else:
        N_run = np.linspace(7.5, 12, 10)

    if mass_m200:
        N_run += 2

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    if mass_m200: xlabel = r'$\log \, M_{200} / $M$_\odot$'

    ylabel = r'$\log \, r_{1/2} / $kpc'
    if flat_scale:
        ylabel = r'$\log \, r_{1/2} / M_\star^{1/3} $' + '\n' + r'[kpc (10$^{10}$M$_\odot)^{-1/3}$]'
        if mass_m200: ylabel = r'$\log \, r_{1/2} / M_{200}^{1/3} $' + '\n' + r'[kpc (10$^{10}$M$_\odot)^{-1/3}$]'
    if r200y: ylabel = r'$\log \, r_{1/2} / r_{200}$'
    if swap:
        xlabel = r'Stellar age [Gyr]'
        if tcx: xlabel = r'$\log$ Stellar age $/ t_{c,1/2}$'

    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0 and sim_low != 1:
            axs[col].set_ylabel(ylabel)
        else:
            axs[col].set_yticklabels([])

        if (col // 2) == 1 or not all_z:
            axs[col].set_xlabel(xlabel)
        else:
            axs[col].set_xticklabels([])

        # axs[col].set_aspect(np.diff(xlims) / np.diff(ylims) * 1.5/1.33)
        axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))
        # if mass_m200:
        #     axs[col].set_aspect(np.diff(xlims) / np.diff(ylims) / 2)

        # axs[col].errorbar([8, 12], [0, 4/3], c='grey', ls=':')

    if swap and sim_hi == 2 and not tcx:
        axs[0].set_xticks([5,10])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if all_ages:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

        if swap:
            cmap_0 = 'magma_r'
            cmap_1 = 'magma_r'

    j_label = 'r12r'

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:  # '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(sim_low, sim_hi):
            if pair == 0:  # 0 (7x)
                mdm = DM_MASS / 7
                
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                color4 = 'chartreuse'
                cmap = cmap_0
            else:  # 1 (1x)
                mdm = DM_MASS 
                
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                color4 = 'fuchsia'
                cmap = cmap_1
                if sim_low != 0:
                    linestyle = '-'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'r200'

                # r_slice = keys.rkey_to_rcolum_dict[r_key, 0]:keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]

                # load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                r200 = raw_data0[keys.key0_dict['r200'][None]][:]

                if tcx:
                    V200 = np.sqrt(GRAV_CONST * M200 / r200)
                    mdm_12 = raw_data0[keys.key0_dict['mdm']['DM']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
                    r12_star = raw_data0[keys.key0_dict['r12']['Star']][:]

                if stars:
                    y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, rsn:rsx]

                if CC_stars and not r12:
                    cc_y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]
                    cc_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]

                if gas:
                    gas_y0 = raw_data0[keys.key0_dict[j_label]['Gas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                if sfgas:
                    sfgas_y0 = raw_data0[keys.key0_dict[j_label]['SFGas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                if DM:
                    DM_y0 = raw_data0[keys.key0_dict[j_label]['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                kappa_co = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

                # mask and units
                log_M200 = np.log10(M200[mask0]) + 10  # log Mstar
                if mass_m200:
                    x0 = M200
                x = np.log10(x0[mask0]) + 10  # log Mstar
                r200 = r200[mask0] #kpc

                if tcx:
                    rho_dm_12 =  mdm_12 / (4 / 3 * np.pi * ((r12_star * 10**0.1)**3 - (r12_star * 10**-0.1)**3)) #10^10 M_sun / kpc^3
                    tc = V200**3 / (GRAV_CONST**2 * rho_dm_12 * mdm) * GYR_ON_S / PC_ON_M #Gyr
                    tc = tc[mask0]
                if stars:
                    y = y0[mask0]  # kpc km /s
                if CC_stars and not r12:
                    cc_y = cc_y0[mask0]  # kpc km /s
                    cc_N = cc_N0[mask0]
                if gas:
                    gas_y = gas_y0[mask0]  # kpc km /s
                if sfgas:
                    sfgas_y = sfgas_y0[mask0]  # kpc km /s
                if DM:
                    DM_y = DM_y0[mask0]  # kpc km /s
                if flat_scale:
                    if stars:
                        y = y0[mask0] / x0[mask0, np.newaxis] ** (1 / 3)
                    if CC_stars and not r12:
                        cc_y = cc_y0[mask0] / x0[mask0] ** (1 / 3)
                    if gas:
                        gas_y = gas_y0[mask0] / x0[mask0] ** (1 / 3)
                    if sfgas:
                        sfgas_y = sfgas_y0[mask0] / x0[mask0] ** (1 / 3)
                    if DM:
                        DM_y = DM_y0[mask0] / x0[mask0] ** (1 / 3)
                if r200y:
                    if stars:
                        y = y0[mask0] / r200[:, np.newaxis]
                    if CC_stars and not r12:
                        cc_y = cc_y0[mask0] / r200
                    if gas:
                        gas_y = gas_y0[mask0] / r200
                    if sfgas:
                        sfgas_y = sfgas_y0[mask0] / r200
                    if DM:
                        DM_y = DM_y0[mask0] / r200

            if stars:

                if not swap:
                    found_1 = False
                    found_2 = False
                    for i in range(len(bin_centres)):
                        if not all_ages and found_1 and found_2:
                            continue

                        if np.any(~np.isnan(y[:, i])):
                            if not found_1:
                                print(np.sum(~np.isnan(y[:, i])))
                                found_1 = True
                                # axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
                                #               r'stellar ages$\in$' + str(list(keys.t_bin_edges[i:i + 2])), ha='right',
                                #               bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

                            else:
                                found_2 = True
                                # axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                                #               r'stellar ages$\in$' + str(list(keys.t_bin_edges[i:i + 2])), ha='right',
                                #               bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

                        else:
                            continue

                        mask = y[:, i] != 0

                        # calc medians etc.
                        ymedians, xedges, bin_is = binned_statistic(x[mask], y[mask, i],
                                                                    statistic=np.nanmedian, bins=N_run)
                        yplus, xedges, _ = binned_statistic(x[mask], y[mask, i],
                                                            statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                        yminus, xedges, _ = binned_statistic(x[mask], y[mask, i],
                                                             statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                        xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                        counts, _ = np.histogram(x, bins=N_run)
                        mask_line = counts > 20 * np.diff(N_run)[0]
                        max_xbin_centre = xcentres[:-1][np.logical_not(mask_line)[1:]][0]

                        # plot
                        axs[col].errorbar(xcentres[mask_line], np.log10(ymedians[mask_line]),
                                          # c=color,
                                          # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                          c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                          ls=linestyle, linewidth=1, zorder=10 - i, alpha=1, path_effects=path_effect_2)

                        #scatter
                        # if i == 0 or i == 6:
                        if False:
                            axs[col].scatter(x[mask][x[mask] > max_xbin_centre], np.log10(y[mask, i][x[mask] > max_xbin_centre]),
                                             color=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                             marker='o', s=3, alpha=1, zorder=0 - i)  # , rasterized=True)
                            axs[col].scatter(x[mask], np.log10(y[mask, i]),
                                             color=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                             marker='o', s=2, alpha=0.5, linewidths=0, zorder=-10 - i, rasterized=True)

                        # m200_intercept = m200_heating_intercept(z, 1-pair) #log Msun
                        # mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)
                        #
                        # xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                        # ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')
                        # axs[col].errorbar(xlin, np.log10(ymed_func(xlin)),
                        #                   # c=color,
                        #                   # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                        #                   c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                        #                   ls=linestyle, linewidth=2, zorder=10-i, alpha=1, path_effects=path_effect_4)

                else:
                    all_sizes = np.nan * np.zeros((len(N_run)-1, len(bin_centres)))

                    for i in range(len(bin_centres)):
                        try:
                            mask = y[:, i] != 0
    
                            # calc medians etc.
                            ymedians, xedges, bin_is = binned_statistic(x[mask], y[mask, i],
                                                                        statistic=np.nanmedian, bins=N_run)
                            xcentres = 0.5 * (xedges[1:] + xedges[:-1])
    
                            counts, _ = np.histogram(x, bins=N_run)
                            mask_line = counts > 20 * np.diff(N_run)[0]
    
                            all_sizes[mask_line, i] = ymedians[mask_line]
                        except ValueError:
                            pass
                    
                    for j in range(len(N_run)-1):

                        _x = bin_centres
                        if tcx:
                            med_tc = np.median(tc[np.logical_and(N_run[j]<x, x<N_run[j+1])])
                            
                            # x = np.log10(bin_centres / med_tc)
                            _x = np.log10(bin_centres[:-1] / med_tc)
                            print('Removed oldest age bin')

                        _y = np.log10(all_sizes[j, :])

                        if tcx:
                            # y = np.log10(all_sizes[j])
                            _y = np.log10(all_sizes[j, :-1])

                        # plot
                        axs[col].errorbar(_x, _y,
                                          c=matplotlib.cm.get_cmap(cmap)(((N_run[j] + N_run[j+1])/2 - N_run[0]) / (N_run[-1] - N_run[0])),
                                          ls=linestyle, linewidth=1, zorder=10 - j, alpha=1, path_effects=path_effect_2)

                        if False:
                            #each galaxy
                            m_mask = np.logical_and(N_run[j]<x, x<N_run[j+1])
                            if tcx:
                                mask_tc = tc[m_mask]
                                _x = np.log10(bin_centres[np.newaxis, :] / mask_tc[:, np.newaxis])
                            else:
                                _x = bin_centres[np.newaxis, :] * np.ones(np.sum(m_mask))[:, np.newaxis]

                            axs[col].errorbar(_x, np.log10(y[m_mask]),
                                              c=matplotlib.cm.get_cmap(cmap)(((N_run[j] + N_run[j+1])/2 - N_run[0]) / (N_run[-1] - N_run[0])),
                                              ls='-', linewidth=0.5, zorder=-10, alpha=0.5, rasterized=True)

            if CC_stars and not r12:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[cc_y != 0], cc_y[cc_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[cc_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if gas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[gas_y != 0], gas_y[gas_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[gas_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color3,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

                # #set nans to 0
                # gas_y[np.isnan(gas_y)] = -100
                # ymedians, xedges, bin_is = binned_statistic(x, gas_y,
                #                                             statistic=np.nanmedian, bins=N_run)
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]
                # axs[col].errorbar(xcentres[mask], ymedians[mask],
                #                   c=color3,
                #                   ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)
            if sfgas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[sfgas_y != 0], sfgas_y[sfgas_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])
                counts, _ = np.histogram(x[sfgas_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]
                mask2 = counts > 20 * np.diff(N_run)[0]

                # plot
                if not stars:
                    axs[col].errorbar(xcentres[mask2], np.log10(ymedians[mask2]),
                                      c=color,  # 4,
                                      ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

                    max_xbin_centre = xcentres[:-1][np.logical_not(mask2)[1:]][0]
                    axs[col].scatter(x[x > max_xbin_centre], np.log10(sfgas_y[x > max_xbin_centre]),
                                     color=color, marker='o', s=3, alpha=1,
                                     linewidths=0, zorder=3)  # , rasterized=True)
                    axs[col].scatter(x, np.log10(sfgas_y),
                                     color=color, marker=marker, s=2, alpha=0.5,
                                     linewidths=0, zorder=2, rasterized=True)

                    nd_x0 = x[sfgas_y != 0][sfgas_y[sfgas_y != 0] > 10 ** ylims[1]]
                    axs[col].scatter(nd_x0, np.ones(len(nd_x0)) * (ylims[0] + 0.97 * (ylims[1] - ylims[0])),
                                     color=color, marker=r'$\uparrow$', s=6 ** 2, alpha=0.5,
                                     # linewidths=0,
                                     zorder=1, rasterized=True)

                    nd_x0 = x[sfgas_y != 0][sfgas_y[sfgas_y != 0] < 10 ** ylims[0]]
                    axs[col].scatter(nd_x0, np.ones(len(nd_x0)) * (ylims[0] + 0.03 * (ylims[1] - ylims[0])),
                                     color=color, marker=r'$\downarrow$', s=6 ** 2, alpha=0.5,
                                     # linewidths=0,
                                     zorder=1, rasterized=True)

                else:
                    axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                      c='k',  # color,#4,
                                      ls='--', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            if DM:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[DM_y != 0], DM_y[DM_y != 0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[DM_y != 0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        # decorations
        # for i in range(20):
        #     #1 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i * 1/2 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 2/3,
        #                               i * 1/2 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        # convert halo to stellar mass
        # mh = np.linspace(*xlims)
        # ms = mgm.mass_function(mgm.mass_function_args(z), mh)

        if col == 1:
            loc = 0.54
            axs[col].text(ms[int(loc * len(ms))], js_d[int(loc * len(ms))] + 0.25,
                          r'FR18 disc',
                          rotation=np.arctan(alpha) * 180 / np.pi, c=fall_color, )
            # path_effects=white_alpha_path_effect_2)

            loc = 0.67
            axs[col].text(ms[int(loc * len(ms))], js_b[int(loc * len(ms))] - 0.25,
                          r'FR18 bulge',
                          rotation=np.arctan(alpha) * 180 / np.pi, c=fall_color, )
            # path_effects=white_alpha_path_effect_2)

        # if col == 3:
        #     loc = 0.2
        #     axs[col].text(ms[int(loc * len(ms))], js_d[int(loc * len(ms))] + 0.6,
        #                   r'Disc galaxies ($\kappa_{\rm co} > 0.35$)',
        #                   rotation=np.arctan(0.4) * 180 / np.pi, path_effects=white_path_effect_3)
        #
        #     loc = 0.2
        #     axs[col].text(ms[int(loc * len(ms))], js_b[int(loc * len(ms))] - 0.1,
        #                   r'Elliptical galaxies ($\kappa_{\rm co} < 0.25$)',
        #                   rotation=np.arctan(0.4) * 180 / np.pi, path_effects=white_path_effect_3)

        # axs[col].text(8.90, 2.202, r'FR18 disc', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)
        # axs[col].text(10.3, 2.22, r'FR18 bulge', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)

    # axs[3].legend([(lines.Line2D([0, 1], [0, 1], ls='')),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Blues')(0.6),
    #                              ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Oranges')(0.6),
    #                              ls='--', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], ls='')),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Blues')(0.25 * 0.8),
    #                               ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('Oranges')(0.25 * 0.8),
    #                               ls='--', lw=2, path_effects=path_effect_4)),
    #                ],
    #               ['Disc', 'galaxies', r'$\kappa_{\rm co} > 0.35$',
    #                'Elliptical', 'galaxies', r'$\kappa_{\rm co} < 0.25$'],
    #               # handler_map={tuple: HandlerTupleVertical(ndivide=4)},
    #               frameon=False,
    #               # markerfirst=False,
    #               labelspacing=0,
    #               loc='lower right', bbox_to_anchor=(1.03,-0.03))

    if all_z:
        axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    else:
        if z_index == 0:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 1:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 2:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 3:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    # for col in range(len(cnp)):
    #     if r12:
    #         radii_label = r'$r=r_{1/2, \star}$'
    #     else:
    #         radii_label = r'$r<r_{200}$'
    #     axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
    #                   radii_label, ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    for col in range(len(cnp)):
        label = ''
        if sim_low == 1:
            label = '$\mu = 5.36$' #LR
        if sim_hi == 1:
            label = '$\mu = 0.77$' #HR
        if not stars and sfgas:
            # label = 'star forming gas'
            label = 'SF gas'
        if label != '':
            axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
                          label, ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=10)

        if label in ('$\mu = 5.36$', '$\mu = 0.77$'):
            if sim_low == 1:
                label = 'LR'
            if sim_hi == 1:
                label = 'HR'

            axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.25 * (ylims[1] - ylims[0]),
                          label, ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if stars and sim_hi == 2 and sfgas:
        axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=1, path_effects=path_effect_2)),
                       (lines.Line2D([0, 1], [0, 1], color='k', ls='--', lw=2, path_effects=path_effect_4)),
                       ],
                      ['Stars', 'SF gas'],
                      loc='upper left')

    if stars and sim_hi == 2:
        if all_z:
            Bbox_top = axs[1].get_position()
            Bbox_bot = axs[2].get_position()
        else:
            Bbox_top = axs[0].get_position()
            Bbox_bot = axs[0].get_position()

        width = 0.1
        pad = 0.05
        if all_z:
            cax = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                            (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                            (Bbox_top.y1 - Bbox_bot.y0) / 2])
        else:
            cax = plt.axes([Bbox_top.x1, Bbox_bot.y0,
                            (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                            Bbox_top.y1 - Bbox_bot.y0])

        n_col_grad = 1384 #101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
    
        if swap:
            x_ = np.digitize(x_, (N_run - N_run[0]) / (N_run[-1] - N_run[0])) / (len(N_run) - 1)
        
        else:
            x_ = np.digitize(x_, (keys.t_bin_edges - keys.t_bin_edges[0]) /
                             (UNIVERSE_AGE - keys.t_bin_edges[0])) / (len(keys.t_bin_edges) - 1)
        
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x.T, cmap=cmap_0,
                   # vmin = 0, vmax = 1.25,
                   vmin=0, vmax=1,
                   aspect=2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        cax.set_xticks([])
        cax.set_xticklabels([])
        # cax.set_yticklabels([])
        cax.yaxis.tick_left()

        if swap:
            ticks = [8,9,10,11,12]
            if mass_m200: ticks = [10,11,12,13,14]
            cax.yaxis.set_ticks((np.array(ticks) - N_run[0]) / (N_run[-1] - N_run[0]) * UNIVERSE_AGE)
            cax.yaxis.set_ticklabels(ticks)

        if all_z:
            cax2 = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                             (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                             (Bbox_top.y1 - Bbox_bot.y0) / 2])
        else:
            # cax2 = plt.axes([Bbox_top.x1, Bbox_bot.y0,
            #                 (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
            #                 Bbox_top.y1 - Bbox_bot.y0])
            cax2 = cax

        cax2.imshow(x.T, cmap=cmap_1,
                    # vmin = 0, vmax = 1.25,
                    vmin=0, vmax=1,
                    aspect=2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        if all_z:
            for age in mgm.lbt(1 / (1 + np.array([0, 0.5, 1, 2]))):
                for _cax in [cax, cax2]:
                    _cax.axhline(age, 0, 1,
                                 c='k', ls=(0, (0.8, 0.8)))
                    _cax.axhline(age, 0, 1,
                                 c='k', ls=(0, (0.8, 0.8)))

        cax2.set_xticks([])
        cax2.set_xticklabels([])

        cax2.yaxis.set_label_position('right')
        cax2.yaxis.tick_right()

        cax2.set_ylabel('Stellar age [Gyr]')
        if swap:
            cax2.set_ylabel(r'$\log \, M_\star / {\rm M}_\odot$')
            if mass_m200: cax2.set_ylabel(r'$\log \, M_{200} / {\rm M}_\odot$')

    # axs[0].set_xlim(xlims)

    if not r200y and not flat_scale:
        # decorations
        frac_size = 8
        axs[0].arrow(xlims[1] - 3* (xlims[1] - xlims[0]) / frac_size, np.log10(EAGLE_EPS),
                     -(xlims[1] - xlims[0]) / frac_size, 0,
                     length_includes_head=True,
                     head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                     head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                     color='k', fc='white', lw=1, zorder=-15, alpha=1)
        axs[0].arrow(xlims[1] - 3* (xlims[1] - xlims[0]) / frac_size, np.log10(2.8 * EAGLE_EPS),
                     -(xlims[1] - xlims[0]) / frac_size, 0,
                     length_includes_head=True,
                     head_width=(ylims[1] - ylims[0]) / frac_size / 3,
                     head_length=(xlims[1] - xlims[0]) / frac_size / 3,
                     color='k', fc='white', lw=1, zorder=-15, alpha=1)

        # axs[0].text(xlims[1] - 1.55 * (xlims[1] - xlims[0]) / frac_size,
        axs[0].text(xlims[1] - 3.55 * (xlims[1] - xlims[0]) / frac_size,
                      np.log10(EAGLE_EPS) + 0.000 * (ylims[1] - ylims[0]),
                      r'$\epsilon$', va='bottom')
        axs[0].text(xlims[1] - 3.55 * (xlims[1] - xlims[0]) / frac_size,
                      np.log10(2.8 * EAGLE_EPS) + 0.005 * (ylims[1] - ylims[0]),
                      r'$2.8 \times \epsilon$', va='bottom')

    if save:
        if sim_low != 0 or sim_hi != 2:
            plt.savefig(name, bbox_inches='tight', pad_inches=0, dpi=300)
        else:
            plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def more_kappa(name='', save=False, all_z=False, r12=False, stars=False, all_ages=True,
               gas=False, sfgas=True, temp=False, CC_stars=False, DM=False):
    N_res = 1
    z_index = 0

    fig = plt.figure(constrained_layout=True)
    if all_z:
        fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
        spec = fig.add_gridspec(ncols=2, nrows=2)
        cnp = combined_name_pairs
    else:
        # fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
        fig.set_size_inches(2.5, 2.5 + 0.03, forward=True)
        spec = fig.add_gridspec(ncols=1, nrows=1)
        cnp = [combined_name_pairs[z_index]]

    # xlims = [1e-3 + 8, 12 - 1e-3]
    xlims = [8, 12]
    # xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [0,  1]

    # ylims = [1e-3 + 0, 5 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    # N_run = np.linspace(7.5, 12, 10)
    # N_run += 2

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\kappa_{\rm co, SF}$'
    # ylabel = r'$\log \, e_{k, \phi, T} /$ km$^2$ s$^{-2}$'
    # ylabel = r'$\log \, |e_{{\rm bind}, T}| /$ km$^2$ s$^{-2}$'

    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    axs = []
    for col in range(len(cnp)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0:
            axs[col].set_ylabel(ylabel)
        else:
            axs[col].set_yticklabels([])

        if (col // 2) == 1 or not all_z:
            axs[col].set_xlabel(xlabel)
            axs[col].set_xticks([8,9,10,11,12])
        else:
            axs[col].set_xticklabels([])
        # axs[col].set_aspect(1)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if all_ages:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

    for col, name_pairs in enumerate(cnp):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:  # '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0:  # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                color4 = 'chartreuse'
                cmap = cmap_0
            else:  # 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                color4 = 'fuchsia'
                cmap = cmap_1

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'lt_5r12' #'lt_5r12' #'r200'

                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                # x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                if stars:
                    rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                    rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                    y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, rsn:rsx]

                    # Ek0 = raw_data0['GalaxyProfiles/Star/KineticEnergy'][:, rsn:rsx]  # 10^10 M_sun km^2 / s^2
                    # M0 = raw_data0['GalaxyProfiles/Star/BinMass'][:, rsn:rsx]  # 10^10 M_sun
                    # y0 = np.log10(y0 * Ek0 / M0)

                if CC_stars and not r12:
                    cc_y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]
                    cc_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]

                if gas:
                    gas_y0 = raw_data0[keys.key0_dict['kappa_co']['Gas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                    gas_Phi = raw_data0['GalaxyProfiles/Gas/PotentialEnergy'][:,
                                          keys.rkey_to_rcolum_dict[global_r_key, None]]

                    gas_Ek = raw_data0['GalaxyProfiles/Gas/KineticEnergy'][:,
                                         keys.rkey_to_rcolum_dict[global_r_key, None]] #10^10 M_sun km^2 / s^2
                    gas_M = raw_data0['GalaxyProfiles/Gas/BinMass'][:,
                                        keys.rkey_to_rcolum_dict[global_r_key, None]] #10^10 M_sun

                    gas_MT = raw_data0['GalaxyProfiles/Gas/MassTemperature'][:,
                                         keys.rkey_to_rcolum_dict[global_r_key, None]] #10^10 M_sun k
                    # sfgas_kappa_T0 = sfgas_M * 1e4 * BOLTZMANN_K / sfgas_Ek
                    gas_kappa_T0 = gas_MT * BOLTZMANN_K / gas_Ek

                if sfgas:
                    sfgas_y0 = raw_data0[keys.key0_dict['kappa_co']['SFGas']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]

                    sfgas_N0 = raw_data0['GalaxyProfiles/SFGas/BinN'][:,
                                          keys.rkey_to_rcolum_dict[global_r_key, None]]

                    # sfgas_Phi = raw_data0['GalaxyProfiles/SFGas/PotentialEnergy'][:,
                    #                       keys.rkey_to_rcolum_dict[global_r_key, None]]
                    sfgas_Ek = raw_data0['GalaxyProfiles/SFGas/KineticEnergy'][:,
                                         keys.rkey_to_rcolum_dict[global_r_key, None]] #10^10 M_sun km^2 / s^2
                    sfgas_M = raw_data0['GalaxyProfiles/SFGas/BinMass'][:,
                                        keys.rkey_to_rcolum_dict[global_r_key, None]] #10^10 M_sun

                    sfgas_MT = raw_data0['GalaxyProfiles/SFGas/MassTemperature'][:,
                                         keys.rkey_to_rcolum_dict[global_r_key, None]] #10^10 M_sun k
                    # sfgas_kappa_T0 = 1e4 * sfgas_M * BOLTZMANN_K / PROTON_MASS  / sfgas_Ek
                    sfgas_kappa_T0 = 3/2 * sfgas_MT * BOLTZMANN_K / PROTON_MASS / sfgas_Ek

                    # sfgas_tot_e0 = np.abs(sfgas_Ek + sfgas_Phi) / sfgas_Ek
                    # sfgas_tot_e0 = 1e4 * sfgas_M * BOLTZMANN_K / PROTON_MASS  / sfgas_Ek
                    sfgas_tot_e0 = np.nan * np.zeros(len(sfgas_y0))

                    # sfgas_y0 = np.log10(sfgas_y0 * sfgas_Ek / sfgas_M)
                    # sfgas_tot_e0 = np.log10(sfgas_Ek / sfgas_M)
                    # # sfgas_tot_e0 = np.log10(np.abs(sfgas_Ek + sfgas_Phi) / sfgas_M)
                    # sfgas_kappa_T0 = np.log10(sfgas_kappa_T0 * sfgas_Ek / sfgas_M)


                if DM:
                    DM_y0 = raw_data0[keys.key0_dict['kappa_co']['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                # mask and units
                log_M200 = np.log10(M200[mask0]) + 10  # log Mstar
                x = np.log10(x0[mask0]) + 10  # log Mstar
                # x = np.log10(M200[mask0]) + 10 # log Mstar
                if stars:
                    y = y0[mask0]
                if CC_stars and not r12:
                    cc_y = cc_y0[mask0]
                    cc_N = cc_N0[mask0]
                if gas:
                    gas_y = gas_y0[mask0]
                    gas_kappa_T = gas_kappa_T0[mask0]
                if sfgas:
                    sfgas_y = sfgas_y0[mask0]
                    sfgas_kappa_T = sfgas_kappa_T0[mask0]
                    sfgas_tot_e = sfgas_tot_e0[mask0]
                    sfgas_N = sfgas_N0[mask0]
                if DM:
                    DM_y = DM_y0[mask0]

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 20 * np.diff(N_run)[0]

            if stars:
                for i in range(len(bin_centres)):

                    # calc medians etc.
                    ymedians, xedges, bin_is = binned_statistic(x[y[:, i]!=0], y[y[:, i]!=0, i],
                                                                statistic=np.nanmedian, bins=N_run)
                    yplus, xedges, _ = binned_statistic(x[y[:, i]!=0], y[y[:, i]!=0, i],
                                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                    yminus, xedges, _ = binned_statistic(x[y[:, i]!=0], y[y[:, i]!=0, i],
                                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                    xcentres = 0.5 * (xedges[1:] + xedges[:-1])


                    m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
                    mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

                    xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                    ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                    # plot
                    axs[col].errorbar(xcentres[mask], ymedians[mask],
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=1, zorder=10 - i, alpha=1, path_effects=path_effect_2)
                    axs[col].errorbar(xlin, ymed_func(xlin),
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=2, zorder=10 - i, alpha=1, path_effects=path_effect_4)

            if CC_stars and not r12:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[cc_y!=0], cc_y[cc_y!=0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                # TODO CC mask
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if gas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[gas_y!=0], gas_y[gas_y!=0],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[gas_y!=0], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # TODO gas mask
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]

                # axs[col].scatter(x, gas_y,
                #                  color=color3, marker='o', s=4, alpha=0.5,
                #                  linewidths=0, zorder=2, rasterized=True)

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

                #gas temp
                ymedians, xedges, bin_is = binned_statistic(x[gas_kappa_T!=0], gas_kappa_T[gas_kappa_T!=0],
                                                            statistic=np.nanmedian, bins=N_run)

                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=8, alpha=1, path_effects=path_effect_2)

            if sfgas:
                # calc medians etc.
                galaxy_mask = np.logical_and(sfgas_N > N_res, sfgas_y!=0)

                ymedians, xedges, bin_is = binned_statistic(x[galaxy_mask], sfgas_y[galaxy_mask],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[galaxy_mask], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]
                mask2 = counts > 150 * np.diff(N_run)[0]

                axs[col].scatter(x[galaxy_mask], sfgas_y[galaxy_mask],
                                 color=color, marker='o', s=4, alpha=0.5,
                                 linewidths=0, zorder=2, rasterized=True)

                axs[col].errorbar(xcentres[mask2], ymedians[mask2],
                                  c=color,
                                  ls=linestyle, linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)
                # axs[col].errorbar(xcentres[mask], ymedians[mask],
                #                   c=color,
                #                   ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
                
                # thermal energy
                if temp:
                    ymedians, xedges, bin_is = binned_statistic(x[galaxy_mask], sfgas_kappa_T[galaxy_mask],
                                                                statistic=np.nanmedian, bins=N_run)

                    axs[col].scatter(x[galaxy_mask], sfgas_kappa_T[galaxy_mask],
                                     color=color2, marker='o', s=4, alpha=0.5,
                                     linewidths=0, zorder=3, rasterized=True)

                    axs[col].errorbar(xcentres[mask], ymedians[mask],
                                      c=color2,
                                      ls=linestyle, linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)

                # #total energy
                # ymedians, xedges, bin_is = binned_statistic(x[galaxy_mask], sfgas_tot_e[galaxy_mask],
                #                                             statistic=np.nanmedian, bins=N_run)
                # axs[col].scatter(x[galaxy_mask], sfgas_tot_e[galaxy_mask],
                #                  color=color2, marker='o', s=4, alpha=0.5,
                #                  linewidths=0, zorder=3, rasterized=True)
                # axs[col].errorbar(xcentres[mask], ymedians[mask],
                #                   c=color2,
                #                   ls=linestyle, linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)

            if DM:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, DM_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        # decorations
        # decorations
    if all_z:
        axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=0$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=0.5$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=1$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                    r'$z=2$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    else:
        if z_index == 0:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=0$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 1:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=0.5$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 2:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=1$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        if z_index == 3:
            axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        r'$z=2$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    for col in range(len(cnp)):
        if r12:
            radii_label = r'$r=r_{1/2, \star}$'
        else:
            radii_label = r'$r<r_{200}$'
        axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
                      radii_label, ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if stars:
        if all_z:
            Bbox_top = axs[1].get_position()
            Bbox_bot = axs[2].get_position()
        else:
            Bbox_top = axs[0].get_position()
            Bbox_bot = axs[0].get_position()

        width = 0.1
        pad = 0.05
        cax = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                        (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                        (Bbox_top.y1 - Bbox_bot.y0) / 2])

        n_col_grad = 101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x.T, cmap=cmap_0,
                   # vmin = 0, vmax = 1.25,
                   vmin=0, vmax=1,
                   aspect=2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        cax.set_xticks([])
        cax.set_xticklabels([])
        # cax.set_yticks([0.6, 0.7, 0.8, 0.9, 1])
        cax.set_yticklabels([])
        cax.yaxis.tick_left()

        cax2 = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                         (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                         (Bbox_top.y1 - Bbox_bot.y0) / 2])

        cax2.imshow(x.T, cmap=cmap_1,
                    # vmin = 0, vmax = 1.25,
                    vmin=0, vmax=1,
                    aspect=2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        for age in mgm.lbt(1 / (1 + np.array([0, 0.5, 1, 2]))):
            print(age)
            for _cax in [cax, cax2]:
                _cax.axhline(age, 0, 1,
                             c='k', ls=(0, (0.8, 0.8)))
                _cax.axhline(age, 0, 1,
                             c='k', ls=(0, (0.8, 0.8)))

        cax2.set_xticks([])
        cax2.set_xticklabels([])

        cax2.yaxis.set_label_position('right')
        cax2.yaxis.tick_right()

        cax2.set_ylabel('Stellar age [Gyr]')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300, pad_inches=0.05)
        plt.close()

    return


def more_sigma_scaling(name='', save=False, r12=False, stars=False, all_ages=True,
                       gas=True, sfgas=True, CC_stars=False, DM=False):
    N_res = 10

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    # xlims = [1e-3 + 8, 12 - 1e-3]
    xlims = [1e-3 + 9, 14 - 1e-3]
    ylims = [1e-3 + 0, 2.5 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    # N_run = np.linspace(7.5, 12, 10)
    N_run += 2

    # xlabel = r'$\log \, M_\star / $M$_\odot$'
    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, \sigma_z /$ km s$^{-1}$'

    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0:
            axs[col].set_ylabel(ylabel)
        else:
            axs[col].set_yticklabels([])

        if (col // 2) == 1:
            axs[col].set_xlabel(xlabel)
        else:
            axs[col].set_xticklabels([])
        # axs[col].set_aspect(1)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if all_ages:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:  # '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0:  # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                color4 = 'chartreuse'
                cmap = cmap_0
            else:  # 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                color4 = 'fuchsia'
                cmap = cmap_1

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'r200'

                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                # x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                if stars:
                    rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                    rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                    y0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, rsn:rsx]

                if CC_stars and not r12:
                    cc_y0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]
                    cc_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]

                if gas:
                    gas_y0 = raw_data0[keys.key0_dict['sigma_z']['Gas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]
                    gas_N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                if sfgas:
                    sfgas_y0 = raw_data0[keys.key0_dict['sigma_z']['SFGas']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]
                    sfgas_N0 = raw_data0[keys.key0_dict['Npart']['SFGas']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                    # J_tot0 = raw_data0[keys.key0_dict['Jtot']['SFGas']][:,
                    #            keys.rkey_to_rcolum_dict[global_r_key, None]]
                    # J_z0 = raw_data0[keys.key0_dict['Jz']['SFGas']][:,
                    #            keys.rkey_to_rcolum_dict[global_r_key, None]]

                    # sfgas_y0 *= J_tot0 / np.abs(J_z0)
                    # sfgas_y0 *= J_tot0 / J_z0

                if DM:
                    DM_y0 = raw_data0[keys.key0_dict['sigma_z']['DM']][:, keys.rkey_to_rcolum_dict[global_r_key, None]]

                # mask and units
                log_M200 = np.log10(M200[mask0]) + 10  # log Mstar
                x = np.log10(x0[mask0]) + 10  # log Mstar
                # x = np.log10(M200[mask0]) + 10 # log Mstar
                if stars:
                    y = y0[mask0]
                if CC_stars and not r12:
                    cc_y = cc_y0[mask0]
                    cc_N = cc_N0[mask0]
                if gas:
                    gas_y = gas_y0[mask0]
                    gas_N = gas_N0[mask0]
                if sfgas:
                    sfgas_y = sfgas_y0[mask0]
                    sfgas_N = sfgas_N0[mask0]
                if DM:
                    DM_y = DM_y0[mask0]

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 20 * np.diff(N_run)[0]

            if stars:
                for i in range(len(bin_centres)):

                    # calc medians etc.
                    ymedians, xedges, bin_is = binned_statistic(x, y[:, i],
                                                                statistic=np.nanmedian, bins=N_run)
                    yplus, xedges, _ = binned_statistic(x, y[:, i],
                                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                    yminus, xedges, _ = binned_statistic(x, y[:, i],
                                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                    xcentres = 0.5 * (xedges[1:] + xedges[:-1])


                    m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
                    mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

                    xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                    ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                    # plot
                    axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=1, zorder=10 - i, alpha=1, path_effects=path_effect_2)
                    axs[col].errorbar(xlin, np.log10(ymed_func(xlin)),
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=2, zorder=10 - i, alpha=1, path_effects=path_effect_4)

            if CC_stars and not r12:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, cc_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                # TODO CC mask
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if gas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[gas_N>N_res], gas_y[gas_N>N_res],
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[gas_N>N_res], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # TODO gas mask
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]

                # axs[col].scatter(x, gas_y,
                #                  color=color3, marker='o', s=4, alpha=0.5,
                #                  linewidths=0, zorder=2, rasterized=True)

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if sfgas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[sfgas_N>N_res], sfgas_y[sfgas_N>N_res],
                                                            statistic=np.nanmedian, bins=N_run)
                yplus, xedges, _ = binned_statistic(x[sfgas_N>N_res], sfgas_y[sfgas_N>N_res],
                                                    statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                yminus, xedges, _ = binned_statistic(x[sfgas_N>N_res], sfgas_y[sfgas_N>N_res],
                                                     statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x[sfgas_N>N_res], bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]
                mask2 = counts > 150 * np.diff(N_run)[0]

                axs[col].scatter(x[sfgas_N>N_res], np.log10(sfgas_y[sfgas_N>N_res]),
                                 color=color, marker='o', s=4, alpha=0.5,
                                 linewidths=0, zorder=2, rasterized=True)

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color,
                                  ls=linestyle, linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)
                axs[col].errorbar(xcentres[mask], np.log10(yplus[mask]),
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
                axs[col].errorbar(xcentres[mask], np.log10(yminus[mask]),
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
                # axs[col].errorbar(xcentres[mask], ymedians[mask],
                #                   c=color,
                #                   ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            if DM:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, DM_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], np.log10(ymedians[mask]),
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        # decorations
    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    for col in range(len(combined_name_pairs)):
        if r12:
            radii_label = r'$r=r_{1/2, \star}$'
        else:
            radii_label = r'$r<r_{200}$'
        axs[col].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.85 * (ylims[1] - ylims[0]),
                      radii_label, va='top')  # , bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        
        axs[col].axhline(np.log10(11), 0, 1, ls=':', c='grey')

    if stars:
        Bbox_top = axs[1].get_position()
        Bbox_bot = axs[2].get_position()

        width = 0.1
        pad = 0.05
        cax = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                        (Bbox_top.x1 - Bbox_top.x0) * (pad + width),
                        (Bbox_top.y1 - Bbox_bot.y0) / 2])

        n_col_grad = 101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x.T, cmap=cmap_0,
                   # vmin = 0, vmax = 1.25,
                   vmin=0, vmax=1,
                   aspect=2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        cax.set_xticks([])
        cax.set_xticklabels([])
        # cax.set_yticks([0.6, 0.7, 0.8, 0.9, 1])
        cax.set_yticklabels([])
        cax.yaxis.tick_left()

        cax2 = plt.axes([Bbox_top.x1, (Bbox_top.y1 + 3 * Bbox_bot.y0) / 4,
                         (Bbox_top.x1 - Bbox_top.x0) * (pad + 2 * width),
                         (Bbox_top.y1 - Bbox_bot.y0) / 2])

        cax2.imshow(x.T, cmap=cmap_1,
                    # vmin = 0, vmax = 1.25,
                    vmin=0, vmax=1,
                    aspect=2 / width / UNIVERSE_AGE, origin='lower', extent=[0, 1, 0, UNIVERSE_AGE])

        for age in mgm.lbt(1 / (1 + np.array([0, 0.5, 1, 2]))):
            print(age)
            for _cax in [cax, cax2]:
                _cax.axhline(age, 0, 1,
                             c='k', ls=(0, (0.8, 0.8)))
                _cax.axhline(age, 0, 1,
                             c='k', ls=(0, (0.8, 0.8)))

        cax2.set_xticks([])
        cax2.set_xticklabels([])

        cax2.yaxis.set_label_position('right')
        cax2.yaxis.tick_right()

        cax2.set_ylabel('Stellar age [Gyr]')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def jangle(name='', save=False, r12=False, stars=True, all_ages=True,
                       gas=True, sfgas=True, CC_stars=False, DM=True):
    N_res = 1

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    xlims = [1e-3 + 8, 12 - 1e-3]
    # xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [1e-3 - 1, 1 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    N_run = np.linspace(7.8, 12, 22)
    # N_run = np.linspace(7.75, 12, 18)
    # N_run = np.linspace(8-1/3, 12, 14)
    # N_run = np.linspace(7.5, 12, 10)
    # N_run += 2

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    # xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$j_z / |\vec{j}|$'

    bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0:
            axs[col].set_ylabel(ylabel)
        else:
            axs[col].set_yticklabels([])

        if (col // 2) == 1:
            axs[col].set_xlabel(xlabel)
        else:
            axs[col].set_xticklabels([])
        # axs[col].set_aspect(1)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if all_ages:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:  # '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0:  # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
                color4 = 'chartreuse'
                cmap = cmap_0
            else:  # 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'
                color4 = 'fuchsia'
                cmap = cmap_1

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                if r12:
                    r_key = 'eq_rh_profile'
                    global_r_key = 'eq_r12'
                else:
                    r_key = 'r200_profile'
                    global_r_key = 'r200'

                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                # x0 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

                if stars:
                    rsn = keys.rkey_to_rcolum_dict[r_key, 0]
                    rsx = keys.rkey_to_rcolum_dict[r_key, len(bin_centres)]
                    y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, rsn:rsx]

                    J_tot0 = raw_data0[keys.key0_dict['Jtot']['Star']][:, rsn:rsx]
                    J_z0 = raw_data0[keys.key0_dict['Jz']['Star']][:, rsn:rsx]

                    y0 = J_z0 / J_tot0

                if CC_stars and not r12:
                    cc_y0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]
                    cc_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, keys.rkey_to_rcolum_dict['CC_r200', None]]

                if gas:
                    gas_J_tot0 = raw_data0[keys.key0_dict['Jtot']['Gas']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]
                    gas_J_z0 = raw_data0[keys.key0_dict['Jz']['Gas']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]

                    gas_y0 = gas_J_z0 / gas_J_tot0

                if sfgas:
                    sfgas_J_tot0 = raw_data0[keys.key0_dict['Jtot']['SFGas']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]
                    sfgas_J_z0 = raw_data0[keys.key0_dict['Jz']['SFGas']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]

                    # sfgas_y0 = sfgas_J_tot0 / np.abs(sfgas_J_z0)
                    sfgas_y0 = sfgas_J_z0 / sfgas_J_tot0

                if DM:
                    DM_J_tot0 = raw_data0[keys.key0_dict['Jtot']['DM']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]
                    DM_J_z0 = raw_data0[keys.key0_dict['Jz']['DM']][:,
                               keys.rkey_to_rcolum_dict[global_r_key, None]]

                    DM_y0 = DM_J_z0 / DM_J_tot0

                # mask and units
                log_M200 = np.log10(M200[mask0]) + 10  # log Mstar
                x = np.log10(x0[mask0]) + 10  # log Mstar
                # x = np.log10(M200[mask0]) + 10 # log Mstar
                if stars:
                    y = y0[mask0]
                if CC_stars and not r12:
                    cc_y = cc_y0[mask0]
                    cc_N = cc_N0[mask0]
                if gas:
                    gas_y = gas_y0[mask0]
                if sfgas:
                    sfgas_y = sfgas_y0[mask0]
                if DM:
                    DM_y = DM_y0[mask0]

            counts, _ = np.histogram(x, bins=N_run)
            mask = counts > 20 * np.diff(N_run)[0]

            if stars:
                for i in range(len(bin_centres)):

                    # calc medians etc.
                    ymedians, xedges, bin_is = binned_statistic(x, y[:, i],
                                                                statistic=np.nanmedian, bins=N_run)
                    yplus, xedges, _ = binned_statistic(x, y[:, i],
                                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                    yminus, xedges, _ = binned_statistic(x, y[:, i],
                                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                    xcentres = 0.5 * (xedges[1:] + xedges[:-1])


                    m200_intercept = m200_heating_intercept(z, 1 - pair)  # log Msun
                    mstar_intercept = mass_function(mass_function_args(z, True), m200_intercept)

                    xlin = np.linspace(mstar_intercept, xcentres[mask][-1])
                    ymed_func = interp1d(xcentres, ymedians, fill_value='extrapolate')

                    # plot
                    axs[col].errorbar(xcentres[mask], ymedians[mask],
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=1, zorder=10 - i, alpha=1, path_effects=path_effect_2)
                    axs[col].errorbar(xlin, ymed_func(xlin),
                                      # c=color,
                                      # c=matplotlib.cm.get_cmap(cmap)(0.8*(bin_centres[i] / UNIVERSE_AGE)),
                                      c=matplotlib.cm.get_cmap(cmap)(bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=2, zorder=10 - i, alpha=1, path_effects=path_effect_4)

            if CC_stars and not r12:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, cc_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                # TODO CC mask
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if gas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, gas_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # TODO gas mask
                # counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 20 * np.diff(N_run)[0]

                # axs[col].scatter(x, gas_y,
                #                  color=color3, marker='o', s=4, alpha=0.5,
                #                  linewidths=0, zorder=2, rasterized=True)

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color3,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

            if sfgas:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, sfgas_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]
                mask2 = counts > 150 * np.diff(N_run)[0]

                # axs[col].scatter(x, sfgas_y,
                #                  color=color, marker='o', s=4, alpha=0.5,
                #                  linewidths=0, zorder=2, rasterized=True)

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color4,
                                  ls=linestyle, linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)
                # axs[col].errorbar(xcentres[mask], ymedians[mask],
                #                   c=color4,
                #                   ls=linestyle, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            if DM:
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x, DM_y,
                                                            statistic=np.nanmedian, bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=color2,
                                  ls=linestyle, linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        # decorations
    axs[0].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                r'$z=0$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[1].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                r'$z=0.5$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[2].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                r'$z=1$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[3].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                r'$z=2$', ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    for col in range(len(combined_name_pairs)):
        if r12:
            radii_label = r'$r=r_{1/2, \star}$'
        else:
            radii_label = r'$r<r_{200}$'
        axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + 0.15 * (ylims[1] - ylims[0]),
                      radii_label, ha='right')  # , bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def equal_bin(N, m):
    sep = (N.size/float(m))*np.arange(1,m+1)
    idx = sep.searchsorted(np.arange(N.size))
    return idx[N.argsort().argsort()]


def am_mass_rg_plot(name='', save=False):
    '''recreation of figure 2 and or 3 from Rodriguez-Gomez et al. 2022
    '''

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    xlims = [1e-3 + 8, 12 - 1e-3]
    ylims = [1e-3 + 0, 4 - 1e-3]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(7.5, 12, 10)

    xlabel = r'$\log \, M_\star / $M$_\odot$'
    ylabel = r'$\log \, j_\star / $kpc km s$^{-1}$'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        axs[col].set_aspect(1)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for col, name_pairs in enumerate(combined_name_pairs):

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                marker = 'o'
                color = 'C0'
            else:# 1 (1x)
                linestyle = '--'
                marker = 'o'
                color = 'C1'

            with h5.File(name_pairs[pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200 = raw_data0[keys.key0_dict['m200'][None]][:]
                x0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                y0 = raw_data0[keys.key0_dict['jz']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
                yq0 = raw_data0[keys.key1_dict['jz']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
                y0 = y0 / yq0
                kappa_rot = raw_data0[keys.key0_dict['kappa_rot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

                #mask and units
                M200 = np.log10(M200[mask0]) + 10 # log Mstar
                x = np.log10(x0[mask0]) + 10 # log Mstar
                y = np.log10(y0[mask0]) # log kpc
                kappa_rot = kappa_rot[mask0]

            mass_bin_index = np.digitize(x, bins=N_run)

            ten = 7
            kappa_bins = np.linspace(0,1,11)
            kappa_actual_index = np.digitize(kappa_rot, bins=kappa_bins)

            kappa_cum_index = np.zeros(len(x))

            for bin_index in range(len(N_run)):

                kappa_cum_index[mass_bin_index == bin_index+1] = equal_bin(
                    kappa_rot[mass_bin_index == bin_index+1], ten)

            # n_bins = len(kappa_bins)-1
            # kappa_mask_index = kappa_actual_index
            n_bins = ten
            kappa_mask_index = kappa_cum_index

            for kappa_index in range(n_bins):
                if np.sum(kappa_mask_index == kappa_index) == 0:
                    print('Warning: empty bin', kappa_index)
                    continue

                c = matplotlib.cm.get_cmap('turbo')((n_bins - kappa_index - 1) / (n_bins-1))
                # calc medians etc.
                ymedians, xedges, bin_is = binned_statistic(x[kappa_mask_index == kappa_index],
                                                            y[kappa_mask_index == kappa_index],
                                                            statistic=np.nanmedian, bins=N_run)
                # yplus, xedges, _ = binned_statistic(x, y,
                #                                     statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
                # yminus, xedges, _ = binned_statistic(x, y,
                #                                      statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
                xcentres = 0.5 * (xedges[1:] + xedges[:-1])

                counts, _ = np.histogram(x, bins=N_run)
                # mask = counts > 10
                mask = counts > 20 * np.diff(N_run)[0]

                # plot
                axs[col].errorbar(xcentres[mask], ymedians[mask],
                                  c=c, ls=linestyle, linewidth=1, zorder=2, alpha=1, path_effects=path_effect_2)

        #decorations
        # for i in range(20):
        #     #1 Mo, Mao, White style.
        #     axs[col].errorbar(xlims, [i * 1/2 + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * 2/3,
        #                               i * 1/2 + np.floor(ylims[0])],
        #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        # Sugessed by Mike
        ms = np.linspace(*xlims)
        alpha = 0.67
        j0_d = 3.17
        j0_b = 2.25
        js_d = j0_d + alpha * (ms - 10.5)
        js_b = j0_b + alpha * (ms - 10.5)

        fall_color = 'darkgreen'
        axs[col].errorbar(ms, js_d, ls=':', c=fall_color)
        axs[col].errorbar(ms, js_b, ls=':', c=fall_color)

        # axs[col].text(8.90, 2.202, r'FR18 disc', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)
        # axs[col].text(10.3, 2.22, r'FR18 bulge', rotation=np.arctan(2 / 3) * 180 / np.pi, c=fall_color)  # angle2)

    axs[0].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[1].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=0.5$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[2].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=1$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
    axs[3].text(xlims[0] + 0.05 * (xlims[1] - xlims[0]), ylims[0] + 0.95 * (ylims[1] - ylims[0]),
                r'$z=2$', va='top', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=300)
        plt.close()

    return


def am_rad_stacked(shade=False, show_all=False, name='', save=False, jz=True, different_panels=True,
                   do_jcs=True, do_gas=False, do_sfgas=True, do_DM=True, stars_by_age=True, CC_stars=False,
                   z_index=0):
    N_res = 1

    z = [0, 0.503, 1.004, 2.012][z_index]

    fig = plt.figure(constrained_layout=True)

    # mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]
    # mass_bin_edges = [[11,11.2], [11.5,11.7], [12.0,12.2]]
    mass_bin_edges = [[10.85,11.15], [11.35,11.65], [11.85,12.15]]
    # mass_bin_edges = [[9.35,9.65], [9.85,10.15], [10.35,10.65]]
    # mass_bin_edges = [[8.85,9.15], [9.85,10.15], [10.85,11.15]]
    # mass_bin_edges = np.array([np.arange(8, 12, 0.25)[:-1], np.arange(8, 12, 0.25)[1:]]).T

    med_stellar_mass = []
    med_r200s = []

    if different_panels:
        fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 * 2 + 0.05, forward=True)
        spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=2)
    else:
        fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
        spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 0.7, 3.3 - 1e-3]
    # ylims = [0, 4]
    ylims = [1e-3 -1, 5 - 1e-3] #[0.3, 4.3] #[0, 4] #[-0.5, 3.5]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    age_bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    xlabel = r'$\log \, r /$ kpc'
    if jz:
        ylabel = r'$\log \, j_z / $kpc km s$^{-1}$'
    else:
        ylabel = r'$\log \, j_{\rm tot} / $kpc km s$^{-1}$'

    axs = []
    for row in range(1 + different_panels):
        axs.append([])
        for col in range(len(mass_bin_edges)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if col == 0: axs[row][col].set_ylabel(ylabel)
            else: axs[row][col].set_yticklabels([])

            axs[row][col].set_xlabel(xlabel)
            axs[row][col].set_aspect(1/1.5)

    if not different_panels:
        axs.append(axs[0])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if jz:
        j_label = 'jz'
    else:
        j_label = 'jtot'

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if different_panels:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

    for pair in range(2):
    # for pair in range(1):
    # for pair in range(1, 2):
        if pair == 0: # 0 (7x)
            linestyle = '-'
            linestyle2 = '-.'
            color = 'C0'
            color2 = 'navy'
            color3 = 'cyan'
            color4 = 'chartreuse'
            cmap = cmap_0
        else:# 1 (1x)
            linestyle = '--' #'-'
            if different_panels:
                linestyle = '-'
            linestyle2 = (0, (0.8, 0.8))
            color = 'C1'
            color2 = 'saddlebrown'
            color3 = 'gold'
            color4 = 'fuchsia'
            cmap = cmap_1
        gas_linestyle = linestyle
        dm_linestyle = linestyle
        
        if different_panels:
            color = 'yellow' #CC_stars
            color2 = 'k' #DM
            color3 = 'k' #gas
            color4 = 'k' #sfgas
        
            gas_linestyle = '--'
            dm_linestyle = (0,(1,1))
            
        #z=0 output only
        with h5.File(combined_name_pairs[z_index][pair], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            # mask1 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]] > 0.4
            # mask0 = np.logical_and(mask0, mask1)

            dm_mass = DM_MASS
            if 1-pair:
                dm_mass /= 7

            #load
            M200 = raw_data0[keys.key0_dict['m200'][None]][:]
            Mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            r200 = raw_data0[keys.key0_dict['r200'][None]][:]

            y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, 0:0+len(bin_centres)]
            yq0 = raw_data0[keys.key1_dict[j_label]['Star']][:, 0:0+len(bin_centres)]
            y0 = y0 / yq0
            N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, 0:0+len(bin_centres)]

            r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

            if stars_by_age:
                rsn = keys.rkey_to_rcolum_dict['age_02_r_binned', 0]
                rsx = keys.rkey_to_rcolum_dict['age_02_r_binned', len(bin_centres) * len(age_bin_centres)]
                rk2 = keys.rkey_to_rcolum_dict['r200_profile', 0]

                y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, rsn:rsx]
                yq0 = raw_data0[keys.key1_dict[j_label]['Star']][:, rsn:rsx]
                y0 = y0 / yq0
                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, rsn:rsx]

                r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, rk2:rk2+len(age_bin_centres)]

            if CC_stars:
                rcc = keys.rkey_to_rcolum_dict['CC_r_binned', 0]

                CC_y0 = raw_data0[keys.key0_dict[j_label]['Star']][:, rcc:rcc+len(bin_centres)]
                CC_yq0 = raw_data0[keys.key1_dict[j_label]['Star']][:, rcc:rcc+len(bin_centres)]
                CC_y0 = CC_y0 / CC_yq0
                CC_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, rcc:rcc+len(bin_centres)]

            if do_gas:
                gas_y0 = raw_data0[keys.key0_dict[j_label]['Gas']][:, 0:0+len(bin_centres)]
                gas_yq0 = raw_data0[keys.key1_dict[j_label]['Gas']][:, 0:0+len(bin_centres)]
                gas_y0 = gas_y0 / gas_yq0
                gas_N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, 0:0+len(bin_centres)]

            if do_sfgas:
                sfgas_y0 = raw_data0[keys.key0_dict[j_label]['SFGas']][:, 0:0+len(bin_centres)]
                sfgas_yq0 = raw_data0[keys.key1_dict[j_label]['SFGas']][:, 0:0+len(bin_centres)]
                sfgas_y0 = sfgas_y0 / sfgas_yq0
                sfgas_N0 = raw_data0[keys.key0_dict['Npart']['SFGas']][:, 0:0+len(bin_centres)]

                r12_sf_0 = raw_data0[keys.key0_dict['r12r']['SFGas']][:, keys.rkey_to_rcolum_dict['r200', None]]

            if do_DM:
                DM_y0 = raw_data0[keys.key0_dict[j_label]['DM']][:, 0:0+len(bin_centres)]
                DM_yq0 = raw_data0[keys.key1_dict[j_label]['DM']][:, 0:0+len(bin_centres)]
                DM_y0 = DM_y0 / DM_yq0
                # DM_N0 = raw_data0[keys.key0_dict['Npart']['DM']][:, 0:0+len(bin_centres)]
                #TODO bug will be removed next time I run
                DM_N0 = raw_data0[keys.key0_dict['mdmr']['DM']][:, 0:0+len(bin_centres)] / dm_mass**2

            #for vc
            if do_jcs:
                mstar = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0+len(bin_centres)] #10^10 M_sun
                mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0+len(bin_centres)]
                #TODO bug will be removed next time I run
                mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0+len(bin_centres)]

                mtot = np.cumsum(mstar + mgas + mdm, axis=1)

                vc = np.sqrt(GRAV_CONST * mtot / 10**bin_centres)
                jc = vc * 10**bin_centres

            #mask and units
            M200 = M200[mask0]
            Mstar = Mstar[mask0]
            r200 = r200[mask0]
            m0 = np.log10(M200) + 10
            # m0 = np.log10(Mstar) + 10
            y0 = y0[mask0] #np.log10(y0[mask0])
            N0 = N0[mask0]
            r12_0 = np.log10(r12_0[mask0])
            if CC_stars:
                CC_y0 = CC_y0[mask0] #np.log10(CC_y0[mask0])
                CC_N0 = CC_N0[mask0]
            if do_gas:
                gas_y0 = gas_y0[mask0] #np.log10(gas_y0[mask0])
                gas_N0 = gas_N0[mask0]
            if do_sfgas:
                sfgas_y0 = sfgas_y0[mask0] #np.log10(sfgas_y0[mask0])
                sfgas_N0 = sfgas_N0[mask0]
                r12_sf_0 = np.log10(r12_sf_0[mask0])
            if do_DM:
                DM_y0 = DM_y0[mask0] #np.log10(DM_y0[mask0])
                DM_N0 = DM_N0[mask0]
            if do_jcs:
                jc = jc[mask0] #np.log10(jc[mask0])

        for col, mass_range in enumerate(mass_bin_edges):

            #mask
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
            med_stellar_mass.append(np.log10(np.median(Mstar[mask])) + 10)
            med_r200s.append(np.median(r200[mask]))
            y = y0[mask]
            N = N0[mask]
            r12 = r12_0[mask]
            if CC_stars:
                cy = CC_y0[mask]
                cN = CC_N0[mask]
            if do_gas:
                gy = gas_y0[mask]
                gN = gas_N0[mask]
            if do_sfgas:
                sy = sfgas_y0[mask]
                sN = sfgas_N0[mask]
                r12sf = r12_sf_0[mask]
            if do_DM:
                dy = DM_y0[mask]
                dN = DM_N0[mask]
            if do_jcs:
                j = jc[mask]

            # calc medians etc.
            iiin = len(bin_centres)  # np.sum(radial_bin_mask)
            ymedians = np.zeros(iiin)
            yplus = np.zeros(iiin)
            yminus = np.zeros(iiin)

            boots = np.zeros((iiin, 3))

            if CC_stars:
                cmedians = np.zeros(iiin)
                cplus = np.zeros(iiin)
                cminus = np.zeros(iiin)

                cboots = np.zeros((iiin, 3))

                cN_mask = cN > N_res

            if do_gas:
                gmedians = np.zeros(iiin)
                gplus = np.zeros(iiin)
                gminus = np.zeros(iiin)

                gboots = np.zeros((iiin, 3))

                gN_mask = gN > N_res

            if do_sfgas:
                smedians = np.zeros(iiin)
                splus = np.zeros(iiin)
                sminus = np.zeros(iiin)

                sboots = np.zeros((iiin, 3))

                sN_mask = sN > N_res

            if do_DM:
                dmedians = np.zeros(iiin)
                dplus = np.zeros(iiin)
                dminus = np.zeros(iiin)

                dboots = np.zeros((iiin, 3))

                dN_mask = dN > N_res

            if do_jcs:
                jmedians = np.zeros(iiin)
                jplus = np.zeros(iiin)
                jminus = np.zeros(iiin)

                jboots = np.zeros((iiin, 3))

            N_mask = N > N_res
            for iii in range(iiin):
                ys = y[:, iii][N_mask[:, iii]]

                ymedians[iii] = np.log10(np.nanmedian(ys))
                # ymedians[j] = np.nanmean(ys)
                yplus[iii] = np.nanquantile(ys, 0.84)
                yminus[iii] = np.nanquantile(ys, 0.16)

                # boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                if CC_stars:
                    #not sure N mask is correct
                    cs = cy[:, iii][cN_mask[:, iii]]

                    cmedians[iii] = np.log10(np.nanmedian(cs))
                    cplus[iii] = np.nanquantile(cs, 0.84)
                    cminus[iii] = np.nanquantile(cs, 0.16)

                if do_gas:
                    #not sure N mask is correct
                    gs = gy[:, iii][gN_mask[:, iii]]

                    gmedians[iii] = np.log10(np.nanmedian(gs))
                    gplus[iii] = np.nanquantile(gs, 0.84)
                    gminus[iii] = np.nanquantile(gs, 0.16)

                if do_sfgas:
                    #not sure N mask is correct
                    ss = sy[:, iii][sN_mask[:, iii]]

                    smedians[iii] = np.log10(np.nanmedian(ss))
                    splus[iii] = np.nanquantile(ss, 0.84)
                    sminus[iii] = np.nanquantile(ss, 0.16)

                    # print(r12sf[np.sum(sN_mask, axis=1) > 0])

                    _r12sf = np.nanmedian(r12sf[np.sum(sN_mask, axis=1) > 0])

                if do_DM:
                    #not sure N mask is correct
                    ds = dy[:, iii][dN_mask[:, iii]]

                    dmedians[iii] = np.log10(np.nanmedian(ds))
                    dplus[iii] = np.nanquantile(ds, 0.84)
                    dminus[iii] = np.nanquantile(ds, 0.16)

                if do_jcs:
                    #not sure N mask is correct
                    jcs = j[:, iii][dN_mask[:, iii]]

                    jmedians[iii] = np.log10(np.nanmedian(jcs))
                    jplus[iii] = np.nanquantile(jcs, 0.84)
                    jminus[iii] = np.nanquantile(jcs, 0.16)

                    # jboots[iii] = my_bootstrap(jcs, statistic=np.nanmedian)

            #plot
            if not stars_by_age:
                axs[pair][col].errorbar(bin_centres, ymedians,
                             c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                             path_effects=path_effect_4)

            if stars_by_age:

                for i in range(len(age_bin_centres)):
                    _y = y[:, i*len(bin_centres):(i+1)*len(bin_centres)]
                    _N = N[:, i*len(bin_centres):(i+1)*len(bin_centres)]
                    _r12 = r12[:, i]
                    # calc medians etc.
                    iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                    ymedians = np.zeros(iiin)
                    yplus = np.zeros(iiin)
                    yminus = np.zeros(iiin)

                    N_mask = _N > N_res
                    for iii in range(iiin):
                        ys = _y[:, iii][N_mask[:, iii]]

                        ymedians[iii] = np.log10(np.nanmedian(ys))
                        # ymedians[j] = np.nanmean(ys)
                        yplus[iii] = np.nanquantile(ys, 0.84)
                        yminus[iii] = np.nanquantile(ys, 0.16)

                    _r12 = np.nanmedian(_r12[np.sum(N_mask, axis=1) > 0])

                    axs[pair][col].errorbar(bin_centres, ymedians,
                                      c=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                                      ls=linestyle, linewidth=1, zorder=10-i, alpha=1,
                                      path_effects=path_effect_2)

                    # frac_size = 20
                    # ya = ylims[0]
                    # if pair:
                    #     ya += (ylims[1] - ylims[0])/frac_size
                    # axs[pair][col].arrow(_r12, ya,
                    #                      0, (ylims[1] - ylims[0])/frac_size,
                    #                      length_includes_head=True,
                    #                      head_width=(xlims[1] - xlims[0])/frac_size/3,
                    #                      head_length=(ylims[1] - ylims[0])/frac_size/3,
                    #                      color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                    #                      lw=1, zorder=-9, alpha=1)


                    axs[pair][col].scatter(_r12, interp1d(bin_centres, ymedians)(_r12),
                                           color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                                           s=6 ** 2, ec='k', linewidths=1.0, zorder=12)

            if CC_stars:
                axs[pair][col].errorbar(bin_centres, cmedians,
                                  c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1,
                                  path_effects=path_effect_2)
            if do_gas:
                axs[pair][col].errorbar(bin_centres, gmedians,
                                        c=color3, ls=gas_linestyle, linewidth=1, zorder=-1, alpha=1,
                                        path_effects=path_effect_2)
                
            if do_sfgas:
                axs[pair][col].errorbar(bin_centres, smedians,
                                        c=color4, ls=gas_linestyle, linewidth=2, zorder=2, alpha=1,
                                        path_effects=path_effect_4)
                
                axs[pair][col].scatter(_r12sf, interp1d(bin_centres, smedians)(_r12sf),
                                       color='k',
                                       s=6 ** 2, ec='k', linewidths=1.0, zorder=12)
            if do_DM:
                axs[pair][col].errorbar(bin_centres, dmedians,
                                        c=color2, ls=dm_linestyle, linewidth=1, zorder=-1, alpha=1,
                                        path_effects=path_effect_2)

            if do_jcs:
                axs[pair][col].errorbar(bin_centres, jmedians,
                                        c='k', ls=linestyle, linewidth=1, zorder=-1, alpha=1,
                                        path_effects=path_effect_2)

                # min_T_sigma = 11 * np.sqrt(3) #km/s
                # R = 10**bin_centres #kpc
                # vc = 10**jmedians / 10**bin_centres #km/s
                # R12 = 1 #kpc
                #
                # approx_min_T_vphi2 = vc**2 - 2 * min_T_sigma**2 * R / R12
                # approx_min_T_jc = np.log10(np.sqrt(approx_min_T_vphi2) * R)
                #
                # print(vc)
                # print(np.sqrt(approx_min_T_vphi2))
                # print(jmedians)
                # print(approx_min_T_jc)
                # print()
                #
                # axs[pair][col].errorbar(bin_centres, approx_min_T_jc,
                #                   c='k', ls=linestyle, linewidth=1, zorder=-1, alpha=1,
                #                   path_effects=path_effect_2)

                # axs[pair][col].errorbar(bin_centres, jmedians - 1.5,
                #                   c='k', ls=linestyle, linewidth=1, zorder=-1, alpha=1,
                #                   path_effects=path_effect_2)

            if shade:
                axs[pair][col].fill_between(bin_centres, yminus, yplus,
                                 color=color, alpha=0.5, edgecolor='k', zorder=8)

                # axs[col].fill_between(bin_centres, boots[:, 0], boots[:, 2],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9, hatch=hatches[linestyle])

                if do_jcs:
                    axs[pair][col].fill_between(bin_centres, jminus, jplus,
                                     color=color2, alpha=0.5, edgecolor='k', zorder=-3)

                    # axs[col].fill_between(bin_centres, jboots[:, 0], jboots[:, 2],
                    #                       color=color2, alpha=0.5, edgecolor='k', zorder=-2,
                    #                       hatch=hatches[linestyle2])

            if show_all:  # np.sum(mask0) < 100 or
                for i in range(np.sum(mask)):
                    axs[pair][col].errorbar(bin_centres, y[i],
                                 color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

                    if do_jcs:
                        axs[pair][col].errorbar(bin_centres, j[i],
                                          color='k', linestyle='-.', linewidth=1, alpha=0.2, rasterized=True)

            #decorations
            # if pair == 0:

            # mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
            # 
            # H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
            # rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            # rho_200 = 200 * rho_crit
            # 
            # # M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
            # M200 = np.mean(M200_0[mask]) #M_sun
            # r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc

            frac_size = 8
            axs[pair][col].arrow(np.log10(EAGLE_EPS), ylims[1] - (ylims[1] - ylims[0])/4,
                              0, -(ylims[1] - ylims[0]) / frac_size,
                              length_includes_head=True,
                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                              # width=(xlims[1] - xlims[0]) / frac_size / 15,
                              color='k', fc='white', lw=1, zorder=-15, alpha=1)
            axs[pair][col].arrow(np.log10(2.8 * EAGLE_EPS), ylims[1] - (ylims[1] - ylims[0])/4,
                              0, -(ylims[1] - ylims[0]) / frac_size,
                              length_includes_head=True,
                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                              # width=(xlims[1] - xlims[0]) / frac_size / 15,
                              color='k', fc='white', lw=1, zorder=-15, alpha=1)
            axs[pair][col].arrow(np.log10(med_r200s[col]), ylims[0],
                              0, (ylims[1] - ylims[0]) / frac_size,
                              length_includes_head=True,
                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                              # width=(xlims[1] - xlims[0]) / frac_size / 15,
                              color='k', fc='white', lw=1, zorder=-15, alpha=1)

            if False:
                ms = np.mean(mass_range)
                alpha = 0.67
                j0_d = 3.17
                j0_b = 2.25
                js_d = j0_d + alpha * (ms - 10.5)
                js_b = j0_b + alpha * (ms - 10.5)

                axs[pair][col].errorbar(xlims, [js_d]*2,
                                  c='darkgreen', ls='-', lw=1, zorder=-10)
                axs[pair][col].errorbar(xlims, [js_b]*2,
                                  c='darkgreen', ls='-', lw=1, zorder=-10)

    for i in range(len(mass_bin_edges)):
        axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.91 * (ylims[1] - ylims[0]),
                       r'$\log \, M_{200} / $M$_\odot \in$[' + str(round(mass_bin_edges[i][0], 2)) + ', ' +
                       str(round(mass_bin_edges[i][1], 2)) + ']',
                       ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)
        axs[1][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.91 * (ylims[1] - ylims[0]),
                       r'$\log \, \overline{M}_{\star} / $M$_\odot =$' + str(round(med_stellar_mass[i], 1)),
                       ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)
        axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.80 * (ylims[1] - ylims[0]),
                       r'$z=$' + str(z),
                       ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

        if different_panels:
            axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                           'HR',
                           ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)
            axs[1][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                           'LR',
                           ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

    if different_panels:

        Bbox_top = axs[0][1].get_position()
        Bbox_bot = axs[0][1].get_position()

        width = 0.1
        pad = 0.05

        cax = plt.axes([Bbox_top.x0, Bbox_bot.y1,
                        Bbox_top.x1 - Bbox_bot.x0,
                        (Bbox_top.y1 - Bbox_top.y0) * (pad + width),])

        n_col_grad = 1384  # 101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
        x_ = np.digitize(x_, (keys.t_bin_edges - keys.t_bin_edges[0]) /
                         (UNIVERSE_AGE - keys.t_bin_edges[0])) / (len(keys.t_bin_edges) - 1)

        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x, cmap=cmap_0,
                   vmin=0, vmax=1,
                   aspect= width * UNIVERSE_AGE / 2, origin='lower', extent=[0, UNIVERSE_AGE, 0, 1])

        cax.set_yticks([])
        cax.set_yticklabels([])
        # cax.set_xticklabels([])
        cax.xaxis.tick_bottom()

        # cax2 = plt.axes([Bbox_top.x0, Bbox_bot.y1,
        #                 Bbox_top.x1 - Bbox_bot.x0,
        #                 (Bbox_top.y1 - Bbox_top.y0) * (pad + 2 * width),])
        cax2 = cax

        cax2.imshow(x, cmap=cmap_1,
                    vmin=0, vmax=1,
                    aspect= width * UNIVERSE_AGE / 2, origin='lower', extent=[0, UNIVERSE_AGE, 0, 1])

        cax2.set_yticks([])
        cax2.set_yticklabels([])
        cax2.xaxis.set_label_position('top')
        cax2.xaxis.tick_top()
        cax2.xaxis.set_ticks([0,2,4,6,8,10,12])

        cax2.set_xlabel('Stellar age [Gyr]')

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
    #                ],
    #               ['HR DM\n' + r'$j_\star(R)$', 'LR DM\n' + r'$j_\star(R)$'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               title=r'$\log \, M_{\star} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[0]),
    #               loc='lower right')
    #
    # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls=(0, (0.8, 0.8)), lw=2,
    #                              path_effects=path_effect_4)),
    #                ],
    #               ['HR DM\n' + r'$j_c(R)$', 'LR DM\n' + r'$j_c(R)$'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               title=r'$\log \, M_{\star} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[1]),
    #               loc='lower right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def more_sigma_rad_stacked(shade=False, show_all=False, name='', save=False, sz=False, different_panels=True,
                           age_panels=False,
                           do_jcs=False, do_gas=False, do_sfgas=True, do_DM=False, stars_by_age=True, CC_stars=False,
                           norm_V200=True, model=False, jeans_star=False):
    N_ress = [2, 2, 2]
    # N_ress = [10, 10, 10]
    # N_ress = [10] * 100
    # N_ress = [6] * 100
    N_res_sfgas = 8

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10.35,10.65], [10.85,11.15], [11.35,11.65]]
    # mass_bin_edges = [[10.3,10.7], [10.8,11.2], [11.3,11.7]]

    # mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]
    # mass_bin_edges = [[8.35,8.65], [9.35,9.65], [10.35,10.65]]
    # mass_bin_edges = np.array([np.arange(8, 12, 0.25)[:-1], np.arange(8, 12, 0.25)[1:]]).T

    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9+2/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    # mass_bin_edges = np.array([N_run[:-1], N_run[1:]]).T

    if age_panels:
        fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 * len(keys.t_bin_edges[1:]) + 0.05, forward=True)
        spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=len(keys.t_bin_edges[1:]))
    elif different_panels:
        fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 * 2 + 0.05, forward=True)
        spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=2)
    else:
        fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
        spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 0.7, 3.3 - 1e-3]

    # ylims = [0, 4]
    ylims = [1e-3 + 0.5, 3 - 1e-3]  # [0, 4]
    if norm_V200:
        ylims = [1e-3 - 0.7, 0.3 - 1e-3]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    age_bin_centres = 0.5 * (keys.t_bin_edges[1:] + keys.t_bin_edges[:-1])

    xlabel = r'$\log \, r /$ kpc'
    if sz:
        ylabel = r'$\log \, \sigma_z / $km s$^{-1}$'
    else:
        ylabel = r'$\log \, \sigma_{\rm tot} / $km s$^{-1}$'
    if norm_V200:
        ylabel = r'$\log \, \sigma_{\rm tot} (r) / V_{200}$'

    axs = []
    for row in range(max(1 + different_panels, age_panels * len(age_bin_centres))):
        axs.append([])
        for col in range(len(mass_bin_edges)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if col == 0:
                axs[row][col].set_ylabel(ylabel)
            else:
                axs[row][col].set_yticklabels([])

            axs[row][col].set_xlabel(xlabel)
            # axs[row][col].set_aspect('equal')

    if not different_panels:
        axs.append(axs[0])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    cmap_0 = 'Blues_r'
    cmap_1 = 'Oranges_r'
    if different_panels:
        cmap_0 = 'turbo'
        cmap_1 = 'turbo'

    name_pairs = combined_name_pairs[0]

    for pair in range(2):
        # for pair in range(1):
        # for pair in range(1, 2):
        if pair == 0:  # 0 (7x)
            linestyle = '-'
            linestyle2 = '-.'
            color = 'C0'
            color2 = 'navy'
            color3 = 'cyan'
            color4 = 'chartreuse'
            cmap = cmap_0
        else:  # 1 (1x)
            linestyle = '--'  # '-'
            if not age_panels and different_panels:
                linestyle = '-'
            linestyle2 = (0, (0.8, 0.8))
            color = 'C1'
            color2 = 'saddlebrown'
            color3 = 'gold'
            color4 = 'fuchsia'
            cmap = cmap_1

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
            str_z = '2'
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
            str_z = '1'
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
            str_z = '0.5'
        else:  # '028_z000p000_' in name_pairs[0]:
            z = 0
            str_z = '0'

        # z=0 output only
        with h5.File(name_pairs[pair], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            # mask1 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]] > 0.4
            # mask0 = np.logical_and(mask0, mask1)

            dm_mass = DM_MASS
            if 1 - pair:
                dm_mass /= 7

            # load
            M200 = raw_data0[keys.key0_dict['m200'][None]][:]
            Mstar = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            r200 = raw_data0[keys.key0_dict['r200'][None]][:]

            y0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, 0:0 + len(bin_centres)]
            if not sz:
                y0 = np.sqrt(y0**2 +
                             raw_data0[keys.key0_dict['sigma_R']['Star']][:, 0:0 + len(bin_centres)]**2 +
                             raw_data0[keys.key0_dict['sigma_phi']['Star']][:, 0:0 + len(bin_centres)]**2)

            N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, 0:0 + len(bin_centres)]

            r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

            if stars_by_age:
                rsn = keys.rkey_to_rcolum_dict['age_02_r_binned', 0]
                rsx = keys.rkey_to_rcolum_dict['age_02_r_binned', len(bin_centres) * len(age_bin_centres)]
                rk2 = keys.rkey_to_rcolum_dict['r200_profile', 0]

                y0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, rsn:rsx]
                if not sz:
                    y0 = np.sqrt(y0 ** 2 +
                                 raw_data0[keys.key0_dict['sigma_R']['Star']][:, rsn:rsx] ** 2 +
                                 raw_data0[keys.key0_dict['sigma_phi']['Star']][:, rsn:rsx] ** 2)

                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, rsn:rsx]

                r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, rk2:rk2 + len(age_bin_centres)]

            if CC_stars:
                rcc = keys.rkey_to_rcolum_dict['CC_r_binned', 0]

                CC_y0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, rcc:rcc + len(bin_centres)]
                if not sz:
                    CC_y0 = np.sqrt(CC_y0 ** 2 +
                                    raw_data0[keys.key0_dict['sigma_R']['Star']][:, rcc:rcc + len(bin_centres)] ** 2 +
                                    raw_data0[keys.key0_dict['sigma_phi']['Star']][:, rcc:rcc + len(bin_centres)] ** 2)

                CC_N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, rcc:rcc + len(bin_centres)]

            if do_gas:
                gas_y0 = raw_data0[keys.key0_dict['sigma_z']['Gas']][:, 0:0 + len(bin_centres)]
                if not sz:
                    gas_y0 = np.sqrt(gas_y0 ** 2 +
                                     raw_data0[keys.key0_dict['sigma_R']['Gas']][:, 0:0 + len(bin_centres)] ** 2 +
                                     raw_data0[keys.key0_dict['sigma_phi']['Gas']][:, 0:0 + len(bin_centres)] ** 2)

                gas_N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, 0:0 + len(bin_centres)]

            if do_sfgas:
                sfgas_y0 = raw_data0[keys.key0_dict['sigma_z']['SFGas']][:, 0:0 + len(bin_centres)]
                if not sz:
                    sfgas_y0 = np.sqrt(sfgas_y0 ** 2 +
                                       raw_data0[keys.key0_dict['sigma_R']['SFGas']][:, 0:0 + len(bin_centres)] ** 2 +
                                       raw_data0[keys.key0_dict['sigma_phi']['SFGas']][:, 0:0 + len(bin_centres)] ** 2)
                sfgas_N0 = raw_data0[keys.key0_dict['Npart']['SFGas']][:, 0:0 + len(bin_centres)]

                r12_sf_0 = raw_data0[keys.key0_dict['r12r']['SFGas']][:, keys.rkey_to_rcolum_dict['r200', None]]

            if do_DM:
                DM_y0 = raw_data0[keys.key0_dict['sigma_z']['DM']][:, 0:0 + len(bin_centres)]
                if not sz:
                    DM_y0 = np.sqrt(DM_y0 ** 2 +
                                    raw_data0[keys.key0_dict['sigma_R']['DM']][:, 0:0 + len(bin_centres)] ** 2 +
                                    raw_data0[keys.key0_dict['sigma_phi']['DM']][:, 0:0 + len(bin_centres)] ** 2)
                # DM_N0 = raw_data0[keys.key0_dict['Npart']['DM']][:, 0:0+len(bin_centres)]
                # TODO bug will be removed next time I run
                DM_N0 = raw_data0[keys.key0_dict['mdmr']['DM']][:, 0:0 + len(bin_centres)] / dm_mass ** 2

            # for vc
            if do_jcs:
                mstar = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0 + len(bin_centres)]  # 10^10 M_sun
                mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0 + len(bin_centres)]
                # TODO bug will be removed next time I run
                mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0 + len(bin_centres)] / dm_mass

                mtot = np.cumsum(mstar + mgas + mdm, axis=1)

                vc = np.sqrt(GRAV_CONST * mtot / 10 ** bin_centres)
                jc = vc * 10 ** bin_centres

            # mask and units
            V200 = np.sqrt(GRAV_CONST * M200[mask0] / r200[mask0])
            M200_0 = M200[mask0] * 1e10
            Mstar_0 = Mstar[mask0] * 1e10
            m0 = np.log10(M200[mask0]) + 10
            y0 = y0[mask0] # np.log10(y0[mask0])
            if norm_V200:
                y0 /= V200[:, np.newaxis]
            N0 = N0[mask0]
            r12_0 = np.log10(r12_0[mask0])
            if CC_stars:
                CC_y0 = CC_y0[mask0]  # np.log10(CC_y0[mask0])
                if norm_V200:
                    CC_y0 /= V200[:, np.newaxis]
                CC_N0 = CC_N0[mask0]
            if do_gas:
                gas_y0 = gas_y0[mask0]  # np.log10(gas_y0[mask0])
                if norm_V200:
                    gas_y0 /= V200[:, np.newaxis]
                gas_N0 = gas_N0[mask0]
            if do_sfgas:
                sfgas_y0 = sfgas_y0[mask0]  # np.log10(sfgas_y0[mask0])
                if norm_V200:
                    sfgas_y0 /= V200[:, np.newaxis]
                sfgas_N0 = sfgas_N0[mask0]
                r12_sf_0 = np.log10(r12_sf_0[mask0])
            if do_DM:
                DM_y0 = DM_y0[mask0]  # np.log10(DM_y0[mask0])
                if norm_V200:
                    DM_y0 /= V200[:, np.newaxis]
                DM_N0 = DM_N0[mask0]
            if do_jcs:
                jc = jc[mask0]  # np.log10(jc[mask0])
                if norm_V200:
                    jc /= V200[:, np.newaxis]

        # for col, mass_range in enumerate(mass_bin_edges):
        for col, (mass_range, N_res) in enumerate(zip(mass_bin_edges, N_ress)):

            # if model:
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])

            H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200 = 200 * rho_crit

            # M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
            M200 = np.mean(M200_0[mask]) #M_sun
            r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
            V200 = np.sqrt(GRAV_CONST * M200 * 1e-10 / r200) #km/s

            Mstar_tot = np.mean(Mstar_0[mask]) #M_sun

            radii = np.logspace(np.log10(keys.log_bin_edges[0]), np.log10(keys.log_bin_edges[-1]))

            if np.log10(M200) > 12.3:
                profile = 'hern'
            else:
                profile = 'exp'

            if not stars_by_age:
                age_bin_centres = [8]

            if not np.isnan(M200):
                for i, median_age in enumerate(age_bin_centres):

                    if age_panels:
                        pair = i

                    if jeans_star:
                        (analytic_v_circ, analytic_dispersion,
                         theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                         stellar_dispersion, v_c_star
                         ) = mgm.get_heated_profile(radii, M200 * 1e-10, MassDM=dm_mass, z=z, time=median_age,
                                                    contracted=True, Mstar=Mstar_tot * 1e-10, hr_size=1-pair)
                        #r12=np.median(r12), profile=profile)
                    else:
                        (analytic_v_circ, analytic_dispersion,
                         theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                         stellar_dispersion, v_c_star
                         ) = mgm.get_heated_profile(radii, M200 * 1e-10, MassDM=dm_mass, z=z, time=median_age,
                                                    contracted=True, Mstar=Mstar_tot * 1e-10, hr_size=True,)
                                                    # ic_heat_const=11)
                        #r12=np.median(r12), profile=profile)
    
                    if sz: #ek:
                        sigma_tot = theory_best_z
                    else:
                        sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)
    
                    # print(sigma_tot)
    
                    # if pair == 0:
                    sqrt3 = np.sqrt(3)
                    if sz:
                        sqrt3 = 1
                    if norm_V200:
                        sqrt3 /= V200
    
                    #halo
                    # if i == 0:
                    if pair < max(1 + different_panels, age_panels * len(age_bin_centres)):
                        axs[pair][col].errorbar(np.log10(radii), np.log10(analytic_dispersion * sqrt3),
                                                c='navy', ls='-.', linewidth=2, zorder=-3, alpha=1)
    
                    c = color
                    if len(age_bin_centres) > 1:
                        c = matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE)
    
                    norm = 1
                    if norm_V200: norm /= V200
    
                    if model:
                        if age_panels:
                            axs[pair][col].errorbar(np.log10(radii), np.log10(sigma_tot * norm),
                                                    c=color, ls=linestyle, linewidth=2, zorder=0-i, alpha=1)

                        else:
                            axs[pair][col].errorbar(np.log10(radii), np.log10(sigma_tot * norm),
                                                    c=c, ls=linestyle, linewidth=1, zorder=0-i, alpha=1)
    
                    if jeans_star:
                        axs[pair][col].errorbar(np.log10(radii), np.log10(stellar_dispersion * sqrt3),
                                             c=color2, ls=linestyle2, linewidth=1, zorder=3, alpha=1)#, path_effects=path_effect_2)
    
                        # axs[1][col].errorbar(np.log10(radii), np.log10(v_c_star / V200),
                        #                      c=color2, ls=linestyle2, linewidth=1, zorder=3, alpha=1)#, path_effects=path_effect_2)

            #use data
            # mask
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
            y = y0[mask]
            N = N0[mask]
            r12 = r12_0[mask]
            if CC_stars:
                cy = CC_y0[mask]
                cN = CC_N0[mask]
            if do_gas:
                gy = gas_y0[mask]
                gN = gas_N0[mask]
            if do_sfgas:
                sy = sfgas_y0[mask]
                sN = sfgas_N0[mask]
                r12sf = r12_sf_0[mask]
            if do_DM:
                dy = DM_y0[mask]
                dN = DM_N0[mask]
            if do_jcs:
                j = jc[mask]

            # calc medians etc.
            iiin = len(bin_centres)  # np.sum(radial_bin_mask)
            ymedians = np.zeros(iiin)
            yplus = np.zeros(iiin)
            yminus = np.zeros(iiin)

            boots = np.zeros((iiin, 3))

            if CC_stars:
                cmedians = np.zeros(iiin)
                cplus = np.zeros(iiin)
                cminus = np.zeros(iiin)

                cboots = np.zeros((iiin, 3))

                cN_mask = cN > N_res

            if do_gas:
                gmedians = np.zeros(iiin)
                gplus = np.zeros(iiin)
                gminus = np.zeros(iiin)

                gboots = np.zeros((iiin, 3))

                gN_mask = gN > N_res

            if do_sfgas:
                smedians = np.zeros(iiin)
                splus = np.zeros(iiin)
                sminus = np.zeros(iiin)

                sboots = np.zeros((iiin, 3))

                sN_mask = sN > N_res_sfgas

            if do_DM:
                dmedians = np.zeros(iiin)
                dplus = np.zeros(iiin)
                dminus = np.zeros(iiin)

                dboots = np.zeros((iiin, 3))

                dN_mask = dN > N_res

            if do_jcs:
                jmedians = np.zeros(iiin)
                jplus = np.zeros(iiin)
                jminus = np.zeros(iiin)

                jboots = np.zeros((iiin, 3))

            N_mask = N > N_res
            for iii in range(iiin):
                ys = y[:, iii][N_mask[:, iii]]

                ymedians[iii] = np.log10(np.nanmedian(ys))
                # ymedians[j] = np.nanmean(ys)
                yplus[iii] = np.nanquantile(ys, 0.84)
                yminus[iii] = np.nanquantile(ys, 0.16)

                # boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                if CC_stars:
                    # not sure N mask is correct
                    cs = cy[:, iii][cN_mask[:, iii]]

                    cmedians[iii] = np.log10(np.nanmedian(cs))
                    cplus[iii] = np.nanquantile(cs, 0.84)
                    cminus[iii] = np.nanquantile(cs, 0.16)

                if do_gas:
                    # not sure N mask is correct
                    gs = gy[:, iii][gN_mask[:, iii]]

                    gmedians[iii] = np.log10(np.nanmedian(gs))
                    gplus[iii] = np.nanquantile(gs, 0.84)
                    gminus[iii] = np.nanquantile(gs, 0.16)

                if do_sfgas:
                    # not sure N mask is correct
                    ss = sy[:, iii][sN_mask[:, iii]]

                    smedians[iii] = np.log10(np.nanmedian(ss))
                    splus[iii] = np.log10(np.nanquantile(ss, 0.84))
                    sminus[iii] = np.log10(np.nanquantile(ss, 0.16))

                    # print(r12sf[np.sum(sN_mask, axis=1) > 0])

                    _r12sf = np.nanmedian(r12sf[np.sum(sN_mask, axis=1) > 0])

                if do_DM:
                    # not sure N mask is correct
                    ds = dy[:, iii][dN_mask[:, iii]]

                    dmedians[iii] = np.log10(np.nanmedian(ds))
                    dplus[iii] = np.nanquantile(ds, 0.84)
                    dminus[iii] = np.nanquantile(ds, 0.16)

                if do_jcs:
                    # not sure N mask is correct
                    jcs = j[:, iii][N_mask[:, iii]]

                    jmedians[iii] = np.log10(np.nanmedian(jcs))
                    jplus[iii] = np.nanquantile(jcs, 0.84)
                    jminus[iii] = np.nanquantile(jcs, 0.16)

                    # jboots[iii] = my_bootstrap(jcs, statistic=np.nanmedian)

            # plot
            if not stars_by_age:
                axs[pair][col].errorbar(bin_centres, ymedians,
                                        c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                                        path_effects=path_effect_4)

            if stars_by_age:

                for i in range(len(age_bin_centres)):

                    if age_panels:
                        pair = i

                    _y = y[:, i * len(bin_centres):(i + 1) * len(bin_centres)]
                    _N = N[:, i * len(bin_centres):(i + 1) * len(bin_centres)]
                    _r12 = r12[:, i]
                    # calc medians etc.
                    iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                    ymedians = np.zeros(iiin)
                    yplus = np.zeros(iiin)
                    yminus = np.zeros(iiin)

                    N_mask = _N > N_res
                    for iii in range(iiin):
                        ys = _y[:, iii][N_mask[:, iii]]

                        ymedians[iii] = np.log10(np.nanmedian(ys))
                        # ymedians[j] = np.nanmean(ys)
                        yplus[iii] = np.nanquantile(ys, 0.84)
                        yminus[iii] = np.nanquantile(ys, 0.16)

                    _r12 = np.nanmedian(_r12[np.sum(N_mask, axis=1) > 0])

                    if age_panels:
                        axs[pair][col].errorbar(bin_centres, ymedians,
                                                c=color,
                                                ls=linestyle, linewidth=2, zorder=10 - i, alpha=1,
                                                path_effects=path_effect_4)
                    else:
                        axs[pair][col].errorbar(bin_centres, ymedians,
                                                c=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                                                ls=linestyle, linewidth=1, zorder=10 - i, alpha=1,
                                                path_effects=path_effect_2)

                    axs[pair][col].scatter(_r12, interp1d(bin_centres, ymedians)(_r12),
                                           color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                                           s=6 ** 2, ec='k', linewidths=1.0, zorder=10)

                    # frac_size = 8
                    # if age_panels:
                    #     axs[pair][col].arrow(_r12, ylims[0],
                    #                          0, (ylims[1] - ylims[0]) / frac_size,
                    #                          length_includes_head=True,
                    #                          head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                    #                          head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                    #                          color=color,
                    #                          lw=1, zorder=-9, alpha=1)
                    # else:
                    #     if not pair:
                    #         axs[pair][col].arrow(_r12, ylims[0],
                    #                              0, (ylims[1] - ylims[0]) / frac_size,
                    #                              length_includes_head=True,
                    #                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                    #                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                    #                              color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                    #                              lw=1, zorder=-9, alpha=1)
                    #     else:
                    #         axs[pair][col].arrow(_r12, ylims[1],
                    #                              0, (ylims[0] - ylims[1]) / frac_size,
                    #                              length_includes_head=True,
                    #                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                    #                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                    #                              color=matplotlib.cm.get_cmap(cmap)(age_bin_centres[i] / UNIVERSE_AGE),
                    #                              lw=1, zorder=-9, alpha=1)

            if CC_stars:
                axs[pair][col].errorbar(bin_centres, cmedians,
                                        c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1,
                                        path_effects=path_effect_2)
            if do_gas:
                axs[pair][col].errorbar(bin_centres, gmedians,
                                        c=color3, ls=linestyle, linewidth=1, zorder=-1, alpha=1,
                                        path_effects=path_effect_2)
            if do_sfgas:
                axs[pair][col].errorbar(bin_centres, smedians,
                                        c='k', ls='--', linewidth=2, zorder=2, alpha=1,
                                        path_effects=path_effect_4)
                
                # print(_r12sf)
                axs[pair][col].scatter(_r12sf, interp1d(bin_centres, smedians)(_r12sf),
                                       color='k',
                                       s=6 ** 2, ec='k', linewidths=1.0, zorder=10)

                if shade:
                    axs[pair][col].fill_between(bin_centres, sminus, splus,
                                                color=color4, alpha=0.5, edgecolor='k', zorder=-3)

            if do_DM:
                axs[pair][col].errorbar(bin_centres, dmedians,
                                        c='navy', ls='-.', linewidth=2, zorder=-1, alpha=1,)
                                        # path_effects=path_effect_2)

            if do_jcs:
                axs[pair][col].errorbar(bin_centres, jmedians,
                                        c='k', ls=linestyle, linewidth=1, zorder=-1, alpha=1,
                                        path_effects=path_effect_2)

                # axs[pair][col].errorbar(bin_centres, jmedians - 1.5,
                #                   c='k', ls=linestyle, linewidth=1, zorder=-1, alpha=1,
                #                   path_effects=path_effect_2)

            if show_all:  # np.sum(mask0) < 100 or
                for i in range(np.sum(mask)):
                    axs[pair][col].errorbar(bin_centres, y[i],
                                            color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

                    if do_jcs:
                        axs[pair][col].errorbar(bin_centres, j[i],
                                                color='k', linestyle='-.', linewidth=1, alpha=0.2, rasterized=True)

            # decorations

            frac_size = 8
            axs[pair][col].arrow(np.log10(EAGLE_EPS), ylims[0],
                              0, (ylims[1] - ylims[0]) / frac_size,
                              length_includes_head=True,
                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                              # width=(xlims[1] - xlims[0]) / frac_size / 15,
                              color='k', fc='white', lw=1, zorder=-15, alpha=1)
            axs[pair][col].arrow(np.log10(2.8 * EAGLE_EPS), ylims[0],
                              0, (ylims[1] - ylims[0]) / frac_size,
                              length_includes_head=True,
                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                              # width=(xlims[1] - xlims[0]) / frac_size / 15,
                              color='k', fc='white', lw=1, zorder=-15, alpha=1)
            axs[pair][col].arrow(np.log10(r200), ylims[0],
                              0, (ylims[1] - ylims[0]) / frac_size,
                              length_includes_head=True,
                              head_width=(xlims[1] - xlims[0]) / frac_size / 3,
                              head_length=(ylims[1] - ylims[0]) / frac_size / 3,
                              # width=(xlims[1] - xlims[0]) / frac_size / 15,
                              color='k', fc='white', lw=1, zorder=-15, alpha=1)

    for i in range(len(mass_bin_edges)):
        axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.91 * (ylims[1] - ylims[0]),
                    r'$\log \, M_{200} / $M$_\odot \in$[' + str(round(mass_bin_edges[i][0],2)) + ', ' +
                       str(round(mass_bin_edges[i][1],2)) + ']',
                    ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)
        axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.80 * (ylims[1] - ylims[0]),
                    r'$z=$' + str(z),
                    ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

        if not age_panels and different_panels:
            axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        'HR',
                        ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)
            axs[1][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                        'LR',
                        ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

        if age_panels:
            for j in range(len(age_bin_centres)):
                axs[j][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                            f'[{keys.t_bin_edges[j]}-{keys.t_bin_edges[j+1]}] Gyr',
                            ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

    if different_panels:

        Bbox_top = axs[0][1].get_position()
        Bbox_bot = axs[0][1].get_position()

        width = 0.1
        pad = 0.05

        cax = plt.axes([Bbox_top.x0, Bbox_bot.y1,
                        Bbox_top.x1 - Bbox_bot.x0,
                        (Bbox_top.y1 - Bbox_top.y0) * (pad + width),])

        n_col_grad = 1384  # 101
        x_ = np.linspace(1e-3 + 0, 1 - 1e-3, n_col_grad)
        x_ = np.digitize(x_, (keys.t_bin_edges - keys.t_bin_edges[0]) /
                         (UNIVERSE_AGE - keys.t_bin_edges[0])) / (len(keys.t_bin_edges) - 1)
        x, _ = np.meshgrid(x_, np.array([0, 1]))
        cax.imshow(x, cmap=cmap_0,
                   vmin=0, vmax=1,
                   aspect= width * UNIVERSE_AGE / 2, origin='lower', extent=[0, UNIVERSE_AGE, 0, 1])

        cax.set_yticks([])
        cax.set_yticklabels([])
        # cax.set_xticklabels([])
        cax.xaxis.tick_bottom()

        # cax2 = plt.axes([Bbox_top.x0, Bbox_bot.y1,
        #                 Bbox_top.x1 - Bbox_bot.x0,
        #                 (Bbox_top.y1 - Bbox_top.y0) * (pad + 2 * width),])
        cax2 = cax

        cax2.imshow(x, cmap=cmap_1,
                    vmin=0, vmax=1,
                    aspect= width * UNIVERSE_AGE / 2, origin='lower', extent=[0, UNIVERSE_AGE, 0, 1])

        cax2.set_yticks([])
        cax2.set_yticklabels([])
        cax2.xaxis.set_label_position('top')
        cax2.xaxis.tick_top()
        cax2.xaxis.set_ticks([0,2,4,6,8,10,12])

        cax2.set_xlabel('Stellar age [Gyr]')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def am_rad_ratio_stacked(shade=True,show_all=False, name='', save=False):
    N_res = 1

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 1, 3 - 1e-3]
    ylims = [-2, 2]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    xlabel = r'$\log \, r /$ kpc'
    ylabel = r'$\log \, j_{\star, \, \rm tot} / j_{\rm gas, \, tot}$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)
        axs[col].set_aspect('equal')

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for pair in range(2):
        if pair == 0: # 0 (7x)
            linestyle = '-'
            marker = 'o'
            color = 'C0'
            label = 'HRDM'
        else:# 1 (1x)
            linestyle = '--'
            marker = 'o'
            color = 'C1'
            label = 'LRDM'

        #z=0 output only
        with h5.File(combined_name_pairs[0][pair], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

            #load
            m0 = raw_data0[keys.key0_dict['m200'][None]][:]
            y0 = raw_data0[keys.key0_dict['jz']['Star']][:, 0:0+len(bin_centres)]
            yq0 = raw_data0[keys.key1_dict['jz']['Star']][:, 0:0+len(bin_centres)]
            gy0 = raw_data0[keys.key0_dict['jz']['Gas']][:, 0:0+len(bin_centres)]
            gyq0 = raw_data0[keys.key1_dict['jz']['Gas']][:, 0:0+len(bin_centres)]
            y0 = y0 / yq0
            gy0 = gy0 / gyq0
            y0 = y0 / gy0
            N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, 0:0+len(bin_centres)]
            gN0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, 0:0+len(bin_centres)]

            #mask and units
            m0 = np.log10(m0[mask0]) + 10
            y0 = np.log10(y0[mask0])
            N0 = N0[mask0]
            gN0 = gN0[mask0]

        for col, mass_range in enumerate(mass_bin_edges):

            #mask
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
            y = y0[mask]
            N = np.amin((N0, gN0), axis=0)[mask]

            # calc medians etc.
            jn = len(bin_centres)  # np.sum(radial_bin_mask)
            ymedians = np.zeros(jn)
            yplus = np.zeros(jn)
            yminus = np.zeros(jn)

            N_mask = N > N_res
            for j in range(jn):
                ys = y[:, j][N_mask[:, j]]

                ymedians[j] = np.nanmedian(ys)
                # ymedians[j] = np.nanmean(ys)
                yplus[j] = np.nanquantile(ys, 0.84)
                yminus[j] = np.nanquantile(ys, 0.16)

            #plot
            axs[col].errorbar(bin_centres, ymedians,
                         c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                         path_effects=path_effect_4)

            if shade:
                axs[col].fill_between(bin_centres, yminus, yplus,
                                 color=color, alpha=0.5, edgecolor='k', zorder=9)

            if show_all:  # np.sum(mask0) < 100 or
                for i in range(np.sum(mask)):
                    axs[col].errorbar(bin_centres, y[i],
                                 color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

            #decorations
            if pair == 0:
                axs[col].errorbar(xlims, [0, 0],
                                  c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def mean_v_phi_rad_stacked(shade=True, show_all=False, all_vcs=False, mean_v_phi=True, name='', save=False):
    N_res = 1
    do_jcs = True

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 0.7, 3.3 - 1e-3]
    ylims = [-0.5, 3.5]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    xlabel = r'$\log \, r /$ kpc'
    if mean_v_phi:
        ylabel = r'$\log \, \overline{v}_\phi / $km s$^{-1}$'
    else:
        ylabel = r'$\log \, V_c / $km s$^{-1}$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)
        axs[col].set_aspect('equal')

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for comp in ['Star']: #['Star', 'Gas']:
        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                label = 'LRDM'

            #z=0 output only
            with h5.File(combined_name_pairs[0][pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask1 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]] > 0.4
                # mask0 = np.logical_and(mask0, mask1)

                #load
                m0 = raw_data0[keys.key0_dict['m200'][None]][:]
                y0 = raw_data0[keys.key0_dict['mean_v_phi'][comp]][:, 0:0+len(bin_centres)]
                N0 = raw_data0[keys.key0_dict['Npart']['Star']][:, 0:0+len(bin_centres)]
                N0_gas = raw_data0[keys.key0_dict['Npart']['Gas']][:, 0:0+len(bin_centres)]

                #for vc
                if do_jcs:
                    mstar = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0+len(bin_centres)] #10^10 M_sun
                    mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0+len(bin_centres)]
                    mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0+len(bin_centres)]

                    mtot = np.cumsum(mstar + mgas + mdm, axis=1)

                    vc = np.sqrt(GRAV_CONST * mtot / 10**bin_centres)
                    jc = vc

                if all_vcs:
                    mstar = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0+len(bin_centres)] #10^10 M_sun
                    mstar = np.cumsum(mstar, axis=1)
                    mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0+len(bin_centres)]
                    mgas = np.cumsum(mgas, axis=1)
                    mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0+len(bin_centres)]
                    mdm = np.cumsum(mdm, axis=1)

                    vstar = np.sqrt(GRAV_CONST * mstar / 10**bin_centres)
                    vgas = np.sqrt(GRAV_CONST * mgas / 10**bin_centres)
                    vdm = np.sqrt(GRAV_CONST * mdm / 10**bin_centres)

                #mask and units
                m0 = np.log10(m0[mask0]) + 10
                y0 = np.log10(y0[mask0])
                N0 = N0[mask0]
                N0_gas = N0_gas[mask0]
                if do_jcs:
                    jc = np.log10(jc[mask0])
                if all_vcs:
                    vstar = np.log10(vstar[mask0])
                    vgas = np.log10(vgas[mask0])
                    vdm = np.log10(vdm[mask0])

            for col, mass_range in enumerate(mass_bin_edges):

                #mask
                mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
                y = y0[mask]
                N = N0[mask]
                N_gas = N0_gas[mask]
                if do_jcs:
                    j = jc[mask]
                if all_vcs:
                    vs = vstar[mask]
                    vg = vgas[mask]
                    vd = vdm[mask]

                # calc medians etc.
                iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                ymedians = np.zeros(iiin)
                yplus = np.zeros(iiin)
                yminus = np.zeros(iiin)

                boots = np.zeros((iiin, 3))

                if do_jcs:
                    jmedians = np.zeros(iiin)
                    jplus = np.zeros(iiin)
                    jminus = np.zeros(iiin)

                    jboots = np.zeros((iiin, 3))

                if all_vcs:
                    vsmedians = np.zeros(iiin)
                    vsplus = np.zeros(iiin)
                    vsminus = np.zeros(iiin)

                    vgmedians = np.zeros(iiin)
                    vgplus = np.zeros(iiin)
                    vgminus = np.zeros(iiin)

                    vdmedians = np.zeros(iiin)
                    vdplus = np.zeros(iiin)
                    vdminus = np.zeros(iiin)

                    vsboots = np.zeros((iiin, 3))
                    vgboots = np.zeros((iiin, 3))
                    vdboots = np.zeros((iiin, 3))

                N_mask = N > N_res
                N_gas_mask = N_gas > N_res
                for iii in range(iiin):
                    ys = y[:, iii][N_mask[:, iii]]

                    ymedians[iii] = np.nanmedian(ys)
                    # ymedians[j] = np.nanmean(ys)
                    yplus[iii] = np.nanquantile(ys, 0.84)
                    yminus[iii] = np.nanquantile(ys, 0.16)

                    if mean_v_phi:
                        boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                    if do_jcs:
                        #not sure N mask is correct
                        jcs = j[:, iii][N_mask[:, iii]]

                        jmedians[iii] = np.nanmedian(jcs)
                        jplus[iii] = np.nanquantile(jcs, 0.84)
                        jminus[iii] = np.nanquantile(jcs, 0.16)

                        jboots[iii] = my_bootstrap(jcs, statistic=np.nanmedian)

                    if all_vcs:
                        #not sure N mask is correct
                        vss = vs[:, iii][N_mask[:, iii]]
                        vgs = vg[:, iii][N_mask[:, iii]]
                        vds = vd[:, iii][N_mask[:, iii]]

                        vsmedians[iii] = np.nanmedian(vss)
                        vsplus[iii] = np.nanquantile(vss, 0.84)
                        vsminus[iii] = np.nanquantile(vss, 0.16)

                        vgmedians[iii] = np.nanmedian(vgs)
                        vgplus[iii] = np.nanquantile(vgs, 0.84)
                        vgminus[iii] = np.nanquantile(vgs, 0.16)

                        vdmedians[iii] = np.nanmedian(vds)
                        vdplus[iii] = np.nanquantile(vds, 0.84)
                        vdminus[iii] = np.nanquantile(vds, 0.16)

                        if False:
                            vsboots[iii] = my_bootstrap(vss, statistic=np.nanmedian)
                            vgboots[iii] = my_bootstrap(vgs, statistic=np.nanmedian)
                            vdboots[iii] = my_bootstrap(vds, statistic=np.nanmedian)

                #plot
                if mean_v_phi:
                    axs[col].errorbar(bin_centres, ymedians,
                                 c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                                 path_effects=path_effect_4)

                if do_jcs:
                    axs[col].errorbar(bin_centres, jmedians,
                                      c=color2, ls=linestyle2, linewidth=2, zorder=-1, alpha=1,
                                      path_effects=path_effect_4)
                if all_vcs:
                    axs[col].errorbar(bin_centres, vsmedians,
                                      c=color2, ls=linestyle2, linewidth=2, zorder=-1, alpha=1,
                                      path_effects=path_effect_4)
                    axs[col].errorbar(bin_centres, vgmedians,
                                      c=color2, ls=linestyle2, linewidth=2, zorder=-1, alpha=1,
                                      path_effects=path_effect_4)
                    axs[col].errorbar(bin_centres, vdmedians,
                                      c=color2, ls=linestyle2, linewidth=2, zorder=-1, alpha=1,
                                      path_effects=path_effect_4)

                if shade:
                    if mean_v_phi:
                        axs[col].fill_between(bin_centres, yminus, yplus,
                                         color=color, alpha=0.5, edgecolor='k', zorder=8)

                        axs[col].fill_between(bin_centres, boots[:, 0], boots[:, 2],
                                              color=color, alpha=0.5, edgecolor='k', zorder=9, hatch=hatches[linestyle])

                    if do_jcs:
                        axs[col].fill_between(bin_centres, jminus, jplus,
                                         color=color2, alpha=0.5, edgecolor='k', zorder=-3)

                        axs[col].fill_between(bin_centres, jboots[:, 0], jboots[:, 2],
                                              color=color2, alpha=0.5, edgecolor='k', zorder=-2,
                                              hatch=hatches[linestyle2])

                    if all_vcs:
                        axs[col].fill_between(bin_centres, vsminus, vsplus,
                                         color=color2, alpha=0.5, edgecolor='k', zorder=-3)
                        axs[col].fill_between(bin_centres, vgminus, vgplus,
                                         color=color2, alpha=0.5, edgecolor='k', zorder=-3)
                        axs[col].fill_between(bin_centres, vdminus, vdplus,
                                         color=color2, alpha=0.5, edgecolor='k', zorder=-3)

                        if False:
                            axs[col].fill_between(bin_centres, vsboots[:, 0], vsboots[:, 2],
                                                  color=color2, alpha=0.5, edgecolor='k', zorder=-2,
                                                  hatch=hatches[linestyle2])
                            axs[col].fill_between(bin_centres, vgboots[:, 0], vgboots[:, 2],
                                                  color=color2, alpha=0.5, edgecolor='k', zorder=-2,
                                                  hatch=hatches[linestyle2])
                            axs[col].fill_between(bin_centres, vdboots[:, 0], vdboots[:, 2],
                                                  color=color2, alpha=0.5, edgecolor='k', zorder=-2,
                                                  hatch=hatches[linestyle2])

                if show_all:  # np.sum(mask0) < 100 or
                    for i in range(np.sum(mask)):
                        if mean_v_phi:
                            axs[col].errorbar(bin_centres, y[i],
                                         color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

                        if do_jcs:
                            axs[col].errorbar(bin_centres, j[i],
                                              color=color2, linestyle=linestyle2, linewidth=1, alpha=0.2, rasterized=True)

                        if all_vcs:
                            axs[col].errorbar(bin_centres, vs[i],
                                              color='k', linestyle='-.', linewidth=1, alpha=0.2, rasterized=True)
                            axs[col].errorbar(bin_centres, vg[i],
                                              color='k', linestyle='-.', linewidth=1, alpha=0.2, rasterized=True)
                            axs[col].errorbar(bin_centres, vd[i],
                                              color='k', linestyle='-.', linewidth=1, alpha=0.2, rasterized=True)

                #decorations
                if pair == 0:
                    axs[col].errorbar(np.log10([EAGLE_EPS, EAGLE_EPS]), ylims,
                                  c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                   ],
                  ['HR DM ' + r'$\overline{v}_\phi$', 'LR DM ' + r'$\overline{v}_\phi$'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  title=r'$\log \, M_{200} / $M$_\odot $' + r'$\in$' + str(mass_bin_edges[0]),
                  loc='upper right')

    axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls=(0, (0.8, 0.8)), lw=2,
                                 path_effects=path_effect_4)),
                   ],
                  ['HR DM ' + r'$V_c$', 'LR DM ' + r'$V_c$'],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  title=r'$\log \, M_{200} / $M$_\odot $' + r'$\in$' + str(mass_bin_edges[1]),
                  loc='upper right')

    for i in range(2, len(mass_bin_edges)):
        axs[i].legend([],[],
                      title=r'$\log \, M_{200} / $M$_\odot $' + r'$\in$' + str(mass_bin_edges[i]),
                      loc='upper right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def sigma_rad_stacked(shade=False, ek=False, mean_v=False, dmsigma=False, gasvc=False, jeans_star=False,
                      name='', save=False):
    N_res = 2
    z_index = 0
    # half_from_size_mass = True

    fig = plt.figure(constrained_layout=True)

    # mass_bin_edges = [[9.85,10.15], [10.35,10.65], [10.85,11.15], [11.35,11.65], [11.85,12.15]]
    mass_bin_edges = [[10.35,10.65], [10.85,11.15], [11.35,11.65]]

    # mass_bin_edges = [[11.7,12], [12,12.3], [12.3,13]]
    # mass_bin_edges = [[11, 11.3], [11.3,11.6], [11.6, 11.9]]
    # mass_bin_edges = [[10.0,10.5], [11.0,11.5], [11.5,12.0], [12.5,13]]

    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9+2/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    mass_bin_edges = np.array([N_run[:-1], N_run[1:]]).T
    # mass_bin_edges = np.array([N_run[:-1] + 0.2, N_run[1:] - 0.2]).T

    # fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 * 3 + 0.08, forward=True)
    # spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=3)
    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 * 2 + 0.08, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=2)

    xlims = [1e-3 - 0.7, 3.3 - 1e-3]
    # xlims = [1e-3 + np.log10(EAGLE_EPS), np.log10(EAGLE_EPS) + 4/3 - 1e-3]
    ylims = [1e-3 -0.6, 0.4 - 1e-3]
    ylims1 = [1e-3 -1.6, 0.4 - 1e-3] #[0.5, 2.5]
    ylims2 = [1e-3 -0.2, 0.1 - 1e-3]
    # ylims2 = [1e-3 -1.1, 1000 - 1e-3]
    ylimss = [ylims, ylims1, ylims2]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    xlabel = r'$\log \, r /$ kpc'
    ylabel = r'$\log \, \sigma_{\rm tot} (r) / V_{200}$'
    ylabel1 = r'$\log \, V_c (r) / V_{200}$'
    # ylabel2 = r'$\log \, V_{c}^{\rm LR} / V_{c, \rm HR}$'
    # ylabel2 = r'$\log \, V_{c}^{\rm LR} / V_{200} - \log \, V_{c, \rm HR} / V_{200}$'
    ylabel2 = r'$\Delta V_{c} / V_{c}$'
    if ek:
        ylabel = r'$\log \, \sqrt{2 E_k} (r) / V_{200}$'
    ylabesl = [ylabel, ylabel1, ylabel2]

    axs = []
    for row in range(2):
        axs.append([])
        for col in range(len(mass_bin_edges)):
            axs[row].append(fig.add_subplot(spec[row, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylimss[row])

            if col == 0: axs[row][col].set_ylabel(ylabesl[row])
            else: axs[row][col].set_yticklabels([])

            if row == 1: axs[row][col].set_xlabel(xlabel)
            else: axs[row][col].set_xticklabels([])

            axs[row][col].set_aspect(np.diff(xlims) / np.diff(ylimss[row]))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if '015_z002p012_' in combined_name_pairs[z_index][0]: z = 2.012
    elif '019_z001p004_' in combined_name_pairs[z_index][0]: z = 1.004
    elif '023_z000p503_' in combined_name_pairs[z_index][0]: z = 0.503
    else: z = 0

    N_gals = [[], []]

    # for comp in ['Star']: #['Star', 'Gas']:2
    comp = 'Star'
    for pair in range(2):
        if pair == 0: # 0 (7x)
            linestyle = '-'
            linestyle2 = '-.'
            marker = 'o'
            color = 'C0'
            color2 = 'navy'
            label = 'HRDM'
        else:# 1 (1x)
            linestyle = '--'
            linestyle2 = (0, (0.8, 0.8))
            marker = 'o'
            color = 'C1'
            color2 = 'saddlebrown'
            label = 'LRDM'

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][pair], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
            
            # #testing
            # M200_0 = (raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]] +
            #           raw_data0[keys.key0_dict['mgas'][None]][:, keys.rkey_to_rcolum_dict['r200', None]] +
            #           raw_data0[keys.key0_dict['mdm'][None]][:, keys.rkey_to_rcolum_dict['r200', None]] +
            #           raw_data0['GalaxyQuantities/BHBinGravMass'][()])

            Mstar_tot_0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            r200 = raw_data0[keys.key0_dict['r200'][None]][:]
            sz = raw_data0[keys.key0_dict['sigma_z'][comp]][:, 0:0+len(bin_centres)]
            sR = raw_data0[keys.key0_dict['sigma_R'][comp]][:, 0:0+len(bin_centres)]
            sphi = raw_data0[keys.key0_dict['sigma_phi'][comp]][:, 0:0+len(bin_centres)]

            r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

            N0 = raw_data0[keys.key0_dict['Npart'][comp]][:, 0:0+len(bin_centres)]

            #add kinetic energy because that's what Arron has done
            if ek or mean_v:
                vphi = raw_data0[keys.key0_dict['mean_v_phi'][comp]][:, 0:0+len(bin_centres)]
                vphi_from_J = raw_data0['GalaxyProfiles/Star/Jtot'][:, 0:0+len(bin_centres)]
                r12_profile = raw_data0['GalaxyProfiles/Star/rHalf'][:, 0:0+len(bin_centres)]
                mstar_profile = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0 + len(bin_centres)]

            if dmsigma:
                dsz = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, 0:0+len(bin_centres)]
                dsy = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, 0:0+len(bin_centres)]
                dsx = raw_data0[keys.key0_dict['DMsigma_tot']['DM']][:, 0:0+len(bin_centres)]

            #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
            half_age0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['r200', None], 2]

            mstar_0 = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0 + len(bin_centres)]  # 10^10 M_sun
            mstar_0 = np.cumsum(mstar_0, axis=1)[mask0, :]
            mgas_0 = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0 + len(bin_centres)]
            mgas_0 = np.cumsum(mgas_0, axis=1)[mask0, :]
            mdm_0 = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0 + len(bin_centres)]
            mdm_0 = np.cumsum(mdm_0, axis=1)[mask0, :]

            # mbh = raw_data0['GalaxyQuantities/BHBinGravMass'][()] # 10^10 M_sun
            mbh_0 = raw_data0['GalaxyQuantities/BHGravMass'][()] # 10^10 M_sun
            mbh_0 = mbh_0[mask0]

            mtot_0 = mdm_0 + mgas_0 + mstar_0 + mbh_0[:, np.newaxis]

            #mask and units
            V200_0 = np.sqrt(GRAV_CONST * M200_0 / r200) # km/s
            m0 = np.log10(M200_0[mask0]) + 10 # log Mstar
            if ek:
                # y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2 +
                #                       vphi[mask0]**2) / V200_0[mask0, np.newaxis])

                y0 = np.log10(np.sqrt(2 * raw_data0[f'GalaxyProfiles/{comp}/KineticEnergy'][:,
                                          0:0+len(bin_centres)][mask0] /
                              raw_data0[f'GalaxyProfiles/{comp}/BinMass'][:,
                                          0:0+len(bin_centres)][mask0]) / V200_0[mask0, np.newaxis])

            else:
                y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2) / V200_0[mask0, np.newaxis])

            # c_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisc'][:, 0:0 + len(bin_centres)]
            # b_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisb'][:, 0:0 + len(bin_centres)]
            # a_0 = raw_data0[f'GalaxyProfiles/{comp}/Axisa'][:, 0:0 + len(bin_centres)]
            # y0 = c_0[mask0] / a_0[mask0]

            N0 = N0[mask0]
            half_age0 = half_age0[mask0]
            r12_0 = r12_0[mask0]
            M200_0 = M200_0[mask0]
            V200_0 = V200_0[mask0]
            Mstar_tot_0 = Mstar_tot_0[mask0]

            vstar_0 = np.sqrt(GRAV_CONST * mstar_0 / 10 ** bin_centres)
            vgas_0 = np.sqrt(GRAV_CONST * mgas_0 / 10 ** bin_centres)
            vdm_0 = np.sqrt(GRAV_CONST * mdm_0 / 10 ** bin_centres)
            vtot_0 = np.sqrt(GRAV_CONST * mtot_0 / 10 ** bin_centres)

            vstar_0 = np.log10(vstar_0 / V200_0[:, np.newaxis])
            vgas_0 = np.log10(vgas_0 / V200_0[:, np.newaxis])
            vdm_0 = np.log10(vdm_0 / V200_0[:, np.newaxis])
            vtot_0 = np.log10(vtot_0 / V200_0[:, np.newaxis])
            # vstar_0 = np.log10(vstar_0)
            # vgas_0 = np.log10(vgas_0)
            # vdm_0 = np.log10(vdm_0)
            # vtot_0 = np.log10(vtot_0)

            vstar_0[np.isinf(vstar_0)] = -100
            vgas_0[np.isinf(vgas_0)] = -100
            vdm_0[np.isinf(vdm_0)] = -100
            vtot_0[np.isinf(vtot_0)] = -100

            if mean_v:
                vphi_0 = np.log10(vphi[mask0] / V200_0[:, np.newaxis])
                #remove negative numbers
                vphi_0[np.isnan(vphi_0)] = 0

                vphi_from_J_0 = np.log10(vphi_from_J[mask0, :] / mstar_profile[mask0, :] / r12_profile[mask0, :] /
                                         V200_0[:, np.newaxis])
                # #remove negative numbers
                vphi_from_J_0[np.isnan(vphi_from_J_0)] = 0

            #assume all haloes have enough DM particles
            if dmsigma:
                dy0 = np.log10(np.sqrt(dsz[mask0] ** 2 + dsy[mask0] ** 2 + dsx[mask0] ** 2) /
                               V200_0[:, np.newaxis])

        with h5.File(combined_name_pairs[z_index][1 - pair], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0

            # load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
            Mstar_tot_1 = raw_data1[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            r200 = raw_data1[keys.key0_dict['r200'][None]][:]
            sz = raw_data1[keys.key0_dict['sigma_z'][comp]][:, 0:0 + len(bin_centres)]
            sR = raw_data1[keys.key0_dict['sigma_R'][comp]][:, 0:0 + len(bin_centres)]
            sphi = raw_data1[keys.key0_dict['sigma_phi'][comp]][:, 0:0 + len(bin_centres)]

            r12_1 = raw_data1[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

            N1 = raw_data1[keys.key0_dict['Npart'][comp]][:, 0:0 + len(bin_centres)]

            # add kinetic energy because that's what Arron has done
            if ek:
                vphi = raw_data1[keys.key0_dict['mean_v_phi'][comp]][:, 0:0 + len(bin_centres)]

            if dmsigma:
                dsz = raw_data1[keys.key0_dict['DMsigma_tot']['DM']][:, 0:0 + len(bin_centres)]
                dsy = raw_data1[keys.key0_dict['DMsigma_tot']['DM']][:, 0:0 + len(bin_centres)]
                dsx = raw_data1[keys.key0_dict['DMsigma_tot']['DM']][:, 0:0 + len(bin_centres)]

            # final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
            half_age1 = raw_data1[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['r200', None], 2]

            mstar_1 = raw_data1[keys.key0_dict['mstarr'][None]][:, 0:0 + len(bin_centres)]  # 10^10 M_sun
            mstar_1 = np.cumsum(mstar_1, axis=1)[mask1, :]
            mgas_1 = raw_data1[keys.key0_dict['mgasr'][None]][:, 0:0 + len(bin_centres)]
            mgas_1 = np.cumsum(mgas_1, axis=1)[mask1, :]
            mdm_1 = raw_data1[keys.key0_dict['mdmr'][None]][:, 0:0 + len(bin_centres)]
            mdm_1 = np.cumsum(mdm_1, axis=1)[mask1, :]

            # mbh = raw_data0['GalaxyQuantities/BHBinGravMass'][()] # 10^10 M_sun
            mbh_1 = raw_data1['GalaxyQuantities/BHGravMass'][()] # 10^10 M_sun
            mbh_1 = mbh_1[mask1]

            mtot_1 = mdm_1 + mgas_1 + mstar_1 + mbh_1[:, np.newaxis]

            # mask and units
            V200_1 = np.sqrt(GRAV_CONST * M200_1 / r200)  # km/s
            m1 = np.log10(M200_1[mask1]) + 10  # log Mstar
            if ek:
                # y1 = np.log10(np.sqrt(sz[mask1] ** 2 + sR[mask1] ** 2 + sphi[mask1] ** 2 +
                #                       vphi[mask1]**2) / V200[mask1, np.newaxis])

                y1 = np.log10(np.sqrt(2 * raw_data1[f'GalaxyProfiles/{comp}/KineticEnergy'][:,
                                          0:0 + len(bin_centres)][mask1] /
                                      raw_data1[f'GalaxyProfiles/{comp}/BinMass'][:,
                                      0:0 + len(bin_centres)][mask1]) / V200_1[mask1, np.newaxis])

            else:
                y1 = np.log10(np.sqrt(sz[mask1] ** 2 + sR[mask1] ** 2 + sphi[mask1] ** 2) / V200_1[mask1, np.newaxis])

            N1 = N1[mask1]
            half_age1 = half_age1[mask1]
            r12_1 = r12_1[mask1]
            M200_1 = M200_1[mask1]
            Mstar_tot_1 = Mstar_tot_1[mask1]

            vstar_1 = np.sqrt(GRAV_CONST * mstar_1 / 10 ** bin_centres)
            vgas_1 = np.sqrt(GRAV_CONST * mgas_1 / 10 ** bin_centres)
            vdm_1 = np.sqrt(GRAV_CONST * mdm_1 / 10 ** bin_centres)
            vtot_1 = np.sqrt(GRAV_CONST * mtot_1 / 10 ** bin_centres)

            vstar_1 = np.log10(vstar_1 / V200_1[mask1, np.newaxis])
            vgas_1 = np.log10(vgas_1 / V200_1[mask1, np.newaxis])
            vdm_1 = np.log10(vdm_1 / V200_1[mask1, np.newaxis])
            vtot_1 = np.log10(vtot_1 / V200_1[mask1, np.newaxis])
            # vstar_1 = np.log10(vstar_1)
            # vgas_1 = np.log10(vgas_1)
            # vdm_1 = np.log10(vdm_1)
            # vtot_1 = np.log10(vtot_1)

            vstar_1[np.isinf(vstar_1)] = -100
            vgas_1[np.isinf(vgas_1)] = -100
            vdm_1[np.isinf(vdm_1)] = -100
            vtot_1[np.isinf(vtot_1)] = -100

            # assume all haloes have enough DM particles
            if dmsigma:
                dy1 = np.log10(np.sqrt(dsz[mask1] ** 2 + dsy[mask1] ** 2 + dsx[mask1] ** 2) /
                               V200_1[mask1, np.newaxis])

            #mask and units

        for col, mass_range in enumerate(mass_bin_edges):

            #mask m200 bin
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])

            y = y0[mask]
            N = N0[mask]
            if dmsigma:
                dy = dy0[mask]
            half_age = half_age0[mask]
            r12 = r12_0[mask]
            M200 = M200_0[mask]
            V200 = V200_0[mask]
            Mstar_tot = Mstar_tot_0[mask]

            print(z, 1-pair, mass_range, round(np.log10(np.mean(Mstar_tot))+10, 3), round(np.median(r12), 3))

            vstar = vstar_0[mask]
            vgas = vgas_0[mask]
            vdm = vdm_0[mask]
            vtot = vtot_0[mask]

            if mean_v:
                vphi = vphi_0[mask]
                vphi_from_J = vphi_from_J_0[mask]

            mstar = mstar_0[mask]
            mgas = mgas_0[mask]
            mdm = mdm_0[mask]
            mtot = mtot_0[mask]

            # median_age = np.nanmedian(half_age)
            median_age = np.nanmedian(half_age) - mgm.lbt(np.array([1 / (1 + z)]))[0]

            N_gal = np.sum(mask)
            N_gals[pair].append(N_gal)

            if N_gal == 0:
                continue

            # theory
            MassDM = DM_MASS
            if pair == 0:
                MassDM /= 7

            H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200 = 200 * rho_crit

            # M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
            M200 = np.mean(M200) * 1e10 #M_sun
            r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
            V200 = np.sqrt(GRAV_CONST * M200 * 1e-10 / r200) #km/s

            radii = np.logspace(np.log10(keys.log_bin_edges[0]), np.log10(keys.log_bin_edges[-1]))

            if np.log10(M200) > 12.3:
                profile = 'hern'
            else:
                profile = 'exp'

            if jeans_star:
                (analytic_v_circ, analytic_dispersion,
                 theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                 stellar_dispersion, v_c_star
                 ) = mgm.get_heated_profile(radii, M200 * 1e-10, MassDM=MassDM, z=z, time=median_age, ic_heat_fraction=0,
                                            contracted=True, Mstar=np.mean(Mstar_tot), hr_size=1-pair)
                #r12=np.median(r12), profile=profile)
            else:
                (analytic_v_circ, analytic_dispersion,
                 theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                 stellar_dispersion, v_c_star
                 ) = mgm.get_heated_profile(radii, M200 * 1e-10, MassDM=MassDM, z=z, time=median_age, ic_heat_fraction=0,
                                            contracted=True, Mstar=np.mean(Mstar_tot), hr_size=True)
                #r12=np.median(r12), profile=profile)

            if False: #ek:
                sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2 + theory_best_v**2)
            else:
                sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)
                # sigma_tot = np.sqrt(3 * theory_best_p**2)

            #for limit of resolved data
            heating_limit = interp1d(np.log10(radii), np.log10(sigma_tot / V200), fill_value='extrapolate')

            heating_limit2 = interp1d(np.log10(radii), np.log10(theory_best_v / V200), fill_value='extrapolate')

            if pair == 0:
                axs[0][col].errorbar(np.log10(radii), np.log10(analytic_dispersion * np.sqrt(3) / V200),
                                     c=color2, ls=linestyle2, linewidth=2, zorder=-3, alpha=1)
                axs[1][col].errorbar(np.log10(radii), np.log10(analytic_v_circ / V200),
                                     c=color2, ls=linestyle2, linewidth=2, zorder=-3, alpha=1)

            axs[0][col].errorbar(np.log10(radii), np.log10(sigma_tot / V200),
                              c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1)#, path_effects=path_effect_4)

            if False:
                axs[1][col].errorbar(np.log10(radii), np.log10(theory_best_v / V200),
                                     c=color, ls=linestyle, linewidth=2, zorder=4, alpha=1)#, path_effects=path_effect_4)

            # #testing
            # axs[1][col].errorbar(np.log10(radii),
            #                      np.log10((analytic_v_circ - theory_best_r**2 /
            #                      analytic_dispersion**2 * analytic_v_circ) / V200),
            #                      c=color2, ls=linestyle2, linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            if jeans_star:
                axs[0][col].errorbar(np.log10(radii), np.log10(stellar_dispersion * np.sqrt(3) / V200),
                                     c=color2, ls=linestyle2, linewidth=1, zorder=3, alpha=1)#, path_effects=path_effect_2)

                axs[1][col].errorbar(np.log10(radii), np.log10(v_c_star / V200),
                                     c=color2, ls=linestyle2, linewidth=1, zorder=3, alpha=1)#, path_effects=path_effect_2)

            # calc medians etc.
            iiin = len(bin_centres)  # np.sum(radial_bin_mask)
            ymedians = np.zeros(iiin)
            yplus = np.zeros(iiin)
            yminus = np.zeros(iiin)

            boots = np.zeros((iiin, 3))

            star_vc = np.zeros(iiin)
            gas_vc = np.zeros(iiin)
            dm_vc = np.zeros(iiin)
            tot_vc = np.zeros(iiin)

            star_vphi = np.zeros(iiin)
            star_vphi_from_J = np.zeros(iiin)

            if dmsigma:
                dymedians = np.zeros(iiin)
                dyplus = np.zeros(iiin)
                dyminus = np.zeros(iiin)

            N_mask = N > N_res
            for iii in range(iiin):
                ys = y[:, iii][N_mask[:, iii]]

                ymedians[iii] = np.nanmedian(ys)
                # ymedians[j] = np.nanmean(ys)
                yplus[iii] = np.nanquantile(ys, 0.84)
                yminus[iii] = np.nanquantile(ys, 0.16)

                if mean_v:
                    star_vphis = vphi[:, iii][N_mask[:, iii]]
                    star_vphi[iii] = np.median(star_vphis)

                    star_vphi_from_Js = vphi_from_J[:, iii][N_mask[:, iii]]
                    star_vphi_from_J[iii] = np.median(star_vphi_from_Js)

                if shade:
                    boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                if dmsigma:
                    dys = dy[:, iii][N_mask[:, iii]]
                    dymedians[iii] = np.nanmedian(dys)
                    dyplus[iii] = np.nanquantile(dys, 0.84)
                    dyminus[iii] = np.nanquantile(dys, 0.16)

                # star_vc[iii] = np.nanmedian(vstar[:, iii])
                # gas_vc[iii] = np.nanmedian(vgas[:, iii])
                # dm_vc[iii] = np.nanmedian(vdm[:, iii])
                # tot_vc[iii] = np.nanmedian(vtot[:, iii])

                # star_vc[iii] = np.log10(np.nanmean(10**vstar[:, iii]))
                # gas_vc[iii] = np.log10(np.nanmean(10**vgas[:, iii]))
                # dm_vc[iii] = np.log10(np.nanmean(10**vdm[:, iii]))
                # tot_vc[iii] = np.log10(np.nanmean(10**vtot[:, iii]))

                star_vc[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mstar[:, iii]) / 10**bin_centres[iii]) / V200)
                gas_vc[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mgas[:, iii]) / 10**bin_centres[iii]) / V200)
                dm_vc[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mdm[:, iii]) / 10**bin_centres[iii]) / V200)
                tot_vc[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mtot[:, iii]) / 10**bin_centres[iii]) / V200)

            #for converge radii
            if np.sum(np.isfinite(ymedians)) < 2:
                r_intercept = np.log10(EAGLE_EPS) #np.nan
            else:
                y_med_interp = interp1d(bin_centres[np.isfinite(ymedians)],
                                        ymedians[np.isfinite(ymedians)], fill_value='extrapolate')

                heated_intercept_func = lambda radii: y_med_interp(radii) - heating_limit(radii)
                try:
                    r_intercept = brentq(heated_intercept_func, np.log10(EAGLE_EPS), 4)
                except ValueError:
                    r_intercept = np.log10(EAGLE_EPS)

            #plot
            if np.sum(np.isfinite(ymedians)) < 2:
                x_converged = np.linspace(r_intercept, xlims[1], 101)
            else:
                x_converged = np.linspace(r_intercept, np.amax(bin_centres[np.isfinite(ymedians)]), 101)
            
            #cut off bins that go crazy a low particle numbers
            if np.log10(M200) < 10.8:
                ymedians[1] = np.nan

            axs[0][col].errorbar(bin_centres, ymedians,
                                 c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)
            axs[0][col].errorbar(x_converged, y_med_interp(x_converged),
                                 c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1, path_effects=path_effect_4)

            #testing
            # print(star_vphi)
            if mean_v:
                # star_vphi_interp = interp1d(bin_centres[np.isfinite(star_vphi)],
                #                         star_vphi[np.isfinite(star_vphi)], fill_value='extrapolate')
                # xx_converged = np.linspace(r_intercept, np.amax(bin_centres[np.isfinite(star_vphi)]), 101)
                # axs[1][col].errorbar(bin_centres, star_vphi,
                #                      c=color, ls=linestyle, linewidth=1, zorder=8, alpha=1, path_effects=path_effect_2)
                # axs[1][col].errorbar(xx_converged, star_vphi_interp(xx_converged),
                #                      c=color, ls=linestyle, linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)

                star_vphi_from_J_interp = interp1d(bin_centres[np.isfinite(star_vphi_from_J)],
                                        star_vphi_from_J[np.isfinite(star_vphi_from_J)], fill_value='extrapolate')
                xx_converged = np.linspace(r_intercept, np.amax(bin_centres[np.isfinite(star_vphi)]), 101)
                axs[1][col].errorbar(bin_centres, star_vphi_from_J,
                                     c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)
                axs[1][col].errorbar(xx_converged, star_vphi_from_J_interp(xx_converged),
                                     c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1, path_effects=path_effect_4)

                axs[1][col].axvline(np.log10(30), 0, 1,
                                    c='k', ls=':')

            # axs[0][col].axvline(r_intercept, 0, 1,
            #                     c=color, ls=linestyle2, linewidth=1, zorder=3, alpha=1)
            # axs[1][col].axvline(r_intercept, 0, 1,
            #                     c=color, ls=linestyle2, linewidth=1, zorder=3, alpha=1)

            try:
                star_vc_interp = interp1d(bin_centres[np.isfinite(star_vc)],
                                          star_vc[np.isfinite(star_vc)], fill_value='extrapolate')

                x_converged = np.linspace(r_intercept, np.amax(bin_centres[np.isfinite(star_vc)]), 101)
                axs[1][col].errorbar(bin_centres, star_vc,
                                     c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)
                axs[1][col].errorbar(x_converged, star_vc_interp(x_converged),
                                     c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1, path_effects=path_effect_4)
            except ValueError:
                axs[1][col].errorbar(bin_centres, star_vc,
                                     c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)

            if gasvc:
                axs[1][col].errorbar(bin_centres, gas_vc,
                                     c='C6', ls=linestyle, linewidth=2, zorder=0, alpha=1, path_effects=path_effect_4)
            if dmsigma:
                # axs[1][col].errorbar(bin_centres, dm_vc,
                #                      c=color2, ls=linestyle2, linewidth=2, zorder=5, alpha=1, path_effects=path_effect_4)
                axs[1][col].errorbar(bin_centres, tot_vc,
                                     c=color2, ls=linestyle2, linewidth=2, zorder=0, alpha=1, path_effects=path_effect_4)

            if shade:
                axs[0][col].fill_between(bin_centres, yminus, yplus,
                                 color=color, alpha=0.5, edgecolor='k', zorder=3)

                axs[0][col].fill_between(bin_centres, boots[:, 0], boots[:, 2],
                                      color=color, alpha=0.5, edgecolor='k', zorder=4, hatch=hatches[linestyle])

            if dmsigma:
                axs[0][col].errorbar(bin_centres, dymedians,
                             c=color2, ls=linestyle2, linewidth=2, zorder=3, alpha=1,
                             path_effects=path_effect_4)

                if shade:
                    axs[0][col].fill_between(bin_centres, dyminus, dyplus,
                                     color=color2, alpha=0.5, edgecolor='k', zorder=-3)

            # #ratio stuff
            # #if#
            # if pair == 1: #0 is LR and 1 is HR #use converged radii already calced
            #
            #     mask_1 = np.logical_and(mass_range[0] < m1, m1 < mass_range[1])
            #
            #     y_1 = y1[mask_1]
            #     N_1 = N1[mask_1]
            #     if dmsigma:
            #         dy_1 = dy1[mask_1]
            #     half_age_1 = half_age1[mask_1]
            #     r121 = r12_1[mask_1]
            #     M2001 = M200_1[mask_1]
            #     Mstar_tot1 = Mstar_tot_1[mask_1]
            #
            #     M2001 = np.median(M2001) * 1e10 #M_sun
            #
            #     r2001 = np.power(3 * M2001 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
            #     V2001 = np.sqrt(GRAV_CONST * M2001 * 1e-10 / r2001) #km/s
            #
            #     vstar1 = vstar_1[mask_1]
            #     vgas1 = vgas_1[mask_1]
            #     vdm1 = vdm_1[mask_1]
            #     vtot1 = vtot_1[mask_1]
            #
            #     mstar1 = mstar_1[mask_1]
            #     mgas1 = mgas_1[mask_1]
            #     mdm1 = mdm_1[mask_1]
            #     mtot1 = mtot_1[mask_1]
            #
            #     # calc medians etc.
            #     iiin = len(bin_centres)  # np.sum(radial_bin_mask)
            #     ymedians1 = np.zeros(iiin)
            #     yplus1 = np.zeros(iiin)
            #     yminus1 = np.zeros(iiin)
            #
            #     boots1 = np.zeros((iiin, 3))
            #
            #     star_vc1 = np.zeros(iiin)
            #     gas_vc1 = np.zeros(iiin)
            #     dm_vc1 = np.zeros(iiin)
            #     tot_vc1 = np.zeros(iiin)
            #
            #     if dmsigma:
            #         dymedians1 = np.zeros(iiin)
            #         dyplus1 = np.zeros(iiin)
            #         dyminus1 = np.zeros(iiin)
            #
            #     N_mask_1 = N_1 > N_res
            #     for iii in range(iiin):
            #         ys1 = y_1[:, iii][N_mask_1[:, iii]]
            #
            #         ymedians1[iii] = np.nanmedian(ys1)
            #         # ymedians1[j] = np.nanmean(ys1)
            #         yplus1[iii] = np.nanquantile(ys1, 0.84)
            #         yminus1[iii] = np.nanquantile(ys1, 0.16)
            #
            #         if shade:
            #             boots1[iii] = my_bootstrap(ys1, statistic=np.nanmedian)
            #
            #         if dmsigma:
            #             dys1 = dy_1[:, iii][N_mask_1[:, iii]]
            #             dymedians1[iii] = np.nanmedian(dys1)
            #             dyplus1[iii] = np.nanquantile(dys1, 0.84)
            #             dyminus1[iii] = np.nanquantile(dys1, 0.16)
            #
            #         star_vc1[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mstar1[:, iii]) / 10**bin_centres[iii]) / V2001)
            #         gas_vc1[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mgas1[:, iii]) / 10**bin_centres[iii]) / V2001)
            #         dm_vc1[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mdm1[:, iii]) / 10**bin_centres[iii]) / V2001)
            #         tot_vc1[iii] = np.log10(np.sqrt(GRAV_CONST * np.mean(mtot1[:, iii]) / 10**bin_centres[iii]) / V2001)
            #
            #     y2 = (10**star_vc * V200 - 10**star_vc1 * V2001) / (10**star_vc1 * V2001)
            #     y2_interp = interp1d(bin_centres[np.isfinite(y2)],
            #                          y2[np.isfinite(y2)], fill_value='extrapolate')
            #
            #     axs[2][col].errorbar(bin_centres, y2 - y2[-1],
            #                          c=color, ls=linestyle, linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)
            #     axs[2][col].errorbar(x_converged, y2_interp(x_converged) - y2[-1],
            #                          c=color, ls=linestyle, linewidth=2, zorder=3, alpha=1, path_effects=path_effect_4)
            #
            #     #heating model
            #     # y3 = analytic_v_circ
            #     y3 = theory_best_v
            #     y3_interp = interp1d(np.log10(radii)[np.isfinite(y3)],
            #                          y3[np.isfinite(y3)], fill_value='extrapolate')
            #
            #     y4 = (y3_interp(bin_centres) - 10**star_vc1 * V2001) / (10**star_vc1 * V2001)
            #     axs[2][col].errorbar(bin_centres, y4,
            #                          c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1)
            #
            # if pair == 0: #0 is HR and 1 is LR
            #     #heating model
            #     y3 = theory_best_v
            #     y3_interp = interp1d(np.log10(radii)[np.isfinite(y3)],
            #                          y3[np.isfinite(y3)], fill_value='extrapolate')
            #
            #     y4 = (y3_interp(bin_centres) - 10**star_vc * V200) / (10**star_vc * V200)
            #     axs[2][col].errorbar(bin_centres, y4,
            #                          c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1)
            #
            # #exp or hernquist model
            # if jeans_star:
            #     y5 = v_c_star
            #     y5_interp = interp1d(np.log10(radii)[np.isfinite(y5)],
            #                          y5[np.isfinite(y5)], fill_value='extrapolate')
            #     # y6 = (y5_interp(bin_centres) - 10**star_vc * V200) / (10**star_vc * V200)
            #     y6 = (10**star_vc * V200 - y5_interp(bin_centres)) / y5_interp(bin_centres)
            #     axs[2][col].errorbar(bin_centres, y6,
            #                          c=color2, ls=linestyle2, linewidth=1, zorder=-1, alpha=1)#, path_effects=path_effect_2)
            #
            # #decorations
            #
            # axs[2][col].axhline(0, 0, 1, c='k', ls=':', linewidth=1, zorder = -6)
            # axs[2][col].axhline(0.03, 0, 1, c='k', ls=':', linewidth=1, zorder = -6)
            # axs[2][col].axhline(-0.03, 0, 1, c='k', ls=':', linewidth=1, zorder = -6)

            # axs[col].errorbar(np.log10([median_r12]*2), ylims,
            #               c=color, ls=(0,(5,1,1,1,1,1)), lw=1, zorder=-10, alpha=1)
            frac_size = 8

            # if half_from_size_mass:
            fit_r12 = 10 ** size_function(size_function_args(z, 1-pair), np.mean(mass_range))  # kpc
            # median_M200 = np.nanmedian(np.log10(M200)) + 10
            # fit_r12 = 10 ** size_function(size_function_args(z, 1-pair), median_M200)  # kpc

            axs[0][col].arrow(np.log10(fit_r12), ylims[0] + (ylims[1] - ylims[0])/frac_size,
                           0, (ylims[1] - ylims[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims[1] - ylims[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color=color, lw=1, zorder=-9, alpha=1)
            axs[1][col].arrow(np.log10(fit_r12), ylims1[0] + (ylims1[1] - ylims1[0])/frac_size,
                           0, (ylims1[1] - ylims1[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims1[1] - ylims1[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color=color, lw=1, zorder=-9, alpha=1)

            if col == len(mass_bin_edges) - 2:
                if pair == 1:
                    axs[1][-2].text(np.log10(fit_r12) + 0.01 * (xlims[1] - xlims[0]),
                                    ylims1[0] + (ylims1[1] - ylims1[0]) * (1/frac_size - 0.01),
                                    r'$r_{1/2}$',
                                    color=color, va='bottom', zorder=20, path_effects=white_path_effect_3)
                # else:
                #     axs[1][-2].text(np.log10(fit_r12) - 0.01 * (xlims[1] - xlims[0]),
                #                     ylims1[0] + (ylims1[1] - ylims1[0]) * (1/frac_size - 0.01),
                #                     r'$r_{1/2}$',
                #                     color=color, va='bottom', ha='right', zorder=20,
                #                     path_effects=white_path_effect_3)

            # else:
            #     median_r12 = np.nanmedian(r12)
            #     axs[0][col].arrow(np.log10(median_r12), ylims[0], 0, (ylims[1] - ylims[0])/frac_size,
            #                    length_includes_head=True,
            #                    head_width=(xlims[1] - xlims[0])/frac_size/3,
            #                    head_length=(ylims[1] - ylims[0])/frac_size/3,
            #                    # width=(xlims[1] - xlims[0]) / frac_size / 15,
            #                    color=color, lw=1, zorder=-4, alpha=1)
            #     axs[1][col].arrow(np.log10(median_r12), ylims1[0], 0, (ylims1[1] - ylims1[0])/frac_size,
            #                    length_includes_head=True,
            #                    head_width=(xlims[1] - xlims[0])/frac_size/3,
            #                    head_length=(ylims1[1] - ylims1[0])/frac_size/3,
            #                    # width=(xlims[1] - xlims[0]) / frac_size / 15,
            #                    color=color, lw=1, zorder=-4, alpha=1)

            if pair == 0:
                # axs[col].errorbar(np.log10([EAGLE_EPS]*2), ylims,
                #               c='k', ls=':', lw=1, zorder=-10, alpha=1)
                # axs[col].errorbar(np.log10([2.8*EAGLE_EPS]*2), ylims,
                #               c='k', ls=':', lw=1, zorder=-10, alpha=1)
                axs[0][col].arrow(np.log10(EAGLE_EPS), ylims[0],
                           0, (ylims[1] - ylims[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims[1] - ylims[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color='k', fc='white', lw=1, zorder=-15, alpha=1)
                axs[1][col].arrow(np.log10(EAGLE_EPS), ylims1[0],
                           0, (ylims1[1] - ylims1[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims1[1] - ylims1[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color='k', fc='white', lw=1, zorder=-15, alpha=1)
                axs[0][col].arrow(np.log10(2.8*EAGLE_EPS), ylims[0],
                           0, (ylims[1] - ylims[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims[1] - ylims[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color='k', fc='white', lw=1, zorder=-15, alpha=1)
                axs[1][col].arrow(np.log10(2.8*EAGLE_EPS), ylims1[0],
                           0, (ylims1[1] - ylims1[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims1[1] - ylims1[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color='k', fc='white', lw=1, zorder=-15, alpha=1)

                axs[0][col].arrow(np.log10(r200), ylims[0],
                           0, (ylims[1] - ylims[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims[1] - ylims[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color='k', fc='white', lw=1, zorder=-15, alpha=1)
                axs[1][col].arrow(np.log10(r200), ylims1[0],
                           0, (ylims1[1] - ylims1[0])/frac_size,
                           length_includes_head=True,
                           head_width=(xlims[1] - xlims[0])/frac_size/3,
                           head_length=(ylims1[1] - ylims1[0])/frac_size/3,
                           # width=(xlims[1] - xlims[0]) / frac_size / 15,
                           color='k', fc='white', lw=1, zorder=-15, alpha=1)

                if col == len(mass_bin_edges) - 2:
                    axs[1][-2].text(np.log10(r200) + 0.01 * (xlims[1] - xlims[0]), ylims1[0],
                                    r'$r_{200}$',
                                    color='k', va='bottom', zorder=20, path_effects=white_path_effect_3)

            if pair == 1:
                axs[1][col].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims1[0] + 0.91 * (ylims1[1] - ylims1[0]),
                           r'$N_{\rm HR} =$' + str(N_gals[0][col]) + r', $N_{\rm LR} =$' + str(N_gals[1][col]),
                           ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

    for i in range(len(mass_bin_edges)):
        axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.91 * (ylims[1] - ylims[0]),
                    r'$\log \, M_{200} / $M$_\odot \in$[' + str(round(mass_bin_edges[i][0],2)) + ', ' +
                       str(round(mass_bin_edges[i][1],2)) + ']',
                    ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)
        axs[0][i].text(xlims[0] + 0.96 * (xlims[1] - xlims[0]), ylims[0] + 0.80 * (ylims[1] - ylims[0]),
                    r'$z=$' + str(z),
                    ha='right', bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8), zorder=7)

    axs[1][-2].text(np.log10(EAGLE_EPS) + 0.01 * (xlims[1] - xlims[0]), ylims1[0],
                    r'$\epsilon$',
                    color='k', va='bottom', zorder=20, path_effects=white_path_effect_3)
    axs[1][-2].text(np.log10(2.8 * EAGLE_EPS) + 0.01 * (xlims[1] - xlims[0]), ylims1[0],
                    r'$2.8 \times \epsilon$',
                    color='k', va='bottom', zorder=20, path_effects=white_path_effect_3)

    axs[0][-1].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                       (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                       (lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2)),
                   ],
                  ['HR stars', 'LR stars', 'halo model'], loc='lower right')

    axs[1][-1].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2)),
                      (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2)),
                   ],
                  ['heating by\nHR DM', 'heating by\nLR DM'], loc='lower right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def sigma_rad_summary(ek=False, r12_norm=False,
                      name='', save=False):
    N_res = 10

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    xlims = [1e-3 + 10, 14 - 1e-3]
    # ylims = [-0.4, 1.6]
    ylims = [np.log10(EAGLE_EPS), np.log10(EAGLE_EPS) + 4/3] #[-0.155, 1.178]
    if r12_norm:
        ylims = [-1.5, 1]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    N_run = np.linspace(9.9, 14, 42)
    N_run = np.linspace(9.85, 14.05, 29)
    N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.75, 14, 18)
    N_run = np.linspace(9+2/3, 14, 14)
    N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, r^{\rm spur} / $kpc'
    if ek:
        ylabel = r'$\log \, \sqrt{2 E_k} (r_{1/2}) / V_{200}$'
    if r12_norm:
        ylabel = r'$\log \, r_{\rm HD} / r_{1/2}$'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        if not r12_norm:
            axs[col].set_aspect(3)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for N_run in [np.linspace(9.8, 14, 22)]:
    # for N_run in [np.linspace(9.5, 14, 10)]:
        #np.linspace(9.75, 14, 18), #np.linspace(9+2/3, 14, 14), np.linspace(9.5, 14, 10)]:

        mass_bin_edges = np.array([N_run[:-1], N_run[1:]]).T
        comp='Star'

        for col, name_pairs in enumerate(combined_name_pairs):

            if '015_z002p012_' in name_pairs[0]:
                z = 2.012
                str_z = '2'
            elif '019_z001p004_' in name_pairs[0]:
                z = 1.004
                str_z = '1'
            elif '023_z000p503_' in name_pairs[0]:
                z = 0.503
                str_z = '0.5'
            else:# '028_z000p000_' in name_pairs[0]:
                z = 0
                str_z = '0'

            for pair in range(2):
                if pair == 0: # 0 (7x)
                    linestyle = '-'
                    linestyle2 = '-.'
                    marker = 'o'
                    color = 'C0'
                    color2 = 'navy'
                    color3 = 'cyan'
                else:# 1 (1x)
                    linestyle = '--'
                    linestyle2 = (0, (0.8, 0.8))
                    marker = 'o'
                    color = 'C1'
                    color2 = 'saddlebrown'
                    color3 = 'gold'

                with h5.File(name_pairs[pair], 'r') as raw_data0:

                    # centrals only
                    mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                    #load
                    M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
                    r200 = raw_data0[keys.key0_dict['r200'][None]][:]
                    sz = raw_data0[keys.key0_dict['sigma_z'][comp]][:, 0:0+len(bin_centres)]
                    sR = raw_data0[keys.key0_dict['sigma_R'][comp]][:, 0:0+len(bin_centres)]
                    sphi = raw_data0[keys.key0_dict['sigma_phi'][comp]][:, 0:0+len(bin_centres)]

                    r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
                    r14_0 = raw_data0['GalaxyQuantities/StellarQuarterMassRadius'][()]
                    r34_0 = raw_data0['GalaxyQuantities/Stellar3QuarterMassRadius'][()]

                    N0 = raw_data0[keys.key0_dict['Npart'][comp]][:, 0:0+len(bin_centres)]

                    #add kinetic energy because that's what Arron has done
                    if ek:
                        vphi = raw_data0[keys.key0_dict['mean_v_phi'][comp]][:, 0:0+len(bin_centres)]

                    #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                    half_age0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['r200', None], 2]

                    #mask and units
                    V200 = np.sqrt(GRAV_CONST * M200_0 / r200) # km/s
                    m0 = np.log10(M200_0[mask0]) + 10 # log Mstar
                    if ek:
                        # y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2 +
                        #                       vphi[mask0]**2) / V200[mask0, np.newaxis])

                        y0 = np.log10(np.sqrt(2 * raw_data0[f'GalaxyProfiles/{comp}/KineticEnergy'][:,
                                                  0:0+len(bin_centres)][mask0] /
                                      raw_data0[f'GalaxyProfiles/{comp}/BinMass'][:,
                                                  0:0+len(bin_centres)][mask0]) / V200[mask0, np.newaxis])

                    else:
                        y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2) / V200[mask0, np.newaxis])

                    N0 = N0[mask0]
                    half_age0 = half_age0[mask0]
                    r12_0 = r12_0[mask0]
                    r14_0 = r14_0[mask0]
                    r34_0 = r34_0[mask0]
                    M200_0 = M200_0[mask0]

                log_r_hrs = np.zeros(len(mass_bin_edges))
                # if not half_from_size_mass:
                r12s = np.zeros(len(mass_bin_edges))
                r14s = np.zeros(len(mass_bin_edges))
                r34s = np.zeros(len(mass_bin_edges))

                r12p = np.zeros(len(mass_bin_edges))
                r14p = np.zeros(len(mass_bin_edges))
                r34p = np.zeros(len(mass_bin_edges))

                r12m = np.zeros(len(mass_bin_edges))
                r14m = np.zeros(len(mass_bin_edges))
                r34m = np.zeros(len(mass_bin_edges))

                for mass_index, mass_range in enumerate(mass_bin_edges):

                    #mask m200 bin
                    mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
                    y = y0[mask]
                    N = N0[mask]
                    half_age = half_age0[mask]
                    r12 = r12_0[mask]
                    r14 = r14_0[mask]
                    r34 = r34_0[mask]
                    # M200 = M200_0[mask]
                    count = np.sum(mask)

                    median_age = np.nanmedian(half_age) - keys.lbt(np.array([1/(1+z)]))[0]

                    # theory
                    MassDM = DM_MASS
                    if pair == 0:
                        MassDM /= 7

                    H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
                    rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                    rho_200 = 200 * rho_crit

                    M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
                    r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
                    V200 = np.sqrt(GRAV_CONST * M200 * 1e-10 / r200) #km/s

                    radii = np.logspace(np.log10(keys.log_bin_edges[0]), np.log10(keys.log_bin_edges[-1]))

                    if np.log10(M200) > 12.5:
                        profile = 'hern'
                    else:
                        profile = 'exp'

                    (analytic_v_circ, analytic_dispersion,
                     theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                     stellar_dispersion, v_c_star
                     ) = mgm.get_heated_profile(radii, M200 * 1e-10, MassDM=MassDM, z=z, time=median_age, ic_heat_fraction=0,
                                                contracted=True, profile=profile)

                    sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)

                    #for limit of resolved data
                    heating_limit = interp1d(np.log10(radii), np.log10(sigma_tot / V200), fill_value='extrapolate')

                    #measurements
                    # #size mask?
                    # fit_r12 = 10 ** size_function(size_function_args(z, 1-pair), np.mean(mass_range))  # kpc
                    # r_mask = np.logical_and(r12 > 0.9 * fit_r12, r12 < 1.1 * fit_r12)
                    # y = y[r_mask]
                    # N = N[r_mask]
                    # half_age = half_age[r_mask]
                    # r12 = r12[r_mask]
                    # M200 = M200[r_mask]

                    # calc medians etc.
                    iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                    ymedians = np.zeros(iiin)
                    yplus = np.zeros(iiin)
                    yminus = np.zeros(iiin)

                    N_mask = N > N_res
                    for iii in range(iiin):
                        ys = y[:, iii][N_mask[:, iii]]

                        ymedians[iii] = np.nanmedian(ys)
                        # ymedians[j] = np.nanmean(ys)
                        yplus[iii] = np.nanquantile(ys, 0.84)
                        yminus[iii] = np.nanquantile(ys, 0.16)

                    #for converge radii
                    if np.sum(np.isfinite(ymedians)) < 2:
                        r_intercept = np.nan #np.log10(EAGLE_EPS)
                    else:
                        min_radii = bin_centres[np.isfinite(ymedians)][0]

                        y_med_interp = interp1d(bin_centres[np.isfinite(ymedians)],
                                                ymedians[np.isfinite(ymedians)], fill_value='extrapolate')

                        heated_intercept_func = lambda radii: y_med_interp(radii) - heating_limit(radii)
                        try:
                            # r_intercept = brentq(heated_intercept_func, min_radii, 4)
                            r_intercept = brentq(heated_intercept_func, np.log10(EAGLE_EPS), 4)
                        except (ValueError, RuntimeError) as e:
                            if mass_range[0] > 10.5:
                                r_intercept = np.log10(EAGLE_EPS)
                            else:
                                r_intercept = np.nan
                    # print(str_z, 1-pair, mass_range, r_intercept)

                    # axs[col].scatter(np.mean(mass_range), r_intercept,
                    #                  c=color, linewidth=1, zorder=3, alpha=1)

                    log_r_hrs[mass_index] = r_intercept

                    # if not half_from_size_mass:
                        #save measured r12s
                    r12s[mass_index] = np.nanmedian(r12)
                    r14s[mass_index] = np.nanmedian(r14)
                    r34s[mass_index] = np.nanmedian(r34)

                    r12p[mass_index] = np.nanquantile(r12, 0.84)
                    r14p[mass_index] = np.nanquantile(r14, 0.84)
                    r34p[mass_index] = np.nanquantile(r34, 0.84)

                    r12m[mass_index] = np.nanquantile(r12, 0.16)
                    r14m[mass_index] = np.nanquantile(r14, 0.16)
                    r34m[mass_index] = np.nanquantile(r34, 0.16)

                    if count < 25 * np.diff(N_run)[0]:
                        r12s[mass_index] = np.nan
                        r14s[mass_index] = np.nan
                        r34s[mass_index] = np.nan

                if not r12_norm:
                    # print(*[f'{_r:0.5},' for _r in log_r_hrs])

                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1), log_r_hrs,
                                      c=color, ls=linestyle, linewidth=1, zorder=4.5, alpha=1, path_effects=path_effect_2)
                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1)[np.logical_not(np.isclose(log_r_hrs, np.log10(EAGLE_EPS)))],
                                      log_r_hrs[np.logical_not(np.isclose(log_r_hrs, np.log10(EAGLE_EPS)))],
                                      c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1, path_effects=path_effect_4)

                    # if half_from_size_mass:
                    log_M200s = np.linspace(xlims[0], xlims[1])
                    log_r12s = size_function(size_function_args(z, 1-pair), log_M200s) #kpc

                    # axs[col].errorbar(log_M200s, log_r12s,
                    #                   c=color2, ls=linestyle2, linewidth=1, zorder=-2, alpha=1)
                    # else:
                    print(np.array([r12m, r12s, r12p]).T)
                    print()
                    
                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(r12s),
                                      # [np.log10(r12s/r12m), np.log10(r12p/r12s)],
                                      c=color, ls=linestyle, linewidth=2, zorder=-1, alpha=1)#, path_effects=path_effect_4)
                                      # c=color, mfc='white', mec=color, ls='', marker='o', markersize=4, zorder=-1)
                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(r14s),
                                      # [np.log10(r14s / r14m), np.log10(r14p / r14s)],
                                      c=color, ls=linestyle, linewidth=2, zorder=-1, alpha=1)#, path_effects=path_effect_4)
                                      # c=color, mfc='white', mec=color, ls='', marker='v', markersize=4, zorder=-1)
                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(r34s),
                                      # [np.log10(r34s/r34m), np.log10(r34p/r34s)],
                                      c=color, ls=linestyle, linewidth=2, zorder=-1, alpha=1)#, path_effects=path_effect_4)
                                      # c=color, mfc='white', mec=color, ls='', marker='^', markersize=4, zorder=-1)

                else: #if r12_norm:
                    log_r12s = size_function(size_function_args(z, 1-pair), np.mean(mass_bin_edges, axis=1)) #kpc

                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1), log_r_hrs - log_r12s,
                                      c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1, path_effects=path_effect_4)

                    if pair == 1:
                        axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(EAGLE_EPS) - log_r12s,
                                          c='k', ls=':', lw=1, zorder=-10)

            #decorations
            m200_intercept_0 = m200_heating_intercept(z, 1)
            m200_intercept_1 = m200_heating_intercept(z, 0)

            axs[col].axvline(m200_intercept_1, c='C1', ls=(0,(0.8,0.8)), linewidth=1, zorder=-3, alpha=1)
            axs[col].axvline(m200_intercept_0, c='C0', ls='-.', linewidth=1, zorder=-3, alpha=1)

            #guess
            log_M200s = np.linspace(*xlims, 101)  # log M_sun
            # cosmology
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200 = 200 * rho_crit
            r200 = np.power(3 * 10 ** (log_M200s - 10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
            V200 = np.sqrt(GRAV_CONST * 10 ** (log_M200s - 10) / r200)  # km/s

            # #z=0
            # rho_200 = 2.548683784984306e-06

            g = lambda c: c ** 2 / (3 * (np.log(1 + c) - c / (1 + c)))
            conc = mgm.concentration_ludlow(z, 10 ** (log_M200s - 10))

            if False:
                pass
                #TODO

            # norm = 10**7.25
            # alpha = 0.37
            #
            #
            #
            # axs[col].errorbar(log_M200s, np.log10(r_est),
            #                   c='C1', ls='-', lw=1, zorder=-10)
            #
            # r_est = np.power(((UNIVERSE_AGE - mgm.lbt(np.array([1 / (1 + z)]))) / 2) * GRAV_CONST ** (1 / 2) *
            #                7e-1 * (g(conc) / 22)**(1 + alpha) * (rho_200)**(1/6 - alpha/3) * (DM_MASS/7)**(1) /
            #                (10**log_M200s) ** ((2 - alpha)/3) * 10**(10 - ((2 - alpha)/3) + (1/6 - alpha/3)),
            #                1/(1+alpha)/slope)
            #
            # inputs = [7.25,
            #           1 / (1 + alpha),
            #           1,
            #           (alpha / 3),  # (1/6 - alpha/3)/(1 + alpha)
            #           1 / (1 + alpha),
            #           (-1 / 2 - alpha / 3)]  # (-2/3 - alpha/3)/(1 + alpha)]
            #
            # r_est = lambda const, t_power, g_power, rho_power, m_power, M_power, age, g_conc, rho_200, mDM,
            #                log_M200: np.log10(
            #     10 ** const * age ** t_power * GRAV_CONST ** (t_power / 2) * g_conc ** g_power * rho_200 ** rho_power *
            #     mDM ** m_power * (10 ** log_M200) ** M_power)
            #
            #
            #
            #
            # axs[col].errorbar(log_M200s, np.log10(r_est),
            #                   c='C0', ls='-', lw=1, zorder=-10)

            if not r12_norm:
                pass
                # for i in range(20):
                #     # 1/3 Mo, Mao, White style.
                #     slope = 1/3
                #     axs[col].errorbar(xlims, [i*slope + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * slope,
                #                               i*slope + np.floor(ylims[0])],
                #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)
                #
                #     slope = -1/2
                #     axs[col].errorbar(xlims, [i*slope + np.floor(ylims[1]) - (xlims[1] - xlims[0])*slope,
                #                               i*slope + np.floor(ylims[1])],
                #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

            else: #if r12_norm:
                axs[col].axhline(0, 0, 1,
                                 c='k', ls=':', lw=1, zorder=-10)

            if not r12_norm:
                frac_size = 8
                # axs[col].arrow(xlims[0], np.log10(EAGLE_EPS),
                #                (xlims[1] - xlims[0])/frac_size, 0,
                #            length_includes_head=True,
                #            head_width=1/frac_size/2,
                #            head_length=1/frac_size,
                #            color='k', fc='white', lw=1, zorder=-10, alpha=1)
                axs[col].arrow(xlims[1] -(xlims[1] - xlims[0]) / frac_size, np.log10(2.8*EAGLE_EPS),
                               -(xlims[1] - xlims[0])/frac_size, 0,
                           length_includes_head=True,
                           head_width=1/frac_size/2,
                           head_length=1/frac_size,
                           color='k', fc='white', lw=1, zorder=-10, alpha=1)


            if not r12_norm:
                dy = 0.06
                va='bottom'
            else:
                dy = 0.95
                va = 'top'
            axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                        r'$z=$' + str_z, ha='right', va=va, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

        find_relation = False
        if find_relation:
            log_M200s = np.array([10.25, 10.75, 11.25, 11.75, 12.25])
            log_M200ss = [log_M200s, log_M200s, log_M200s, log_M200s, log_M200s, log_M200s, log_M200s, log_M200s]

            data_HR0 = np.array([0.61414729, 0.48824306, 0.17530408, -0.15490196, np.nan])
            data_LR0 = np.array([np.nan,  0.97299522,  0.74404189,  0.41903483, 0.03228173])
            data_HR05 = np.array([0.48689, 0.27481, -0.13458, np.nan, np.nan])
            data_LR05 = np.array([0.94033, 0.78332, 0.47693, 0.10348, -0.1549])
            data_HR1 = np.array([0.37373, 0.088466, -0.1549, np.nan, np.nan])
            data_LR1 = np.array([0.81023, 0.6131, 0.26264, -0.12294, np.nan])
            data_HR2 = np.array([0.14714, -0.1549, -0.45, np.nan, np.nan])
            data_LR2 = np.array([0.6295, 0.3568, -0.081177, np.nan, np.nan])
            data = [data_HR0, data_HR05, data_HR1, data_HR2, data_LR0, data_LR05, data_LR1, data_LR2]

            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(0 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_0 = rho_crit #200 * rho_crit
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(0.5 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_05 = rho_crit #200 * rho_crit
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(1 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_1 = rho_crit #200 * rho_crit
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(2 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_2 = rho_crit #200 * rho_crit
            rho_200s = [rho_200_0, rho_200_05, rho_200_1, rho_200_2, rho_200_0, rho_200_05, rho_200_1, rho_200_2]

            g = lambda c: c ** 2 / (3 * (np.log(1 + c) - c / (1 + c)))
            conc_0 = mgm.concentration_ludlow(0, 10 ** (log_M200s - 10))
            conc_05 = mgm.concentration_ludlow(0.5, 10 ** (log_M200s - 10))
            conc_1 = mgm.concentration_ludlow(1, 10 ** (log_M200s - 10))
            conc_2 = mgm.concentration_ludlow(2, 10 ** (log_M200s - 10))
            # concs = [conc_0, conc_05, conc_1, conc_2, conc_0, conc_05, conc_1, conc_2]
            g_concs = [g(conc_0), g(conc_05), g(conc_1), g(conc_2), g(conc_0), g(conc_05), g(conc_1), g(conc_2)]

            ages = (UNIVERSE_AGE - mgm.lbt(1 / (1 + np.array([0, 0.5, 1, 2])))) / 2
            age_0, age_05, age1, age2 = ages
            ages = [age_0, age_05, age1, age2, age_0, age_05, age1, age2]

            masses = [DM_MASS/7, DM_MASS/7, DM_MASS/7, DM_MASS/7, DM_MASS, DM_MASS, DM_MASS, DM_MASS]

            # r_est = lambda const, t_power, G_power, g_power, rho_power, m_power, M_power, age, g_conc, rho_200, mDM, M200: np.log10(
            #         const * age**t_power * GRAV_CONST**G_power * g_conc**g_power * rho_200**rho_power *
            #         mDM**m_power * M200**M_power)
            # r_min = lambda args: np.nansum(
            #     [(data_i - r_est(*args, age_i, g_conc_i, rho_200_i, mDM_i, M200_i))**2
            #      for data_i, age_i, g_conc_i, rho_200_i, mDM_i, M200_i in zip(
            #         data, ages, g_concs, rho_200s, masses, log_M200ss)])

            r_est = lambda const, t_power, G_power, g_power, rho_power, m_power, M_power, age, g_conc, rho_200, mDM, log_M200: np.log10(
                    10**const * age**t_power * GRAV_CONST**G_power * g_conc**g_power * rho_200**rho_power *
                    mDM**m_power * (10**(log_M200-1)) ** M_power * GYR_ON_S / PC_ON_M)
            # r_min = lambda args: np.nansum(
            #     [(data_i - r_est(*args, age_i, g_conc_i, rho_200_i, mDM_i, log_M200_i))**2
            #      for data_i, age_i, g_conc_i, rho_200_i, mDM_i, log_M200_i in zip(
            #         data, ages, g_concs, rho_200s, masses, log_M200ss)])

            def r_min(args):

                a_1 = args[2]/2 #time units
                a_4 = args[2] - 1/3 #length units
                a_5 = args[2] - a_4 - args[6] #mass units

                out = np.nansum([(data_i - r_est(args[0],
                                                 a_1,
                                                 args[2],
                                                 args[3],
                                                 a_4,
                                                 a_5,
                                                 args[6],
                                                 age_i, g_conc_i, rho_200_i, mDM_i, log_M200_i))**2
                    for data_i, age_i, g_conc_i, rho_200_i, mDM_i, log_M200_i in zip(
                        data, ages, g_concs, rho_200s, masses, log_M200ss)])
                return out

            alpha = -0.30

            # const,
            # t_power,
            # G_power,
            # g_power,
            # rho_power,
            # m_power,
            # M_power
            inputs = [14,
                      1/(1 + alpha),
                      1/(2 * (1 + alpha)),
                      1,
                      (1/6 - alpha/3)/(1 + alpha), #(1/6 - alpha/3)/(1 + alpha)
                      1/(1 + alpha),
                      (-2/3 + alpha/3)/(1 + alpha)] #(-2/3 - alpha/3)/(1 + alpha)]

            # inputs = [2.0,
            #           0.625,
            #           1.25,
            #           3,
            #           1.25 - 1/3,
            #           0.25 + 1/3,
            #           -0.25]

            for inp in inputs:
                print(inp)

            #plot input
            if True:
                for i in range(4):
                    axs[i].errorbar(log_M200s, r_est(*inputs, ages[i], g_concs[i], rho_200s[i], DM_MASS, log_M200s),
                                      c='C1', ls=':', lw=3, zorder=-10)
                    axs[i].errorbar(log_M200s, r_est(*inputs, ages[i], g_concs[i], rho_200s[i], DM_MASS/7, log_M200s),
                                      c='C0', ls=':', lw=3, zorder=-10)

                    print(r_est(*inputs, ages[i], g_concs[i], rho_200s[i], DM_MASS, log_M200s))

            out = minimize(r_min, inputs, method = 'Powell')

            print(out.message)

            out.x[4] = out.x[2] - 1 / 3  # length units
            out.x[5] = out.x[2] - out.x[4] - out.x[6]  # mass units
            out.x[1] = out.x[2] / 2  # time units

            for outx in out.x:
                print(outx)

            if False:
                for i in range(4):
                    axs[i].errorbar(log_M200s, r_est(*out.x, ages[i], g_concs[i], rho_200s[i], DM_MASS, log_M200s),
                                    c='C1', ls='--', lw=3, zorder=-10)
                    axs[i].errorbar(log_M200s, r_est(*out.x, ages[i], g_concs[i], rho_200s[i], DM_MASS / 7, log_M200s),
                                    c='C0', ls='--', lw=3, zorder=-10)

            r_min_alpha = lambda args: np.nansum(
                [(data_i - r_est(args[0],
                                 1/(1 + args[1]),
                                 1/(2 * (1 + args[1])),
                                 1, #1 + args[1], #1,
                                 (1/6 - args[1]/3)/(1 + args[1]), #(1/6 - args[1]/3)/(1 + args[1]), #args[2],
                                 1/(1 + args[1]),
                                 (-2/3 + args[1]/3)/(1 + args[1]), #(-2/3 + args[1]/3)/(1 + args[1]), #args[2],
                  age_i, g_conc_i, rho_200_i, mDM_i, log_M200_i)) ** 2
                 for data_i, age_i, g_conc_i, rho_200_i, mDM_i, log_M200_i in zip(
                    data, ages, g_concs, rho_200s, masses, log_M200ss)])

            out = minimize(r_min_alpha, (6, alpha, 0))#, method = 'Powell')

            print(out.message)
            for outx in out.x:
                print(outx)

            #done with optimising, better resolution for line
            log_M200s = np.linspace(*xlims)
            conc_0 = mgm.concentration_ludlow(0, 10 ** (log_M200s - 10))
            conc_05 = mgm.concentration_ludlow(0.5, 10 ** (log_M200s - 10))
            conc_1 = mgm.concentration_ludlow(1, 10 ** (log_M200s - 10))
            conc_2 = mgm.concentration_ludlow(2, 10 ** (log_M200s - 10))
            g_concs = [g(conc_0), g(conc_05), g(conc_1), g(conc_2)]

            for i in range(4):
                axs[i].errorbar(log_M200s, r_est(out.x[0],
                                                 1/(1 + out.x[1]),
                                                 1/(2 * (1 + out.x[1])),
                                                 1, #1 + out.x[1], #1,
                                                 (1/6 - out.x[1]/3)/(1 + out.x[1]), #(1/6 - out.x[1]/3)/(1 + out.x[1]),
                                                 1/(1 + out.x[1]),
                                                 (-2/3 + out.x[1]/3)/(1 + out.x[1]), #(-2/3 + out.x[1]/3)/(1 + out.x[1]),
                                                 ages[i], g_concs[i], rho_200s[i], DM_MASS, log_M200s),
                                c='C1', ls='-', lw=3, zorder=-10)
                axs[i].errorbar(log_M200s, r_est(out.x[0],
                                                 1/(1 + out.x[1]),
                                                 1/(2 * (1 + out.x[1])),
                                                 1, #1 + out.x[1], #1,
                                                 (1/6 - out.x[1]/3)/(1 + out.x[1]), #(1/6 - out.x[1]/3)/(1 + out.x[1]),
                                                 1/(1 + out.x[1]),
                                                 (-2/3 + out.x[1]/3)/(1 + out.x[1]), #(-2/3 + out.x[1]/3)/(1 + out.x[1]),
                                                 ages[i], g_concs[i], rho_200s[i], DM_MASS / 7, log_M200s),
                                c='C0', ls='-', lw=3, zorder=-10)

                # zs = [0, 0.503, 1.004, 2.012]
                # axs[i].errorbar(log_M200s, -0.6*log_M200s + 8 - (1+zs[i])**(2),
                #                 c='grey', ls=':', lw=1, zorder=-20)
                # print(-0.6*log_M200s + 8 - (1+zs[i])**(2))

            # for i in range(4):
            #     axs[i].errorbar(log_M200s, r_est(*out.x, ages[i], g_concs[i], rho_200s[i], DM_MASS, log_M200s),
            #                       c='C1', ls='-', lw=1, zorder=-10)
            #     axs[i].errorbar(log_M200s, r_est(*out.x, ages[i], g_concs[i], rho_200s[i], DM_MASS/7, log_M200s),
            #                       c='C0', ls='-', lw=1, zorder=-10)

        r_model = True
        if r_model:

            # log_M200s = np.array([10.25, 10.75, 11.25, 11.75, 12.25])
            log_M200s = np.linspace(*xlims)

            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(0 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_0 = rho_crit #200 * rho_crit
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(0.5 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_05 = rho_crit #200 * rho_crit
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(1 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_1 = rho_crit #200 * rho_crit
            H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(2 + 1, 3))  # km^2/s^2 / kpc^2
            rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
            rho_200_2 = rho_crit #200 * rho_crit
            rho_200s = [rho_200_0, rho_200_05, rho_200_1, rho_200_2, rho_200_0, rho_200_05, rho_200_1, rho_200_2]

            g = lambda c: c ** 2 / (3 * (np.log(1 + c) - c / (1 + c)))
            conc_0 = mgm.concentration_ludlow(0, 10 ** (log_M200s - 10))
            conc_05 = mgm.concentration_ludlow(0.5, 10 ** (log_M200s - 10))
            conc_1 = mgm.concentration_ludlow(1, 10 ** (log_M200s - 10))
            conc_2 = mgm.concentration_ludlow(2, 10 ** (log_M200s - 10))
            # concs = [conc_0, conc_05, conc_1, conc_2, conc_0, conc_05, conc_1, conc_2]
            g_concs = [g(conc_0), g(conc_05), g(conc_1), g(conc_2), g(conc_0), g(conc_05), g(conc_1), g(conc_2)]

            ages = (UNIVERSE_AGE - mgm.lbt(1 / (1 + np.array([0, 0.5, 1, 2])))) / 2
            age_0, age_05, age1, age2 = ages
            ages = [age_0, age_05, age1, age2, age_0, age_05, age1, age2]

            masses = [DM_MASS/7, DM_MASS/7, DM_MASS/7, DM_MASS/7, DM_MASS, DM_MASS, DM_MASS, DM_MASS]

            r_est = lambda const, t_power, G_power, g_power, rho_power, m_power, M_power, age, g_conc, rho_200, mDM, log_M200: np.log10(
                    10**const * age**t_power * GRAV_CONST**G_power * g_conc**g_power * rho_200**rho_power *
                    mDM**m_power * (10**(log_M200-1)) ** M_power * GYR_ON_S / PC_ON_M)

            alpha = -0.3
            # consts
            # t_power,
            # G_power,
            # g_power,
            # rho_power,
            # m_power,
            # M_power
            inputs = [4.182,
                      1/(1 + alpha),
                      1/(2 * (1 + alpha)),
                      1,
                      (1/6 - alpha/3)/(1 + alpha), #(1/6 - alpha/3)/(1 + alpha)
                      1/(1 + alpha),
                      (-2/3 + alpha/3)/(1 + alpha)] #(-2/3 - alpha/3)/(1 + alpha)]

            inputs = [2.0,
                      0.625,
                      1.25,
                      3,
                      1.25 - 1/3,
                      0.25 + 1/3,
                      -0.25]

            for inp in inputs:
                print(inp)

            #plot input
            for i in range(4):
                # axs[i].errorbar(log_M200s, r_est(*inputs, ages[i], g_concs[i], rho_200s[i], DM_MASS, log_M200s),
                #                   c='C1', ls='-', lw=1, zorder=-10)
                # axs[i].errorbar(log_M200s, r_est(*inputs, ages[i], g_concs[i], rho_200s[i], DM_MASS/7, log_M200s),
                #                   c='C0', ls='-', lw=1, zorder=-10)

                #slope

                zs = [0, 0.503, 1.004, 2.012]
                lb = 0 #0.3 #3.3 #9.3
                axs[i].errorbar(log_M200s, lb + 0.6*np.log10(10**5.45 * DM_MASS / 10**(log_M200s-10))
                                -1.5 * np.log10(1+zs[i]),
                                c='C1', ls=':', lw=2, zorder=-20)
                axs[i].errorbar(log_M200s, lb + 0.6*np.log10(10**5.45 * DM_MASS / 7 /  10**(log_M200s-10))
                                -1.5 * np.log10(1+zs[i]),
                                c='C0', ls=':', lw=2, zorder=-20)
                # print(lb + 0.6*np.log10(10**5.45 * DM_MASS / 10**(log_M200s-10)) -1.5 * np.log10(1+zs[i]))

                NlogN = False
                if NlogN:
                    c = 4.4
                    N = 10**(log_M200s-10) / DM_MASS #* 10**5
                    axs[i].errorbar(log_M200s, np.log10(10**c * np.log10(N) / N)
                                    -2 * np.log10(1+zs[i]),
                                    c='C1', ls='-', lw=2, zorder=-20)
                    N = 10**(log_M200s-10) / (DM_MASS / 7) #* 10**5
                    axs[i].errorbar(log_M200s, np.log10(10**c * np.log10(N) / N)
                                    -2 * np.log10(1+zs[i]),
                                    c='C0', ls='-', lw=2, zorder=-20)
                    print(np.log10(10**c * np.log10(N) / N))

            # zs = [0, 0.503, 1.004, 2.012]
            # index = 4
            # axs[3].text(log_M200s[index] + 0.01 * (xlims[1] - xlims[0]),
            #             lb + 0.6*np.log10(10**5.4 * DM_MASS / 10**(log_M200s[index]-10)) -1.5 * np.log10(1+zs[i]),
            #             r'$r = (10^{5.4} * m_{\rm DM} / M_{200})^{0.6} (1 + z)^{1.5}$',
            #             rotation_mode='anchor', ha='left', va='bottom',
            #             rotation=(np.arctan(-0.6 * np.diff(xlims) / np.diff(ylims))*180/np.pi)[0],
            #             color='k', path_effects=white_path_effect_3,)
            #             # bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

        axs[3].text(xlims[1] - 1.55 * (xlims[1] - xlims[0]) / frac_size,
                    np.log10(2.8 * EAGLE_EPS) + 0.0 * (ylims[1] - ylims[0]),
                    r'$2.8 \times \epsilon$', va='bottom')

        axs[3].text(xlims[0] + 0.82 * (xlims[1] - xlims[0]), 0.1,  #ylims[0] + 0 * (ylims[1] - ylims[0]),
                    r'$r_{1/4}$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.78 * (xlims[1] - xlims[0]), 0.525,  #ylims[0] + 0 * (ylims[1] - ylims[0]),
                    r'$r_{1/2}$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))
        axs[3].text(xlims[0] + 0.74 * (xlims[1] - xlims[0]), 0.95,  #ylims[0] + 0 * (ylims[1] - ylims[0]),
                    r'$r_{3/4}$', ha='right')#, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def vc_rad_summary(ek=False, r12_norm=False,
                      name='', save=False):
    N_res = 2

    fig = plt.figure(constrained_layout=True)
    # fig.set_size_inches(4 * len(combined_name_pairs), 4.1, forward=True)
    fig.set_size_inches(3.3 * 2, 3.3 * 2 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=2, nrows=2)

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [np.log10(EAGLE_EPS), np.log10(EAGLE_EPS) + 4/3] #[-0.155, 1.178]
    if r12_norm:
        ylims = [-1.5, 1]

    # xlims = [1e-3 + 8, 14 - 1e-3]
    # ylims = [-2/3, 4/3]

    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9+2/3, 14, 14)
    N_run = np.linspace(9.5, 14, 10)

    mass_bin_edges = np.array([N_run[:-1], N_run[1:]]).T

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'$\log \, r_{\rm HD} / $kpc'
    if ek:
        ylabel = r'$\log \, \sqrt{2 E_k} (r_{1/2}) / V_{200}$'
    if r12_norm:
        ylabel = r'$\log \, r_{\rm HD} / r_{1/2}$'

    axs = []
    for col in range(len(combined_name_pairs)):
        axs.append(fig.add_subplot(spec[col // 2, col % 2]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if (col % 2) == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        if (col // 2) == 1: axs[col].set_xlabel(xlabel)
        else: axs[col].set_xticklabels([])
        if not r12_norm:
            axs[col].set_aspect(3)
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    comp='Star'


    for col, name_pairs in enumerate(combined_name_pairs):
        vc_interpolator_holder = [[], []]

        if '015_z002p012_' in name_pairs[0]:
            z = 2.012
            str_z = '2'
        elif '019_z001p004_' in name_pairs[0]:
            z = 1.004
            str_z = '1'
        elif '023_z000p503_' in name_pairs[0]:
            z = 0.503
            str_z = '0.5'
        else:# '028_z000p000_' in name_pairs[0]:
            z = 0
            str_z = '0'

        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                color3 = 'cyan'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                color3 = 'gold'

            with h5.File(name_pairs[pair], 'r') as raw_data0:

                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0

                #load
                M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
                Mstar_tot_0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
                r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]
                sz = raw_data0[keys.key0_dict['sigma_z'][comp]][:, 0:0+len(bin_centres)]
                sR = raw_data0[keys.key0_dict['sigma_R'][comp]][:, 0:0+len(bin_centres)]
                sphi = raw_data0[keys.key0_dict['sigma_phi'][comp]][:, 0:0+len(bin_centres)]

                r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
                r14_0 = raw_data0['GalaxyQuantities/StellarQuarterMassRadius'][()]
                r34_0 = raw_data0['GalaxyQuantities/Stellar3QuarterMassRadius'][()]

                N0 = raw_data0[keys.key0_dict['Npart'][comp]][:, 0:0+len(bin_centres)]

                #add kinetic energy because that's what Arron has done
                if ek:
                    vphi = raw_data0[keys.key0_dict['mean_v_phi'][comp]][:, 0:0+len(bin_centres)]

                #final 0, 1, 2, 3, 4 are 90, 75, 50, 25, 10 star particle age percentile
                half_age0 = raw_data0[keys.key0_dict['age'][None]][:, keys.rkey_to_rcolum_dict['r200', None], 2]

                mstar_0 = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0 + len(bin_centres)]  # 10^10 M_sun
                mstar_0 = np.cumsum(mstar_0, axis=1)[mask0, :]
                mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0 + len(bin_centres)]
                mgas = np.cumsum(mgas, axis=1)[mask0, :]
                mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0 + len(bin_centres)]
                mdm = np.cumsum(mdm, axis=1)[mask0, :]

                # mbh = raw_data0['GalaxyQuantities/BHBinGravMass'][()] # 10^10 M_sun
                mbh = raw_data0['GalaxyQuantities/BHGravMass'][()]  # 10^10 M_sun
                mbh = mbh[mask0]

                mtot = mdm + mgas + mstar_0 + mbh[:, np.newaxis]

                #mask and units
                V200_0 = np.sqrt(GRAV_CONST * M200_0 / r200_0) # km/s
                m0 = np.log10(M200_0[mask0]) + 10 # log Mstar
                if ek:
                    # y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2 +
                    #                       vphi[mask0]**2) / V200[mask0, np.newaxis])

                    y0 = np.log10(np.sqrt(2 * raw_data0[f'GalaxyProfiles/{comp}/KineticEnergy'][:,
                                              0:0+len(bin_centres)][mask0] /
                                  raw_data0[f'GalaxyProfiles/{comp}/BinMass'][:,
                                              0:0+len(bin_centres)][mask0]) / V200_0[mask0, np.newaxis])

                else:
                    y0 = np.log10(np.sqrt(sz[mask0] ** 2 + sR[mask0] ** 2 + sphi[mask0] ** 2) / V200_0[mask0, np.newaxis])

                N0 = N0[mask0]
                half_age0 = half_age0[mask0]
                r12_0 = r12_0[mask0]
                r14_0 = r14_0[mask0]
                r34_0 = r34_0[mask0]
                M200_0 = M200_0[mask0]
                V200_0 = V200_0[mask0]
                Mstar_tot_0 = Mstar_tot_0[mask0]

                vstar_0 = np.sqrt(GRAV_CONST * mstar_0 / 10 ** bin_centres[np.newaxis, :])
                vgas_0 = np.sqrt(GRAV_CONST * mgas / 10 ** bin_centres[np.newaxis, :])
                vdm_0 = np.sqrt(GRAV_CONST * mdm / 10 ** bin_centres[np.newaxis, :])

                vstar_0 = np.log10(vstar_0 / V200_0[:, np.newaxis])
                vgas_0 = np.log10(vgas_0 / V200_0[:, np.newaxis])
                vdm_0 = np.log10(vdm_0 / V200_0[:, np.newaxis])

            log_r_hrs = np.zeros(len(mass_bin_edges))
            # if not half_from_size_mass:
            r12s = np.zeros(len(mass_bin_edges))
            r14s = np.zeros(len(mass_bin_edges))
            r34s = np.zeros(len(mass_bin_edges))

            for mass_index, mass_range in enumerate(mass_bin_edges):

                #mask m200 bin
                mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])

                if np.sum(mask) == 0:
                    vc_interpolator_holder[pair].append(lambda x: np.nan)

                    log_r_hrs[mass_index] = np.nan

                    r12s[mass_index] = np.nan
                    r14s[mass_index] = np.nan
                    r34s[mass_index] = np.nan

                    continue

                y = y0[mask]
                N = N0[mask]
                half_age = half_age0[mask]
                r12 = r12_0[mask]
                r14 = r14_0[mask]
                r34 = r34_0[mask]

                M200 = M200_0[mask]
                Mstar_tot = Mstar_tot_0[mask]

                vstar = vstar_0[mask]
                vgas = vgas_0[mask]
                vdm = vdm_0[mask]

                mstar = mstar_0[mask]

                median_age = np.nanmedian(half_age) - keys.lbt(np.array([1/(1+z)]))[0]

                # theory
                MassDM = DM_MASS
                if pair == 0:
                    MassDM /= 7

                H_z2 = HUBBLE_CONST ** 2 * ((1-Omega_m) + Omega_m * np.power(z + 1, 3))
                rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
                rho_200 = 200 * rho_crit

                # M200 = 10**(mass_range[0] + (mass_range[1] - mass_range[0])/2) #M_sun
                M200 = np.mean(M200) * 1e10 #M_sun
                r200 = np.power(3 * M200 * 1e-10 / (4 * np.pi * rho_200), 1 / 3)  # kpc
                V200 = np.sqrt(GRAV_CONST * M200 * 1e-10 / r200) #km/s

                radii = np.logspace(np.log10(keys.log_bin_edges[0]), np.log10(keys.log_bin_edges[-1]))

                if np.log10(M200) > 12.5:
                    profile = 'hern'
                else:
                    profile = 'exp'

                (analytic_v_circ, analytic_dispersion,
                 theory_best_v, theory_best_z, theory_best_r, theory_best_p,
                 stellar_dispersion, v_c_star
                 ) = mgm.get_heated_profile(radii, M200 * 1e-10, MassDM=MassDM, z=z, time=median_age, ic_heat_fraction=0,
                                            contracted=True, Mstar=np.mean(Mstar_tot), hr_size=True)
                #r12=np.median(r12), profile=profile)

                # sigma_tot = np.sqrt(theory_best_z**2 + theory_best_r**2 + theory_best_p**2)

                #for limit of resolved data
                heating_limit = interp1d(np.log10(radii), theory_best_v, fill_value='extrapolate')

                #measurements
                # calc medians etc.
                iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                vcmedians = np.zeros(iiin)

                for iii in range(iiin):
                    vcmedians[iii] = np.sqrt(GRAV_CONST * np.mean(mstar[:, iii]) / 10 ** bin_centres[iii]) #km/s

                #for converge radii
                if np.sum(np.isfinite(vcmedians)) < 2:
                    r_intercept = np.nan #np.log10(EAGLE_EPS)
                    vc_interpolator_holder[pair].append(lambda x: np.nan)

                else:
                    vc_med_interp = interp1d(bin_centres[np.isfinite(vcmedians)],
                                             vcmedians[np.isfinite(vcmedians)], fill_value='extrapolate')
                    vc_interpolator_holder[pair].append(vc_med_interp)

                    heated_intercept_func = lambda radii: vc_med_interp(radii) - heating_limit(radii)
                    try:
                        r_intercept = brentq(heated_intercept_func, np.amax((np.log10(EAGLE_EPS),
                                                                             bin_centres[np.isfinite(vcmedians)][0])), 2)

                    except (ValueError, RuntimeError) as e:
                        print(z, 1-pair, mass_range, e)
                        r_intercept = np.log10(EAGLE_EPS)

                # axs[col].scatter(np.mean(mass_range), r_intercept,
                #                  c=color, linewidth=1, zorder=3, alpha=1)

                log_r_hrs[mass_index] = r_intercept

                # if not half_from_size_mass:
                    #save measured r12s
                r12s[mass_index] = np.nanmedian(r12)
                r14s[mass_index] = np.nanmedian(r14)
                r34s[mass_index] = np.nanmedian(r34)

            if not r12_norm:
                axs[col].errorbar(np.mean(mass_bin_edges, axis=1), log_r_hrs,
                                  c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1, path_effects=path_effect_4)

                # if half_from_size_mass:
                log_M200s = np.linspace(xlims[0], xlims[1])
                log_r12s = size_function(size_function_args(z, 1-pair), log_M200s) #kpc

                # axs[col].errorbar(log_M200s, log_r12s,
                #                   c=color2, ls=linestyle2, linewidth=1, zorder=-2, alpha=1)#, path_effects=path_effect_2)
                # # else:
                # axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(r12s),
                #                   c=color, ls=linestyle, linewidth=1, zorder=-1, alpha=1, path_effects=path_effect_2)
                # axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(r14s),
                #                   c=color, ls=linestyle, linewidth=1, zorder=-1, alpha=1, path_effects=path_effect_2)
                # axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(r34s),
                #                   c=color, ls=linestyle, linewidth=1, zorder=-1, alpha=1, path_effects=path_effect_2)

            else: #if r12_norm:
                log_r12s = size_function(size_function_args(z, 1-pair), np.mean(mass_bin_edges, axis=1)) #kpc

                axs[col].errorbar(np.mean(mass_bin_edges, axis=1), log_r_hrs - log_r12s,
                                  c=color, ls=linestyle, linewidth=2, zorder=5, alpha=1, path_effects=path_effect_4)

                if pair == 1:
                    axs[col].errorbar(np.mean(mass_bin_edges, axis=1), np.log10(EAGLE_EPS) - log_r12s,
                                      c='k', ls=':', lw=1, zorder=-10)

        log_r_vc_ratio_95 = np.zeros(len(mass_bin_edges))

        # for mass_index, mass_range in enumerate(mass_bin_edges):
        #
        #     f = lambda r: vc_interpolator_holder[0][mass_index](r) / vc_interpolator_holder[1][mass_index](r) - 0.97
        #
        #     try:
        #         log_r_vc_ratio_95[mass_index] = brentq(f, np.log10(EAGLE_EPS), 4)
        #     except ValueError as e:
        #         print(mass_range, e)
        #         log_r_vc_ratio_95[mass_index] = np.log10(EAGLE_EPS)
        #
        #     if np.isnan(f(log_r_vc_ratio_95[mass_index])):
        #         log_r_vc_ratio_95[mass_index] = np.nan
        #
        # axs[col].errorbar(np.mean(mass_bin_edges, axis=1), log_r_vc_ratio_95,
        #                   # c=matplotlib.cm.get_cmap('inferno_r')((3 + col) / 6), ls='-',
        #                   c='cyan',
        #                   linewidth=2, zorder=20, alpha=1, path_effects=path_effect_4)

        color='hotpink'
        for diff, color in zip([0.03, 0.1, 0.15], ['hotpink', 'cyan', 'lime']):

            for mass_index, mass_range in enumerate(mass_bin_edges):

                #zero at r=1000=10^3
                f = lambda r: ((vc_interpolator_holder[0][mass_index](r) - vc_interpolator_holder[0][mass_index](3) -
                                vc_interpolator_holder[1][mass_index](r) + vc_interpolator_holder[1][mass_index](3)) /
                               vc_interpolator_holder[0][mass_index](r)) - diff #0.03 #diff

                try:
                    log_r_vc_ratio_95[mass_index] = brentq(f, np.log10(EAGLE_EPS), 3)
                except ValueError as e:
                    log_r_vc_ratio_95[mass_index] = np.log10(EAGLE_EPS)

                if np.isnan(f(log_r_vc_ratio_95[mass_index])):
                    log_r_vc_ratio_95[mass_index] = np.nan

            axs[col].errorbar(np.mean(mass_bin_edges, axis=1), log_r_vc_ratio_95,
                              # c=matplotlib.cm.get_cmap('inferno_r')((3 + col) / 6), ls='-',
                              c=color,
                              linewidth=2, zorder=20, alpha=1, path_effects=path_effect_4)

        #decorations
        m200_intercept_0 = m200_heating_intercept(z, 1)
        m200_intercept_1 = m200_heating_intercept(z, 0)

        axs[col].axvline(m200_intercept_1, c='C1', ls=(0,(0.8,0.8)), linewidth=1, zorder=-3, alpha=1)
        axs[col].axvline(m200_intercept_0, c='C0', ls='-.', linewidth=1, zorder=-3, alpha=1)

        if not r12_norm:
            pass
            # for i in range(20):
            #     # 1/3 Mo, Mao, White style.
            #     slope = 1/3
            #     axs[col].errorbar(xlims, [i*slope + np.floor(ylims[0]) - (xlims[1] - xlims[0]) * slope,
            #                               i*slope + np.floor(ylims[0])],
            #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)
            #
            #     slope = -1/2
            #     axs[col].errorbar(xlims, [i*slope + np.floor(ylims[1]) - (xlims[1] - xlims[0])*slope,
            #                               i*slope + np.floor(ylims[1])],
            #                       c='grey', ls=':', lw=1, zorder=-10, alpha=0.5)

        else: #if r12_norm:
            axs[col].axhline(0, 0, 1,
                             c='k', ls=':', lw=1, zorder=-10)

        if not r12_norm:
            frac_size = 8
            # axs[col].arrow(xlims[0], np.log10(EAGLE_EPS),
            #                (xlims[1] - xlims[0])/frac_size, 0,
            #            length_includes_head=True,
            #            head_width=1/frac_size/2,
            #            head_length=1/frac_size,
            #            color='k', fc='white', lw=1, zorder=-10, alpha=1)
            axs[col].arrow(xlims[1], np.log10(2.8*EAGLE_EPS),
                           -(xlims[1] - xlims[0])/frac_size, 0,
                       length_includes_head=True,
                       head_width=1/frac_size/2,
                       head_length=1/frac_size,
                       color='k', fc='white', lw=1, zorder=-10, alpha=1)

        if not r12_norm:
            dy = 0.06
            va='bottom'
        else:
            dy = 0.95
            va = 'top'
        axs[col].text(xlims[0] + 0.95 * (xlims[1] - xlims[0]), ylims[0] + dy * (ylims[1] - ylims[0]),
                    r'$z=$' + str_z, ha='right', va=va, bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def mass_rad_stacked(shade=True, show_all=False, name='', save=False):
    N_res = 1

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 0.7, 3.3 - 1e-3]
    ylims = [5.8, 9.8]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))

    xlabel = r'$\log \, r /$ kpc'
    ylabel = r'${\rm d} \, \log \, (M / $M$_\odot) \, / {\rm d}\, \log \, (r /$kpc$)$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)
        if save:
            axs[col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for comp in ['Star']: #['Star', 'Gas']:
        for pair in range(2):
            if pair == 0: # 0 (7x)
                linestyle = '-'
                linestyle2 = '-.'
                marker = 'o'
                color = 'C0'
                color2 = 'navy'
                label = 'HRDM'
            else:# 1 (1x)
                linestyle = '--'
                linestyle2 = (0, (0.8, 0.8))
                marker = 'o'
                color = 'C1'
                color2 = 'saddlebrown'
                label = 'LRDM'

            #z=0 output only
            with h5.File(combined_name_pairs[0][pair], 'r') as raw_data0:
                # centrals only
                mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
                # mask1 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]] > 0.4
                # mask0 = np.logical_and(mask0, mask1)

                #load
                m0 = raw_data0[keys.key0_dict['m200'][None]][:]

                mstar = raw_data0[keys.key0_dict['mstarr'][None]][:, 0:0+len(bin_centres)] #10^10 M_sun
                mgas = raw_data0[keys.key0_dict['mgasr'][None]][:, 0:0+len(bin_centres)]
                mdm = raw_data0[keys.key0_dict['mdmr'][None]][:, 0:0+len(bin_centres)]

                mtot = mstar + mgas + mdm #np.cumsum(mstar + mgas + mdm, axis=1)

                N0 = raw_data0[keys.key0_dict['Npart'][comp]][:, 0:0+len(bin_centres)]

                r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

                r12_r0 = raw_data0[keys.key0_dict['r12r']['Star']][:, 0:0+len(bin_centres)]

                # volume_shell = 4/3*np.pi * (keys.log_bin_edges[1:]**3 - keys.log_bin_edges[:-1]**3)
                # rho0 = np.log10(mstar[mask0] / volume_shell[np.newaxis, :])
                # rhogas = np.log10(mgas[mask0] / volume_shell[np.newaxis, :])
                # rhodm = np.log10(mdm[mask0] / volume_shell[np.newaxis, :])

                #mask and units
                m0 = np.log10(m0[mask0]) + 10
                N0 = N0[mask0]
                r12_0 = np.log10(r12_0[mask0])

                # mstar = np.roll(mstar, shift=0, axis=1)

                dr = np.diff(keys.log_bin_edges)
                y0 = np.log10(mstar[mask0] / dr) + 10
                y0[np.logical_not(np.isfinite(y0))] = 1e-20
                # dlgas = np.log10(mgas[mask0] / dr) + 10
                # dldm = np.log10(mdm[mask0] / dr) + 10

                r12_r0 = np.log10(r12_r0[mask0])

            for col, mass_range in enumerate(mass_bin_edges):

                #mask
                mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
                y = y0[mask]
                N = N0[mask]
                r12 = r12_0[mask]
                r12_r = r12_r0[mask]

                # calc medians etc.
                iiin = len(bin_centres)  # np.sum(radial_bin_mask)
                ymedians = np.zeros(iiin)
                yplus = np.zeros(iiin)
                yminus = np.zeros(iiin)

                boots = np.zeros((iiin, 3))

                #N_mask = N > N_res
                for iii in range(iiin):
                    # no mass is meaningful
                    ys = y[:, iii]#[N_mask[:, iii]]

                    ymedians[iii] = np.nanmedian(ys)
                    # ymedians[j] = np.nanmean(ys)
                    yplus[iii] = np.nanquantile(ys, 0.84)
                    yminus[iii] = np.nanquantile(ys, 0.16)

                    boots[iii] = my_bootstrap(ys, statistic=np.nanmedian)

                    bin_centres[iii] = np.nanmedian(r12_r[:, iii])

                not_finite_mask = ymedians < ylims[0]
                ymedians[not_finite_mask] = np.nan
                yplus[not_finite_mask] = np.nan
                yminus[not_finite_mask] = np.nan

                boots[not_finite_mask, :] = np.nan

                #plot
                axs[col].errorbar(bin_centres, ymedians,
                             c=color, ls=linestyle, linewidth=2, zorder=10, alpha=1,
                             path_effects=path_effect_4)

                if shade:
                    axs[col].fill_between(bin_centres, yminus, yplus,
                                     color=color, alpha=0.5, edgecolor='k', zorder=8)

                    axs[col].fill_between(bin_centres, boots[:, 0], boots[:, 2],
                                          color=color, alpha=0.5, edgecolor='k', zorder=9, hatch=hatches[linestyle])

                if show_all:  # np.sum(mask0) < 100 or
                    for i in range(np.sum(mask)):
                        axs[col].errorbar(bin_centres, y[i],
                                     color=color, linestyle=linestyle, linewidth=1, alpha=0.2, rasterized=True)

                #decorations
                if pair == 0:
                    axs[col].errorbar(np.log10([EAGLE_EPS, EAGLE_EPS]), ylims,
                                  c='k', ls=':', lw=1, zorder=-10, alpha=1)
                    axs[col].errorbar(np.log10([2.8*EAGLE_EPS, 2.8*EAGLE_EPS]), ylims,
                                  c='k', ls=':', lw=1, zorder=-10, alpha=1)

                axs[col].axvline(np.nanmedian(r12), 0, 1,
                                 c=color2, ls=linestyle2, lw=1, zorder=-4, alpha=1)
                # axs[col].axvline(np.nanquantile(r12, 0.84), 0, 1,
                #                  c=color2, ls=linestyle2, lw=1, zorder=-4, alpha=1)
                # axs[col].axvline(np.nanquantile(r12, 0.16), 0, 1,
                #                  c=color2, ls=linestyle2, lw=1, zorder=-4, alpha=1)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                   ],
                  ['HR DM', 'LR DM'],
                  handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  title=r'$\log \, M_{200} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[0]),
                  loc='upper right')

    for i in range(1, len(mass_bin_edges)):
        axs[i].legend([],[],
                      title=r'$\log \, M_{200} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[i]),
                      loc='upper right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def matched_comparison(name='', save=False, ek=False, shade=True):
    N_res = 10
    N_bins = 16
    z_index_highlight = 0

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10,10.2], [11,11.2], [12,12.2]]

    fig.set_size_inches(3.3 * len(mass_bin_edges), 3.3 + 0.1, forward=True)
    spec = fig.add_gridspec(ncols=len(mass_bin_edges), nrows=1)

    xlims = [1e-3 - 0.6, 0.4 - 1e-3]
    ylims = [1e-3 - 0.6, 0.4 - 1e-3]

    xlabel = r'$\log \, \sigma_{\rm tot}^{\rm HR} (r_{1/2}) / V_{200}$'
    ylabel = r'$\log \, \sigma_{\rm tot}^{\rm LR} (r_{1/2}) / V_{200}$'

    axs = []
    for col in range(len(mass_bin_edges)):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)
        axs[col].set_aspect('equal')

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    for z_index in range(4):
        color = matplotlib.cm.get_cmap('inferno_r')((3 + z_index) / 6)

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
            r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]
            sz0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sR0 = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sphi0 = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            cop0 = raw_data0['GalaxyQuantities/SubGroupCOP'][()]

            #add kinetic energy because that's what Arron has done
            if ek:
                vphi0 = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            #mask and units
            V200_0 = np.sqrt(GRAV_CONST * M200_0 / r200_0) # km/s
            log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
            y0 = np.log10(np.sqrt(sz0[mask0]**2 + sR0[mask0]**2 + sphi0[mask0]**2) / V200_0[mask0])
            if ek:
                y0 = np.log10(np.sqrt(sz0[mask0] ** 2 + sR0[mask0] ** 2 + sphi0[mask0] ** 2
                                      + vphi0[mask0]**2) / V200_0[mask0])
            gn0 = gn0[mask0]
            cop0 = cop0[mask0]

        with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
            gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

            # load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
            r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]
            sz1 = raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sR1 = raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sphi1 = raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            cop1 = raw_data1['GalaxyQuantities/SubGroupCOP'][()]

            # add kinetic energy because that's what Arron has done
            if ek:
                vphi1 = raw_data1[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            # mask and units
            V200_1 = np.sqrt(GRAV_CONST * M200_1 / r200_1)  # km/s
            log_M200_1 = np.log10(M200_1[mask1]) + 10  # log Mstar
            y1 = np.log10(np.sqrt(sz1[mask1] ** 2 + sR1[mask1] ** 2 + sphi1[mask1] ** 2) / V200_1[mask1])
            if ek:
                y1 = np.log10(np.sqrt(sz1[mask1] ** 2 + sR1[mask1] ** 2 + sphi1[mask1] ** 2
                                      + vphi1[mask1] ** 2) / V200_1[mask1])
            gn1 = gn1[mask1]
            cop1 = cop1[mask1]

            # #mask
            # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
            # x = x0[not_crazy_mask0]
            # y = y0[not_crazy_mask0]

        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        #not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        #not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0)) # should be same as np.arange(np.sum(ismatched0))
        order1 = np.argsort(np.argsort(matched_gn_1))

        #this also works
        # order0 = np.arange(len(matched_gn_0))
        # arg = np.argsort(np.argsort(matched_gn_1))
        # order1 = matched_gn_1 - np.cumsum(np.concatenate([[0,], np.diff(np.unique(matched_gn_1))-1]))[arg] - 1

        y0 = y0[in0][order0]
        log_M200_0 = log_M200_0[in0][order0]
        cop0 = cop0[in0][order0]
        gn0 = gn0[in0][order0]

        y1 = y1[in1][order1]
        log_M200_1 = log_M200_1[in1][order1]
        cop1 = cop1[in1][order1]
        gn1 = gn1[in1][order1]

        m0 = np.log10((10**log_M200_0 + 10**log_M200_1) / 2)

        # y0 = cop0[:,0,1]
        # y1 = cop1[:,0,1]

        for col, mass_range in enumerate(mass_bin_edges):

            #mask
            mask = np.logical_and(mass_range[0] < m0, m0 < mass_range[1])
            x = y0[mask]
            y = y1[mask]

            #rotate data by 45 deg
            xd = (y + x) / 2
            yd = (y - x) / 2

            # calc medians etc.
            ydmedians, xdedges, bin_is = binned_statistic(xd, yd,
                                                          statistic=np.nanmedian, bins=np.linspace(*xlims, N_bins))

            ydplus, _, _ = binned_statistic(xd, yd,
                                             statistic=lambda x: np.nanquantile(x, 0.16),
                                             bins=np.linspace(*xlims, N_bins))
            ydminus, _, _ = binned_statistic(xd, yd,
                                             statistic=lambda x: np.nanquantile(x, 0.84),
                                             bins=np.linspace(*xlims, N_bins))
            xdcentres = 0.5 * (xdedges[1:] + xdedges[:-1])

            counts, _ = np.histogram(x, bins=np.linspace(*xlims, N_bins))
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]

            boots = np.zeros((len(xdedges) - 1, 3))
            for i in range(len(xdedges) - 1):
                boots[i] = my_bootstrap(yd[bin_is == i + 1], statistic=np.nanmedian)

            #plot
            axs[col].errorbar(xdcentres[mask] - ydmedians[mask], xdcentres[mask] + ydmedians[mask],
                              c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            if shade:
                if z_index == z_index_highlight:
                    axs[col].fill((*(xdcentres[mask] - ydminus[mask]), *np.flip(xdcentres[mask] - ydplus[mask])),
                                  (*(xdcentres[mask] + ydminus[mask]), *np.flip(xdcentres[mask] + ydplus[mask])),
                                  color=color, alpha=0.5, edgecolor='k', zorder=8)

                axs[col].fill((*(xdcentres[mask] - boots[mask, 0]), *np.flip(xdcentres[mask] - boots[mask, 2])),
                              (*(xdcentres[mask] + boots[mask, 0]), *np.flip(xdcentres[mask] + boots[mask, 2])),
                              color=color, alpha=0.5, edgecolor='k', zorder=9, hatch='x' * (z_index + 1))

                #this is wrong
                # axs[col].fill_between(xdcentres[mask] - ydmedians[mask],
                #                       xdcentres[mask] + ydminus[mask], xdcentres[mask] + ydplus[mask],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=8)
                # axs[col].fill_between(xdcentres[mask] - ydmedians[mask],
                #                       xdcentres[mask] + boots[mask, 0], xdcentres[mask] + boots[mask, 2],
                #                       color=color, alpha=0.5, edgecolor='k', zorder=9, hatch='x' * (z_index + 1))

            if z_index == z_index_highlight:
                axs[col].scatter(x, y,
                                 color=color, marker='o', s=4, alpha=0.5,
                                 linewidths=0, zorder=2, rasterized=True)

            # axs[col].errorbar(xdcentres, ydmedians,
            #                   c='C2', ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            # axs[col].scatter(xd, yd,
            #                  color='C2', marker='o', s=4, alpha=0.5,
            #                  linewidths=0, zorder=2, rasterized=True)

            #decorations
            axs[col].errorbar(xlims, xlims,
                              c='k', ls=':', lw=1, zorder=-10, alpha=1)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
    #                ],
    #               ['HR DM\n' + r'$j_\star(R)$', 'LR DM\n' + r'$j_\star(R)$'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               title=r'$\log \, M_{200} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[0]),
    #               loc='lower right')
    #
    # axs[1].legend([(lines.Line2D([0, 1], [0, 1], color='navy', ls='-.', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color='saddlebrown', ls=(0, (0.8, 0.8)), lw=2,
    #                path_effects=path_effect_4)),
    #                ],
    #               ['HR DM\n' + r'$j_c(R)$', 'LR DM\n' + r'$j_c(R)$'],
    #               handlelength=2, labelspacing=0.2, handletextpad=0.4,
    #               title=r'$\log \, M_{200} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[1]),
    #               loc='lower right')

    for i in range(0, len(mass_bin_edges)):
        axs[i].legend([],[],
                      title=r'$\log \, M_{200} / $M$_\odot $' + '\n' + r'$\in$' + str(mass_bin_edges[i]),
                      loc='lower right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def matched_sigma_resolved(name='', save=False, ek=False, shade=True):
    N_res = 10
    N_bins = 16
    z_index_highlight = 1

    fig = plt.figure(constrained_layout=True)

    fig.set_size_inches(4.125, 4.125, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [1e-3 + 1, 2.5 - 1e-3]
    ylims = [1e-3 + 1, 2.5 - 1e-3]
    ticks = [1.25, 1.50, 1.75, 2.00, 2.25]

    xlabel = r'$\log \, \sigma_{\rm tot}^{\rm HR} (r_{1/2}) /$ km s$^{-1}$'
    ylabel = r'$\log \, \sigma_{\rm tot}^{\rm LR} (r_{1/2}) /$ km s$^{-1}$'

    axs = []
    for col in range(1):
        axs.append(fig.add_subplot(spec[col]))
        axs[col].set_xlim(xlims)
        axs[col].set_ylim(ylims)

        if col == 0: axs[col].set_ylabel(ylabel)
        else: axs[col].set_yticklabels([])

        axs[col].set_xlabel(xlabel)
        axs[col].set_aspect('equal')

        axs[col].set_xticks(ticks)
        axs[col].set_yticks(ticks)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    #bi flag
    # color0 = '#d00070'
    # color1 = '#8c4799'
    # color2 = '#0032a0'

    cmap = 'cool'
    color0 = matplotlib.cm.get_cmap(cmap)(2 / 2)
    color1 = matplotlib.cm.get_cmap(cmap)(1 / 2)
    color2 = matplotlib.cm.get_cmap(cmap)(0 / 2)

    comp='Star' #'Gas'

    for z_index in range(1):
        z = [0, 0.503, 1.004, 2.012][z_index]

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
            r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]
            sz0 = raw_data0[keys.key0_dict['sigma_z'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sR0 = raw_data0[keys.key0_dict['sigma_R'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sphi0 = raw_data0[keys.key0_dict['sigma_phi'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            #add kinetic energy because that's what Arron has done
            if ek:
                vphi0 = raw_data0[keys.key0_dict['mean_v_phi'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            #mask and units
            V200_0 = np.sqrt(GRAV_CONST * M200_0 / r200_0) # km/s
            log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
            # y0 = np.log10(np.sqrt(sz0[mask0]**2 + sR0[mask0]**2 + sphi0[mask0]**2) / V200_0[mask0])
            y0 = np.log10(np.sqrt(sz0[mask0]**2 + sR0[mask0]**2 + sphi0[mask0]**2))
            if ek:
                # y0 = np.log10(np.sqrt(sz0[mask0] ** 2 + sR0[mask0] ** 2 + sphi0[mask0] ** 2
                #                       + vphi0[mask0]**2) / V200_0[mask0])
                y0 = np.log10(np.sqrt(sz0[mask0] ** 2 + sR0[mask0] ** 2 + sphi0[mask0] ** 2 + vphi0[mask0]**2))
            gn0 = gn0[mask0]
            # cop0 = cop0[mask0]

        with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
            gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

            # load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
            r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]
            sz1 = raw_data1[keys.key0_dict['sigma_z'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sR1 = raw_data1[keys.key0_dict['sigma_R'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            sphi1 = raw_data1[keys.key0_dict['sigma_phi'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]
            # add kinetic energy because that's what Arron has done
            if ek:
                vphi1 = raw_data1[keys.key0_dict['mean_v_phi'][comp]][:, keys.rkey_to_rcolum_dict['eq_r12', None]]

            # mask and units
            V200_1 = np.sqrt(GRAV_CONST * M200_1 / r200_1)  # km/s
            log_M200_1 = np.log10(M200_1[mask1]) + 10  # log Mstar
            y1 = np.log10(np.sqrt(sz1[mask1] ** 2 + sR1[mask1] ** 2 + sphi1[mask1] ** 2))
            if ek:
                y1 = np.log10(np.sqrt(sz1[mask1] ** 2 + sR1[mask1] ** 2 + sphi1[mask1] ** 2 + vphi1[mask1] ** 2))
            gn1 = gn1[mask1]
            # cop1 = cop1[mask1]

            # #mask
            # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
            # x = x0[not_crazy_mask0]
            # y = y0[not_crazy_mask0]

        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        #not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        #not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0)) # should be same as np.arange(np.sum(ismatched0))
        order1 = np.argsort(np.argsort(matched_gn_1))

        #this also works
        # order0 = np.arange(len(matched_gn_0))
        # arg = np.argsort(np.argsort(matched_gn_1))
        # order1 = matched_gn_1 - np.cumsum(np.concatenate([[0,], np.diff(np.unique(matched_gn_1))-1]))[arg] - 1

        y0 = y0[in0][order0]
        log_M200_0 = log_M200_0[in0][order0]
        # cop0 = cop0[in0][order0]
        # gn0 = gn0[in0][order0]

        y1 = y1[in1][order1]
        log_M200_1 = log_M200_1[in1][order1]
        # cop1 = cop1[in1][order1]
        # gn1 = gn1[in1][order1]

        m0 = np.log10((10**log_M200_0 + 10**log_M200_1) / 2)

        col = 0

        m200_intercept_0 = m200_heating_intercept(z, 1)
        m200_intercept_1 = m200_heating_intercept(z, 0)

        #mask
        mask = np.ones(len(y0), dtype=bool)
        x = y0[mask]
        y = y1[mask]

        #rotate data by 45 deg
        xd = (y + x) / 2
        yd = (y - x) / 2

        # calc medians etc.
        ydmedians, xdedges, bin_is = binned_statistic(xd, yd,
                                                      statistic=np.nanmedian, bins=np.linspace(*xlims, N_bins))

        ydplus, _, _ = binned_statistic(xd, yd,
                                         statistic=lambda x: np.nanquantile(x, 0.16),
                                         bins=np.linspace(*xlims, N_bins))
        ydminus, _, _ = binned_statistic(xd, yd,
                                         statistic=lambda x: np.nanquantile(x, 0.84),
                                         bins=np.linspace(*xlims, N_bins))
        xdcentres = 0.5 * (xdedges[1:] + xdedges[:-1])

        counts, _ = np.histogram(x, bins=np.linspace(*xlims, N_bins))
        # mask = counts > 10
        mask = counts > 20 * np.diff(N_run)[0]

        boots = np.zeros((len(xdedges) - 1, 3))
        for i in range(len(xdedges) - 1):
            boots[i] = my_bootstrap(yd[bin_is == i + 1], statistic=np.nanmedian)

        #plot
        axs[col].errorbar(xdcentres[mask] - ydmedians[mask], xdcentres[mask] + ydmedians[mask],
                          c=color0, ls='-', linewidth=1, zorder=10, alpha=1)

        if shade:
            if z_index == z_index_highlight:
                axs[col].fill((*(xdcentres[mask] - ydminus[mask]), *np.flip(xdcentres[mask] - ydplus[mask])),
                              (*(xdcentres[mask] + ydminus[mask]), *np.flip(xdcentres[mask] + ydplus[mask])),
                              color=color0, alpha=0.5, edgecolor='k', zorder=8)

            axs[col].fill((*(xdcentres[mask] - boots[mask, 0]), *np.flip(xdcentres[mask] - boots[mask, 2])),
                          (*(xdcentres[mask] + boots[mask, 0]), *np.flip(xdcentres[mask] + boots[mask, 2])),
                          color=color0, alpha=0.5, edgecolor='k', zorder=9, hatch='x' * (z_index + 1))

        axs[col].scatter(x, y,
                         color=color0, marker='o', s=0.6, alpha=0.3,
                         linewidths=0, zorder=0, rasterized=True)

        #above low res heating limit
        mask = m0 > m200_intercept_0
        x = y0[mask]
        y = y1[mask]

        #rotate data by 45 deg
        xd = (y + x) / 2
        yd = (y - x) / 2

        # calc medians etc.
        ydmedians, xdedges, bin_is = binned_statistic(xd, yd,
                                                      statistic=np.nanmedian, bins=np.linspace(*xlims, N_bins))

        ydplus, _, _ = binned_statistic(xd, yd,
                                         statistic=lambda x: np.nanquantile(x, 0.16),
                                         bins=np.linspace(*xlims, N_bins))
        ydminus, _, _ = binned_statistic(xd, yd,
                                         statistic=lambda x: np.nanquantile(x, 0.84),
                                         bins=np.linspace(*xlims, N_bins))
        xdcentres = 0.5 * (xdedges[1:] + xdedges[:-1])

        counts, _ = np.histogram(x, bins=np.linspace(*xlims, N_bins))
        # mask = counts > 10
        mask = counts > 20 * np.diff(N_run)[0]

        boots = np.zeros((len(xdedges) - 1, 3))
        for i in range(len(xdedges) - 1):
            boots[i] = my_bootstrap(yd[bin_is == i + 1], statistic=np.nanmedian)

        #plot
        axs[col].errorbar(xdcentres[mask] - ydmedians[mask], xdcentres[mask] + ydmedians[mask],
                          c=color1, ls='-', linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        if shade:
            if z_index == z_index_highlight:
                axs[col].fill((*(xdcentres[mask] - ydminus[mask]), *np.flip(xdcentres[mask] - ydplus[mask])),
                              (*(xdcentres[mask] + ydminus[mask]), *np.flip(xdcentres[mask] + ydplus[mask])),
                              color=color1, alpha=0.5, edgecolor='k', zorder=8)

            axs[col].fill((*(xdcentres[mask] - boots[mask, 0]), *np.flip(xdcentres[mask] - boots[mask, 2])),
                          (*(xdcentres[mask] + boots[mask, 0]), *np.flip(xdcentres[mask] + boots[mask, 2])),
                          color=color1, alpha=0.5, edgecolor='k', zorder=9, hatch='x' * (z_index + 1))

        axs[col].scatter(x, y,
                         color=color1, marker='o', s=2, alpha=0.4,
                         linewidths=0, zorder=1, rasterized=True)

        #above hi res heating limit
        mask = m0 > m200_intercept_1
        x = y0[mask]
        y = y1[mask]

        #rotate data by 45 deg
        xd = (y + x) / 2
        yd = (y - x) / 2

        # calc medians etc.
        ydmedians, xdedges, bin_is = binned_statistic(xd, yd,
                                                      statistic=np.nanmedian, bins=np.linspace(*xlims, N_bins))

        ydplus, _, _ = binned_statistic(xd, yd,
                                         statistic=lambda x: np.nanquantile(x, 0.16),
                                         bins=np.linspace(*xlims, N_bins))
        ydminus, _, _ = binned_statistic(xd, yd,
                                         statistic=lambda x: np.nanquantile(x, 0.84),
                                         bins=np.linspace(*xlims, N_bins))
        xdcentres = 0.5 * (xdedges[1:] + xdedges[:-1])

        counts, _ = np.histogram(x, bins=np.linspace(*xlims, N_bins))
        # mask = counts > 10
        mask = counts > 20 * np.diff(N_run)[0]

        boots = np.zeros((len(xdedges) - 1, 3))
        for i in range(len(xdedges) - 1):
            boots[i] = my_bootstrap(yd[bin_is == i + 1], statistic=np.nanmedian)

        #plot
        axs[col].errorbar(xdcentres[mask] - ydmedians[mask], xdcentres[mask] + ydmedians[mask],
                          c=color2, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

        if shade:
            if z_index == z_index_highlight:
                axs[col].fill((*(xdcentres[mask] - ydminus[mask]), *np.flip(xdcentres[mask] - ydplus[mask])),
                              (*(xdcentres[mask] + ydminus[mask]), *np.flip(xdcentres[mask] + ydplus[mask])),
                              color=color2, alpha=0.5, edgecolor='k', zorder=8)

            axs[col].fill((*(xdcentres[mask] - boots[mask, 0]), *np.flip(xdcentres[mask] - boots[mask, 2])),
                          (*(xdcentres[mask] + boots[mask, 0]), *np.flip(xdcentres[mask] + boots[mask, 2])),
                          color=color2, alpha=0.5, edgecolor='k', zorder=9, hatch='x' * (z_index + 1))

        axs[col].scatter(x, y,
                         color=color2, marker='o', s=4, alpha=0.5,
                         linewidths=0, zorder=2, rasterized=True)

    #decorations
    axs[0].errorbar(xlims, xlims,
                      c='k', ls=':', lw=1, zorder=10, alpha=1)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color=color2, ls='-', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color=color1, ls='-', lw=1, path_effects=path_effect_2)),
                   (lines.Line2D([0, 1], [0, 1], color=color0, ls='-', lw=1)),
                   ],
                  ['both resolved', 'HR resolved,\nLR unresolved', 'both unresolved'],
                  loc='lower right', frameon=False)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return

def matched_mass(name='', save=False, ek=False, shade=False, r12=True, z_index=0, disk_sample=False,
                 k_co=False, s_tot=False, mstar=False, ca=False, size=False):
    N_res = 10
    # color = matplotlib.cm.get_cmap('inferno_r')((1.5 + z_index)/5)

    xlims = [10, 14]
    # ylims0 = [0.5, 1.5]
    # ylims0 = [1e-3 + -50, 50 - 1e-3]
    ylims0 = [-1, 1]
    ylims1 = [1e-3 + 0, 50 - 1e-3] #keep same scale
    if k_co:
        ylims0 = [1e-3 + -0.6, 0.6 - 1e-3]
    if size:
        # ylims0 = [-7, 7]
        ylims0 = [-1, 1]
        ylims1 = [1e-3 + 0, 7 - 1e-3] #keep same scale
        
    if mstar:
        ylims1 = [1e-3 + 0, 1 - 1e-3] #keep same scale

    ylims2 = [1e-3 + -0.5, 1 - 1e-3]
    ylimss = [ylims0, ylims1, ylims2]

    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.75, 14, 18)
    N_run = np.linspace(9 + 2/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200}^{\rm HR} / $M$_\odot$'
    # if r12: ylabel0 =r'$\Delta \sigma_{\rm tot} (r_{1/2})$ [km/s]'
    # if r12: ylabel0 =r'$\sigma_{\rm tot}^{\rm LR} (r_{1/2}) - \sigma_{\rm tot}^{\rm HR} (r_{1/2})$ [km/s]'
    if r12: ylabel0 =r'$\log \, \sigma_{\rm tot}^{\rm LR} (r_{1/2}) / \sigma_{\rm tot}^{\rm HR} (r_{1/2})$'
    else: ylabel0 =r'$\Delta \sigma_{\rm tot} (r < r_{200})$'
    # if size: ylabel0 =r'$\Delta r_{1/2}$ [kpc]'
    # if size: ylabel0 =r'$r_{1/2}^{\rm LR} - r_{1/2}^{\rm HR}$ [kpc]'
    if size: ylabel0 =r'$\log \, r_{1/2}^{\rm LR} / r_{1/2}^{\rm HR}$'
    if k_co:
        if r12: ylabel0 = r'$\Delta \kappa_{\rm co} (r_{1/2})$'
        else: ylabel0 = r'$\Delta \kappa_{\rm co} (r < r_{200})$'
    if mstar:
        ylabel0 = r'$\log \, M_\star^{\rm HR} / M_\star^{\rm LR}$'
    if ca:
        ylabel0 = r'$\Delta c/a$'

    ylabel1 = r'scatter [km/s]' #r'scatter $[(X_{84} - X_{16}) / 2]$'
    if size:
        ylabel1 = r'scatter [kpc]'  # r'scatter $[(X_{84} - X_{16}) / 2]$'
    if mstar:
        ylabel1 = r'scatter [$10^{10} \, {\rm M}_\odot$]'  # r'scatter $[(X_{84} - X_{16}) / 2]$'

    ylabel2 = r'correlation'
    ylabels = [ylabel0, ylabel1, ylabel2]

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(3.3, 3.3 * 2 + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=3, height_ratios=[1, *([0.5]*2)])

    axs = []
    for row in range(3):
        axs.append(fig.add_subplot(spec[row]))
        axs[row].set_xlim(xlims)
        axs[row].set_ylim(ylimss[row])

        axs[row].set_ylabel(ylabels[row])

        if row != 2: axs[row].set_xticklabels([])
        else: axs[row].set_xlabel(xlabel)

        if row == 0: axs[row].set_aspect(np.diff(xlims)/np.diff(ylimss[row]))
        else: axs[row].set_aspect(np.diff(xlims)/np.diff(ylimss[row])/2)

        if size and row == 1:
            axs[row].set_yticks([2,4,6])

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    # #separate figures
    # fig0 = plt.figure(constrained_layout=True)
    # fig0.set_size_inches(3.3, 3.3, forward=True)
    # # fig0.set_size_inches(2.5, 2.5, forward=True)
    # ax0 = plt.gca()
    #
    # fig1 = plt.figure(constrained_layout=True)
    # fig1.set_size_inches(3.3, 3.3, forward=True)
    # spec = fig1.add_gridspec(ncols=1, nrows=2)
    #
    # ax1 = fig1.add_subplot(spec[0])
    # ax2 = fig1.add_subplot(spec[1])
    #
    # axs = [ax0, ax1, ax2]
    #
    # for row in range(3):
    #     axs[row].set_xlim(xlims)
    #     axs[row].set_ylim(ylimss[row])
    #
    #     axs[row].set_ylabel(ylabels[row])
    #
    #     if row == 1: axs[row].set_xticklabels([])
    #     else: axs[row].set_xlabel(xlabel)
    #
    #     if row == 0:
    #         axs[row].set_aspect(np.diff(xlims)/np.diff(ylimss[row]))
    #     else: axs[row].set_aspect(np.diff(xlims)/np.diff(ylimss[row])/2)
    #
    # fig1.subplots_adjust(hspace=0.00, wspace=0.00)

    if r12:
        rad_key = 'eq_r12'
    else:
        rad_key = 'r200'

    for z_index in [z_index]: # [3,2,1,0]: #range(4):
        color = matplotlib.cm.get_cmap('inferno_r')((3 + z_index) / 6)
        color2 = matplotlib.cm.get_cmap('inferno_r')((0 + z_index) / 6)
        # color = 'C2'
        z = [0, 0.503, 1.004, 2.012][z_index]
        disk_color = matplotlib.cm.get_cmap('viridis_r')((0 + z_index) / 6)

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            # mask0 = np.logical_and(mask0,
            #                        raw_data0[keys.key0_dict['kappa_co']['Star']][:,
            #                        keys.rkey_to_rcolum_dict[rad_key, None]] > 0.4)
            gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
            r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]
            # cop0 = raw_data0['GalaxyQuantities/SubGroupCOP'][()]
            mstar_0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            if s_tot or ek:
                sz0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                sR0 = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                sphi0 = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            #add kinetic energy because that's what Arron has done
            if ek:
                vphi0 = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            if k_co or disk_sample:
                kappa_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            if ca:
                ca_0 = raw_data0[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                ca_0 /= raw_data0[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            if size:
                r12_0 = raw_data0[keys.key0_dict['r12'][None]][:]

            #mask and units
            V200_0 = np.sqrt(GRAV_CONST * M200_0 / r200_0) # km/s
            log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
            if s_tot:
                # y0_all = np.log10(np.sqrt(sz0[mask0]**2 + sR0[mask0]**2 + sphi0[mask0]**2) / V200_0[mask0])
                y0_all = np.sqrt(sz0[mask0]**2 + sR0[mask0]**2 + sphi0[mask0]**2)
            if ek:
                y0_all = np.log10(np.sqrt(sz0[mask0] ** 2 + sR0[mask0] ** 2 + sphi0[mask0] ** 2
                                      + vphi0[mask0]**2) / V200_0[mask0])
            if k_co:
                y0_all = kappa_0[mask0]
            if mstar:
                y0_all = mstar_0[mask0] #/ M200_0[mask0]
            if ca:
                y0_all = ca_0[mask0]
            if size:
                y0_all = r12_0[mask0]

            gn0 = gn0[mask0]
            # cop0 = cop0[mask0]

        with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
            # mask1 = np.logical_and(mask1,
            #                        raw_data1[keys.key0_dict['kappa_co']['Star']][:,
            #                        keys.rkey_to_rcolum_dict[rad_key, None]] > 0.4)
            gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

            # load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
            r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]

            mstar_1 = raw_data1[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            if s_tot or ek:
                sz1 = raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                sR1 = raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                sphi1 = raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            # add kinetic energy because that's what Arron has done
            if ek:
                vphi1 = raw_data1[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

            if k_co or disk_sample:
                kappa_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            if ca:
                ca_1 = raw_data1[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
                ca_1 /= raw_data1[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            if size:
                r12_1 = raw_data1[keys.key0_dict['r12'][None]][:]

            # cop1 = raw_data1['GalaxyQuantities/SubGroupCOP'][()]

            # mask and units
            V200_1 = np.sqrt(GRAV_CONST * M200_1 / r200_1)  # km/s
            log_M200_1 = np.log10(M200_1[mask1]) + 10  # log Mstar
            if s_tot:
                # y1_all = np.log10(np.sqrt(sz1[mask1] ** 2 + sR1[mask1] ** 2 + sphi1[mask1] ** 2) / V200_1[mask1])
                y1_all = np.sqrt(sz1[mask1]**2 + sR1[mask1]**2 + sphi1[mask1]**2)
            if ek:
                y1_all = np.log10(np.sqrt(sz1[mask1] ** 2 + sR1[mask1] ** 2 + sphi1[mask1] ** 2
                                      + vphi1[mask1] ** 2) / V200_1[mask1])
            if k_co:
                y1_all = kappa_1[mask1]
            if mstar:
                y1_all = mstar_1[mask1] #/ M200_1[mask1]
            if ca:
                y1_all = ca_1[mask1]
            if size:
                y1_all = r12_1[mask1]

            gn1 = gn1[mask1]
            # cop1 = cop1[mask1]

        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        #not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        #not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0)) # should be same as np.arange(np.sum(ismatched0))
        order1 = np.argsort(np.argsort(matched_gn_1))

        #this also works
        # order0 = np.arange(len(matched_gn_0))
        # arg = np.argsort(np.argsort(matched_gn_1))
        # order1 = matched_gn_1 - np.cumsum(np.concatenate([[0,], np.diff(np.unique(matched_gn_1))-1]))[arg] - 1

        y0 = y0_all[in0][order0]
        m_0 = log_M200_0[in0][order0]
        # cop0 = cop0[in0][order0]
        gn0 = gn0[in0][order0]

        y1 = y1_all[in1][order1]
        m_1 = log_M200_1[in1][order1]
        # cop1 = cop1[in1][order1]
        gn1 = gn1[in1][order1]

        #for disk sample
        if disk_sample:
            kappa_co_cut = 0.4

            # matched sample
            k0 = kappa_0[mask0][in0][order0]
            k1 = kappa_1[mask1][in1][order1]

            match_disk_mask = np.logical_and(k0 > kappa_co_cut, k1 > kappa_co_cut)
            # match_disk_mask = np.logical_and(DT_0[in0][order0] > 0.5, DT_1[in1][order1] > 0.5)

            y0_disk = y0[match_disk_mask]
            m_0_disk = m_0[match_disk_mask]
            y1_disk = y1[match_disk_mask]
            m_1_disk = m_1[match_disk_mask]

            y_disk = y1_disk - y0_disk
            x_disk = np.log10((10**m_0_disk + 10**m_1_disk) / 2)

#         for g0, g1 in zip(gn0[match_disk_mask], gn1[match_disk_mask]):
#             if g0 < 200:
#                 out = '''
# \\newpage
# \\begin{{figure*}}
# \includegraphics[width=0.45\\textwidth]{{/home/matt/Documents/UWA/EAGLE/results/images/HR/visualisation_group{0}_face_on.png}}
# \includegraphics[width=0.45\\textwidth]{{/home/matt/Documents/UWA/EAGLE/results/images/FR/visualisation_group{1}_face_on.png}} \\\\
# \includegraphics[width=0.45\\textwidth]{{/home/matt/Documents/UWA/EAGLE/results/images/HR/visualisation_group{0}_edge_on.png}}
# \includegraphics[width=0.45\\textwidth]{{/home/matt/Documents/UWA/EAGLE/results/images/FR/visualisation_group{1}_edge_on.png}}
# \caption{{HR group number {0}, LR group number {1}}}
# \\end{{figure*}}'''.format(g0, g1)
#
#                 print(out)

        #what to actually plot
        # y = y1 - y0
        y = np.log10(y1 / y0)
        # x = np.log10((10**m_0 + 10**m_1) / 2)
        x = m_0

        # calc medians etc.
        ymedians, xedges, bin_is = binned_statistic(x, y,
                                                    statistic=np.nanmedian, bins=N_run)
        yplus, _, _ = binned_statistic(x, y,
                                       statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        yminus, _, _ = binned_statistic(x, y,
                                        statistic=lambda x: np.nanquantile(x, 0.16),  bins=N_run)
        xcentres = 0.5 * (xedges[1:] + xedges[:-1])

        counts, _ = np.histogram(x, bins=N_run)
        # mask = counts > 18
        mask = counts > 20 * np.diff(N_run)[0]

        boots = np.zeros((len(xedges) - 1, 3))
        for i in range(len(xedges) - 1):
            boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

        #no matching comparison
        y0medians, _, bin_is0 = binned_statistic(log_M200_0, y0_all,
                                           statistic=np.nanmedian, bins=N_run)
        y0plus, _, _ = binned_statistic(log_M200_0, y0_all,
                                       statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        y0minus, _, _ = binned_statistic(log_M200_0, y0_all,
                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)

        y1medians, _, bin_is1 = binned_statistic(log_M200_1, y1_all,
                                           statistic=np.nanmedian, bins=N_run)
        y1plus, _, _ = binned_statistic(log_M200_1, y1_all,
                                       statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        y1minus, _, _ = binned_statistic(log_M200_1, y1_all,
                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)

        #plot
        axs[0].errorbar(xcentres[mask], ymedians[mask],
                         c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        axs[0].errorbar(xcentres[mask], yplus[mask],
                         c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        axs[0].errorbar(xcentres[mask], yminus[mask],
                         c=color, ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        # axs[0].errorbar(xcentres[mask], y0medians[mask],
        #                  c='C0', ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        # axs[0].errorbar(xcentres[mask], y0plus[mask],
        #                  c='C0', ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        # axs[0].errorbar(xcentres[mask], y0minus[mask],
        #                  c='C0', ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        #
        # axs[0].errorbar(xcentres[mask], y1medians[mask],
        #                  c='C1', ls='-', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        # axs[0].errorbar(xcentres[mask], y1plus[mask],
        #                  c='C1', ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)
        # axs[0].errorbar(xcentres[mask], y1minus[mask],
        #                  c='C1', ls='--', linewidth=2, zorder=10, alpha=1, path_effects=white_path_effect_4)

        # axs[0].errorbar(xcentres[mask], y1medians[mask] - y0medians[mask],
        #                  c=color2, ls=(0,(5,3,1,3)), linewidth=2, zorder=30, alpha=1, path_effects=path_effect_4)

        if shade:
            # if z_index == z_index_highlight:
            #     axs[0].fill_between(xcentres[mask], yminus[mask], yplus[mask],
            #                         color=color, alpha=0.5, edgecolor='k', zorder=8)
            axs[0].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                                color=color, alpha=0.7, edgecolor='white', zorder=9, hatch='x' * (z_index + 1))

        # if z_index == z_index_highlight:
        axs[0].scatter(x, y,
                       color=color, marker='o', s=4, alpha=0.5,
                       linewidths=0, zorder=-20, rasterized=True)

        n_bins = 26 #16 #int(2*len(N_run))
        hex_bins = np.linspace(xlims[0]-(np.diff(xlims)[0] / n_bins), xlims[1], n_bins//2)
        hex_counts, _ = np.histogram(x, bins=hex_bins)
        hex_counts = np.concatenate((hex_counts, [1]))
        hex_bin_is = np.digitize(x, bins=hex_bins)
        n_bins = (n_bins, int(n_bins / np.sqrt(3)))  # (int(np.sqrt(3) * n_bins), n_bins) #
        axs[0].hexbin(x, y, C=1/hex_counts[hex_bin_is-1],
                      cmap='Greys',
                      reduce_C_function = lambda x: np.log10(np.nansum(x)),
                      extent=[*xlims, *ylims0], mincnt=2, gridsize=n_bins, zorder=-5)

        if disk_sample:
            ymedians_disk, xedges_disk, bin_is_disk = binned_statistic(x_disk, y_disk,
                                                        statistic=np.nanmedian, bins=N_run)
            # yplus, _, _ = binned_statistic(x_disk, y_disk,
            #                                statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            # yminus, _, _ = binned_statistic(x_disk, y_disk,
            #                                 statistic=lambda x: np.nanquantile(x, 0.16),  bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            mask_disk = counts > 10
            # mask_disk = counts > 1

            # boots = np.zeros((len(xedges) - 1, 3))
            # for i in range(len(xedges) - 1):
            #     boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

            #plot
            axs[0].errorbar(xcentres[mask_disk], ymedians_disk[mask_disk],
                             c=disk_color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            # if shade:
            #     # if z_index == z_index_highlight:
            #     #     axs[0].fill_between(xcentres[mask], yminus[mask], yplus[mask],
            #     #                         color=color, alpha=0.5, edgecolor='k', zorder=8)
            #     axs[0].fill_between(xcentres_disk[mask], boots_disk[mask, 0], boots_disk[mask, 2],
            #                         color=disk_color, alpha=0.5, edgecolor='k', zorder=9, hatch='x' * (z_index + 1))

            # if z_index == z_index_highlight:
            axs[0].scatter(x_disk, y_disk,
                           color=disk_color, marker='o', s=4, alpha=1,
                           linewidths=0, zorder=3, rasterized=True, path_effects=path_effect_1)

        #extras
        y = y1 - y0
        # y = np.log10(y1 / y0)
        # ymedians, xedges, bin_is = binned_statistic(x, y,
        #                                             statistic=np.nanmedian, bins=N_run)
        yplus, _, _ = binned_statistic(x, y,
                                       statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        yminus, _, _ = binned_statistic(x, y,
                                        statistic=lambda x: np.nanquantile(x, 0.16),  bins=N_run)
        
        #scatter
        axs[1].errorbar(xcentres[mask], (yplus[mask] - yminus[mask])/2,
                        c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

        # axs[1].errorbar(xcentres[mask],
        #                 np.sqrt(((y0plus[mask] - y0minus[mask])/2)**2 + ((y1plus[mask] - y1minus[mask])/2)**2),
        #                 c=color, ls='--', linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)
        axs[1].errorbar(xcentres[mask], (y0plus[mask] - y0minus[mask])/2,
                        c='C0', ls='-.', linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)
        axs[1].errorbar(xcentres[mask], (y1plus[mask] - y1minus[mask])/2,
                        c='C1', ls=(0,(0.8,0.8)), linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)

        #correlation
        not_crazy_mask = np.logical_and(np.isfinite(y0), np.isfinite(y1))
        y0nc = y0[not_crazy_mask]
        y1nc = y1[not_crazy_mask]
        bin_isnc = bin_is[not_crazy_mask]

        # #assumes central limit theorem
        # spearman = np.array([scipy.stats.spearmanr(y0nc[bin_isnc == i + 1], y1nc[bin_isnc == i + 1])[0]
        #                     for i in np.arange(len(xedges) - 1)[mask]])
        #assumes gaussian
        pearson  = np.array([scipy.stats.pearsonr(y0nc[bin_isnc == i + 1], y1nc[bin_isnc == i + 1])[0]
                            for i in np.arange(len(xedges) - 1)[mask]])

        # axs[2].errorbar(xcentres[mask], spearman,
        #                 c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
        axs[2].errorbar(xcentres[mask], pearson,
                        c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

        #decorations
        m200_intercept_0 = m200_heating_intercept(z, 1)
        m200_intercept_1 = m200_heating_intercept(z, 0)

        for i in range(3):
            axs[i].axvline(m200_intercept_1, c='C1', ls=(0,(0.8,0.8)), linewidth=1, zorder=-3, alpha=1)
            axs[i].axvline(m200_intercept_0, c='C0', ls='-.', linewidth=1, zorder=-3, alpha=1)

        axs[0].errorbar(xlims, [0]*2,
                        c='k', ls=':', lw=1, zorder=-3, alpha=1)#, path_effects=white_path_effect_3)
        axs[2].errorbar(xlims, [0]*2,
                        c='k', ls=':', lw=1, zorder=-10, alpha=1)

        log_M200s = np.linspace(*xlims, 101)  # log M_sun
        # cosmology
        H_z2 = HUBBLE_CONST ** 2 * ((1 - Omega_m) + Omega_m * np.power(z + 1, 3))  # km^2/s^2 / kpc^2
        rho_crit = 3 * H_z2 / (8 * np.pi * GRAV_CONST)  # 10^10 M_sun / kpc^3
        rho_200 = 200 * rho_crit
        r200 = np.power(3 * 10 ** (log_M200s - 10) / (4 * np.pi * rho_200), 1 / 3)  # kpc
        V200 = np.sqrt(GRAV_CONST * 10 ** (log_M200s - 10) / r200)  # km/s

        if False:
            if s_tot or ek:
                for mult in [1, 0.1]:
                    # axs[0].errorbar(log_M200s, V200*mult,
                    #                     color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)
                    # axs[0].errorbar(log_M200s, -V200*mult,
                    #                     color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)
                    axs[1].errorbar(log_M200s, V200*mult,
                                        color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)

                # index = int(0.7/8*len(log_M200s))
                # slope = (np.diff(V200) / np.diff(log_M200s))[index + 8]
                # axs[0].text(log_M200s[index],
                #             V200[index] - 0.03 * (ylims0[1] - ylims0[0]),
                #             r'$V_{200}$', rotation_mode='anchor', ha='left', va='top',
                #             rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims0))*180/np.pi)[0],
                #             color='k', path_effects=white_path_effect_3)
                # index = int(5.2/8*len(log_M200s))
                # slope = (np.diff(0.1 * V200) / np.diff(log_M200s))[index + 16]
                # axs[0].text(log_M200s[index],
                #             0.1 * V200[index] + 0.005 * (ylims0[1] - ylims0[0]),
                #             r'$0.1 \times V_{200}$', rotation_mode='anchor', ha='left', va='bottom',
                #             rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims0))*180/np.pi)[0],
                #             color='k', path_effects=white_path_effect_3)
                index = int(3.5/8*len(log_M200s))
                slope = (np.diff(0.1 * V200) / np.diff(log_M200s))[index + 11]
                height_ratio = 0.5
                axs[1].text(log_M200s[index],
                            0.1 * V200[index] - 0.05 * (ylims1[1] - ylims1[0]),
                            r'$0.1 \times V_{200}$', rotation_mode='anchor', ha='left', va='top',
                            rotation=(np.arctan2(np.diff(xlims) * slope * height_ratio, np.diff(ylims1))*180/np.pi)[0],
                            color='k', path_effects=white_path_effect_3)

            if size:
                for mult in [0.1, 0.01]:
                    # axs[0].errorbar(log_M200s, r200*mult,
                    #                     color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)
                    # axs[0].errorbar(log_M200s, -r200*mult,
                    #                     color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)
                    axs[1].errorbar(log_M200s, r200*mult,
                                        color='k', ls='-.', lw=1, zorder=3, path_effects=white_path_effect_3)

                # index = int(0/8*len(log_M200s))
                # slope = -(np.diff(0.1 * r200) / np.diff(log_M200s))[index + 4]
                # axs[0].text(log_M200s[index] - 0.005 * (xlims[1] - xlims[0]),
                #             -0.1 * r200[index] + 0.07 * (ylims0[1] - ylims0[0]),
                #             r'$-0.1 \times r_{200}$', rotation_mode='anchor', ha='left', va='bottom',
                #             rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims0))*180/np.pi)[0],
                #             color='k', path_effects=white_path_effect_3)
                # index = int(5.2/8*len(log_M200s))
                # slope = (np.diff(0.01 * r200) / np.diff(log_M200s))[index + 16]
                # axs[0].text(log_M200s[index],
                #             0.01 * r200[index] + 0.005 * (ylims0[1] - ylims0[0]),
                #             r'$0.01 \times r_{200}$', rotation_mode='anchor', ha='left', va='bottom',
                #             rotation=(np.arctan2(np.diff(xlims) * slope, np.diff(ylims0))*180/np.pi)[0],
                #             color='k', path_effects=white_path_effect_3)
                index = int(3.5/8*len(log_M200s))
                slope = (np.diff(0.01 * r200) / np.diff(log_M200s))[index + 14]
                height_ratio = 0.5
                axs[1].text(log_M200s[index],
                            0.01 * r200[index] + 0.01 * (ylims1[1] - ylims1[0]),
                            r'$0.01 \times r_{200}$', rotation_mode='anchor', ha='left', va='bottom',
                            rotation=(np.arctan2(np.diff(xlims) * slope * height_ratio, np.diff(ylims1))*180/np.pi)[0],
                            color='k', path_effects=white_path_effect_3)

    # axs[0].legend([(lines.Line2D([0, 1], [0, 1], color=color, ls='-', lw=2, path_effects=white_path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color=color2, ls=(0,(5,3,1,3)), lw=2, path_effects=path_effect_4)),
    #                ],
    #               # ['Median difference', 'Difference of medians'],)
    #               [r'$\widetilde{{\rm LR} - {\rm H}}{\rm R}$',
    #                r'$\widetilde{\rm F}{\rm R} - \widetilde{\rm H}{\rm R}$'],)
    #               # loc='upper right')

    if s_tot:
        axs[1].legend([(lines.Line2D([0, 1], [0, 1], color=color, ls='-', lw=2, path_effects=path_effect_4)),
                       (lines.Line2D([0, 1], [0, 1], color='C0', ls='-.', lw=1, path_effects=path_effect_2)),
                       (lines.Line2D([0, 1], [0, 1], color='C1', ls=(0, (0.8, 0.8)), lw=1, path_effects=path_effect_2)),
                       ],
                      ['HR-LR', 'HR', 'LR'],
                      loc='upper left', labelspacing=0.2)#, handletextpad=0.4,)

    # axs[2].legend([(lines.Line2D([0, 1], [0, 1], color=color, ls='-', lw=2, path_effects=path_effect_4)),
    #                (lines.Line2D([0, 1], [0, 1], color=color, ls=':', lw=1, path_effects=path_effect_2)),
    #                ],
    #               ['Spearman', 'Pearson'],)
    #               # loc='upper right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

        # fig0.savefig(name[:-4] + '0' + name[-4:], bbox_inches='tight', dpi=200)
        # fig1.savefig(name[:-4] + '1' + name[-4:], bbox_inches='tight', dpi=200)
        #
        # plt.close()
        # plt.close()

    return


def matched_appendix_plots(name='', save=False, shade=False, z_index=0, disk_sample=False, r12=False,
                           mass=False, kinematics=False, size=False, am=False, shape=False, kin_ind=False, orb_ind=False):

    '''
1  1 $M_{200}$
2  1 $M_\star$
3  1 $M_{\rm gas}$
4  1 $M_{\rm BH}$
5  2 $\overline{v}_\phi (r_{1/2})$
6  2 $\overline{v}_\phi$
7  2 $\sigma_{\rm tot} (r_{1/2})$
8  2 $\sigma_{\rm tot}$
9  3 $R_{1/2}$
10 3 $z_{1/2}$
11 4 $j_z$
12 5 $z_{1/2} / R_{1/2}$
13 5 $c/a$
14 6 $\overline{v}_phi / \sigma_{\rm 1D}$
15 6 $\lambda_{\rm Re}$
16 6 $\kappa_{\rm rot}$
17 6 $\kappa_{\rm co}$
18 7 $D/T$
19 7 $S/T$
    '''

    N_res = 10

    if mass:
        ny = 4
    elif kinematics:
        ny = 4
    elif size:
        ny = 3
    elif am:
        ny = 1
    elif shape:
        ny = 2
    elif kin_ind:
        ny = 4
    elif orb_ind:
        ny = 2
    else:
        raise ValueError

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(3.3, 3.3 * (ny+1) + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=ny+1, height_ratios=[1]*(ny+1))
    # spec = fig.add_gridspec(ncols=1, nrows=ny, height_ratios=[1]*ny + [0.5])

    xlabel = r'$\log \, M_{200}^{\rm HR} / $M$_\odot$'
    xlims = [10, 14]

    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.75, 14, 18)
    N_run = np.linspace(9 + 2 / 3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    color = matplotlib.cm.get_cmap('inferno_r')((3 + z_index) / 6)
    color2 = matplotlib.cm.get_cmap('inferno_r')((0 + z_index) / 6)
    # color = 'C2'
    disk_color = matplotlib.cm.get_cmap('viridis_r')((0 + z_index) / 6)

    ylimss = []
    ylabels = []
    kwargss = []
    if mass:
        ylabel0 = r'$\log \, M_{200}^{\rm LR} / M_{200}^{\rm HR}$'
        # ylabel0 = r'$\log \, M_{\rm DM}$'
        ylabel1 = r'$\log \, M_\star^{\rm LR} / M_{\rm BH}^{\rm HR}$'
        ylabel2 = r'$\log \, M_{\rm gas}^{\rm LR} / M_{\rm BH}^{\rm HR}$'
        ylabel3 = r'$\log \, M_{\rm BH}^{\rm LR} / M_{\rm BH}^{\rm HR}$'
        ylabels = [ylabel0, ylabel1, ylabel2, ylabel3]
        ylims0 = [-0.5,0.5]
        ylims1 = [-0.5,0.5]
        ylims2 = [-0.5,0.5]
        ylims3 = [-0.5,0.5]
        ylimss = [ylims0, ylims1, ylims2, ylims3]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.1), 'ls':'-', 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.4), 'ls':'-', 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.6), 'ls':'-', 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.9), 'ls':'-', 'lw':2, 'path_effects':white_path_effect_4}]

    elif kinematics:
        ylabel0 = r'$\log \, \overline{v}_\phi^{\rm LR} / \overline{v}_\phi^{\rm HR}$'
        ylabel1 = r'$\log \, \overline{v}_\phi^{\rm LR} (r_{1/2}) / \overline{v}_\phi^{\rm HR} (r_{1/2})$'
        ylabel2 = r'$\log \, \sigma_{\rm tot}^{\rm LR} / \sigma_{\rm tot}^{\rm HR}$'
        ylabel3 = r'$\log \, \sigma_{\rm tot}^{\rm LR} (r_{1/2}) / \sigma_{\rm tot}^{\rm HR} (r_{1/2})$'
        ylabels = [ylabel0, ylabel1, ylabel2, ylabel3]
        ylims0 = [-1,1]
        ylims1 = [-1,1]
        ylims2 = [-1,1]
        ylims3 = [-1,1]
        ylimss = [ylims0, ylims1, ylims2, ylims3]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.0), 'ls':'--', 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.3), 'ls':'--', 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.5), 'ls':'--', 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.8), 'ls':'--', 'lw':2, 'path_effects':white_path_effect_4}]

    elif size:
        ylabel0 = r'$\log \, R_{1/2}^{\rm LR} / R_{1/2}^{\rm HR}$'
        ylabel1 = r'$\log \, z_{1/2}^{\rm LR} / z_{1/2}^{\rm LR}$'
        ylabel2 = r'$\log \, z_{1/2}^{\rm LR} (R_{1/2}) / z_{1/2}^{\rm LR} (R_{1/2})$'
        ylabels = [ylabel0, ylabel1, ylabel2]
        ylims0 = [-1,1]
        ylims1 = [-1,1]
        ylims2 = [-1,1]
        ylimss = [ylims0, ylims1, ylims2]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.2), 'ls':(0,(1,1)), 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(1.0), 'ls':(0,(1,1)), 'lw':2, 'path_effects':white_path_effect_4},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.65), 'ls':(0,(1,1)), 'lw':2, 'path_effects':white_path_effect_4}]

    elif am:
        ylabel0 = r'$\log \, j_\star^{\rm LR} / j_\star^{\rm HR}$'
        ylabels = [ylabel0]
        ylims0 = [-1.5,1.5]
        ylimss = [ylims0]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.7), 'ls':'-.', 'lw':2, 'path_effects':white_path_effect_4}]

    elif shape:
        ylabel0 = r'$z_{1/2}^{\rm LR} / R_{1/2}^{\rm LR} - z_{1/2}^{\rm HR} / R_{1/2}^{\rm HR}$'
        ylabel1 = r'$c/a^{\rm LR} - c/a^{\rm HR}$'
        ylabels = [ylabel0, ylabel1]
        ylims0 = [-1,1]
        ylims1 = [-1,1]
        ylimss = [ylims0, ylims1]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.00), 'ls':(0,(1,1)), 'lw':3},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.57), 'ls':(0,(1,1)), 'lw':3}]

    elif kin_ind:
        ylabel0 = r'$v^{\rm LR}/\sigma^{\rm LR} - v^{\rm HR}/\sigma^{\rm HR}$'
        ylabel1 = r'$\lambda_r^{\rm LR} - \lambda_r^{\rm HR}$'
        ylabel2 = r'$\kappa_{\rm rot}^{\rm LR} - \kappa_{\rm rot}^{\rm HR}$'
        ylabel3 = r'$\kappa_{\rm co}^{\rm LR} - \kappa_{\rm co}^{\rm HR}$'
        ylabels = [ylabel0, ylabel1, ylabel2, ylabel3]
        ylims0 = [-1,1]
        ylims1 = [-1,1]
        ylims2 = [-1,1]
        ylims3 = [-1,1]
        ylimss = [ylims0, ylims1, ylims2, ylims3]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.14), 'ls':'-', 'lw':3},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.43), 'ls':'-', 'lw':3},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.71), 'ls':'-', 'lw':3},
                   {'c':matplotlib.cm.get_cmap('turbo')(1.00), 'ls':'-', 'lw':3}]

    elif orb_ind:
        ylabel0 = r'${\rm D/T}^{\rm LR} - {\rm D/T}^{\rm HR}$'
        ylabel1 = r'${\rm S/T}^{\rm LR} - {\rm S/T}^{\rm HR}$'
        ylabels = [ylabel0, ylabel1]
        ylims0 = [-1,1]
        ylims1 = [-1,1]
        ylimss = [ylims0, ylims1]
        kwargss = [{'c':matplotlib.cm.get_cmap('turbo')(0.29), 'ls':'--', 'lw':3},
                   {'c':matplotlib.cm.get_cmap('turbo')(0.86), 'ls':'--', 'lw':3}]

    ylabels.append('correlation')
    ylimss.append([-0.5, 1])

    axs = []
    for row in range(ny+1):
        axs.append(fig.add_subplot(spec[row]))
        axs[row].set_xlim(xlims)
        axs[row].set_ylim(ylimss[row])

        axs[row].set_ylabel(ylabels[row])

        if row != ny:
            axs[row].set_xticklabels([])
        else:
            axs[row].set_xlabel(xlabel)

        # if row == 0:
        axs[row].set_aspect(np.diff(xlims) / np.diff(ylimss[row]))
        # else:
        #     axs[row].set_aspect(np.diff(xlims) / np.diff(ylimss[row]) / 2)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    # for z_index in [z_index]:  # [3,2,1,0]: #range(4):
    z = [0, 0.503, 1.004, 2.012][z_index]

    y0s = []
    y1s = []

    # z=0 output only
    with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
        # centrals only
        mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
        # mask0 = np.logical_and(mask0,
        #                        raw_data0[keys.key0_dict['kappa_co']['Star']][:,
        #                        keys.rkey_to_rcolum_dict[rad_key, None]] > 0.4)
        gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

        # load
        M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]

        if mass:
            y00 = M200_0[mask0]
            y01 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y02 = raw_data0[keys.key0_dict['mgas'][None]][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y03 = raw_data0[keys.key0_dict['mbh'][None]][:][mask0]
            # y0s = [y00, y01, y02, y03]
            y0s = [np.log10(y00), np.log10(y01), np.log10(y02), np.log10(y03)]

        elif kinematics:
            sz0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            sR0 = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            sphi0 = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]

            szr = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask0]
            sRr = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask0]
            sphir = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask0]

            y00 = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y01 = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask0]
            y02 = np.sqrt(sz0**2 + sR0**2 + sphi0**2)
            y03 = np.sqrt(szr**2 + sRr**2 + sphir**2)
            # y0s = [y00, y01, y02, y03]
            y0s = [np.log10(y00), np.log10(y01), np.log10(y02), np.log10(y03)]

        elif size:
            y00 = raw_data0[keys.key0_dict['R12']['Star']][:][mask0]
            y01 = raw_data0[keys.key0_dict['z12']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y02 = raw_data0[keys.key0_dict['z12']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask0]
            # y0s = [y00, y01, y02]
            y0s = [np.log10(y00), np.log10(y01), np.log10(y02)]

        elif am:
            # y00 = raw_data0[keys.key0_dict['jz']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y00 = raw_data0[keys.key0_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            # y0s = [y00]
            y0s = [np.log10(y00)]

        elif shape:
            ylabel0 = r'$z_{1/2}^{\rm LR} / R_{1/2}^{\rm LR} - z_{1/2}^{\rm HR} / R_{1/2}^{\rm HR}$'
            ylabel1 = r'$c/a^{\rm LR} - c/a^{\rm HR}$'

            y00 = raw_data0[keys.key0_dict['z12']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y00 /= raw_data0[keys.key0_dict['R12']['Star']][:][mask0]
            y01 = raw_data0[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y01 /= raw_data0[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask0]
            y0s = [y00, y01]

        elif kin_ind:
            if r12:
                rad_key = 'eq_r12'
            else:
                rad_key = 'r200'

            vphi0 = raw_data0[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask0]
            
            sz0 = raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask0]
            sR0 = raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask0]
            sphi0 = raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask0]
            
            y00 = vphi0 / np.sqrt((sz0**2 + sR0**2 + sphi0**2) / 3)
            y01 = vphi0 / np.sqrt(vphi0**2 + (sz0**2 + sR0**2 + sphi0**2) / 3)
            y02 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask0]
            y03 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask0]
            y0s = [y00, y01, y02, y03]

        elif orb_ind:
            fjzonc_0 = raw_data0['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict['r200', None], :][mask0]

            # > 0.7
            n = -round((1 - 0.7) / (2/(keys.n_jzjc_bins - 1))+1)
            y00 = fjzonc_0[:, n]
            y01 =  1 - 2*(1-fjzonc_0[:, (keys.n_jzjc_bins - 1)//2])
            y0s = [y00, y01]

        log_M200_0 = np.log10(M200_0[mask0]) + 10  # log Mstar
        gn0 = gn0[mask0]
        
    with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
        # centrals only
        mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
        # mask1 = np.logical_and(mask1,
        #                        raw_data1[keys.key0_dict['kappa_co']['Star']][:,
        #                        keys.rkey_to_rcolum_dict[rad_key, None]] > 0.4)
        gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

        # load
        M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]

        if mass:
            y10 = M200_1[mask1]
            y11 = raw_data1[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y12 = raw_data1[keys.key0_dict['mgas'][None]][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y13 = raw_data1[keys.key0_dict['mbh'][None]][:][mask1]
            # y1s = [y10, y11, y12, y13]
            y1s = [np.log10(y10), np.log10(y11), np.log10(y12), np.log10(y13)]

        elif kinematics:
            sz1 = raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            sR1 = raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            sphi1 = raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]

            szr = raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask1]
            sRr = raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask1]
            sphir = raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask1]

            y10 = raw_data1[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y11 = raw_data1[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask1]
            y12 = np.sqrt(sz1**2 + sR1**2 + sphi1**2)
            y13 = np.sqrt(szr**2 + sRr**2 + sphir**2)
            # y1s = [y10, y11, y12, y13]
            y1s = [np.log10(y10), np.log10(y11), np.log10(y12), np.log10(y13)]

        elif size:
            y10 = raw_data1[keys.key0_dict['R12']['Star']][:][mask1]
            y11 = raw_data1[keys.key0_dict['z12']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y12 = raw_data1[keys.key0_dict['z12']['Star']][:, keys.rkey_to_rcolum_dict['eq_r12', None]][mask1]
            # y1s = [y10, y11, y12]
            y1s = [np.log10(y10), np.log10(y11), np.log10(y12)]

        elif am:
            # y10 = raw_data1[keys.key0_dict['jz']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y10 = raw_data1[keys.key0_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            # y1s = [y10]
            y1s = [np.log10(y10)]

        elif shape:
            ylabel0 = r'$z_{1/2}^{\rm LR} / R_{1/2}^{\rm LR} - z_{1/2}^{\rm HR} / R_{1/2}^{\rm HR}$'
            ylabel1 = r'$c/a^{\rm LR} - c/a^{\rm HR}$'

            y10 = raw_data1[keys.key0_dict['z12']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y10 /= raw_data1[keys.key0_dict['R12']['Star']][:][mask1]
            y11 = raw_data1[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y11 /= raw_data1[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]][mask1]
            y1s = [y10, y11]

        elif kin_ind:
            if r12:
                rad_key = 'eq_r12'
            else:
                rad_key = 'r200'

            vphi1 = raw_data1[keys.key0_dict['mean_v_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask1]

            sz1 = raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask1]
            sR1 = raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask1]
            sphi1 = raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask1]

            y10 = vphi1 / np.sqrt((sz1**2 + sR1**2 + sphi1**2) / 3)
            y11 = vphi1 / np.sqrt(vphi1**2 + (sz1**2 + sR1**2 + sphi1**2) / 3)
            y12 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask1]
            y13 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]][mask1]
            y1s = [y10, y11, y12, y13]

        elif orb_ind:
            fjzonc_0 = raw_data1['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict['r200', None], :][mask1]

            # > 0.7
            n = -round((1 - 0.7) / (2/(keys.n_jzjc_bins - 1))+1)
            y10 = fjzonc_0[:, n]
            y11 =  1 - 2*(1-fjzonc_0[:, (keys.n_jzjc_bins - 1)//2])
            y1s = [y10, y11]

        log_M200_1 = np.log10(M200_1[mask1]) + 10  # log Mstar
        gn1 = gn1[mask1]

    with h5.File(matched_groups_files[z_index], 'r') as match_data:
        matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
        matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

    # not all haloes have stars
    ni0 = np.isin(matched_gn_0, gn0)
    ni1 = np.isin(matched_gn_1, gn1)
    ni = np.logical_and(ni0, ni1)
    matched_gn_0 = matched_gn_0[ni]
    matched_gn_1 = matched_gn_1[ni]

    # not all haloes matched with stars
    in0 = np.isin(gn0, matched_gn_0)
    in1 = np.isin(gn1, matched_gn_1)

    order0 = np.argsort(np.argsort(matched_gn_0))  # should be same as np.arange(np.sum(ismatched0))
    order1 = np.argsort(np.argsort(matched_gn_1))

    # this also works
    # order0 = np.arange(len(matched_gn_0))
    # arg = np.argsort(np.argsort(matched_gn_1))
    # order1 = matched_gn_1 - np.cumsum(np.concatenate([[0,], np.diff(np.unique(matched_gn_1))-1]))[arg] - 1

    for i in range(len(y0s)):
        y0s[i] = y0s[i][in0][order0]

    m_0 = log_M200_0[in0][order0]
    # cop0 = cop0[in0][order0]
    gn0 = gn0[in0][order0]

    for i in range(len(y1s)):
        y1s[i] = y1s[i][in1][order1]

    m_1 = log_M200_1[in1][order1]
    # cop1 = cop1[in1][order1]
    gn1 = gn1[in1][order1]

    # # for disk sample
    # if disk_sample:
    #     kappa_co_cut = 0.4
    #
    #     # matched sample
    #     k0 = kappa_0[mask0][in0][order0]
    #     k1 = kappa_1[mask1][in1][order1]
    #
    #     match_disk_mask = np.logical_and(k0 > kappa_co_cut, k1 > kappa_co_cut)
    #     # match_disk_mask = np.logical_and(DT_0[in0][order0] > 0.5, DT_1[in1][order1] > 0.5)
    #
    #     y0_disk = y0[match_disk_mask]
    #     m_0_disk = m_0[match_disk_mask]
    #     y1_disk = y1[match_disk_mask]
    #     m_1_disk = m_1[match_disk_mask]
    #
    #     y_disk = y1_disk - y0_disk
    #     x_disk = np.log10((10 ** m_0_disk + 10 ** m_1_disk) / 2)

    # what to actually plot
    for row in range(ny):
        y = y1s[row] - y0s[row]
        # y = np.log10(y1 / y0)
        # x = np.log10((10**m_0 + 10**m_1) / 2)
        x = m_0

        # calc medians etc.
        ymedians, xedges, bin_is = binned_statistic(x, y,
                                                    statistic=np.nanmedian, bins=N_run)
        yplus, _, _ = binned_statistic(x, y,
                                       statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
        yminus, _, _ = binned_statistic(x, y,
                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
        xcentres = 0.5 * (xedges[1:] + xedges[:-1])

        counts, _ = np.histogram(x, bins=N_run)
        # mask = counts > 18
        mask = counts > 20 * np.diff(N_run)[0]

        boots = np.zeros((len(xedges) - 1, 3))
        for i in range(len(xedges) - 1):
            boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

        # plot
        axs[row].errorbar(xcentres[mask], ymedians[mask],
                          **kwargss[row])

        axs[row].errorbar(xcentres[mask], yplus[mask],
                          **kwargss[row])
        axs[row].errorbar(xcentres[mask], yminus[mask],
                          **kwargss[row])

        if shade:
            axs[row].fill_between(xcentres[mask], boots[mask, 0], boots[mask, 2],
                                  )#color=color, alpha=0.7, edgecolor='white', zorder=9, hatch='x' * (z_index + 1))

        axs[row].scatter(x, y,
                         color=kwargss[row]['c'], marker='o', s=4, alpha=0.5,
                         linewidths=0, zorder=-20, rasterized=True)

        n_bins = 26  # 16 #int(2*len(N_run))
        hex_bins = np.linspace(xlims[0] - (np.diff(xlims)[0] / n_bins), xlims[1], n_bins // 2)
        hex_counts, _ = np.histogram(x, bins=hex_bins)
        hex_counts = np.concatenate((hex_counts, [1]))
        hex_bin_is = np.digitize(x, bins=hex_bins)
        n_bins = (n_bins, int(n_bins / np.sqrt(3)))  # (int(np.sqrt(3) * n_bins), n_bins) #
        axs[row].hexbin(x, y, C=1 / hex_counts[hex_bin_is - 1],
                        cmap='Greys',
                        reduce_C_function=lambda x: np.log10(np.nansum(x)),
                        extent=[*xlims, *ylimss[row]], mincnt=2, gridsize=n_bins, zorder=-5)

        #calculate correlation
        not_crazy_mask = np.logical_and(np.isfinite(y0s[row]), np.isfinite(y1s[row]))
        y0nc = y0s[row][not_crazy_mask]
        y1nc = y1s[row][not_crazy_mask]
        bin_isnc = bin_is[not_crazy_mask]

        # #assumes central limit theorem
        # spearman = np.array([scipy.stats.spearmanr(y0nc[bin_isnc == i + 1], y1nc[bin_isnc == i + 1])[0]
        #                     for i in np.arange(len(xedges) - 1)[mask]])
        # assumes gaussian
        pearson = np.array([scipy.stats.pearsonr(y0nc[bin_isnc == i + 1], y1nc[bin_isnc == i + 1])[0]
                            for i in np.arange(len(xedges) - 1)[mask]])

        # axs[ny].errorbar(xcentres[mask], spearman,
        #                 c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
        axs[ny].errorbar(xcentres[mask], pearson,
                         **kwargss[row])

        # decorations
        m200_intercept_0 = m200_heating_intercept(z, 1)
        m200_intercept_1 = m200_heating_intercept(z, 0)

        axs[row].axvline(m200_intercept_1, c='C1', ls=(0, (0.8, 0.8)), linewidth=1, zorder=-3, alpha=1)
        axs[row].axvline(m200_intercept_0, c='C0', ls='-.', linewidth=1, zorder=-3, alpha=1)

        axs[row].errorbar(xlims, [0] * 2,
                        c='k', ls=':', lw=1, zorder=-3, alpha=1)

    axs[ny].errorbar(xlims, [0] * 2,
                     c='k', ls=':', lw=1, zorder=-3, alpha=1)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def matched_mass_all_indicators(name='', save=False, shade=False, r12=True, z_index=0):
    N_res = 10
    z_index_highlight = z_index
    # color = matplotlib.cm.get_cmap('inferno_r')((1.5 + z_index)/5)

    fig = plt.figure(constrained_layout=True)

    fig.set_size_inches(3.3, 3.3 * 2 + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=3, height_ratios=[1, *([0.5]*2)])

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims0 = [1e-3 + -0.4, 0.6 - 1e-3]
    ylims1 = [1e-3 + 0, 0.5 - 1e-3]
    ylims2 = [1e-3 + -0.5, 1 - 1e-3]
    ylimss = [ylims0, ylims1, ylims2]

    # N_run = np.linspace(9.5, 14, 10)
    N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.9, 14, 43)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel0 =r'$\Delta X$'
    ylabel1 = r'scatter' #r'scatter $[(X_{84} - X_{16}) / 2]$'
    ylabel2 = r'correlation'
    ylabels = [ylabel0, ylabel1, ylabel2]

    axs = []
    for row in range(3):
        axs.append(fig.add_subplot(spec[row]))
        axs[row].set_xlim(xlims)
        axs[row].set_ylim(ylimss[row])

        axs[row].set_ylabel(ylabels[row])

        if row != 2: axs[row].set_xticklabels([])
        else: axs[row].set_xlabel(xlabel)

        if row == 0: axs[row].set_aspect(np.diff(xlims)/np.diff(ylimss[row]))
        else: axs[row].set_aspect(np.diff(xlims)/np.diff(ylimss[row])/2)

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if r12:
        rad_key = 'eq_r12'
    else:
        rad_key = 'r200'

    for z_index in [z_index]: #range(4):
        color = matplotlib.cm.get_cmap('inferno_r')((3 + z_index) / 6)
        # color = 'C2'
        z = [0, 0.503, 1.004, 2.012][z_index]

        #z=0 output only
        with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
            # centrals only
            mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
            gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

            #load
            M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
            r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]

            mstar_0 = raw_data0[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            r12_0 = raw_data0[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

            mgas_0 = raw_data0[keys.key0_dict['mgasr'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

            # mbh = raw_data0['GalaxyQuantities/BHBinGravMass'][()] # 10^10 M_sun
            mbh_0 = raw_data0['GalaxyQuantities/BHGravMass'][()] # 10^10 M_sun
            # mbh_0 = raw_data0['GalaxyQuantities/BHMass'][()] # 10^10 M_sun

            j_0 = raw_data0[keys.key0_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
            jq0 = raw_data0[keys.key1_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
            j_0 = j_0 / jq0

            sigma_0 = np.sqrt(raw_data0[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                              raw_data0[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                              raw_data0[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2)

            ssfr_0 = (raw_data0[keys.key0_dict['sfr'][None]][:, keys.rkey_to_rcolum_dict['r200', None], :
                   ] / mstar_0[:, np.newaxis])[:, 1]

            z12R12_0 = raw_data0[keys.key0_dict['z12R12']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            z12R12_0 /= raw_data0[keys.key1_dict['z12R12']['Star']][:]
            z12R12_0 = z12R12_0[mask0]
            ca_0 = raw_data0[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            ca_0 /= raw_data0[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            ca_0 = ca_0[mask0]
            kappa_co_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            kappa_co_0 = kappa_co_0[mask0]
            kappa_rot_0 = raw_data0[keys.key0_dict['kappa_rot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            kappa_rot_0 = kappa_rot_0[mask0]
            v_sigma_0 = (raw_data0[keys.key3_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] /
                np.sqrt((raw_data0[keys.key0_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                         raw_data0[keys.key1_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                         raw_data0[keys.key2_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2)/3))
            v_sigma_0 = v_sigma_0[mask0]

            fjzonc_0 = raw_data0['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict[rad_key, None], :]
            fjzonc_0 = fjzonc_0[mask0]

            # > 0.7
            n = -round((1 - 0.7) / (2 / (keys.n_jzjc_bins - 1)) + 1)
            DT_0 = fjzonc_0[:, n]
            ST_0 = 2 * (1 - fjzonc_0[:, (keys.n_jzjc_bins - 1) // 2])

            #mask and units
            log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
            # y0_all = [z12R12_0, ca_0, kappa_co_0, kappa_rot_0, v_sigma_0, DT_0, ST_0]
            # legend_all = [r'$z_{1/2}/R_{1/2}$', r'$c/a$', r'$\kappa_{\rm co}$', r'$\kappa_{\rm rot}$',
            #               r'$v/\sigma_{\rm 1D}$', r'D/T', r'S/T']

            # y0_all = [np.log10(M200_0[mask0]),
            #           np.log10(mstar_0[mask0]),
            #           np.log10(r12_0[mask0]),
            #           np.log10(j_0[mask0]),
            #           np.log10(sigma_0[mask0]),
            #           np.log10(ssfr_0[mask0] + 1e-5),]
            # legend_all = [r'$\log \, M_{200}$', 
            #               r'$\log \, M_{\star}$',
            #               r'$\log \, r_{1/2}$',
            #               r'$\log \, j_\star$',
            #               r'$\log \, \sigma_{\rm tot} (r_{1/2})$',
            #               r'$\log \, $sSFR (1 Gyr)',]

            y0_all = [np.log10(M200_0[mask0]),
                      np.log10(mstar_0[mask0]),
                      np.log10(mgas_0[mask0]),
                      np.log10(mbh_0[mask0]),]
            legend_all = [r'$\log \, M_{200}$', 
                          r'$\log \, M_{\star}$',
                          r'$\log \, M_{\rm gas}$',
                          r'$\log \, M_{\rm BH}$',]
            
            gn0 = gn0[mask0]

        with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
            # centrals only
            mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
            gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

            # load
            M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
            r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]

            mstar_1 = raw_data1[keys.key0_dict['mstar'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]
            r12_1 = raw_data1[keys.key0_dict['r12r']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]

            mgas_1 = raw_data1[keys.key0_dict['mgasr'][None]][:, keys.rkey_to_rcolum_dict['r200', None]]

            # mbh = raw_data0['GalaxyQuantities/BHBinGravMass'][()] # 10^10 M_sun
            mbh_1 = raw_data1['GalaxyQuantities/BHGravMass'][()] # 10^10 M_sun
            # mbh_1 = raw_data1['GalaxyQuantities/BHMass'][()] # 10^10 M_sun

            j_1 = raw_data1[keys.key0_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
            jq1 = raw_data1[keys.key1_dict['jtot']['Star']][:, keys.rkey_to_rcolum_dict['r200', None]]
            j_1 = j_1 / jq1

            sigma_1 = np.sqrt(raw_data1[keys.key0_dict['sigma_z']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                              raw_data1[keys.key0_dict['sigma_R']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                              raw_data1[keys.key0_dict['sigma_phi']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2)

            ssfr_1 = (raw_data1[keys.key0_dict['sfr'][None]][:, keys.rkey_to_rcolum_dict['r200', None], :
                   ] / mstar_1[:, np.newaxis])[:, 1]

            z12R12_1 = raw_data1[keys.key0_dict['z12R12']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            z12R12_1 /= raw_data1[keys.key1_dict['z12R12']['Star']][:]
            z12R12_1 = z12R12_1[mask1]
            ca_1 = raw_data1[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            ca_1 /= raw_data1[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            ca_1 = ca_1[mask1]
            kappa_co_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            kappa_co_1 = kappa_co_1[mask1]
            kappa_rot_1 = raw_data1[keys.key0_dict['kappa_rot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
            kappa_rot_1 = kappa_rot_1[mask1]
            v_sigma_1 = (raw_data1[keys.key3_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] /
                np.sqrt((raw_data1[keys.key0_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                         raw_data1[keys.key1_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                         raw_data1[keys.key2_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2)/3))
            v_sigma_1 = v_sigma_1[mask1]

            fjzonc_1 = raw_data1['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict[rad_key, None], :]
            fjzonc_1 = fjzonc_1[mask1]

            # > 0.7
            n = -round((1 - 0.7) / (2 / (keys.n_jzjc_bins - 1)) + 1)
            DT_1 = fjzonc_1[:, n]
            ST_1 = 2 * (1 - fjzonc_1[:, (keys.n_jzjc_bins - 1) // 2])

            #mask and units
            log_M200_1 = np.log10(M200_1[mask1]) + 10 # log Mstar
            # y1_all = [z12R12_1, ca_1, kappa_co_1, kappa_rot_1, v_sigma_1, DT_1, ST_1]
            gn1 = gn1[mask1]

            # y1_all = [z12R12_1, ca_1, kappa_co_1, kappa_rot_1, v_sigma_1, DT_1, ST_1]
            # y1_all = [np.log10(M200_1[mask1]),
            #           np.log10(mstar_1[mask1]),
            #           np.log10(r12_1[mask1]),
            #           np.log10(j_1[mask1]),
            #           np.log10(sigma_1[mask1]),
            #           np.log10(ssfr_1[mask1] + 1e-5),]

            y1_all = [np.log10(M200_1[mask1]),
                      np.log10(mstar_1[mask1]),
                      np.log10(mgas_1[mask1]),
                      np.log10(mbh_1[mask1]),]

        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        #not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        #not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0))
        order1 = np.argsort(np.argsort(matched_gn_1))

        n_indicators = len(y0_all)

        for indicator in range(n_indicators):
            color = matplotlib.cm.get_cmap('turbo')(indicator / (n_indicators-1))

            y0 = y0_all[indicator][in0][order0]
            m_0 = log_M200_0[in0][order0]

            y1 = y1_all[indicator][in1][order1]
            m_1 = log_M200_1[in1][order1]

            #what to actually plot
            y = y1 - y0
            x = np.log10((10**m_0 + 10**m_1) / 2)

            # calc medians etc.
            ymedians, xedges, bin_is = binned_statistic(x, y,
                                                        statistic=np.nanmedian, bins=N_run)
            yplus, _, _ = binned_statistic(x, y,
                                           statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)
            yminus, _, _ = binned_statistic(x, y,
                                            statistic=lambda x: np.nanquantile(x, 0.16),  bins=N_run)
            ystds, _, _ = binned_statistic(x, y,
                                           statistic=np.nanstd, bins=N_run)
            xcentres = 0.5 * (xedges[1:] + xedges[:-1])

            counts, _ = np.histogram(x, bins=N_run)
            # mask = counts > 10
            mask = counts > 20 * np.diff(N_run)[0]

            boots = np.zeros((len(xedges) - 1, 3))
            for i in range(len(xedges) - 1):
                boots[i] = my_bootstrap(y[bin_is == i + 1], statistic=np.nanmedian)

            #plot
            axs[0].errorbar(xcentres[mask], ymedians[mask],
                             c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            # axs[0].errorbar(xcentres[mask], y1medians[mask] - y0medians[mask],
            #                  c=color, ls='-.', linewidth=1, zorder=3, alpha=1, path_effects=path_effect_2)

            if shade:
                axs[0].fill_between(xcentres[mask], yminus[mask], yplus[mask],
                                    color=color, alpha=0.5, edgecolor='k', zorder=8)

            #extras
            #scatter
            axs[1].errorbar(xcentres[mask], (yplus[mask] - yminus[mask])/2,
                            c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            # axs[1].errorbar(xcentres[mask], ystds[mask],
            #                 c=color, ls=':', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)

            #correlation
            not_crazy_mask = np.logical_and(np.isfinite(y0), np.isfinite(y1))
            y0nc = y0[not_crazy_mask]
            y1nc = y1[not_crazy_mask]
            bin_isnc = bin_is[not_crazy_mask]

            #assumes central limit theorem
            spearman = np.array([scipy.stats.spearmanr(y0nc[bin_isnc == i + 1], y1nc[bin_isnc == i + 1])[0]
                                for i in np.arange(len(xedges) - 1)[mask]])
            # #assumes gaussian
            # pearson  = np.array([scipy.stats.pearsonr(y0nc[bin_isnc == i + 1], y1nc[bin_isnc == i + 1])[0]
            #                     for i in np.arange(len(xedges) - 1)[mask]])

            axs[2].errorbar(xcentres[mask], spearman,
                            c=color, ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
            # axs[2].errorbar(xcentres[mask], pearson,
            #                 c=color, ls=':', linewidth=1, zorder=10, alpha=1, path_effects=path_effect_2)

        #decorations
        m200_intercept_0 = m200_heating_intercept(z, 1)
        m200_intercept_1 = m200_heating_intercept(z, 0)

        for i in range(3):
            axs[i].axvline(m200_intercept_1, c='C1', ls=(0,(0.8,0.8)), linewidth=1, zorder=-3, alpha=1)
            axs[i].axvline(m200_intercept_0, c='C0', ls='-.', linewidth=1, zorder=-3, alpha=1)

        axs[0].errorbar(xlims, [0]*2,
                     c='k', ls=':', lw=1, zorder=-10, alpha=1)
        axs[2].errorbar(xlims, [0]*2,
                        c='k', ls=':', lw=1, zorder=-10, alpha=1)

        for indicator in range(n_indicators):
            color = matplotlib.cm.get_cmap('turbo')(indicator / (n_indicators-1))

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color=matplotlib.cm.get_cmap('turbo')(indicator / (n_indicators-1)),
                                 ls='-', lw=2, path_effects=path_effect_4))
                    for indicator in range(n_indicators)],
                  [legend_all[indicator] for indicator in range(n_indicators)],
                  # handlelength=2, labelspacing=0.2, handletextpad=0.4,
                  loc='upper right')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def matched_disk_galaxy_sample(name='', save=False, z_index=0, r12=False, shade=True):
    N_res = 10
    z_index_highlight = z_index
    # color = matplotlib.cm.get_cmap('inferno_r')((1.5 + z_index)/5)

    fig = plt.figure(constrained_layout=True)

    fig.set_size_inches(3.3, 3.3 + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [0, 1]

    # N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9.8, 14, 22)
    # N_run = np.linspace(9.5, 14, 10)
    N_run = np.linspace(9.5, 13.5, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'Disk galaxy fraction [$F(\kappa_{\rm co} > 0.4)$]'

    axs = []
    for row in range(1):
        axs.append(fig.add_subplot(spec[row]))
        axs[row].set_xlim(xlims)
        axs[row].set_ylim(ylims)

        axs[row].set_ylabel(ylabel)

        if row != 0: axs[row].set_xticklabels([])
        else: axs[row].set_xlabel(xlabel)

        axs[row].set_aspect(np.diff(xlims)/np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    if r12:
        rad_key = 'eq_r12'
    else:
        rad_key = 'r200'

    z = [0, 0.503, 1.004, 2.012][z_index]

    #z=0 output only
    with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
        # centrals only
        mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
        gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

        #load
        M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
        r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]

        z12R12_0 = raw_data0[keys.key0_dict['z12R12']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        z12R12_0 /= raw_data0[keys.key1_dict['z12R12']['Star']][:]
        z12R12_0 = z12R12_0[mask0]
        ca_0 = raw_data0[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        ca_0 /= raw_data0[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        ca_0 = ca_0[mask0]
        kappa_co_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_co_0 = kappa_co_0[mask0]
        kappa_rot_0 = raw_data0[keys.key0_dict['kappa_rot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_rot_0 = kappa_rot_0[mask0]
        v_sigma_0 = (raw_data0[keys.key3_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] /
            np.sqrt((raw_data0[keys.key0_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                     raw_data0[keys.key1_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                     raw_data0[keys.key2_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2)/3))
        v_sigma_0 = v_sigma_0[mask0]

        #mask and units
        log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
        gn0 = gn0[mask0]


    with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
        # centrals only
        mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
        gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

        # load
        M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
        r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]

        z12R12_1 = raw_data1[keys.key0_dict['z12R12']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        z12R12_1 /= raw_data1[keys.key1_dict['z12R12']['Star']][:]
        z12R12_1 = z12R12_1[mask1]
        ca_1 = raw_data1[keys.key0_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        ca_1 /= raw_data1[keys.key1_dict['ca']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        ca_1 = ca_1[mask1]
        kappa_co_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_co_1 = kappa_co_1[mask1]
        kappa_rot_1 = raw_data1[keys.key0_dict['kappa_rot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_rot_1 = kappa_rot_1[mask1]
        v_sigma_1 = (raw_data1[keys.key3_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] /
            np.sqrt((raw_data1[keys.key0_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                     raw_data1[keys.key1_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2 +
                     raw_data1[keys.key2_dict['vsigma']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]] ** 2)/3))
        v_sigma_1 = v_sigma_1[mask1]

        #mask and units
        log_M200_1 = np.log10(M200_1[mask1]) + 10 # log Mstar
        gn1 = gn1[mask1]

        # #mask
        # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
        # x = x0[not_crazy_mask0]
        # y = y0[not_crazy_mask0]

    with h5.File(matched_groups_files[z_index], 'r') as match_data:
        matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
        matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

    #not all haloes have stars
    ni0 = np.isin(matched_gn_0, gn0)
    ni1 = np.isin(matched_gn_1, gn1)
    ni = np.logical_and(ni0, ni1)
    matched_gn_0 = matched_gn_0[ni]
    matched_gn_1 = matched_gn_1[ni]

    #not all haloes matched with stars
    in0 = np.isin(gn0, matched_gn_0)
    in1 = np.isin(gn1, matched_gn_1)

    order0 = np.argsort(np.argsort(matched_gn_0))
    order1 = np.argsort(np.argsort(matched_gn_1))

    #done sorting.
    kappa_co_cut = 0.4
    m200_intercept = m200_heating_intercept(z, 0)
    m_interp = np.linspace(m200_intercept, np.amax(N_run))

    #LR
    ymeans_m, xedges, bin_is = binned_statistic(log_M200_1, kappa_co_1 > kappa_co_cut,
                                                statistic=np.nanmean, bins=N_run)
    counts, _ = np.histogram(log_M200_1, bins=N_run)

    xcentres = 0.5 * (xedges[1:] + xedges[:-1])

    ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
    counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

    axs[0].errorbar(xcentres, ymeans_m,
                    c='C1', ls='--', linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
    axs[0].errorbar(m_interp, ymeans_interp(m_interp),
                    c='C1', ls='--', linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
    if shade:
        axs[0].fill_between(m_interp,
                            ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                            ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                              color='C1', alpha=0.3, edgecolor='k', zorder=0.3, hatch=hatches['--'])

    #HR
    ymeans_m, xedges, bin_is = binned_statistic(log_M200_0, kappa_co_0 > kappa_co_cut,
                                                statistic=np.nanmean, bins=N_run)
    counts, _ = np.histogram(log_M200_0, bins=N_run)

    ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
    counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

    axs[0].errorbar(xcentres, ymeans_m,
                    c='C0', ls='-', linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
    axs[0].errorbar(m_interp, ymeans_interp(m_interp),
                    c='C0', ls='-', linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
    if shade:
        axs[0].fill_between(m_interp,
                            ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                            ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                              color='C0', alpha=0.3, edgecolor='k', zorder=0.3, hatch=hatches['-'])

    #matches
    k0 = kappa_co_0[in0][order0]
    m_0 = log_M200_0[in0][order0]

    k1 = kappa_co_1[in1][order1]
    m_1 = log_M200_1[in1][order1]

    m_m = np.log10((10 ** m_0 + 10 ** m_1) / 2)

    #LR not HR
    ymeans_m, xedges, bin_is = binned_statistic(m_m, np.logical_and(k0 < kappa_co_cut, k1 > kappa_co_cut),
                                                statistic=np.nanmean, bins=N_run)
    counts, _ = np.histogram(m_1, bins=N_run)

    ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
    counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

    axs[0].errorbar(xcentres, ymeans_m,
                    c='C3', ls=(0,(0.8, 0.8)), linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
    axs[0].errorbar(m_interp, ymeans_interp(m_interp),
                    c='C3', ls=(0,(0.8, 0.8)), linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
    if shade:
        axs[0].fill_between(m_interp,
                            ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                            ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                              color='C3', alpha=0.3, edgecolor='k', zorder=3.3, hatch=hatches[(0,(0.8,0.8))])

    #HR not LR
    ymeans_m, xedges, bin_is = binned_statistic(m_m, np.logical_and(k0 > kappa_co_cut, k1 < kappa_co_cut),
                                                statistic=np.nanmean, bins=N_run)
    counts, _ = np.histogram(m_1, bins=N_run)

    ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
    counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

    axs[0].errorbar(xcentres, ymeans_m,
                    c='C2', ls='-.', linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
    axs[0].errorbar(m_interp, ymeans_interp(m_interp),
                    c='C2', ls='-.', linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
    if shade:
        axs[0].fill_between(m_interp,
                            ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                            ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                              color='C2', alpha=0.3, edgecolor='k', zorder=3.3, hatch=hatches['-.'])

    ymeans_m, xedges, bin_is = binned_statistic(m_m, np.logical_and(k0 > kappa_co_cut, k1 > kappa_co_cut),
                                                statistic=np.nanmean, bins=N_run)
    counts, _ = np.histogram(m_1, bins=N_run)

    ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
    counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

    axs[0].errorbar(xcentres, ymeans_m,
                    c='C4', ls='-', linewidth=1, zorder=8, alpha=1, path_effects=path_effect_2)
    axs[0].errorbar(m_interp, ymeans_interp(m_interp),
                    c='C4', ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
    if shade:
        axs[0].fill_between(m_interp,
                            ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                            ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                              color='C4', alpha=0.4, edgecolor='k', zorder=8.3)

    axs[0].axvline(m200_heating_intercept(z, 1), 0, 1,
                     color='navy', ls='-.', lw=1, zorder=-3)
    axs[0].axvline(m200_heating_intercept(z, 0), 0, 1,
                     color='saddlebrown', ls=(0,(0.8,0.8)), lw=1, zorder=-3)

    axs[0].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C2', ls='-.', lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C3', ls=(0,(0.8,0.8)), lw=2, path_effects=path_effect_4)),
                   (lines.Line2D([0, 1], [0, 1], color='C4', ls='-', lw=2, path_effects=path_effect_4)),
                   ],
                  ['HR disks', 'LR disks',
                   r'HR disk $\Leftrightarrow$ LR bulge',
                   'LR disk $\Leftrightarrow$ HR bulge',
                   'Disks in both sims.'],#at both resolutions'],
                  labelspacing=0.08, borderpad=0.15, borderaxespad=0, #bbox_to_anchor=(0.04, 1.03),
                  loc='upper center')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return

def kappa_fractions(name='', save=False, z_index=0, r12=False, shade=False, disks=False,
                    DT=False, ST=False, matched=False, disjoint=False):
    N_res = 10
    z_index_highlight = z_index
    # color = matplotlib.cm.get_cmap('inferno_r')((1.5 + z_index)/5)

    # kappa_co_cuts = [0.45, 0.50, 0.55, 0.6, 0.65, 0.7]
    # kappa_co_cuts = [0.5, 0.6, 0.7]

    # kappa_co_cuts = np.linspace(0, 1, 21)

    kappa_co_cuts = [0.5, 0.6, 0.7]
    if DT:
        kappa_co_cuts = [0.2, 0.35, 0.5]
        ylimss = [[0,1], [0,0.6], [0,0.2]]
    if ST:
        kappa_co_cuts = [0.5, 0.65, 0.8]
        ylimss = [[0,1], [0,0.6], [0,0.2]]

    if (not disks) and (not DT) and (not ST):
        kappa_co_cuts= [0.35, 0.5, 0.65]
        ylimss = [[0,1], [0,0.6], [0,0.2]]

    # if DT or ST:
    #     r12 = True

    fig = plt.figure(constrained_layout=True)

    fig.set_size_inches(3.0 * len(kappa_co_cuts), 3.0 + 0.1, forward=True)
    # fig.set_size_inches(4, 4.01, forward=True)
    spec = fig.add_gridspec(ncols=len(kappa_co_cuts), nrows=1)

    xlims = [1e-3 + 10, 14 - 1e-3]
    ylims = [0, 1]

    # N_run = np.linspace(9.8, 14, 22)
    N_run = np.linspace(9.75, 14, 18)
    # N_run = np.linspace(9+2/3, 14, 14)
    # N_run = np.linspace(9.5, 14, 10)

    xlabel = r'$\log \, M_{200} / $M$_\odot$'
    ylabel = r'fraction'

    if disks:
        ylabel = r'$F_{\rm disks}(\kappa_{\rm co} > X)$'
        if DT:
            ylabel = r'$F_{\rm disks}({\rm D/T} > X)$'
        if ST:
            ylabel = r'$F_{\rm disks}({\rm S/T} < X)$'
    else:
        label = r'$\kappa_{\rm co} > $'
        if DT:
            label = r'${\rm D/T} > $'
        if ST:
            label = r'${\rm S/T} < $'

    axs = []
    for col in range(len(kappa_co_cuts)):
        axs.append(fig.add_subplot(spec[col]))

        # ax2 = axs[0].twinx()
        # ax2.set_yticklabels('')

        axs[col].set_ylim(ylimss[col])
        # ax2.set_ylim(ylims)

        axs[col].set_xlim(xlims)
        # axs[row].set_ylim(ylims)

        axs[col].set_ylabel(ylabel)

        if ST or 'solo' in name:
            axs[col].set_xlabel(xlabel)
        else:
            axs[col].set_xlabel('')
            axs[col].set_xticklabels([])

        axs[col].set_aspect(np.diff(xlims)/np.diff(ylimss[col]))
        # ax2.set_aspect(np.diff(xlims)/np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.33)

    if r12:
        rad_key = 'eq_r12'
    else:
        rad_key = 'r200' #'lt_30'

    z = [0, 0.503, 1.004, 2.012][z_index]

    #z=0 output only
    with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
        # centrals only
        mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
        gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

        #load
        M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
        r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]

        kappa_co_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_co_0 = kappa_co_0[mask0]

        fjzonc_0 = raw_data0['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict[rad_key, None], :]
        fjzonc_0 = fjzonc_0[mask0]

        # > 0.7
        n = -round((1 - 0.7) / (2/(keys.n_jzjc_bins - 1))+1)
        if DT:
            DT_0 = fjzonc_0[:, n]
        if ST:
            ST_0 = 1 - 2*(1-fjzonc_0[:, (keys.n_jzjc_bins - 1)//2])

        #mask and units
        log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
        gn0 = gn0[mask0]

    with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
        # centrals only
        mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
        gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

        # load
        M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
        r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]

        kappa_co_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_co_1 = kappa_co_1[mask1]

        fjzonc_1 = raw_data1['GalaxyProfiles/Star/fjzjc'][:, keys.rkey_to_rcolum_dict[rad_key, None], :]
        fjzonc_1 = fjzonc_1[mask1]

        # > 0.7
        n = -round((1 - 0.7) / (2/(keys.n_jzjc_bins - 1))+1)
        if DT:
            DT_1 = fjzonc_1[:, n]
        if ST:
            ST_1 = 1 - 2*(1-fjzonc_1[:, (keys.n_jzjc_bins - 1)//2])

        #mask and units
        log_M200_1 = np.log10(M200_1[mask1]) + 10 # log Mstar
        gn1 = gn1[mask1]

        # #mask
        # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
        # x = x0[not_crazy_mask0]
        # y = y0[not_crazy_mask0]

    if matched or disjoint or disks:
        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        #not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        #not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0))
        order1 = np.argsort(np.argsort(matched_gn_1))

    #done sorting.
    m200_intercept = m200_heating_intercept(z, 0)
    m_interp = np.linspace(m200_intercept, np.amax(N_run), 1001)
    rm_interp = np.linspace(np.amin(N_run), m200_intercept, 1001)
    if not (matched or disjoint):
        m200_intercept_low = m200_heating_intercept(z, 1)
        m_interp_low = np.linspace(m200_intercept_low, np.amax(N_run), 1001)
        rm_interp_low = np.linspace(np.amin(N_run), m200_intercept_low, 1001)
    else:
        m_interp_low = m_interp
        rm_interp_low = rm_interp

    cmap = 'cividis'

    if disks:
        kappa_co_cut = 0.4

        #matched sample
        k0 = kappa_co_0[in0][order0]
        k1 = kappa_co_1[in1][order1]

        match_disk_mask = np.logical_and(k0 > kappa_co_cut, k1 > kappa_co_cut)
        # match_disk_mask = np.logical_and(DT_0[in0][order0] > 0.5, DT_1[in1][order1] > 0.5)

        kappa_co_0 = k0[match_disk_mask]
        log_M200_0 = log_M200_0[in0][order0][match_disk_mask]
        kappa_co_1 = k1[match_disk_mask]
        log_M200_1 = log_M200_1[in1][order1][match_disk_mask]

        if DT:
            DT_0 = DT_0[in0][order0][match_disk_mask]
            DT_1 = DT_1[in1][order1][match_disk_mask]
        if ST:
            ST_0 = ST_0[in0][order0][match_disk_mask]
            ST_1 = ST_1[in1][order1][match_disk_mask]

        # #teat each sim separately
        # disk_mask_0 = kappa_co_0 > kappa_co_cut
        # kappa_co_0 = kappa_co_0[disk_mask_0]
        # log_M200_0 = log_M200_0[disk_mask_0]
        #
        # disk_mask_1 = kappa_co_1 > kappa_co_cut
        # kappa_co_1 = kappa_co_1[disk_mask_1]
        # log_M200_1 = log_M200_1[disk_mask_1]

        cmap = 'viridis'

    # if ST:
    #     cmap += '_r'

    if DT:
        kappa_co_0 = DT_0
        kappa_co_1 = DT_1
    if ST:
        kappa_co_0 = ST_0
        kappa_co_1 = ST_1

    # for N_run in [np.linspace(9.8, 14, 22), np.linspace(9.75, 14, 18),
    #               np.linspace(9+2/3, 14, 14), np.linspace(9.5, 14, 10)]:
        # for iii, N_run in enumerate((N_run[np.newaxis, :] + np.linspace(0, np.diff(N_run)[0], 41)[:, np.newaxis])[:-1]):

    #fractions
    for kappa_index, kappa_co_cut in enumerate(kappa_co_cuts):
        color = matplotlib.cm.get_cmap(cmap)(kappa_index / (len(kappa_co_cuts)-1))

        #LR
        ymeans_m, xedges, bin_is = binned_statistic(log_M200_1, kappa_co_1 > kappa_co_cut,
                                                    statistic=np.nanmean, bins=N_run)
        counts, _ = np.histogram(log_M200_1, bins=N_run)

        xcentres = 0.5 * (xedges[1:] + xedges[:-1])

        ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate', kind='nearest')
        # counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

        # axs[kappa_index].errorbar(xcentres, ymeans_m,
        #                 c='C1', ls='', marker='s', zorder=10, alpha=1, ms=2, mew=0)

        # if iii == 0:
        axs[kappa_index].errorbar(rm_interp, ymeans_interp(rm_interp), #xcentres, ymeans_m,
                        c='C1', ls='--', linewidth=1, zorder=9, alpha=1, path_effects=path_effect_2)
                        # c=color,
        axs[kappa_index].errorbar(m_interp, ymeans_interp(m_interp),
                        c='C1', ls='--', linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
                        # c=color,
        if shade:
            boots = np.zeros((len(xedges) - 1, 3))
            for i in range(len(xedges) - 1):
                # boots[i] = my_bootstrap((kappa_co_1 > kappa_co_cut)[bin_is == i + 1], statistic=np.nanmean)
                x = np.sum((kappa_co_1 > kappa_co_cut)[bin_is == i + 1])
                n = np.sum(bin_is == i + 1)
                alpha = 0.16 * 2
                #https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
                boots[i] = scipy.stats.beta.ppf([alpha / 2, 0.5, 1 - alpha / 2], [x, 1, x + 1], [n - x + 1, 1, n - x])
                boots[i][np.isnan(boots[i])] = x/n

            boots_0_interp = interp1d(xcentres, boots[:,0], fill_value='extrapolate')
            boots_2_interp = interp1d(xcentres, boots[:,2], fill_value='extrapolate')

            axs[kappa_index].fill_between(m_interp,
                                boots_0_interp(m_interp), boots_2_interp(m_interp),
                                # color='C0',
                                color=color, alpha=0.3, edgecolor='k', zorder=0.9, hatch=hatches['--'])

            axs[kappa_index].fill_between(xcentres,
                                boots[:,0], boots[:,2],
                                # color='C0',
                                color=color, alpha=0.3, edgecolor='k', zorder=0.3)

            # boots = np.zeros((len(xedges) - 1, 3))
            # for i in range(len(xedges) - 1):
            #     estimate_i = np.mean((kappa_co_1 > kappa_co_cut)[bin_is == i + 1])
            #     for j, q in enumerate([0.16, 0.5, 0.84]):
            #         # boots[i] = scipy.stats.beta.ppf(q, )
            #         const = 0
            #         if np.isclose(q, 0.16): const=-1
            #         if np.isclose(q, 0.84): const=1
            #         boots[i, j] = const * np.sqrt(estimate_i * (1 - estimate_i) / np.sum(bin_is == i + 1))
            #
            # axs[0].fill_between(xcentres,
            #                     boots[:,0], boots[:,2],
            #                     # color='C0',
            #                     color=color, alpha=0.3, edgecolor='k', zorder=0.3, hatch=hatches['--'])

            # axs[0].fill_between(m_interp,
            #                     ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp))/2,
            #                     ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp))/2,
            #                     # color='C1',
            #                     color=color, alpha=0.3, edgecolor='k', zorder=0.3, hatch=hatches['--'])

            # axs[0].errorbar(xcentres, ymeans_m, 1/np.sqrt(counts)/2,
            #                 # color='C1',
            #                 color=color, ls='', linewidth=1, zorder=1, capsize=5, capthick=2)

        #HR
        ymeans_m, xedges, bin_is = binned_statistic(log_M200_0, kappa_co_0 > kappa_co_cut,
                                                    statistic=np.nanmean, bins=N_run)
        counts, _ = np.histogram(log_M200_0, bins=N_run)

        ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate', kind='nearest')
        # counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

        # axs[kappa_index].errorbar(xcentres, ymeans_m,
        #                 c='C0', ls='', marker='o', zorder=10.1, alpha=1, ms=2, mew=0)

        # if iii == 0:
        axs[kappa_index].errorbar(rm_interp_low, ymeans_interp(rm_interp_low), #xcentres, ymeans_m,
                        c='C0', ls='-', linewidth=1, zorder=8, alpha=1, path_effects=path_effect_2)
                        # c=color,
        axs[kappa_index].errorbar(m_interp_low, ymeans_interp(m_interp_low),
                        c='C0', ls='-', linewidth=2, zorder=8, alpha=1, path_effects=path_effect_4)
                        # c=color,

        print(kappa_co_cut)
        print(np.sum(kappa_co_1[log_M200_1 > 12] > kappa_co_cut), len(kappa_co_1[log_M200_1 > 12]),
              np.sum(kappa_co_1[log_M200_1 > 12] > kappa_co_cut)/ len(kappa_co_1[log_M200_1 > 12]))
        print(np.sum(kappa_co_0[log_M200_0 > 12] > kappa_co_cut), len(kappa_co_0[log_M200_0 > 12]),
              np.sum(kappa_co_0[log_M200_0 > 12] > kappa_co_cut)/ len(kappa_co_0[log_M200_0 > 12]))

        if shade:
            boots = np.zeros((len(xedges) - 1, 3))
            for i in range(len(xedges) - 1):
                # boots[i] = my_bootstrap((kappa_co_0 > kappa_co_cut)[bin_is == i + 1], statistic=np.nanmean)
                x = np.sum((kappa_co_0 > kappa_co_cut)[bin_is == i + 1])
                n = np.sum(bin_is == i + 1)
                alpha = 0.16 * 2
                #https://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval
                boots[i] = scipy.stats.beta.ppf([alpha / 2, 0.5, 1 - alpha / 2], [x, 1, x + 1], [n - x + 1, 1, n - x])
                boots[i][np.isnan(boots[i])] = x/n

            boots_0_interp = interp1d(xcentres, boots[:,0], fill_value='extrapolate')
            boots_2_interp = interp1d(xcentres, boots[:,2], fill_value='extrapolate')

            axs[kappa_index].fill_between(m_interp_low,
                                boots_0_interp(m_interp_low), boots_2_interp(m_interp_low),
                                # color='C0',
                                color=color, alpha=0.3, edgecolor='k', zorder=0.9, hatch=hatches['-'])

            axs[kappa_index].fill_between(xcentres,
                                boots[:,0], boots[:,2],
                                # color='C0',
                                color=color, alpha=0.3, edgecolor='k', zorder=0.3)

            # axs[0].fill_between(xcentres,
            #                     boots[:,0], boots[:,2],
            #                     # color='C0',
            #                     color=color, alpha=0.3, edgecolor='k', zorder=0.3, hatch=hatches['-'])

            # axs[0].fill_between(m_interp_low,
            #                     ymeans_interp(m_interp_low) - 1/np.sqrt(counts_interp(m_interp_low))/2,
            #                     ymeans_interp(m_interp_low) + 1/np.sqrt(counts_interp(m_interp_low))/2,
            #                     # color='C0',
            #                     color=color, alpha=0.3, edgecolor='k', zorder=0.3, hatch=hatches['-'])

            # axs[0].errorbar(xcentres, ymeans_m, 1/np.sqrt(counts)/2,
            #                 # color='C0',
            #                 color=color, ls='', linewidth=1, zorder=1, capsize=5, capthick=2)

        #matches
        if matched or disjoint:
            if disks:
                k0 = kappa_co_0
                m_0 = log_M200_0

                k1 = kappa_co_1
                m_1 = log_M200_1

                m_m = np.log10((10 ** m_0 + 10 ** m_1) / 2)

            else:
                k0 = kappa_co_0[in0][order0]
                m_0 = log_M200_0[in0][order0]

                k1 = kappa_co_1[in1][order1]
                m_1 = log_M200_1[in1][order1]

                m_m = np.log10((10 ** m_0 + 10 ** m_1) / 2)

            if disjoint:
                #LR not HR
                ymeans_m, xedges, bin_is = binned_statistic(m_m, np.logical_and(k0 < kappa_co_cut, k1 > kappa_co_cut),
                                                            statistic=np.nanmean, bins=N_run)
                counts, _ = np.histogram(m_1, bins=N_run)

                ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
                counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

                axs[kappa_index].errorbar(xcentres, ymeans_m,
                                c='C3', ls=(0,(0.8, 0.8)), linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
                axs[kappa_index].errorbar(m_interp, ymeans_interp(m_interp),
                                c='C3', ls=(0,(0.8, 0.8)), linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
                if shade:
                    axs[kappa_index].fill_between(m_interp,
                                        ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                                        ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                                          color='C3', alpha=0.3, edgecolor='k', zorder=3.3, hatch=hatches[(0,(0.8,0.8))])

                #HR not LR
                ymeans_m, xedges, bin_is = binned_statistic(m_m, np.logical_and(k0 > kappa_co_cut, k1 < kappa_co_cut),
                                                            statistic=np.nanmean, bins=N_run)
                counts, _ = np.histogram(m_1, bins=N_run)

                ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
                counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

                axs[kappa_index].errorbar(xcentres, ymeans_m,
                                c='C2', ls='-.', linewidth=1, zorder=7, alpha=1, path_effects=path_effect_2)
                axs[kappa_index].errorbar(m_interp, ymeans_interp(m_interp),
                                c='C2', ls='-.', linewidth=2, zorder=9, alpha=1, path_effects=path_effect_4)
                if shade:
                    axs[kappa_index].fill_between(m_interp,
                                        ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                                        ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                                          color='C2', alpha=0.3, edgecolor='k', zorder=3.3, hatch=hatches['-.'])

            if matched:
                ymeans_m, xedges, bin_is = binned_statistic(m_m, np.logical_and(k0 > kappa_co_cut, k1 > kappa_co_cut),
                                                            statistic=np.nanmean, bins=N_run)
                counts, _ = np.histogram(m_1, bins=N_run)

                ymeans_interp = interp1d(xcentres, ymeans_m, fill_value='extrapolate')
                counts_interp = interp1d(xcentres, counts, fill_value='extrapolate')

                axs[kappa_index].errorbar(xcentres, ymeans_m,
                                c='C4', ls='-', linewidth=1, zorder=8, alpha=1, path_effects=path_effect_2)
                axs[kappa_index].errorbar(m_interp, ymeans_interp(m_interp),
                                c='C4', ls='-', linewidth=2, zorder=10, alpha=1, path_effects=path_effect_4)
                if shade:
                    axs[kappa_index].fill_between(m_interp,
                                        ymeans_interp(m_interp) - 1/np.sqrt(counts_interp(m_interp)),
                                        ymeans_interp(m_interp) + 1/np.sqrt(counts_interp(m_interp)),
                                          color='C4', alpha=0.4, edgecolor='k', zorder=8.3)

        axs[kappa_index].axvline(m200_heating_intercept(z, 1), 0, 1,
                         color='navy', ls='-.', lw=1, zorder=-3)
        axs[kappa_index].axvline(m200_heating_intercept(z, 0), 0, 1,#*star_lims,
                         color='saddlebrown', ls=(0,(0.8,0.8)), lw=1, zorder=-3)

        dx = 0.93
        ha = 'right'
        dy0 = 0.93
        dy1 = 0.77
        if DT and kappa_index == 2:
            dy0 = 0.76
            dy1 = 0.60
        elif ST and kappa_index == 2:
            dx = 0.07
            ha = 'left'
            dy0 = 0.64
            dy1 = 0.48

        axs[kappa_index].text(xlims[0] + dx * (xlims[1] - xlims[0]),
                              ylimss[kappa_index][0] + dy0 * (ylimss[kappa_index][1] - ylimss[kappa_index][0]),
                              label + str(kappa_co_cut),
                              ha=ha, va='top',
                              bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

        axs[kappa_index].text(xlims[0] + dx * (xlims[1] - xlims[0]),
                              ylimss[kappa_index][0] + dy1 * (ylimss[kappa_index][1] - ylimss[kappa_index][0]),
                              fr"$z={['0', '0.5', '1', '2'][z_index]}$",
                              ha=ha, va='top',
                              bbox=dict(boxstyle='round', fc='w', ec='0.8', alpha=0.8))

        if z == 0:
            #Correa & Schaye 2020
            data = [11.623, 0.98121,
                    11.713, 0.93111,
                    11.928, 0.89562,
                    12.055, 0.85177,
                    12.149, 0.83090,
                    12.257, 0.78079,
                    12.360, 0.70564,
                    12.460, 0.61587,
                    12.591, 0.50731,
                    12.695, 0.39875,
                    12.827, 0.29854,
                    12.917, 0.22547,
                    13.021, 0.17537,
                    13.144, 0.18372,
                    13.240, 0.10856,
                    13.334, 0.089770]
            cs_x = data[::2]
            cs_y = data[1::2]

            # cs_interp = interp1d(cs_x, cs_y, fill_value='extrapolate')
            # cs_x_erp = np.linspace(11.634, 13.334, len(cs_x) + 2)

            axs[kappa_index].errorbar(cs_x, cs_y,
                                      c='grey', ls='-', linewidth=2, zorder=0, alpha=0.5)
            # axs[kappa_index].errorbar(cs_x_erp, cs_interp(cs_x_erp),
            #                           c='grey', ls='', marker='o', mfc='white', mew=2,
            #                           linewidth=0, zorder=0, alpha=0.5)#, path_effects=path_effect_2)

    line_objects = []
    legend_labels = []

    round_n = 1
    loc0 = 'upper left'
    loc1 = 'upper right'
    if ST:
        round_n = 2
        if disks:
            loc0 = 'lower right'
        else:
            loc0 = 'upper left'
        loc1 = 'upper right'

    if DT:
        round_n = 2

    # for kappa_index, kappa_co_cut in enumerate(kappa_co_cuts):
    #     color = matplotlib.cm.get_cmap(cmap)(kappa_index / (len(kappa_co_cuts)-1))
    #
    #     line_objects.append((lines.Line2D([0, 1], [0, 1], color=color, ls='-', lw=6)))
    #
    #     if disks:
    #         # legend_labels.append(r'$F_{\rm disks} (\kappa_{\rm co} >$' + f'{kappa_co_cut})')
    #         legend_labels.append(f'{round(kappa_co_cut, round_n)}')
    #     else:
    #         if DT:
    #             legend_labels.append(r'$F ({\rm D/T} >$' + f'{round(kappa_co_cut, round_n)})')
    #         elif ST:
    #             legend_labels.append(r'$F ({\rm S/T} <$' + f'{round(kappa_co_cut, round_n)})')
    #         else:
    #             legend_labels.append(r'$F (\kappa_{\rm co} >$' + f'{round(kappa_co_cut, round_n)})')

    # ax2.legend([(patches.Patch(facecolor='grey', edgecolor='k', alpha=0.3, hatch=hatches['-']),
    #              lines.Line2D([0, 1], [0, 1], color='grey', ls='-', lw=2, path_effects=path_effect_4)),
    #             (patches.Patch(facecolor='grey', edgecolor='k', alpha=0.3, hatch=hatches['--']),
    #              lines.Line2D([0, 1], [0, 1], color='grey', ls='--', lw=2, path_effects=path_effect_4))],
    #            ['HR', 'LR'],
    #            loc=loc0,)

    # if disks:
    #     title = r'$\kappa_{\rm co}$ limit'
    #     if DT:
    #         title = r'D/T limit'
    #     if ST:
    #         title = r'S/T limit'
    #
    #     axs[0].legend(line_objects, legend_labels,
    #                   title=title, loc=loc1)
    #
    # else:
    #     axs[0].legend(line_objects, legend_labels, loc=loc1)

    if DT:
        axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='grey', ls='-', alpha=0.5, lw=2)),
                       ],
                      ['SDSS discs'],
                      loc='upper right')
    if ST:
        axs[2].legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', lw=2, path_effects=path_effect_4)),
                       (lines.Line2D([0, 1], [0, 1], color='C1', ls='--', lw=2, path_effects=path_effect_4)),
                       ],
                      ['HR galaxies', 'LR galaxies'],
                      loc='upper left')

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200, pad_inches=0)
        plt.close()

    return


def stacked_disk_jzoncs(name='', save=False, z_index=0,
                        log_mass=True, resolved_r=True, mstar_weight=True, diff_individual=True):

    N_res = 1
    half_j_res = False
    norm_r = False

    fig = plt.figure(constrained_layout=True)

    # mass_bin_edges = [[10.4,11.0],[11.0,11.2],[11.5,11.7],[12.0,12.2],[12.2,13]]
    mass_bin_edges = [[12.0,12.2]]
    # if len(mass_bin_edges) > 1:
    #     print('Have not set up colour bar to work for different mass bins')
    #     raise NotImplementedError

    cmap_axis_ratio = 0.1
    fig.set_size_inches(3.3 * 3, 3.3 * (cmap_axis_ratio + len(mass_bin_edges)) + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=3, nrows=len(mass_bin_edges)+1,
                            height_ratios=[cmap_axis_ratio, *([1]*len(mass_bin_edges))])

    if resolved_r:
        xlims = [np.log10(EAGLE_EPS), np.log10(keys.STAR_FRAME_APERTURE_PHYSICAL)]
    else:
        xlims = [1e-3 - 0.7, 3.3 - 1e-3]
    ylims = [-1, 1]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))
    xextent = [np.log10(keys.log_bin_edges[0]), np.log10(keys.log_bin_edges[-1])]
    n_jzjc_bins = keys.n_jzjc_bins

    xlabel = r'$\log \, r /$ kpc'
    ylabel = r'$j_z / j_c(E)$'

    mass_cmap_axis = fig.add_subplot(spec[0, 0:2])
    diff_cmap_axis = fig.add_subplot(spec[0, 2])

    mass_cmap_axis.tick_params(top=True, labeltop=True, labelbottom=False, labelleft=False)
    diff_cmap_axis.tick_params(top=True, labeltop=True, labelbottom=False, labelleft=False)
    mass_cmap_axis.xaxis.set_label_position('top')
    diff_cmap_axis.xaxis.set_label_position('top')

    mass_cmap_axis.set_xlabel(r'$\log \, {\rm d}^2 \langle M_{\star} / M_{\star, \, \rm total} \rangle / {\rm d} (\log \, r / {\rm kpc}) \, {\rm d} (j_z / j_c)$')
    # mass_cmap_axis.set_xlabel(r'$\log \, \overline{\rm M} / {\rm M}_\odot$')
    diff_cmap_axis.set_xlabel(r'$100 \times \langle (M^{\rm LR}_\star - M^{\rm HR}_\star) / [(M^{\rm LR}_\star + M^{\rm HR}_\star) /2] \rangle$')

    axs = []
    for row in range(len(mass_bin_edges)):
        axs.append([])
        for col in range(3):
            axs[row].append(fig.add_subplot(spec[row+1, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if col == 0:
                axs[row][col].set_ylabel(ylabel)
            else:
                axs[row][col].set_yticklabels([])

            if row == len(mass_bin_edges) - 1:
                axs[row][col].set_xlabel(xlabel)
            else:
                axs[row][col].set_xticklabels([])

            # axs[col].set_aspect('equal')

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    # rad_key = 'eq_r12'
    rad_key = 'r200'

    z = [0, 0.503, 1.004, 2.012][z_index]

    #z=0 output only
    with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
        # centrals only
        mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
        gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

        #load
        M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]
        r200_0 = raw_data0[keys.key0_dict['r200'][None]][:]

        kappa_co_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_co_0 = kappa_co_0[mask0]
        # kappa_rot_0 = raw_data0[keys.key0_dict['kappa_rot']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        # kappa_rot_0 = kappa_rot_0[mask0]

        # load
        fjzonc_0 = raw_data0['GalaxyProfiles/Star/fjzjc'][:, 0:0 + len(bin_centres), :]
        fjzonc_0 = fjzonc_0[mask0]

        mstarr_0 = raw_data0[keys.key0_dict['mstarr']['Star']][:, 0:0 + len(bin_centres)]
        mstarr_0 = mstarr_0[mask0] * 1e10
        # mstarr_0 = np.log10(mstarr_0)

        #mask and units
        log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
        gn0 = gn0[mask0]

    with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
        # centrals only
        mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
        gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

        # load
        M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]
        r200_1 = raw_data1[keys.key0_dict['r200'][None]][:]

        kappa_co_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]
        kappa_co_1 = kappa_co_1[mask1]

        fjzonc_1 = raw_data1['GalaxyProfiles/Star/fjzjc'][:, 0:0 + len(bin_centres), :]
        fjzonc_1 = fjzonc_1[mask1]

        mstarr_1 = raw_data1[keys.key0_dict['mstarr']['Star']][:, 0:0 + len(bin_centres)]
        mstarr_1 = mstarr_1[mask1] * 1e10
        # mstarr_1 = np.log10(mstarr_1)

        #mask and units
        log_M200_1 = np.log10(M200_1[mask1]) + 10 # log Mstar
        gn1 = gn1[mask1]

        # #mask
        # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
        # x = x0[not_crazy_mask0]
        # y = y0[not_crazy_mask0]

    with h5.File(matched_groups_files[z_index], 'r') as match_data:
        matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
        matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

    #not all haloes have stars
    ni0 = np.isin(matched_gn_0, gn0)
    ni1 = np.isin(matched_gn_1, gn1)
    ni = np.logical_and(ni0, ni1)
    matched_gn_0 = matched_gn_0[ni]
    matched_gn_1 = matched_gn_1[ni]

    #not all haloes matched with stars
    in0 = np.isin(gn0, matched_gn_0)
    in1 = np.isin(gn1, matched_gn_1)

    order0 = np.argsort(np.argsort(matched_gn_0))
    order1 = np.argsort(np.argsort(matched_gn_1))

    #done sorting.
    kappa_co_cut = 0.4
    m200_intercept = m200_heating_intercept(z, 0)

    #matches
    kappa_0 = kappa_co_0[in0][order0]
    m200_0 = log_M200_0[in0][order0]

    fjzonc_0 = fjzonc_0[in0][order0]
    mstarr_0 = mstarr_0[in0][order0]

    kappa_1 = kappa_co_1[in1][order1]
    m200_1 = log_M200_1[in1][order1]

    fjzonc_1 = fjzonc_1[in1][order1]
    mstarr_1 = mstarr_1[in1][order1]

    m_m = np.log10((10 ** m200_0 + 10 ** m200_1) / 2)

    # if disks:
    is_matched_disk = np.logical_and(kappa_0 > kappa_co_cut, kappa_1 > kappa_co_cut)

    # kappa_0 = kappa_0[is_matched_disk]
    m200_0 = m200_0[is_matched_disk]
    # kappa_1 = kappa_1[is_matched_disk]
    m200_1 = m200_1[is_matched_disk]

    fjzonc_0 = fjzonc_0[is_matched_disk]
    mstarr_0 = mstarr_0[is_matched_disk]
    fjzonc_1 = fjzonc_1[is_matched_disk]
    mstarr_1 = mstarr_1[is_matched_disk]

    m_m = m_m[is_matched_disk]

    for row, mass_range in enumerate(mass_bin_edges):

        # mask
        mask = np.logical_and(mass_range[0] < m_m, m_m < mass_range[1])

        # k0 = kappa_0[mask]
        # m0 = m200_0[mask]
        # k1 = kappa_0[mask]
        # m1 = m200_0[mask]

        j0 = fjzonc_0[mask]
        m0 = mstarr_0[mask]

        j1 = fjzonc_1[mask]
        m1 = mstarr_1[mask]

        #from cdf to pdf
        if mstar_weight:
            j0 = np.flip(-np.diff(j0 * m0[:, :, np.newaxis], axis=2), axis=2)
            j1 = np.flip(-np.diff(j1 * m1[:, :, np.newaxis], axis=2), axis=2)

            j0[np.isnan(j0)] = 0
            j1[np.isnan(j1)] = 0

        else:
            j0 = np.flip(-np.diff(j0, axis=2), axis=2)
            j1 = np.flip(-np.diff(j1, axis=2), axis=2)

        #norm then average. pretty sure average first is correct
        # j0 = j0 / np.amax(j0, axis=2)[:, :, np.newaxis]
        # j1 = j1 / np.amax(j1, axis=2)[:, :, np.newaxis]

        if diff_individual:
            diff_i = j1 - j0
            diff_i = diff_i / (j1 + j0) * 100

            if norm_r:
                diff_i = (j1 / np.amax(j1, axis=2)[:, :, np.newaxis]) - (j0 / np.amax(j0, axis=2)[:, :, np.newaxis])
                diff_i = diff_i / (((j1 / np.amax(j1, axis=2)[:, :, np.newaxis]) +
                                    (j0 / np.amax(j0, axis=2)[:, :, np.newaxis])) /2) * 100

            # diff_i = (j1 / np.sum(m1, axis=1)[:, np.newaxis, np.newaxis] -
            #           j0 / np.sum(m0, axis=1)[:, np.newaxis, np.newaxis])
            # diff_i = diff_i / (j1 / np.sum(m1, axis=1)[:, np.newaxis, np.newaxis] +
            #                    j0 / np.sum(m0, axis=1)[:, np.newaxis, np.newaxis]) * 100

            diff_i = np.nanmean(diff_i, axis=0)
            # diff_i = np.std(diff_i, axis=0)
            diff_i = diff_i.T

            # print(np.shape(m0)[0])
            mstar_frac_diff = (np.sum(m0, axis=1) - np.sum(m1, axis=1)) / (np.sum(m0, axis=1) + np.sum(m1, axis=1))
            # print(mstar_frac_diff)
            print(np.mean(mstar_frac_diff))
            # print(np.median(mstar_frac_diff))
            # print(np.std(mstar_frac_diff))

            #norm mass in radius

        #stack pdfs
        j0 = np.mean(j0 / np.sum(np.sum(j0, axis=2), axis=1)[:, np.newaxis, np.newaxis], axis=0)
        j1 = np.mean(j1 / np.sum(np.sum(j1, axis=2), axis=1)[:, np.newaxis, np.newaxis], axis=0)
        # j0 = np.mean(j0, axis=0)
        # j1 = np.mean(j1, axis=0)
        # j0 = np.nanmean(j0, axis=0)
        # j1 = np.nanmean(j1, axis=0)
        # j0 = j0[0]
        # j1 = j1[0]

        diff = (j1 - j0).T
        #percent
        diff = diff / ((j1 + j0).T / 2) * 100

        if diff_individual:
            diff = diff_i

        if half_j_res:
            j0_hjr = np.array([j0[:, 0+i*2] + j0[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
            j1_hjr = np.array([j1[:, 0+i*2] + j1[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
            diff = (j1_hjr - j0_hjr).T
            diff = diff / ((j1_hjr + j0_hjr).T / 2) * 100

        # j0_hjr = np.array([j0[:, 0+i*16] + j0[:, 1+i*16] for i in range(n_jzjc_bins//16)]).T
        # j1_hjr = np.array([j1[:, 0+i*16] + j1[:, 1+i*16] for i in range(n_jzjc_bins//16)]).T
        # diff = (j1_hjr - j0_hjr).T
        # diff = diff / ((j1_hjr + j0_hjr).T / 2) * 100

        # j0 = np.array([j0[:, 0+i*2] + j0[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
        # j0 = np.array([j0[0+i*2, :] + j0[1+i*2, :] for i in range(np.shape(j1)[0]//2)])
        # j1 = np.array([j1[:, 0+i*2] + j1[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
        # j1 = np.array([j1[0+i*2, :] + j1[1+i*2, :] for i in range(np.shape(j1)[0]//2)])

        # j0 = np.array([j0[:, 0+i] + j0[:, 1+i] for i in range(n_jzjc_bins-2)]).T
        # j0 = np.array([j0[0+i*16, :] + j0[1+i*16, :] for i in range(np.shape(j1)[0]//16)])
        # j1 = np.array([j1[:, 0+i] + j1[:, 1+i] for i in range(n_jzjc_bins-2)]).T
        # j1 = np.array([j1[0+i*16, :] + j1[1+i*16, :] for i in range(np.shape(j1)[0]//16)])

        j0 = j0 * np.shape(j0)[0] * np.shape(j0)[1]
        j1 = j1 * np.shape(j1)[0] * np.shape(j1)[1]

        #norm each r
        if norm_r:
            j0 = j0 / np.amax(j0, axis=1)[:, np.newaxis]
            j1 = j1 / np.amax(j1, axis=1)[:, np.newaxis]

        if log_mass:
            if norm_r:
                j0 = np.log10(j0 + 1e-1)
                j1 = np.log10(j1 + 1e-1)
            else:
                j0 = np.log10(j0 + 3e-1)
                j1 = np.log10(j1 + 3e-1)
                # j0 = np.log10(j0 + 3e9)
                # j1 = np.log10(j1 + 3e9)

        if len(mass_bin_edges) > 1:
            #hard code
            j_edges = [-0.5, 1.2]
        else:
            j_edges = [np.amin((np.nanmin(j0), np.nanmin(j1))), np.amax((np.nanmax(j0), np.nanmax(j1)))]

        diff_edges = [np.amin((np.nanmin(diff), -np.nanmax(diff)))] * 2
        diff_edges[1] = -diff_edges[1]
        diff_edges = [-30,30]
        # diff_edges = [-1,1]

        j_cmap = 'viridis' #'cubehelix' #'magma' #'cubehelix'
        diff_cmap = 'twilight_shifted'

        diff_cmap = matplotlib.colors.LinearSegmentedColormap.from_list(
            'trunc', plt.get_cmap(diff_cmap)(np.linspace(0.08, 0.92, 265)))

        # diff_cmap = 'Spectral'
        # diff_cmap = lambda x: matplotlib.cm.get_cmap(diff_cmap)( 0.1 + 0.8 * x )
        axs[row][0].imshow(j0.T, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
                           cmap=j_cmap, vmin=j_edges[0], vmax=j_edges[1])
        axs[row][1].imshow(j1.T, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
                           cmap=j_cmap, vmin=j_edges[0], vmax=j_edges[1])
        axs[row][2].imshow(diff, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
                           cmap=diff_cmap, vmin=diff_edges[0], vmax=diff_edges[1])
        # axs[row][3].imshow(diff_i, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
        #                    cmap=diff_cmap, vmin=diff_i_edges[0], vmax=diff_i_edges[1])

        axs[row][0].text(xlims[0] + 0.035 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[row]),
                         color='white', zorder=12, #)
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))
        axs[row][0].text(xlims[0] + 0.965 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'HR',
                         color='white', zorder=12, ha='right',
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))
        axs[row][1].text(xlims[0] + 0.965 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'LR',
                         color='white', zorder=12, ha='right',
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))
        axs[row][1].text(xlims[0] + 0.035 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'N = ' + f'{np.shape(m0)[0]}' + r' matched disks',
                         color='white', zorder=12,
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))

        gradient = np.array([np.linspace(0, 1, 101)] * 2)
        if not norm_r:
            mass_cmap_axis.imshow(gradient, cmap=j_cmap,
                                  extent=[*j_edges, 0, 1], aspect=cmap_axis_ratio * np.diff(j_edges))
        diff_cmap_axis.imshow(gradient, cmap=diff_cmap,
                              extent=[*diff_edges, 0, 1], aspect=cmap_axis_ratio * np.diff(diff_edges))

        for col in range(3):
            if col < 2:
                c='white'
            else:
                c='k'
            axs[row][col].axvline(np.log10(EAGLE_EPS), 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axvline(np.log10(2.8 * EAGLE_EPS), 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axvline(np.log10(keys.STAR_FRAME_APERTURE_PHYSICAL), 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axhline(0, 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axhline(0.7, 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def stacked_gas_jzoncs(name='', save=False, z_index=0,
                       log_mass=True, resolved_r=False, mass_weight=True, diff_individual=False,
                       do_match=False, disks_only=False):
    N_gas_min = 1000
    half_j_res = False
    norm_r = False

    fig = plt.figure(constrained_layout=True)

    mass_bin_edges = [[10.4,11.0],[11.0,11.2],[11.5,11.7],[12.0,12.2],[12.2,13]]
    # mass_bin_edges = [[12.0,12.2]]
    # if len(mass_bin_edges) > 1:
    #     print('Have not set up colour bar to work for different mass bins')
    #     raise NotImplementedError

    cmap_axis_ratio = 0.1
    fig.set_size_inches(3.3 * 3, 3.3 * (cmap_axis_ratio + len(mass_bin_edges)) + 0.05, forward=True)
    spec = fig.add_gridspec(ncols=3, nrows=len(mass_bin_edges)+1,
                            height_ratios=[cmap_axis_ratio, *([1]*len(mass_bin_edges))])

    if resolved_r:
        xlims = [np.log10(EAGLE_EPS), np.log10(keys.STAR_FRAME_APERTURE_PHYSICAL)]
    else:
        xlims = [1e-3 - 0.7, 3.3 - 1e-3]
    ylims = [-1, 1]

    bin_centres = np.log10(0.5 * (keys.log_bin_edges[1:] + keys.log_bin_edges[:-1]))
    xextent = [np.log10(keys.log_bin_edges[0]), np.log10(keys.log_bin_edges[-1])]
    n_jzjc_bins = keys.n_jzjc_bins

    xlabel = r'$\log \, r /$ kpc'
    ylabel = r'$j_z / j_c(E)$'

    mass_cmap_axis = fig.add_subplot(spec[0, 0:2])
    diff_cmap_axis = fig.add_subplot(spec[0, 2])

    mass_cmap_axis.tick_params(top=True, labeltop=True, labelbottom=False, labelleft=False)
    diff_cmap_axis.tick_params(top=True, labeltop=True, labelbottom=False, labelleft=False)
    mass_cmap_axis.xaxis.set_label_position('top')
    diff_cmap_axis.xaxis.set_label_position('top')

    mass_cmap_axis.set_xlabel(r'$\log \, {\rm d}^2 \langle M_{\rm gas} / M_{\rm gas, \, \rm total} \rangle / {\rm d} (\log \, r / {\rm kpc}) \, {\rm d} (j_z / j_c)$')
    if diff_individual:
        diff_cmap_axis.set_xlabel(r'$100 \times \langle (M^{\rm LR}_{\rm gas} - M^{\rm HR}_{\rm gas}) / [(M^{\rm LR}_{\rm gas} + M^{\rm HR}_{\rm gas}) /2] \rangle$')
    else:
        diff_cmap_axis.set_xlabel(r'$100 \times (\langle M^{\rm LR}_{\rm gas} \rangle - \langle M^{\rm HR}_{\rm gas} \rangle) / [(\langle M^{\rm LR}_{\rm gas} \rangle + \langle M^{\rm HR}_{\rm gas} \rangle )/2]$')

    axs = []
    for row in range(len(mass_bin_edges)):
        axs.append([])
        for col in range(3):
            axs[row].append(fig.add_subplot(spec[row+1, col]))
            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            if col == 0:
                axs[row][col].set_ylabel(ylabel)
            else:
                axs[row][col].set_yticklabels([])

            if row == len(mass_bin_edges) - 1:
                axs[row][col].set_xlabel(xlabel)
            else:
                axs[row][col].set_xticklabels([])

            # axs[col].set_aspect('equal')

    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    # rad_key = 'eq_r12'
    rad_key = 'r200'

    z = [0, 0.503, 1.004, 2.012][z_index]

    #z=0 output only
    with h5.File(combined_name_pairs[z_index][0], 'r') as raw_data0:
        # centrals only
        mask0 = raw_data0[keys.key0_dict['sn'][None]][:] == 0
        gn0 = raw_data0[keys.key0_dict['gn'][None]][:]

        #load
        M200_0 = raw_data0[keys.key0_dict['m200'][None]][:]

        kappa_co_0 = raw_data0[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

        fjzonc_0 = raw_data0['GalaxyProfiles/Gas/fjzjc'][:, 0:0 + len(bin_centres), :]

        N0 = raw_data0[keys.key0_dict['Npart']['Gas']][:, keys.rkey_to_rcolum_dict['r200', None]]

        mstarr_0 = raw_data0[keys.key0_dict['mgasr']['Gas']][:, 0:0 + len(bin_centres)]
        # mstarr_0 = np.log10(mstarr_0)

        #mask and units
        gn0 = gn0[mask0]
        log_M200_0 = np.log10(M200_0[mask0]) + 10 # log Mstar
        mstarr_0 = mstarr_0[mask0] * 1e10
        kappa_co_0 = kappa_co_0[mask0]
        fjzonc_0 = fjzonc_0[mask0]
        N0 = N0[mask0]

        Nmask0 = N0 > N_gas_min

        log_M200_0 = log_M200_0[Nmask0]
        mstarr_0 = mstarr_0[Nmask0]
        kappa_co_0 = kappa_co_0[Nmask0]
        fjzonc_0 = fjzonc_0[Nmask0]
        N0 = N0[Nmask0]

    with h5.File(combined_name_pairs[z_index][1], 'r') as raw_data1:
        # centrals only
        mask1 = raw_data1[keys.key0_dict['sn'][None]][:] == 0
        gn1 = raw_data1[keys.key0_dict['gn'][None]][:]

        # load
        M200_1 = raw_data1[keys.key0_dict['m200'][None]][:]

        kappa_co_1 = raw_data1[keys.key0_dict['kappa_co']['Star']][:, keys.rkey_to_rcolum_dict[rad_key, None]]

        fjzonc_1 = raw_data1['GalaxyProfiles/Gas/fjzjc'][:, 0:0 + len(bin_centres), :]

        N1 = raw_data1[keys.key0_dict['Npart']['Gas']][:, keys.rkey_to_rcolum_dict['r200', None]]

        mstarr_1 = raw_data1[keys.key0_dict['mgasr']['Gas']][:, 0:0 + len(bin_centres)]
        # mstarr_1 = np.log10(mstarr_1)

        #mask and units
        gn1 = gn1[mask1]
        log_M200_1 = np.log10(M200_1[mask1]) + 10 # log Mstar
        mstarr_1 = mstarr_1[mask1] * 1e10
        kappa_co_1 = kappa_co_1[mask1]
        fjzonc_1 = fjzonc_1[mask1]
        N1 = N1[mask1]

        Nmask1 = N1 > N_gas_min

        log_M200_1 = log_M200_1[Nmask1]
        mstarr_1 = mstarr_1[Nmask1]
        kappa_co_1 = kappa_co_1[Nmask1]
        fjzonc_1 = fjzonc_1[Nmask1]
        N1 = N1[Nmask1]

        # #mask
        # not_crazy_mask0 = np.logical_and(np.isfinite(x0), np.isfinite(y0))
        # x = x0[not_crazy_mask0]
        # y = y0[not_crazy_mask0]

    if do_match:
        with h5.File(matched_groups_files[z_index], 'r') as match_data:
            matched_gn_0 = match_data['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
            matched_gn_1 = match_data['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

        #not all haloes have stars
        ni0 = np.isin(matched_gn_0, gn0)
        ni1 = np.isin(matched_gn_1, gn1)
        ni = np.logical_and(ni0, ni1)
        matched_gn_0 = matched_gn_0[ni]
        matched_gn_1 = matched_gn_1[ni]

        #not all haloes matched with stars
        in0 = np.isin(gn0, matched_gn_0)
        in1 = np.isin(gn1, matched_gn_1)

        order0 = np.argsort(np.argsort(matched_gn_0))
        order1 = np.argsort(np.argsort(matched_gn_1))

        #done sorting.
        kappa_co_cut = 0.4
        m200_intercept = m200_heating_intercept(z, 0)

        #matches
        kappa_0 = kappa_co_0[in0][order0]
        log_M200_0 = log_M200_0[in0][order0]

        fjzonc_0 = fjzonc_0[in0][order0]
        mstarr_0 = mstarr_0[in0][order0]

        kappa_1 = kappa_co_1[in1][order1]
        log_M200_1 = log_M200_1[in1][order1]

        fjzonc_1 = fjzonc_1[in1][order1]
        mstarr_1 = mstarr_1[in1][order1]

        m_m = np.log10((10 ** log_M200_0 + 10 ** log_M200_1) / 2)

    if disks_only:
        is_matched_disk = np.logical_and(kappa_0 > kappa_co_cut, kappa_1 > kappa_co_cut)

        # kappa_0 = kappa_0[is_matched_disk]
        log_M200_0 = log_M200_0[is_matched_disk]
        # kappa_1 = kappa_1[is_matched_disk]
        log_M200_1 = log_M200_1[is_matched_disk]

        fjzonc_0 = fjzonc_0[is_matched_disk]
        mstarr_0 = mstarr_0[is_matched_disk]
        fjzonc_1 = fjzonc_1[is_matched_disk]
        mstarr_1 = mstarr_1[is_matched_disk]

        m_m = m_m[is_matched_disk]

    for row, mass_range in enumerate(mass_bin_edges):

        # mask
        if do_match:
            mask = np.logical_and(mass_range[0] < m_m, m_m < mass_range[1])

            j0 = fjzonc_0[mask]
            m0 = mstarr_0[mask]

            j1 = fjzonc_1[mask]
            m1 = mstarr_1[mask]

        else:
            mask0 = np.logical_and(mass_range[0] < log_M200_0, log_M200_0 < mass_range[1])
            j0 = fjzonc_0[mask0]
            m0 = mstarr_0[mask0]

            mask1 = np.logical_and(mass_range[0] < log_M200_1, log_M200_1 < mass_range[1])
            j1 = fjzonc_1[mask1]
            m1 = mstarr_1[mask1]

        #from cdf to pdf
        if mass_weight:
            j0 = np.flip(-np.diff(j0 * m0[:, :, np.newaxis], axis=2), axis=2)
            j1 = np.flip(-np.diff(j1 * m1[:, :, np.newaxis], axis=2), axis=2)

            j0[np.isnan(j0)] = 0
            j1[np.isnan(j1)] = 0

        else:
            j0 = np.flip(-np.diff(j0, axis=2), axis=2)
            j1 = np.flip(-np.diff(j1, axis=2), axis=2)

        #norm then average. pretty sure average first is correct
        # j0 = j0 / np.amax(j0, axis=2)[:, :, np.newaxis]
        # j1 = j1 / np.amax(j1, axis=2)[:, :, np.newaxis]

        if diff_individual:
            diff_i = j1 - j0
            diff_i = diff_i / (j1 + j0) * 100

            if norm_r:
                diff_i = (j1 / np.amax(j1, axis=2)[:, :, np.newaxis]) - (j0 / np.amax(j0, axis=2)[:, :, np.newaxis])
                diff_i = diff_i / (((j1 / np.amax(j1, axis=2)[:, :, np.newaxis]) +
                                    (j0 / np.amax(j0, axis=2)[:, :, np.newaxis])) /2) * 100

            # diff_i = (j1 / np.sum(m1, axis=1)[:, np.newaxis, np.newaxis] -
            #           j0 / np.sum(m0, axis=1)[:, np.newaxis, np.newaxis])
            # diff_i = diff_i / (j1 / np.sum(m1, axis=1)[:, np.newaxis, np.newaxis] +
            #                    j0 / np.sum(m0, axis=1)[:, np.newaxis, np.newaxis]) * 100

            diff_i = np.nanmean(diff_i, axis=0)
            # diff_i = np.std(diff_i, axis=0)
            diff_i = diff_i.T

            # print(np.shape(m0)[0])
            mstar_frac_diff = (np.sum(m0, axis=1) - np.sum(m1, axis=1)) / ((np.sum(m0, axis=1) + np.sum(m1, axis=1))/2)
            # print(mstar_frac_diff)
            print(np.mean(mstar_frac_diff))
            # print(np.median(mstar_frac_diff))
            # print(np.std(mstar_frac_diff))

            #norm mass in radius

        #stack pdfs
        j0 = np.nanmean(j0 / np.sum(np.sum(j0, axis=2), axis=1)[:, np.newaxis, np.newaxis], axis=0)
        j1 = np.nanmean(j1 / np.sum(np.sum(j1, axis=2), axis=1)[:, np.newaxis, np.newaxis], axis=0)
        # j0 = np.mean(j0, axis=0)
        # j1 = np.mean(j1, axis=0)
        # j0 = np.nanmean(j0, axis=0)
        # j1 = np.nanmean(j1, axis=0)
        # j0 = j0[0]
        # j1 = j1[0]

        if diff_individual:
            diff = diff_i

        else:
            diff = (j1 - j0).T
            # percent
            diff = diff / ((j1 + j0).T / 2) * 100

        if half_j_res:
            j0_hjr = np.array([j0[:, 0+i*2] + j0[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
            j1_hjr = np.array([j1[:, 0+i*2] + j1[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
            diff = (j1_hjr - j0_hjr).T
            diff = diff / ((j1_hjr + j0_hjr).T / 2) * 100

        # j0_hjr = np.array([j0[:, 0+i*16] + j0[:, 1+i*16] for i in range(n_jzjc_bins//16)]).T
        # j1_hjr = np.array([j1[:, 0+i*16] + j1[:, 1+i*16] for i in range(n_jzjc_bins//16)]).T
        # diff = (j1_hjr - j0_hjr).T
        # diff = diff / ((j1_hjr + j0_hjr).T / 2) * 100

        # j0 = np.array([j0[:, 0+i*2] + j0[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
        # j0 = np.array([j0[0+i*2, :] + j0[1+i*2, :] for i in range(np.shape(j1)[0]//2)])
        # j1 = np.array([j1[:, 0+i*2] + j1[:, 1+i*2] for i in range(n_jzjc_bins//2)]).T
        # j1 = np.array([j1[0+i*2, :] + j1[1+i*2, :] for i in range(np.shape(j1)[0]//2)])

        # j0 = np.array([j0[:, 0+i] + j0[:, 1+i] for i in range(n_jzjc_bins-2)]).T
        # j0 = np.array([j0[0+i*16, :] + j0[1+i*16, :] for i in range(np.shape(j1)[0]//16)])
        # j1 = np.array([j1[:, 0+i] + j1[:, 1+i] for i in range(n_jzjc_bins-2)]).T
        # j1 = np.array([j1[0+i*16, :] + j1[1+i*16, :] for i in range(np.shape(j1)[0]//16)])

        j0 = j0 * np.shape(j0)[0] * np.shape(j0)[1]
        j1 = j1 * np.shape(j1)[0] * np.shape(j1)[1]

        #norm each r
        if norm_r:
            j0 = j0 / np.amax(j0, axis=1)[:, np.newaxis]
            j1 = j1 / np.amax(j1, axis=1)[:, np.newaxis]

        if log_mass:
            if norm_r:
                j0 = np.log10(j0 + 1e-1)
                j1 = np.log10(j1 + 1e-1)
            else:
                j0 = np.log10(j0 + 3e-1)
                j1 = np.log10(j1 + 3e-1)
                # j0 = np.log10(j0 + 3e9)
                # j1 = np.log10(j1 + 3e9)

        if len(mass_bin_edges) > 1:
            #hard code
            j_edges = [-0.5, 1.2]
        else:
            j_edges = [np.amin((np.nanmin(j0), np.nanmin(j1))), np.amax((np.nanmax(j0), np.nanmax(j1)))]

        diff_edges = [np.amin((np.nanmin(diff), -np.nanmax(diff)))] * 2
        diff_edges[1] = -diff_edges[1]
        diff_edges = [-90,90]
        # diff_edges = [-1,1]

        j_cmap = 'viridis' #'cubehelix' #'magma' #'cubehelix'
        diff_cmap = 'twilight_shifted'

        diff_cmap = matplotlib.colors.LinearSegmentedColormap.from_list(
            'trunc', plt.get_cmap(diff_cmap)(np.linspace(0.08, 0.92, 265)))

        # diff_cmap = 'Spectral'
        # diff_cmap = lambda x: matplotlib.cm.get_cmap(diff_cmap)( 0.1 + 0.8 * x )
        axs[row][0].imshow(j0.T, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
                           cmap=j_cmap, vmin=j_edges[0], vmax=j_edges[1])
        axs[row][1].imshow(j1.T, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
                           cmap=j_cmap, vmin=j_edges[0], vmax=j_edges[1])
        axs[row][2].imshow(diff, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
                           cmap=diff_cmap, vmin=diff_edges[0], vmax=diff_edges[1])
        # axs[row][3].imshow(diff_i, extent=[*xextent, *ylims], aspect=np.diff(xlims)/np.diff(ylims),
        #                    cmap=diff_cmap, vmin=diff_i_edges[0], vmax=diff_i_edges[1])

        axs[row][0].text(xlims[0] + 0.035 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'$\log \, M_{200} / $M$_\odot \in$' + str(mass_bin_edges[row]),
                         color='white', zorder=12, #)
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))
        axs[row][0].text(xlims[0] + 0.965 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'HR',
                         color='white', zorder=12, ha='right',
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))
        axs[row][1].text(xlims[0] + 0.965 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                         r'LR',
                         color='white', zorder=12, ha='right',
                         bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))

        if do_match:
            axs[row][1].text(xlims[0] + 0.035 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                             r'N = ' + f'{np.shape(m0)[0]}' + r' matched disks',
                             color='white', zorder=12,
                             bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))
        else:
            axs[row][1].text(xlims[0] + 0.035 * (xlims[1] - xlims[0]), ylims[0] + 0.05 * (ylims[1] - ylims[0]),
                             r'N = ' + f'{np.shape(m0)[0]}, {np.shape(m1)[0]}',
                             color='white', zorder=12,
                             bbox=dict(boxstyle='round', fc='k', ec='0.8', alpha=0.8))

        gradient = np.array([np.linspace(0, 1, 101)] * 2)
        if not norm_r:
            mass_cmap_axis.imshow(gradient, cmap=j_cmap,
                                  extent=[*j_edges, 0, 1], aspect=cmap_axis_ratio * np.diff(j_edges))
        diff_cmap_axis.imshow(gradient, cmap=diff_cmap,
                              extent=[*diff_edges, 0, 1], aspect=cmap_axis_ratio * np.diff(diff_edges))

        for col in range(3):
            if col < 2:
                c='white'
            else:
                c='k'
            axs[row][col].axvline(np.log10(EAGLE_EPS), 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axvline(np.log10(2.8 * EAGLE_EPS), 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axvline(np.log10(keys.STAR_FRAME_APERTURE_PHYSICAL), 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axhline(0, 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)
            axs[row][col].axhline(0.7, 0, 1,
                                  c=c, ls=':', lw=1, zorder=4, alpha=1)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    return


def mspur_evo(name='', save=False):

    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(3.3, 3.3 + 0.04, forward=True)
    spec = fig.add_gridspec(ncols=1, nrows=1)

    xlims = [0, UNIVERSE_AGE]
    ylims = [10,12]

    xlabel = r'$t$ [Gyr]'
    xlabel2 = r'$z$'
    ylabel = r'$\log \, M_{200}^{\rm spur} / {\rm M}_\odot$'

    axs = []
    for row in range(1):
        axs.append([])
        for col in range(1):
            axs[row].append(fig.add_subplot(spec[row, col]))

            axs2 = axs[0][0].twiny()
            # scale factor
            axs2.set_xlim([0, 1])
            z_numbers = np.array([0, 0.5, 1, 2])#, 4, 8])
            z_labels = ['0', '0.5', '1', '2']#, '4', '8']
            t_norm = mgm.lbt(1 / (1 + z_numbers)) / UNIVERSE_AGE

            axs2.set_xticks(t_norm)
            axs2.set_xticklabels(z_labels)
            axs2.set_xlabel(xlabel2)

            axs[row][col].set_xlim(xlims)
            axs[row][col].set_ylim(ylims)

            axs[row][col].set_ylabel(ylabel)
            axs[row][col].set_xlabel(xlabel)

            axs[row][col].set_aspect(np.diff(xlims) / np.diff(ylims))

    fig.subplots_adjust(hspace=0.00, wspace=0.00)
    
    zs = [0, 0.503, 1.004, 2.012]
    ts = mgm.lbt(1/(1+np.array(zs)))
    
    print(ts)

    M200_HR = [m200_heating_intercept(z, True) for z in zs]
    Mstar_HR = [mass_function(mass_function_args(z, True), M200) for z, M200 in zip(zs, M200_HR)]
    
    M200_LR = [m200_heating_intercept(z, False) for z in zs]
    Mstar_LR = [mass_function(mass_function_args(z, False), M200) for z, M200 in zip(zs, M200_LR)]

    axs[0][0].errorbar(ts, M200_HR,
                       c='C0', marker='o', markersize=8, 
                       ls='')
                       # ls=':', lw=1, path_effects=path_effect_2)
                       # ls='-', lw=2, path_effects=path_effect_4)
    axs[0][0].errorbar(ts, M200_LR,
                       c='C1', marker='o', markersize=8, 
                       ls='')
                       # ls=':', lw=1, path_effects=path_effect_2)
                       # ls='--', lw=2, path_effects=path_effect_4)

    # axs[0][0].errorbar(ts, Mstar_HR,
    #                    c='C0', marker='o', markersize=8, ls='-', lw=2, path_effects=path_effect_4)
    # axs[0][0].errorbar(ts, Mstar_LR,
    #                    c='C1', marker='o', markersize=8, ls='--', lw=2, path_effects=path_effect_4)

    # norm = 7.35 #10^10
    # norm = 13.35 #1
    # norm = 11.55 #7 #9.75 #4
    norm = 10.95 #7 #9.75 #4
    # log_func = lambda z, mdm, norm=norm, z_power=-1.5, m_power=0.6: norm + z_power * np.log10(1 + z) + m_power * np.log10(mdm)
    log_func = lambda z, mdm, norm=norm, z_power=-1.5, m_power=0.6: np.log10(10**norm * (1 + z)**z_power * mdm**m_power)
    
    # norm = 12.25
    # log_func = lambda z, mdm, norm=norm, z_power=-1.5, m_power=0.6: z_power * np.log10(1 + z) + m_power * np.log10(mdm * 10**norm)
    # log_func = lambda z, mdm, norm=norm, z_power=-1.5, m_power=0.6: np.log10( (mdm * 10**norm)**m_power * (1 + z)**z_power )

    lin_z = np.linspace(0, 10, 100)
    lin_t = mgm.lbt(1/(1+np.array(lin_z)))

    lin_LR = log_func(lin_z, DM_MASS * 10 **(10-6)) #* 10 **10)
    lin_HR = log_func(lin_z, DM_MASS/7 * 10 **(10-6)) #* 10 **10)
    
    print(lin_HR)

    axs[0][0].errorbar(lin_t, lin_HR,
                       c='C0', ls=':', lw=2, zorder=-3)
    axs[0][0].errorbar(lin_t, lin_LR,
                       c='C1', ls=':', lw=2, zorder=-3)

    if save:
        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()
    
    return
    
if __name__ == '__main__':
    print('Ignoring numpy warnings')
    np.seterr(all='ignore')

    from time import time as _t
    start_time = _t()

    save = True

    #TODO reorder

    #figure 1
    # visulization

    #figure 2
    # matched_fraction_success('../results/matched_fraction_summary.pdf', save=save)
    # matched_fraction_success('../results/matched_fraction_summary_subhaloes.pdf', save=save,
    #                          sub=True, seperate_panels=False, stellar_masses=True)

    # figure 3
    # stellar_mass_halo_mass_relation(name='../results/stellarmasshalomass.pdf', save=save)

    #figure 4
    # stellar_mass_all_well(name='../results/stellarmassgasmass.pdf', save=save)
    # stellar_mass_all_good(name='../results/stellarmassbhmass.pdf', save=save)
    # bh_mass_diff(name='../results/stellarmassbhmass_diff.pdf', save=save)

    # figure 5
    # more_kappa(name='../results/more_kappa_scaling.pdf', save=save)
    # sf_temp(name='../results/sf_temp_mass.pdf', save=save)
    # formation_time(name='../results/formation_time.pdf', save=save)
    # sf_gas_density_mass(name='../results/sf_gas_density_mass.pdf', save=save)
    # star_formation_rate(name='../results/star_formation_rate.pdf', save=save)

    # # #figure 6
    # # matched_mass(name='../results/matched_mass.pdf', save=save, mstar=True)
    # matched_mass(name='../results/matched_size.pdf', save=save, size=True)
    # matched_mass(name='../results/matched_disp.pdf', save=save, s_tot=True)

    #figure 7
    # size_mass_relation(name='../results/sizemass.pdf', save=save)
    # more_size_mass_relation(name='../results/moresizemass.pdf', save=save)

    #figure 8
    # dispersion_mass_relation(name='../results/dispersionmass.pdf', save=save)
    # dispersion_mass_relation(name='../results/dispersionmass_ek_with_mean.pdf', save=save, ek=True)
    # dispersion_mass_relation(name='../results/dispersionmass_z.pdf', save=save, sigma_z=True)

    #figure 9
    # proj_dispersion_mass_relation(name='../results/dispersionmass_proj.pdf', save=save)

    #figure 10
    # scaling_size_mass(name='../results/scaling_size_mass_0.pdf', save=save)
    # scaling_vc_mass(name='../results/scaling_vc_mass_0.pdf', save=save)

    #figure 11
    # scaling_am_mass(name='../results/scaling_am_mass.pdf', save=save)

    #figure 12
    # sigma_rad_stacked(name='../results/sigma_rad_stacked.pdf', save=save)
    # sigma_rad_stacked(name='../results/sigma_rad_stacked_for_testing.pdf', save=save,
    #                   dmsigma=True, jeans_star=True)
    # vc_rad_summary(name='../results/vc_rad_summary.pdf', save=save)

    #figure 13
    # sigma_rad_summary(name='../results/sigma_rad_summary.pdf', save=save)
    # sigma_rad_summary(name='../results/sigma_rad_summary_norm.pdf', save=save, r12_norm=True)

    # #figure 14
    # more_am_scaling(name='../results/sf_am_scaling.pdf', save=save,
    #                 sfgas=True, stars=False, gas=False, CC_stars=False, DM=False,
    #                 all_z=False, jz=False, flat_scale=True, sim_low=0, sim_hi=2)
    # more_am_scaling(name='../results/hr_am_scaling.pdf', save=save,
    #                 stars=True, sfgas=True, gas=False, CC_stars=False, DM=False,
    #                 all_z=False, jz=False, flat_scale=True, sim_low=0, sim_hi=1)
    # more_am_scaling(name='../results/lr_am_scaling.pdf', save=save,
    #                 stars=True, sfgas=True, gas=False, CC_stars=False, DM=False,
    #                 all_z=False, jz=False, flat_scale=True, sim_low=1, sim_hi=2)

    # more_size_scaling(name='../results/sf_size_scaling.pdf', save=save,
    #                   sfgas=True, stars=False, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, flat_scale=True, sim_low=0, sim_hi=2)
    # more_size_scaling(name='../results/hr_size_scaling.pdf', save=save,
    #                   stars=True, sfgas=True, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, flat_scale=False, sim_low=0, sim_hi=1,
    #                   mass_m200=True)
    # more_size_scaling(name='../results/lr_size_scaling.pdf', save=save,
    #                   stars=True, sfgas=True, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, flat_scale=False, sim_low=1, sim_hi=2,
    #                   mass_m200=True)

    # more_size_scaling(name='../results/hr_size_swap.pdf', save=save,
    #                   stars=True, sfgas=False, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, flat_scale=False, sim_low=0, sim_hi=1, swap=True, tcx=False)
    # more_size_scaling(name='../results/lr_size_swap.pdf', save=save,
    #                   stars=True, sfgas=False, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, flat_scale=False, sim_low=1, sim_hi=2, swap=True, tcx=False)

    # more_size_scaling(name='../results/hr_size_swap_scale.pdf', save=save,
    #                   stars=True, sfgas=False, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, sim_low=0, sim_hi=1,
    #                   swap=True, tcx=False, r200y=True, flat_scale=False,
    #                   mass_m200=True)
    # more_size_scaling(name='../results/lr_size_swap_scale.pdf', save=save,
    #                   stars=True, sfgas=False, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, sim_low=1, sim_hi=2,
    #                   swap=True, tcx=False, r200y=True, flat_scale=False,
    #                   mass_m200=True)
    # more_size_scaling(name='../results/lr_size_swap_scale.pdf', save=save,
    #                   stars=True, sfgas=False, gas=False, CC_stars=False, DM=False,
    #                   all_z=False, sim_low=0, sim_hi=2,
    #                   swap=True, tcx=True, r200y=True, flat_scale=False,
    #                   mass_m200=True)
    
    # more_am_scaling(name='../results/more_am_scaling_r12.pdf', save=save, r12=True)
    # am_rad_stacked(name='../results/am_rad_stacked_by_age.pdf', save=save, stars_by_age=True)
    # am_rad_stacked(name='../results/am_rad_stacked.pdf', save=save, stars_by_age=False)

    # jangle(name='../results/jangle.pdf', save=save)

    #figure 15
    # more_sigma_rad_stacked(name='../results/sigma_rad_stacked_by_age.pdf', save=save, stars_by_age=True)
    # more_sigma_rad_stacked(name='../results/sigma_rad_stacked.pdf', save=save, stars_by_age=False)

    # more_sigma_scaling(name='../results/more_sigma_scaling.pdf', save=save)

    # figure 16
    # dispersion_age(name='../results/dispersion_age_0.pdf', save=save)
    # dispersion_age(name='../results/dispersion_age_05.pdf', save=save, z_index=1)
    # dispersion_age(name='../results/dispersion_age_0_r200.pdf', save=save, r12=False)
    # dispersion_age(name='../results/dispersion_age_1.pdf', save=save, z_index=2)
    # zoom seems to work better for z!=0
    # dispersion_age(name='../results/dispersion_zoom.pdf', save=save, zoom_bins=True)

    #figure 17
    # shape_plots(name='../results/shape_plots.pdf', save=save, all_z=True)
    # shape_plots(name='../results/shape_plots_ba.pdf', save=save, ca=False, ba=True, all_z=True)

    # figure 18
    # shape_relation_plots(name='../results/shape_relation_plots.pdf', save=save)

    #figure 19
    # kappa_fractions(name='../results/kappa_fractions_solo.pdf', save=save, disks=False)
    # kappa_fractions(name='../results/kappa_fractions_all.pdf', save=save, disks=False)
    # kappa_fractions(name='../results/DT_fractions_all.pdf', save=save, disks=False, DT=True)
    # kappa_fractions(name='../results/ST_fractions_all.pdf', save=save, disks=False, ST=True)

    # kappa_co_limits_plot(name='../results/kappa_co_limits_plot.pdf', save=save, all_z=False)
    # kappa_co_age_plot(name='../results/kappa_co_age_plot.pdf', save=save)

    #figure 20
    # disk_fraction_cut_plot(name='../results/disk_fraction_cut_plot.pdf', save=save)

    # #figure 21
    # stacked_disk_jzoncs(name='../results/stacjed_disk_jzoncs.pdf', save=save)
    # stacked_gas_jzoncs(name='../results/stacjed_gas_jzoncs.pdf', save=save)

    #figure Bonus!
    # matched_mass(name='../results/matched_mass_kappa_co.pdf', save=save, k_co=True)
    # matched_mass(name='../results/matched_mass_ca.pdf', save=save, ca=True)
    # matched_mass_all_indicators(name='../results/matched_mass_all.pdf', save=save)

    #####
    # bonus
    # matched_appendix_plots('../results/matched_appendix_mass.pdf', save=save, mass=True)
    # matched_appendix_plots('../results/matched_appendix_kin.pdf', save=save, kinematics=True)
    # matched_appendix_plots('../results/matched_appendix_size.pdf', save=save, size=True)
    # matched_appendix_plots('../results/matched_appendix_am.pdf', save=save, am=True)
    # matched_appendix_plots('../results/matched_appendix_shape.pdf', save=save, shape=True)
    # matched_appendix_plots('../results/matched_appendix_kin_ind.pdf', save=save, kin_ind=True)
    # matched_appendix_plots('../results/matched_appendix_orb_ind.pdf', save=save, orb_ind=True)

    # mspur_evo(name='../results/mspur_evo', save=save)

    # height_age(name='../results/dispersion_age_0.pdf', save=False)

    #figure 11 #this is probably getting removed ...
    # am_rad_stacked(name='../results/am_rad_stacked.pdf', save=save)

    # kappa_fractions(name='../results/DT_fractions_disk.pdf', save=save, disks=True, DT=True)
    # kappa_fractions(name='../results/ST_fractions_disk.pdf', save=save, disks=True, ST=True)
    # kappa_fractions(name='../results/kappa_fractions_disk.pdf', save=save, disks=True)

    # dispersion_mass_relation(name='../results/ekmass.pdf', save=save, ek=True, uncontracted=True, star_jeans=True)
    # fraction_of_heated_galaxies(name='../results/heated_fraction.pdf', save=save)

    # dispersion_age(name='../results/mean_v_phi_age_0.pdf', save=save, mean_v_phi=True)

    # am_mass_rg_plot(name='../results/am_mass_rg.pdf', save=save)

    # mean_v_phi_rad_stacked(mean_v_phi=False, all_vcs=True,
    #                        name='../results/vc_rad_stacked.pdf', save=save)

    # mean_v_phi_rad_stacked(name='../results/mean_v_phi_rad_stacked.pdf', save=save)
    # mass_rad_stacked(name='../results/mass_rad_stacked.pdf', save=save)
    # am_rad_ratio_stacked(name='../results/am_rad_ratio_stacked.pdf', save=save)


    # matching
    # matched_sigma_resolved(name='../results/matched_sigma_resolved.pdf', save=save)
    # matched_comparison(name='../results/matched_comparison.pdf', save=save)
    # matched_mass(name='../results/matched_mass.pdf', save=save)
    # matched_mass(name='../results/matched_mass_r200.pdf', save=save, r12=False)

    # matched_mass(name='../results/matched_mass_2.pdf', save=save, z_index=2)

    # matched_disk_galaxy_sample(name='../results/matched_disk_galaxy_sample.pdf', save=save)

    # kappa_percentiles(name='../results/kappa_percentiles_disk.pdf', save=save, disks=True)
    # kappa_percentiles(name='../results/kappa_percentiles_all.pdf', save=save, disks=False)

    end_time = _t()
    print('Time', round(end_time - start_time,1))

    plt.show()

    pass
