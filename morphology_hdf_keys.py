#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""
import numpy as np

import h5py as h5

from my_galaxy_models import *

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

cosmo_to_cgs_desnity_units = 6.76991e-31 #g cm^-3 / 10^10 M_sun Mpc^-3

STAR_FRAME_APERTURE_PHYSICAL = 30
GAS_FRAME_APERTURE_PHYSICAL = 70
n_jzjc_bins = 21

PROTON_MASS = 1.672_621_924e-24 #g
COSMOLOGICAL_X = 0.75 #fraction of hydrogen by mass

N_H1_PER_g = COSMOLOGICAL_X / PROTON_MASS # g^-1
#1.2285 needs to be in there from Shaye et al. 2015
# N_H1_PER_g = 1 / PROTON_MASS / 1.2285 # g^-1

#1.00794

def concentration_ludlow(z, M200):
    '''Ludlow et al. 2016. Appendix B
    M200 in 10^10 M_sun
    '''
    delta_sc = 1.686

    c_0 = 3.395 * (1 + z)**(-0.215)

    beta = 0.307 * (1 + z)**0.540

    gamma_1 = 0.628 * (1 + z)**(-0.047)

    gamma_2 = 0.317 * (1 + z)**(-0.893)

    omega_l0 = 0.693
    omega_m0 = 0.307
    little_h = 0.6777

    omega_l = omega_l0 / (omega_l0 + omega_m0 * (1 + z)**3)
    omega_m = 1 - omega_l

    psi  = omega_m**(4/7) - omega_l + (1 + omega_m / 2) * (1 + omega_l/70)
    psi0 = omega_m0**(4/7) - omega_l0 + (1 + omega_m0 / 2) * (1 + omega_l0/70)

    a = (1 + z)**(-1.0)

    Dz = omega_m / omega_m0 * psi0 / psi * (1 + z)**(-1.0)

    nu_0 = (4.135 - 0.564 * a**(-1.0) - 0.210 * a**(-2.0) +
          0.0557 * a**(-3.0) - 0.00348 * a**(-4.0)) * Dz**(-1.0)

    #TODO check if don't need little h if units are in 10^10 Msun
    xi = (M200 / little_h)**(-1.0)

    sigma = Dz * 22.26 * xi**0.292 / (1 + 1.53 * xi**0.275 + 3.36 * xi**0.198)

    nu = delta_sc / sigma

    c = c_0 * (nu / nu_0)**(-gamma_1) * (1 + (nu / nu_0)**(1/beta) )**(-beta*(gamma_2 - gamma_1))

    return(c)


def process_array(raw_data, key, mask, column):
    if key is None:
        return None

    data = raw_data[key][()]

    try:
        if np.size(data) > 1:
            if mask is not None:
                data = data[mask]

            if column is not None:
                data = data[:, column]

            if np.size(data) == np.sum(mask) * np.sum(column):
                data = data.reshape((np.sum(mask), np.sum(column)))
        else:
            data = data * np.ones((np.sum(mask), np.sum(column)))
    except (TypeError, IndexError):
        # if np.size(data) > 1:
        #     data = data[mask]
        #     data = data[:, column]
        if np.size(data) == np.sum(mask):
            data = data.reshape(np.sum(mask))
        # else:
        #     data = data * np.ones(np.sum(mask))

    return data


def return_full_array(raw_data0, mask0=None,
                      x0key=None, x0column=None,
                      x1key=None, x1column=None,
                      x2key=None, x2column=None,
                      x3key=None, x3column=None,
                      x4key=None, x4column=None,
                      xtransform = lambda x: x):

    data = []
    x0data = process_array(raw_data0, x0key, mask0, x0column)
    data.append(x0data)
    if x1key != None:
        x1data = process_array(raw_data0, x1key, mask0, x1column)
        data.append(x1data)
    if x2key != None:
        x2data = process_array(raw_data0, x2key, mask0, x2column)
        data.append(x2data)
    if x3key != None:
        x3data = process_array(raw_data0, x3key, mask0, x3column)
        data.append(x3data)
    if x4key != None:
        x4data = process_array(raw_data0, x4key, mask0, x4column)
        data.append(x4data)

    if 'Formation' not in x0key:
        shapes = [np.shape(d) for d in data]
        correct_shape = shapes[np.argmax([len(s) for s in shapes])]
        for i, (d, s) in enumerate(zip(data, shapes)):
            if s != correct_shape:
                data[i] = d[:, np.newaxis]

    x0 = xtransform(*data)

    return x0


def return_full_mask(raw_data0,
                     mask0key=None, mask0column=None, comp0=None, mask0transform = lambda mask: True,
                     mask1key=None, mask1column=None, comp1=None, mask1transform = lambda mask: True,
                     mask2key=None, mask2column=None, comp2=None, mask2transform = lambda mask: True,
                     ii=0):

    if mask2key != None:
        mask0 = np.hstack(mask0transform(return_array_from_keys(raw_data0, mask0key, mask0column, comp0, ii=ii)))
        mask1 = np.hstack(mask1transform(return_array_from_keys(raw_data0, mask1key, mask1column, comp1, ii=ii)))
        mask2 = np.hstack(mask2transform(return_array_from_keys(raw_data0, mask2key, mask2column, comp2, ii=ii)))

        mask0 = np.logical_and(np.logical_and(mask0, mask1), mask2)

    elif mask1key != None:
        mask0 = np.hstack(mask0transform(return_array_from_keys(raw_data0, mask0key, mask0column, comp0, ii=ii)))
        mask1 = np.hstack(mask1transform(return_array_from_keys(raw_data0, mask1key, mask1column, comp1, ii=ii)))

        mask0 = np.logical_and(mask0, mask1)

    elif mask0key != None:
        mask0 = np.hstack(mask0transform(return_array_from_keys(raw_data0, mask0key, mask0column, comp0, ii=ii)))
    else:
        mask0 = np.ones(np.shape(raw_data0['GalaxyQuantities/GroupNumber'][()]), dtype=bool)

    return mask0


def return_array_from_keys(raw_data0, xkey, rkey, comp, mask=None, ii=None):

    x0 = return_full_array(raw_data0, mask,
                           x0key=key0_dict[xkey][comp], x0column=column0_dict[xkey][rkey, ii],
                           x1key=key1_dict[xkey][comp], x1column=column1_dict[xkey][rkey, ii],
                           x2key=key2_dict[xkey][comp], x2column=column2_dict[xkey][rkey, ii],
                           x3key=key3_dict[xkey][comp], x3column=column3_dict[xkey][rkey, ii],
                           x4key=key4_dict[xkey][comp], x4column=column4_dict[xkey][rkey, ii],
                           xtransform=transform_dict[xkey])

    return x0


def return_arrays_from_keys(raw_data0, xkey, comp, radial_bin_mask, mask=None):

    x0 = return_full_array(raw_data0, mask,
                           x0key=key0_dict[xkey][comp],
                           x0column=radial_bin_mask if not isinstance(column0_dict[xkey], FakeOb) else column0_dict[xkey],
                           x1key=key1_dict[xkey][comp],
                           x1column=radial_bin_mask if not isinstance(column1_dict[xkey], FakeOb) else column1_dict[xkey],
                           x2key=key2_dict[xkey][comp],
                           x2column=radial_bin_mask if not isinstance(column2_dict[xkey], FakeOb) else column2_dict[xkey],
                           x3key=key3_dict[xkey][comp],
                           x3column=radial_bin_mask if not isinstance(column3_dict[xkey], FakeOb) else column3_dict[xkey],
                           x4key=key4_dict[xkey][comp],
                           x4column=radial_bin_mask if not isinstance(column4_dict[xkey], FakeOb) else column4_dict[xkey],
                           xtransform=transform_dict[xkey])

    return x0


class FakeString(str):
    def __init__(self, value):
        str.__init__(value)
    def __getitem__(self, index):
        return(self)

class FakeOb:
    def __init__(self, ob):
        self.ob = ob
    def __getitem__(self, index):
        return (self.ob)

class FakeReturn:
    def __init__(self):
        pass
    def __getitem__(self, index):
        return (index)

class InsertStrOb:
    def __init__(self, front, back):
        self.front = front
        self.back = back
    def __getitem__(self, index):
        return (self.front + index + self.back)

class NoneDict(dict):
    def __init__(self):
        super().__init__(self)

    def __getitem__(self, index):
        try:
            return(super().__getitem__(index))
        except KeyError:
            return(FakeOb(None))

class FalseDict(dict):
    def __init__(self):
        super().__init__(self)

    def __getitem__(self, index):
        try:
            return(super().__getitem__(index))
        except KeyError:
            return(False)

class ExtraArgDict(dict):
    def __init__(self):
        super().__init__(self)

    def __getitem__(self, index):
        return(super().__getitem__(index[0])(index[1]))

#TODO make dictionaty that returns non if no key

#TODO load this

#TODO read from file after creating next time!
lin_bin_edges = np.linspace(0, 30, 1)  # *2 for proj and 3d
log_bin_edges = np.logspace(-1, 3, 5*4+1)  # *2 for proj and 3d
# log_bin_edges = np.logspace(-1, 3, 8*4+1)  # *2 for proj and 3d
# + r<30kpc, r<r200, r<R_half, r<R_quarter, r<R3quarter, R=R_half, R=R_quater, R=R3quater
# + r<30kpc, r<r200, r<r_half, r<2*r_half, r<5*r_half, R=R_half

t_t0 = 0
t_t2 = 2
t_t4 = 4
t_t6 = 6
t_t8 = 8
t_t10 = 10
t_t12 = 12
t_t14 = 14 #UNIVERSE_AGE ??? this messes everything up on at least one plot
t_bin_edges = np.array([t_t0, t_t2, t_t4, t_t6, t_t8, t_t10, t_t12, t_t14])

# change if bins change
n_lin = len(lin_bin_edges) - 1 #6
n_log = len(log_bin_edges) - 1 #3
n_scale = 8
n_star_extra = 7 * 2
n_dim = 2 * n_lin + n_log + n_lin + n_scale

# [[star_J,
#   r_half, r_quarter, r3quarter,
#   R_half, R_quarter, R3quarter],
#  # [lin_profiles, log_profiles, proj_lin_profiles, proj_log_profiles,
#  [log_profiles,
#   aperture_30, aperture_r200, aperture_3d_r12, aperture_3d_5r12, annuli_3d_r12,
#   annuli_face_R12, annuli_3d_rs, apature_box_R12_box,
#   *age_kinematics_half, *age_kinematics_r200, *age_log_profiles, yet_to_CC_profile, yet_to_CC_r200]])

# #TODO read these in...
# lower_bin_edges = np.hstack((*[f'r{e:.3}' for e in lin_bin_edges[:-1]], *[f'r{e:.3}' for e in log_bin_edges[:-1]],
#                              *[f'r{e:.3}' for e in lin_bin_edges[:-1]], *[f'r{e:.3}' for e in lin_bin_edges[:-1]],
#                              0, 0, 0, 0, '0.8 r1/2', '0.8 R1/2', '0.8 0.2 rs', 0,
#                              *[f'rh {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
#                              *[f'r200 {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
#                              *[[f'r{e:.3} {t_bin_edges[i]}-{t_bin_edges[i+1]}' for e in log_bin_edges[:-1]]
#                               for i in range(len(t_bin_edges)-1)],
#                              *[f'CC r{e:.3}' for e in log_bin_edges[:-1]],
#                              0)
#                             ).astype(dtype=np.string_)
#
# upper_bin_edges = np.hstack((*[f'r{e:.3}' for e in lin_bin_edges[1:]], *[f'r{e:.3}' for e in log_bin_edges[1:]],
#                              *[f'r{e:.3}' for e in lin_bin_edges[1:]], *[f'r{e:.3}' for e in lin_bin_edges[1:]],
#                              'r30', 'r200', 'r1/2', '5r1/2', '1.25 r1/2', '1.25 R1/2', '1.25 0.2 rs', 'box R1/2',
#                              *[f'rh {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
#                              *[f'r200 {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
#                              *[[f'r{e:.3} {t_bin_edges[i]}-{t_bin_edges[i+1]}' for e in log_bin_edges[1:]]
#                               for i in range(len(t_bin_edges)-1)],
#                              *[f'CC r{e:.3}' for e in log_bin_edges[1:]],
#                              'CC r200')
#                             ).astype(dtype=np.string_)

lower_bin_edges = np.hstack((*[f'r{e:.3}' for e in lin_bin_edges[:-1]], *[f'r{e:.3}' for e in log_bin_edges[:-1]],
                             *[f'r{e:.3}' for e in lin_bin_edges[:-1]], *[f'r{e:.3}' for e in lin_bin_edges[:-1]],
                             0, 0, 0, 0, '0.8 r1/2', '0.8 R1/2', '0.8 0.2 rs', 0,
                             0,
                             *[f'CC r{e:.3}' for e in log_bin_edges[:-1]],
                             *[f'rh {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
                             *[f'r200 {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
                             *[[f'r{e:.3} {t_bin_edges[i]}-{t_bin_edges[i+1]}' for e in log_bin_edges[:-1]]
                              for i in range(len(t_bin_edges)-1)],)
                            ).astype(dtype=np.string_)

upper_bin_edges = np.hstack((*[f'r{e:.3}' for e in lin_bin_edges[1:]], *[f'r{e:.3}' for e in log_bin_edges[1:]],
                             *[f'r{e:.3}' for e in lin_bin_edges[1:]], *[f'r{e:.3}' for e in lin_bin_edges[1:]],
                             'r30', 'r200', 'r1/2', '5r1/2', '1.25 r1/2', '1.25 R1/2', '1.25 0.2 rs', 'box R1/2',
                             'CC r200',
                             *[f'CC r{e:.3}' for e in log_bin_edges[1:]],
                             *[f'rh {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
                             *[f'r200 {t_bin_edges[i]}-{t_bin_edges[i+1]}' for i in range(len(t_bin_edges)-1)],
                             *[[f'r{e:.3} {t_bin_edges[i]}-{t_bin_edges[i+1]}' for e in log_bin_edges[1:]]
                              for i in range(len(t_bin_edges)-1)],)
                            ).astype(dtype=np.string_)

# for i, (bl, bu) in enumerate(zip(lower_bin_edges, upper_bin_edges)):
#     print(i, bl, bu)

#
lt_r12_c = np.argmax(b'r1/2' == upper_bin_edges)
lt_5r12_c = np.argmax(b'5r1/2' == upper_bin_edges)
lt_r200_c = np.argmax(b'r200' == upper_bin_edges)
lt_r200_c_binned = np.argmax(b'r200 0-2' == upper_bin_edges)
lt_r30_c = np.argmax(b'r30' == upper_bin_edges)

eq_R12_c = np.argmax(b'1.25 R1/2' == upper_bin_edges)
eq_rh_c = np.argmax(b'1.25 r1/2' == upper_bin_edges)
lt_box_R12_c = np.argmax(b'box R1/2' == upper_bin_edges)
eq_02rs_c = np.argmax(b'1.25 0.2 rs' == upper_bin_edges)
eq_rh_c_binned = np.argmax(b'rh 0-2' == upper_bin_edges)

age_02_r_binned = np.argmax(b'r0.158 0-2' == upper_bin_edges)
age_24_r_binned = np.argmax(b'r0.158 2-4' == upper_bin_edges)
age_46_r_binned = np.argmax(b'r0.158 4-6' == upper_bin_edges)
age_68_r_binned = np.argmax(b'r0.158 6-8' == upper_bin_edges)
age_810_r_binned = np.argmax(b'r0.158 8-10' == upper_bin_edges)
age_1012_r_binned = np.argmax(b'r0.158 10-12' == upper_bin_edges)
age_1214_r_binned = np.argmax(b'r0.158 12-14' == upper_bin_edges)
#this doubles up as gas orientation
CC_r_binned = np.argmax(b'CC r0.158' == upper_bin_edges)
CC_r200 = np.argmax(b'CC r200' == upper_bin_edges)

#rkey
rkey_to_rcolum_dict = ExtraArgDict()
rkey_to_rcolum_dict.update({'r200':lambda ii: lt_r200_c,
                            'r200_profile':lambda ii: lt_r200_c_binned + ii,
                            'lt_30':lambda ii: lt_r30_c,
                            'lt_r12':lambda ii: lt_r12_c,
                            'lt_5r12':lambda ii: lt_5r12_c,
                            'eq_R12':lambda ii: eq_R12_c,
                            'eq_r12':lambda ii: eq_rh_c,
                            'eq_rh':lambda ii: eq_02rs_c,
                            'eq_rh_profile':lambda ii: eq_rh_c_binned + ii,
                            'eq_rh_profile0':lambda ii: eq_rh_c_binned + 0,
                            'eq_rh_profile1':lambda ii: eq_rh_c_binned + 1,
                            'eq_rh_profile2':lambda ii: eq_rh_c_binned + 2,
                            'eq_rh_profile3':lambda ii: eq_rh_c_binned + 3,
                            'eq_rh_profile4':lambda ii: eq_rh_c_binned + 4,
                            'eq_rh_profile5':lambda ii: eq_rh_c_binned + 5,
                            'eq_rh_profile6':lambda ii: eq_rh_c_binned + 6,
                            'eq_r12_profile':lambda ii: eq_rh_c_binned + ii,
                            'eq_r12_profile0':lambda ii: eq_rh_c_binned + 0,
                            'eq_r12_profile1':lambda ii: eq_rh_c_binned + 1,
                            'eq_r12_profile2':lambda ii: eq_rh_c_binned + 2,
                            'eq_r12_profile3':lambda ii: eq_rh_c_binned + 3,
                            'eq_r12_profile4':lambda ii: eq_rh_c_binned + 4,
                            'eq_r12_profile5':lambda ii: eq_rh_c_binned + 5,
                            'eq_r12_profile6':lambda ii: eq_rh_c_binned + 6,
                            'lt_box_R12': lambda  ii: lt_box_R12_c,
                            'box': lambda  ii: lt_box_R12_c,
                            'eq_02rs':lambda ii: eq_02rs_c,
                            'test':lambda ii: 'test',
                            'age_02_r_binned': lambda ii: age_02_r_binned + ii,
                            'age_24_r_binned': lambda ii: age_24_r_binned + ii,
                            'age_46_r_binned': lambda ii: age_46_r_binned + ii,
                            'age_68_r_binned': lambda ii: age_68_r_binned + ii,
                            'age_810_r_binned': lambda ii: age_810_r_binned + ii,
                            'age_1012_r_binned': lambda ii: age_1012_r_binned + ii,
                            'age_1214_r_binned': lambda ii: age_1214_r_binned + ii,
                            'CC_r_binned': lambda ii: CC_r_binned + ii,
                            'CC_r200': lambda ii: CC_r200,
                            'gas_o_binned': lambda ii: CC_r_binned + ii,
                            'gas_o_r12': lambda ii: CC_r200,
                            })

rkey_label_dict = {'r200':r'[All particles assigned to subhalo]',
                   'r200_profile':r'[All stars assigned to subhalo, 2Gyr age bins]',
                   'lt_30':r'[$r < 30$ kpc]',
                   'lt_r12':r'[$r < r_{1/2}$]',
                   'lt_5r12':r'[$r < 5 r_{1/2}$]',
                   'eq_R14':r'[$R = r_{1/4}$]',
                   'eq_R12':r'[$R = R_{1/2}$]',
                   'eq_r12':r'[$r = r_{1/2}$]',
                   'eq_rh':r'[$r = r_{1/2}$]',
                   'eq_rh_profile':r'[$r = 0.2 r_s,$ 2Gyr age bins]',
                   'eq_rh_profile0':r'[$r = 0.2 r_s,$ stars 0-2 Gyr old]',
                   'eq_rh_profile1':r'[$r = 0.2 r_s,$ stars 2-4 Gyr old]',
                   'eq_rh_profile2':r'[$r = 0.2 r_s,$ stars 4-6 Gyr old]',
                   'eq_rh_profile3':r'[$r = 0.2 r_s,$ stars 6-8 Gyr old]',
                   'eq_rh_profile4':r'[$r = 0.2 r_s,$ stars 8-10 Gyr old]',
                   'eq_rh_profile5':r'[$r = 0.2 r_s,$ stars 10-12 Gyr old]',
                   'eq_rh_profile6':r'[$r = 0.2 r_s,$ stars 12-14 Gyr old]',
                   'eq_02Rs': r'$[R = 3/4 \times 0.2 r_s(M_{200}, z)]$',
                   'eq_02rs': r'$[R = 0.2 r_s(M_{200}, z)]$',
                   'eq_R34':r'[$R = r_{3/4}$]',
                   'test':'test'
                   }

#xykey and comp
key0_dict = NoneDict()
key0_dict.update({'gn':FakeOb('GalaxyQuantities/GroupNumber'),
                  'sn':FakeOb('GalaxyQuantities/SubGroupNumber'),
                  'mstar':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mgas':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mdm':FakeOb('GalaxyProfiles/DM/BinMass'),
                  'm200':FakeOb('GalaxyQuantities/M200_crit'),
                  'mtot':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mbary':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mstarmtot':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mstarmtotr':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mstarm200':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mgasmtot':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mbarymtot':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mstarm200r':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mstarr':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mgasr':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mdmr':FakeOb('GalaxyProfiles/DM/BinMass'),
                  'mbh':FakeOb('GalaxyQuantities/BHMass'), #FakeOb('GalaxyQuantities/BHBinMass'),
                  'mbhmstar':FakeOb('GalaxyQuantities/BHMass'),
                  '02rs':FakeOb('GalaxyQuantities/M200_crit'),
                  'r200': FakeOb('GalaxyQuantities/R200_crit'),
                  'r12': FakeOb('GalaxyQuantities/StellarHalfMassRadius'),
                  'r12r': InsertStrOb('GalaxyProfiles/', '/rHalf'),
                  'r12_r200': FakeOb('GalaxyQuantities/StellarHalfMassRadius'),
                  'r12_r200r': InsertStrOb('GalaxyProfiles/', '/rHalf'),
                  'R12': FakeOb('GalaxyQuantities/StellarHalfMassProjRadius'),
                  'R12gas': FakeOb('GalaxyQuantities/GasHalfMassProjRadius'),
                  'z12': InsertStrOb('GalaxyProfiles/', '/zHalf'),
                  'z12R12': InsertStrOb('GalaxyProfiles/', '/zHalf'),
                  'z12R34': InsertStrOb('GalaxyProfiles/', '/zHalf'),
                  'z12R14': InsertStrOb('GalaxyProfiles/', '/zHalf'),
                  'ca': InsertStrOb('GalaxyProfiles/', '/Axisc'),
                  'ba': InsertStrOb('GalaxyProfiles/', '/Axisb'),
                  'cb': InsertStrOb('GalaxyProfiles/', '/Axisc'),
                  'Jz': InsertStrOb('GalaxyProfiles/', '/Jz'),
                  'Jtot': InsertStrOb('GalaxyProfiles/', '/Jtot'),
                  'jzjtot': InsertStrOb('GalaxyProfiles/', '/Jz'),
                  'jz': InsertStrOb('GalaxyProfiles/', '/Jz'),
                  'jtot': InsertStrOb('GalaxyProfiles/', '/Jtot'),
                  'kappa_rot': InsertStrOb('GalaxyProfiles/', '/kappaRot'),
                  'quart_kappa_rot': InsertStrOb('GalaxyProfiles/', '/kappaRot'),
                  'kappa_co': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  'kappa_co_disk': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  'quart_kappa_co': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  'quart_kappa_co_disk': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  'mean_v_R': InsertStrOb('GalaxyProfiles/', '/MeanVR'),
                  'mean_v_phi': InsertStrOb('GalaxyProfiles/', '/MeanVphi'),
                  'mean_v_R_v200': InsertStrOb('GalaxyProfiles/', '/MeanVR'),
                  'mean_v_phi_v200': InsertStrOb('GalaxyProfiles/', '/MeanVphi'),
                  'mean_v_phi_vtot12': InsertStrOb('GalaxyProfiles/', '/MeanVphi'),
                  'median_v_phi': InsertStrOb('GalaxyProfiles/', '/MedianVphi'),
                  'sigma_z': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'sigma_z_dm_sigma_z': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'DMsigma_z': InsertStrOb('GalaxyProfiles/', '/DMsigmaz'),
                  'DMsigma_tot': InsertStrOb('GalaxyProfiles/', '/DMsigmaz'),
                  'sigma_phi_v200': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'sigma_R_v200': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  # 'sigma_z_v200': InsertStrOb('GalaxyProfiles/', '/DMsigmaz'),
                  'sigma_z_v200': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'sigma_R': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  'sigma_phi': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'sigma_tot': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'sigma_tot_v200': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'root_ek_v200': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'vsigma': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'lambda_r1bin': InsertStrOb('GalaxyProfiles/', '/sigmaphi'),
                  'Npart': InsertStrOb('GalaxyProfiles/', '/BinN'),
                  'dm_density': FakeOb('GalaxyProfiles/DM/BinMass'),
                  'ndm': FakeOb('GalaxyProfiles/DM/BinMass'),
                  'dt':FakeOb('GalaxyProfiles/Star/fjzjc'),
                  'quart_dt':FakeOb('GalaxyProfiles/Star/fjzjc'),
                  'quart_dt_disk':FakeOb('GalaxyProfiles/Star/fjzjc'),
                  'st':FakeOb('GalaxyProfiles/Star/fjzjc'),
                  'age':FakeOb('GalaxyProfiles/Star/PercentileStellarAge'),
                  'age90':FakeOb('GalaxyProfiles/Star/PercentileStellarAge'),
                  'age75':FakeOb('GalaxyProfiles/Star/PercentileStellarAge'),
                  'age50':FakeOb('GalaxyProfiles/Star/PercentileStellarAge'),
                  'age25':FakeOb('GalaxyProfiles/Star/PercentileStellarAge'),
                  'age10':FakeOb('GalaxyProfiles/Star/PercentileStellarAge'),
                  'sfr':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'sfr025':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'sfr05':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'sfr1':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'sfr2':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'ssfr':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'ssfr025':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'ssfr05':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'ssfr1':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'ssfr2':FakeOb('GalaxyProfiles/Star/StarFormationRates'),
                  'ek':InsertStrOb('GalaxyProfiles/', '/KineticEnergy'),
                  'pot':InsertStrOb('GalaxyProfiles/', '/PotentialEnergy'),
                  'ek_pot': InsertStrOb('GalaxyProfiles/', '/KineticEnergy'),
                  'DMek': InsertStrOb('GalaxyProfiles/', '/DMsigmaz'),
                  })

key1_dict = NoneDict()
key1_dict.update({'mtot':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mbary':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mstarmtot':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mstarmtotr':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mstarm200':FakeOb('GalaxyQuantities/M200_crit'),
                  'mstarm200r':FakeOb('GalaxyQuantities/M200_crit'),
                  'mgasmtot':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'mbarymtot':FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'mbhmstar':FakeOb('GalaxyProfiles/Star/BinMass'),
                  '02rs':FakeOb('GalaxyQuantities/R200_crit'),
                  'r12_r200': FakeOb('GalaxyQuantities/R200_crit'),
                  'r12_r200r': FakeOb('GalaxyQuantities/R200_crit'),
                  'mean_v_R_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'mean_v_phi_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'mean_v_phi_vtot12': FakeOb('GalaxyProfiles/Star/BinMass'),
                  'sigma_z_dm_sigma_z': FakeOb('GalaxyProfiles/DM/DMsigmaz'),
                  'sigma_z_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'sigma_phi_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'sigma_R_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'sigma_tot_v200': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  'DMsigma_tot': InsertStrOb('GalaxyProfiles/', '/DMsigmay'),
                  'root_ek_v200': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  'z12R12': FakeOb('GalaxyQuantities/StellarHalfMassProjRadius'),
                  # 'z12R34': FakeOb('GalaxyQuantities/Stellar3QuaterMassProjRadius'),
                  # 'z12R14': FakeOb('GalaxyQuantities/StellarQuaterMassProjRadius'),
                  'z12R34': FakeOb('GalaxyQuantities/Stellar3QuaterMassProjRaduis'),
                  'z12R14': FakeOb('GalaxyQuantities/StellarQuaterMassProjRaduis'),
                  'ca': InsertStrOb('GalaxyProfiles/', '/Axisa'),
                  'ba': InsertStrOb('GalaxyProfiles/', '/Axisa'),
                  'cb': InsertStrOb('GalaxyProfiles/', '/Axisb'),
                  'jzjtot': InsertStrOb('GalaxyProfiles/', '/Jtot'),
                  'jz': InsertStrOb('GalaxyProfiles/', '/BinMass'),
                  'jtot': InsertStrOb('GalaxyProfiles/', '/BinMass'),
                  'dm_density': FakeOb('GalaxyQuantities/StellarHalfMassRadius'),
                  'sigma_tot': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  'vsigma': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  'lambda_r1bin': InsertStrOb('GalaxyProfiles/', '/sigmaR'),
                  'ssfr':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'ssfr025':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'ssfr05':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'ssfr1':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'ssfr2':FakeOb('GalaxyProfiles/Star/BinMass'),
                  'ek_pot': InsertStrOb('GalaxyProfiles/', '/PotentialEnergy'),
                  'ek':InsertStrOb('GalaxyProfiles/', '/BinN'),
                  'DMek': InsertStrOb('GalaxyProfiles/', '/DMsigmay'),
                  })

key2_dict = NoneDict()
key2_dict.update({'mtot':FakeOb('GalaxyProfiles/DM/BinMass'),
                  'mstarmtot': FakeOb('GalaxyProfiles/DM/BinMass'),
                  'mstarmtotr': FakeOb('GalaxyProfiles/DM/BinMass'),
                  'mgasmtot': FakeOb('GalaxyProfiles/DM/BinMass'),
                  'mbarymtot':FakeOb('GalaxyProfiles/DM/BinMass'),
                  'mean_v_R_v200':FakeOb('GalaxyQuantities/R200_crit'),
                  'mean_v_phi_v200':FakeOb('GalaxyQuantities/R200_crit'),
                  'mean_v_phi_vtot12': FakeOb('GalaxyProfiles/Gas/BinMass'),
                  'sigma_z_v200':FakeOb('GalaxyQuantities/R200_crit'),
                  'sigma_phi_v200':FakeOb('GalaxyQuantities/R200_crit'),
                  'sigma_R_v200':FakeOb('GalaxyQuantities/R200_crit'),
                  'sigma_tot_v200': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'DMsigma_tot': InsertStrOb('GalaxyProfiles/', '/DMsigmax'),
                  'root_ek_v200': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'sigma_tot': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'vsigma': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'lambda_r1bin': InsertStrOb('GalaxyProfiles/', '/sigmaz'),
                  'DMek': InsertStrOb('GalaxyProfiles/', '/DMsigmax'),
                  })

key3_dict = NoneDict()
key3_dict.update({'sigma_tot_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'root_ek_v200': InsertStrOb('GalaxyProfiles/', '/MeanVphi'),
                  'mean_v_phi_vtot12': FakeOb('GalaxyProfiles/DM/BinMass'),
                  'vsigma': InsertStrOb('GalaxyProfiles/', '/MeanVphi'),
                  'lambda_r1bin': InsertStrOb('GalaxyProfiles/', '/MeanVphi'),
                  'DMek': FakeOb('Header/MDM'),
                  })

key4_dict = NoneDict()
key4_dict.update({'sigma_tot_v200':FakeOb('GalaxyQuantities/R200_crit'),
                  'root_ek_v200':FakeOb('GalaxyQuantities/M200_crit'),
                  'mean_v_phi_vtot12': FakeOb('GalaxyQuantities/StellarHalfMassRadius'),})

#xykey
transform_dict = {'gn':lambda y: y,
                  'sn':lambda y: y,
                  'mstar':lambda y: np.log10(y) + 10,
                  'mgas':lambda y: np.log10(y) + 10,
                  'mdm':lambda y: np.log10(y) + 10,
                  'mtot':lambda y0, y1, y2: np.log10(y0 + y1 + y2) + 10,
                  'mbary':lambda y0, y1: np.log10(y0 + y1) + 10,
                  'mstarmtot':lambda y0, y1, y2: np.log10(y0 / (y0 + y1 + y2)),
                  'mstarmtotr':lambda y0, y1, y2: y0 / (y0 + y1 + y2),
                  'mstarm200':lambda y0, y1: np.log10(y0 / y1),
                  'mgasmtot':lambda y0, y1, y2: np.log10(y0 / (y0 + y1 + y2)),
                  'mbarymtot':lambda y0, y1, y2: np.log10((y0 + y1) / (y0 + y1 + y2)),
                  'm200':lambda y: np.log10(y) + 10,
                  'mbh':lambda y: np.log10(y) + 10 + np.log10(y > 2 * 1e-5 / LITTLE_H),
                  'mbhmstar':lambda x, y: np.log10(x / y),
                  '02rs': lambda x, y: np.log10(0.2 * y / concentration_ludlow(0, x)), #TODO impliment redshift properly / don't hard code
                  'r200':lambda y: np.log10(y),
                  'r12': lambda x: np.log10(x),
                  'r12r': lambda x: np.log10(x),
                  'r12_r200': lambda x, y: np.log10(x / y),
                  'r12_r200r': lambda x, y: np.log10(x / y),
                  'R12': lambda x: np.log10(x),
                  'R12gas': lambda x: np.log10(x),
                  'z12': lambda x: np.log10(x) + np.log10(2*(x > 1e-6) -1),
                  'z12R12': lambda x0, x1: np.log10(x0/x1) + np.log10(2*(x0 > 1e-6) -1),
                  'z12R34': lambda x0, x1: np.log10(x0/x1) + np.log10(2*(x0 > 1e-6) -1),
                  'z12R14': lambda x0, x1: np.log10(x0/x1) + np.log10(2*(x0 > 1e-6) -1),
                  'ca': lambda x, y: np.log10(x/y), #np.log10(x/xdenom) + np.log10(2*(x/xdenom > 1e-6) -1),
                  'ba': lambda x, y: x/y, #np.log10(x/xdenom) + np.log10(2*(x/xdenom > 1e-6) -1),
                  'cb': lambda x, y: x/y, #np.log10(x/xdenom) + np.log10(2*(x/xdenom > 1e-6) -1),
                  'Jz': lambda x: np.log10(x),
                  'Jtot': lambda x: np.log10(x),
                  'jzjtot': lambda z, tot: z / tot, #np.arccos(z / tot),
                  'jz': lambda x, y: np.log10(x / y),
                  'jtot': lambda x, y: np.log10(x / y),
                  'kappa_rot': lambda x: x, #np.log10(x) #+ np.log10(2*(x > 1e-6) -1),
                  'quart_kappa_rot': lambda x: x,
                  # 'kappa_co': lambda x: np.log10(x) + np.log10(2*(x > 1e-6) -1),
                  'kappa_co': lambda x: x,
                  'kappa_co_disk': lambda x: np.log10(x) + np.log10(2*(x > 1e-6) -1),
                  'quart_kappa_co': lambda x: x,
                  'quart_kappa_co_disk': lambda x: x,
                  'mean_v_R_v_circ': lambda x, y: np.log10(x / y), #+ np.log10(2*(x > 1e0) -1),
                  'mean_v_phi_v_circ': lambda x, y: np.log10(x / y), #+ np.log10(2*(x > 1e0) -1),
                  'v_circ': lambda x: np.log10(x), #+ np.log10(2*(x > 1e0) -1),
                  'DMv_circ': lambda x: np.log10(x), #+ np.log10(2*(x > 1e0) -1),
                  'mean_v_R': lambda x: np.log10(x), #+ np.log10(2*(x > 1e0) -1),
                  'mean_v_phi': lambda x: np.log10(x), #+ np.log10(2*(x > 1e0) -1),
                  'median_v_phi': lambda x: np.log10(x), #+ np.log10(2*(x > 100) -1),
                  'sigma_z': lambda x: np.log10(x), #+ np.log10(2*(x > 1e-6) -1),
                  'DMsigma_z': lambda x: np.log10(x), #+ np.log10(2*(x > 1e-6) -1),
                  'DMsigma_tot': lambda x, y, z: np.log10(np.sqrt(x**2 + y**2 + z**2)),
                  'sigma_z_dm_sigma_z': lambda x, y: np.log10(x / y),
                  'mean_v_R_v200': lambda s, m, r: np.log10(s / np.sqrt((GRAV_CONST * m / r))), #s / np.sqrt((GRAV_CONST * m / r)),
                  'mean_v_phi_v200': lambda s, m, r: np.log10(s / np.sqrt((GRAV_CONST * m / r))), #s / np.sqrt((GRAV_CONST * m / r)),
                  'mean_v_phi_vtot12': lambda v, mdm, mstar, mgas, r: np.log10(v / np.sqrt((GRAV_CONST * (mdm + mstar + mgas) / r))), #+ np.log10(2*(s > 1e-3) -1),
                  'sigma_z_v200': lambda s, m, r: np.log10(s / np.sqrt((GRAV_CONST * m / r))),
                  'sigma_phi_v200': lambda s, m, r: np.log10(s / np.sqrt((GRAV_CONST * m / r))),
                  'sigma_R_v200': lambda s, m, r: np.log10(s / np.sqrt((GRAV_CONST * m / r))),
                  'sigma_tot_v200': lambda x0, x1, x2, m, r: np.log10(np.sqrt(x0**2 + x1**2 + x2**2) / np.sqrt((GRAV_CONST * m / r))),
                  # 'sigma_tot_v200': lambda x0, x1, x2, m, r: np.log10(np.sqrt(x0**2 + x1**2 + x2**2
                  #   ) / np.sqrt((GRAV_CONST * m / (m / (200 * (3 * 0.06777**2 / (8 * np.pi * GRAV_CONST)) * 4/3 * np.pi))**(1/3)))),
                  'root_ek_v200': lambda x0, x1, x2, x3, m: np.log10(np.sqrt(x0**2 + x1**2 + x2**2 + x3**2
                    ) / np.sqrt((GRAV_CONST * m / (m / (200 * (3 * 0.06777**2 / (8 * np.pi * GRAV_CONST)) * 4/3 * np.pi))**(1/3)))),
                  'sigma_R': lambda x: np.log10(x), #+ np.log10(2*(x > 1e-6) -1),
                  'sigma_phi': lambda x: np.log10(x), #+ np.log10(2*(x > 1e-6) -1),
                  'sigma_tot': lambda x0, x1, x2: np.log10(np.sqrt(x0**2 + x1**2 + x2**2)),
                  # 'vsigma': lambda x0, x1, x2, x3: x3 / np.sqrt((x0**2 + x1**2 + x2**2)/3),
                  'vsigma': lambda x0, x1, x2, x3: np.log10(x3 / np.sqrt((x0**2 + x1**2 + x2**2)/3)),
                  'lambda_r1bin': lambda x0, x1, x2, x3: x3 / np.sqrt(x3**2 + (x0**2 + x1**2 + x2**2)/3),
                  'Npart': lambda x: x,
                  # 'dm_density': lambda x, xdenom: np.log10(x / (4/3 * np.pi * xdenom**3)),
                  'dm_density': lambda x, xdenom: np.log10(x / (4/3 * np.pi * ((xdenom * 10**0.1)**3 - (xdenom * 10**-0.1)**3))),
                  'ndm': lambda x: x,
                  'mstarm200r': lambda x, y: np.log10(x / y),
                  'mstarr': lambda y,: np.log10(y) + 10,
                  'mgasr': lambda y: np.log10(y) + 10,
                  'mdmr':lambda y: np.log10(y) + 10,
                  'dt':lambda x: x[:,n_jzjc_bins * 17//20],
                  'quart_dt':lambda x: x[:,n_jzjc_bins * 17//20],
                  'quart_dt_disk':lambda x: x[:,n_jzjc_bins * 17//20],
                  # 'st':lambda x: 2*(1 - x[:,n_jzjc_bins//2]),
                  'st':lambda x: 2*(1 - x[:,n_jzjc_bins//2]),
                  'age':lambda x: x[:,2],
                  'age90':lambda x: x[:,0],
                  'age75':lambda x: x[:,1],
                  'age50':lambda x: x[:,2],
                  'age25':lambda x: x[:,3],
                  'age10':lambda x: x[:,4],
                  'sfr':lambda x: np.log10(x[:,2]),
                  'sfr025':lambda x: np.log10(x[:,0]),
                  'sfr05':lambda x: np.log10(x[:,1]),
                  'sfr1':lambda x: np.log10(x[:,2]),
                  'sfr2':lambda x: np.log10(x[:,3]),
                  'ssfr':lambda x, y: np.log10(x[:,2] / y),
                  'ssfr025':lambda x, y: np.log10(x[:,0] / y),
                  'ssfr05':lambda x, y: np.log10(x[:,1] / y),
                  'ssfr1':lambda x, y: np.log10(x[:,2] / y),
                  'ssfr2':lambda x, y: np.log10(x[:,3] / y),
                  'ek': lambda x, N: np.log10(x / N) + 10,
                  'pot': lambda x: np.log10(-x) + 10,
                  'ek_pot': lambda x, y: np.log10(- x / y),
                  'DMek': lambda x, y, z, m: np.log10(m * (x**2 + y**2 + z**2)) + 10,
                  None: lambda x: x
                  }

#TODO add radii to labels...
label_dict = {'gn':'Group Number',
              'sn':'Subgroup Number',
              'mstar':r'$\log \, M_\star / $M$_\odot$',
              'mgas':r'$\log \, M_{\rm gas} / $M$_\odot$',
              'mdm':r'$\log \, M_{\rm DM} / $M$_\odot$',
              'mtot':r'$\log \, M_{\rm halo} / $M$_\odot$',
              'm200':r'$\log \, M_{200} / $M$_\odot$',
              'mbary':r'$\log \, M_{\rm bary} / $M$_\odot$',
              'mstarmtot':r'$\log \, M_\star / M_{\rm halo} $',
              'mstarmtotr':r'$M_\star / M_{\rm total} $',
              'mstarm200':r'$\log \, M_\star / M_{200} $',
              'mgasmtot':r'$\log \, M_{\rm gas} / M_{\rm halo} $',
              'mbarymtot':r'$\log \, M_{\rm bary} / M_{\rm halo} $',
              'mbh':r'$\log \, M_{\rm BH} / $M$_\odot$',
              'mbhmstar':r'$\log \, M_{\rm BH} / M_\star$',
              '02rs':r'$\log \, 0.2 r_s(M_{200}, z)$ / kpc',
              'r200': r'$\log \, r_{200} / $kpc',
              'r12': r'$\log \, r_{1/2, \star} / $kpc',
              'r12r': r'$\log \, r_{1/2, \star} / $kpc',
              'r12_r200': r'$\log \, r_{1/2, \star} / r_{200}$',
              'r12_r200r': r'$\log \, r_{1/2, \star} / r_{200}$',
              'R12': r'$\log \, R_{1/2, \star} / $kpc',
              'R12gas': r'$\log \, R_{1/2, \rm gas} / $kpc',
              'z12': r'$\log \, z_{1/2, \star} / $kpc',
              'z12R12': r'$\log \, z_{1/2, \star} / R_{1/2, \star}$',
              'z12R34': r'$\log \, z_{1/2, \star} / R_{3/4, \star}$',
              'z12R14': r'$\log \, z_{1/2, \star} / R_{1/4, \star}$',
              'ca': r'$\log \, c/a$', #r'$\log \, c/a_\star$',
              'ba': r'$b/a$', #r'$\log \, b/a_\star$',
              'cb': r'$c/b$', #r'$\log \, b/a_\star$',
              'Jz': r'$\log \, J_z / 10^{10}$M$_{\odot}$ kpc km s$^{-1}$',
              'Jtot': r'$\log \, |\vec{J}| / 10^{10}$M$_{\odot}$ kpc km s$^{-1}$',
              'jzjtot':r'$j_z / j_{\rm tot}$',
              'jz': r'$\log \, j_z / $ kpc km s$^{-1}$',
              'cum_jz': r'$\log \, j_z (<r) / $ kpc km s$^{-1}$',
              'cum_Jz': r'$\log \, J_z (<r) / 10^{10}$M$_{\odot}$ kpc km s$^{-1}$',
              'jtot': r'$\log \, |\vec{j}| / $ kpc km s$^{-1}$',
              'kappa_rot': r'$\kappa_{\rm rot}$', #r'$\log \, \kappa_{\rm rot}$',
              'quart_kappa_rot': r'$F(\kappa_{\rm rot} > X)$',
              # 'kappa_co': r'$\log \, \kappa_{\rm co}$',
              'kappa_co': r'$\kappa_{\rm co}$',
              'kappa_co_disk': r'$\log \, \kappa_{\rm co} | \kappa_{\rm co} > 0.4$',
              'quart_kappa_co': r'$F(\kappa_{\rm co} > X)$',
              'quart_kappa_co_disk': r'$F(\kappa_{\rm co} > X | \kappa_{\rm co} > 0.4)$',
              'mean_v_R_v_circ': r'$\log \, \overline{v}_R / V_c$',
              'mean_v_phi_v_circ': r'$\log \, \overline{v}_\phi / V_c$',
              'v_circ': r'$\log \, V_c /$km s$^{-1}$',
              'DMv_circ': r'$\log \, V_c /$km s$^{-1}$',
              'mean_v_R': r'$\log \, \overline{v}_R /$km s$^{-1}$',
              'mean_v_phi': r'$\log \, \overline{v}_\phi /$km s$^{-1}$',
              'median_v_phi': r'$\log \, $median$(v_\phi) /$km s$^{-1}$',
              'sigma_z': r'$\log \, \sigma_z /$km s$^{-1}$',
              'DMsigma_z': r'$\log \, \sigma_z /$km s$^{-1}$',
              'DMsigma_tot': r'$\log \, \sigma_{\rm tot} /$km s$^{-1}$',
              'sigma_z_dm_sigma_z': r'$\log \, \sigma_{z, \star} / \sigma_{z, \rm DM} $',
              'mean_v_R_v200': r'$\log \, \overline{v}_R / V_{200} $',
              'mean_v_phi_v200': r'$\log \, \overline{v}_\phi / V_{200} $',
              'mean_v_phi_vtot12': r'$\log \, \overline{v}_\phi / \sqrt{G M / r}$',
              'sigma_z_v200': r'$\log \, \sigma_z / V_{200} $',
              'sigma_phi_v200': r'$\log \, \sigma_\phi / V_{200} $',
              'sigma_R_v200': r'$\log \, \sigma_R / V_{200} $',
              'sigma_tot_v200': r'$\log \, \sigma_{\rm tot} / V_{200} $',
              'root_ek_v200': r'$\log \, \sqrt{2 e_k} / V_{200} $',
              'sigma_R': r'$\log \, \sigma_R /$km s$^{-1}$',
              'sigma_phi': r'$\log \, \sigma_\phi /$km s$^{-1}$',
              'sigma_tot': r'$\log \, \sigma_{\rm tot} /$km s$^{-1}$',
              'vsigma': r'$\log \, \overline{v}_\phi / \sigma_{\rm 1D} $',
              'lambda_r1bin': r'$\lambda_r $ (only using 1 radial bin)',
              'Npart': r'$\log \,$N$_\star$',
              'dm_density': r'$\log \, $N$_{\rm dm} (< r_{1/2}) / V(r_{1/2})$ [1/kpc$^3$]',
              'ndm': r'$\log \, $N$_{\rm dm} (< ?)$',
              'mstarm200r':r'$\log \, M_\star / M_{200}$ [$r ?$kpc]',
              'mstarr':r'$\log \, M_\star / $M$_\odot$ [$r ?$kpc]',
              'cum_mstarr':r'$\log \, M_\star (<r) / $M$_\odot$ [$r ?$kpc]',
              'mgasr':r'$\log \, M_{\rm gas} / $M$_\odot$ [$r ?$kpc]',
              'mdmr':r'$\log \, M_{\rm DM} / $M$_\odot$ [$r ?$kpc]',
              'dt':r'D/T $[F(j_z / j_c > 0.7)]$',
              'quart_dt':r'F(D/T $[F(j_z / j_c > 0.7)] > X)$',
              'quart_dt_disk':r'F(D/T $[F(j_z / j_c > 0.7)] > X | \kappa_{\rm co} > 0.4)$',
              # 'st':r'1 - S/T',
              'st':r'S/T',
              'age':r'median stellar formation age [Gyr]',
              'age90':r'$t_{90}$ [Gyr]',
              'age75':r'$t_{75}$ [Gyr]',
              'age50':r'$t_{50}$ [Gyr]',
              'age25':r'$t_{25}$ [Gyr]',
              'age10':r'$t_{10}$ [Gyr]',
              'sfr':r'log SFR / M$_\odot$ Gyr $^{-1}$ (1 Gyr)',
              'sfr025':r'log SFR / M$_\odot$ Gyr$^{-1}$ (0.25 Gyr)',
              'sfr05':r'log SFR / M$_\odot$ Gyr$^{-1}$ (0.5 Gyr)',
              'sfr1':r'log SFR / M$_\odot$ Gyr$^{-1}$ (1 Gyr)',
              'sfr2':r'log SFR / M$_\odot$ Gyr$^{-1}$ (2 Gyr)',
              'ssfr':r'log sSFR / Gyr$^{-1}$ (1 Gyr)',
              'ssfr025':r'log sSFR / Gyr$^{-1}$ (0.25 Gyr)',
              'ssfr05':r'log sSFR / Gyr$^{-1}$ (0.5 Gyr)',
              'ssfr1':r'log sSFR / Gyr$^{-1}$ (1 Gyr)',
              'ssfr2':r'log sSFR / Gyr$^{-1}$ (2 Gyr)',
              'ek': r'log Kinetic Energy / M$_\odot$ km  s$^{-2}$',
              'cum_ek': r'log cumulative Kinetic Energy / M$_\odot$ km  s$^{-2}$',
              'pot': r'log - Potential Energy / M$_\odot$ km s$^{-2}$',
              'ek_pot': r'log Kinetic Energy / Potential Energy',
              'DMek': r'log Kinetic Energy / M$_\odot$ km s$^{-2}$',
              }
lims_dict = {'gn':[1, 1_000_000],
             'sn':[0, 1000],
             'mstar':[8,12], #[7,13],
             'mgas':[7,13],
             'mdm':[10,14],
             'mtot':[10,14],
             'm200':[10, 14], #[8,14],
             'mbary':[7,13],
             'mstarmtot':[-5.5,0.5],
             'mstarmtotr':[-0.1, 1.1],
             'mstarm200':[-4,-1],#[-5.5,0.5],
             'mgasmtot':[-5.5,0.5],
             'mbarymtot':[-5.5,0.5],
             'mbh':[5.5, 9],
             'mbhmstar':[-5, 1],
             '02rs': [-1, 2],
             'r200': [0.5, 3.5],
             'r12': [-0.5, 1.5],
             'r12r': [-1, 2],
             'r12_r200': [-2.1, -0.6],
             'r12_r200r': [-2.6, -0.6],
             'R12': [-0.5, 1.5],
             'R12gas': [-0.5, 2.5],
             'z12': [-0.9, 1.1],
             'z12R12': [-1.25, 0],
             'z12R34': [-1.5, 0.5],
             'z12R14': [-1.5, 0.5],
             'ca': [-1, 0], #[0,1], #[-1.8, 0.2],
             'ba':[0,1], #[-1.8, 0.2],
             'cb':[0,1], #[-1.8, 0.2],
             'Jz': [-4, 2], #[-0.5,5.5], #[-1.8, 0.2],
             'Jtot':[-0.5,5.5], #[-1.8, 0.2],
             'jzjtot':[-1.1,1.1],
             'jz':[0,4], #[-1.8, 0.2],
             'cum_jz': [-1.5,4.5],
             'cum_Jz': [-4, 2],#[-1.5,4.5],
             'jtot':[0,4], #[-1.8, 0.2],
             'kappa_rot': [0, 1], #[-0.8, 0],
             'quart_kappa_rot': [0, 1],
             # 'kappa_co': [-1.2, 0],
             'kappa_co': [0, 1],
             # 'kappa_co_disk': [-0.4, 0],
             'kappa_co_disk': [-1.2, 0],
             'quart_kappa_co': [0, 1],
             'quart_kappa_co_disk': [0, 1],
             'mean_v_R_v_circ': [-2, 0], #[0.5, 3.5], #[-0.5, 2.5],
             'mean_v_phi_v_circ': [-2, 0], #[0.5, 3.5], #[-0.5, 2.5],
             'v_circ': [-0.5, 2.5], #[0.5, 3.5], #[-0.5, 2.5],
             'DMv_circ':[0, 3],#[-0.5, 2.5],
             'mean_v_R': [-0.5, 2.5],
             'mean_v_phi': [-0.5, 2.5],
             'median_v_phi': [-0.5, 2.5],
             # 'sigma_z': [-0.5, 2.5],
             'sigma_z': [1, 2.5],
             'DMsigma_z': [1, 2.5],
             'DMsigma_tot': [1, 2.5],
             'sigma_z_dm_sigma_z':[-1,0.5],
             'mean_v_R_v200': [-2, 1], #[-1, 1], #[-2, 1], #[-2, 0],
             'mean_v_phi_v200': [-2, 0.5], #[-1, 1], #[-2, 1], #[-2, 0],
             'mean_v_phi_vtot12': [-2,0.5],
             'sigma_z_v200': [-1, 0.5],
             'sigma_R_v200': [-1, 0.5],
             'sigma_phi_v200': [-1, 0.5],
             'sigma_tot_v200': [-0.6, 0.4],  #[-1, 0.5],
             'root_ek_v200': [-0.7, 0.3], #[-1, 0.5],
             'sigma_R': [1,2.5],#[-0.5, 2.5],
             'sigma_phi': [1,2.5], #[-0.5, 2.5],
             'sigma_tot': [1.3, 2.5], #[0, 3],
             'vsigma': [-2, 1], #[0, 2],
             'lambda_r1bin': [0, 1], #[-2,0],
             'Npart': [0, 6],
             'dm_density': [-5,5],
             'ndm': [0,6],
             'mstarm200r':[-5, -2],
             'mstarr': [5,9], #[6,12],
             'cum_mstarr':[6,12],
             'mgasr':[6,12],
             'mdmr':[6,12],
             'dt':[0,1],
             'quart_dt':[0,1],
             'quart_dt_disk':[0,1],
             # 'st':[0,1.25],
             'st':[0,1],
             'age':[0+1e-3,UNIVERSE_AGE],
             'age90':[0+1e-3,UNIVERSE_AGE],
             'age75':[0+1e-3,UNIVERSE_AGE],
             'age50':[0+1e-3,UNIVERSE_AGE],
             'age25':[0+1e-3,UNIVERSE_AGE],
             'age10':[0+1e-3,UNIVERSE_AGE],
             'sfr': [-4, 2],
             'sfr025': [-4, 2],
             'sfr05': [-4, 2],
             'sfr1': [-4, 2],
             'sfr2': [-4, 2],
             'ssfr': [-3, 0],
             'ssfr025': [-4, 2],
             'ssfr05': [-4, 2],
             'ssfr1': [-4, 2],
             'ssfr2': [-4, 2],
             'ek': [8, 12],
             'cum_ek': [8, 12],
             'pot': [8, 12],
             'ek_pot': [-2, 2],
             'DMek': [8, 12],
             'cum_DMek': [8, 12],
             None:[None, None]
             }

filename_dict = FakeReturn()

#xykey and rkey
column0_dict = NoneDict()
column0_dict.update({'gn':FakeOb(None),
                     'sn':FakeOb(None),
                     'mstar':FakeOb(lt_r200_c),
                     'mgas':FakeOb(lt_r200_c),
                     'mdm':FakeOb(lt_r200_c),
                     'mtot':FakeOb(lt_r200_c),
                     'mbary':FakeOb(lt_r200_c),
                     'mstarmtot':FakeOb(lt_r200_c),
                     'mstarmtotr':rkey_to_rcolum_dict,
                     'mstarm200':FakeOb(lt_r200_c),
                     'mgasmtot':FakeOb(lt_r200_c),
                     'mbarymtot':FakeOb(lt_r200_c),
                     'm200':FakeOb(None),
                     'mbh':FakeOb(None),
                     'mbhmstar':FakeOb(None),
                     '02rs': FakeOb(None),
                     'r200': FakeOb(None),
                     'r12': FakeOb(None),
                     'r12r': rkey_to_rcolum_dict,
                     'r12_r200': FakeOb(None),
                     'r12_r200r': rkey_to_rcolum_dict,
                     'R12': FakeOb(None),
                     'R12gas': FakeOb(None),
                     'z12': rkey_to_rcolum_dict,
                     'z12R12': FakeOb(lt_r200_c),
                     'z12R34': FakeOb(lt_r200_c),
                     'z12R14': FakeOb(lt_r200_c),
                     'ca': rkey_to_rcolum_dict,
                     'ba': rkey_to_rcolum_dict,
                     'cb': rkey_to_rcolum_dict,
                     'Jz': rkey_to_rcolum_dict,
                     'Jtot': rkey_to_rcolum_dict,
                     'jzjtot': rkey_to_rcolum_dict,
                     'jz': rkey_to_rcolum_dict,
                     'jtot': rkey_to_rcolum_dict,
                     'kappa_rot': rkey_to_rcolum_dict,
                     'quart_kappa_rot': rkey_to_rcolum_dict,
                     'kappa_co': rkey_to_rcolum_dict,
                     'kappa_co_disk': rkey_to_rcolum_dict,
                     'quart_kappa_co': rkey_to_rcolum_dict,
                     'quart_kappa_co_disk': rkey_to_rcolum_dict,
                     'mean_v_R_v_circ': rkey_to_rcolum_dict,
                     'mean_v_phi_v_circ': rkey_to_rcolum_dict,
                     'v_circ': rkey_to_rcolum_dict,
                     'DMv_circ': rkey_to_rcolum_dict,
                     'mean_v_R': rkey_to_rcolum_dict,
                     'mean_v_phi': rkey_to_rcolum_dict,
                     'median_v_phi': rkey_to_rcolum_dict,
                     'sigma_z': rkey_to_rcolum_dict,
                     'sigma_z_dm_sigma_z': rkey_to_rcolum_dict,
                     'DMsigma_z': rkey_to_rcolum_dict,
                     'mean_v_R_v200': rkey_to_rcolum_dict,
                     'mean_v_phi_v200': rkey_to_rcolum_dict,
                     'mean_v_phi_vtot12':rkey_to_rcolum_dict,
                     'sigma_z_v200': rkey_to_rcolum_dict,
                     'sigma_phi_v200': rkey_to_rcolum_dict,
                     'sigma_R_v200': rkey_to_rcolum_dict,
                     'sigma_tot_v200': rkey_to_rcolum_dict,
                     'root_ek_v200': rkey_to_rcolum_dict,
                     'sigma_R': rkey_to_rcolum_dict,
                     'sigma_phi':rkey_to_rcolum_dict,
                     'sigma_tot':rkey_to_rcolum_dict,
                     'DMsigma_tot':rkey_to_rcolum_dict,
                     'vsigma':rkey_to_rcolum_dict,
                     'lambda_r1bin':rkey_to_rcolum_dict,
                     'Npart':rkey_to_rcolum_dict,
                     'dm_density':FakeOb(lt_r12_c),
                     'ndm':rkey_to_rcolum_dict,
                     'mstarm200r':rkey_to_rcolum_dict,
                     'mstarr':rkey_to_rcolum_dict,
                     'mgasr':rkey_to_rcolum_dict,
                     'mdmr':rkey_to_rcolum_dict,
                     'dt':rkey_to_rcolum_dict,
                     'quart_dt':rkey_to_rcolum_dict,
                     'quart_dt_disk':rkey_to_rcolum_dict,
                     'st':rkey_to_rcolum_dict,
                     'age':rkey_to_rcolum_dict,
                     'age90':rkey_to_rcolum_dict,
                     'age75':rkey_to_rcolum_dict,
                     'age50':rkey_to_rcolum_dict,
                     'age25':rkey_to_rcolum_dict,
                     'age10':rkey_to_rcolum_dict,
                     'sfr':rkey_to_rcolum_dict,
                     'sfr025':rkey_to_rcolum_dict,
                     'sfr05':rkey_to_rcolum_dict,
                     'sfr1':rkey_to_rcolum_dict,
                     'sfr2':rkey_to_rcolum_dict,
                     'ssfr':rkey_to_rcolum_dict,
                     'ssfr025':rkey_to_rcolum_dict,
                     'ssfr05':rkey_to_rcolum_dict,
                     'ssfr1':rkey_to_rcolum_dict,
                     'ssfr2':rkey_to_rcolum_dict,
                     'ek': rkey_to_rcolum_dict,
                     'pot': rkey_to_rcolum_dict,
                     'ek_pot': rkey_to_rcolum_dict,
                     'DMek': rkey_to_rcolum_dict,
                     })

column1_dict = NoneDict()
column1_dict.update({'mdm':FakeOb(0),
                     'mstarmtot':FakeOb(lt_r200_c),
                     'mstarmtotr':rkey_to_rcolum_dict,
                     'mstarm200': FakeOb(None),
                     'mgasmtot':FakeOb(lt_r200_c),
                     'mbarymtot':FakeOb(lt_r200_c),
                     'mtot':FakeOb(lt_r200_c),
                     'mbary':FakeOb(lt_r200_c),
                     'mbhmstar':rkey_to_rcolum_dict,
                     'mstarm200r': FakeOb(None),
                     '02rs': FakeOb(None),
                     'r12_r200': FakeOb(None),
                     'r12_r200r': FakeOb(None),
                     'ca': rkey_to_rcolum_dict,
                     'ba': rkey_to_rcolum_dict,
                     'cb': rkey_to_rcolum_dict,
                     'jzjtot': rkey_to_rcolum_dict,
                     'jz': rkey_to_rcolum_dict,
                     'jtot': rkey_to_rcolum_dict,
                     'sigma_tot': rkey_to_rcolum_dict,
                     'DMsigma_tot': rkey_to_rcolum_dict,
                     'mean_v_phi_v200': FakeOb(None),
                     'mean_v_phi_vtot12':rkey_to_rcolum_dict,
                     'mean_v_R_v_circ': rkey_to_rcolum_dict,
                     'mean_v_phi_v_circ': rkey_to_rcolum_dict,
                     'sigma_z_dm_sigma_z': FakeOb(eq_02rs_c), #rkey_to_rcolum_dict,
                     'sigma_z_v200': FakeOb(None),
                     'sigma_phi_v200': FakeOb(None),
                     'sigma_R_v200': FakeOb(None),
                     'sigma_tot_v200': rkey_to_rcolum_dict,
                     'root_ek_v200': rkey_to_rcolum_dict,
                     'vsigma': rkey_to_rcolum_dict,
                     'lambda_r1bin': rkey_to_rcolum_dict,
                     # 'mdmr':FakeOb(0),
                     'ssfr':rkey_to_rcolum_dict,
                     'ssfr025':rkey_to_rcolum_dict,
                     'ssfr05':rkey_to_rcolum_dict,
                     'ssfr1':rkey_to_rcolum_dict,
                     'ssfr2':rkey_to_rcolum_dict,
                     'ek_pot': rkey_to_rcolum_dict,
                     'ek': rkey_to_rcolum_dict,
                     'DMek': rkey_to_rcolum_dict,
                     })

column2_dict = NoneDict()
column2_dict.update({'mstarmtot':FakeOb(lt_r200_c),
                     'mstarmtotr':rkey_to_rcolum_dict,
                     'mgasmtot':FakeOb(lt_r200_c),
                     'mbarymtot':FakeOb(lt_r200_c),
                     'mtot':FakeOb(lt_r200_c),
                     'sigma_tot': rkey_to_rcolum_dict,
                     'DMsigma_tot': rkey_to_rcolum_dict,
                     'mean_v_phi_v200': FakeOb(None),
                     'mean_v_phi_vtot12':rkey_to_rcolum_dict,
                     'sigma_z_v200': FakeOb(None),
                     'sigma_phi_v200': FakeOb(None),
                     'sigma_R_v200': FakeOb(None),
                     'sigma_tot_v200': rkey_to_rcolum_dict,
                     'root_ek_v200': rkey_to_rcolum_dict,
                     'vsigma': rkey_to_rcolum_dict,
                     'lambda_r1bin': rkey_to_rcolum_dict,
                     'DMek': rkey_to_rcolum_dict,
                     })

column3_dict = NoneDict()
column3_dict.update({#'mstarmtot':FakeOb(0),
                     #'mstarmtotr':rkey_to_rcolum_dict,
                     'mgasmtot':FakeOb(0),
                     'mbarymtot':FakeOb(0),
                     'mtot':FakeOb(0),
                     'vsigma': rkey_to_rcolum_dict,
                     'mean_v_phi_vtot12':rkey_to_rcolum_dict,
                     'lambda_r1bin': rkey_to_rcolum_dict,
                     'sigma_tot_v200': FakeOb(None),
                     'root_ek_v200': rkey_to_rcolum_dict,
                     'DMek': FakeOb(None),
                     })

column4_dict = NoneDict()
column4_dict.update({'sigma_tot_v200': FakeOb(None),
                     'root_ek_v200': FakeOb(None),
                     'mean_v_phi_vtot12': FakeOb(None),})

#need to include the minimum and maximum value
quartile_dict = FalseDict()
quartile_dict.update({'quart_kappa_rot': [0, 1/3, 0.5, 0.7, 1],
                      'quart_kappa_co': [0, 0.25, 0.35, 0.5, 0.65, 1],
                      # 'quart_kappa_co': [0, 1/6, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                      'quart_kappa_co_disk': [0, 1/6, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                      'quart_dt': [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                      'quart_dt_disk': [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1],
                      })

mask_dict = NoneDict()
mask_dict.update({'kappa_co_disk': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  'quart_kappa_co_disk': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  'quart_dt_disk': InsertStrOb('GalaxyProfiles/', '/kappaCo'),
                  })
mask_column_dict = NoneDict()
mask_column_dict.update({'kappa_co_disk': rkey_to_rcolum_dict,
                         'quart_kappa_co_disk': rkey_to_rcolum_dict,
                         'quart_dt_disk': rkey_to_rcolum_dict,
                         })
mask_func_dict = FalseDict()
mask_func_dict.update({'kappa_co_disk': lambda kco: kco > 0.4,
                       'quart_kappa_co_disk': lambda kco: kco > 0.4,
                       'quart_dt_disk': lambda kco: kco > 0.4,
                        })


if __name__ == '__main__':
    pass