#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

import h5py as h5

from scipy.integrate import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

import traceback as tb
import warnings

#matplotlib
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

import matplotlib.lines as lines
import matplotlib.patheffects as path_effects

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 12#20
rcParams['xtick.labelsize'] = 12#20
rcParams['ytick.labelsize'] = 12#20
rcParams['axes.labelsize'] = 12#22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 #/ 0.6777 #10^10 Msun / h

n_jzjc_bins = 21

NO_GROUP_NUMBER = 1073741824 #2**30

SCALE_A = 1
BOX_SIZE = 33.885 #50 * 0.6777 #kpc

VERBOSE = True

def get_defined_region(match_index, matched_groups_file, halo_data_location_7x, halo_data_location_1x, snap, nr200 = 1.5):

    #matches
    with h5.File(matched_groups_file, 'r') as matched_groups:
        group_7x = matched_groups['GroupBijectiveMatches/MatchedGroupNumber7x'][match_index]
        group_1x = matched_groups['GroupBijectiveMatches/MatchedGroupNumber1x'][match_index]

    # with h5.File(matched_subs_file, 'r') as matched_subs:
    print(group_7x)
    print(group_1x)

    #7x
    fn = halo_data_location_7x + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    NGrp_c = 0

    Group_R_Crit200_7x = 0
    GroupCentreOfPotential_7x = np.zeros(3)

    for ifile in range(Ntask):
        fn = halo_data_location_7x + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']

            if Ngroups > 0:

                NGrp_c += Ngroups

                if NGrp_c >= group_7x:
                    group_match_index_7x = group_7x - (NGrp_c - Ngroups) - 1

                    Group_R_Crit200_7x = fs["FOF/Group_R_Crit200"][group_match_index_7x]
                    GroupCentreOfPotential_7x = fs["FOF/GroupCentreOfPotential"][group_match_index_7x]

                    break

    print(GroupCentreOfPotential_7x)

    #1x
    fn = halo_data_location_1x + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    NGrp_c = 0

    Group_R_Crit200_1x = 0
    GroupCentreOfPotential_1x = np.zeros(3)

    for ifile in range(Ntask):
        fn = halo_data_location_1x + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']

            if Ngroups > 0:

                NGrp_c += Ngroups

                if NGrp_c >= group_1x:
                    group_match_index_1x = group_1x - (NGrp_c - Ngroups) - 1

                    Group_R_Crit200_1x = fs["FOF/Group_R_Crit200"][group_match_index_1x]
                    GroupCentreOfPotential_1x = fs["FOF/GroupCentreOfPotential"][group_match_index_1x]

                    break

    print(GroupCentreOfPotential_1x)

    proj_centre = 0.5 * (GroupCentreOfPotential_7x + GroupCentreOfPotential_1x)
    proj_extent = nr200 * 0.5 * (Group_R_Crit200_7x + Group_R_Crit200_1x)

    xlims = [proj_centre[0] - proj_extent, proj_centre[0] + proj_extent]
    ylims = [proj_centre[1] - proj_extent, proj_centre[1] + proj_extent]
    zlims = [proj_centre[2] - proj_extent, proj_centre[2] + proj_extent]

    extent = [xlims, ylims, zlims]

    print(extent)

    return extent, GroupCentreOfPotential_7x, GroupCentreOfPotential_1x

#TODO work out what to do with periodic boundaries...

def read_groups_defined_region(halo_data_location, snap, extent):

    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups // 100, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups // 100, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups // 100, 3), dtype=np.float32)

    # Subhalo arrays
    GroupNumber = np.zeros(TotNsubgroups // 100, dtype=np.int32)
    SubGroupNumber = np.zeros(TotNsubgroups // 100, dtype=np.int32)
    SubGroupCentreOfPotential = np.zeros((TotNsubgroups // 100, 3), dtype=np.float32)

    NGrp_c = 0
    NSub_c = 0

    for ifile in range(Ntask):
        fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:

                GroupCentreOfPotential_file = fs["FOF/GroupCentreOfPotential"][()]

                group_mask = np.logical_and(np.logical_and(np.logical_and(GroupCentreOfPotential_file[:, 0] > extent[0][0],
                                                                          GroupCentreOfPotential_file[:, 0] < extent[0][1]),
                                                           np.logical_and(GroupCentreOfPotential_file[:, 1] > extent[1][0],
                                                                          GroupCentreOfPotential_file[:, 1] < extent[1][1])),
                                                           np.logical_and(GroupCentreOfPotential_file[:, 2] > extent[2][0],
                                                                          GroupCentreOfPotential_file[:, 2] < extent[2][1]))

                N_groups_added = np.sum(group_mask)

                GroupCentreOfPotential[NGrp_c:NGrp_c + N_groups_added, :] = GroupCentreOfPotential_file[group_mask, :]

                Group_M_Crit200[NGrp_c:NGrp_c + N_groups_added] = fs["FOF/Group_M_Crit200"][group_mask]
                Group_R_Crit200[NGrp_c:NGrp_c + N_groups_added] = fs["FOF/Group_R_Crit200"][group_mask]

                NGrp_c += N_groups_added

            if Nsubgroups > 0:

                SubCentreOfPotential_file = fs["Subhalo/CentreOfPotential"][()]

                sub_mask = np.logical_and(np.logical_and(np.logical_and(SubCentreOfPotential_file[:, 0] > extent[0][0],
                                                                        SubCentreOfPotential_file[:, 0] < extent[0][1]),
                                                         np.logical_and(SubCentreOfPotential_file[:, 1] > extent[1][0],
                                                                        SubCentreOfPotential_file[:, 1] < extent[1][1])),
                                                         np.logical_and(SubCentreOfPotential_file[:, 2] > extent[2][0],
                                                                        SubCentreOfPotential_file[:, 2] < extent[2][1]))

                N_subs_added = np.sum(sub_mask)

                SubGroupCentreOfPotential[NSub_c:NSub_c + N_subs_added, :] = SubCentreOfPotential_file[sub_mask, :]

                GroupNumber[NSub_c:NSub_c + N_subs_added]    = fs["Subhalo/GroupNumber"][sub_mask]
                SubGroupNumber[NSub_c:NSub_c + N_subs_added] = fs["Subhalo/SubGroupNumber"][sub_mask]

                NSub_c += N_subs_added

    #remove zeros
    Group_M_Crit200 = Group_M_Crit200[: NGrp_c]
    Group_R_Crit200 = Group_R_Crit200[: NGrp_c]
    GroupCentreOfPotential = GroupCentreOfPotential[: NGrp_c]

    GroupNumber = GroupNumber[: NSub_c]
    SubGroupNumber = SubGroupNumber[: NSub_c]
    SubGroupCentreOfPotential = SubGroupCentreOfPotential[: NSub_c]

    print(NGrp_c)
    print(NSub_c)

    print('Loaded halos')

    return (Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
            GroupNumber, SubGroupNumber, SubGroupCentreOfPotential)


def read_particles_defined_region(particle_data_location, snap, extent):

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    PosGas = np.zeros((NumPartTot[0] // 100, 3), dtype=np.float32)
    MassGas = np.zeros(NumPartTot[0] // 100, dtype=np.float32)

    PosDM = np.zeros((NumPartTot[1] // 100, 3), dtype=np.float32)

    PosStar = np.zeros((NumPartTot[4] // 100, 3), dtype=np.float32)
    VelStar = np.zeros((NumPartTot[4] // 100, 3), dtype=np.float32)
    MassStar = np.zeros(NumPartTot[4] // 100, dtype=np.float32)

    NStar_c = 0
    NGas_c = 0
    NDM_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[4] > 0:
                PosStarFile = fs["PartType4/Coordinates"][()]
                VelStarFile = fs["PartType4/Velocity"][()]

                Star_extent_mask = np.logical_and(np.logical_and(np.logical_and(PosStarFile[:, 0] > extent[0][0],
                                                                                PosStarFile[:, 0] < extent[0][1]),
                                                                 np.logical_and(PosStarFile[:, 1] > extent[1][0],
                                                                                PosStarFile[:, 1] < extent[1][1])),
                                                                 np.logical_and(PosStarFile[:, 2] > extent[2][0],
                                                                                PosStarFile[:, 2] < extent[2][1]))

                N_star_added = np.sum(Star_extent_mask)

                PosStar[NStar_c:NStar_c + N_star_added, :] = PosStarFile[Star_extent_mask, :]
                VelStar[NStar_c:NStar_c + N_star_added, :] = VelStarFile[Star_extent_mask, :]
                MassStar[NStar_c:NStar_c + N_star_added]   = fs["PartType4/Mass"][Star_extent_mask]

                NStar_c += N_star_added

            if NumParts[0] > 0:
                PosGasFile = fs["PartType0/Coordinates"][()]

                Gas_extent_mask = np.logical_and(np.logical_and(np.logical_and(PosGasFile[:, 0] > extent[0][0],
                                                                               PosGasFile[:, 0] < extent[0][1]),
                                                                np.logical_and(PosGasFile[:, 1] > extent[1][0],
                                                                               PosGasFile[:, 1] < extent[1][1])),
                                                                np.logical_and(PosGasFile[:, 2] > extent[2][0],
                                                                               PosGasFile[:, 2] < extent[2][1]))

                N_Gas_added = np.sum(Gas_extent_mask)

                PosGas[NGas_c:NGas_c + N_Gas_added, :] = PosGasFile[Gas_extent_mask, :]
                MassGas[NGas_c:NGas_c + N_Gas_added]   = fs["PartType0/Mass"][Gas_extent_mask]

                NGas_c += N_Gas_added

            if NumParts[1] > 0:
                PosDMFile = fs["PartType1/Coordinates"][()]

                DM_extent_mask = np.logical_and(np.logical_and(np.logical_and(PosDMFile[:, 0] > extent[0][0],
                                                                              PosDMFile[:, 0] < extent[0][1]),
                                                               np.logical_and(PosDMFile[:, 1] > extent[1][0],
                                                                              PosDMFile[:, 1] < extent[1][1])),
                                                               np.logical_and(PosDMFile[:, 2] > extent[2][0],
                                                                              PosDMFile[:, 2] < extent[2][1]))

                N_DM_added = np.sum(DM_extent_mask)

                PosDM[NDM_c:NDM_c + N_DM_added, :] = PosDMFile[DM_extent_mask, :]

                NDM_c += N_DM_added

    #remove zeros
    PosGas = PosGas[:NGas_c, :]
    MassGas = MassGas[:NGas_c]

    PosDM = PosDM[:NDM_c, :]

    PosStar = PosStar[:NStar_c, :]
    VelStar = VelStar[:NStar_c, :]
    MassStar = MassStar[:NStar_c]

    print(NGas_c)
    print(NDM_c)
    print(NStar_c)

    print('loaded particles')

    return (PosGas, MassGas, PosDM, PosStar, VelStar, MassStar)


def make_matched_galaxy_image(extent, cop_7x, cop_1x,
                              Group_M_Crit200_7x, Group_R_Crit200_7x, GroupCentreOfPotential_7x,
                              GroupNumber_7x, SubGroupNumber_7x, SubGroupCentreOfPotential_7x,
                              Group_M_Crit200_1x, Group_R_Crit200_1x, GroupCentreOfPotential_1x,
                              GroupNumber_1x, SubGroupNumber_1x, SubGroupCentreOfPotential_1x,
                              PosGas_7x, MassGas_7x, PosDM_7x, PosStar_7x, VelStar_7x, MassStar_7x,
                              PosGas_1x, MassGas_1x, PosDM_1x, PosStar_1x, VelStar_1x, MassStar_1x,
                              group_focus=0, save=True, name=None, out_loc=None, nr200=1.5):

    figsize0 = (20, 20)
    figsize1 = (8, 5*8)

    gs = 4**2
    ss = 3**2

    hwg = (extent[0][1] - extent[0][0]) * 0.03
    hws = (extent[0][1] - extent[0][0]) * 0.02
    lwg = 4
    lws = 1

    g_arrow_kwargs = {'shape':'full',
                      'lw':lwg,
                      'length_includes_head':True,
                      'head_width':hwg,
                      'linestyle':'-',
                      'alpha':0.5}
    s_arrow_kwargs = {'shape':'full',
                      'lw':lws,
                      'length_includes_head':True,
                      'head_width':hws,
                      'linestyle':'-',
                      'alpha':0.5}

    # (x0, y0, z0, dx0, dy0, dz0,
    #  x1, y1, z1, dx1, dy1, dz1,
    #  xb0, yb0, zb0, dxb0, dyb0, dzb0,
    #  xb1, yb1, zb1, dxb1, dyb1, dzb1,
    #  x2, y2, z2, dx2, dy2, dz2,
    #  x3, y3, z3, dx3, dy3, dz3,
    #  dr_tot) = get_arrow_data(best_central_0, best_central_1, group_proj_mask_0, group_proj_mask_1,
    #                           group_cops_0, group_cops_1,
    #                           best_0, best_1, sub_proj_mask_0, sub_proj_mask_1, sub_cops_0, sub_cops_1)

    # for i in range(4):
    #     dr_tot[i] = np.sort(dr_tot[i])

    # group_arg = funky_args_paried_sorted(dr_tot[0], dr_tot[1]) #np.argsort(np.hstack((dr_tot[0], dr_tot[1])))
    # sub_arg = funky_args_paried_sorted(dr_tot[2], dr_tot[3]) #np.argsort(np.hstack((dr_tot[2], dr_tot[3])))

    #put togather images
    xsize=200 #pixels

    #make arrays
    xlims, ylims, zlims = extent
    width = xlims[1] - xlims[0] #Mpc / h
    
    gal_lims = [-50 * width, 50 * width] #kpc

    x_edges = np.linspace(*xlims, xsize)
    y_edges = np.linspace(*ylims, xsize)
    z_edges = np.linspace(*zlims, xsize)

    gal_edges = np.linspace(*gal_lims, xsize)

    #7x
    #DM
    dm_hist_xz_7x,_,_ = np.histogram2d(PosDM_7x[:, 0], PosDM_7x[:, 2], bins = [x_edges, z_edges])
    dm_hist_xy_7x,_,_ = np.histogram2d(PosDM_7x[:, 0], PosDM_7x[:, 1], bins = [x_edges, y_edges])
    dm_hist_zy_7x,_,_ = np.histogram2d(PosDM_7x[:, 2], PosDM_7x[:, 1], bins = [z_edges, y_edges])

    dm_hist_xz_7x = np.flip(np.log10(1 + dm_hist_xz_7x.T), axis=0)
    dm_hist_xy_7x = np.flip(np.log10(1 + dm_hist_xy_7x.T), axis=0)
    dm_hist_zy_7x = np.flip(np.log10(1 + dm_hist_zy_7x.T), axis=0)

    dm_max_c_7x = np.nanmax((dm_hist_xz_7x, dm_hist_xy_7x, dm_hist_zy_7x))
    dm_min_c_7x = np.nanmin((dm_hist_xz_7x, dm_hist_xy_7x, dm_hist_zy_7x))

    dm_hist_xz_7x = (dm_hist_xz_7x - dm_min_c_7x) / (dm_max_c_7x - dm_min_c_7x)
    dm_hist_xy_7x = (dm_hist_xy_7x - dm_min_c_7x) / (dm_max_c_7x - dm_min_c_7x)
    dm_hist_zy_7x = (dm_hist_zy_7x - dm_min_c_7x) / (dm_max_c_7x - dm_min_c_7x)

    #gas
    gas_hist_xz_7x,_,_ = np.histogram2d(PosGas_7x[:, 0], PosGas_7x[:, 2], bins = [x_edges, z_edges])
    gas_hist_xy_7x,_,_ = np.histogram2d(PosGas_7x[:, 0], PosGas_7x[:, 1], bins = [x_edges, y_edges])
    gas_hist_zy_7x,_,_ = np.histogram2d(PosGas_7x[:, 2], PosGas_7x[:, 1], bins = [z_edges, y_edges])

    gas_hist_xz_7x = np.flip(np.log10(1 + gas_hist_xz_7x.T), axis=0)
    gas_hist_xy_7x = np.flip(np.log10(1 + gas_hist_xy_7x.T), axis=0)
    gas_hist_zy_7x = np.flip(np.log10(1 + gas_hist_zy_7x.T), axis=0)

    gas_max_c_7x = np.nanmax((gas_hist_xz_7x, gas_hist_xy_7x, gas_hist_zy_7x))
    gas_min_c_7x = np.nanmin((gas_hist_xz_7x, gas_hist_xy_7x, gas_hist_zy_7x))

    gas_hist_xz_7x = (gas_hist_xz_7x - gas_min_c_7x) / (gas_max_c_7x - gas_min_c_7x)
    gas_hist_xy_7x = (gas_hist_xy_7x - gas_min_c_7x) / (gas_max_c_7x - gas_min_c_7x)
    gas_hist_zy_7x = (gas_hist_zy_7x - gas_min_c_7x) / (gas_max_c_7x - gas_min_c_7x)

    #star
    star_hist_xz_7x,_,_ = np.histogram2d(PosStar_7x[:, 0], PosStar_7x[:, 2], bins = [x_edges, z_edges])
    star_hist_xy_7x,_,_ = np.histogram2d(PosStar_7x[:, 0], PosStar_7x[:, 1], bins = [x_edges, y_edges])
    star_hist_zy_7x,_,_ = np.histogram2d(PosStar_7x[:, 2], PosStar_7x[:, 1], bins = [z_edges, y_edges])

    star_hist_xz_7x = np.flip(np.log10(1 + star_hist_xz_7x.T), axis=0)
    star_hist_xy_7x = np.flip(np.log10(1 + star_hist_xy_7x.T), axis=0)
    star_hist_zy_7x = np.flip(np.log10(1 + star_hist_zy_7x.T), axis=0)

    star_max_c_7x = np.nanmax((star_hist_xz_7x, star_hist_xy_7x, star_hist_zy_7x))
    star_min_c_7x = np.nanmin((star_hist_xz_7x, star_hist_xy_7x, star_hist_zy_7x))

    star_hist_xz_7x = (star_hist_xz_7x - star_min_c_7x) / (star_max_c_7x - star_min_c_7x)
    star_hist_xy_7x = (star_hist_xy_7x - star_min_c_7x) / (star_max_c_7x - star_min_c_7x)
    star_hist_zy_7x = (star_hist_zy_7x - star_min_c_7x) / (star_max_c_7x - star_min_c_7x)

    #1x
    #DM
    dm_hist_xz_1x,_,_ = np.histogram2d(PosDM_1x[:, 0], PosDM_1x[:, 2], bins = [x_edges, z_edges])
    dm_hist_xy_1x,_,_ = np.histogram2d(PosDM_1x[:, 0], PosDM_1x[:, 1], bins = [x_edges, y_edges])
    dm_hist_zy_1x,_,_ = np.histogram2d(PosDM_1x[:, 2], PosDM_1x[:, 1], bins = [z_edges, y_edges])

    dm_hist_xz_1x = np.flip(np.log10(1 + dm_hist_xz_1x.T), axis=0)
    dm_hist_xy_1x = np.flip(np.log10(1 + dm_hist_xy_1x.T), axis=0)
    dm_hist_zy_1x = np.flip(np.log10(1 + dm_hist_zy_1x.T), axis=0)

    dm_max_c_1x = np.nanmax((dm_hist_xz_1x, dm_hist_xy_1x, dm_hist_zy_1x))
    dm_min_c_1x = np.nanmin((dm_hist_xz_1x, dm_hist_xy_1x, dm_hist_zy_1x))

    dm_hist_xz_1x = (dm_hist_xz_1x - dm_min_c_1x) / (dm_max_c_1x - dm_min_c_1x)
    dm_hist_xy_1x = (dm_hist_xy_1x - dm_min_c_1x) / (dm_max_c_1x - dm_min_c_1x)
    dm_hist_zy_1x = (dm_hist_zy_1x - dm_min_c_1x) / (dm_max_c_1x - dm_min_c_1x)

    #gas
    gas_hist_xz_1x,_,_ = np.histogram2d(PosGas_1x[:, 0], PosGas_1x[:, 2], bins = [x_edges, z_edges])
    gas_hist_xy_1x,_,_ = np.histogram2d(PosGas_1x[:, 0], PosGas_1x[:, 1], bins = [x_edges, y_edges])
    gas_hist_zy_1x,_,_ = np.histogram2d(PosGas_1x[:, 2], PosGas_1x[:, 1], bins = [z_edges, y_edges])

    gas_hist_xz_1x = np.flip(np.log10(1 + gas_hist_xz_1x.T), axis=0)
    gas_hist_xy_1x = np.flip(np.log10(1 + gas_hist_xy_1x.T), axis=0)
    gas_hist_zy_1x = np.flip(np.log10(1 + gas_hist_zy_1x.T), axis=0)

    gas_max_c_1x = np.nanmax((gas_hist_xz_1x, gas_hist_xy_1x, gas_hist_zy_1x))
    gas_min_c_1x = np.nanmin((gas_hist_xz_1x, gas_hist_xy_1x, gas_hist_zy_1x))

    gas_hist_xz_1x = (gas_hist_xz_1x - gas_min_c_1x) / (gas_max_c_1x - gas_min_c_1x)
    gas_hist_xy_1x = (gas_hist_xy_1x - gas_min_c_1x) / (gas_max_c_1x - gas_min_c_1x)
    gas_hist_zy_1x = (gas_hist_zy_1x - gas_min_c_1x) / (gas_max_c_1x - gas_min_c_1x)

    #star
    star_hist_xz_1x,_,_ = np.histogram2d(PosStar_1x[:, 0], PosStar_1x[:, 2], bins = [x_edges, z_edges])
    star_hist_xy_1x,_,_ = np.histogram2d(PosStar_1x[:, 0], PosStar_1x[:, 1], bins = [x_edges, y_edges])
    star_hist_zy_1x,_,_ = np.histogram2d(PosStar_1x[:, 2], PosStar_1x[:, 1], bins = [z_edges, y_edges])

    star_hist_xz_1x = np.flip(np.log10(1 + star_hist_xz_1x.T), axis=0)
    star_hist_xy_1x = np.flip(np.log10(1 + star_hist_xy_1x.T), axis=0)
    star_hist_zy_1x = np.flip(np.log10(1 + star_hist_zy_1x.T), axis=0)

    star_max_c_1x = np.nanmax((star_hist_xz_1x, star_hist_xy_1x, star_hist_zy_1x))
    star_min_c_1x = np.nanmin((star_hist_xz_1x, star_hist_xy_1x, star_hist_zy_1x))

    star_hist_xz_1x = (star_hist_xz_1x - star_min_c_1x) / (star_max_c_1x - star_min_c_1x)
    star_hist_xy_1x = (star_hist_xy_1x - star_min_c_1x) / (star_max_c_1x - star_min_c_1x)
    star_hist_zy_1x = (star_hist_zy_1x - star_min_c_1x) / (star_max_c_1x - star_min_c_1x)

    #projections
    #7x
    # real units and centre and everything
    starpos_7x = PosStar_7x - cop_7x - SCALE_A * BOX_SIZE / 2
    starpos_7x %= SCALE_A * BOX_SIZE
    starpos_7x -= SCALE_A * BOX_SIZE / 2
    starpos_7x *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    r = np.linalg.norm(starpos_7x, axis=1)
    starmask_7x = (r < 30 * LITTLE_H)

    starvel_7x = VelStar_7x * np.sqrt(SCALE_A)  # km/s
    starmass_7x = MassStar_7x * LITTLE_H  # 10^10 M_sun

    vel_offset_7x = np.sum(starmass_7x[starmask_7x, np.newaxis] * starvel_7x[starmask_7x, :],
                          axis=0) / np.sum(starmass_7x[starmask_7x])
    starvel_7x -= vel_offset_7x

    (starpos_7x, starvel_7x, rotation_7x) = align(starpos_7x, starvel_7x, starmass_7x, apature=30)

    #gas
    gaspos_7x = PosGas_7x - cop_7x - SCALE_A * BOX_SIZE / 2
    gaspos_7x %= SCALE_A * BOX_SIZE
    gaspos_7x -= SCALE_A * BOX_SIZE / 2
    gaspos_7x *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    gasmass_7x = MassGas_7x * LITTLE_H  # 10^10 M_sun

    gaspos_7x = rotation_7x.apply(gaspos_7x)

    #1x
    # real units and centre and everything
    starpos_1x = PosStar_1x - cop_1x - SCALE_A * BOX_SIZE / 2
    starpos_1x %= SCALE_A * BOX_SIZE
    starpos_1x -= SCALE_A * BOX_SIZE / 2
    starpos_1x *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    r = np.linalg.norm(starpos_1x, axis=1)
    starmask_1x = (r < 30 * LITTLE_H)

    starvel_1x = VelStar_1x * np.sqrt(SCALE_A)  # km/s
    starmass_1x = MassStar_1x * LITTLE_H  # 10^10 M_sun

    vel_offset_1x = np.sum(starmass_1x[starmask_1x, np.newaxis] * starvel_1x[starmask_1x, :],
                          axis=0) / np.sum(starmass_1x[starmask_1x])
    starvel_1x -= vel_offset_1x

    (starpos_1x, starvel_1x, rotation_1x) = align(starpos_1x, starvel_1x, starmass_1x, apature=30)

    #gas
    gaspos_1x = PosGas_1x - cop_1x - SCALE_A * BOX_SIZE / 2
    gaspos_1x %= SCALE_A * BOX_SIZE
    gaspos_1x -= SCALE_A * BOX_SIZE / 2
    gaspos_1x *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    gasmass_1x = MassGas_1x * LITTLE_H  # 10^10 M_sun

    gaspos_1x = rotation_1x.apply(gaspos_1x)

    #ga[gal_lims, gal_lims]
    gas_proj_xz_7x,_,_ = np.histogram2d(gaspos_7x[:, 0], gaspos_7x[:, 2], bins = [gal_edges, gal_edges])
    gas_proj_xy_7x,_,_ = np.histogram2d(gaspos_7x[:, 0], gaspos_7x[:, 1], bins = [gal_edges, gal_edges])
    gas_proj_zy_7x,_,_ = np.histogram2d(gaspos_7x[:, 2], gaspos_7x[:, 1], bins = [gal_edges, gal_edges])

    gas_proj_xz_7x = np.flip(np.log10(1 + gas_proj_xz_7x.T), axis=0)
    gas_proj_xy_7x = np.flip(np.log10(1 + gas_proj_xy_7x.T), axis=0)
    gas_proj_zy_7x = np.flip(np.log10(1 + gas_proj_zy_7x.T), axis=0)

    gas_max_c_proj_7x = np.nanmax((gas_proj_xz_7x, gas_proj_xy_7x, gas_proj_zy_7x))
    gas_min_c_proj_7x = np.nanmin((gas_proj_xz_7x, gas_proj_xy_7x, gas_proj_zy_7x))

    gas_proj_xz_7x = (gas_proj_xz_7x - gas_min_c_proj_7x) / (gas_max_c_proj_7x - gas_min_c_proj_7x)
    gas_proj_xy_7x = (gas_proj_xy_7x - gas_min_c_proj_7x) / (gas_max_c_proj_7x - gas_min_c_proj_7x)
    gas_proj_zy_7x = (gas_proj_zy_7x - gas_min_c_proj_7x) / (gas_max_c_proj_7x - gas_min_c_proj_7x)

    #star
    star_proj_xz_7x,_,_ = np.histogram2d(starpos_7x[starmask_7x, 0], starpos_7x[starmask_7x, 2], bins = [gal_edges, gal_edges])
    star_proj_xy_7x,_,_ = np.histogram2d(starpos_7x[starmask_7x, 0], starpos_7x[starmask_7x, 1], bins = [gal_edges, gal_edges])
    star_proj_zy_7x,_,_ = np.histogram2d(starpos_7x[starmask_7x, 2], starpos_7x[starmask_7x, 1], bins = [gal_edges, gal_edges])

    star_proj_xz_7x = np.flip(np.log10(1 + star_proj_xz_7x.T), axis=0)
    star_proj_xy_7x = np.flip(np.log10(1 + star_proj_xy_7x.T), axis=0)
    star_proj_zy_7x = np.flip(np.log10(1 + star_proj_zy_7x.T), axis=0)

    star_max_c_proj_7x = np.nanmax((star_proj_xz_7x, star_proj_xy_7x, star_proj_zy_7x))
    star_min_c_proj_7x = np.nanmin((star_proj_xz_7x, star_proj_xy_7x, star_proj_zy_7x))

    star_proj_xz_7x = (star_proj_xz_7x - star_min_c_proj_7x) / (star_max_c_proj_7x - star_min_c_proj_7x)
    star_proj_xy_7x = (star_proj_xy_7x - star_min_c_proj_7x) / (star_max_c_proj_7x - star_min_c_proj_7x)
    star_proj_zy_7x = (star_proj_zy_7x - star_min_c_proj_7x) / (star_max_c_proj_7x - star_min_c_proj_7x)

    #1x
    #gas
    gas_proj_xz_1x,_,_ = np.histogram2d(gaspos_1x[:, 0], gaspos_1x[:, 2], bins = [gal_edges, gal_edges])
    gas_proj_xy_1x,_,_ = np.histogram2d(gaspos_1x[:, 0], gaspos_1x[:, 1], bins = [gal_edges, gal_edges])
    gas_proj_zy_1x,_,_ = np.histogram2d(gaspos_1x[:, 2], gaspos_1x[:, 1], bins = [gal_edges, gal_edges])

    gas_proj_xz_1x = np.flip(np.log10(1 + gas_proj_xz_1x.T), axis=0)
    gas_proj_xy_1x = np.flip(np.log10(1 + gas_proj_xy_1x.T), axis=0)
    gas_proj_zy_1x = np.flip(np.log10(1 + gas_proj_zy_1x.T), axis=0)

    gas_max_c_proj_1x = np.nanmax((gas_proj_xz_1x, gas_proj_xy_1x, gas_proj_zy_1x))
    gas_min_c_proj_1x = np.nanmin((gas_proj_xz_1x, gas_proj_xy_1x, gas_proj_zy_1x))

    gas_proj_xz_1x = (gas_proj_xz_1x - gas_min_c_proj_1x) / (gas_max_c_proj_1x - gas_min_c_proj_1x)
    gas_proj_xy_1x = (gas_proj_xy_1x - gas_min_c_proj_1x) / (gas_max_c_proj_1x - gas_min_c_proj_1x)
    gas_proj_zy_1x = (gas_proj_zy_1x - gas_min_c_proj_1x) / (gas_max_c_proj_1x - gas_min_c_proj_1x)

    #star
    star_proj_xz_1x,_,_ = np.histogram2d(starpos_1x[starmask_1x, 0], starpos_1x[starmask_1x, 2], bins = [gal_edges, gal_edges])
    star_proj_xy_1x,_,_ = np.histogram2d(starpos_1x[starmask_1x, 0], starpos_1x[starmask_1x, 1], bins = [gal_edges, gal_edges])
    star_proj_zy_1x,_,_ = np.histogram2d(starpos_1x[starmask_1x, 2], starpos_1x[starmask_1x, 1], bins = [gal_edges, gal_edges])

    star_proj_xz_1x = np.flip(np.log10(1 + star_proj_xz_1x.T), axis=0)
    star_proj_xy_1x = np.flip(np.log10(1 + star_proj_xy_1x.T), axis=0)
    star_proj_zy_1x = np.flip(np.log10(1 + star_proj_zy_1x.T), axis=0)

    star_max_c_proj_1x = np.nanmax((star_proj_xz_1x, star_proj_xy_1x, star_proj_zy_1x))
    star_min_c_proj_1x = np.nanmin((star_proj_xz_1x, star_proj_xy_1x, star_proj_zy_1x))

    star_proj_xz_1x = (star_proj_xz_1x - star_min_c_proj_1x) / (star_max_c_proj_1x - star_min_c_proj_1x)
    star_proj_xy_1x = (star_proj_xy_1x - star_min_c_proj_1x) / (star_max_c_proj_1x - star_min_c_proj_1x)
    star_proj_zy_1x = (star_proj_zy_1x - star_min_c_proj_1x) / (star_max_c_proj_1x - star_min_c_proj_1x)

    #image'
    #DM
    dm_separate_img_xz = np.zeros((*np.shape(dm_hist_xz_1x), 3))
    dm_separate_img_xy = np.zeros((*np.shape(dm_hist_xy_1x), 3))
    dm_separate_img_zy = np.zeros((*np.shape(dm_hist_zy_1x), 3))

    dm_separate_img_xz[:, :, 0] =  dm_hist_xz_1x * 1.2
    dm_separate_img_xy[:, :, 0] =  dm_hist_xy_1x * 1.2
    dm_separate_img_zy[:, :, 0] =  dm_hist_zy_1x * 1.2

    dm_separate_img_xz[:, :, 1] =  (dm_hist_xz_7x + dm_hist_xz_1x) * 1.2 / 2
    dm_separate_img_xy[:, :, 1] =  (dm_hist_xy_7x + dm_hist_xy_1x) * 1.2 / 2
    dm_separate_img_zy[:, :, 1] =  (dm_hist_zy_7x + dm_hist_zy_1x) * 1.2 / 2

    dm_separate_img_xz[:, :, 2] =  dm_hist_xz_7x * 1.2
    dm_separate_img_xy[:, :, 2] =  dm_hist_xy_7x * 1.2
    dm_separate_img_zy[:, :, 2] =  dm_hist_zy_7x * 1.2

    dm_diff_img_xz = np.zeros((*np.shape(dm_hist_xz_1x), 3))
    dm_diff_img_xy = np.zeros((*np.shape(dm_hist_xy_1x), 3))
    dm_diff_img_zy = np.zeros((*np.shape(dm_hist_zy_1x), 3))

    dm_diff_img_xz[:, :, 0] = (dm_hist_xz_1x - dm_hist_xz_7x + 1)/2
    dm_diff_img_xy[:, :, 0] = (dm_hist_xy_1x - dm_hist_xy_7x + 1)/2
    dm_diff_img_zy[:, :, 0] = (dm_hist_zy_1x - dm_hist_zy_7x + 1)/2

    dm_diff_img_xz[:, :, 1] = (dm_hist_xz_1x - dm_hist_xz_7x + 1)/2
    dm_diff_img_xy[:, :, 1] = (dm_hist_xy_1x - dm_hist_xy_7x + 1)/2
    dm_diff_img_zy[:, :, 1] = (dm_hist_zy_1x - dm_hist_zy_7x + 1)/2

    dm_diff_img_xz[:, :, 2] = (dm_hist_xz_1x - dm_hist_xz_7x + 1)/2
    dm_diff_img_xy[:, :, 2] = (dm_hist_xy_1x - dm_hist_xy_7x + 1)/2
    dm_diff_img_zy[:, :, 2] = (dm_hist_zy_1x - dm_hist_zy_7x + 1)/2

    #gas
    gas_separate_img_xz = np.zeros((*np.shape(gas_hist_xz_1x), 3))
    gas_separate_img_xy = np.zeros((*np.shape(gas_hist_xy_1x), 3))
    gas_separate_img_zy = np.zeros((*np.shape(gas_hist_zy_1x), 3))

    gas_separate_img_xz[:, :, 0] =  gas_hist_xz_1x * 1.2
    gas_separate_img_xy[:, :, 0] =  gas_hist_xy_1x * 1.2
    gas_separate_img_zy[:, :, 0] =  gas_hist_zy_1x * 1.2

    gas_separate_img_xz[:, :, 1] =  gas_hist_xz_7x /np.sqrt(2) * 1.2
    gas_separate_img_xy[:, :, 1] =  gas_hist_xy_7x /np.sqrt(2) * 1.2
    gas_separate_img_zy[:, :, 1] =  gas_hist_zy_7x /np.sqrt(2) * 1.2

    gas_separate_img_xz[:, :, 2] =  gas_hist_xz_7x /np.sqrt(2) * 1.2
    gas_separate_img_xy[:, :, 2] =  gas_hist_xy_7x /np.sqrt(2) * 1.2
    gas_separate_img_zy[:, :, 2] =  gas_hist_zy_7x /np.sqrt(2) * 1.2

    #star
    star_separate_img_xz = np.zeros((*np.shape(star_hist_zy_1x), 3))
    star_separate_img_xy = np.zeros((*np.shape(star_hist_zy_1x), 3))
    star_separate_img_zy = np.zeros((*np.shape(star_hist_zy_1x), 3))

    star_separate_img_xz[:, :, 0] =  star_hist_xz_1x * 1.2
    star_separate_img_xy[:, :, 0] =  star_hist_xy_1x * 1.2
    star_separate_img_zy[:, :, 0] =  star_hist_zy_1x * 1.2

    star_separate_img_xz[:, :, 1] =  star_hist_xz_7x * 1.2
    star_separate_img_xy[:, :, 1] =  star_hist_xy_7x * 1.2
    star_separate_img_zy[:, :, 1] =  star_hist_zy_7x * 1.2

    proj_star_7x = np.zeros((*np.shape(star_proj_xz_7x), 3))
    proj_gas_7x = np.zeros((*np.shape(gas_proj_xz_7x), 3))
    proj_star_1x = np.zeros((*np.shape(star_proj_xz_1x), 3))
    proj_gas_1x = np.zeros((*np.shape(gas_proj_xz_1x), 3))
    proj_quadrants = np.zeros((*np.shape(star_proj_xz_7x), 3))

    proj_star_7x[:, :, 1] = star_proj_xz_7x * 1.2
    proj_gas_7x[:, :, 1] = gas_proj_xz_7x * 1.2

    proj_star_1x[:, :, 0] = star_proj_xz_1x * 1.2

    proj_gas_1x[:, :, 0] = gas_proj_xz_1x * 1.2

    proj_quadrants[:, xsize//2:, 0] = star_proj_xz_1x[:, xsize//2:] * 1.2
    proj_quadrants[:, :xsize//2, 1] = star_proj_xz_7x[:, :xsize//2] * 1.2
    # proj_quadrants[:, xsize//2:, 0] = star_proj_xz_1x[:, xsize//2:] * 1.2
    # proj_quadrants[:, :xsize//2, 1] = star_proj_xz_7x[:, xsize//2 - 1:] * 1.2

    # proj_quadrants[:, :xsize//2, 2] = star_proj_xz_7x[:, :xsize//2] * 0 * 1.2

    # proj_quadrants[:, :, 0] = star_proj_xz_7x * 1.2
    # proj_quadrants[:, :, 1] = star_proj_xz_1x /np.sqrt(2) * 1.2
    # proj_quadrants[:, :, 2] = star_proj_xz_1x /np.sqrt(2) * 1.2

    # proj_quadrants[:, :, 0] = (dm_hist_xz_1x - star_proj_xz_7x + 1)/2
    # proj_quadrants[:, :, 1] = (dm_hist_xz_1x - star_proj_xz_7x + 1)/2
    # proj_quadrants[:, :, 2] = (dm_hist_xz_1x - star_proj_xz_7x + 1)/2

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 2], marker='o', c='C0', s=gs)
    ax00.scatter(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 2], marker='o', c='C1', s=gs)

    ax10.scatter(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 1], marker='o', c='C0', s=gs)
    ax10.scatter(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 1], marker='o', c='C1', s=gs)

    ax11.scatter(GroupCentreOfPotential_7x[:, 2], GroupCentreOfPotential_7x[:, 1], marker='o', c='C0', s=gs)
    ax11.scatter(GroupCentreOfPotential_1x[:, 2], GroupCentreOfPotential_1x[:, 1], marker='o', c='C1', s=gs)

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 2], Group_R_Crit200_7x)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 2], Group_R_Crit200_1x)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 2], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 2], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')
    #
    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(dm_separate_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(dm_separate_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(dm_separate_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C0', **g_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(xb0, zb0, dxb0, dzb0)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C0', **g_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(xb0, yb0, dxb0, dyb0)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C0', **g_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(zb0, yb0, dzb0, dyb0)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C1', **g_arrow_kwargs)
    #  for x_, z_, dx_, dz_ in zip(xb1, zb1, dxb1, dzb1)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C1', **g_arrow_kwargs)
    #  for x_, y_, dx_, dy_ in zip(xb1, yb1, dxb1, dyb1)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C1', **g_arrow_kwargs)
    #  for z_, y_, dz_, dy_ in zip(zb1, yb1, dzb1, dyb1)]

    if save:
        filename = f'{out_loc}/{name}_positions_group{group_focus}_dm_separate'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 2], marker='o', c='C0', s=gs)
    ax00.scatter(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 2], marker='o', c='C1', s=gs)

    ax10.scatter(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 1], marker='o', c='C0', s=gs)
    ax10.scatter(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 1], marker='o', c='C1', s=gs)

    ax11.scatter(GroupCentreOfPotential_7x[:, 2], GroupCentreOfPotential_7x[:, 1], marker='o', c='C0', s=gs)
    ax11.scatter(GroupCentreOfPotential_1x[:, 2], GroupCentreOfPotential_1x[:, 1], marker='o', c='C1', s=gs)

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 2], Group_R_Crit200_7x)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 2], Group_R_Crit200_1x)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 2], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 2], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')
    #
    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(dm_diff_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(dm_diff_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(dm_diff_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C0', **g_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x0, z0, dx0, dz0)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C0', **g_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x0, y0, dx0, dy0)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C0', **g_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z0, y0, dz0, dy0)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C1', **g_arrow_kwargs)
    #  for x_, z_, dx_, dz_ in zip(x1, z1, dx1, dz1)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C1', **g_arrow_kwargs)
    #  for x_, y_, dx_, dy_ in zip(x1, y1, dx1, dy1)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C1', **g_arrow_kwargs)
    #  for z_, y_, dz_, dy_ in zip(z1, y1, dz1, dy1)]

    if save:
        filename = f'{out_loc}/{name}_positions_group{group_focus}_dm_diff'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(SubGroupCentreOfPotential_7x[:, 0], SubGroupCentreOfPotential_7x[:, 2], marker='o', c='C2', s=ss)
    ax00.scatter(SubGroupCentreOfPotential_1x[:, 0], SubGroupCentreOfPotential_1x[:, 2], marker='o', c='C3', s=ss)

    ax10.scatter(SubGroupCentreOfPotential_7x[:, 0], SubGroupCentreOfPotential_7x[:, 1], marker='o', c='C2', s=ss)
    ax10.scatter(SubGroupCentreOfPotential_1x[:, 0], SubGroupCentreOfPotential_1x[:, 1], marker='o', c='C3', s=ss)

    ax11.scatter(SubGroupCentreOfPotential_7x[:, 2], SubGroupCentreOfPotential_7x[:, 1], marker='o', c='C2', s=ss)
    ax11.scatter(SubGroupCentreOfPotential_1x[:, 2], SubGroupCentreOfPotential_1x[:, 1], marker='o', c='C3', s=ss)

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 2], Group_R_Crit200_7x)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 2], Group_R_Crit200_1x)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 2], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 2], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')
    #
    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(gas_separate_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(gas_separate_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(gas_separate_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C2', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x2, z2, dx2, dz2)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C2', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x2, y2, dx2, dy2)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C2', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z2, y2, dz2, dy2)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C3', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x3, z3, dx3, dz3)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C3', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x3, y3, dx3, dy3)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C3', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z3, y3, dz3, dy3)]

    if save:
        filename = f'{out_loc}/{name}_positions_group{group_focus}_gas_separate'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(SubGroupCentreOfPotential_7x[:, 0], SubGroupCentreOfPotential_7x[:, 2], marker='o', c='C2', s=ss)
    ax00.scatter(SubGroupCentreOfPotential_1x[:, 0], SubGroupCentreOfPotential_1x[:, 2], marker='o', c='C3', s=ss)

    ax10.scatter(SubGroupCentreOfPotential_7x[:, 0], SubGroupCentreOfPotential_7x[:, 1], marker='o', c='C2', s=ss)
    ax10.scatter(SubGroupCentreOfPotential_1x[:, 0], SubGroupCentreOfPotential_1x[:, 1], marker='o', c='C3', s=ss)

    ax11.scatter(SubGroupCentreOfPotential_7x[:, 2], SubGroupCentreOfPotential_7x[:, 1], marker='o', c='C2', s=ss)
    ax11.scatter(SubGroupCentreOfPotential_1x[:, 2], SubGroupCentreOfPotential_1x[:, 1], marker='o', c='C3', s=ss)

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 2], Group_R_Crit200_7x)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 2], Group_R_Crit200_1x)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 0], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 0], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_7x[:, 2], GroupCentreOfPotential_7x[:, 1], Group_R_Crit200_7x)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(GroupCentreOfPotential_1x[:, 2], GroupCentreOfPotential_1x[:, 1], Group_R_Crit200_1x)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')

    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(star_separate_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(star_separate_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(star_separate_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C2', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x2, z2, dx2, dz2)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C2', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x2, y2, dx2, dy2)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C2', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z2, y2, dz2, dy2)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C3', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x3, z3, dx3, dz3)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C3', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x3, y3, dx3, dy3)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C3', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z3, y3, dz3, dy3)]

    if save:
        filename = f'{out_loc}/{name}_positions_group{group_focus}_star_separate'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize1)

    ax0 = plt.subplot(511)
    ax1 = plt.subplot(512)
    ax2 = plt.subplot(513)
    ax3 = plt.subplot(514)
    ax4 = plt.subplot(515)

    ax0.set_ylabel(r"$Z'$ [kpc/h]")
    ax1.set_ylabel(r"$Z'$ [kpc/h]")
    ax2.set_ylabel(r"$Z'$ [kpc/h]")
    ax3.set_ylabel(r"$Z'$ [kpc/h]")
    ax4.set_ylabel(r"$Z'$ [kpc/h]")
    ax4.set_xlabel(r"$X'$ [kpc/h]")

    ax0.set_aspect('equal')
    ax1.set_aspect('equal')
    ax2.set_aspect('equal')
    ax3.set_aspect('equal')
    ax4.set_aspect('equal')

    ax0.set_xlim(gal_lims)
    ax0.set_ylim(gal_lims)
    ax1.set_xlim(gal_lims)
    ax1.set_ylim(gal_lims)
    ax2.set_xlim(gal_lims)
    ax2.set_ylim(gal_lims)
    ax3.set_xlim(gal_lims)
    ax3.set_ylim(gal_lims)
    ax4.set_xlim(gal_lims)
    ax4.set_ylim(gal_lims)

    ax0.imshow(proj_star_7x, extent=[*gal_lims, *gal_lims])
    ax1.imshow(proj_gas_7x, extent=[*gal_lims, *gal_lims])
    ax2.imshow(proj_star_1x, extent=[*gal_lims, *gal_lims])
    ax3.imshow(proj_gas_1x, extent=[*gal_lims, *gal_lims])
    ax4.imshow(proj_quadrants, extent=[*gal_lims, *gal_lims])

    X, Y = np.meshgrid((gal_edges[1:] + gal_edges[:-1]) / 2, (gal_edges[1:] + gal_edges[:-1]) / 2)
    ax4.contour(X, Y, np.flip(np.sum(proj_quadrants, axis=2), axis=0),
                levels = np.linspace(0.4,1.2,5), colors=['white']*5, linewidths=1, linestyles=[':', '-', ':', '-', ':'])

    if save:
        filename = f'{out_loc}/{name}_positions_group{group_focus}_star_proj'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def get_arrow_data(best_central_7x, best_central_1, group_proj_mask_0, group_proj_mask_1, group_cops_0, group_cops_1,
                   best_0, best_1, sub_proj_mask_0, sub_proj_mask_1, sub_cops_0, sub_cops_1):

    dr_tot = [[], [], [], []]

    x0, y0, z0 = [], [], []
    dx0,dy0,dz0 = [], [], []
    for i, j in enumerate(best_central_0):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_0[i] or group_proj_mask_1[j]):
            if not np.isnan(group_cops_0[i, 0]) and not np.isnan(group_cops_1[j, 0]):
                x0.append(group_cops_0[i, 0])
                y0.append(group_cops_0[i, 1])
                z0.append(group_cops_0[i, 2])
                dx0.append(group_cops_1[j, 0] - group_cops_0[i, 0])
                dy0.append(group_cops_1[j, 1] - group_cops_0[i, 1])
                dz0.append(group_cops_1[j, 2] - group_cops_0[i, 2])

                dr_tot[0].append(np.sqrt(pacman_dist2(group_cops_1[j, :], group_cops_0[i, :])))

    x1, y1, z1 = [], [], []
    dx1,dy1,dz1 = [], [], []
    for i, j in enumerate(best_central_1):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_1[i] or group_proj_mask_0[j]):
            if not np.isnan(group_cops_1[i, 0]) and not np.isnan(group_cops_0[j, 0]):
                x1.append(group_cops_1[i, 0])
                y1.append(group_cops_1[i, 1])
                z1.append(group_cops_1[i, 2])
                dx1.append(group_cops_0[j, 0] - group_cops_1[i, 0])
                dy1.append(group_cops_0[j, 1] - group_cops_1[i, 1])
                dz1.append(group_cops_0[j, 2] - group_cops_1[i, 2])

                dr_tot[1].append(np.sqrt(pacman_dist2(group_cops_0[j, :], group_cops_1[i, :])))

    xb0, yb0, zb0 = [], [], []
    dxb0, dyb0, dzb0 = [], [], []
    for i, j in enumerate(best_central_0):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_0[i] or group_proj_mask_1[j]):
            if best_central_1[j] == i:
                if not np.isnan(group_cops_0[i, 0]) and not np.isnan(group_cops_1[j, 0]):
                    xb0.append(group_cops_0[i, 0])
                    yb0.append(group_cops_0[i, 1])
                    zb0.append(group_cops_0[i, 2])
                    dxb0.append(group_cops_1[j, 0] - group_cops_0[i, 0])
                    dyb0.append(group_cops_1[j, 1] - group_cops_0[i, 1])
                    dzb0.append(group_cops_1[j, 2] - group_cops_0[i, 2])

    xb1, yb1, zb1 = [], [], []
    dxb1, dyb1, dzb1 = [], [], []
    for i, j in enumerate(best_central_1):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_1[i] or group_proj_mask_0[j]):
            if best_central_0[j] == i:
                if not np.isnan(group_cops_1[i, 0]) and not np.isnan(group_cops_0[j, 0]):
                    xb1.append(group_cops_1[i, 0])
                    yb1.append(group_cops_1[i, 1])
                    zb1.append(group_cops_1[i, 2])
                    dxb1.append(group_cops_0[j, 0] - group_cops_1[i, 0])
                    dyb1.append(group_cops_0[j, 1] - group_cops_1[i, 1])
                    dzb1.append(group_cops_0[j, 2] - group_cops_1[i, 2])

    x2, y2, z2 = [], [], []
    dx2,dy2,dz2 = [], [], []
    for i, j in enumerate(best_0):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (sub_proj_mask_0[i] or sub_proj_mask_1[j]):
            x2.append(sub_cops_0[i, 0])
            y2.append(sub_cops_0[i, 1])
            z2.append(sub_cops_0[i, 2])
            dx2.append(sub_cops_1[j, 0] - sub_cops_0[i, 0])
            dy2.append(sub_cops_1[j, 1] - sub_cops_0[i, 1])
            dz2.append(sub_cops_1[j, 2] - sub_cops_0[i, 2])

            dr_tot[2].append(np.sqrt(pacman_dist2(sub_cops_1[j, :], sub_cops_0[i, :])))

    x3, y3, z3 = [], [], []
    dx3,dy3,dz3 = [], [], []
    for i, j in enumerate(best_1):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (sub_proj_mask_1[i] or sub_proj_mask_0[j]):
            x3.append(sub_cops_1[i, 0])
            y3.append(sub_cops_1[i, 1])
            z3.append(sub_cops_1[i, 2])
            dx3.append(sub_cops_0[j, 0] - sub_cops_1[i, 0])
            dy3.append(sub_cops_0[j, 1] - sub_cops_1[i, 1])
            dz3.append(sub_cops_0[j, 2] - sub_cops_1[i, 2])

            dr_tot[3].append(np.sqrt(pacman_dist2(sub_cops_0[j, :], sub_cops_1[i, :])))

    return(x0, y0, z0, dx0, dy0, dz0,
           x1, y1, z1, dx1, dy1, dz1,
           xb0, yb0, zb0, dxb0, dyb0, dzb0,
           xb1, yb1, zb1, dxb1, dyb1, dzb1,
           x2, y2, z2, dx2, dy2, dz2,
           x3, y3, z3, dx3, dy3, dz3,
           dr_tot)


def align(pos, vel, mass, apature=30):
    """Aligns the z cartesian direction with the direction of angular momentum.
    Can use an apature.
    """
    # direction to align
    if apature is not None:
        r = np.linalg.norm(pos, axis=1)
        mask = r < apature

        j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

    else:
        j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

    # find rotation
    rotation = find_rotaion_matrix(j_tot)

    # rotate stars
    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    return pos, vel, rotation


def find_rotaion_matrix(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0
    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)

    if j_tot[2] < 0:
        x += 180

    return Rotation.from_euler('yx', [y, x], degrees=True)


def funky_args_paried_sorted(arr0, arr1):

    paired0 = np.zeros(len(arr0), dtype=bool)
    paired1 = np.zeros(len(arr1), dtype=bool)

    pairs = 0

    for i, a0 in enumerate(arr0):
        for j, a1 in enumerate(arr1):
            if a0 == a1: #np.all(np.isclose(a0, a1, rtol=1e-9)):
                paired0[i] = True
                paired1[j] = True
                pairs += 1

    out_0 = np.zeros(len(arr0))
    k, j = 0, 0
    for i in range(len(arr0)):
        if paired0[i]:
            out_0[i] = k * 2
            k += 1
        else:
            out_0[i] = 2*pairs + j
            j += 1

    out_1 = np.zeros(len(arr1))
    k, j = 0, 0
    for i in range(len(arr1)):
        if paired1[i]:
            out_1[i] = 1 + k * 2
            k += 1
        else:
            out_1[i] = len(arr0) + pairs + j
            j += 1

    return out_0, out_1


def vec_pacman_dist2(u, v):
    dx = np.abs(u[:, 0] - v[:, 0])
    dy = np.abs(u[:, 1] - v[:, 1])
    dz = np.abs(u[:, 2] - v[:, 2])

    dx[dx > 16.9425] = 33.885 - dx[dx > 16.9425]
    dy[dy > 16.9425] = 33.885 - dy[dy > 16.9425]
    dz[dz > 16.9425] = 33.885 - dz[dz > 16.9425]

    return dx * dx + dy * dy + dz * dz


def do_everything(particle_data_location_7x, halo_data_location_7x,
                  particle_data_location_1x, halo_data_location_1x,
                  matched_groups_file, matched_subs_file,
                  snap, output_image_location, match_index):

    start_time = time.time()
    extent, cop_7x, cop_1x = get_defined_region(match_index, matched_groups_file, halo_data_location_7x, halo_data_location_1x, snap)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    group_data_7x = read_groups_defined_region(halo_data_location_7x, snap, extent)
    group_data_1x = read_groups_defined_region(halo_data_location_1x, snap, extent)
    # (Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
    #  GroupNumber, SubGroupNumber, SubGroupCentreOfPotential)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    part_dtat_7x = read_particles_defined_region(particle_data_location_7x, snap, extent)
    part_dtat_1x = read_particles_defined_region(particle_data_location_1x, snap, extent)
    # (PosGas, MassGas, PosDM, PosStar, VelStar, MassStar)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    make_matched_galaxy_image(extent, cop_7x, cop_1x,
                              *group_data_7x, *group_data_1x,
                              *part_dtat_7x, *part_dtat_1x,
                              group_focus=match_index, name=snap, out_loc=output_image_location)
    if VERBOSE: print(str(np.round(time.time() - start_time, 1)))

    return


if __name__ == '__main__':
    _, snap_index, match_index = sys.argv

    snap_index = int(snap_index)
    match_index = int(match_index)

    #hyades
    # particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
    #                                '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
    #                            '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    #ozstar
    particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
                                   '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
                               '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']
    output_image_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/images',
                                 '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/images']

    matched_groups_files = [
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/028_z000p000_matched_groups_only_25.hdf5',
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/023_z000p503_matched_groups_only_25.hdf5',
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/019_z001p004_matched_groups_only_25.hdf5',
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/015_z002p012_matched_groups_only_25.hdf5' ]

    matched_subs_files = [
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/028_z000p000_matched_subgroups_only_25.hdf5',
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/023_z000p503_matched_subgroups_only_25.hdf5',
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/019_z001p004_matched_subgroups_only_25.hdf5',
        '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/015_z002p012_matched_subgroups_only_25.hdf5' ]

    particle_data_location_1x = particle_data_location_list[snap_index // 4]
    particle_data_location_7x = particle_data_location_list[1 + (snap_index // 4)]
    halo_data_location_1x = halo_data_location_list[snap_index // 4]
    halo_data_location_7x = halo_data_location_list[1 + (snap_index // 4)]
    output_data_location_1x =  output_data_location_list[snap_index // 4]
    output_data_location_7x =  output_data_location_list[1 + (snap_index // 4)]
    output_image_location_1x =  output_image_location_list[snap_index // 4]
    output_image_location_7x =  output_image_location_list[1 + (snap_index // 4)]
    kdtree_location_1x = output_data_location_1x
    kdtree_location_7x = output_data_location_7x

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]
    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    matched_groups_file = matched_groups_files[snap_index % 4]
    matched_subs_file = matched_subs_files[snap_index % 4]

    do_everything(particle_data_location_7x, halo_data_location_7x,
                  particle_data_location_1x, halo_data_location_1x,
                  matched_groups_file, matched_subs_file,
                  snap, output_image_location_1x, match_index)