#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

#import os
import numpy as np
#ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

# import matplotlib
# matplotlib.use('Agg')

import h5py  as h5

# from scipy.integrate         import quad
from scipy.optimize          import brentq
from scipy.spatial           import cKDTree
from scipy.spatial.transform import Rotation
from scipy.stats             import binned_statistic

import pickle

import time

import sys

import ReadEagleParticles_All
import ReadEagleSubfind_halo

def read_calculate_write(particle_data_location, subfind_data_location, tree_data_location,
                         output_data_location, snap):
  '''Galaxies
  Needs to do everything because of memory / speed things.
  '''
  #file name things
  line =snap.split('\n')[0].split('_')
  Num  =line[0]
  fend ='_'+line[1]
  exts =Num.zfill(3)

  #load halos
  time_halo_read = time.time()
  halo_data = ReadEagleSubfind_halo.ReadEagleSubfind_halo(subfind_data_location,'',fend,exts)

  Group_M_Crit200    = halo_data['Group_M_Crit200'][()]
  Group_R_Crit200    = halo_data['Group_R_Crit200'][()]
  GroupPos           = halo_data['GroupPos'][()]
  FirstSub           = halo_data['FirstSub'][()]

  # HalfMassRadius     = halo_data['HalfMassRad'][()]
  # HalfMassProjRad    = halo_data['HalfMassProjRad'][()]
  SubPos             = halo_data['SubPos'][()]
  #there is an issue with on of the file's FirstSub.
  try:
    SubPos = SubPos[FirstSub]
  except IndexError:
    print(len(FirstSub))
    print(max(FirstSub))

    for i in range(len(FirstSub)):
      if FirstSub[i] >= len(SubPos):
        print('adjusting halo')
        FirstSub[i] = len(FirstSub) - 1

    SubPos = SubPos[FirstSub]
  # SubPos             = SubPos[FirstSub]
  # Vmax               = halo_data['Vmax'][()]
  # Rmax               = halo_data['Rmax'][()]
  # MassType           = halo_data['MassType'][()]
  # SubMass            = halo_data['SubMass'][()]
  # GroupNumber        = halo_data['GroupNumber'][()]
  # SubGroupNumber     = halo_data['SubGroupNumber'][()]

  print('loaded halo data in ', np.round(time.time() - time_halo_read,1))

  n_centrals = len(Group_M_Crit200)
  print(n_centrals, ' centrals to create profiles for.')

  #read particles
  #maximum 40Gb for 25 mpc box with 7x dm rezolution.
  time_particle_read = time.time()
  particle_data = ReadEagleParticles_All.ReadEagleParticles_All(
    particle_data_location,'',fend,exts)

  #Global properties
  HubbleParam     = particle_data['HubbleParam']
  HubbleExpansion = particle_data['HubbleExpansion']
  Redshift        = particle_data['Redshift']
  ExpansionFactor = particle_data['ExpansionFactor']
  PartMassDM      = particle_data['PartMassDM']
  BoxSize         = particle_data['BoxSize']
  om              = particle_data['om']

  # Gas
  PosGas     = particle_data['PosGas']
  # VelGas     = particle_data['VelGas']
  MassGas    = particle_data['MassGas']
  # IDsGas     = particle_data['IDsGas']
  GrpNum_Gas = particle_data['GrpNum_Gas']
  SubNum_Gas = particle_data['SubNum_Gas']
  #DM
  PosDM     = particle_data['PosDM']
  VelDM     = particle_data['VelDM']
  # IDsDM     = particle_data['IDsDM']
  GrpNum_DM = particle_data['GrpNum_DM']
  SubNum_DM = particle_data['SubNum_DM']
  #Star
  PosStar       = particle_data['PosStar']
  VelStar       = particle_data['VelStar']
  MassStar      = particle_data['MassStar']
  # BirthDensity  = particle_data['BirthDensity']
  # Metallicity    = particle_data['Metallicity']
  Star_aform    = particle_data['Star_aform']
  # Star_tform    = particle_data['Star_tform']
  BindingEnergy = particle_data['BindingEnergy']
  # IDsDM         = particle_data['IDsDM']
  GrpNum_Star   = particle_data['GrpNum_Star']
  SubNum_Star   = particle_data['SubNum_Star']

  print('loaded particle data in ', np.round(time.time() - time_particle_read,1))

  #tree stuff
  time_kd_make = time.time()
  #TODO save trees

  print('Shape is ', np.shape(PosGas))

  kdtree_Gas  = cKDTree(PosGas,  leafsize=10, boxsize=BoxSize,
                        compact_nodes=False, balanced_tree=False, copy_data=True)

  time_kd_gas = time.time()
  print('calculated gas  kdtree in ', np.round(time_kd_gas  - time_kd_make,1))
  print('Shape is ', np.shape(PosDM))

  kdtree_DM   = cKDTree(PosDM,   leafsize=10, boxsize=BoxSize,
                        compact_nodes=False, balanced_tree=False, copy_data=True)

  time_kd_dm = time.time()
  print('calculated dm   kdtree in ', np.round(time_kd_dm   - time_kd_gas,1))
  print('Shape is ', np.shape(PosStar))

  kdtree_Star = cKDTree(PosStar, leafsize=10, boxsize=BoxSize,
                        compact_nodes=False, balanced_tree=False, copy_data=True)

  time_kd_star = time.time()
  print('calculated star kdtree in ', np.round(time_kd_star - time_kd_dm,1))

  print('calculated all  kdtree in ', np.round(time_kd_star - time_kd_make,1))

  print('loaded everything')

  constant_200 = get_200_constant(H=HubbleExpansion, a=ExpansionFactor, z=Redshift,
                                  om=om, h=HubbleParam, H_0=100*HubbleParam)
  constant_200_z0 = get_200_constant(H=None, a=None, z=0,
                                     om=om, h=HubbleParam, H_0=100*HubbleParam
                                     ) * ExpansionFactor**3
  thirty_ckpc = HubbleParam * 30 / ExpansionFactor * 1e-3

  print('Constant 200    = \t', constant_200)
  print('Constant 200 z0 = \t', constant_200_z0)
  print('30 h * cMpc     = \t', thirty_ckpc)
  print('BoxSize h * Mpc = \t', BoxSize)

  #set up arrays to save values
  group_id    = np.zeros(n_centrals, dtype=np.int64)
  subgroup_id = np.zeros(n_centrals, dtype=np.int64)

  galaxy_R200                = np.zeros(n_centrals, dtype=np.float64)
  galaxy_M200                = np.zeros(n_centrals, dtype=np.float64)
  galaxy_R200_z0             = np.zeros(n_centrals, dtype=np.float64)
  galaxy_M200_z0             = np.zeros(n_centrals, dtype=np.float64)

  stellar_half_mass_radius   = np.zeros(n_centrals, dtype=np.float64)
  stellar_quater_mass_radius = np.zeros(n_centrals, dtype=np.float64)
  stellar3quater_mass_radius = np.zeros(n_centrals, dtype=np.float64)

  stellar_half_mass_radius_proj   = np.zeros(n_centrals, dtype=np.float64)
  stellar_quater_mass_radius_proj = np.zeros(n_centrals, dtype=np.float64)
  stellar3quater_mass_radius_proj = np.zeros(n_centrals, dtype=np.float64)

  axis_a = np.zeros(n_centrals, dtype=np.float64)
  axis_b = np.zeros(n_centrals, dtype=np.float64)
  axis_c = np.zeros(n_centrals, dtype=np.float64)

  # lin_bin_edges = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
  #              21,22,23,24,25,26,27,28,29,30]
  lin_bin_edges = np.linspace(0, 30, 31)
  log_bin_edges = np.logspace(np.log10(1), np.log10(30), 14)
  #+ <30kpc, <r200, <R_half, <2R_half, <R_quater, <R_3quater
  # to put in header
  # bins = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
  #         21,22,23,24,25,26,27,28,29,30,
  #         1, 1.3, 1.7, 2.2, 2.9, 3.7,  4.8, 6.2, 8.1, 10.5, 13.7, 17.8, 23.0, 30.0,
  #         30, 200, 1,2, 1,3]

  #change if bins change
  n_dim = 49
  n_lin = 30
  n_log = 13

  #stars
  bin_mass  = np.zeros((n_centrals,n_dim), dtype=np.float64)
  bin_N     = np.zeros((n_centrals,n_dim), dtype=np.int64)

  fjzjc10 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc09 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc08 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc07 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc06 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc05 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc04 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc03 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc02 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc01 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjc00 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm1 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm2 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm3 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm4 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm5 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm6 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm7 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm8 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm9 = np.zeros((n_centrals,n_dim), dtype=np.float64)
  fjzjcm0 = np.zeros((n_centrals,n_dim), dtype=np.float64)

  kappa_rot = np.zeros((n_centrals,n_dim), dtype=np.float64)
  kappa_co  = np.zeros((n_centrals,n_dim), dtype=np.float64)

  mean_v_R     = np.zeros((n_centrals,n_dim), dtype=np.float64)
  mean_v_phi   = np.zeros((n_centrals,n_dim), dtype=np.float64)
  median_v_phi = np.zeros((n_centrals,n_dim), dtype=np.float64)

  sigma_z   = np.zeros((n_centrals,n_dim), dtype=np.float64)
  sigma_R   = np.zeros((n_centrals,n_dim), dtype=np.float64)
  sigma_phi = np.zeros((n_centrals,n_dim), dtype=np.float64)

  z_half = np.zeros((n_centrals,n_dim), dtype=np.float64)

  formation_a_median = np.zeros((n_centrals,n_dim), dtype=np.float64)

  #DM
  dm_N       = np.zeros((n_centrals,n_dim), dtype=np.int64)
  dm_sigma_x = np.zeros((n_centrals,n_dim), dtype=np.float64)
  dm_sigma_y = np.zeros((n_centrals,n_dim), dtype=np.float64)
  dm_sigma_z = np.zeros((n_centrals,n_dim), dtype=np.float64)

  #gas
  gas_mass = np.zeros((n_centrals,n_dim), dtype=np.float64)

  time_profile_calc = time.time()
  #only calculate centrals
  for halo_i in range(n_centrals):
    try:
      FOF_R200            = Group_R_Crit200[halo_i]
      # centre_of_potential = GroupPos[halo_i]
      centre_of_potential = SubPos[halo_i]

      #calculate M200 and R200
      #use the tree
      gas_200_mask  = kdtree_Gas.query_ball_point( x = centre_of_potential, r = 2 * FOF_R200)
      dm_200_mask   = kdtree_DM.query_ball_point(  x = centre_of_potential, r = 2 * FOF_R200)
      star_200_mask = kdtree_Star.query_ball_point(x = centre_of_potential, r = 2 * FOF_R200)

      gas_pos  = PosGas[gas_200_mask]
      gas_pos  = gas_pos - centre_of_potential - BoxSize/2
      gas_pos %= BoxSize
      gas_pos -= BoxSize/2
      gas_mass = MassGas[gas_200_mask]

      dm_pos   = PosDM[dm_200_mask]
      dm_pos   = dm_pos - centre_of_potential - BoxSize/2
      dm_pos  %= BoxSize
      dm_pos  -= BoxSize/2
      dm_mass  = PartMassDM * np.ones(len(dm_200_mask))

      star_pos  = PosStar[star_200_mask]
      star_pos  = star_pos - centre_of_potential - BoxSize/2
      star_pos %= BoxSize
      star_pos -= BoxSize/2
      star_mass = MassStar[star_200_mask]

      (R200EU, M200) = get_crit_200(dm_mass, dm_pos, gas_mass, gas_pos, star_mass, star_pos,
                                  constant_200)

      if ((np.abs(R200EU - FOF_R200) / R200EU) > 0.2):
        print('Warning: My R200 very different from subfind')
        print(R200EU, '\t ', FOF_R200)

      R200 = R200EU * 1e3 * ExpansionFactor / HubbleParam #kpc
      M200 = M200 * 1e10 / HubbleParam #Msun

      (R200_z0, M200_z0) = get_crit_200(dm_mass, dm_pos, gas_mass, gas_pos, star_mass, star_pos,
                                        constant_200_z0)

      R200_z0 = R200_z0 * 1e3 * ExpansionFactor / HubbleParam #kpc
      M200_z0 = M200_z0 * 1e10 / HubbleParam #Msun

      # star_index_mask = kdtree_Star.query_ball_point(x = centre_of_potential,
      #                                                r = thirty_ckpc)
      # #mask relevant
      # pos    = PosStar[star_index_mask]
      # vel    = VelStar[star_index_mask]
      # mass   = MassStar[star_index_mask]
      # bind   = BindingEnergy[star_index_mask]
      # form_a = Star_aform[star_index_mask]

      # if len(mass) == 0:
      #   # print('Warning: Group ', halo_i, ' has no star particles < 30kpc.')
      #   # print(centre_of_potential)
      #   continue

      star_index_mask = kdtree_Star.query_ball_point(x = centre_of_potential,
                                                     r = R200EU)

      sub = SubNum_Star[star_index_mask]
      sub_mask = (np.abs(sub) == 0)

      #mask relevant
      pos    = PosStar[star_index_mask][sub_mask]
      vel    = VelStar[star_index_mask][sub_mask]
      mass   = MassStar[star_index_mask][sub_mask]
      bind   = BindingEnergy[star_index_mask][sub_mask]
      form_a = Star_aform[star_index_mask][sub_mask]

      if len(mass) == 0:
        # print('Warning: Group ', halo_i, ' has no star particles < 30kpc.')
        # print(centre_of_potential)
        continue

      #real units and centre and everything
      pos = pos - centre_of_potential - BoxSize/2
      pos %= BoxSize
      pos -= BoxSize/2
      pos *= 1000 * ExpansionFactor / HubbleParam #to kpc

      r = np.linalg.norm(pos, axis=1)
      mask = (r < 30)

      vel  *= np.sqrt(ExpansionFactor) #km/s
      mass /= HubbleParam #10^10 M_sun

      vel_offset = np.sum(mass[mask, np.newaxis] * vel[mask,:], axis=0) / np.sum(mass[mask])
      vel -= vel_offset

      # pot *= ExpansionFactor #(km/s)^2

      (pos, vel) = align(pos, vel, mass, apature=30)

      #cylindrical coords
      (R, phi, z, v_R, v_phi, v_z
       ) = get_cylindrical(pos, vel)

      r = np.linalg.norm(pos, axis=1)

      # j_z = np.cross(R, v_phi)
      j_z = pos[:,0] * vel[:,1] - pos[:,1] * vel[:,0]
      j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

      arg_bind = np.argsort(bind)
      arg_arg  = np.argsort(arg_bind)

      #TODO check
      j_E = max_within_50(j_tot[arg_bind])[arg_arg]
      j_zonc = j_z / j_E

      #shape parameters
      (a,b,c) = find_abc(pos, mass)

      axis_a[halo_i] = a
      axis_b[halo_i] = b
      axis_c[halo_i] = c

      #calculate profiles
      lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
      log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)

      #save profiles
      #linear
      bin_mass[halo_i, 0:n_lin] = lin_profiles[0]
      bin_N[halo_i, 0:n_lin]    = lin_profiles[1]

      fjzjc10[halo_i, 0:n_lin] = lin_profiles[2]
      fjzjc09[halo_i, 0:n_lin] = lin_profiles[3]
      fjzjc08[halo_i, 0:n_lin] = lin_profiles[4]
      fjzjc07[halo_i, 0:n_lin] = lin_profiles[5]
      fjzjc06[halo_i, 0:n_lin] = lin_profiles[6]
      fjzjc05[halo_i, 0:n_lin] = lin_profiles[7]
      fjzjc04[halo_i, 0:n_lin] = lin_profiles[8]
      fjzjc03[halo_i, 0:n_lin] = lin_profiles[9]
      fjzjc02[halo_i, 0:n_lin] = lin_profiles[10]
      fjzjc01[halo_i, 0:n_lin] = lin_profiles[11]
      fjzjc00[halo_i, 0:n_lin] = lin_profiles[12]
      fjzjcm1[halo_i, 0:n_lin] = lin_profiles[13]
      fjzjcm2[halo_i, 0:n_lin] = lin_profiles[14]
      fjzjcm3[halo_i, 0:n_lin] = lin_profiles[15]
      fjzjcm4[halo_i, 0:n_lin] = lin_profiles[16]
      fjzjcm5[halo_i, 0:n_lin] = lin_profiles[17]
      fjzjcm6[halo_i, 0:n_lin] = lin_profiles[18]
      fjzjcm7[halo_i, 0:n_lin] = lin_profiles[19]
      fjzjcm8[halo_i, 0:n_lin] = lin_profiles[20]
      fjzjcm9[halo_i, 0:n_lin] = lin_profiles[21]
      fjzjcm0[halo_i, 0:n_lin] = lin_profiles[22]

      kappa_rot[halo_i, 0:n_lin] = lin_profiles[23]
      kappa_co[halo_i, 0:n_lin]  = lin_profiles[24]

      mean_v_R[halo_i, 0:n_lin]     = lin_profiles[25]
      mean_v_phi[halo_i, 0:n_lin]   = lin_profiles[26]
      median_v_phi[halo_i, 0:n_lin] = lin_profiles[27]

      sigma_z[halo_i, 0:n_lin]   = lin_profiles[28]
      sigma_R[halo_i, 0:n_lin]   = lin_profiles[29]
      sigma_phi[halo_i, 0:n_lin] = lin_profiles[30]

      z_half[halo_i, 0:n_lin] = lin_profiles[31]

      formation_a_median[halo_i, 0:n_lin] = lin_profiles[32]

      #log
      bin_mass[halo_i, n_lin:(n_lin+n_log)] = log_profiles[0]
      bin_N[halo_i, n_lin:(n_lin+n_log)]    = log_profiles[1]

      fjzjc10[halo_i, n_lin:(n_lin+n_log)] = log_profiles[2]
      fjzjc09[halo_i, n_lin:(n_lin+n_log)] = log_profiles[3]
      fjzjc08[halo_i, n_lin:(n_lin+n_log)] = log_profiles[4]
      fjzjc07[halo_i, n_lin:(n_lin+n_log)] = log_profiles[5]
      fjzjc06[halo_i, n_lin:(n_lin+n_log)] = log_profiles[6]
      fjzjc05[halo_i, n_lin:(n_lin+n_log)] = log_profiles[7]
      fjzjc04[halo_i, n_lin:(n_lin+n_log)] = log_profiles[8]
      fjzjc03[halo_i, n_lin:(n_lin+n_log)] = log_profiles[9]
      fjzjc02[halo_i, n_lin:(n_lin+n_log)] = log_profiles[10]
      fjzjc01[halo_i, n_lin:(n_lin+n_log)] = log_profiles[11]
      fjzjc00[halo_i, n_lin:(n_lin+n_log)] = log_profiles[12]
      fjzjcm1[halo_i, n_lin:(n_lin+n_log)] = log_profiles[13]
      fjzjcm2[halo_i, n_lin:(n_lin+n_log)] = log_profiles[14]
      fjzjcm3[halo_i, n_lin:(n_lin+n_log)] = log_profiles[15]
      fjzjcm4[halo_i, n_lin:(n_lin+n_log)] = log_profiles[16]
      fjzjcm5[halo_i, n_lin:(n_lin+n_log)] = log_profiles[17]
      fjzjcm6[halo_i, n_lin:(n_lin+n_log)] = log_profiles[18]
      fjzjcm7[halo_i, n_lin:(n_lin+n_log)] = log_profiles[19]
      fjzjcm8[halo_i, n_lin:(n_lin+n_log)] = log_profiles[20]
      fjzjcm9[halo_i, n_lin:(n_lin+n_log)] = log_profiles[21]
      fjzjcm0[halo_i, n_lin:(n_lin+n_log)] = log_profiles[22]

      kappa_rot[halo_i, n_lin:(n_lin+n_log)] = log_profiles[23]
      kappa_co[halo_i, n_lin:(n_lin+n_log)]  = log_profiles[24]

      mean_v_R[halo_i, n_lin:(n_lin+n_log)]     = log_profiles[25]
      mean_v_phi[halo_i, n_lin:(n_lin+n_log)]   = log_profiles[26]
      median_v_phi[halo_i, n_lin:(n_lin+n_log)] = log_profiles[27]

      sigma_z[halo_i, n_lin:(n_lin+n_log)]   = log_profiles[28]
      sigma_R[halo_i, n_lin:(n_lin+n_log)]   = log_profiles[29]
      sigma_phi[halo_i, n_lin:(n_lin+n_log)] = log_profiles[30]

      z_half[halo_i, n_lin:(n_lin+n_log)] = log_profiles[31]

      formation_a_median[halo_i, n_lin:(n_lin+n_log)] = log_profiles[32]

      #aparures
      (R_half, R_quater, R3quater) = get_radii_that_are_interesting(R, mass)
      (r_half, r_quater, r3quater) = get_radii_that_are_interesting(r, mass)

      galaxy_R200[halo_i]    = R200
      galaxy_M200[halo_i]    = M200
      galaxy_R200_z0[halo_i] = R200_z0
      galaxy_M200_z0[halo_i] = M200_z0
      stellar_half_mass_radius_proj[halo_i]   = R_half
      stellar_quater_mass_radius_proj[halo_i] = R_quater
      stellar3quater_mass_radius_proj[halo_i] = R3quater
      stellar_half_mass_radius[halo_i]   = r_half
      stellar_quater_mass_radius[halo_i] = r_quater
      stellar3quater_mass_radius[halo_i] = r3quater

      #calculate
      apature_30     = get_kinematic_apature(30,       pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
      apature_r200   = get_kinematic_apature(R200,     pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
      apature_half   = get_kinematic_apature(r_half,   pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
      apature2half   = get_kinematic_apature(2*r_half, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
      apature_quater = get_kinematic_apature(r_quater, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)
      apature3quater = get_kinematic_apature(r3quater, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a)

      #save apatures
      bin_mass[halo_i, (n_lin+n_log):n_dim] = [apature_30[0], apature_r200[0], apature_half[0], apature2half[0], apature_quater[0], apature3quater[0]]
      bin_N[halo_i, (n_lin+n_log):n_dim]    = [apature_30[1], apature_r200[1], apature_half[1], apature2half[1], apature_quater[1], apature3quater[1]]

      fjzjc10[halo_i, (n_lin+n_log):n_dim] = [apature_30[2], apature_r200[2], apature_half[2], apature2half[2], apature_quater[2], apature3quater[2]]
      fjzjc09[halo_i, (n_lin+n_log):n_dim] = [apature_30[3], apature_r200[3], apature_half[3], apature2half[3], apature_quater[3], apature3quater[3]]
      fjzjc08[halo_i, (n_lin+n_log):n_dim] = [apature_30[4], apature_r200[4], apature_half[4], apature2half[4], apature_quater[4], apature3quater[4]]
      fjzjc07[halo_i, (n_lin+n_log):n_dim] = [apature_30[5], apature_r200[5], apature_half[5], apature2half[5], apature_quater[5], apature3quater[5]]
      fjzjc06[halo_i, (n_lin+n_log):n_dim] = [apature_30[6], apature_r200[6], apature_half[6], apature2half[6], apature_quater[6], apature3quater[6]]
      fjzjc05[halo_i, (n_lin+n_log):n_dim] = [apature_30[7], apature_r200[7], apature_half[7], apature2half[7], apature_quater[7], apature3quater[7]]
      fjzjc04[halo_i, (n_lin+n_log):n_dim] = [apature_30[8], apature_r200[8], apature_half[8], apature2half[8], apature_quater[8], apature3quater[8]]
      fjzjc03[halo_i, (n_lin+n_log):n_dim] = [apature_30[9], apature_r200[9], apature_half[9], apature2half[9], apature_quater[9], apature3quater[9]]
      fjzjc02[halo_i, (n_lin+n_log):n_dim] = [apature_30[10], apature_r200[10], apature_half[10], apature2half[10], apature_quater[10], apature3quater[0]]
      fjzjc01[halo_i, (n_lin+n_log):n_dim] = [apature_30[11], apature_r200[11], apature_half[11], apature2half[11], apature_quater[11], apature3quater[11]]
      fjzjc00[halo_i, (n_lin+n_log):n_dim] = [apature_30[12], apature_r200[12], apature_half[12], apature2half[12], apature_quater[12], apature3quater[12]]
      fjzjcm1[halo_i, (n_lin+n_log):n_dim] = [apature_30[13], apature_r200[13], apature_half[13], apature2half[13], apature_quater[13], apature3quater[13]]
      fjzjcm2[halo_i, (n_lin+n_log):n_dim] = [apature_30[14], apature_r200[14], apature_half[14], apature2half[14], apature_quater[14], apature3quater[14]]
      fjzjcm3[halo_i, (n_lin+n_log):n_dim] = [apature_30[15], apature_r200[15], apature_half[15], apature2half[15], apature_quater[15], apature3quater[15]]
      fjzjcm4[halo_i, (n_lin+n_log):n_dim] = [apature_30[16], apature_r200[16], apature_half[16], apature2half[16], apature_quater[16], apature3quater[16]]
      fjzjcm5[halo_i, (n_lin+n_log):n_dim] = [apature_30[17], apature_r200[17], apature_half[17], apature2half[17], apature_quater[17], apature3quater[17]]
      fjzjcm6[halo_i, (n_lin+n_log):n_dim] = [apature_30[18], apature_r200[18], apature_half[18], apature2half[18], apature_quater[18], apature3quater[18]]
      fjzjcm7[halo_i, (n_lin+n_log):n_dim] = [apature_30[19], apature_r200[19], apature_half[19], apature2half[19], apature_quater[19], apature3quater[19]]
      fjzjcm8[halo_i, (n_lin+n_log):n_dim] = [apature_30[20], apature_r200[20], apature_half[20], apature2half[20], apature_quater[20], apature3quater[20]]
      fjzjcm9[halo_i, (n_lin+n_log):n_dim] = [apature_30[21], apature_r200[21], apature_half[21], apature2half[21], apature_quater[21], apature3quater[21]]
      fjzjcm0[halo_i, (n_lin+n_log):n_dim] = [apature_30[22], apature_r200[22], apature_half[22], apature2half[22], apature_quater[22], apature3quater[22]]

      kappa_rot[halo_i, (n_lin+n_log):n_dim] = [apature_30[23], apature_r200[23], apature_half[23], apature2half[23], apature_quater[23], apature3quater[23]]
      kappa_co[halo_i, (n_lin+n_log):n_dim]  = [apature_30[24], apature_r200[24], apature_half[24], apature2half[24], apature_quater[24], apature3quater[24]]

      mean_v_R[halo_i, (n_lin+n_log):n_dim]     = [apature_30[25], apature_r200[25], apature_half[25], apature2half[25], apature_quater[25], apature3quater[25]]
      mean_v_phi[halo_i, (n_lin+n_log):n_dim]   = [apature_30[26], apature_r200[26], apature_half[26], apature2half[26], apature_quater[26], apature3quater[26]]
      median_v_phi[halo_i, (n_lin+n_log):n_dim] = [apature_30[27], apature_r200[27], apature_half[27], apature2half[27], apature_quater[27], apature3quater[27]]

      sigma_z[halo_i, (n_lin+n_log):n_dim]   = [apature_30[28], apature_r200[28], apature_half[28], apature2half[28], apature_quater[28], apature3quater[28]]
      sigma_R[halo_i, (n_lin+n_log):n_dim]   = [apature_30[29], apature_r200[29], apature_half[29], apature2half[29], apature_quater[29], apature3quater[29]]
      sigma_phi[halo_i, (n_lin+n_log):n_dim] = [apature_30[30], apature_r200[30], apature_half[30], apature2half[29], apature_quater[30], apature3quater[30]]

      z_half[halo_i, (n_lin+n_log):n_dim] = [apature_30[31], apature_r200[31], apature_half[31], apature2half[31], apature_quater[31], apature3quater[31]]

      formation_a_median[halo_i, (n_lin+n_log):n_dim] = [apature_30[32], apature_r200[32], apature_half[32], apature2half[32], apature_quater[32], apature3quater[32]]

      #DM stuff
      #use the tree
      dm_index_mask = kdtree_DM.query_ball_point(x = centre_of_potential,
                                                 r = thirty_ckpc)

      dpos = PosDM[dm_index_mask]
      dvel = VelDM[dm_index_mask]

      #real units and centre and everything
      dpos = dpos - centre_of_potential - BoxSize/2
      dpos %= BoxSize
      dpos -= BoxSize/2
      dpos *= 1000 * ExpansionFactor / HubbleParam #to kpc

      dvel *= np.sqrt(ExpansionFactor) #km/s
      dvel -= vel_offset

      #don't need to align dm

      #dm profiles
      dm_r = np.linalg.norm(dpos, axis=1)

      dm_N[halo_i, 0:n_lin] = np.histogram(dm_r, bins=lin_bin_edges)[0]

      dm_sigma_x[halo_i, 0:n_lin] = binned_statistic(dm_r, dvel[:,0], bins=lin_bin_edges, statistic='std')[0]
      dm_sigma_y[halo_i, 0:n_lin] = binned_statistic(dm_r, dvel[:,1], bins=lin_bin_edges, statistic='std')[0]
      dm_sigma_z[halo_i, 0:n_lin] = binned_statistic(dm_r, dvel[:,2], bins=lin_bin_edges, statistic='std')[0]

      dm_N[halo_i, n_lin:(n_lin+n_log)] = np.histogram(dm_r, bins=log_bin_edges)[0]

      dm_sigma_x[halo_i, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,0], bins=log_bin_edges, statistic='std')[0]
      dm_sigma_y[halo_i, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,1], bins=log_bin_edges, statistic='std')[0]
      dm_sigma_z[halo_i, n_lin:(n_lin+n_log)] = binned_statistic(dm_r, dvel[:,2], bins=log_bin_edges, statistic='std')[0]

      dm_N[halo_i, (n_lin+n_log):n_dim] = [np.sum(dm_r<30), np.sum(dm_r<R200), np.sum(dm_r<r_half),
                                              np.sum(dm_r<2*r_half), np.sum(dm_r<r_quater), np.sum(dm_r<r3quater)]

      dm_sigma_x[halo_i, (n_lin+n_log):n_dim] = [np.std(dvel[dm_r<30,      0]), np.std(dvel[dm_r<R200,    0]),
                                                    np.std(dvel[dm_r<r_half,  0]), np.std(dvel[dm_r<2*r_half,0]),
                                                    np.std(dvel[dm_r<r_quater,0]), np.std(dvel[dm_r<r3quater,0])]
      dm_sigma_y[halo_i, (n_lin+n_log):n_dim] = [np.std(dvel[dm_r<30,      1]), np.std(dvel[dm_r<R200,    1]),
                                                    np.std(dvel[dm_r<r_half,  1]), np.std(dvel[dm_r<2*r_half,1]),
                                                    np.std(dvel[dm_r<r_quater,1]), np.std(dvel[dm_r<r3quater,1])]
      dm_sigma_z[halo_i, (n_lin+n_log):n_dim] = [np.std(dvel[dm_r<30,      2]), np.std(dvel[dm_r<R200,    2]),
                                                    np.std(dvel[dm_r<r_half,  2]), np.std(dvel[dm_r<2*r_half,2]),
                                                    np.std(dvel[dm_r<r_quater,2]), np.std(dvel[dm_r<r3quater,2])]

      if halo_i%1000 == 0:
        print('calculated ', halo_i, ' in ', np.round(time.time() - time_profile_calc,1))

      halo_i += 1

    except Exception:
      pass

  print('done all calculations.')
  #save all quantities

  #dark matter profile file
  with h5.File(output_data_location + snap + '_galaxy_DM_profile.hdf5', "w") as dm_output:
    dm_quant  = dm_output.create_group("GalaxyQuantities")
    dm_prof   = dm_output.create_group("GalaxyProfiles")

    dm_quant.create_dataset('GroupNumebr',    data = group_id)
    dm_quant.create_dataset('SubGroupNumber', data = subgroup_id)

    dm_prof.create_dataset('DMN',      data = dm_N)
    dm_prof.create_dataset('DMsigmax', data=dm_sigma_x)
    dm_prof.create_dataset('DMsigmay', data=dm_sigma_y)
    dm_prof.create_dataset('DMsigmaz', data=dm_sigma_z)

    print('wrote DM file.')

  #star profile file
  with h5.File(output_data_location + snap + '_galaxy_kinematic_profile.hdf5', "w") as output:
    quant  = output.create_group("GalaxyQuantities")
    prof   = output.create_group("GalaxyProfiles")

    quant.create_dataset('GroupNumber',    data = group_id)
    quant.create_dataset('SubGroupNumber', data = subgroup_id)

    quant.create_dataset('StellarHalfMassRadius',     data = stellar_half_mass_radius)
    quant.create_dataset('StellarQuarterMassRadius',  data = stellar_quater_mass_radius)
    quant.create_dataset('Stellar3QuarterMassRadius', data = stellar3quater_mass_radius)

    quant.create_dataset('StellarHalfMassRadiusProj',     data = stellar_half_mass_radius_proj)
    quant.create_dataset('StellarQuarterMassRadiusProj',  data = stellar_quater_mass_radius_proj)
    quant.create_dataset('Stellar3QuarterMassRadiusProj', data = stellar3quater_mass_radius_proj)

    quant.create_dataset('R200', data = galaxy_R200)
    quant.create_dataset('M200', data = galaxy_M200)
    quant.create_dataset('R200z0crit', data = galaxy_R200_z0)
    quant.create_dataset('M200z0crit', data = galaxy_M200_z0)

    quant.create_dataset('Axisa', data = axis_a)
    quant.create_dataset('Axisb', data = axis_b)
    quant.create_dataset('Axisc', data = axis_c)

    prof.create_dataset('BinMass', data = bin_mass)
    prof.create_dataset('BinN',    data = bin_N)

    prof.create_dataset('fjzjc10', data = fjzjc10)
    prof.create_dataset('fjzjc09', data = fjzjc09)
    prof.create_dataset('fjzjc08', data = fjzjc08)
    prof.create_dataset('fjzjc07', data = fjzjc07)
    prof.create_dataset('fjzjc06', data = fjzjc06)
    prof.create_dataset('fjzjc05', data = fjzjc05)
    prof.create_dataset('fjzjc04', data = fjzjc04)
    prof.create_dataset('fjzjc03', data = fjzjc03)
    prof.create_dataset('fjzjc02', data = fjzjc02)
    prof.create_dataset('fjzjc01', data = fjzjc01)
    prof.create_dataset('fjzjc00', data = fjzjc00)
    prof.create_dataset('fjzjcm1', data = fjzjcm1)
    prof.create_dataset('fjzjcm2', data = fjzjcm2)
    prof.create_dataset('fjzjcm3', data = fjzjcm3)
    prof.create_dataset('fjzjcm4', data = fjzjcm4)
    prof.create_dataset('fjzjcm5', data = fjzjcm5)
    prof.create_dataset('fjzjcm6', data = fjzjcm6)
    prof.create_dataset('fjzjcm7', data = fjzjcm7)
    prof.create_dataset('fjzjcm8', data = fjzjcm8)
    prof.create_dataset('fjzjcm9', data = fjzjcm9)
    prof.create_dataset('fjzjcm0', data = fjzjcm0)

    prof.create_dataset('kappaRot', data = kappa_rot)
    prof.create_dataset('kappaCo',  data = kappa_co)

    prof.create_dataset('MeanVR',     data = mean_v_R)
    prof.create_dataset('MeanVphi',   data = mean_v_phi)
    prof.create_dataset('MedianVphi', data = median_v_phi)

    prof.create_dataset('sigmaz',   data = sigma_z)
    prof.create_dataset('sigmaR',   data = sigma_R)
    prof.create_dataset('sigmaphi', data = sigma_phi)

    prof.create_dataset('zHalf', data = z_half)

    prof.create_dataset('MedianFormationa', data = formation_a_median)

    print('wrote file.')

  return

def get_cylindrical(PosStars, VelStars):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(np.square(PosStars[:,0]) + np.square(PosStars[:,1]))
  varphi   = np.arctan2(PosStars[:,1], PosStars[:,0])
  z        = PosStars[:,2]

  v_rho    = VelStars[:,0] * np.cos(varphi) + VelStars[:,1] * np.sin(varphi)
  v_varphi = -VelStars[:,0]* np.sin(varphi) + VelStars[:,1] * np.cos(varphi)
  v_z      = VelStars[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  R = find_rotaton_matrix(galaxy_anular_momentum)
  pos = R.apply(pos)
  '''
  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

def align(pos, vel, mass, apature=30):
  '''Aligns the z cartesian direction with the direction of angular momentum.
  Can use an apature.
  '''
  #direction to align
  if apature != None:
    r = np.linalg.norm(pos, axis=1)
    mask = r < apature

    j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

  else:
    j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

  #find rotation
  rotation = find_rotaion_matrix(j_tot)

  #rotate stars
  pos = rotation.apply(pos)
  vel = rotation.apply(vel)

  return(pos, vel)

def get_radii_that_are_interesting(r, mass):
  '''Find stellar half mass, quater mass and 3 quater mass.
  '''
  arg_order = np.argsort(r)
  cum_mass = np.cumsum(mass[arg_order])
  #fraction
  cum_mass /= cum_mass[-1]

  half   = r[arg_order][np.where(cum_mass > 0.50)[0][0]]
  quater = r[arg_order][np.where(cum_mass > 0.25)[0][0]]
  three  = r[arg_order][np.where(cum_mass > 0.75)[0][0]]

  return(half, quater, three)

def max_within_50(array):
  '''returns the largest value of array within 50 indices
  probably can be faster
  '''
  list_len = np.size(array)

  max_array = np.zeros(list_len)

  for i in range(list_len):
    low = np.amax((0, i-50))
    hi  = np.amin((i+50, list_len))

    max_array[i] = np.amax(array[low:hi])

  return(max_array)

def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
  '''Calculates the reduced inertia tensor
  M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
  Itterative selection is done in the other function.
  '''
  norm = m_p / e2_p

  m = np.zeros((3,3))

  for i in range(3):
    for j in range(3):
      m[i,j] = np.sum(norm * r_p[:, i] * r_p[:, j])

  m /= np.sum(norm)

  return(m)

def process_tensor(m):
  '''
  '''
  #DO NOT use np.linalg.eigh(m)
  #looks like there is a bug

  (eigan_values, eigan_vectors) = np.linalg.eig(m)

  order = np.flip(np.argsort(eigan_values))

  eigan_values  = eigan_values[order]
  eigan_vectors = eigan_vectors[order]

  return(eigan_values, eigan_vectors)

def defined_particles(pos, mass, eigan_values, eigan_vectors):
  '''Assumes eigan values are sorted
  '''
  #projection along each axis
  projected_a = (pos[:,0] * eigan_vectors[0,0] + pos[:,1] * eigan_vectors[0,1] +
                 pos[:,2] * eigan_vectors[0,2])
  projected_b = (pos[:,0] * eigan_vectors[1,0] + pos[:,1] * eigan_vectors[1,1] +
                 pos[:,2] * eigan_vectors[1,2])
  projected_c = (pos[:,0] * eigan_vectors[2,0] + pos[:,1] * eigan_vectors[2,1] +
                 pos[:,2] * eigan_vectors[2,2])

  #ellipse distance #Thob et al. 2019 eqn 4.
  ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1]/eigan_values[0]) +
                      np.square(projected_c) / (eigan_values[2]/eigan_values[0]) )

  #ellipse radius #Thob et al. 2019 eqn 4.
  ellipse_radius = np.power(np.square(eigan_values[0]) / (eigan_values[1] * eigan_values[2]), 1/3
                            ) * np.square(30)

  #Thob et al. 2019 eqn 4.
  inside_mask = ellipse_distance <= ellipse_radius

  return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])

def find_abc(pos, mass):
  '''Finds the major, intermediate and minor axes.
  Follows Thob et al. 2019 using quadrupole moment of mass to bias towards
  particles closer to the centre
  '''
  #no clue why this isn't working
  #try except
  try:
    n_ini = len(mass)

    #start off speherical
    r2 = np.square(np.linalg.norm(pos, axis=1))

    #stop problems with r=0
    po  = pos[r2 != 0]
    mas = mass[r2 != 0]
    r2  = r2[r2 != 0]

    #mass tensor of particles
    m = reduced_quadrupole_moments_of_mass_tensor(po, mas, r2)

    #linear algebra stuff
    (eigan_values, eigan_vectors) = process_tensor(m)

    #to see convergeance
    cona = np.sqrt(eigan_values[2] / eigan_values[0])
    bona = np.sqrt(eigan_values[1] / eigan_values[0])

    # done = False
    for i in range(100):

      #redefine particles, calculate ellipse distance
      (po, mas, ellipse_r2) = defined_particles(po, mas, eigan_values, eigan_vectors)

      if len(mas) == 0:
        #if galaxy is small, error is expected so don't print error
        if n_ini > 50:
          print('Warning: No particles left when finding shape')

        return(np.zeros(3, dtype=np.float64))

      #mass tensor of new particles
      m = reduced_quadrupole_moments_of_mass_tensor(po, mas, ellipse_r2)


      if np.any(np.isnan(m)) or np.any(np.isinf(m)):
        if n_ini > 50:
          print('Warning: Found a nan / inf when finding shape')

        (eigan_values, eigan_vectors) = (np.zeros(3, dtype=np.float64),
                                         np.identity(3, dtype=np.float64))

      else:
        #linear algebra stuff
        (eigan_values, eigan_vectors) = process_tensor(m)

      if (1 - np.sqrt(eigan_values[2] / eigan_values[0]) / cona < 0.01) and (
          1 - np.sqrt(eigan_values[1] / eigan_values[0]) / bona < 0.01):
        #converged
        # done = True
        break

      else:
        cona = np.sqrt(eigan_values[2] / eigan_values[0])
        bona = np.sqrt(eigan_values[1] / eigan_values[0])

    #some warnings
    # if not done:
    #   print('Warning: Shape did not converge.')

    # if len(mas) < 100:
    #   print('Warning: Defining shape with <100 particles.')

  except ValueError:
    return(np.zeros(3, dtype=np.float64))

  return(np.sqrt(eigan_values))

###############################################################################

def get_200_constant(H=None, a=None, z=None, om=[0.307,0.693], h=0.6777, H_0=67.77):
  '''rho < 200 rho_c(z)
  M(<R200) / ((4/3)pi R200^3) < 200 (3/(8pi)) H^2 /G
  M(<R200) / R200^3 < 100 H^2 / G
  H in EAGLE units.
  H_0 in km/s/Mpc
  '''
  G = 4.301e1 #Mpc (km/s)^2 / (10^10 Msun) (source wikipedia)

  if H != None and a != None:
    H *= 3.085678E24 * 1e-5 #to km/s/Mpc

    constant = 100 * a**3 * np.square(H / h) / G #10^10 Msun / kcp^3

  if z != None:
    H_z = np.sqrt( np.square(H_0) * (om[0] * (1+z)**3 + om[1]) )
    a   = 1/(1 + z)

    const_z = 100 * a**3 * np.square(H_z / h) / G #10^10 Msun * a^3 / (h^2 * Mcp^3)

  if H != None and a != None and z != None:
    if np.abs(constant - const_z) / constant > 0.001:
      print('Warning: Box cosmology inconsistant: H:', H, '\t H(z)', H_z)

  if H == None or a == None:
    constant = const_z

  #Will raise an error if neither H nor z is defined

  return(constant)

def get_crit_200(dm_mass, dm_pos, gas_mass, gas_pos, star_mass, star_pos,
                 constant_200):
  '''Assume pos in h*Mpc/a and mass_dm and mass in 10^10 Msun*h.
  Returns R200 in h*Mpc/a and M200 in 10^10Msun*h
  '''
  #Could also include bh, but should be negligable...
  dm_r   = np.linalg.norm(dm_pos,   axis=1)
  gas_r  = np.linalg.norm(gas_pos,  axis=1)
  star_r = np.linalg.norm(star_pos, axis=1)

  #TODO speed up by log(R200) or something
  #Should be faster than sorting points / using cumsum
  # M / 4/3 r_200^3 = 200 * 3/(8 pi) * H^2/G
  # M = 100 * r_200^3 * H^2 / G)
  zero = lambda R200 : (np.sum(dm_mass[dm_r < R200]) + np.sum(gas_mass[gas_r < R200]) +
    np.sum(star_mass[star_r < R200])) - constant_200 * R200 * R200 * R200

  #this could take a little while with large N
  try:
    # R200 = brentq(zero, (np.amin(dm_r)+1e-3), 100 * np.amax(dm_r))
    R200 = brentq(zero, (np.amin(dm_r)+1e-6), 100 * np.amax(dm_r))
  except ValueError:
    # print('Warning: f(a), f(b) signs. Assume halo is less dense than rho_crit so R200=0')
    R200 = 0

  M200 = np.sum(dm_mass[dm_r < R200]) + np.sum(gas_mass[gas_r < R200]) + np.sum(star_mass[star_r < R200])

  return(R200, M200)

###############################################################################

def my_binned_statistic(x, value, bins, statistic='sum'):
  '''Calls binned statistic, but returns zeros when x and value are empty.
  '''
  if len(x)==0 and len(value)==0:
    out = np.zeros(len(bins)-1)
  else:
    out = binned_statistic(x, value, bins=bins, statistic=statistic)[0]

  return(out)

def my_already_binned_statistic(value, digitized, bin_edges, statistic='sum'):
  '''Calculates the statistic in bins if binned_statistic has already digitized
  the data set.
  Should be faster than calling binned stastic a bunch of times ......
  '''
  n = len(bin_edges) - 1
  out_array = np.zeros(n, dtype=np.float64)

  if statistic == 'sum':
    func = np.sum
  elif statistic == 'median':
    func = np.median
  else:
    raise ValueError('Statistic ' + str(statistic) + ' not understood.')

  #would be faster without a for loop
  for i in range(n):
    out_array[i] = func(value[digitized == i+1])

  return(out_array)

def get_kinematic_profiles(bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a):
  '''Given galaxy particle data and bins, calculates kenematic profiles.
  See Genel et al. 2015, Lagos et al. 2017, Correra et al. 2017, Wilkinson et al. 2021
  Probably not super efficent to call binned statistic a bunch of times, can use
  the third output to mask bins. Too lazy / will be more confusing to code though.
  '''
  #binned
  (bin_mass,_,bin_number) = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
  bin_N    = np.histogram(R, bins=bin_edges)[0]

  number_of_bins = len(bin_edges) - 1

  #j_zonc
  #I don't like this. Needs to be done though
  fjzjc10 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc09 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc08 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc07 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc06 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc05 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc04 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc03 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc02 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc01 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjc00 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm1 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm2 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm3 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm4 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm5 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm6 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm7 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm8 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm9 = np.zeros(number_of_bins, dtype=np.float64)
  fjzjcm0 = np.zeros(number_of_bins, dtype=np.float64)

  #calculate kappas
  K_tot = np.zeros(number_of_bins, dtype=np.float64)
  K_rot = np.zeros(number_of_bins, dtype=np.float64)
  K_co  = np.zeros(number_of_bins, dtype=np.float64)

  #sigmas
  mean_v_R     = np.zeros(number_of_bins, dtype=np.float64)
  mean_v_phi   = np.zeros(number_of_bins, dtype=np.float64)
  median_v_phi = np.zeros(number_of_bins, dtype=np.float64)

  sigma_z    = np.zeros(number_of_bins, dtype=np.float64)
  sigma_R    = np.zeros(number_of_bins, dtype=np.float64)
  sigma_phi  = np.zeros(number_of_bins, dtype=np.float64)

  z_half = np.zeros(number_of_bins, dtype=np.float64)

  form_a_median = np.zeros(number_of_bins, dtype=np.float64)

  #would be faster without a for loop
  for i in range(number_of_bins):
    mask = (bin_number == i+1)

    bin_massi = 1 / bin_mass[i]

    mmass = mass[mask]
    # bin_massi = 1 / np.sum(mmass)
    mj_zonc = j_zonc[mask]
    mv_phi = v_phi[mask]

    #j_zonc
    fjzjc10[i] = np.sum(mmass[mj_zonc >= 1.0]) * bin_massi
    fjzjc09[i] = np.sum(mmass[mj_zonc >= 0.9]) * bin_massi
    fjzjc08[i] = np.sum(mmass[mj_zonc >= 0.8]) * bin_massi
    fjzjc07[i] = np.sum(mmass[mj_zonc >= 0.7]) * bin_massi
    fjzjc06[i] = np.sum(mmass[mj_zonc >= 0.6]) * bin_massi
    fjzjc05[i] = np.sum(mmass[mj_zonc >= 0.5]) * bin_massi
    fjzjc04[i] = np.sum(mmass[mj_zonc >= 0.4]) * bin_massi
    fjzjc03[i] = np.sum(mmass[mj_zonc >= 0.3]) * bin_massi
    fjzjc02[i] = np.sum(mmass[mj_zonc >= 0.2]) * bin_massi
    fjzjc01[i] = np.sum(mmass[mj_zonc >= 0.1]) * bin_massi
    fjzjc00[i] = np.sum(mmass[mj_zonc >= 0.0]) * bin_massi
    fjzjcm1[i] = np.sum(mmass[mj_zonc >=-0.1]) * bin_massi
    fjzjcm2[i] = np.sum(mmass[mj_zonc >=-0.2]) * bin_massi
    fjzjcm3[i] = np.sum(mmass[mj_zonc >=-0.3]) * bin_massi
    fjzjcm4[i] = np.sum(mmass[mj_zonc >=-0.4]) * bin_massi
    fjzjcm5[i] = np.sum(mmass[mj_zonc >=-0.5]) * bin_massi
    fjzjcm6[i] = np.sum(mmass[mj_zonc >=-0.6]) * bin_massi
    fjzjcm7[i] = np.sum(mmass[mj_zonc >=-0.7]) * bin_massi
    fjzjcm8[i] = np.sum(mmass[mj_zonc >=-0.8]) * bin_massi
    fjzjcm9[i] = np.sum(mmass[mj_zonc >=-0.9]) * bin_massi
    fjzjcm0[i] = np.sum(mmass[mj_zonc >=-1.0]) * bin_massi

    #calculate kappas
    K_tot[i] = np.sum(mmass * np.square(np.linalg.norm(vel[mask], axis=1)))
    K_rot[i] = np.sum(mmass * np.square(mv_phi))

    co = (mv_phi > 0)
    K_co[i] = np.sum(mmass[co] * np.square(mv_phi[co]))

    #sigmas
    mean_v_R[i]     = np.sum(mmass * v_R[mask]) * bin_massi
    mean_v_phi[i]   = np.sum(mmass * mv_phi) * bin_massi
    median_v_phi[i] = np.median(mv_phi)

    sigma_z[i]    = np.sum(mmass * np.square(v_z[mask])) * bin_massi
    sigma_R[i]    = np.sum(mmass * np.square(v_R[mask])) * bin_massi
    sigma_phi[i]  = np.sum(mmass * np.square(mv_phi - mean_v_phi[i])) * bin_massi

    #TODO replace with mass weighted median
    z_half[i] = np.median(np.abs(z[mask]))

    #TODO replace with mass weighted median
    #TODO add initial mass weighted median
    #probably doesn't need to be a profile
    form_a_median[i] = np.median(form_a)

  kappa_rot = K_rot / K_tot
  kappa_co  = K_co  / K_tot

  return(bin_mass, bin_N,
         fjzjc10, fjzjc09, fjzjc08, fjzjc07, fjzjc06, fjzjc05, fjzjc04, fjzjc03, fjzjc02, fjzjc01, fjzjc00,
         fjzjcm1, fjzjcm2, fjzjcm3, fjzjcm4, fjzjcm5, fjzjcm6, fjzjcm7, fjzjcm8, fjzjcm9, fjzjcm0,
         kappa_rot, kappa_co,
         mean_v_R, mean_v_phi, median_v_phi,
         sigma_z, sigma_R, sigma_phi,
         z_half, form_a_median)

def get_kinematic_apature(apature, pos, vel, mass, r, R, z, v_R, v_phi, v_z, j_zonc, form_a):
  '''Same as get_kinematic_profile, except apature rather than profile.
  '''
  #mask
  mask = (r < apature)

  pos  = pos[mask]
  vel  = vel[mask]
  mass = mass[mask]
  (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])
  j_zonc = j_zonc[mask]
  form_a = form_a[mask]

  bin_mass = np.sum(mass)
  bin_N    = np.size(mass)

  bin_massi = 1/bin_mass

  #jz/jc
  fjzjc10 = np.sum(mass[j_zonc >=1.0]) * bin_massi
  fjzjc09 = np.sum(mass[j_zonc > 0.9]) * bin_massi
  fjzjc08 = np.sum(mass[j_zonc > 0.8]) * bin_massi
  fjzjc07 = np.sum(mass[j_zonc > 0.7]) * bin_massi
  fjzjc06 = np.sum(mass[j_zonc > 0.6]) * bin_massi
  fjzjc05 = np.sum(mass[j_zonc > 0.5]) * bin_massi
  fjzjc04 = np.sum(mass[j_zonc > 0.4]) * bin_massi
  fjzjc03 = np.sum(mass[j_zonc > 0.3]) * bin_massi
  fjzjc02 = np.sum(mass[j_zonc > 0.2]) * bin_massi
  fjzjc01 = np.sum(mass[j_zonc > 0.1]) * bin_massi
  fjzjc00 = np.sum(mass[j_zonc > 0.0]) * bin_massi
  fjzjcm1 = np.sum(mass[j_zonc >-0.1]) * bin_massi
  fjzjcm2 = np.sum(mass[j_zonc >-0.2]) * bin_massi
  fjzjcm3 = np.sum(mass[j_zonc >-0.3]) * bin_massi
  fjzjcm4 = np.sum(mass[j_zonc >-0.4]) * bin_massi
  fjzjcm5 = np.sum(mass[j_zonc >-0.5]) * bin_massi
  fjzjcm6 = np.sum(mass[j_zonc >-0.6]) * bin_massi
  fjzjcm7 = np.sum(mass[j_zonc >-0.7]) * bin_massi
  fjzjcm8 = np.sum(mass[j_zonc >-0.8]) * bin_massi
  fjzjcm9 = np.sum(mass[j_zonc >-0.9]) * bin_massi
  fjzjcm0 = np.sum(mass[j_zonc >-1.0]) * bin_massi

  #calculate kappas
  K_tot = np.sum(mass * np.square(np.linalg.norm(vel,axis=1)))
  K_rot = np.sum(mass * np.square(v_phi))
  K_co  = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

  kappa_rot = K_rot / K_tot
  kappa_co  = K_co  / K_tot

  #sigmas
  mean_v_R   = np.sum(mass * v_R)   * bin_massi
  mean_v_phi = np.sum(mass * v_phi) * bin_massi
  median_v_phi = np.median(v_phi)

  sigma_z    = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
  sigma_R    = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
  sigma_phi  = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

  z_half = np.median(np.abs(z))

  form_a_median = np.median(form_a)

  return(bin_mass, bin_N,
         fjzjc10, fjzjc09, fjzjc08, fjzjc07, fjzjc06, fjzjc05, fjzjc04, fjzjc03, fjzjc02, fjzjc01, fjzjc00,
         fjzjcm1, fjzjcm2, fjzjcm3, fjzjcm4, fjzjcm5, fjzjcm6, fjzjcm7, fjzjcm8, fjzjcm9, fjzjcm0,
         kappa_rot, kappa_co,
         mean_v_R, mean_v_phi, median_v_phi,
         sigma_z, sigma_R, sigma_phi,
         z_half, form_a_median)

if __name__ == '__main__':
  #time
  start_time = time.time()

  _, run_index = sys.argv

  run_index = int(run_index)

  #Where the data is
  Base0 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/Jacks_7x376dm/data'
  pwd0  = '/mnt/su3ctm/mwilkinson/EAGLE/Jacks_7x376dm/'

  Base1 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/Jacks_7x376dm_Restart_z=1/data'
  pwd1  = '/mnt/su3ctm/mwilkinson/EAGLE/Jacks_7x376dm_Restart_z=1/'

  Base2 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/REFERENCE/data'
  pwd2  = '/mnt/su3ctm/mwilkinson/EAGLE/REFERENCE/'

  Base3 = '/mnt/su3ctm/ludlow/Eagle/L0025N0376/REFERENCE_Restart_z=1/data'
  pwd3  = '/mnt/su3ctm/mwilkinson/EAGLE/REFERENCE_Restart_z=1/'

  snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']

  Base = [Base0, Base1, Base2, Base3][run_index //4]
  pwd  = [pwd0,  pwd1,  pwd2,  pwd3][run_index  //4]

  snap = snap_list[run_index % 4]

  #do the thing
  read_calculate_write(Base, Base, Base,
                       pwd, snap)

  print('finished snap ' + snap + ' in ' + str(np.round(time.time() - start_time, 1)) + 's')


  print('done in ' +  str(np.round(time.time() - start_time, 1)) + 's')
  pass