#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

import h5py as h5

# import pickle
from scipy.spatial import cKDTree

import ctypes

LITTLE_H = 0.6777
# BOX_SIZE = 67.77
BOX_SIZE = 33.885 #50 * 0.6777 #kpc

NO_GROUP_NUMBER = 1073741824 #2**30

PEANO_SO_LOC = '/home/mwilkinson/EAGLE/scripts/'
# PEANO_SO_LOC = '/home/mwilkins/EAGLE/scripts/'

VERBOSE = True

def my_read(particle_data_location, halo_data_location, kdtree_location, snap, bits):

    fn = f'{halo_data_location}groups_{snap}/eagle_subfind_tab_{snap}.0.hdf5'
    start_time = time.time()
    if VERBOSE: print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    # FirstSub = np.zeros(TotNgroups, dtype=np.int32)

    # # Subhalo arrays
    # GroupNumber = np.zeros(TotNsubgroups, dtype=np.int32)
    # SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int32)
    # SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float32)

    NGrp_c = 0
    # NSub_c = 0

    if VERBOSE: print('TotNGroups:', TotNgroups)
    if VERBOSE: print('TotNSubgroups:', TotNsubgroups)

    for ifile in range(Ntask):
        fn = f'{particle_data_location}groups_{snap}/eagle_subfind_tab_{snap}.{str(ifile)}.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            # Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                # FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            # if Nsubgroups > 0:
            #     GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
            #     SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]
            #     SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/CentreOfPotential"][()]

                # NSub_c += Nsubgroups

    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    if VERBOSE: print('Loaded halos')

    #particles
    fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.0.hdf5'
    start_time = time.time()
    if VERBOSE: print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs
        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    if VERBOSE: print('NumPartTot:', NumPartTot)

    # PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    IDDM = np.zeros(NumPartTot[1], dtype=np.uint64)
    GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    SubNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    BindingEnergyDM = np.zeros(NumPartTot[1], dtype=np.float32)

    NDM_c = 0

    for ifile in range(FNumPerSnap):

        fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.{str(ifile)}.hdf5'
        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[1] > 0:

                # PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"][()]
                SubNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/SubGroupNumber"][()]
                IDDM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/ParticleIDs"][()]
                BindingEnergyDM[NDM_c:NDM_c + NumParts[1]] = fs["/PartType1/ParticleBindingEnergy"][()]

                NDM_c += NumParts[1]

    if VERBOSE: print(NDM_c)

    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')
    if VERBOSE: print('loaded particles')

    #t=0 positions from IDs
    #needs snap so that order matches
    IC_positions_decoded_file_name = f'{kdtree_location}{snap}_ID_decoded_positions.hdf5'
    start_time = time.time()
    if VERBOSE: print('IC positions decoded name ', IC_positions_decoded_file_name)

    if os.path.exists(IC_positions_decoded_file_name):
        if VERBOSE: print('Loading')

        with h5.File(IC_positions_decoded_file_name, "r") as fs:
            PosDM_ICs = fs["PartType1/ICCoordinates"][()]
            IC_IDDMs = fs["PartType1/ParticleIDs"][()]

        # if not np.all(IC_IDDMs == IDDM):
        #     raise ValueError('IDs do not mactch!')

        if VERBOSE: print('Opened IC positions')

    else:
        if VERBOSE: print('Calculating')
        #make tree
        PosDM_ICs = decode_peano_ids(IDDM, bits)
        if VERBOSE: print('Decoded IC positions in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with h5.File(IC_positions_decoded_file_name, "w") as fs:
            quant = fs.create_group('PartType1')
            quant.create_dataset('ICCoordinates', data=PosDM_ICs)
            quant.create_dataset('ParticleIDs', data=IDDM)
        if VERBOSE: print('Saved dm decoded IC positions.')
    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    #t=0 kdtree
    #needs snap so that order matches
    dm_tree_file_name = f'{kdtree_location}{snap}_t0_dm_tree.pickle'
    start_time = time.time()
    if VERBOSE: print('DM tree file name ', dm_tree_file_name)

    if os.path.exists(dm_tree_file_name):
        if VERBOSE: print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            t0_dm_kd_tree = pickle.load(dm_tree_file)
        if VERBOSE: print('Opened dm kdtree')

    else:
        if VERBOSE: print('Calculating')
        #make tree
        t0_dm_kd_tree = cKDTree(PosDM_ICs, leafsize=10, boxsize=BOX_SIZE)
        if VERBOSE: print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(t0_dm_kd_tree, dm_tree_file, protocol=4)
        if VERBOSE: print('Saved dm kdtree.')
    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    if VERBOSE: print('Done t=0 kdtree')

    #REMOVE DM TREE FOR MEMORY REASONS
    del t0_dm_kd_tree

    #t=N kdtree
    dm_tree_file_name = f'{kdtree_location}{snap}_dm_tree.pickle'
    start_time = time.time()
    if VERBOSE: print('DM tree file name ', dm_tree_file_name)

    if os.path.exists(dm_tree_file_name):
        if VERBOSE: print('Loading')
        with open(dm_tree_file_name, 'rb') as dm_tree_file:
            tn_dm_kd_tree = pickle.load(dm_tree_file)
        print('Opened dm kdtree')

    else:
        if VERBOSE: print('Calculating')
        #load positions in EAGLE units
        PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
        NDM_c = 0
        for ifile in range(FNumPerSnap):
            fn = f'{particle_data_location}particledata_{snap}/eagle_subfind_particles_{snap}.{str(ifile)}.hdf5'
            with h5.File(fn, "r") as fs:
                Header = fs['Header'].attrs
                NumParts = Header['NumPart_ThisFile']
                if NumParts[1] > 0:
                    PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
                    NDM_c += NumParts[1]
        #make tree
        tn_dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
        if VERBOSE: print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
        #save
        with open(dm_tree_file_name, 'wb') as dm_tree_file:
            pickle.dump(tn_dm_kd_tree, dm_tree_file, protocol=4)
        if VERBOSE: print('Saved dm kdtree.')
    if VERBOSE: print('in ' + str(np.round(time.time() - start_time, 1)) + 's')

    if VERBOSE: print('Done t=N kdtree')

    #REMOVE DM TREE FOR MEMORY REASONS
    del tn_dm_kd_tree

    if VERBOSE: print('loaded eveything')

    return (TotNgroups, TotNsubgroups,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential, tn_dm_kd_tree, t0_dm_kd_tree,
            IDDM, GrpNum_DM, SubNum_DM, BindingEnergyDM, PosDM_ICs)


def decode_peano_ids(ids, bits, BOX_SIZE=BOX_SIZE, so_loc=PEANO_SO_LOC):

    n = len(ids)

    key = (ctypes.c_uint64 * n)(*ids)

    c_peano_hilbert_key_inverse = ctypes.CDLL(f'{so_loc}peano_for_py.so').peano_hilbert_key_inverse_

    x_array = (ctypes.c_int64 * n)(*[0] * n)
    y_array = (ctypes.c_int64 * n)(*[0] * n)
    z_array = (ctypes.c_int64 * n)(*[0] * n)

    c_peano_hilbert_key_inverse(ctypes.byref(key), ctypes.c_int64(bits),
                                ctypes.byref(x_array), ctypes.byref(y_array), ctypes.byref(z_array),
                                ctypes.c_int64(n))

    x_np = np.array(x_array)
    y_np = np.array(y_array)
    z_np = np.array(z_array)

    pos_ics = np.vstack((x_np, y_np, z_np)).T / (2 ** bits) * BOX_SIZE

    pos_ics = pos_ics.astype(np.float32)

    return pos_ics


def do_everything(particle_data_location0, halo_data_location0, kdtree_location0,
                  particle_data_location1, halo_data_location1, kdtree_location1,
                  snap, output_data_location):

    start_time = time.time()
    if '7x' in particle_data_location0: bits = 21
    else: bits = 14
    my_read(particle_data_location0, halo_data_location0, kdtree_location0, snap, bits)

    if '7x' in particle_data_location1: bits = 21
    else: bits = 14
    my_read(particle_data_location1, halo_data_location1, kdtree_location1, snap, bits)
    print(str(np.round(time.time() - start_time, 1)))

    return


if __name__ == '__main__':
    _, snap_index = sys.argv

    snap_index = int(snap_index)

    #hyades
    particle_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                                   '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                               '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/']
    output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
                                 '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    #ozstar
    # particle_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                                '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # halo_data_location_list = ['/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/',
    #                            '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/']
    # output_data_location_list = ['/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/']

    particle_data_location0 = particle_data_location_list[snap_index // 4]
    particle_data_location1 = particle_data_location_list[1 + (snap_index // 4)]
    halo_data_location0 = halo_data_location_list[snap_index // 4]
    halo_data_location1 = halo_data_location_list[1 + (snap_index // 4)]
    output_data_location0 =  output_data_location_list[snap_index // 4]
    output_data_location1 =  output_data_location_list[1 + (snap_index // 4)]
    kdtree_location0 = output_data_location0
    kdtree_location1 = output_data_location1
    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index % 4]
    SCALE_A_list = [1, 1 / (1 + 0.503), 1 / (1 + 1.004), 1 / (1 + 2.012)]
    SCALE_A = SCALE_A_list[snap_index % 4]

    do_everything(particle_data_location1, halo_data_location1, kdtree_location1,
                  particle_data_location0, halo_data_location0, kdtree_location0,
                  snap, output_data_location0)

    if VERBOSE: print('Done all')

    pass
