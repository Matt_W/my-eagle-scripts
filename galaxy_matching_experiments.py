#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

from functools import lru_cache, wraps

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d

from scipy.integrate   import quad

from scipy.optimize    import minimize
from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.spatial.transform import Rotation
from scipy.spatial.distance import cdist
from scipy.special import kn, iv
from scipy.special import erf
from scipy.special import lambertw
from scipy.special import spence #aka dilogarithm

from scipy.stats import binned_statistic
import scipy.stats

import warnings

import matplotlib
# matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt

import matplotlib.lines as lines
import matplotlib.patheffects as path_effects

from sphviewer.tools import QuickView
from sphviewer.tools import Blend

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 12#20
rcParams['xtick.labelsize'] = 12#20
rcParams['ytick.labelsize'] = 12#20
rcParams['axes.labelsize'] = 12#22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

from morphology_hdf_keys import *
import dm_run_plot_scripts

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun / h

n_jzjc_bins = 21

NO_GROUP_NUMBER = 1073741824 #2**30

SCALE_A = 1
BOX_SIZE = 33.885 #50 * 0.6777 #kpc

# The custom handler class
from matplotlib.legend_handler import HandlerTuple
class HandlerTupleVertical(HandlerTuple):
    def __init__(self, **kwargs):
        HandlerTuple.__init__(self, **kwargs)
        # HandlerLine2D.__init__(self, **kwargs)

    def create_artists(self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans):
        # How many lines are there.
        numlines = len(orig_handle)
        handler_map = legend.get_legend_handler_map()

        # divide the vertical space where the lines will go
        # into equal parts based on the number of lines
        height_y = (height / numlines)

        leglines = []
        for i, handle in enumerate(orig_handle):
            handler = legend.get_legend_handler(handler_map, handle)

            legline = handler.create_artists(legend, handle, xdescent, (2 * i + 1) * height_y, width, 2 * height,
                                             fontsize,
                                             trans)
            leglines.extend(legline)

        return leglines


# TODO this needs to be sped up!
def pacman_dist2(u, v):
    dx = np.abs(u[0] - v[0])
    dy = np.abs(u[1] - v[1])
    dz = np.abs(u[2] - v[2])

    if dx > 16.9425: dx = 33.885 - dx
    if dy > 16.9425: dy = 33.885 - dy
    if dz > 16.9425: dz = 33.885 - dz

    return dx * dx + dy * dy + dz * dz


def vec_pacman_dist(u, v):
    dx = np.abs(u[:, 0] - v[:, 0])
    dy = np.abs(u[:, 1] - v[:, 1])
    dz = np.abs(u[:, 2] - v[:, 2])

    dx[dx > 16.9425] = 33.885 - dx[dx > 16.9425]
    dy[dy > 16.9425] = 33.885 - dy[dy > 16.9425]
    dz[dz > 16.9425] = 33.885 - dz[dz > 16.9425]

    return np.sqrt(dx * dx + dy * dy + dz * dz)


def vec_pacman_dist2(u, v):
    dx = np.abs(u[:, 0] - v[:, 0])
    dy = np.abs(u[:, 1] - v[:, 1])
    dz = np.abs(u[:, 2] - v[:, 2])

    dx[dx > 16.9425] = 33.885 - dx[dx > 16.9425]
    dy[dy > 16.9425] = 33.885 - dy[dy > 16.9425]
    dz[dz > 16.9425] = 33.885 - dz[dz > 16.9425]

    return dx * dx + dy * dy + dz * dz


def my_bootstrap(data, n=1000, statistic=np.median, quantiles=[0.16, 0.50, 0.84]):
    estimates = np.zeros(n)
    ld = len(data)
    for i in range(n):
        estimates[i] = statistic(data[np.random.randint(0, ld, ld)])

    return [np.quantile(estimates, q) for q in quantiles]


def align(pos, vel, mass, apature=30):
    """Aligns the z cartesian direction with the direction of angular momentum.
    Can use an apature.
    """
    # direction to align
    if apature is not None:
        r = np.linalg.norm(pos, axis=1)
        mask = r < apature

        j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

    else:
        j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

    # find rotation
    rotation = find_rotaion_matrix(j_tot)

    # rotate stars
    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    return pos, vel, rotation


def find_rotaion_matrix(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0
    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)

    if j_tot[2] < 0:
        x += 180

    return Rotation.from_euler('yx', [y, x], degrees=True)


#TODO rlu
def old_match_haloes(M200_0, cops_0, M200_1, cops_1):

    a = 1
    b = 1
    c = 10
    d = 1

    # memory issues
    # pair_distances = cdist(cops_0, cops_1)

    # start_time = time.time()
    # pair_distances = cdist(cops_0, cops_1, pacman_dist2)
    # pair_distances = np.sqrt(pair_distances)
    # end_time = time.time()
    # print(f'cdist time {end_time - start_time}')

    # start_time = time.time()
    pair_distances = np.zeros((np.shape(cops_0)[0], np.shape(cops_1)[0]))
    for i in range(np.shape(cops_1)[0]):
        pair_distances[:, i] = vec_pacman_dist2(cops_0, np.array([cops_1[i, :]]))
    # end_time = time.time()
    # print(f'my_dist time {end_time - start_time}')

    log_mass_distance = cdist(np.reshape(np.log10(M200_0), (len(M200_0), 1)),
                              np.reshape(np.log10(M200_1), (len(M200_1), 1)))
    pair_distances = np.power(c * pair_distances, a) + np.power(d * log_mass_distance, b)

    del log_mass_distance

    best_0 = np.nanargmin(pair_distances, axis=1)
    best_1 = np.nanargmin(pair_distances, axis=0)

    del pair_distances

    match_0 = np.array([best_1[i] == j for j, i in enumerate(best_0)])
    match_1 = np.array([best_0[i] == j for j, i in enumerate(best_1)])

    best_aug_1 = best_1[match_1]
    for i in range(len(best_aug_1)):
        while i not in best_aug_1:
            best_aug_1[best_aug_1 > i] -= 1
    best_aug_1 = np.argsort(best_aug_1)

    return match_0, match_1, best_0, best_1, best_aug_1


def cache(fun):
    cache.cache_ = {}

    def inner(*args, **kwargs):
        if str(args) not in cache.cache_:
            # print(f'Caching {str(args)}')  # to check when it is cached
            cache.cache_[str(args)] = fun(*args, **kwargs)
        return cache.cache_[str(args)]
    return inner


# @lru_cache()
@cache
def load_particle_matched_haloes(data0, data1, matches, mask_0=None, mask_1=None, diagnostics=False, VERBOSE=False,
                                 N_match=0):
    # 0 is 7x
    # 1 is 1x

    gn_0 = data0['GalaxyQuantities/GroupNumber'][mask_0].astype(np.int64)
    gn_1 = data1['GalaxyQuantities/GroupNumber'][mask_1].astype(np.int64)
    sn_0 = data0['GalaxyQuantities/SubGroupNumber'][mask_0].astype(np.int64)
    sn_1 = data1['GalaxyQuantities/SubGroupNumber'][mask_1].astype(np.int64)

    gn_0_pm = matches['GroupNumbers/Zero'][()].astype(np.int64)
    gn_1_pm = matches['GroupNumbers/One'][()].astype(np.int64)
    sn_0_pm = matches['SubGroupNumbers/Zero'][()].astype(np.int64)
    sn_1_pm = matches['SubGroupNumbers/One'][()].astype(np.int64)

    const = 10_000_000

    n_0 = const * gn_0 + sn_0
    n_1 = const * gn_1 + sn_1

    pm_0 = const * gn_0_pm + sn_0_pm
    pm_1 = const * gn_1_pm + sn_1_pm

    full_mask_0 = np.in1d(pm_0, n_0)
    full_mask_1 = np.isin(pm_1, n_1)

    best_group_0 = matches['MatchedHaloes/OneToZero'][full_mask_0].astype(np.int64)
    best_group_1 = matches['MatchedHaloes/ZeroToOne'][full_mask_1].astype(np.int64)

    best_sub_0 = matches['MatchedSubHaloes/OneToZero'][full_mask_0].astype(np.int64)
    best_sub_1 = matches['MatchedSubHaloes/ZeroToOne'][full_mask_1].astype(np.int64)

    N_group_0 = matches['NParticlesMatchedToGroup/OneToZero'][full_mask_0]
    N_group_1 = matches['NParticlesMatchedToGroup/ZeroToOne'][full_mask_1]

    N_sub_0 = matches['NParticlesMatchedToSub/OneToZero'][full_mask_0]
    N_sub_1 = matches['NParticlesMatchedToSub/ZeroToOne'][full_mask_1]

    best_0 = const * best_group_0 + best_sub_0
    best_1 = const * best_group_1 + best_sub_1

    in_0 = np.isin(best_0, n_1)
    in_1 = np.isin(best_1, n_0)

    if VERBOSE: print('started fixing 0')
    best_0[np.logical_not(in_0)] = NO_GROUP_NUMBER
    # fix group numbers
    for i0, g0 in enumerate(best_0):
        if VERBOSE:
            if not i0 % 100_000:
                print(f'{i0} / {len(best_0)}')

        if not in_0[i0]:
            continue

        if N_sub_0[i0] < N_match:
            continue

        m01 = g0 == n_1
        nz01 = np.nonzero(m01)[0]

        if len(nz01) > 0:
            best_0[i0] = nz01
        else:
            best_0[i0] = NO_GROUP_NUMBER

    best_0[best_0 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started fixing 1')
    best_1[np.logical_not(in_1)] = NO_GROUP_NUMBER
    # fix group numbers
    for i1, g1 in enumerate(best_1):
        if VERBOSE:
            if not i1 % 10_000:
                print(f'{i1} / {len(best_1)}')

        if not in_1[i1]:
            continue

        if N_sub_1[i1] < N_match:
            continue

        m10 = g1 == n_0
        nz10 = np.nonzero(m10)[0]

        if len(nz10) > 0:
            best_1[i1] = nz10
        else:
            best_1[i1] = NO_GROUP_NUMBER

    best_1[best_1 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started matching 0')
    match_0 = np.zeros(len(n_0), dtype=bool)
    for j, i in enumerate(best_0):
        if i < len(best_1):
            if best_1[i] == j:
                match_0[j] = True

    if VERBOSE: print('started matching 1')
    match_1 = np.zeros(len(n_1), dtype=bool)
    for j, i in enumerate(best_1):
        if i < len(best_0):
            if best_0[i] == j:
                match_1[j] = True

    if VERBOSE: print('started best aug')
    best_aug_1 = np.argsort(best_1[match_1])

    #centrals
    #centrals
    best_central_0 = const * best_group_0 #+ best_sub_0
    best_central_1 = const * best_group_1 #+ best_sub_1

    n_0 = const * gn_0 #+ sn_0
    n_1 = const * gn_1 #+ sn_1

    in_0 = sn_0 == 0
    in_1 = sn_1 == 0

    if VERBOSE: print('started fixing central 0')
    best_central_0[np.logical_not(in_0)] = NO_GROUP_NUMBER
    # fix group numbers
    for i0, g0 in enumerate(best_central_0):
        if VERBOSE:
            if not i0 % 100_000:
                print(f'{i0} / {len(best_central_0)}')

        if not in_0[i0]:
            continue

        if N_group_0[i0] < N_match:
            continue

        m01 = g0 == n_1
        nz01 = np.nonzero(m01)[0]

        if len(nz01) > 1:
            best_central_0[i0] = nz01[0]
        elif len(nz01) > 0:
            best_central_0[i0] = nz01
        else:
            best_central_0[i0] = NO_GROUP_NUMBER

    best_central_0[best_central_0 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started fixing central 1')
    best_central_1[np.logical_not(in_1)] = NO_GROUP_NUMBER
    # fix group numbers
    for i1, g1 in enumerate(best_central_1):
        if VERBOSE:
            if not i1 % 10_000:
                print(f'{i1} / {len(best_central_1)}')

        if not in_1[i1]:
            continue

        if N_group_1[i1] < N_match:
            continue

        m10 = g1 == n_0
        nz10 = np.nonzero(m10)[0]

        if len(nz10) > 1:
            best_central_1[i1] = nz10[0]
        elif len(nz10) > 0:
            best_central_1[i1] = nz10
        else:
            best_central_1[i1] = NO_GROUP_NUMBER

    best_central_1[best_central_1 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

    if VERBOSE: print('started matching central 0')
    match_central_0 = np.zeros(len(n_0), dtype=bool)
    for j, i in enumerate(best_central_0):
        if i < len(best_central_1):
            if best_central_1[i] == j:
                match_central_0[j] = True

    if VERBOSE: print('started matching central 1')
    match_central_1 = np.zeros(len(n_1), dtype=bool)
    for j, i in enumerate(best_central_1):
        if i < len(best_central_0):
            if best_central_0[i] == j:
                match_central_1[j] = True

    if VERBOSE: print('started best aug central')
    best_aug_central_1 = np.argsort(best_central_1[match_central_1])

    if diagnostics:
        pass

    return (match_0, match_1, best_0, best_1, best_aug_1,
            match_central_0, match_central_1, best_central_0, best_central_1, best_aug_central_1)


#only for galaxies with stars
def full_match_stats(data0, data1, matches, VERBOSE=True,
                     save=True, name=None, outfile=None, calculate=False):
    # 0 is 7x
    # 1 is 1x

    if calculate or outfile == None:

        N_match = 25 #50
        N_min = 1000 #100

        print(f'N match = {N_match}')
        print(f'N_dm min = {N_min}')

        mdm_0 = data0['SubGroup/MassType1'][()]
        mdm_1 = data1['SubGroup/MassType1'][()]

        gn_0_pm = matches['GroupNumbers/Zero'][()].astype(np.int64)
        gn_1_pm = matches['GroupNumbers/One'][()].astype(np.int64)
        sn_0_pm = matches['SubGroupNumbers/Zero'][()].astype(np.int64)
        sn_1_pm = matches['SubGroupNumbers/One'][()].astype(np.int64)

        full_mask_0 = mdm_0 > N_min * DM_MASS / LITTLE_H #/ 7
        full_mask_1 = mdm_1 > N_min * DM_MASS / LITTLE_H

        gn_0 = data0['SubGroup/GroupNumber'][full_mask_0].astype(np.int64)
        gn_1 = data1['SubGroup/GroupNumber'][full_mask_1].astype(np.int64)
        sn_0 = data0['SubGroup/SubgroupNumber'][full_mask_0].astype(np.int64)
        sn_1 = data1['SubGroup/SubgroupNumber'][full_mask_1].astype(np.int64)

        best_group_0 = matches['MatchedHaloes/OneToZero'][full_mask_0].astype(np.int64)
        best_group_1 = matches['MatchedHaloes/ZeroToOne'][full_mask_1].astype(np.int64)

        best_sub_0 = matches['MatchedSubHaloes/OneToZero'][full_mask_0].astype(np.int64)
        best_sub_1 = matches['MatchedSubHaloes/ZeroToOne'][full_mask_1].astype(np.int64)

        N_group_0 = matches['NParticlesMatchedToGroup/OneToZero'][full_mask_0]
        N_group_1 = matches['NParticlesMatchedToGroup/ZeroToOne'][full_mask_1]

        N_sub_0 = matches['NParticlesMatchedToSub/OneToZero'][full_mask_0]
        N_sub_1 = matches['NParticlesMatchedToSub/ZeroToOne'][full_mask_1]

        const = 10_000_000

        n_0 = const * gn_0 + sn_0
        n_1 = const * gn_1 + sn_1

        pm_0 = const * gn_0_pm + sn_0_pm
        pm_1 = const * gn_1_pm + sn_1_pm

        best_0 = const * best_group_0 + best_sub_0
        best_1 = const * best_group_1 + best_sub_1

        in_0 = np.isin(best_0, n_1)
        in_1 = np.isin(best_1, n_0)

        if VERBOSE: print('started fixing 0')
        # best_0[np.logical_not(in_0)] = NO_GROUP_NUMBER
        # fix group numbers
        for i0, g0 in enumerate(best_0):
            if not i0 % 100_000:
                if VERBOSE: print(f'{i0} / {len(best_0)}')

            if not in_0[i0]:
                best_0[i0] = NO_GROUP_NUMBER

            if N_sub_0[i0] < N_match:
                best_0[i0] = NO_GROUP_NUMBER

            m01 = g0 == n_1
            nz01 = np.nonzero(m01)[0]

            if len(nz01) > 0:
                best_0[i0] = nz01
            else:
                best_0[i0] = NO_GROUP_NUMBER

        best_0[best_0 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

        if VERBOSE: print('started fixing 1')
        # best_1[np.logical_not(in_1)] = NO_GROUP_NUMBER
        # fix group numbers
        for i1, g1 in enumerate(best_1):
            if not i1 % 10_000:
                if VERBOSE: print(f'{i1} / {len(best_1)}')

            if not in_1[i1]:
                best_1[i1] = NO_GROUP_NUMBER

            if N_sub_1[i1] < N_match:
                best_1[i1] = NO_GROUP_NUMBER

            m10 = g1 == n_0
            nz10 = np.nonzero(m10)[0]

            if len(nz10) > 0:
                best_1[i1] = nz10
            else:
                best_1[i1] = NO_GROUP_NUMBER

        best_1[best_1 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

        if VERBOSE: print('started matching 0')
        match_0 = np.zeros(len(n_0), dtype=bool)
        for j, i in enumerate(best_0):
            if i < len(best_1):
                if best_1[i] == j:
                    match_0[j] = True
                    continue

        if VERBOSE: print('started matching 1')
        match_1 = np.zeros(len(n_1), dtype=bool)
        for j, i in enumerate(best_1):
            if i < len(best_0):
                if best_0[i] == j:
                    match_1[j] = True
                    continue

        if VERBOSE: print('started best aug')
        best_aug_1 = np.argsort(best_1[match_1])

        best_aug_0 = np.argsort(best_0[match_0])

        #centrals
        #centrals
        best_central_0 = const * best_group_0 #+ best_sub_0
        best_central_1 = const * best_group_1 #+ best_sub_1

        n_0 = const * gn_0 #+ sn_0
        n_1 = const * gn_1 #+ sn_1

        in_0 = sn_0 == 0
        in_1 = sn_1 == 0

        if VERBOSE: print('started fixing central 0')
        # best_central_0[np.logical_not(in_0)] = NO_GROUP_NUMBER
        # fix group numbers
        for i0, g0 in enumerate(best_central_0):
            if not i0 % 100_000:
                if VERBOSE: print(f'{i0} / {len(best_central_0)}')

            if not in_0[i0]:
                best_central_0[i0] = NO_GROUP_NUMBER

            if N_group_0[i0] < N_match:
                best_central_0[i0] = NO_GROUP_NUMBER

            m01 = g0 == n_1
            nz01 = np.nonzero(m01)[0]

            if len(nz01) > 1:
                best_central_0[i0] = nz01[0]
            elif len(nz01) > 0:
                best_central_0[i0] = nz01
            else:
                best_central_0[i0] = NO_GROUP_NUMBER

        best_central_0[best_central_0 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

        if VERBOSE: print('started fixing central 1')
        # best_central_1[np.logical_not(in_1)] = NO_GROUP_NUMBER
        # fix group numbers
        for i1, g1 in enumerate(best_central_1):
            if not i1 % 10_000:
                if VERBOSE: print(f'{i1} / {len(best_central_1)}')

            if not in_1[i1]:
                best_central_1[i1] = NO_GROUP_NUMBER

            if N_group_1[i1] < N_match:
                best_central_1[i1] = NO_GROUP_NUMBER

            m10 = g1 == n_0
            nz10 = np.nonzero(m10)[0]

            if len(nz10) > 1:
                best_central_1[i1] = nz10[0]
            elif len(nz10) > 0:
                best_central_1[i1] = nz10
            else:
                best_central_1[i1] = NO_GROUP_NUMBER

        best_central_1[best_central_1 > NO_GROUP_NUMBER - 1] = NO_GROUP_NUMBER

        if VERBOSE: print('started matching central 0')
        match_central_0 = np.zeros(len(n_0), dtype=bool)
        for j, i in enumerate(best_central_0):
            if i < len(best_central_1):
                if best_central_1[i] == j:
                    match_central_0[j] = True
                    continue

        if VERBOSE: print('started matching central 1')
        match_central_1 = np.zeros(len(n_1), dtype=bool)
        for j, i in enumerate(best_central_1):
            if i < len(best_central_0):
                if best_central_0[i] == j:
                    match_central_1[j] = True
                    continue

        if VERBOSE: print('started best aug central')
        best_aug_central_1 = np.argsort(best_central_1[match_central_1])

        # print(np.sum(match_central_1))
        # print(np.amax(best_aug_central_1))
        # print(len(best_aug_central_1))
        # print(len(np.unique(best_aug_central_1)))

        #save
        if outfile is not None and N_min == 0:
            if 'sub' in outfile:
                print('Output file ', outfile)
                with h5.File(outfile, 'w') as output:

                    group = output.create_group('SubGroupBijectiveMatches')

                    group.create_dataset('MatchedGroupNumber7x', data=gn_0[match_0])
                    group.create_dataset('MatchedGroupNumber1x', data=gn_1[match_1][best_aug_1])
                    group.create_dataset('MatchedSubGroupNumber7x', data=sn_0[match_0])
                    group.create_dataset('MatchedSubGroupNumber1x', data=sn_1[match_1][best_aug_1])

            else:
                print('Output file ', outfile)
                with h5.File(outfile, 'w') as output:

                    group = output.create_group('GroupBijectiveMatches')

                    group.create_dataset('MatchedGroupNumber7x', data = gn_0[match_central_0])
                    group.create_dataset('MatchedGroupNumber1x', data = gn_1[match_central_1][best_aug_central_1])

    else:
        mdm_0 = data0['SubGroup/MassType1'][()]
        mdm_1 = data1['SubGroup/MassType1'][()]

        M200_0 = data0['Group/Group_M_Crit200'][()]
        M200_1 = data1['Group/Group_M_Crit200'][()]

        # gn_0_pm = matches['GroupNumbers/Zero'][()].astype(np.int64)
        # gn_1_pm = matches['GroupNumbers/One'][()].astype(np.int64)
        # sn_0_pm = matches['SubGroupNumbers/Zero'][()].astype(np.int64)
        # sn_1_pm = matches['SubGroupNumbers/One'][()].astype(np.int64)

        gn_0 = np.arange(0, len(M200_0), dtype=np.int64) #data0['Group/GroupNumber'][()].astype(np.int64)
        gn_1 = np.arange(0, len(M200_1), dtype=np.int64) #data1['Group/GroupNumber'][()].astype(np.int64)

        sub_gn_0 = data0['SubGroup/GroupNumber'][()].astype(np.int64)
        sub_gn_1 = data1['SubGroup/GroupNumber'][()].astype(np.int64)
        sub_sn_0 = data0['SubGroup/SubgroupNumber'][()].astype(np.int64)
        sub_sn_1 = data1['SubGroup/SubgroupNumber'][()].astype(np.int64)

        # best_group_0 = matches['MatchedHaloes/OneToZero'][()].astype(np.int64)
        # best_group_1 = matches['MatchedHaloes/ZeroToOne'][()].astype(np.int64)
        #
        # best_sub_0 = matches['MatchedSubHaloes/OneToZero'][()].astype(np.int64)
        # best_sub_1 = matches['MatchedSubHaloes/ZeroToOne'][()].astype(np.int64)
        #
        # N_group_0 = matches['NParticlesMatchedToGroup/OneToZero'][()]
        # N_group_1 = matches['NParticlesMatchedToGroup/ZeroToOne'][()]
        #
        # N_sub_0 = matches['NParticlesMatchedToSub/OneToZero'][()]
        # N_sub_1 = matches['NParticlesMatchedToSub/ZeroToOne'][()]

        if 'sub' in outfile:
            print('Output file ', outfile)
            with h5.File(outfile, 'r') as output:

                sub_matched_gn_0 = output['SubGroupBijectiveMatches/MatchedGroupNumber7x'][()]
                sub_matched_gn_1 = output['SubGroupBijectiveMatches/MatchedGroupNumber1x'][()]
                sub_matched_sn_0 = output['SubGroupBijectiveMatches/MatchedSubGroupNumber7x'][()]
                sub_matched_sn_1 = output['SubGroupBijectiveMatches/MatchedSubGroupNumber1x'][()]

        else:
            print('Output file ', outfile)
            with h5.File(outfile, 'r') as output:

                matched_gn_0 = output['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
                matched_gn_1 = output['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

    cut = 1 #* LITTLE_H #10^10 M_sun

    N_M200_0 = np.sum(M200_0 / LITTLE_H > cut)
    N_M200_1 = np.sum(M200_1 / LITTLE_H > cut)

    Nm_M200_0 = np.sum(M200_0[matched_gn_0] / LITTLE_H > cut)
    Nm_M200_1 = np.sum(M200_1[matched_gn_1] / LITTLE_H > cut)

    print(Nm_M200_0, N_M200_0, Nm_M200_0 / N_M200_0)
    print(Nm_M200_1, N_M200_1, Nm_M200_1 / N_M200_1)
    print(Nm_M200_0 + Nm_M200_1, N_M200_0 + Nm_M200_1, (Nm_M200_0 + Nm_M200_1) / (N_M200_0 + N_M200_1))
    print()

    if VERBOSE: print('started diagnostics')



    # # if False:
    # if outfile is not None:
    #     print('Output file ', outfile)
    #     with h5.File(outfile, 'r') as output:
    #         gn_out0 = output['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
    #         gn_out1 = output['GroupBijectiveMatches/MatchedGroupNumber1x'][()]
    #
    #     try:
    #         cops_0 = data0['SubGroup/CentreOfPotential'][()]  # Mpc/h
    #         cops_1 = data1['SubGroup/CentreOfPotential'][()]  # Mpc/h
    #         cops_0 = cops_0[full_mask_0]
    #         cops_1 = cops_1[full_mask_1]
    #         for i in [2**n for n in range(20)]:
    #             g0_mask = np.logical_and(gn_0 == gn_out0[i], sn_0 == 0)
    #             g1_mask = np.logical_and(gn_1 == gn_out1[i], sn_1 == 0)
    #             print(i, gn_out0[i], gn_out1[i], np.nonzero(g0_mask), np.nonzero(g1_mask))
    #             print(cops_0[g0_mask])
    #             print(cops_1[g1_mask])
    #             print()
    #     except Exception as e:
    #         print('tried to thow exception:', e)




    # full_mask_0 = ()
    # full_mask_1 = ()

    # mstar_0 = data0['SubGroup/MassType4'][full_mask_0] # 10^10 M_sun/h
    # mstar_1 = data1['SubGroup/MassType4'][full_mask_1] #[mask_1[sn_1 == 0]] # 10^10 M_sun/h
    # R200_0 = np.ones(len(mstar_0))
    # R200_1 = np.ones(len(mstar_1))
    #
    # cops_0 = data0['SubGroup/CentreOfPotential'][()] # Mpc/h
    # cops_1 = data1['SubGroup/CentreOfPotential'][()] # Mpc/h
    # cops_0 = cops_0[full_mask_0]
    # cops_1 = cops_1[full_mask_1]
    #
    # dcop = vec_pacman_dist(cops_0[match_0], cops_1[match_1][best_aug_1])
    #
    # plot_positions(cops_0, cops_1, mstar_0, mstar_1, R200_0, R200_1,
    #                match_0, match_1,
    #                best_0, best_1,
    #                best_aug_1, dcop, save)

    # gn_all = np.arange(len(data0['Group/Group_M_Crit200'][()])) + 1
    # print(np.setdiff1d(gn_all, gn_0_pm[sn_0_pm == 0]))

    # central_indices_0 = np.nonzero(sn_0 == 0)[0]
    # central_indices_1 = np.nonzero(sn_1 == 0)[0]
    #
    # M200_0_pm = data0['Group/Group_M_Crit200'][()]
    # M200_1_pm = data1['Group/Group_M_Crit200'][()]
    #
    # M200_pm_mask_0 = np.isin(np.arange(len(M200_0_pm)), gn_0[sn_0 == 0]-1, assume_unique=True)
    # M200_pm_mask_1 = np.isin(np.arange(len(M200_1_pm)), gn_1[sn_1 == 0]-1, assume_unique=True)
    #
    # M200_0 = np.zeros(len(gn_0), dtype=np.float32)
    # M200_1 = np.zeros(len(gn_1), dtype=np.float32)
    # R200_0 = np.zeros(len(gn_0), dtype=np.float32)
    # R200_1 = np.zeros(len(gn_1), dtype=np.float32)
    #
    # group_cops_0 = np.nan * np.ones((len(gn_0), 3), dtype=np.float32)
    # group_cops_1 = np.nan * np.ones((len(gn_1), 3), dtype=np.float32)
    #
    # R200_0[central_indices_0] = data0['Group/Group_R_Crit200'][M200_pm_mask_0]
    # R200_1[central_indices_1] = data1['Group/Group_R_Crit200'][M200_pm_mask_1]
    #
    # mdm_0 = np.zeros(len(gn_0), dtype=np.float32)
    # mdm_1 = np.zeros(len(gn_1), dtype=np.float32)
    # mdm_0_pm = data0['SubGroup/MassType1'][()] # 10^10 M_sun/h
    # mdm_1_pm = data1['SubGroup/MassType1'][()] # 10^10 M_sun/h
    # mdm_0[central_indices_0] = mdm_0_pm[full_mask_0][central_indices_0]
    # mdm_1[central_indices_1] = mdm_1_pm[full_mask_1][central_indices_1]
    #
    # cops_0_pm = data0['Group/GroupCentreOfPotential'][()]
    # cops_1_pm = data1['Group/GroupCentreOfPotential'][()]
    #
    # group_cops_0[central_indices_0] = cops_0_pm[M200_pm_mask_0, :]
    # group_cops_1[central_indices_1] = cops_1_pm[M200_pm_mask_1, :]
    #
    # dcop = vec_pacman_dist(group_cops_0[match_central_0], group_cops_1[match_central_1][best_aug_central_1])
    #
    # # print('start plot')
    # # plot_match_diagnostics(M200_0, M200_1, R200_0, R200_1, mstar_0, mstar_1,
    # #                        match_central_0, match_central_1, best_aug_central_1,
    # #                        dcop, save, name, N_min, N_match)
    #
    # # plot_match_diagnostics(mdm_0, mdm_1, R200_0, R200_1, mstar_0, mstar_1,
    # #                        match_central_0, match_central_1, best_aug_central_1,
    # #                        dcop, save, name, N_min, N_match, mdm=True)
    #
    # sub_cops_0 = data0['SubGroup/CentreOfPotential'][()] # Mpc/h
    # sub_cops_1 = data1['SubGroup/CentreOfPotential'][()] # Mpc/h
    # sub_cops_0 = sub_cops_0[full_mask_0]
    # sub_cops_1 = sub_cops_1[full_mask_1]

    #

    # dcop = vec_pacman_dist(group_cops_0[match_0], group_cops_1[match_1][best_aug_1])

    # plot_match_diagnostics(mdm_0, mdm_1, R200_0, R200_1, mstar_0, mstar_1,
    #                        match_0, match_1, best_aug_1,
    #                        dcop, save, name, N_min, N_match, mdm=True)

    # M200_0 = data0['Group/Group_M_Crit200'][()]
    # M200_1 = data1['Group/Group_M_Crit200'][()]
    #
    # M200_0 = np.zeros(len(gn_0), dtype=np.float32)
    # M200_1 = np.zeros(len(gn_1), dtype=np.float32)
    # M200_0[central_indices_0] = data0['Group/Group_M_Crit200'][M200_pm_mask_0]
    # M200_1[central_indices_1] = data1['Group/Group_M_Crit200'][M200_pm_mask_1]
    #
    # # mdm_0 = data0['SubGroup/MassType1'][()] # 10^10 M_sun/h
    # # mdm_1 = data1['SubGroup/MassType1'][()] # 10^10 M_sun/h
    #
    # mstar_0 = data0['SubGroup/MassType4'][full_mask_0] # 10^10 M_sun/h
    # mstar_1 = data1['SubGroup/MassType4'][full_mask_1] # 10^10 M_sun/h
    #
    # gn_0 = data0['SubGroup/GroupNumber'][full_mask_0].astype(np.int64)
    # gn_1 = data1['SubGroup/GroupNumber'][full_mask_1].astype(np.int64)
    # sn_0 = data0['SubGroup/SubgroupNumber'][full_mask_0].astype(np.int64)
    # sn_1 = data1['SubGroup/SubgroupNumber'][full_mask_1].astype(np.int64)
    #
    # plot_match_diagnostics_2(M200_0, M200_1, mstar_0, mstar_1,
    #                          gn_0, gn_1, sn_0, sn_1,
    #                          match_central_0, match_central_1, best_aug_central_1,
    #                          match_0, match_1, best_aug_1, best_aug_0,
    #                          save, name, N_min, N_match)

    # plot_positions(group_cops_0, group_cops_1, M200_0, M200_1, R200_0, R200_1,
    #                match_central_0, match_central_1,
    #                best_central_0, best_central_1,
    #                best_aug_central_1, dcop, save,
    #                name=name, N_lim=N_min, N_match=N_match)

    # cops_0 = data0['GalaxyQuantities/SubGroupCOP'][mask_0, :]  # Mpc/h
    # cops_1 = data1['GalaxyQuantities/SubGroupCOP'][mask_1, :]  # Mpc/h
    # dcop = vec_pacman_dist(cops_0[match_0], cops_1[match_1][best_aug_1])

    # plot_positions(cops_0, cops_1, M200_0, M200_1, R200_0, R200_1,
    #                match_0, match_1, best_0, best_1, best_aug_1, dcop, save)

    # [better_plot_positions(group_cops_0, group_cops_1,
    #                        M200_0, M200_1,
    #                        R200_0, R200_1,
    #                        match_central_0,match_central_1,best_central_0,best_central_1,best_aug_central_1,
    #                        sub_cops_0, sub_cops_1,
    #                        mstar_0, mstar_1,
    #                        match_0, match_1, best_0, best_1, best_aug_1,
    #                        i, save, name, N_min, N_match) for i in [0, 1, 2, 4, 8, 16, 32, 64, 128, 256]]

    # better_plot_positions(group_cops_0, group_cops_1,
    #                       M200_0, M200_1,
    #                       R200_0, R200_1,
    #                       match_central_0,match_central_1,best_central_0,best_central_1,best_aug_central_1,
    #                       sub_cops_0, sub_cops_1,
    #                       mstar_0, mstar_1,
    #                       match_0, match_1, best_0, best_1, best_aug_1,
    #                       256, save, name, N_min, N_match)

    return


#TODO dictionary arguments
def experiment_match_haloes(filename_0, filename_1, matchfile,
                            xkey, ykey, rkey, comp, ckey, binkey, bins, snap_str=None,
                            cent_only=True, cent_sat_split=False, four_z=False,
                            save=True, diagnostics=False, verbose=False,
                            M200_min = 0.01):

    with h5.File(filename_0, 'r') as data0:
        with h5.File(filename_1, 'r') as data1:
            with h5.File(matchfile, 'r') as matches:

                # mask_0 = M200_0 > M200_min
                # M200_0 = M200_0[mask_0]

                if cent_only:
                    sg_0 = data0['GalaxyQuantities/SubGroupNumber'][()]
                    mask_0 = sg_0 == 0
                    sg_1 = data1['GalaxyQuantities/SubGroupNumber'][()]
                    mask_1 = sg_1 == 0

                else:
                    M200_0 = data0['GalaxyQuantities/M200_crit'][()]
                    M200_1 = data1['GalaxyQuantities/M200_crit'][()]

                    mask_0 = np.ones(len(M200_0), dtype=bool)
                    mask_1 = np.ones(len(M200_1), dtype=bool)

                (match_0, match_1, best_0, best_1, best_aug_1,
                 match_central_0, match_central_1, best_central_0, best_central_1, best_aug_central_1
                 ) = load_particle_matched_haloes(data0, data1, matches, mask_0, mask_1)

                if verbose:
                    print(sum(match_0), len(match_0), sum(match_0)/len(match_0))
                    print(sum(match_1), len(match_1), sum(match_1)/len(match_1))

                if cent_only:
                    m0 = match_central_0
                    m1 = match_central_1
                    b1 = best_aug_central_1
                elif cent_sat_split:
                    m0 = np.logical_and(match_0, 1 - match_central_0)
                    m1 = np.logical_and(match_1, 1 - match_central_1)
                    b1 = np.argsort(best_1[m1])
                else:
                    m0 = match_0
                    m1 = match_1
                    b1 = best_1

                # plots
                if xkey is None:
                    plot_matched_quantity(data0, data1, mask_0, mask_1, m0, m1, b1,
                                          ykey, rkey, comp, ckey, binkey, bins, snap_str,
                                          save)

                if xkey is not None:
                    plot_matched_quantity_diff(data0, data1, mask_0, mask_1, m0, m1, b1,
                                               xkey, ykey, rkey, comp, ckey, binkey, bins, snap_str,
                                               save)

                # profiles
                # for n, (index0, index1) in enumerate(zip(np.arange(len(match_0))[match_0],
                #                                          np.arange(len(match_1))[match_1][best_aug_1])):
                #
                #     plot_matched_galaxy_profiles(data0, data1, mask_0, mask_1, index0, index1, str(index0), str(index1), save)
                #
                #     if n > 100:
                #         return

    return


def plot_matched_galaxy_profiles(data0, data1, mask0, mask1, index0, index1, name_0='', name_1='', save=True):

    fig = plt.figure(figsize=(5,20))

    ncols = 1
    nrows = 9

    bin_centres = 10 ** ((np.log10(log_bin_edges[:-1]) + np.log10(log_bin_edges[1:])) / 2)
    log_bin_centres = np.log10(bin_centres)

    spec = fig.add_gridspec(ncols=ncols, nrows=nrows)  #width_ratios=[1]*(ncols*four-1), #height_ratios=[1, 0.5, 2.2, 0.5])

    axs = []
    ax_in = []
    for row in range(nrows):
        axs.append([])
        for col in range(ncols):
            axs[row].append(fig.add_subplot(spec[row,col]))

            if row != nrows-1:
                axs[row][col].set_xticklabels([])

            axs[row][col].set_xlim([log_bin_centres[0], log_bin_centres[-1]])

    axs[0][0].set_ylabel(r'Log Cumulative Mass [$10^{10}$ M$_\odot$]')
    axs[1][0].set_ylabel(r'$V_c, \overline{v}_\phi$ [km/s]')
    axs[2][0].set_ylabel(r'$\sigma$ [km/s]')
    axs[3][0].set_ylabel(r'Log Cumulative $J_{\mathrm{tot}}, J_z$ [$10^{10}$ M$_\odot$ kpc km /s]')
    axs[4][0].set_ylabel(r'$\kappa_{\mathrm{co}}, \kappa_{\mathrm{rot}}$')
    axs[5][0].set_ylabel(r'$\log z_{1/2}$ [kpc]')
    axs[6][0].set_ylabel(r'$c/a$, $b/a$')
    axs[7][0].set_ylabel(r'age')
    axs[8][0].set_ylabel(r'$j_z / j_c(E)$')

    axs[8][0].set_xlabel(r'$\log r$ [kpc]')
    # axs[8][1].set_xlabel(r'$j_z / j_c(E)$')

    C0dm = 'C0'
    C0gas = 'C2'
    C0star = 'C4'
    C0all = 'C6'
    C1dm = 'C1'
    C1gas = 'C3'
    C1star = 'C5'
    C1all = 'C7'

    lsa = '-'
    lsb = '--'
    lsc = '-.'
    lsd = ':'

    axs[0][0].legend([((lines.Line2D([0, 1], [0, 1], color=C0dm, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1dm, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa)))],
                     ['DM', 'Gas', 'Stars'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1,1), loc='upper left')
    axs[1][0].legend([((lines.Line2D([0, 1], [0, 1], color=C0dm, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1dm, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0all, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1all, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsc)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsc))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsc)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsc)))],
                     [r'DM $\sqrt{GM/r}$', r'Gas $\sqrt{GM/r}$', r'Star $\sqrt{GM/r}$', r'Total $\sqrt{GM/r}$',
                      r'Gas $\overline{v}_\phi$', r'Gas median$(v_\phi)$', r'Star $\overline{v}_\phi$', r'Star median$(v_\phi)$'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1.4,1), loc='upper left')
    axs[2][0].legend([((lines.Line2D([0, 1], [0, 1], color=C0dm, ls=lsa, linewidth=5, alpha=0.6)),
                       (lines.Line2D([0, 1], [0, 1], color=C1dm, ls=lsa, linewidth=5, alpha=0.6))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa, linewidth=5, alpha=0.6)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa, linewidth=5, alpha=0.6))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa, linewidth=5, alpha=0.6)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa, linewidth=5, alpha=0.6))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsc)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsc))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsd)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsd))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsb, linewidth=3)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsb, linewidth=3))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsc, linewidth=3)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsc, linewidth=3))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsd, linewidth=3)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsd, linewidth=3)))],
                     [r'DM $\sigma_{\mathrm{1D}}$', r'Gas $\sigma_{\mathrm{1D}}$', r'Star $\sigma_{\mathrm{1D}}$',
                      r'Gas $\sigma_z$', r'Gas $\sigma_R$', r'Gas $\sigma_\phi$',
                      r'Star $\sigma_z$', r'Star $\sigma_R$', r'Star $\sigma_\phi$',],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1,1), loc='upper left')
    axs[3][0].legend([((lines.Line2D([0, 1], [0, 1], color=C0dm, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1dm, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0dm, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1dm, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsb)))],
                     [r'DM $J_{\mathrm{tot}}$', r'DM $J_{z}$', r'Gas $J_{\mathrm{tot}}$', r'Gas $J_{z}$',
                      r'Star $J_{\mathrm{tot}}$', r'Star $J_{z}$'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1.4,1), loc='upper left')
    axs[4][0].legend([((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsb)))],
                     [r'Gas $\kappa_{\mathrm{co}}$', r'Gas $\kappa_{\mathrm{rot}}$',
                      r'Star $\kappa_{\mathrm{co}}$', r'Star $\kappa_{\mathrm{rot}}$'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1,1), loc='upper left')
    axs[5][0].legend([((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa)))],
                     [r'Gas $z_{1/2}$', r'Star $z_{1/2}$'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1,1), loc='upper left')
    axs[6][0].legend([
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0gas, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1gas, ls=lsb))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsa)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsa))),
                      ((lines.Line2D([0, 1], [0, 1], color=C0star, ls=lsb)),
                       (lines.Line2D([0, 1], [0, 1], color=C1star, ls=lsb)))],
                     [r'Gas $c/a$', r'Gas $b/a$', r'Star $c/a$', r'Star $b/a$'],
                     handler_map={tuple: HandlerTupleVertical(ndivide=2)},
                     bbox_to_anchor=(1,1), loc='upper left')

    cops0 = data0['GalaxyQuantities/COP'][mask0, :][index0]
    cops1 = data1['GalaxyQuantities/COP'][mask1, :][index1]

    dm_mass0 = data0['GalaxyProfiles/DM/BinMass'][mask0, :n_log][index0]
    dm_mass1 = data1['GalaxyProfiles/DM/BinMass'][mask1, :n_log][index1]
    gas_mass0 = data0['GalaxyProfiles/Gas/BinMass'][mask0, :n_log][index0]
    gas_mass1 = data1['GalaxyProfiles/Gas/BinMass'][mask1, :n_log][index1]
    star_mass0 = data0['GalaxyProfiles/Star/BinMass'][mask0, :n_log][index0]
    star_mass1 = data1['GalaxyProfiles/Star/BinMass'][mask1, :n_log][index1]

    gas_mean_v_phi0 = data0['GalaxyProfiles/Gas/MeanVphi'][mask0, :n_log][index0]
    gas_mean_v_phi1 = data1['GalaxyProfiles/Gas/MeanVphi'][mask1, :n_log][index1]
    gas_median_v_phi0 = data0['GalaxyProfiles/Gas/MedianVphi'][mask0, :n_log][index0]
    gas_median_v_phi1 = data1['GalaxyProfiles/Gas/MedianVphi'][mask1, :n_log][index1]
    star_mean_v_phi0 = data0['GalaxyProfiles/Star/MeanVphi'][mask0, :n_log][index0]
    star_mean_v_phi1 = data1['GalaxyProfiles/Star/MeanVphi'][mask1, :n_log][index1]
    star_median_v_phi0 = data0['GalaxyProfiles/Star/MedianVphi'][mask0, :n_log][index0]
    star_median_v_phi1 = data1['GalaxyProfiles/Star/MedianVphi'][mask1, :n_log][index1]

    dm_sigmax0 = data0['GalaxyProfiles/DM/DMsigmax'][mask0, :n_log][index0]
    dm_sigmax1 = data1['GalaxyProfiles/DM/DMsigmax'][mask1, :n_log][index1]
    dm_sigmay0 = data0['GalaxyProfiles/DM/DMsigmay'][mask0, :n_log][index0]
    dm_sigmay1 = data1['GalaxyProfiles/DM/DMsigmay'][mask1, :n_log][index1]
    dm_sigmaz0 = data0['GalaxyProfiles/DM/DMsigmaz'][mask0, :n_log][index0]
    dm_sigmaz1 = data1['GalaxyProfiles/DM/DMsigmaz'][mask1, :n_log][index1]

    gas_sigmaz0 = data0['GalaxyProfiles/Gas/sigmaz'][mask0, :n_log][index0]
    gas_sigmaz1 = data1['GalaxyProfiles/Gas/sigmaz'][mask1, :n_log][index1]
    gas_sigmaR0 = data0['GalaxyProfiles/Gas/sigmaR'][mask0, :n_log][index0]
    gas_sigmaR1 = data1['GalaxyProfiles/Gas/sigmaR'][mask1, :n_log][index1]
    gas_sigmap0 = data0['GalaxyProfiles/Gas/sigmaphi'][mask0, :n_log][index0]
    gas_sigmap1 = data1['GalaxyProfiles/Gas/sigmaphi'][mask1, :n_log][index1]
    star_sigmaz0 = data0['GalaxyProfiles/Star/sigmaz'][mask0, :n_log][index0]
    star_sigmaz1 = data1['GalaxyProfiles/Star/sigmaz'][mask1, :n_log][index1]
    star_sigmaR0 = data0['GalaxyProfiles/Star/sigmaR'][mask0, :n_log][index0]
    star_sigmaR1 = data1['GalaxyProfiles/Star/sigmaR'][mask1, :n_log][index1]
    star_sigmap0 = data0['GalaxyProfiles/Star/sigmaphi'][mask0, :n_log][index0]
    star_sigmap1 = data1['GalaxyProfiles/Star/sigmaphi'][mask1, :n_log][index1]

    dm_Jtot0 = data0['GalaxyProfiles/DM/Jtot'][mask0, :n_log][index0]
    dm_Jtot1 = data1['GalaxyProfiles/DM/Jtot'][mask1, :n_log][index1]
    dm_Jz0 = data0['GalaxyProfiles/DM/Jz'][mask0, :n_log][index0]
    dm_Jz1 = data1['GalaxyProfiles/DM/Jz'][mask1, :n_log][index1]
    gas_Jtot0 = data0['GalaxyProfiles/Gas/Jtot'][mask0, :n_log][index0]
    gas_Jtot1 = data1['GalaxyProfiles/Gas/Jtot'][mask1, :n_log][index1]
    gas_Jz0 = data0['GalaxyProfiles/Gas/Jz'][mask0, :n_log][index0]
    gas_Jz1 = data1['GalaxyProfiles/Gas/Jz'][mask1, :n_log][index1]
    star_Jtot0 = data0['GalaxyProfiles/Star/Jtot'][mask0, :n_log][index0]
    star_Jtot1 = data1['GalaxyProfiles/Star/Jtot'][mask1, :n_log][index1]
    star_Jz0 = data0['GalaxyProfiles/Star/Jz'][mask0, :n_log][index0]
    star_Jz1 = data1['GalaxyProfiles/Star/Jz'][mask1, :n_log][index1]

    gas_kco0 = data0['GalaxyProfiles/Gas/kappaCo'][mask0, :n_log][index0]
    gas_kco1 = data1['GalaxyProfiles/Gas/kappaCo'][mask1, :n_log][index1]
    gas_krot0 = data0['GalaxyProfiles/Gas/kappaRot'][mask0, :n_log][index0]
    gas_krot1 = data1['GalaxyProfiles/Gas/kappaRot'][mask1, :n_log][index1]
    star_kco0 = data0['GalaxyProfiles/Star/kappaCo'][mask0, :n_log][index0]
    star_kco1 = data1['GalaxyProfiles/Star/kappaCo'][mask1, :n_log][index1]
    star_krot0 = data0['GalaxyProfiles/Star/kappaRot'][mask0, :n_log][index0]
    star_krot1 = data1['GalaxyProfiles/Star/kappaRot'][mask1, :n_log][index1]

    gas_z120 = data0['GalaxyProfiles/Gas/zHalf'][mask0, :n_log][index0]
    gas_z121 = data1['GalaxyProfiles/Gas/zHalf'][mask1, :n_log][index1]
    star_z120 = data0['GalaxyProfiles/Star/zHalf'][mask0, :n_log][index0]
    star_z121 = data1['GalaxyProfiles/Star/zHalf'][mask1, :n_log][index1]

    gas_axa0 = data0['GalaxyProfiles/Gas/Axisa'][mask0, :n_log][index0]
    gas_axa1 = data1['GalaxyProfiles/Gas/Axisa'][mask1, :n_log][index1]
    gas_axb0 = data0['GalaxyProfiles/Gas/Axisb'][mask0, :n_log][index0]
    gas_axb1 = data1['GalaxyProfiles/Gas/Axisb'][mask1, :n_log][index1]
    gas_axc0 = data0['GalaxyProfiles/Gas/Axisc'][mask0, :n_log][index0]
    gas_axc1 = data1['GalaxyProfiles/Gas/Axisc'][mask1, :n_log][index1]
    star_axa0 = data0['GalaxyProfiles/Star/Axisa'][mask0, :n_log][index0]
    star_axa1 = data1['GalaxyProfiles/Star/Axisa'][mask1, :n_log][index1]
    star_axb0 = data0['GalaxyProfiles/Star/Axisb'][mask0, :n_log][index0]
    star_axb1 = data1['GalaxyProfiles/Star/Axisb'][mask1, :n_log][index1]
    star_axc0 = data0['GalaxyProfiles/Star/Axisc'][mask0, :n_log][index0]
    star_axc1 = data1['GalaxyProfiles/Star/Axisc'][mask1, :n_log][index1]

    #
    axs[0][0].errorbar(log_bin_centres, np.log10(np.cumsum(dm_mass0)), c=C0dm, ls=lsa)
    axs[0][0].errorbar(log_bin_centres, np.log10(np.cumsum(dm_mass1)), c=C1dm, ls=lsa)
    axs[0][0].errorbar(log_bin_centres, np.log10(np.cumsum(gas_mass0)), c=C0gas, ls=lsa)
    axs[0][0].errorbar(log_bin_centres, np.log10(np.cumsum(gas_mass1)), c=C1gas, ls=lsa)
    axs[0][0].errorbar(log_bin_centres, np.log10(np.cumsum(star_mass0)), c=C0star, ls=lsa)
    axs[0][0].errorbar(log_bin_centres, np.log10(np.cumsum(star_mass1)), c=C1star, ls=lsa)

    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * np.cumsum(dm_mass0) / bin_centres), c=C0dm, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * np.cumsum(dm_mass1) / bin_centres), c=C1dm, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * np.cumsum(gas_mass0) / bin_centres), c=C0gas, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * np.cumsum(gas_mass1) / bin_centres), c=C1gas, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * np.cumsum(star_mass0) / bin_centres), c=C0star, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * np.cumsum(star_mass1) / bin_centres), c=C1star, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * (np.cumsum(dm_mass0) + np.cumsum(gas_mass0) +
                                                              np.cumsum(star_mass0)) / bin_centres), c=C0all, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, np.sqrt(GRAV_CONST * (np.cumsum(dm_mass1) + np.cumsum(gas_mass1) +
                                                              np.cumsum(star_mass1)) / bin_centres), c=C1all, ls=lsa)
    axs[1][0].errorbar(log_bin_centres, gas_mean_v_phi0, c=C0gas, ls=lsb)
    axs[1][0].errorbar(log_bin_centres, gas_mean_v_phi1, c=C1gas, ls=lsb)
    axs[1][0].errorbar(log_bin_centres, gas_median_v_phi0, c=C0gas, ls=lsc)
    axs[1][0].errorbar(log_bin_centres, gas_median_v_phi1, c=C1gas, ls=lsc)
    axs[1][0].errorbar(log_bin_centres, star_mean_v_phi0, c=C0star, ls=lsb)
    axs[1][0].errorbar(log_bin_centres, star_mean_v_phi1, c=C1star, ls=lsb)
    axs[1][0].errorbar(log_bin_centres, star_median_v_phi0, c=C0star, ls=lsc)
    axs[1][0].errorbar(log_bin_centres, star_median_v_phi1, c=C1star, ls=lsc)

    axs[2][0].errorbar(log_bin_centres, np.sqrt((dm_sigmax0**2 + dm_sigmay0**2 + dm_sigmaz0**2)/3),
                       c=C0dm, ls=lsa, linewidth=5, alpha=0.6)
    axs[2][0].errorbar(log_bin_centres, np.sqrt((dm_sigmax1**2 + dm_sigmay1**2 + dm_sigmaz1**2)/3),
                       c=C1dm, ls=lsa, linewidth=5, alpha=0.6)
    axs[2][0].errorbar(log_bin_centres, np.sqrt((gas_sigmaz0**2 + gas_sigmaR0**2 + gas_sigmap0**2)/3),
                       c=C0gas, ls=lsa, linewidth=5, alpha=0.6)
    axs[2][0].errorbar(log_bin_centres, np.sqrt((gas_sigmaz1**2 + gas_sigmaR1**2 + gas_sigmap1**2)/3),
                       c=C1gas, ls=lsa, linewidth=5, alpha=0.6)
    axs[2][0].errorbar(log_bin_centres, np.sqrt((star_sigmaz0**2 + star_sigmaR0**2 + star_sigmap0**2)/3),
                       c=C0star, ls=lsa, linewidth=5, alpha=0.6)
    axs[2][0].errorbar(log_bin_centres, np.sqrt((star_sigmaz1**2 + star_sigmaR1**2 + star_sigmap1**2)/3),
                       c=C1star, ls=lsa, linewidth=5, alpha=0.6)
    axs[2][0].errorbar(log_bin_centres, gas_sigmaz0, c=C0gas, ls=lsb)
    axs[2][0].errorbar(log_bin_centres, gas_sigmaz1, c=C1gas, ls=lsb)
    axs[2][0].errorbar(log_bin_centres, gas_sigmaR0, c=C0gas, ls=lsc)
    axs[2][0].errorbar(log_bin_centres, gas_sigmaR1, c=C1gas, ls=lsc)
    axs[2][0].errorbar(log_bin_centres, gas_sigmap0, c=C0gas, ls=lsd)
    axs[2][0].errorbar(log_bin_centres, gas_sigmap1, c=C1gas, ls=lsd)
    axs[2][0].errorbar(log_bin_centres, star_sigmaz0, c=C0star, ls=lsb, linewidth=3)
    axs[2][0].errorbar(log_bin_centres, star_sigmaz1, c=C1star, ls=lsb, linewidth=3)
    axs[2][0].errorbar(log_bin_centres, star_sigmaR0, c=C0star, ls=lsc, linewidth=3)
    axs[2][0].errorbar(log_bin_centres, star_sigmaR1, c=C1star, ls=lsc, linewidth=3)
    axs[2][0].errorbar(log_bin_centres, star_sigmap0, c=C0star, ls=lsd, linewidth=3)
    axs[2][0].errorbar(log_bin_centres, star_sigmap1, c=C1star, ls=lsd, linewidth=3)

    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(dm_Jtot0)), c=C0dm, ls=lsa)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(dm_Jtot1)), c=C1dm, ls=lsa)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(dm_Jz0)), c=C0dm, ls=lsb)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(dm_Jz1)), c=C1dm, ls=lsb)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(gas_Jtot0)), c=C0gas, ls=lsa)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(gas_Jtot1)), c=C1gas, ls=lsa)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(gas_Jz0)), c=C0gas, ls=lsb)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(gas_Jz1)), c=C1gas, ls=lsb)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(star_Jtot0)), c=C0star, ls=lsa, linewidth=3)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(star_Jtot1)), c=C1star, ls=lsa, linewidth=3)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(star_Jz0)), c=C0star, ls=lsb, linewidth=3)
    axs[3][0].errorbar(log_bin_centres, np.log10(np.cumsum(star_Jz1)), c=C1star, ls=lsb, linewidth=3)

    axs[4][0].errorbar(log_bin_centres, gas_kco0, c=C0gas, ls=lsa)
    axs[4][0].errorbar(log_bin_centres, gas_kco1, c=C1gas, ls=lsa)
    axs[4][0].errorbar(log_bin_centres, gas_krot0, c=C0gas, ls=lsb)
    axs[4][0].errorbar(log_bin_centres, gas_krot1, c=C1gas, ls=lsb)
    axs[4][0].errorbar(log_bin_centres, star_kco0, c=C0star, ls=lsa)
    axs[4][0].errorbar(log_bin_centres, star_kco1, c=C1star, ls=lsa)
    axs[4][0].errorbar(log_bin_centres, star_krot0, c=C0star, ls=lsb)
    axs[4][0].errorbar(log_bin_centres, star_krot1, c=C1star, ls=lsb)

    axs[5][0].errorbar(log_bin_centres, np.log10(gas_z120), c=C0gas, ls=lsa)
    axs[5][0].errorbar(log_bin_centres, np.log10(gas_z121), c=C1gas, ls=lsa)
    axs[5][0].errorbar(log_bin_centres, np.log10(star_z120), c=C0star, ls=lsa)
    axs[5][0].errorbar(log_bin_centres, np.log10(star_z121), c=C1star, ls=lsa)

    axs[6][0].errorbar(log_bin_centres, gas_axc0 / gas_axa0, c=C0gas, ls=lsa)
    axs[6][0].errorbar(log_bin_centres, gas_axc1 / gas_axa1, c=C1gas, ls=lsa)
    axs[6][0].errorbar(log_bin_centres, gas_axb0 / gas_axa0, c=C0gas, ls=lsb)
    axs[6][0].errorbar(log_bin_centres, gas_axb1 / gas_axa1, c=C1gas, ls=lsb)
    axs[6][0].errorbar(log_bin_centres, star_axc0 / star_axa0, c=C0star, ls=lsa)
    axs[6][0].errorbar(log_bin_centres, star_axc1 / star_axa1, c=C1star, ls=lsa)
    axs[6][0].errorbar(log_bin_centres, star_axb0 / star_axa0, c=C0star, ls=lsb)
    axs[6][0].errorbar(log_bin_centres, star_axb1 / star_axa1, c=C1star, ls=lsb)

    if save:
        name = f'../results/matching/profiles_{name_0}_{name_1}'
        plt.savefig(name, bbox_inches='tight')
        plt.close()

    return


def plot_positions(cops_0, cops_1, M200_0, M200_1, R200_0, R200_1,
                   match_0, match_1, best_0, best_1, best_aug_1, dcop,
                   save=False, name=None, N_lim=1000, N_match=0):

    plt.figure(figsize=(20,20))

    ax00 = plt.subplot(221)
    # ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax10.set_xlabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$X$ [Mpc/h]')
    ax00.set_ylabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Z$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    lims = [10 * LITTLE_H, 20 * LITTLE_H]
    # lims = [0, 50 * LITTLE_H]

    ax00.set_xlim(lims)
    ax10.set_xlim(lims)
    ax11.set_xlim(lims)
    ax00.set_ylim(lims)
    ax10.set_ylim(lims)
    ax11.set_ylim(lims)

    ax00.set_xticklabels([])
    ax11.set_yticklabels([])

    s_0 = np.log10(M200_0) * 10 + 4
    s_1 = np.log10(M200_1) * 10 + 4

    ax00.scatter(cops_0[:, 0], cops_0[:, 1], s=s_0, marker='o', c='C0')
    ax00.scatter(cops_1[:, 0], cops_1[:, 1], s=s_1, marker='o', c='C1')

    ax10.scatter(cops_0[:, 2], cops_0[:, 1], s=s_0, marker='o', c='C0')
    ax10.scatter(cops_1[:, 2], cops_1[:, 1], s=s_1, marker='o', c='C1')

    ax11.scatter(cops_0[:, 2], cops_0[:, 0], s=s_0, marker='o', c='C0')
    ax11.scatter(cops_1[:, 2], cops_1[:, 0], s=s_1, marker='o', c='C1')

    x = []
    y = []
    z = []
    k = 0
    for i, j in enumerate(best_0):
        if match_0[i]:
            x.append(cops_0[i, 0])
            x.append(cops_1[j, 0])
            x.append(np.nan)
            y.append(cops_0[i, 1])
            y.append(cops_1[j, 1])
            y.append(np.nan)
            z.append(cops_0[i, 2])
            z.append(cops_1[j, 2])
            z.append(np.nan)
    ax00.errorbar(x, y, c='C2')
    ax10.errorbar(z, y, c='C2')
    ax11.errorbar(z, x, c='C2')

    # lddcop = np.log10(dcop * 2 / (R200_0[match_0] + R200_1[match_1][best_aug_1]))

    # x = []
    # y = []
    # z = []
    # for i, j in enumerate(best_1):
    #     if match_1[i]:
    #         # if dcop[k] > 0.2:
    #         if lddcop[k] > 0:
    #             x.append(cops_0[j, 0])
    #             x.append(cops_1[i, 0])
    #             x.append(np.nan)
    #             y.append(cops_0[j, 1])
    #             y.append(cops_1[i, 1])
    #             y.append(np.nan)
    #             z.append(cops_0[j, 2])
    #             z.append(cops_1[i, 2])
    #             z.append(np.nan)
    #         k += 1
    # ax00.errorbar(x, y, c='C3')
    # ax10.errorbar(z, y, c='C3')
    # ax11.errorbar(z, x, c='C3')

    if save:
        filename = f'../results/matching/{name}_positions_N{N_lim}_M{N_match}'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def funky_args_paried_sorted(arr0, arr1):

    paired0 = np.zeros(len(arr0), dtype=bool)
    paired1 = np.zeros(len(arr1), dtype=bool)

    pairs = 0

    for i, a0 in enumerate(arr0):
        for j, a1 in enumerate(arr1):
            if a0 == a1: #np.all(np.isclose(a0, a1, rtol=1e-9)):
                paired0[i] = True
                paired1[j] = True
                pairs += 1

    out_0 = np.zeros(len(arr0))
    k, j = 0, 0
    for i in range(len(arr0)):
        if paired0[i]:
            out_0[i] = k * 2
            k += 1
        else:
            out_0[i] = 2*pairs + j
            j += 1

    out_1 = np.zeros(len(arr1))
    k, j = 0, 0
    for i in range(len(arr1)):
        if paired1[i]:
            out_1[i] = 1 + k * 2
            k += 1
        else:
            out_1[i] = len(arr0) + pairs + j
            j += 1

    return out_0, out_1


def plot_match_diagnostics(M200_0, M200_1, R200_0, R200_1, m_star_0, m_star_1,
                           match_0, match_1, best_aug_1,
                           # match_0_add, match_1_add,
                           # match_0_rest, match_1_rest,
                           # match_0_final, match_1_final,
                           dcop, save=False, name=None, N_limit=1000, N_match=0,
                           with_stars=False, mdm=False):
    # mass vs mass
    plt.figure(figsize=(10,10))

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_xlabel(r'$\log M_{200} [10^{10}$ M$_\odot / h ]$ [hi res.]')
    ax00.set_ylabel(r'Fraction of matched centrals [hi res.]' + with_stars * ' [with stars]')
    ax11.set_xlabel(r'$\log M_{200} [10^{10}$ M$_\odot / h ]$ [fid. res.]')
    ax11.set_ylabel(r'Fraction of matched centrals [fid. res.]' + with_stars * ' [with stars]')
    ax10.set_ylabel(r'$\log \, M_{200}$ hi res - $\log \, M_{200}$ fid. res.')
    ax10.set_xlabel(r'$\log \, M_{200} [10^{10}$ M$_\odot / h ]$ [hi res.]')
    ax01.set_ylabel(r'$\log \, \Delta \vec{\rm COP} / r_{200}$')
    ax01.set_xlabel(r'$\log \, M_{200} [10^{10}$ M$_\odot / h ]$ [hi res.]')

    if mdm:
        ax00.set_xlabel(r'$\log M_{\rm DM} [10^{10}$ M$_\odot / h ]$ [hi res.]')
        ax00.set_ylabel(r'Fraction of matched centrals [hi res.]' + with_stars * ' [with stars]')
        ax11.set_xlabel(r'$\log M_{\rm DM} [10^{10}$ M$_\odot / h ]$ [fid. res.]')
        ax11.set_ylabel(r'Fraction of matched centrals [fid. res.]' + with_stars * ' [with stars]')
        ax10.set_ylabel(r'$\log \, M_{\rm DM}$ hi res - $\log \, M_{\rm DM}$ fid. res.')
        ax10.set_xlabel(r'$\log \, M_{\rm DM} [10^{10}$ M$_\odot / h ]$ [hi res.]')
        ax01.set_ylabel(r'$\log \, \Delta \vec{\rm COP} / r_{200}$')
        ax01.set_xlabel(r'$\log \, M_{\rm DM} [10^{10}$ M$_\odot / h ]$ [hi res.]')

    # ax11.legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=10)),
    #              (lines.Line2D([0, 1], [0, 1], color='C1', ls='-', linewidth=10)),
    #              (lines.Line2D([0, 1], [0, 1], color='C2', ls='-', linewidth=10)),],
    #             ['Central-central bijective match', 'Central-satellite bijective match', 'Central-any surjective match'],
    #             loc='lower right')

    # ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    # ax10.set_aspect('equal')
    # ax11.set_aspect('equal')

    # lims = [40,50]
    # lims = np.array([-1.5, 3.5])
    lims = np.array([-1.5, 3.5])
    dm_lims = [-0.5,0.5]
    r_lims = [-2.2, 1.6]

    ax00.set_xlim(lims)
    ax01.set_xlim(lims)
    ax10.set_xlim(lims)
    ax11.set_xlim(lims)
    ax00.set_ylim([0, 1])
    ax01.set_ylim(*r_lims)
    ax10.set_ylim(*dm_lims)
    ax11.set_ylim([0, 1])

    mbins = np.linspace(*lims, 41)

    m_star_bins = [-5, -3, -2, -1, 0, 1]

    lm200m0 = np.log10(M200_0[match_0])
    lmstarm0 = np.log10(m_star_0[match_0])
    nall, _ = np.histogram(np.log10(M200_0), bins=mbins)
    nmatch, _ = np.histogram(lm200m0, bins=mbins)
    ax00.bar((mbins[1:] + mbins[:-1]) / 2, nmatch / nall, width=np.diff(mbins), color='C0')
    for i, logMstar in enumerate(m_star_bins):
        m = lm200m0[lmstarm0 > logMstar]
        nno, _ = np.histogram(m, bins=mbins)
        ax00.bar((mbins[1:] + mbins[:-1]) / 2, nno / nall, width=np.diff(mbins), color=f'C{i+1}')

    lm200m1 = np.log10(M200_1[match_1])
    lmstarm1 = np.log10(m_star_1[match_1])
    nall, _ = np.histogram(np.log10(M200_1), bins=mbins)
    nmatch, _ = np.histogram(lm200m1, bins=mbins)
    ax11.bar((mbins[1:] + mbins[:-1]) / 2, nmatch / nall, width=np.diff(mbins),
             color='C0', label=r'$M_\star = 0$')
    for i, logMstar in enumerate(m_star_bins):
        m = lm200m1[lmstarm1 > logMstar]
        nno, _ = np.histogram(m, bins=mbins)
        ax11.bar((mbins[1:] + mbins[:-1]) / 2, nno / nall, width=np.diff(mbins), color=f'C{i+1}',
                 label=rf'$M_\star > 10^{{{logMstar + 10}}} $M$_\odot / h$')

    ax11.legend(loc='lower right')

    matched_M200_0 = np.log10(M200_0[match_0])
    matched_M200_1 = np.log10(M200_1[match_1][best_aug_1])

    x = matched_M200_0
    y = matched_M200_0 - matched_M200_1

    ax10.errorbar(x, y, marker='.', ls='', markersize=2)
    N = 20
    ax10.hexbin(x, y, C=np.ones(len(y)),
                reduce_C_function=lambda n : np.log10(np.sum(n)), vmin=0, vmax=4,
                gridsize=(int((np.amax(x[np.isfinite(x)]) - np.amin(x[np.isfinite(x)])) /
                              (lims[1] - lims[0]) * N * 2),
                          int((np.amax(y[np.isfinite(y)]) - np.amin(y[np.isfinite(y)])) /
                              (dm_lims[1] - dm_lims[0]) * N * 2 / np.sqrt(3))),
                cmap='Blues', mincnt=5, edgecolors='none', zorder=5)

    ymedians, xedges, bin_is = binned_statistic(x, y,
                                                statistic=np.nanmedian, bins=mbins)
    yplus, xedges, _ = binned_statistic(x, y,
                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=mbins)
    yminus, xedges, _ = binned_statistic(x, y,
                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=mbins)
    path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    ax10.errorbar(0.5 * (xedges[1:] + xedges[:-1]), ymedians,
                 c='C0', ls='-', linewidth=2, zorder=10, path_effects=path_effect)
    ax10.fill_between(0.5 * (xedges[1:] + xedges[:-1]), yminus, yplus,
                     color='C0', alpha=0.3, edgecolor='k', zorder=9)

    ax10.errorbar(lims, [0]*2, ls=':', c='k')

    # dcop_r200 = np.log10(dcop * 2 / (R200_0[match_0] + R200_1[match_1][best_aug_1]))
    dcop_r200 = np.log10(dcop / R200_0[match_0])
    y = dcop_r200

    ax01.errorbar(x, y, marker='.', ls='', markersize=2)
    N = 20
    ax01.hexbin(x, y, C=np.ones(len(y)),
                reduce_C_function=lambda n : np.log10(np.sum(n)), vmin=0, vmax=4,
                gridsize=(int((np.amax(x[np.isfinite(x)]) - np.amin(x[np.isfinite(x)])) /
                              (lims[1] - lims[0]) * N * 2),
                          int((np.amax(y[np.isfinite(y)]) - np.amin(y[np.isfinite(y)])) /
                              (r_lims[1] - r_lims[0]) * N * 2 / np.sqrt(3))),
                cmap='Blues', mincnt=5, edgecolors='none', zorder=5)

    ymedians, xedges, bin_is = binned_statistic(x, y,
                                                statistic=np.nanmedian, bins=mbins)
    yplus, xedges, _ = binned_statistic(x, y,
                                        statistic=lambda x: np.nanquantile(x, 0.16), bins=mbins)
    yminus, xedges, _ = binned_statistic(x, y,
                                         statistic=lambda x: np.nanquantile(x, 0.84), bins=mbins)
    path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
    ax01.errorbar(0.5 * (xedges[1:] + xedges[:-1]), ymedians,
                 c='C0', ls='-', linewidth=2, zorder=10, path_effects=path_effect)
    ax01.fill_between(0.5 * (xedges[1:] + xedges[:-1]), yminus, yplus,
                     color='C0', alpha=0.3, edgecolor='k', zorder=9)

    inv_r200 = np.log10(1e-2/R200_0[match_0])
    ymedians, xedges, bin_is = binned_statistic(matched_M200_0, inv_r200,
                                                statistic=np.nanmedian, bins=mbins)
    ax01.errorbar(0.5 * (xedges[1:] + xedges[:-1]), ymedians,
                 c='k', ls=':', linewidth=2, zorder=10)

    if save:
        filename = f'../results/matching/{name}_diagnostics_N{N_limit}_M{N_match}'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def plot_match_diagnostics_2(M200_0, M200_1, mstar_0, mstar_1,
                             gn_0, gn_1, sn_0, sn_1,
                             match_central_0, match_central_1, best_aug_central_1,
                             match_0, match_1, best_aug_1, best_aug_0,
                             save=True, name=None, N_limit=1000, N_match=0):

    # mass vs mass
    plt.figure(figsize=(12,10))

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_xlabel(r'$\log M_{200} [10^{10}$ M$_\odot / h ]$ [hi res.]')
    ax00.set_ylabel(r'Fraction of matched haloes [hi res.]')

    ax01.set_xlabel(r'$\log M_\star [10^{10}$ M$_\odot / h ]$ [hi. res.]')
    ax01.set_ylabel(r'Fraction of matched subhaloes [hi res.]')

    ax10.set_xlabel(r'$\log M_{200} [10^{10}$ M$_\odot / h ]$ [fid. res.]')
    ax10.set_ylabel(r'Fraction of matched haloes [fid. res.]')

    ax11.set_xlabel(r'$\log M_\star [10^{10}$ M$_\odot / h ]$ [fid. res.]')
    ax11.set_ylabel(r'Fraction of matched subcentrals [fid. res.]')

    #TODO twin axis

    # ax11.legend([(lines.Line2D([0, 1], [0, 1], color='C0', ls='-', linewidth=10)),
    #              (lines.Line2D([0, 1], [0, 1], color='C1', ls='-', linewidth=10)),
    #              (lines.Line2D([0, 1], [0, 1], color='C2', ls='-', linewidth=10)),],
    #             ['Central-central bijective match', 'Central-satellite bijective match', 'Central-any surjective match'],
    #             loc='lower right')

    # ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    # ax10.set_aspect('equal')
    # ax11.set_aspect('equal')

    # lims = [40,50]
    # lims = np.array([-1.5, 3.5])

    dm_lims = np.array([-1.5, 3.5])
    star_lims = np.array([-4.2, 1.2])

    ax00.set_xlim(dm_lims)
    ax01.set_xlim(star_lims)
    ax10.set_xlim(dm_lims)
    ax11.set_xlim(star_lims)

    ax00.set_ylim([0, 1])
    ax01.set_ylim([0, 1])
    ax10.set_ylim([0, 1])
    ax11.set_ylim([0, 1])

    dm_bins = np.linspace(*dm_lims, 41)
    star_bins = np.linspace(*star_lims, 41)

    nall, _ = np.histogram(np.log10(M200_0), bins=dm_bins)
    nmatch, _ = np.histogram(np.log10(M200_0[match_central_0]), bins=dm_bins)

    n_cent, _ = np.histogram(np.log10(M200_0[sn_0 == 0]), bins=dm_bins) # = nall

    m_halo_cent_cent = np.zeros(len(M200_0), dtype=bool)
    m_halo_cent_sub = np.zeros(len(M200_0), dtype=bool)
    m_halo_cent_none = np.zeros(len(M200_0), dtype=bool)

    m_fail_cent_cent = np.zeros(len(M200_0), dtype=bool)
    m_fail_cent_sub = np.zeros(len(M200_0), dtype=bool)
    m_fail_cent_none = np.zeros(len(M200_0), dtype=bool)

    m_cent_cent = np.zeros(len(M200_0), dtype=bool)
    m_cent_sub = np.zeros(len(M200_0), dtype=bool)
    m_cent_none = np.zeros(len(M200_0), dtype=bool)

    index_range = np.arange(len(gn_1))[match_1][best_aug_1]

    k = 0
    for i, (gn, sn) in enumerate(zip(gn_0, sn_0)):

        if sn == 0:
            halo = match_central_0[i]
            match = match_0[i]
            cent_stat = None

            if match:
                j = index_range[k]

                # if i < 2_000:
                #     print(f'gn {gn}, sn {sn}, i {i}, j {j}, gn_1 {gn_1[j]}, sn_1 {sn_1[j]}')

                cent_stat = sn_1[j] != 0

            if halo:
                if match:
                    if cent_stat:
                        m_halo_cent_sub[i] = True
                    else:
                        m_halo_cent_cent[i] = True
                else:
                    m_halo_cent_none[i] = True
            else:
                if match:
                    if cent_stat:
                        m_fail_cent_sub[i] = True
                    else:
                        m_fail_cent_cent[i] = True
                else:
                    m_fail_cent_none[i] = True

            if match:
                if cent_stat:
                    m_cent_sub[i] = True
                else:
                    m_cent_cent[i] = True
            else:
                m_cent_none[i] = True

        if match_0[i]:
            k += 1

    # n_cent_cent, _ = np.histogram(np.log10(M200_0[match_central_0]), bins=dm_bins)

    n_halo_cent_cent, _ = np.histogram(np.log10(M200_0[m_halo_cent_cent]), bins=dm_bins)
    n_halo_cent_sub, _ = np.histogram(np.log10(M200_0[m_halo_cent_sub]), bins=dm_bins)
    n_halo_cent_none, _ = np.histogram(np.log10(M200_0[m_halo_cent_none]), bins=dm_bins)

    n_fail_cent_cent, _ = np.histogram(np.log10(M200_0[m_fail_cent_cent]), bins=dm_bins)
    n_fail_cent_sub, _ = np.histogram(np.log10(M200_0[m_fail_cent_sub]), bins=dm_bins)
    n_fail_cent_none, _ = np.histogram(np.log10(M200_0[m_fail_cent_none]), bins=dm_bins)

    n_cent_cent, _ = np.histogram(np.log10(M200_0[m_cent_cent]), bins=dm_bins)
    n_cent_sub, _ = np.histogram(np.log10(M200_0[m_cent_sub]), bins=dm_bins)
    n_cent_none, _ = np.histogram(np.log10(M200_0[m_cent_none]), bins=dm_bins)

    ax00.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat(nmatch / nall, 2), 0)),
                  color='k', lw=3, zorder=5)

    ax00.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_fail_cent_sub + n_fail_cent_none +
                  n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax00.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_fail_cent_sub + n_halo_cent_none +
                                                                  n_halo_cent_sub + n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax00.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_halo_cent_none + n_halo_cent_sub +
                                                                  n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax00.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_halo_cent_sub + n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax00.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat(n_halo_cent_cent / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)

    ax00.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (n_fail_cent_sub + n_fail_cent_none +
            n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall, width=np.diff(dm_bins),
             color='C7', hatch='**', alpha=.99)
    ax00.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (n_fail_cent_sub +
            n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall, width=np.diff(dm_bins),
             color='C6', hatch=r'\\\\\\', alpha=.99)

    ax00.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (
            n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall,  width=np.diff(dm_bins),
             color='C4', hatch='+++', alpha=.99)
    ax00.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (n_halo_cent_sub + n_halo_cent_cent) / nall, width=np.diff(dm_bins),
             color='C1', hatch='//////', alpha=.99)
    ax00.bar((dm_bins[1:] + dm_bins[:-1]) / 2, n_halo_cent_cent / nall, width=np.diff(dm_bins),
             color='C0', alpha=.99)


    nall, _ = np.histogram(np.log10(M200_1), bins=dm_bins)
    nmatch, _ = np.histogram(np.log10(M200_1[match_central_1]), bins=dm_bins)

    # ax10.bar((dm_bins[1:] + dm_bins[:-1]) / 2, nmatch / nall, width=np.diff(dm_bins),
    #          color='k', fill=False, lw=3)
    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat(nmatch / nall, 2), 0)),
                  color='k', lw=3, zorder=5)

    m_halo_cent_cent = np.zeros(len(M200_1), dtype=bool)
    m_halo_cent_sub = np.zeros(len(M200_1), dtype=bool)
    m_halo_cent_none = np.zeros(len(M200_1), dtype=bool)

    m_fail_cent_cent = np.zeros(len(M200_1), dtype=bool)
    m_fail_cent_sub = np.zeros(len(M200_1), dtype=bool)
    m_fail_cent_none = np.zeros(len(M200_1), dtype=bool)

    m_cent_cent = np.zeros(len(M200_1), dtype=bool)
    m_cent_sub = np.zeros(len(M200_1), dtype=bool)
    m_cent_none = np.zeros(len(M200_1), dtype=bool)

    index_range = np.arange(len(gn_0))[match_0][best_aug_0]

    k = 0
    for i, (gn, sn) in enumerate(zip(gn_1, sn_1)):

        if sn == 0:
            halo = match_central_1[i]
            match = match_1[i]
            cent_stat = None

            if match:
                j = index_range[k]

                # if i < 2_000:
                #     print(f'gn {gn}, sn {sn}, i {i}, j {j}, gn_1 {gn_1[j]}, sn_1 {sn_1[j]}')

                cent_stat = sn_0[j] != 0

            if halo:
                if match:
                    if cent_stat:
                        m_halo_cent_sub[i] = True
                    else:
                        m_halo_cent_cent[i] = True
                else:
                    m_halo_cent_none[i] = True
            else:
                if match:
                    if cent_stat:
                        m_fail_cent_sub[i] = True
                    else:
                        m_fail_cent_cent[i] = True
                else:
                    m_fail_cent_none[i] = True

            if match:
                if cent_stat:
                    m_cent_sub[i] = True
                else:
                    m_cent_cent[i] = True
            else:
                m_cent_none[i] = True

        if match_1[i]:
            k += 1

    # n_cent_cent, _ = np.histogram(np.log10(M200_0[match_central_0]), bins=dm_bins)

    n_halo_cent_cent, _ = np.histogram(np.log10(M200_1[m_halo_cent_cent]), bins=dm_bins)
    n_halo_cent_sub, _ = np.histogram(np.log10(M200_1[m_halo_cent_sub]), bins=dm_bins)
    n_halo_cent_none, _ = np.histogram(np.log10(M200_1[m_halo_cent_none]), bins=dm_bins)

    n_fail_cent_cent, _ = np.histogram(np.log10(M200_1[m_fail_cent_cent]), bins=dm_bins)
    n_fail_cent_sub, _ = np.histogram(np.log10(M200_1[m_fail_cent_sub]), bins=dm_bins)
    n_fail_cent_none, _ = np.histogram(np.log10(M200_1[m_fail_cent_none]), bins=dm_bins)

    n_cent_cent, _ = np.histogram(np.log10(M200_1[m_cent_cent]), bins=dm_bins)
    n_cent_sub, _ = np.histogram(np.log10(M200_1[m_cent_sub]), bins=dm_bins)
    n_cent_none, _ = np.histogram(np.log10(M200_1[m_cent_none]), bins=dm_bins)

    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat(nmatch / nall, 2), 0)),
                  color='k', lw=3, zorder=5)

    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_fail_cent_sub + n_fail_cent_none +
                  n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_fail_cent_sub + n_halo_cent_none +
                                                                  n_halo_cent_sub + n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_halo_cent_none + n_halo_cent_sub +
                                                                  n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat((n_halo_cent_sub + n_halo_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax10.errorbar(np.repeat(dm_bins, 2), np.hstack((0, np.repeat(n_halo_cent_cent / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)

    ax10.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (n_fail_cent_sub + n_fail_cent_none +
            n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall, width=np.diff(dm_bins),
             color='C7', hatch='**', label='halo-none, central-none', alpha=.99)
    ax10.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (n_fail_cent_sub +
            n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall, width=np.diff(dm_bins),
             color='C6', hatch=r'\\\\\\', label='halo-none, central-satellite', alpha=.99)

    ax10.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (
            n_halo_cent_none + n_halo_cent_sub + n_halo_cent_cent) / nall,  width=np.diff(dm_bins),
             color='C4', hatch='+++', label='halo-halo, central-none', alpha=.99)
    ax10.bar((dm_bins[1:] + dm_bins[:-1]) / 2, (n_halo_cent_sub + n_halo_cent_cent) / nall, width=np.diff(dm_bins),
             color='C1', hatch='//////', label='halo-halo, central-satellite', alpha=.99)
    ax10.bar((dm_bins[1:] + dm_bins[:-1]) / 2, n_halo_cent_cent / nall, width=np.diff(dm_bins),
             color='C0', label='central-central', alpha=.99)

    #
    lmstarm0 = np.log10(mstar_0[match_0])

    nall, _ = np.histogram(np.log10(mstar_0), bins=star_bins)
    nmatch, _ = np.histogram(lmstarm0, bins=star_bins)

    n_cent, _ = np.histogram(np.log10(mstar_0[sn_0 == 0]), bins=star_bins)
    n_sub, _ = np.histogram(np.log10(mstar_0[sn_0 != 0]), bins=star_bins)

    n_any_any, _ = np.histogram(np.log10(mstar_0[match_0]), bins=star_bins)

    n_cent_any, _ = np.histogram(np.log10(mstar_0[match_0][sn_0[match_0] == 0]), bins=star_bins)
    n_sub_any, _ = np.histogram(np.log10(mstar_0[match_0][sn_0[match_0] != 0]), bins=star_bins)

    n_cent_cent, _ = np.histogram(np.log10(mstar_0[match_0][np.logical_and(sn_0[match_0] == 0,
                                                            sn_1[match_1][best_aug_1] == 0)]), bins=star_bins)
    n_cent_sub, _ = np.histogram(np.log10(mstar_0[match_0][np.logical_and(sn_0[match_0] == 0,
                                                            sn_1[match_1][best_aug_1] != 0)]), bins=star_bins)
    n_sub_cent, _ = np.histogram(np.log10(mstar_0[match_0][np.logical_and(sn_0[match_0] != 0,
                                                            sn_1[match_1][best_aug_1] == 0)]), bins=star_bins)
    n_sub_sub, _ = np.histogram(np.log10(mstar_0[match_0][np.logical_and(sn_0[match_0] != 0,
                                                            sn_1[match_1][best_aug_1] != 0)]), bins=star_bins)

    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(nmatch / nall, 2), 0)),
                  color='k', lw=3, zorder=5)

    # ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_cent / nall, 2), 0)),
    #               color='C0', lw=3)
    # ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_sub / nall, 2), 0)),
    #               color='C1', lw=3)

    # ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_cent_any / nall, 2), 0)),
    #               color='k', lw=1, ls='-', zorder=5)
    # ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_sub_any + n_cent_any) / nall, 2), 0)),
    #               color='k', lw=3)

    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_sub - n_sub_any +
                                                                    n_cent - n_cent_any + n_any_any) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_cent - n_cent_any + n_any_any) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_any_any / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_sub_cent + n_cent_any) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_cent_sub + n_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_cent_cent / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)

    ax01.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_sub - n_sub_any +
                                                    n_cent - n_cent_any + n_any_any) / nall, width=np.diff(star_bins),
             color='C5', hatch='oo', alpha=.99)
    ax01.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_cent - n_cent_any + n_any_any) / nall, width=np.diff(star_bins),
             color='C4', hatch='+++', alpha=.99)

    ax01.bar((star_bins[1:] + star_bins[:-1]) / 2, n_any_any / nall, width=np.diff(star_bins),
             color='C3', hatch=r'\\', alpha=.99)
    ax01.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_sub_cent + n_cent_any) / nall, width=np.diff(star_bins),
             color='C2', hatch='....', alpha=.99)
    ax01.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_cent_sub + n_cent_cent) / nall, width=np.diff(star_bins),
             color='C1', hatch='//////', alpha=.99)
    ax01.bar((star_bins[1:] + star_bins[:-1]) / 2, n_cent_cent / nall, width=np.diff(star_bins),
             color='C0', alpha=.99)


    nall, _ = np.histogram(np.log10(mstar_1), bins=star_bins)
    nmatch, _ = np.histogram(np.log10(mstar_1[match_1]), bins=star_bins)

    n_cent, _ = np.histogram(np.log10(mstar_1[sn_1 == 0]), bins=star_bins)
    n_sub, _ = np.histogram(np.log10(mstar_1[sn_1 != 0]), bins=star_bins)

    n_any_any, _ = np.histogram(np.log10(mstar_1[match_1]), bins=star_bins)

    n_cent_any, _ = np.histogram(np.log10(mstar_1[match_1][sn_1[match_1] == 0]), bins=star_bins)
    n_sub_any, _ = np.histogram(np.log10(mstar_1[match_1][sn_1[match_1] != 0]), bins=star_bins)

    n_cent_cent, _ = np.histogram(np.log10(mstar_1[match_1][best_aug_1][np.logical_and(sn_1[match_1][best_aug_1] == 0,
                                                            sn_0[match_0] == 0)]), bins=star_bins)
    n_cent_sub, _ = np.histogram(np.log10(mstar_1[match_1][best_aug_1][np.logical_and(sn_1[match_1][best_aug_1] == 0,
                                                            sn_0[match_0] != 0)]), bins=star_bins)
    n_sub_cent, _ = np.histogram(np.log10(mstar_1[match_1][best_aug_1][np.logical_and(sn_1[match_1][best_aug_1] != 0,
                                                            sn_0[match_0] == 0)]), bins=star_bins)
    n_sub_sub, _ = np.histogram(np.log10(mstar_1[match_1][best_aug_1][np.logical_and(sn_1[match_1][best_aug_1] != 0,
                                                            sn_0[match_0] != 0)]), bins=star_bins)

    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(nmatch / nall, 2), 0)),
                  color='k', lw=3, zorder=5)

    # ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_cent / nall, 2), 0)),
    #               color='C0', lw=3)
    # ax01.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_sub / nall, 2), 0)),
    #               color='C1', lw=3)

    # ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_cent_any / nall, 2), 0)),
    #               color='k', lw=2, ls='--', zorder=5)
    # ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_sub_any + n_cent_any) / nall, 2), 0)),
    #               color='k', lw=3)

    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_sub - n_sub_any +
                                                                    n_cent - n_cent_any + n_any_any) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_cent - n_cent_any + n_any_any) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_any_any / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_sub_cent + n_cent_any) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat((n_cent_sub + n_cent_cent) / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)
    ax11.errorbar(np.repeat(star_bins, 2), np.hstack((0, np.repeat(n_cent_cent / nall, 2), 0)),
                  color='k', lw=1, ls='-', zorder=5)

    ax11.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_sub - n_sub_any +
                                                    n_cent - n_cent_any + n_any_any) / nall, width=np.diff(star_bins),
             color='C5', hatch='oo', label='satellite-none', alpha=.99)
    ax11.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_cent - n_cent_any + n_any_any) / nall, width=np.diff(star_bins),
             color='C4', hatch='+++', label='central-none', alpha=.99)

    ax11.bar((star_bins[1:] + star_bins[:-1]) / 2, n_any_any / nall, width=np.diff(star_bins),
             color='C3', hatch=r'\\', label='satellite-satellite', alpha=.99)
    ax11.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_sub_cent + n_cent_any) / nall, width=np.diff(star_bins),
             color='C2', hatch='....', label='satellite-central', alpha=.99)
    ax11.bar((star_bins[1:] + star_bins[:-1]) / 2, (n_cent_sub + n_cent_cent) / nall, width=np.diff(star_bins),
             color='C1', hatch='//////', label='central-satellite', alpha=.99)
    ax11.bar((star_bins[1:] + star_bins[:-1]) / 2, n_cent_cent / nall, width=np.diff(star_bins),
             color='C0', label='central-central', alpha=.99)

    ax10.legend(loc='lower right')
    ax11.legend(loc='lower right')

    if save:
        # filename = f'../results/matching/{name}_diagnostics_N{N_limit}_M{N_match}.pdf'
        filename = f'../results/matching/{name}_diagnostics_N{N_limit}_M{N_match}'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def plot_matched_quantity(data0, data1, mask_0, mask_1, match_0, match_1, best_aug_1,
                          xkey, rkey, comp, ckey, binkey, bin0_edges, snap_str,
                          save=False, histbins=20, cmap='turbo'):

    x0_raw = return_array_from_keys(data0, xkey, rkey, comp, mask_0, ii=None)
    x1_raw = return_array_from_keys(data1, xkey, rkey, comp, mask_1, ii=None)

    x0 = x0_raw[match_0]
    x1 = x1_raw[match_1][best_aug_1]

    c0_raw = return_array_from_keys(data0, ckey, rkey, comp, mask_0, ii=None)
    c1_raw = return_array_from_keys(data1, ckey, rkey, comp, mask_1, ii=None)
    if c0_raw is not None and c1_raw is not None:
        cs = 0.5 * (c0_raw[match_0] + c1_raw[match_1][best_aug_1])
    else:
        cs = np.zeros(len(x0))

    clim = lims_dict[ckey]

    bin0_raw = return_array_from_keys(data0, binkey, rkey, comp, mask_0, ii=None)
    bin1_raw = return_array_from_keys(data1, binkey, rkey, comp, mask_1, ii=None)

    if bin0_raw is not None and bin1_raw is not None:
        bin0 = 0.5 * (bin0_raw[match_0] + bin1_raw[match_1][best_aug_1])

        bin0_raw[match_0] = bin0
        bin1_raw[match_1] = bin0[np.argsort(best_aug_1)]
    else:
        bin0 = np.ones(len(x0), dtype=bool)
        bin0_raw = np.ones(len(x0_raw), dtype=bool)
        bin1_raw = np.ones(len(x1_raw), dtype=bool)

    mask = np.digitize(bin0, bin0_edges)

    mask0_raw = np.digitize(bin0_raw, bin0_edges)
    mask1_raw = np.digitize(bin1_raw, bin0_edges)

    lims = lims_dict[xkey]

    fig = plt.figure(figsize=(5.3 * min(len(bin0_edges) - 1, 4), 5.3 * ((len(bin0_edges) + 2) // 4)),
                     constrained_layout=True)
    fig.set_constrained_layout_pads(w_pad=0, h_pad=0, hspace=0, wspace=0)

    for ii in range(len(bin0_edges)-1):

        ax = plt.subplot((len(bin0_edges) + 2) // 4, min(len(bin0_edges) - 1, 4), ii+1)
        ax.set_aspect(1)

        warnings.filterwarnings("ignore")
        plt.subplots_adjust(wspace=0, hspace=0)

        #plot is heres

        x = x1[mask == ii+1]
        y = x0[mask == ii+1]
        c = cs[mask == ii+1]

        x_raw = x1_raw[mask1_raw == ii+1]
        y_raw = x0_raw[mask0_raw == ii+1]

        ax.scatter(x, y, s=3, c=c, vmin=clim[0], vmax=clim[1], cmap=cmap)

        #histograms
        hist_bin_edges = np.linspace(*lims, histbins+1)
        Nx_raw, _ = np.histogram(x_raw, bins=hist_bin_edges)
        Ny_raw, _ = np.histogram(y_raw, bins=hist_bin_edges)
        Nx, _ = np.histogram(x, bins=hist_bin_edges)
        Ny, _ = np.histogram(y, bins=hist_bin_edges)

        hist_max = np.amax((Nx_raw, Ny_raw))
        hist_const = 0.3 * (lims[1] - lims[0]) / hist_max

        hb = np.repeat(hist_bin_edges, 2)
        hx_raw = hist_const * np.hstack((0, np.repeat(Nx_raw, 2), 0)) + lims[0]
        hy_raw = hist_const * np.hstack((0, np.repeat(Ny_raw, 2), 0)) + lims[0]
        hx = hist_const * np.hstack((0, np.repeat(Nx, 2), 0)) + lims[0]
        hy = hist_const * np.hstack((0, np.repeat(Ny, 2), 0)) + lims[0]

        ax.errorbar(hb, hx_raw, ls='-', c='C3')
        ax.errorbar(hy_raw, hb, ls='-', c='C3')
        ax.errorbar(hb, hx, ls='-', c='C0')
        ax.errorbar(hy, hb, ls='-', c='C0')

        #decorations start here
        padx = 0.05
        pady = 0.05
        dy = 0.05
        bin_label = f'{label_dict[binkey]} = [{round(bin0_edges[ii], 1)} - {round(bin0_edges[ii + 1], 1)}]'
        ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+0*dy)*(lims[1]-lims[0]),
                bin_label, va='top')
        rlab = rkey_label_dict[rkey]#[1:-1]
        ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+1*dy)*(lims[1]-lims[0]),
                rlab, va='top')
        N_label = f'$N$ matched {np.sum(mask==ii+1)}'
        ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+2*dy)*(lims[1]-lims[0]),
                N_label, va='top')
        N_label = f'$N$ hi res. {np.sum(mask0_raw==ii+1)} $N$ fid. res. {np.sum(mask1_raw==ii+1)}'
        ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+3*dy)*(lims[1]-lims[0]),
                N_label, va='top')
        corr, pvalue = scipy.stats.spearmanr(x0[mask == ii+1], x1[mask == ii+1], nan_policy='omit')
        ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+4*dy)*(lims[1]-lims[0]),
                f'Spearman={round(corr, 3)}', va='top')

        if ii % 4 == 0:
            xlab = label_dict[xkey] + ' [hi res.]'
            ax.set_ylabel(xlab)
        else:
            ax.set_yticklabels([])

        if ii // 4 == (len(bin0_edges) -2) // 4:
            xlab = label_dict[xkey] + ' [fid. res.]'
            ax.set_xlabel(xlab)
        else:
            ax.set_xticklabels([])

        ax.set_xlim(lims)
        ax.set_ylim(lims)

        ax.errorbar(lims, lims, ls=':', c='k')
        # ax.errorbar(np.array(lims) + 1, lims, ls=':', c='k')
        # ax.errorbar(np.array(lims) - 1, lims, ls=':', c='k')

        if ii == len(bin0_edges) - 2 and ckey is not None:
            # Bbox = ax.get_position()
            # cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])
            # cbar = plt.colorbar(cax=cax)
            # cbar.set_label(label_dict[ckey])
            # print('tried')

            Bbox = ax.get_position()

            cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])

            nticks = 5
            x_ = np.linspace(1, 0, 1001)
            x, _ = np.meshgrid(x_, np.array([0, 1]))
            cax.imshow(x.T, cmap=cmap, aspect=2 / 1001 / 0.08)
            cax.yaxis.set_label_position('right')
            cax.yaxis.tick_right()
            cax.set_yticks(np.linspace(0, 1001, nticks))
            cax.set_yticklabels([round(q, 1) for q in np.linspace(clim[1], clim[0], 5)])
            cax.set_xticks([])
            cax.set_xticklabels([])
            cax.set_ylabel(label_dict[ckey])

    if save:
        filename = '../results/matching/'
        filename += filename_dict[xkey]
        filename += '_' + filename_dict[rkey]
        filename += '_' + filename_dict[comp]
        if ckey is not None:
            filename += '_' + filename_dict[ckey]
        filename += '_cent_' + snap_str
        if len(bin0_edges) > 2:
            filename += '_' + filename_dict[binkey] + f'_{len(bin0_edges) - 1}bins'
        filename += '.png'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def plot_matched_quantity_diff(data0, data1, mask_0, mask_1, match_0, match_1, best_aug_1,
                               xkey, ykey, rkey, comp, ckey, binkey, bin0_edges, snap_str=None,
                               save=True, histbins=20, cmap='turbo', N_run=13, bootstrap=True):

    x0_raw = return_array_from_keys(data0, xkey, rkey, comp, mask_0, ii=None)
    x1_raw = return_array_from_keys(data1, xkey, rkey, comp, mask_1, ii=None)

    x0 = x0_raw[match_0] #0.5 * (x0_raw[match_0] + x1_raw[match_1][best_aug_1])

    x0_raw[match_0] = x0
    x1_raw[match_1] = x0[np.argsort(best_aug_1)]

    y0_raw = return_array_from_keys(data0, ykey, rkey, comp, mask_0, ii=None)
    y1_raw = return_array_from_keys(data1, ykey, rkey, comp, mask_1, ii=None)

    y0 = y0_raw[match_0]
    y1 = y1_raw[match_1][best_aug_1]

    c0_raw = return_array_from_keys(data0, ckey, rkey, comp, mask_0, ii=None)
    c1_raw = return_array_from_keys(data1, ckey, rkey, comp, mask_1, ii=None)
    if c0_raw is not None and c1_raw is not None:
        cs = 0.5 * (c0_raw[match_0] + c1_raw[match_1][best_aug_1])
    else:
        cs = np.zeros(len(x0))

    clim = lims_dict[ckey]

    bin0_raw = return_array_from_keys(data0, binkey, rkey, comp, mask_0, ii=None)
    bin1_raw = return_array_from_keys(data1, binkey, rkey, comp, mask_1, ii=None)

    if bin0_raw is not None and bin1_raw is not None:
        bin0 = 0.5 * (bin0_raw[match_0] + bin1_raw[match_1][best_aug_1])

        bin0_raw[match_0] = bin0
        bin1_raw[match_1] = bin0[np.argsort(best_aug_1)]
    else:
        bin0 = np.ones(len(x0), dtype=bool)
        bin0_raw = np.ones(len(x0_raw), dtype=bool)
        bin1_raw = np.ones(len(x1_raw), dtype=bool)

    mask = np.digitize(bin0, bin0_edges)

    mask0_raw = np.digitize(bin0_raw, bin0_edges)
    mask1_raw = np.digitize(bin1_raw, bin0_edges)

    xlims = lims_dict[xkey]

    mx = np.nanquantile(np.abs(y0 - y1)[np.isfinite(y0 - y1)], 0.95) * 2
    ylims = [-mx, mx]

    if type(N_run) == int:
        N_run = np.linspace(*xlims, N_run)

    # if True: #yaspect is None:
        # mx = np.nanmax(np.abs(y0 - y1)[np.isfinite(y0 - y1)])
    # else:
    #     ylims = yaspect

    # fig = plt.figure(figsize=(5.3 * min(len(bin0_edges) - 1, 4), 5.3 * ((len(bin0_edges) + 2) // 4)),
    #                  constrained_layout=True)
    # fig.set_constrained_layout_pads(w_pad=0, h_pad=0, hspace=0, wspace=0)

    nrows = (len(bin0_edges) + 2) // 4
    ncols = min(len(bin0_edges) - 1, 4)
    fig = plt.figure(figsize=(5.3 * 1 * ncols, 5.3 * 5/3 * nrows),
                     constrained_layout=True)
    gs = plt.GridSpec(nrows=nrows*3, ncols=ncols,
                      height_ratios=[1, 1/3, 1/3]*nrows, width_ratios=[1]*ncols)

    for ii in range(len(bin0_edges)-1):

        # ax = plt.subplot((len(bin0_edges) + 2) // 4, min(len(bin0_edges) - 1, 4), ii+1)
        ax = plt.subplot(gs[3 * (ii // 4), ii % 4])

        ax.set_aspect(np.diff(xlims) / np.diff(ylims))

        warnings.filterwarnings("ignore")
        plt.subplots_adjust(wspace=0, hspace=0)

        #plot is heres

        x = x0[mask == ii+1]
        y = y0[mask == ii+1] - y1[mask == ii+1]
        c = cs[mask == ii+1]

        not_crazy_mask = np.logical_and(np.isfinite(x), np.isfinite(y))
        if np.sum(not_crazy_mask) == 0:
            return
        # elif np.sum(not_crazy_mask) < len(not_crazy_mask):
        #     print(f'removed {np.sum(1-not_crazy_mask)} / {len(not_crazy_mask)} infs and nans')
        #     print(np.sum(1-np.isfinite(x)), np.sum(1-np.isfinite(y)))
        #     print(np.sum(1-np.isfinite(y0[mask == ii+1])), np.sum(1-np.isfinite(y1[mask == ii+1])))

        x0_r = x0_raw[mask0_raw == ii+1]
        x1_r = x1_raw[mask1_raw == ii+1]
        y0_r = y0_raw[mask0_raw == ii+1]
        y1_r = y1_raw[mask1_raw == ii+1]

        not_crazy_mask0_raw = np.logical_and(np.isfinite(x0_r), np.isfinite(y0_r))
        not_crazy_mask1_raw = np.logical_and(np.isfinite(x1_r), np.isfinite(y1_r))

        ax.scatter(x, y, s=3, c=c, vmin=clim[0], vmax=clim[1], cmap=cmap)

        ymedians, xedges, bin_is = binned_statistic(x[not_crazy_mask], y[not_crazy_mask],
                                                    statistic=np.nanmedian, bins=N_run)
        yplus, xedges, _ = binned_statistic(x[not_crazy_mask], y[not_crazy_mask],
                                            statistic=lambda x: np.nanquantile(x, 0.16), bins=N_run)
        yminus, xedges, _ = binned_statistic(x[not_crazy_mask], y[not_crazy_mask],
                                             statistic=lambda x: np.nanquantile(x, 0.84), bins=N_run)

        path_effect = [path_effects.Stroke(linewidth=4, foreground='black'), path_effects.Normal()]
        plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), ymedians,
                     c='grey', ls='-', linewidth=2, zorder=10, path_effects=path_effect)
        plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]), yminus, yplus,
                         color='grey', alpha=0.3, edgecolor='k', zorder=9)

        if bootstrap:
            boots = np.zeros((len(xedges) - 1, 3))
            for i in range(len(xedges) - 1):
                boots[i] = my_bootstrap(y[not_crazy_mask][bin_is == i + 1], statistic=np.nanmedian)

            # plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 1],
            #              c=colour, ls=linestyle, linewidth=2, zorder=9.7, path_effects=path_effect)
            plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 0], boots[:, 2],
                             color='grey', alpha=0.5, edgecolor='k', zorder=9.3, hatch='xxx')

        #ratio of medians of unmatched sample
        if True:
            ymedians0_raw, xedges, bin_is = binned_statistic(x0_r[not_crazy_mask0_raw], y0_r[not_crazy_mask0_raw],
                                                             statistic=np.nanmedian, bins=N_run)
            ymedians1_raw, xedges, bin_is = binned_statistic(x1_r[not_crazy_mask1_raw], y1_r[not_crazy_mask1_raw],
                                                            statistic=np.nanmedian, bins=N_run)

            plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), ymedians0_raw - ymedians1_raw,
                         c='k', ls=(0,(0.8,0.8)), linewidth=3, zorder=8)

        #all data (not binned)
        if len(bin0_edges) > 2:
            not_crazy_mask0 = np.logical_and(np.logical_and(np.isfinite(x0), np.isfinite(y0)), np.isfinite(y1))
            ymedians0, xedges, bin_is0 = binned_statistic(x0[not_crazy_mask0], (y0 - y1)[not_crazy_mask0],
                                                          statistic=np.nanmedian, bins=N_run)

            plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), ymedians0,
                         c='magenta', ls='--', linewidth=2, zorder=8.5, path_effects=path_effect)

            # if bootstrap:
            #     boots0 = np.zeros((len(xedges) - 1, 3))
            #     for i in range(len(xedges) - 1):
            #         boots0[i] = my_bootstrap((y0 - y1)[not_crazy_mask0][bin_is0 == i + 1], statistic=np.nanmedian)
            #
            #     # plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), boots[:, 1],
            #     #              c=colour, ls=linestyle, linewidth=2, zorder=9.7, path_effects=path_effect)
            #     plt.fill_between(0.5 * (xedges[1:] + xedges[:-1]), boots0[:, 0], boots0[:, 2],
            #                      color='maroon', alpha=0.5, edgecolor='k', zorder=8.3, hatch=r'////')

        #histograms
        if False:
            hist_bin_edges = np.linspace(*ylims, histbins+1)
            Ny, _ = np.histogram(y, bins=hist_bin_edges)

            hist_max = np.amax(Ny)
            hist_const = 0.3 * (xlims[1] - xlims[0]) / hist_max

            hb = np.repeat(hist_bin_edges, 2)
            hy = hist_const * np.hstack((0, np.repeat(Ny, 2), 0)) + xlims[0]

            ax.errorbar(hy, hb, ls='-', c='C0')

        # #decorations start here
        # padx = 0.05
        # pady = 0.05
        # dy = 0.05
        # bin_label = f'{label_dict[binkey]} = [{round(bin0_edges[ii], 1)} - {round(bin0_edges[ii + 1], 1)}]'
        # ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+0*dy)*(lims[1]-lims[0]),
        #         bin_label, va='top')
        # rlab = rkey_label_dict[rkey][1:-1]
        # ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+1*dy)*(lims[1]-lims[0]),
        #         rlab, va='top')
        # N_label = f'$N$ matched {np.sum(mask==ii+1)}'
        # ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+2*dy)*(lims[1]-lims[0]),
        #         N_label, va='top')
        # N_label = f'$N$ fid. res. {np.sum(mask1_raw==ii+1)} $N$ hi res. {np.sum(mask0_raw==ii+1)}'
        # ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+3*dy)*(lims[1]-lims[0]),
        #         N_label, va='top')
        # corr, pvalue = scipy.stats.spearmanr(x0[mask == ii+1], x1[mask == ii+1], nan_policy='omit')
        # ax.text(lims[0] + padx*(lims[1]-lims[0]), lims[1] - (pady+4*dy)*(lims[1]-lims[0]),
        #         f'Spearman={round(corr, 3)}', va='top')

        if ii % 4 == 0:
            ylab = label_dict[ykey] + ' hi res. - '  + label_dict[ykey] +  ' fid. res.'
            ax.set_ylabel(ylab)
        else:
            ax.set_yticklabels([])

        # if ii // 4 == (len(bin0_edges) -2) // 4:
        #     xlab = label_dict[xkey]
        #     ax.set_xlabel(xlab)
        # else:
        #     ax.set_xticklabels([])
        ax.set_xticklabels([])

        ax.set_xlim(xlims)
        ax.set_ylim(ylims)

        ax.errorbar(xlims, [0,0], ls=':', c='k')
        # ax.errorbar(np.array(lims) + 1, lims, ls=':', c='k')
        # ax.errorbar(np.array(lims) - 1, lims, ls=':', c='k')

        title = 'z=' + snap_str[7:].replace('p', '.')
        if len(bin0_edges) > 2:
            title += '\n' + label_dict[binkey] + ': ' + str(bin0_edges[ii]) + ' - ' + str(round(bin0_edges[ii + 1], 1))

        if len(bin0_edges) > 2 and ii == len(bin0_edges) - 2:
            l = plt.legend([(lines.Line2D([0, 1], [0, 1], color='purple', ls='', marker='.')),
                            (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='magenta', ls='--', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='k', ls=(1,(0.8,0.8)), linewidth=3)),],
                           [r'($\overline{x}_i, \Delta y_i$)', rf'Med. diff. [{label_dict[binkey]} bins]',
                            r'Med. diff. [entire sample]', rf'Diff. med. (inc. unmacthced haloes)'],
                           title=title, ncol=1, )
        elif len(bin0_edges) > 2:
            l = plt.legend([], [],
                           title=title, ncol=1, )
        else:
            l = plt.legend([#(lines.Line2D([0, 1], [0, 1], color='purple', ls='', marker='.')),
                            (lines.Line2D([0, 1], [0, 1], color='grey', ls='-', linewidth=3)),
                            (lines.Line2D([0, 1], [0, 1], color='k', ls=(1,(0.8,0.8)), linewidth=3)),],
                           # [r'$\Delta y_i$', r'Median $\Delta y_i$',
                           #  r'$\Delta$ Median'],
                           [r'Med. match diff.',
                            r'Med. pop. diff.'],
                           title=title, ncol=1, )
        l.set_zorder(-10)
        l.get_title().set_multialignment('center')

        if ckey is not None and ii == len(bin0_edges) - 2:
            # Bbox = ax.get_position()
            # cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])
            # cbar = plt.colorbar(cax=cax)
            # cbar.set_label(label_dict[ckey])
            # print('tried')

            Bbox = ax.get_position()

            cax = plt.axes([Bbox.x1, Bbox.y0, (Bbox.x1 - Bbox.x0) * 0.08, Bbox.y1 - Bbox.y0])

            nticks = 5
            x_ = np.linspace(1, 0, 1001)
            x, _ = np.meshgrid(x_, np.array([0, 1]))
            cax.imshow(x.T, cmap=cmap, aspect=2 / 1001 / 0.08)
            cax.yaxis.set_label_position('right')
            cax.yaxis.tick_right()
            cax.set_yticks(np.linspace(0, 1001, nticks))
            cax.set_yticklabels([round(q, 1) for q in np.linspace(clim[1], clim[0], 5)])
            cax.set_xticks([])
            cax.set_xticklabels([])
            cax.set_ylabel(label_dict[ckey])

        #
        #correlation plot
        ax = plt.subplot(gs[3 * (ii // 4) + 1, ii % 4])
        # ax.set_aspect(np.diff(xlims) / 1 * 1/3)
        ax.set_aspect(np.diff(xlims) / 2 * 1/3)
        # correlations

        digits = np.digitize(x0, N_run)

        y_spearman_corr = np.nan * np.ones(len(N_run)-1)
        y_pearson_corr = np.nan *np.ones(len(N_run)-1)
        y_std = np.nan * np.ones(len(N_run)-1)
        for jj in range(len(N_run)-1):
            x_mask = np.logical_and(np.logical_and(digits == jj + 1, mask == ii + 1),
                                    np.logical_and(np.isfinite(y0), np.isfinite(y1)))

            y_spearman_corr[jj], pvalue = scipy.stats.spearmanr(y0[x_mask], y1[x_mask], nan_policy='omit')
            # x_pearson_corr[jj], pvalue =
            try:
                y_pearson_corr[jj] = scipy.stats.pearsonr(y0[x_mask], y1[x_mask])[0]
            except ValueError:
                pass

            y_std[jj] = np.nanstd((y1-y0)[x_mask])
            # y_std[jj] = np.nanstd((y1-y0)[x_mask])

        plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), y_spearman_corr,
                     c='r', ls='-', linewidth=2)
        plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), y_pearson_corr,
                     c='b', ls='-', linewidth=2)

        plt.legend(['Spearman', 'Pearson'])

        if ii % 4 == 0:
            ylab = r'$\rho$'
            ax.set_ylabel(ylab)
        else:
            ax.set_yticklabels([])

        ax.set_xticklabels([])

        ax.set_xlim(xlims)
        # ax.set_ylim([0,1])
        ax.set_ylim([-1,1])
        ax.axhline(0, 0, 1, ls=':', c='k')

        #dispersion plot
        ax = plt.subplot(gs[3 * (ii // 4) + 2, ii % 4])
        # disp_y0 = np.log10(y_std / ymedians0_raw)

        print(y_std)
        # print(ymedians0_raw)

        disp_y0 = np.log10(y_std) #np.log10(y_std / 10**ymedians0_raw)

        try:
            # not_crazy_mask = np.logical_and(np.isfinite(disp_y0), np.logical_and(disp_y0 < 5, disp_y0 > -5))
            not_crazy_mask = np.isfinite(disp_y0)
            _y_min = np.nanmin(disp_y0[not_crazy_mask])
            _y_max = np.nanmax(disp_y0[not_crazy_mask])
            # ylims = [1.5 * _y_min, 1.5 * _y_max]
            ylims = [_y_min, _y_max]
            ax.set_aspect(np.diff(xlims) / np.diff(ylims) * 1 * 1/3)
        except ValueError:
            ylims = [-1, 1]
            ax.set_aspect(np.diff(xlims) / np.diff(ylims) * 1 * 1/3)

        print(disp_y0)
        print(ylims)

        plt.errorbar(0.5 * (xedges[1:] + xedges[:-1]), disp_y0,
                     c='k', ls='-', linewidth=2)

        if ii % 4 == 0:
            ylab = r'$\log \, \sigma_Y$'
            ax.set_ylabel(ylab)
        else:
            ax.set_yticklabels([])

        if ii // 4 == (len(bin0_edges) -2) // 4:
            xlab = label_dict[xkey]
            ax.set_xlabel(xlab)
        else:
            ax.set_xticklabels([])

        ax.set_xlim(xlims)
        ax.set_ylim(ylims)

    if save:
        filename = '../results/matching/'
        filename += filename_dict[xkey]
        filename += '_' + filename_dict[ykey]
        filename += '_' + filename_dict[rkey]
        filename += '_' + filename_dict[comp]
        if ckey != None:
            filename += '_' + filename_dict[ckey]
        filename += '_cent_' + snap_str
        if len(bin0_edges) > 2:
            filename += '_' + filename_dict[binkey] + f'_{len(bin0_edges) - 1}bins'
        filename += '_diff.png'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return

def make_matched_galaxy_image(matched_groups, matched_subs, data0, data1,
                              VERBOSE=True, save=True, name=None, out_loc=None, nr200 = 1.5):

    #won't have to do this on ozstar
    group_number_0 = data0['GalaxyQuantities/GroupNumber'][()]
    group_number_1 = data1['GalaxyQuantities/GroupNumber'][()]

    sub_number_0 = data0['GalaxyQuantities/SubGroupNumber'][()]
    sub_number_1 = data1['GalaxyQuantities/SubGroupNumber'][()]

    R200_0 = data0['GalaxyQuantities/R200_crit'][()]
    R200_1 = data1['GalaxyQuantities/R200_crit'][()]

    group_cops_0 = data0['GalaxyQuantities/GroupCOP'][()]
    group_cops_1 = data1['GalaxyQuantities/GroupCOP'][()]

    sub_cops_0 = data0['GalaxyQuantities/SubGroupCOP'][()]
    sub_cops_1 = data1['GalaxyQuantities/SubGroupCOP'][()]

    central_indices_0 = np.nonzero(sub_number_0 == 0)[0]
    central_indices_1 = np.nonzero(sub_number_1 == 0)[0]

    #matches
    group_matches_0 = matched_groups['GroupBijectiveMatches/MatchedGroupNumber7x'][()]
    group_matches_1 = matched_groups['GroupBijectiveMatches/MatchedGroupNumber1x'][()]

    sub_group_matches_0 = matched_subs['SubGroupBijectiveMatches/MatchedGroupNumber7x'][()]
    sub_group_matches_0 = matched_subs['SubGroupBijectiveMatches/MatchedGroupNumber1x'][()]
    sub_sub_matches_0 = matched_subs['SubGroupBijectiveMatches/MatchedSubGroupNumber7x'][()]
    sub_sub_matches_0 = matched_subs['SubGroupBijectiveMatches/MatchedSubGroupNumber1x'][()]

    # for group_focus in range(min(10_000):
    for group_focus in [256]:
        central_index_0 = np.logical_and(group_number_0 == group_matches_0[group_focus], sub_number_0 == 0)
        central_index_1 = np.logical_and(group_number_1 == group_matches_1[group_focus], sub_number_1 == 0)

        proj_centre_0 = group_cops_0[central_index_0]
        proj_centre_1 = group_cops_1[central_index_1]
        proj_extent_0 = R200_0[central_index_0] * LITTLE_H / 1000
        proj_extent_1 = R200_1[central_index_1] * LITTLE_H / 1000

        proj_centre = 0.5 * (proj_centre_0 + proj_centre_1)[0]
        proj_extent = nr200 * 0.5 * (proj_extent_0 + proj_extent_1)[0]

        group_proj_mask_0 = vec_pacman_dist2(proj_centre[np.newaxis, :], group_cops_0) < proj_extent**2
        group_proj_mask_1 = vec_pacman_dist2(proj_centre[np.newaxis, :], group_cops_1) < proj_extent**2

        sub_proj_mask_0 = vec_pacman_dist2(proj_centre[np.newaxis, :], sub_cops_0) < proj_extent**2
        sub_proj_mask_1 = vec_pacman_dist2(proj_centre[np.newaxis, :], sub_cops_1) < proj_extent**2

        group_cops_0_masked = group_cops_0[group_proj_mask_0]
        group_cops_1_masked = group_cops_1[group_proj_mask_1]
        sub_cops_0_masked = sub_cops_0[sub_proj_mask_0]
        sub_cops_1_masked = sub_cops_1[sub_proj_mask_1]

        R200_0_masked = R200_0[group_proj_mask_0] * LITTLE_H / 1000
        R200_1_masked = R200_1[group_proj_mask_1] * LITTLE_H / 1000

        #TODO this will be extracted from kdtrees
        with h5.File(f'/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_group272_cutout.hdf5', 'r') as particles1:
            MassGas_1 = particles1['PartType0/Mass'][()]
            PosGas_1 = particles1['PartType0/Coordinates'][()]

            PosDM_1 = particles1['PartType1/Coordinates'][()]

            MassStar_1 = particles1['PartType4/Mass'][()]
            PosStar_1 = particles1['PartType4/Coordinates'][()]
            VelStar_1 = particles1['PartType4/Velocity'][()]

        with h5.File(f'/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_group257_cutout.hdf5', 'r') as particles0:

            MassGas_0 = particles0['PartType0/Mass'][()]
            PosGas_0 = particles0['PartType0/Coordinates'][()]

            PosDM_0 = particles0['PartType1/Coordinates'][()]

            MassStar_0 = particles0['PartType4/Mass'][()]
            PosStar_0 = particles0['PartType4/Coordinates'][()]
            VelStar_0 = particles0['PartType4/Velocity'][()]

        # mask relevant particles
        # 7x
        GasMask_0 = vec_pacman_dist2(proj_centre[np.newaxis, :], PosGas_0) < proj_extent ** 2
        DMMask_0 = vec_pacman_dist2(proj_centre[np.newaxis, :], PosDM_0) < proj_extent ** 2
        StarMask_0 = vec_pacman_dist2(proj_centre[np.newaxis, :], PosStar_0) < proj_extent ** 2

        MassGas_0 = MassGas_0[GasMask_0]
        PosGas_0 = PosGas_0[GasMask_0]

        MassDM_0 = np.ones(np.sum(DMMask_0))  # particles0['PartType0/Mass'][DMMask_0]
        PosDM_0 = PosDM_0[DMMask_0]

        MassStar_0 = MassStar_0[StarMask_0]
        PosStar_0 = PosStar_0[StarMask_0]
        VelStar_0 = VelStar_0[StarMask_0]

        # 1x
        GasMask_1 = vec_pacman_dist2(proj_centre[np.newaxis, :], PosGas_1) < proj_extent ** 2
        DMMask_1 = vec_pacman_dist2(proj_centre[np.newaxis, :], PosDM_1) < proj_extent ** 2
        StarMask_1 = vec_pacman_dist2(proj_centre[np.newaxis, :], PosStar_1) < proj_extent ** 2

        MassGas_1 = MassGas_1[GasMask_1]
        PosGas_1 = PosGas_1[GasMask_1]

        MassDM_1 = np.ones(np.sum(DMMask_1))  # particles1['PartType1/Mass'][DMMask_1]
        PosDM_1 = PosDM_1[DMMask_1]

        MassStar_1 = MassStar_1[StarMask_1]
        PosStar_1 = PosStar_1[StarMask_1]
        VelStar_1 = VelStar_1[StarMask_1]

        better_plot_positions(group_cops_0_masked, group_cops_1_masked, sub_cops_0_masked, sub_cops_1_masked,
                              proj_centre, proj_extent, proj_centre_0, proj_centre_1,
                              R200_0_masked, R200_1_masked,
                              PosGas_0, PosDM_0, PosStar_0, VelStar_0, MassGas_0, MassStar_0,
                              PosGas_1, PosDM_1, PosStar_1, VelStar_1, MassGas_1, MassStar_1,
                              group_focus, save, name, out_loc)

    return


def better_plot_positions(gc_0, gc_1, sc_0, sc_1,
                          proj_centre, proj_extent, proj_centre_0, proj_centre_1,
                          R200_0, R200_1,
                          PosGas_0, PosDM_0, PosStar_0, VelStar_0, MassGas_0, MassStar_0,
                          PosGas_1, PosDM_1, PosStar_1, VelStar_1, MassGas_1, MassStar_1,
                          group_focus=0, save=True, name=None, out_loc=None, nr200 = 1.5):

    figsize0 = (20,20)
    figsize1 = (8,5*8)

    gs = 4**2
    ss = 3**2

    hwg = proj_extent * 0.06
    hws = proj_extent * 0.04
    lwg = 4
    lws = 1

    g_arrow_kwargs = {'shape':'full',
                      'lw':lwg,
                      'length_includes_head':True,
                      'head_width':hwg,
                      'linestyle':'-',
                      'alpha':0.5}
    s_arrow_kwargs = {'shape':'full',
                      'lw':lws,
                      'length_includes_head':True,
                      'head_width':hws,
                      'linestyle':'-',
                      'alpha':0.5}

    # (x0, y0, z0, dx0, dy0, dz0,
    #  x1, y1, z1, dx1, dy1, dz1,
    #  xb0, yb0, zb0, dxb0, dyb0, dzb0,
    #  xb1, yb1, zb1, dxb1, dyb1, dzb1,
    #  x2, y2, z2, dx2, dy2, dz2,
    #  x3, y3, z3, dx3, dy3, dz3,
    #  dr_tot) = get_arrow_data(best_central_0, best_central_1, group_proj_mask_0, group_proj_mask_1,
    #                           group_cops_0, group_cops_1,
    #                           best_0, best_1, sub_proj_mask_0, sub_proj_mask_1, sub_cops_0, sub_cops_1)

    # for i in range(4):
    #     dr_tot[i] = np.sort(dr_tot[i])

    # group_arg = funky_args_paried_sorted(dr_tot[0], dr_tot[1]) #np.argsort(np.hstack((dr_tot[0], dr_tot[1])))
    # sub_arg = funky_args_paried_sorted(dr_tot[2], dr_tot[3]) #np.argsort(np.hstack((dr_tot[2], dr_tot[3])))

    #put togather images
    xsize=200 #pixels

    #make arrays
    xlims = [proj_centre[0] - proj_extent, proj_centre[0] + proj_extent]
    ylims = [proj_centre[1] - proj_extent, proj_centre[1] + proj_extent]
    zlims = [proj_centre[2] - proj_extent, proj_centre[2] + proj_extent]
    gal_lims = [-100 * proj_extent, 100 * proj_extent]

    x_edges = np.linspace(*xlims, xsize)
    y_edges = np.linspace(*ylims, xsize)
    z_edges = np.linspace(*zlims, xsize)

    gal_edges = np.linspace(*gal_lims, xsize)

    #7x
    #DM
    dm_hist_xz_0,_,_ = np.histogram2d(PosDM_0[:, 0], PosDM_0[:, 2], bins = [x_edges, z_edges])
    dm_hist_xy_0,_,_ = np.histogram2d(PosDM_0[:, 0], PosDM_0[:, 1], bins = [x_edges, y_edges])
    dm_hist_zy_0,_,_ = np.histogram2d(PosDM_0[:, 2], PosDM_0[:, 1], bins = [z_edges, y_edges])

    dm_hist_xz_0 = np.flip(np.log10(1 + dm_hist_xz_0.T), axis=0)
    dm_hist_xy_0 = np.flip(np.log10(1 + dm_hist_xy_0.T), axis=0)
    dm_hist_zy_0 = np.flip(np.log10(1 + dm_hist_zy_0.T), axis=0)

    dm_max_c_0 = np.nanmax((dm_hist_xz_0, dm_hist_xy_0, dm_hist_zy_0))
    dm_min_c_0 = np.nanmin((dm_hist_xz_0, dm_hist_xy_0, dm_hist_zy_0))

    dm_hist_xz_0 = (dm_hist_xz_0 - dm_min_c_0) / (dm_max_c_0 - dm_min_c_0)
    dm_hist_xy_0 = (dm_hist_xy_0 - dm_min_c_0) / (dm_max_c_0 - dm_min_c_0)
    dm_hist_zy_0 = (dm_hist_zy_0 - dm_min_c_0) / (dm_max_c_0 - dm_min_c_0)

    #gas
    gas_hist_xz_0,_,_ = np.histogram2d(PosGas_0[:, 0], PosGas_0[:, 2], bins = [x_edges, z_edges])
    gas_hist_xy_0,_,_ = np.histogram2d(PosGas_0[:, 0], PosGas_0[:, 1], bins = [x_edges, y_edges])
    gas_hist_zy_0,_,_ = np.histogram2d(PosGas_0[:, 2], PosGas_0[:, 1], bins = [z_edges, y_edges])

    gas_hist_xz_0 = np.flip(np.log10(1 + gas_hist_xz_0.T), axis=0)
    gas_hist_xy_0 = np.flip(np.log10(1 + gas_hist_xy_0.T), axis=0)
    gas_hist_zy_0 = np.flip(np.log10(1 + gas_hist_zy_0.T), axis=0)

    gas_max_c_0 = np.nanmax((gas_hist_xz_0, gas_hist_xy_0, gas_hist_zy_0))
    gas_min_c_0 = np.nanmin((gas_hist_xz_0, gas_hist_xy_0, gas_hist_zy_0))

    gas_hist_xz_0 = (gas_hist_xz_0 - gas_min_c_0) / (gas_max_c_0 - gas_min_c_0)
    gas_hist_xy_0 = (gas_hist_xy_0 - gas_min_c_0) / (gas_max_c_0 - gas_min_c_0)
    gas_hist_zy_0 = (gas_hist_zy_0 - gas_min_c_0) / (gas_max_c_0 - gas_min_c_0)

    #star
    star_hist_xz_0,_,_ = np.histogram2d(PosStar_0[:, 0], PosStar_0[:, 2], bins = [x_edges, z_edges])
    star_hist_xy_0,_,_ = np.histogram2d(PosStar_0[:, 0], PosStar_0[:, 1], bins = [x_edges, y_edges])
    star_hist_zy_0,_,_ = np.histogram2d(PosStar_0[:, 2], PosStar_0[:, 1], bins = [z_edges, y_edges])

    star_hist_xz_0 = np.flip(np.log10(1 + star_hist_xz_0.T), axis=0)
    star_hist_xy_0 = np.flip(np.log10(1 + star_hist_xy_0.T), axis=0)
    star_hist_zy_0 = np.flip(np.log10(1 + star_hist_zy_0.T), axis=0)

    star_max_c_0 = np.nanmax((star_hist_xz_0, star_hist_xy_0, star_hist_zy_0))
    star_min_c_0 = np.nanmin((star_hist_xz_0, star_hist_xy_0, star_hist_zy_0))

    star_hist_xz_0 = (star_hist_xz_0 - star_min_c_0) / (star_max_c_0 - star_min_c_0)
    star_hist_xy_0 = (star_hist_xy_0 - star_min_c_0) / (star_max_c_0 - star_min_c_0)
    star_hist_zy_0 = (star_hist_zy_0 - star_min_c_0) / (star_max_c_0 - star_min_c_0)

    #1x
    #DM
    dm_hist_xz_1,_,_ = np.histogram2d(PosDM_1[:, 0], PosDM_1[:, 2], bins = [x_edges, z_edges])
    dm_hist_xy_1,_,_ = np.histogram2d(PosDM_1[:, 0], PosDM_1[:, 1], bins = [x_edges, y_edges])
    dm_hist_zy_1,_,_ = np.histogram2d(PosDM_1[:, 2], PosDM_1[:, 1], bins = [z_edges, y_edges])

    dm_hist_xz_1 = np.flip(np.log10(1 + dm_hist_xz_1.T), axis=0)
    dm_hist_xy_1 = np.flip(np.log10(1 + dm_hist_xy_1.T), axis=0)
    dm_hist_zy_1 = np.flip(np.log10(1 + dm_hist_zy_1.T), axis=0)

    dm_max_c_1 = np.nanmax((dm_hist_xz_1, dm_hist_xy_1, dm_hist_zy_1))
    dm_min_c_1 = np.nanmin((dm_hist_xz_1, dm_hist_xy_1, dm_hist_zy_1))

    dm_hist_xz_1 = (dm_hist_xz_1 - dm_min_c_1) / (dm_max_c_1 - dm_min_c_1)
    dm_hist_xy_1 = (dm_hist_xy_1 - dm_min_c_1) / (dm_max_c_1 - dm_min_c_1)
    dm_hist_zy_1 = (dm_hist_zy_1 - dm_min_c_1) / (dm_max_c_1 - dm_min_c_1)

    #gas
    gas_hist_xz_1,_,_ = np.histogram2d(PosGas_1[:, 0], PosGas_1[:, 2], bins = [x_edges, z_edges])
    gas_hist_xy_1,_,_ = np.histogram2d(PosGas_1[:, 0], PosGas_1[:, 1], bins = [x_edges, y_edges])
    gas_hist_zy_1,_,_ = np.histogram2d(PosGas_1[:, 2], PosGas_1[:, 1], bins = [z_edges, y_edges])

    gas_hist_xz_1 = np.flip(np.log10(1 + gas_hist_xz_1.T), axis=0)
    gas_hist_xy_1 = np.flip(np.log10(1 + gas_hist_xy_1.T), axis=0)
    gas_hist_zy_1 = np.flip(np.log10(1 + gas_hist_zy_1.T), axis=0)

    gas_max_c_1 = np.nanmax((gas_hist_xz_1, gas_hist_xy_1, gas_hist_zy_1))
    gas_min_c_1 = np.nanmin((gas_hist_xz_1, gas_hist_xy_1, gas_hist_zy_1))

    gas_hist_xz_1 = (gas_hist_xz_1 - gas_min_c_1) / (gas_max_c_1 - gas_min_c_1)
    gas_hist_xy_1 = (gas_hist_xy_1 - gas_min_c_1) / (gas_max_c_1 - gas_min_c_1)
    gas_hist_zy_1 = (gas_hist_zy_1 - gas_min_c_1) / (gas_max_c_1 - gas_min_c_1)

    #star
    star_hist_xz_1,_,_ = np.histogram2d(PosStar_1[:, 0], PosStar_1[:, 2], bins = [x_edges, z_edges])
    star_hist_xy_1,_,_ = np.histogram2d(PosStar_1[:, 0], PosStar_1[:, 1], bins = [x_edges, y_edges])
    star_hist_zy_1,_,_ = np.histogram2d(PosStar_1[:, 2], PosStar_1[:, 1], bins = [z_edges, y_edges])

    star_hist_xz_1 = np.flip(np.log10(1 + star_hist_xz_1.T), axis=0)
    star_hist_xy_1 = np.flip(np.log10(1 + star_hist_xy_1.T), axis=0)
    star_hist_zy_1 = np.flip(np.log10(1 + star_hist_zy_1.T), axis=0)

    star_max_c_1 = np.nanmax((star_hist_xz_1, star_hist_xy_1, star_hist_zy_1))
    star_min_c_1 = np.nanmin((star_hist_xz_1, star_hist_xy_1, star_hist_zy_1))

    star_hist_xz_1 = (star_hist_xz_1 - star_min_c_1) / (star_max_c_1 - star_min_c_1)
    star_hist_xy_1 = (star_hist_xy_1 - star_min_c_1) / (star_max_c_1 - star_min_c_1)
    star_hist_zy_1 = (star_hist_zy_1 - star_min_c_1) / (star_max_c_1 - star_min_c_1)

    #projections
    #7x
    # real units and centre and everything
    starpos_0 = PosStar_0 - proj_centre_0 - SCALE_A * BOX_SIZE / 2
    starpos_0 %= SCALE_A * BOX_SIZE
    starpos_0 -= SCALE_A * BOX_SIZE / 2
    starpos_0 *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    r = np.linalg.norm(starpos_0, axis=1)
    starmask_0 = (r < 30 * LITTLE_H)

    starvel_0 = VelStar_0 * np.sqrt(SCALE_A)  # km/s
    starmass_0 = MassStar_0 * LITTLE_H  # 10^10 M_sun

    vel_offset_0 = np.sum(starmass_0[starmask_0, np.newaxis] * starvel_0[starmask_0, :],
                          axis=0) / np.sum(starmass_0[starmask_0])
    starvel_0 -= vel_offset_0

    (starpos_0, starvel_0, rotation_0) = align(starpos_0, starvel_0, starmass_0, apature=30)

    #gas
    gaspos_0 = PosGas_0 - proj_centre_0 - SCALE_A * BOX_SIZE / 2
    gaspos_0 %= SCALE_A * BOX_SIZE
    gaspos_0 -= SCALE_A * BOX_SIZE / 2
    gaspos_0 *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    gasmass_0 = MassGas_0 * LITTLE_H  # 10^10 M_sun

    gaspos_0 = rotation_0.apply(gaspos_0)

    #1x
    # real units and centre and everything
    starpos_1 = PosStar_1 - proj_centre_1 - SCALE_A * BOX_SIZE / 2
    starpos_1 %= SCALE_A * BOX_SIZE
    starpos_1 -= SCALE_A * BOX_SIZE / 2
    starpos_1 *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    r = np.linalg.norm(starpos_1, axis=1)
    starmask_1 = (r < 30 * LITTLE_H)

    starvel_1 = VelStar_1 * np.sqrt(SCALE_A)  # km/s
    starmass_1 = MassStar_1 * LITTLE_H  # 10^10 M_sun

    vel_offset_1 = np.sum(starmass_1[starmask_1, np.newaxis] * starvel_1[starmask_1, :],
                          axis=0) / np.sum(starmass_1[starmask_1])
    starvel_1 -= vel_offset_1

    (starpos_1, starvel_1, rotation_1) = align(starpos_1, starvel_1, starmass_1, apature=30)

    #gas
    gaspos_1 = PosGas_1 - proj_centre_1 - SCALE_A * BOX_SIZE / 2
    gaspos_1 %= SCALE_A * BOX_SIZE
    gaspos_1 -= SCALE_A * BOX_SIZE / 2
    gaspos_1 *= 1000 * SCALE_A  #/ LITTLE_H  # to kpc

    gasmass_1 = MassGas_1 * LITTLE_H  # 10^10 M_sun

    gaspos_1 = rotation_1.apply(gaspos_1)

    #ga[gal_lims, gal_lims]
    gas_proj_xz_0,_,_ = np.histogram2d(gaspos_0[:, 0], gaspos_0[:, 2], bins = [gal_edges, gal_edges])
    gas_proj_xy_0,_,_ = np.histogram2d(gaspos_0[:, 0], gaspos_0[:, 1], bins = [gal_edges, gal_edges])
    gas_proj_zy_0,_,_ = np.histogram2d(gaspos_0[:, 2], gaspos_0[:, 1], bins = [gal_edges, gal_edges])

    gas_proj_xz_0 = np.flip(np.log10(1 + gas_proj_xz_0.T), axis=0)
    gas_proj_xy_0 = np.flip(np.log10(1 + gas_proj_xy_0.T), axis=0)
    gas_proj_zy_0 = np.flip(np.log10(1 + gas_proj_zy_0.T), axis=0)

    gas_max_c_proj_0 = np.nanmax((gas_proj_xz_0, gas_proj_xy_0, gas_proj_zy_0))
    gas_min_c_proj_0 = np.nanmin((gas_proj_xz_0, gas_proj_xy_0, gas_proj_zy_0))

    gas_proj_xz_0 = (gas_proj_xz_0 - gas_min_c_proj_0) / (gas_max_c_proj_0 - gas_min_c_proj_0)
    gas_proj_xy_0 = (gas_proj_xy_0 - gas_min_c_proj_0) / (gas_max_c_proj_0 - gas_min_c_proj_0)
    gas_proj_zy_0 = (gas_proj_zy_0 - gas_min_c_proj_0) / (gas_max_c_proj_0 - gas_min_c_proj_0)

    #star
    star_proj_xz_0,_,_ = np.histogram2d(starpos_0[starmask_0, 0], starpos_0[starmask_0, 2], bins = [gal_edges, gal_edges])
    star_proj_xy_0,_,_ = np.histogram2d(starpos_0[starmask_0, 0], starpos_0[starmask_0, 1], bins = [gal_edges, gal_edges])
    star_proj_zy_0,_,_ = np.histogram2d(starpos_0[starmask_0, 2], starpos_0[starmask_0, 1], bins = [gal_edges, gal_edges])

    star_proj_xz_0 = np.flip(np.log10(1 + star_proj_xz_0.T), axis=0)
    star_proj_xy_0 = np.flip(np.log10(1 + star_proj_xy_0.T), axis=0)
    star_proj_zy_0 = np.flip(np.log10(1 + star_proj_zy_0.T), axis=0)

    star_max_c_proj_0 = np.nanmax((star_proj_xz_0, star_proj_xy_0, star_proj_zy_0))
    star_min_c_proj_0 = np.nanmin((star_proj_xz_0, star_proj_xy_0, star_proj_zy_0))

    star_proj_xz_0 = (star_proj_xz_0 - star_min_c_proj_0) / (star_max_c_proj_0 - star_min_c_proj_0)
    star_proj_xy_0 = (star_proj_xy_0 - star_min_c_proj_0) / (star_max_c_proj_0 - star_min_c_proj_0)
    star_proj_zy_0 = (star_proj_zy_0 - star_min_c_proj_0) / (star_max_c_proj_0 - star_min_c_proj_0)

    #1x
    #gas
    gas_proj_xz_1,_,_ = np.histogram2d(gaspos_1[:, 0], gaspos_1[:, 2], bins = [gal_edges, gal_edges])
    gas_proj_xy_1,_,_ = np.histogram2d(gaspos_1[:, 0], gaspos_1[:, 1], bins = [gal_edges, gal_edges])
    gas_proj_zy_1,_,_ = np.histogram2d(gaspos_1[:, 2], gaspos_1[:, 1], bins = [gal_edges, gal_edges])

    gas_proj_xz_1 = np.flip(np.log10(1 + gas_proj_xz_1.T), axis=0)
    gas_proj_xy_1 = np.flip(np.log10(1 + gas_proj_xy_1.T), axis=0)
    gas_proj_zy_1 = np.flip(np.log10(1 + gas_proj_zy_1.T), axis=0)

    gas_max_c_proj_1 = np.nanmax((gas_proj_xz_1, gas_proj_xy_1, gas_proj_zy_1))
    gas_min_c_proj_1 = np.nanmin((gas_proj_xz_1, gas_proj_xy_1, gas_proj_zy_1))

    gas_proj_xz_1 = (gas_proj_xz_1 - gas_min_c_proj_1) / (gas_max_c_proj_1 - gas_min_c_proj_1)
    gas_proj_xy_1 = (gas_proj_xy_1 - gas_min_c_proj_1) / (gas_max_c_proj_1 - gas_min_c_proj_1)
    gas_proj_zy_1 = (gas_proj_zy_1 - gas_min_c_proj_1) / (gas_max_c_proj_1 - gas_min_c_proj_1)

    #star
    star_proj_xz_1,_,_ = np.histogram2d(starpos_1[starmask_1, 0], starpos_1[starmask_1, 2], bins = [gal_edges, gal_edges])
    star_proj_xy_1,_,_ = np.histogram2d(starpos_1[starmask_1, 0], starpos_1[starmask_1, 1], bins = [gal_edges, gal_edges])
    star_proj_zy_1,_,_ = np.histogram2d(starpos_1[starmask_1, 2], starpos_1[starmask_1, 1], bins = [gal_edges, gal_edges])

    star_proj_xz_1 = np.flip(np.log10(1 + star_proj_xz_1.T), axis=0)
    star_proj_xy_1 = np.flip(np.log10(1 + star_proj_xy_1.T), axis=0)
    star_proj_zy_1 = np.flip(np.log10(1 + star_proj_zy_1.T), axis=0)

    star_max_c_proj_1 = np.nanmax((star_proj_xz_1, star_proj_xy_1, star_proj_zy_1))
    star_min_c_proj_1 = np.nanmin((star_proj_xz_1, star_proj_xy_1, star_proj_zy_1))

    star_proj_xz_1 = (star_proj_xz_1 - star_min_c_proj_1) / (star_max_c_proj_1 - star_min_c_proj_1)
    star_proj_xy_1 = (star_proj_xy_1 - star_min_c_proj_1) / (star_max_c_proj_1 - star_min_c_proj_1)
    star_proj_zy_1 = (star_proj_zy_1 - star_min_c_proj_1) / (star_max_c_proj_1 - star_min_c_proj_1)

    #image'
    #DM
    dm_separate_img_xz = np.zeros((*np.shape(dm_hist_xz_1), 3))
    dm_separate_img_xy = np.zeros((*np.shape(dm_hist_xy_1), 3))
    dm_separate_img_zy = np.zeros((*np.shape(dm_hist_zy_1), 3))

    dm_separate_img_xz[:, :, 0] =  dm_hist_xz_1 * 1.2
    dm_separate_img_xy[:, :, 0] =  dm_hist_xy_1 * 1.2
    dm_separate_img_zy[:, :, 0] =  dm_hist_zy_1 * 1.2

    dm_separate_img_xz[:, :, 1] =  (dm_hist_xz_0 + dm_hist_xz_1) * 1.2 / 2
    dm_separate_img_xy[:, :, 1] =  (dm_hist_xy_0 + dm_hist_xy_1) * 1.2 / 2
    dm_separate_img_zy[:, :, 1] =  (dm_hist_zy_0 + dm_hist_zy_1) * 1.2 / 2

    dm_separate_img_xz[:, :, 2] =  dm_hist_xz_0 * 1.2
    dm_separate_img_xy[:, :, 2] =  dm_hist_xy_0 * 1.2
    dm_separate_img_zy[:, :, 2] =  dm_hist_zy_0 * 1.2

    dm_diff_img_xz = np.zeros((*np.shape(dm_hist_xz_1), 3))
    dm_diff_img_xy = np.zeros((*np.shape(dm_hist_xy_1), 3))
    dm_diff_img_zy = np.zeros((*np.shape(dm_hist_zy_1), 3))

    dm_diff_img_xz[:, :, 0] = (dm_hist_xz_1 - dm_hist_xz_0 + 1)/2
    dm_diff_img_xy[:, :, 0] = (dm_hist_xy_1 - dm_hist_xy_0 + 1)/2
    dm_diff_img_zy[:, :, 0] = (dm_hist_zy_1 - dm_hist_zy_0 + 1)/2

    dm_diff_img_xz[:, :, 1] = (dm_hist_xz_1 - dm_hist_xz_0 + 1)/2
    dm_diff_img_xy[:, :, 1] = (dm_hist_xy_1 - dm_hist_xy_0 + 1)/2
    dm_diff_img_zy[:, :, 1] = (dm_hist_zy_1 - dm_hist_zy_0 + 1)/2

    dm_diff_img_xz[:, :, 2] = (dm_hist_xz_1 - dm_hist_xz_0 + 1)/2
    dm_diff_img_xy[:, :, 2] = (dm_hist_xy_1 - dm_hist_xy_0 + 1)/2
    dm_diff_img_zy[:, :, 2] = (dm_hist_zy_1 - dm_hist_zy_0 + 1)/2

    #gas
    gas_separate_img_xz = np.zeros((*np.shape(gas_hist_xz_1), 3))
    gas_separate_img_xy = np.zeros((*np.shape(gas_hist_xy_1), 3))
    gas_separate_img_zy = np.zeros((*np.shape(gas_hist_zy_1), 3))

    gas_separate_img_xz[:, :, 0] =  gas_hist_xz_1 * 1.2
    gas_separate_img_xy[:, :, 0] =  gas_hist_xy_1 * 1.2
    gas_separate_img_zy[:, :, 0] =  gas_hist_zy_1 * 1.2

    gas_separate_img_xz[:, :, 1] =  gas_hist_xz_0 /np.sqrt(2) * 1.2
    gas_separate_img_xy[:, :, 1] =  gas_hist_xy_0 /np.sqrt(2) * 1.2
    gas_separate_img_zy[:, :, 1] =  gas_hist_zy_0 /np.sqrt(2) * 1.2

    gas_separate_img_xz[:, :, 2] =  gas_hist_xz_0 /np.sqrt(2) * 1.2
    gas_separate_img_xy[:, :, 2] =  gas_hist_xy_0 /np.sqrt(2) * 1.2
    gas_separate_img_zy[:, :, 2] =  gas_hist_zy_0 /np.sqrt(2) * 1.2

    #star
    star_separate_img_xz = np.zeros((*np.shape(star_hist_zy_1), 3))
    star_separate_img_xy = np.zeros((*np.shape(star_hist_zy_1), 3))
    star_separate_img_zy = np.zeros((*np.shape(star_hist_zy_1), 3))

    star_separate_img_xz[:, :, 0] =  star_hist_xz_1 * 1.2
    star_separate_img_xy[:, :, 0] =  star_hist_xy_1 * 1.2
    star_separate_img_zy[:, :, 0] =  star_hist_zy_1 * 1.2

    star_separate_img_xz[:, :, 1] =  star_hist_xz_0 * 1.2
    star_separate_img_xy[:, :, 1] =  star_hist_xy_0 * 1.2
    star_separate_img_zy[:, :, 1] =  star_hist_zy_0 * 1.2

    proj_star_0 = np.zeros((*np.shape(star_proj_xz_0), 3))
    proj_gas_0 = np.zeros((*np.shape(gas_proj_xz_0), 3))
    proj_star_1 = np.zeros((*np.shape(star_proj_xz_1), 3))
    proj_gas_1 = np.zeros((*np.shape(gas_proj_xz_1), 3))
    proj_quadrants = np.zeros((*np.shape(star_proj_xz_0), 3))

    proj_star_0[:, :, 1] = star_proj_xz_0 * 1.2
    proj_gas_0[:, :, 1] = gas_proj_xz_0 * 1.2

    proj_star_1[:, :, 0] = star_proj_xz_1 * 1.2

    proj_gas_1[:, :, 0] = gas_proj_xz_1 * 1.2

    proj_quadrants[:, xsize//2:, 0] = star_proj_xz_1[:, xsize//2:] * 1.2
    proj_quadrants[:, :xsize//2, 1] = star_proj_xz_0[:, :xsize//2] * 1.2
    # proj_quadrants[:, xsize//2:, 0] = star_proj_xz_1[:, xsize//2:] * 1.2
    # proj_quadrants[:, :xsize//2, 1] = star_proj_xz_0[:, xsize//2 - 1:] * 1.2

    # proj_quadrants[:, :xsize//2, 2] = star_proj_xz_0[:, :xsize//2] * 0 * 1.2

    # proj_quadrants[:, :, 0] = star_proj_xz_0 * 1.2
    # proj_quadrants[:, :, 1] = star_proj_xz_1 /np.sqrt(2) * 1.2
    # proj_quadrants[:, :, 2] = star_proj_xz_1 /np.sqrt(2) * 1.2

    # proj_quadrants[:, :, 0] = (dm_hist_xz_1 - star_proj_xz_0 + 1)/2
    # proj_quadrants[:, :, 1] = (dm_hist_xz_1 - star_proj_xz_0 + 1)/2
    # proj_quadrants[:, :, 2] = (dm_hist_xz_1 - star_proj_xz_0 + 1)/2

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(gc_0[:, 0], gc_0[:, 2], marker='o', c='C0', s=gs, ec='white')
    ax00.scatter(gc_1[:, 0], gc_1[:, 2], marker='o', c='C1', s=gs, ec='white')

    ax10.scatter(gc_0[:, 0], gc_0[:, 1], marker='o', c='C0', s=gs, ec='white')
    ax10.scatter(gc_1[:, 0], gc_1[:, 1], marker='o', c='C1', s=gs, ec='white')

    ax11.scatter(gc_0[:, 2], gc_0[:, 1], marker='o', c='C0', s=gs, ec='white')
    ax11.scatter(gc_1[:, 2], gc_1[:, 1], marker='o', c='C1', s=gs, ec='white')

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 2], R200_0)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 2], R200_1)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 1], R200_0)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 1], R200_1)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 2], gc_0[:, 1], R200_0)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 2], gc_1[:, 1], R200_1)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')
    #
    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(dm_separate_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(dm_separate_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(dm_separate_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C0', **g_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(xb0, zb0, dxb0, dzb0)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C0', **g_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(xb0, yb0, dxb0, dyb0)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C0', **g_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(zb0, yb0, dzb0, dyb0)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C1', **g_arrow_kwargs)
    #  for x_, z_, dx_, dz_ in zip(xb1, zb1, dxb1, dzb1)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C1', **g_arrow_kwargs)
    #  for x_, y_, dx_, dy_ in zip(xb1, yb1, dxb1, dyb1)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C1', **g_arrow_kwargs)
    #  for z_, y_, dz_, dy_ in zip(zb1, yb1, dzb1, dyb1)]

    if save:
        filename = f'{out_loc}{name}_positions_group{group_focus}_dm_separate'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(gc_0[:, 0], gc_0[:, 2], marker='o', c='C0', s=gs, ec='white')
    ax00.scatter(gc_1[:, 0], gc_1[:, 2], marker='o', c='C1', s=gs, ec='white')

    ax10.scatter(gc_0[:, 0], gc_0[:, 1], marker='o', c='C0', s=gs, ec='white')
    ax10.scatter(gc_1[:, 0], gc_1[:, 1], marker='o', c='C1', s=gs, ec='white')

    ax11.scatter(gc_0[:, 2], gc_0[:, 1], marker='o', c='C0', s=gs, ec='white')
    ax11.scatter(gc_1[:, 2], gc_1[:, 1], marker='o', c='C1', s=gs, ec='white')

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 2], R200_0)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 2], R200_1)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 1], R200_0)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 1], R200_1)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 2], gc_0[:, 1], R200_0)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 2], gc_1[:, 1], R200_1)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')
    #
    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(dm_diff_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(dm_diff_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(dm_diff_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C0', **g_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x0, z0, dx0, dz0)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C0', **g_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x0, y0, dx0, dy0)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C0', **g_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z0, y0, dz0, dy0)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C1', **g_arrow_kwargs)
    #  for x_, z_, dx_, dz_ in zip(x1, z1, dx1, dz1)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C1', **g_arrow_kwargs)
    #  for x_, y_, dx_, dy_ in zip(x1, y1, dx1, dy1)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C1', **g_arrow_kwargs)
    #  for z_, y_, dz_, dy_ in zip(z1, y1, dz1, dy1)]

    if save:
        filename = f'{out_loc}{name}_positions_group{group_focus}_dm_diff'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(sc_0[:, 0], sc_0[:, 2], marker='o', c='C2', s=ss, ec='white')
    ax00.scatter(sc_1[:, 0], sc_1[:, 2], marker='o', c='C3', s=ss, ec='white')

    ax10.scatter(sc_0[:, 0], sc_0[:, 1], marker='o', c='C2', s=ss, ec='white')
    ax10.scatter(sc_1[:, 0], sc_1[:, 1], marker='o', c='C3', s=ss, ec='white')

    ax11.scatter(sc_0[:, 2], sc_0[:, 1], marker='o', c='C2', s=ss, ec='white')
    ax11.scatter(sc_1[:, 2], sc_1[:, 1], marker='o', c='C3', s=ss, ec='white')

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 2], R200_0)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 2], R200_1)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 1], R200_0)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 1], R200_1)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 2], gc_0[:, 1], R200_0)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 2], gc_1[:, 1], R200_1)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')
    #
    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(gas_separate_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(gas_separate_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(gas_separate_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C2', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x2, z2, dx2, dz2)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C2', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x2, y2, dx2, dy2)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C2', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z2, y2, dz2, dy2)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C3', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x3, z3, dx3, dz3)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C3', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x3, y3, dx3, dy3)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C3', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z3, y3, dz3, dy3)]

    if save:
        filename = f'{out_loc}{name}_positions_group{group_focus}_gas_separate'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize0)

    ax00 = plt.subplot(221)
    ax01 = plt.subplot(222)
    ax10 = plt.subplot(223)
    ax11 = plt.subplot(224)

    ax00.set_ylabel(r'$Z$ [Mpc/h]')
    ax10.set_xlabel(r'$X$ [Mpc/h]')
    ax10.set_ylabel(r'$Y$ [Mpc/h]')
    ax11.set_xlabel(r'$Z$ [Mpc/h]')

    ax01.set_xlabel(r'$N$')
    ax01.set_ylabel(r'$\Delta r$ [Mpc/h]')

    ax00.set_aspect('equal')
    # ax01.set_aspect('equal')
    ax10.set_aspect('equal')
    ax11.set_aspect('equal')

    ax00.set_xlim(xlims)
    ax00.set_ylim(zlims)
    ax10.set_xlim(xlims)
    ax10.set_ylim(ylims)
    ax11.set_xlim(zlims)
    ax11.set_ylim(ylims)

    ax01.set_visible(False)

    # ax00.set_xticklabels([])
    # ax11.set_yticklabels([])

    ax00.scatter(sc_0[:, 0], sc_0[:, 2], marker='o', c='C2', s=ss, ec='white')
    ax00.scatter(sc_1[:, 0], sc_1[:, 2], marker='o', c='C3', s=ss, ec='white')

    ax10.scatter(sc_0[:, 0], sc_0[:, 1], marker='o', c='C2', s=ss, ec='white')
    ax10.scatter(sc_1[:, 0], sc_1[:, 1], marker='o', c='C3', s=ss, ec='white')

    ax11.scatter(sc_0[:, 2], sc_0[:, 1], marker='o', c='C2', s=ss, ec='white')
    ax11.scatter(sc_1[:, 2], sc_1[:, 1], marker='o', c='C3', s=ss, ec='white')

    cw = 1
    circs00s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 2], R200_0)]
    [ax00.add_patch(circs00) for circs00 in circs00s0]
    circs00s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 2], R200_1)]
    [ax00.add_patch(circs00) for circs00 in circs00s1]

    circs10s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 0], gc_0[:, 1], R200_0)]
    [ax10.add_patch(circs00) for circs00 in circs10s0]
    circs10s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 0], gc_1[:, 1], R200_1)]
    [ax10.add_patch(circs00) for circs00 in circs10s1]

    circs11s0 = [plt.Circle((x, z), r, color='C0', lw=cw, fill=False)
                for x, z, r in zip(gc_0[:, 2], gc_0[:, 1], R200_0)]
    [ax11.add_patch(circs00) for circs00 in circs11s0]
    circs11s1 = [plt.Circle((x, z), r, color='C1', lw=cw, fill=False)
                for x, z, r in zip(gc_1[:, 2], gc_1[:, 1], R200_1)]
    [ax11.add_patch(circs00) for circs00 in circs11s1]

    # ax01.bar(group_arg[0], dr_tot[0], align='edge', width=1, color='C0')
    # ax01.bar(group_arg[1], dr_tot[1], align='edge', width=1, color='C1')
    # ax01.bar(sub_arg[0] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[2],
    #          align='edge', width=1, color='C2')
    # ax01.bar(sub_arg[1] + len(dr_tot[0]) + len(dr_tot[1]), dr_tot[3],
    #          align='edge', width=1, color='C3')

    # ax01.set_xlim([0, len(dr_tot[3]) + len(dr_tot[2]) + len(dr_tot[1]) + len(dr_tot[0])])
    # ax01.set_ylim([0, 2 * nr200 * proj_extent])

    ax00.imshow(star_separate_img_xz, extent=[*xlims, *zlims])
    ax10.imshow(star_separate_img_xy, extent=[*xlims, *ylims])
    ax11.imshow(star_separate_img_zy, extent=[*zlims, *ylims])

    # [ax00.arrow(x_, z_, dx_, dz_, color='C2', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x2, z2, dx2, dz2)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C2', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x2, y2, dx2, dy2)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C2', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z2, y2, dz2, dy2)]
    # [ax00.arrow(x_, z_, dx_, dz_, color='C3', **s_arrow_kwargs)
    #   for x_, z_, dx_, dz_ in zip(x3, z3, dx3, dz3)]
    # [ax10.arrow(x_, y_, dx_, dy_, color='C3', **s_arrow_kwargs)
    #   for x_, y_, dx_, dy_ in zip(x3, y3, dx3, dy3)]
    # [ax11.arrow(z_, y_, dz_, dy_, color='C3', **s_arrow_kwargs)
    #   for z_, y_, dz_, dy_ in zip(z3, y3, dz3, dy3)]

    if save:
        filename = f'{out_loc}{name}_positions_group{group_focus}_star_separate'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    #plot stars here
    plt.figure(figsize=figsize1)

    ax0 = plt.subplot(511)
    ax1 = plt.subplot(512)
    ax2 = plt.subplot(513)
    ax3 = plt.subplot(514)
    ax4 = plt.subplot(515)

    ax0.set_ylabel(r"$Z'$ [kpc/h]")
    ax1.set_ylabel(r"$Z'$ [kpc/h]")
    ax2.set_ylabel(r"$Z'$ [kpc/h]")
    ax3.set_ylabel(r"$Z'$ [kpc/h]")
    ax4.set_ylabel(r"$Z'$ [kpc/h]")
    ax4.set_xlabel(r"$X'$ [kpc/h]")

    ax0.set_aspect('equal')
    ax1.set_aspect('equal')
    ax2.set_aspect('equal')
    ax3.set_aspect('equal')
    ax4.set_aspect('equal')

    ax0.set_xlim(gal_lims)
    ax0.set_ylim(gal_lims)
    ax1.set_xlim(gal_lims)
    ax1.set_ylim(gal_lims)
    ax2.set_xlim(gal_lims)
    ax2.set_ylim(gal_lims)
    ax3.set_xlim(gal_lims)
    ax3.set_ylim(gal_lims)
    ax4.set_xlim(gal_lims)
    ax4.set_ylim(gal_lims)

    ax0.imshow(proj_star_0, extent=[*gal_lims, *gal_lims])
    ax1.imshow(proj_gas_0, extent=[*gal_lims, *gal_lims])
    ax2.imshow(proj_star_1, extent=[*gal_lims, *gal_lims])
    ax3.imshow(proj_gas_1, extent=[*gal_lims, *gal_lims])
    ax4.imshow(proj_quadrants, extent=[*gal_lims, *gal_lims])

    X, Y = np.meshgrid((gal_edges[1:] + gal_edges[:-1]) / 2, (gal_edges[1:] + gal_edges[:-1]) / 2)
    ax4.contour(X, Y, np.sum(proj_quadrants,axis=2),
                levels = np.linspace(0.4,1.2,5), colors=['white']*5, linewidths=1, linestyles=[':', '-', ':', '-', ':'])

    if save:
        filename = f'{out_loc}{name}_positions_group{group_focus}_star_proj'
        print(filename)
        plt.savefig(filename, bbox_inches='tight')
        plt.close()

    return


def get_arrow_data(best_central_0, best_central_1, group_proj_mask_0, group_proj_mask_1, group_cops_0, group_cops_1,
                   best_0, best_1, sub_proj_mask_0, sub_proj_mask_1, sub_cops_0, sub_cops_1):

    dr_tot = [[], [], [], []]

    x0, y0, z0 = [], [], []
    dx0,dy0,dz0 = [], [], []
    for i, j in enumerate(best_central_0):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_0[i] or group_proj_mask_1[j]):
            if not np.isnan(group_cops_0[i, 0]) and not np.isnan(group_cops_1[j, 0]):
                x0.append(group_cops_0[i, 0])
                y0.append(group_cops_0[i, 1])
                z0.append(group_cops_0[i, 2])
                dx0.append(group_cops_1[j, 0] - group_cops_0[i, 0])
                dy0.append(group_cops_1[j, 1] - group_cops_0[i, 1])
                dz0.append(group_cops_1[j, 2] - group_cops_0[i, 2])

                dr_tot[0].append(np.sqrt(pacman_dist2(group_cops_1[j, :], group_cops_0[i, :])))

    x1, y1, z1 = [], [], []
    dx1,dy1,dz1 = [], [], []
    for i, j in enumerate(best_central_1):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_1[i] or group_proj_mask_0[j]):
            if not np.isnan(group_cops_1[i, 0]) and not np.isnan(group_cops_0[j, 0]):
                x1.append(group_cops_1[i, 0])
                y1.append(group_cops_1[i, 1])
                z1.append(group_cops_1[i, 2])
                dx1.append(group_cops_0[j, 0] - group_cops_1[i, 0])
                dy1.append(group_cops_0[j, 1] - group_cops_1[i, 1])
                dz1.append(group_cops_0[j, 2] - group_cops_1[i, 2])

                dr_tot[1].append(np.sqrt(pacman_dist2(group_cops_0[j, :], group_cops_1[i, :])))

    xb0, yb0, zb0 = [], [], []
    dxb0, dyb0, dzb0 = [], [], []
    for i, j in enumerate(best_central_0):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_0[i] or group_proj_mask_1[j]):
            if best_central_1[j] == i:
                if not np.isnan(group_cops_0[i, 0]) and not np.isnan(group_cops_1[j, 0]):
                    xb0.append(group_cops_0[i, 0])
                    yb0.append(group_cops_0[i, 1])
                    zb0.append(group_cops_0[i, 2])
                    dxb0.append(group_cops_1[j, 0] - group_cops_0[i, 0])
                    dyb0.append(group_cops_1[j, 1] - group_cops_0[i, 1])
                    dzb0.append(group_cops_1[j, 2] - group_cops_0[i, 2])

    xb1, yb1, zb1 = [], [], []
    dxb1, dyb1, dzb1 = [], [], []
    for i, j in enumerate(best_central_1):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (group_proj_mask_1[i] or group_proj_mask_0[j]):
            if best_central_0[j] == i:
                if not np.isnan(group_cops_1[i, 0]) and not np.isnan(group_cops_0[j, 0]):
                    xb1.append(group_cops_1[i, 0])
                    yb1.append(group_cops_1[i, 1])
                    zb1.append(group_cops_1[i, 2])
                    dxb1.append(group_cops_0[j, 0] - group_cops_1[i, 0])
                    dyb1.append(group_cops_0[j, 1] - group_cops_1[i, 1])
                    dzb1.append(group_cops_0[j, 2] - group_cops_1[i, 2])

    x2, y2, z2 = [], [], []
    dx2,dy2,dz2 = [], [], []
    for i, j in enumerate(best_0):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (sub_proj_mask_0[i] or sub_proj_mask_1[j]):
            x2.append(sub_cops_0[i, 0])
            y2.append(sub_cops_0[i, 1])
            z2.append(sub_cops_0[i, 2])
            dx2.append(sub_cops_1[j, 0] - sub_cops_0[i, 0])
            dy2.append(sub_cops_1[j, 1] - sub_cops_0[i, 1])
            dz2.append(sub_cops_1[j, 2] - sub_cops_0[i, 2])

            dr_tot[2].append(np.sqrt(pacman_dist2(sub_cops_1[j, :], sub_cops_0[i, :])))

    x3, y3, z3 = [], [], []
    dx3,dy3,dz3 = [], [], []
    for i, j in enumerate(best_1):
        if i < NO_GROUP_NUMBER and j < NO_GROUP_NUMBER and (sub_proj_mask_1[i] or sub_proj_mask_0[j]):
            x3.append(sub_cops_1[i, 0])
            y3.append(sub_cops_1[i, 1])
            z3.append(sub_cops_1[i, 2])
            dx3.append(sub_cops_0[j, 0] - sub_cops_1[i, 0])
            dy3.append(sub_cops_0[j, 1] - sub_cops_1[i, 1])
            dz3.append(sub_cops_0[j, 2] - sub_cops_1[i, 2])

            dr_tot[3].append(np.sqrt(pacman_dist2(sub_cops_0[j, :], sub_cops_1[i, :])))

    return(x0, y0, z0, dx0, dy0, dz0,
           x1, y1, z1, dx1, dy1, dz1,
           xb0, yb0, zb0, dxb0, dyb0, dzb0,
           xb1, yb1, zb1, dxb1, dyb1, dzb1,
           x2, y2, z2, dx2, dy2, dz2,
           x3, y3, z3, dx3, dy3, dz3,
           dr_tot)

if __name__ == '__main__':

    start_time = time.time()

    save=False

    np.seterr(all='ignore')

    combined_name_pairs = [
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_galaxy_kinematic_profile.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_galaxy_kinematic_profile.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_galaxy_kinematic_profile.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_galaxy_kinematic_profile.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_galaxy_kinematic_profile.hdf5']
    ]

    match_files = ['/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_haloes.hdf5',
                   '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_haloes.hdf5',
                   '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_haloes.hdf5',
                   '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_haloes.hdf5']

    comps = ['Star']  # ['Star', 'Gas', 'DM']
    # xkeys = ['sigma_tot']
    xkeys = ['R12']#['kappa_rot', 'sigma_tot_v200', 'mean_v_phi_v200'] #['sigma_tot_v200'] #'ca', 'z12R12', 'z12R34', 'z12R14'] #key0_dict.keys() #['age']
    ykeys = ['m200'] #None
    rkeys = ['r200'] #['eq_02rs'] # #['lt_5r12'] #['eq_rh_profile'] # 'r200', 'lt_r12', 'eq_R14', 'eq_R12',  'eq_R34'
    ckeys = [None] #None 'age'
    bkeys = ['m200'] #['m200'] #['m200'] #['age']#, ['age'] 'mstar']
    bins  = [[-10, 100]] #[np.linspace(10, 12.5, 11)] #[[8.5, 9, 9.5, 10, 15]] #[[8,15], ]#[[10. , 10.5, 11. , 11.5, 12. , 12.5, 13. , 15. ],] # [[0,2,4,6,8,10,12,UNIVERSE_AGE]] #
             #[ 8. ,  8.5,  9. ,  9.5, 10. , 10.5, 11. , 13. ]]
    #[[0, UNIVERSE_AGE]]# ,  #[[0, 1/6, 0.4, 1]] #[[0, 1], ] #[[0, UNIVERSE_AGE],] # #

    # comps = ['Star']  # ['Star', 'Gas', 'DM']
    # xkeys = ['r12']#['sigma_tot_v200', 'sigma_tot', 'kappa_co', 'kappa_rot']
    # ykeys = ['r12_r200']#['sigma_tot_v200', 'sigma_tot', 'kappa_co', 'kappa_rot']
    # rkeys = ['r200']#, 'r200'] #['lt_5r12'] #['r200_profile'] #['lt_5r12', 'r200'] #['eq_rh_profile'] # 'r200', 'lt_r12', 'eq_R14', 'eq_R12',  'eq_R34'
    # ckeys = ['age'] #['age'] #None 'age'
    # bkeys = ['m200'] #['m200']#, 'mstar']
    # bins  = [[10. , 10.5, 11. , 11.5, 12. , 12.5, 13. , 15. ],]

    # for i in [0]: #[0, 1, 2, 3]:  #
    #     for comp in comps:
    #         for xkey in xkeys:
    #             for rkey in rkeys:
    #                 for binkey, bin_ in zip(bkeys, bins):
    #                     for ckey in ckeys:
    #                         for ykey in ykeys:
    #                             experiment_match_haloes(combined_name_pairs[i][0], combined_name_pairs[i][1], match_files[i],
    #                                                     ykey, xkey, rkey, comp, ckey, binkey, bin_,
    #                                                     save=save, snap_str=combined_name_pairs[i][0][53:65], diagnostics=False)

    # for i in [0]: #, 1, 2, 3]:
    #     for comp in comps:
    #         for xkey in xkeys:
    #             for ykey in ykeys:
    #                 for rkey in rkeys:
    #                     for binkey, bin_ in zip(bkeys, bins):
    #                         for ckey in ckeys:
    #                             experiment_match_haloes(combined_name_pairs[i][0], combined_name_pairs[i][1], match_files[i],
    #                                                     ykey, xkey, rkey, comp, ckey, binkey, bin_,
    #                                                     snap_str = combined_name_pairs[i][0][53:65],
    #                                                     save=save, diagnostics=True)

    combined_name_pairs = [
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/028_z000p000_some_subhalo_properties.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_some_subhalo_properties.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/023_z000p503_some_subhalo_properties.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_some_subhalo_properties.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/019_z001p004_some_subhalo_properties.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_some_subhalo_properties.hdf5'],
        ['/home/matt/Documents/UWA/EAGLE/L0050N752/REF_7x752dm/015_z002p012_some_subhalo_properties.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_some_subhalo_properties.hdf5']
    ]

    matched_groups_files = [
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_groups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_groups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_groups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_groups_only_25.hdf5'
    ]
    matched_subs_files = [
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/028_z000p000_matched_subgroups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/023_z000p503_matched_subgroups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/019_z001p004_matched_subgroups_only_25.hdf5',
        '/home/matt/Documents/UWA/EAGLE/L0050N752/REFERENCE/015_z002p012_matched_subgroups_only_25.hdf5'
    ]

    for i in [0,1,2,3]:
        with h5.File(combined_name_pairs[i][0], 'r') as data0:
            with h5.File(combined_name_pairs[i][1], 'r') as data1:
                with h5.File(match_files[i], 'r') as matches:

                    full_match_stats(data0, data1, matches, VERBOSE=True,
                                     save=save, name=match_files[i][51:63], outfile=matched_groups_files[i])

    # with h5.File(combined_name_pairs[0][0], 'r') as data0:
    #     with h5.File(combined_name_pairs[0][1], 'r') as data1:
    #         with h5.File(matched_groups_files[0], 'r') as matched_groups:
    #             with h5.File(matched_subs_files[0], 'r') as matched_subs:
    #                 make_matched_galaxy_image(matched_groups, matched_subs, data0, data1,
    #                                           VERBOSE=True, save=save,
    #                                           name=matched_groups_files[0][51:63], out_loc='../results/matching/')

    plt.show()