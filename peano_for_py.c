#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
#include "allvars.h"
#include "proto.h"
#include "domain.h"*/


/*! \file peano.c
 *  \brief constructs Peano-Hilbert ordering of particles
 */

//typedef __int128_t peanokey;
//typedef long long int peanokey;
typedef u_int64_t peanokey;

static char quadrants[24][2][2][2] = {
  /* rotx=0, roty=0-3 */
  {{{0, 7}, {1, 6}}, {{3, 4}, {2, 5}}},
  {{{7, 4}, {6, 5}}, {{0, 3}, {1, 2}}},
  {{{4, 3}, {5, 2}}, {{7, 0}, {6, 1}}},
  {{{3, 0}, {2, 1}}, {{4, 7}, {5, 6}}},
  /* rotx=1, roty=0-3 */
  {{{1, 0}, {6, 7}}, {{2, 3}, {5, 4}}},
  {{{0, 3}, {7, 4}}, {{1, 2}, {6, 5}}},
  {{{3, 2}, {4, 5}}, {{0, 1}, {7, 6}}},
  {{{2, 1}, {5, 6}}, {{3, 0}, {4, 7}}},
  /* rotx=2, roty=0-3 */
  {{{6, 1}, {7, 0}}, {{5, 2}, {4, 3}}},
  {{{1, 2}, {0, 3}}, {{6, 5}, {7, 4}}},
  {{{2, 5}, {3, 4}}, {{1, 6}, {0, 7}}},
  {{{5, 6}, {4, 7}}, {{2, 1}, {3, 0}}},
  /* rotx=3, roty=0-3 */
  {{{7, 6}, {0, 1}}, {{4, 5}, {3, 2}}},
  {{{6, 5}, {1, 2}}, {{7, 4}, {0, 3}}},
  {{{5, 4}, {2, 3}}, {{6, 7}, {1, 0}}},
  {{{4, 7}, {3, 0}}, {{5, 6}, {2, 1}}},
  /* rotx=4, roty=0-3 */
  {{{6, 7}, {5, 4}}, {{1, 0}, {2, 3}}},
  {{{7, 0}, {4, 3}}, {{6, 1}, {5, 2}}},
  {{{0, 1}, {3, 2}}, {{7, 6}, {4, 5}}},
  {{{1, 6}, {2, 5}}, {{0, 7}, {3, 4}}},
  /* rotx=5, roty=0-3 */
  {{{2, 3}, {1, 0}}, {{5, 4}, {6, 7}}},
  {{{3, 4}, {0, 7}}, {{2, 5}, {1, 6}}},
  {{{4, 5}, {7, 6}}, {{3, 2}, {0, 1}}},
  {{{5, 2}, {6, 1}}, {{4, 3}, {7, 0}}}
};


static char rotxmap_table[24] = { 4, 5, 6, 7, 8, 9, 10, 11,
  12, 13, 14, 15, 0, 1, 2, 3, 17, 18, 19, 16, 23, 20, 21, 22
};

static char rotymap_table[24] = { 1, 2, 3, 0, 16, 17, 18, 19,
  11, 8, 9, 10, 22, 23, 20, 21, 14, 15, 12, 13, 4, 5, 6, 7
};

static char rotx_table[8] = { 3, 0, 0, 2, 2, 0, 0, 1 };
static char roty_table[8] = { 0, 1, 1, 2, 2, 3, 3, 0 };

static char sense_table[8] = { -1, -1, -1, +1, +1, -1, -1, -1 };

static int flag_quadrants_inverse = 1;
static char quadrants_inverse_x[24][8];
static char quadrants_inverse_y[24][8];
static char quadrants_inverse_z[24][8];

void peano_hilbert_key_inverse_(u_int64_t *key, long long int bits, long long int *x, long long int *y, long long int *z, long long int size_of)
{
  long long int i, j, keypart, bitx, bity, bitz, mask, quad,rotation;
  long long int shift;
  long long int seven;
  char sense, rotx, roty;

  if(flag_quadrants_inverse)
    {
      flag_quadrants_inverse = 0;
      for(rotation = 0; rotation < 24; rotation++)
	for(bitx = 0; bitx < 2; bitx++)
	  for(bity = 0; bity < 2; bity++)
	    for(bitz = 0; bitz < 2; bitz++)
	      {
		quad = quadrants[rotation][bitx][bity][bitz];
		quadrants_inverse_x[rotation][quad] = bitx;
		quadrants_inverse_y[rotation][quad] = bity;
		quadrants_inverse_z[rotation][quad] = bitz;
	      }
    }

  for (j = 0; j < size_of; j++)
  {  
    x[j] = 0;
    y[j] = 0;
    z[j] = 0;
    key[j] = key[j] >> 1;

    seven = 7;

    shift = 3 * (bits - 1);

    mask = seven << shift;

    rotation = 0;
    sense = 1;

    for(i = 0; i < bits; i++, mask >>= 3, shift -= 3)
    {
      keypart = (key[j] & mask) >> shift;

      quad = (sense == 1) ? (keypart) : (7 - keypart);

      x[j] = (x[j] << 1) + quadrants_inverse_x[rotation][quad];
      y[j] = (y[j] << 1) + quadrants_inverse_y[rotation][quad];
      z[j] = (z[j] << 1) + quadrants_inverse_z[rotation][quad];

      rotx = rotx_table[quad];
      roty = roty_table[quad];
      sense *= sense_table[quad];

      while(rotx > 0)
      {
        rotation = rotxmap_table[rotation];
        rotx--;
      }

      while(roty > 0)
      {
        rotation = rotymap_table[rotation];
	    roty--;
      }
    }
  }
}
