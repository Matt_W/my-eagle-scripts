#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 20:28:08 2020

@author: matt
"""

import os
# import time

import numpy as np
# import h5py as h5

import h5py as h5

from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.interpolate import interp1d
from scipy.interpolate import interpn

from scipy.integrate   import quad

from scipy.optimize    import minimize
from scipy.optimize    import brentq

from scipy.signal      import savgol_filter

from scipy.spatial.transform import Rotation

from scipy.special     import gamma
from scipy.special     import lambertw
from scipy.special     import erf

#matplotlib
import matplotlib.pyplot as plt
import splotch as splt

from matplotlib.colors import LogNorm
from matplotlib.colors import ListedColormap
from matplotlib.cm     import viridis
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#for gifs
import imageio

#for getting argument names of funtions
import inspect

#my files
import halo_calculations as halo
import load_EAGLE

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

#best way to visualize galaxies
'''
SELECT
  Redshift,
  -- GalaxyID,
  MassType_Gas,
  StarFormationRate,
  -- BlackHoleMassAccretionRate,
  Image_face,
  Image_edge,
  Image_box
FROM
  RefL0100N1504_Subhalo
WHERE
  TopLeafID = 16636222
ORDER BY
  Redshift
'''

grav_const   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)

def histOutline(dataIn, *args, **kwargs):
  (histIn, binsIn) = np.histogram(dataIn, *args, **kwargs)

  stepSize = binsIn[1] - binsIn[0]

  bins = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
  data = np.zeros(len(binsIn)*2 + 2, dtype=np.float)
  for bb in range(len(binsIn)):
    bins[2*bb + 1] = binsIn[bb]
    bins[2*bb + 2] = binsIn[bb] + stepSize
    if bb < len(histIn):
      data[2*bb + 1] = histIn[bb]
      data[2*bb + 2] = histIn[bb]

  bins[0] = bins[1]
  bins[-1] = bins[-2]
  data[0] = 0
  data[-1] = 0

  return (bins, data)

###############################################################################
#change coordinates
def get_cylindrical(star_pos, star_vel):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(star_pos[:,0]**2 + star_pos[:,1]**2)
  varphi   = np.arctan2(star_pos[:,1], star_pos[:,0])
  z        = star_pos[:,2]

  v_rho    = star_vel[:,0] * np.cos(varphi) + star_vel[:,1] * np.sin(varphi)
  v_varphi = -star_vel[:,0]* np.sin(varphi) + star_vel[:,1] * np.cos(varphi)

  # v_rho    = (star_pos[:,0] * star_vel[:,0] + star_pos[:,1] * star_vel[:,1]
  #             ) / np.sqrt(star_pos[:,0]**2 + star_pos[:,1]**2)
  # v_varphi = (star_pos[:,0] * star_vel[:,1] - star_pos[:,1] * star_vel[:,0]
  #             ) / np.sqrt(star_pos[:,0]**2 + star_pos[:,1]**2)

  v_z      = star_vel[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

def get_equal_number_bins(x, n_per_bin):
  '''
  '''
  n_per_bin = int(np.round(n_per_bin))
  x_argsort = np.argsort(x)
  #make bin edges half way between point [i * n_per_bin -1] and [i * n_per_bin]
  edges = [0.5*(x[x_argsort][i*n_per_bin-1] + x[x_argsort][i*n_per_bin])
           for i in range(int(np.ceil(len(x)/n_per_bin)))]
  edges[0] = np.amin(x)
  edges.append(np.amax(x))
  edges = np.array(edges)
  return(edges)

def my_density_plot(x, y, bins=None, ax=None,
                    v_transition=None, vmax=None, s=2, cmap=plt.cm.viridis):
  '''
  '''
  if np.all(bins == None):
    bins = int(np.ceil(min((np.sqrt(len(x)/(10)), 200))))

  data, x_e, y_e = np.histogram2d(x, y, bins=bins, density=False)
  data = np.log10(data+1)

  if vmax == None:
    vmax = np.amax(data)
  if v_transition == None:
    # v_transition = np.log10(50)
    v_transition = max(np.log10(100/len(x_e)*len(y_e)), 1.7)
    # print(v_transition)

  z = interpn((0.5*(x_e[1:] + x_e[:-1]), 0.5*(y_e[1:]+y_e[:-1])), data,
              np.vstack([x, y]).T, bounds_error=False, fill_value=None,
              method='nearest')

  #only turn into hist2d if it doesn't look too bad
  if (x_e[1] - x_e[0]) / (x_e[-1] - x_e[0]) < 0.03:

    #only show the ones on the outskirts
    low_mask = np.less_equal(z, v_transition)
    # print(np.sum(low_mask)) #on the order of 5000.
    x_low, y_low, z_low = x[low_mask], y[low_mask], z[low_mask]

    if ax != None:
      ax.scatter(x_low, y_low, c=z_low, s=s, zorder=-1, rasterized=True,
                 vmin=0,vmax=vmax, cmap=cmap)
    else:
      plt.scatter(x_low, y_low, c=z_low, s=s, zorder=-1, rasterized=True,
                  vmin=0,vmax=vmax, cmap=cmap)

    #make a colormap that is transparent under a density threshold
    viridis_alpha = cmap(np.arange(cmap.N))

    c = min(v_transition/vmax, 1)
    viridis_alpha[:,-1] = np.hstack((np.zeros(int(np.floor(c*cmap.N))),
                                    1*np.ones(int(np.ceil((1-c)*cmap.N)))))
    viridis_alpha = ListedColormap(viridis_alpha)

    if ax != None:
      ax.pcolor(x_e, y_e, data.T, #edgecolor='',
                vmin=0,vmax=vmax, cmap=viridis_alpha)
    else:
      plt.pcolor(x_e, y_e, data.T, #edgecolor='',
                 vmin=0,vmax=vmax, cmap=viridis_alpha)

  else:
    if ax != None:
      ax.scatter(x, y, c=z, s=s, zorder=-1, rasterized=True,
                 vmin=0,vmax=vmax, cmap=plt.cm.viridis)
    else:
      plt.scatter(x, y, c=z, s=s, zorder=-1, rasterized=True,
                  vmin=0,vmax=vmax, cmap=plt.cm.viridis)

  return(vmax)

'''
###############################################################################
######################## single galaxy, single snapshot #######################
###############################################################################
'''
#TODO write better
def plot_projection(star_pos, thirty_kpc=30, name='', snap='', save=False):
  '''Depreciated
  '''
  xlim=[-thirty_kpc,thirty_kpc]
  ylim=xlim
  zlim=xlim
  bins = np.linspace(xlim[0], xlim[1], 81)

  fig, ax = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True,
                         figsize=(5, 8))

  max_height = 100
  min_height = 1

  splt.hist2D(star_pos[:,0], star_pos[:,1],
              clog=True, clim=[min_height,max_height], xlim=xlim, ylim=ylim,
              bins=bins, ax=ax[0,0], dens=False)
  splt.hist2D(star_pos[:,0], star_pos[:,2],
              clog=True, clim=[min_height,max_height], xlim=xlim, ylim=zlim,
              bins=bins, ax=ax[1,0], dens=False)
  splt.hist2D(star_pos[:,1], star_pos[:,2],
              clog=True, clim=[min_height,max_height], xlim=xlim, ylim=zlim,
              bins=bins, ax=ax[1,1], dens=False)

  ax[0,0].set_ylabel('y [kpc]')
  ax[1,0].set_ylabel('z [kpc]')
  ax[1,0].set_xlabel('x [kpc]')
  ax[1,1].set_xlabel('y [kpc]')

  ax[0,0].set_aspect('equal')
  ax[1,0].set_aspect('equal')
  ax[1,1].set_aspect('equal')

  ax[0,1].set_visible(False)

  cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
  plt.colorbar(cax=cbar_ax)

  fig.canvas.draw()
  image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
  image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

  if save:
    fname = '../galaxies/' + name + '/results/position_projection_' + snap
    plt.savefig(fname, bbox_inches="tight")
    plt.close()

  return(image)

def plot_jzonc(star_j_c, star_pos, star_vel, star_mass,
               name='', snap='', save=False, ax=None):
  '''
  '''
  if ax==None:
    fig, ax = plt.subplots()

  #assumed already aligned
  star_j_z = np.cross(star_pos, star_vel)[:,2]

  jzonc = star_j_z / star_j_c

  if ax.lines == []:
    c='C0'
  # elif time == 10:
  #   c='C1'
  else:
    c='C1'

  ax.hist(jzonc, bins=np.linspace(-1.5,1.5,61), label='_nolegend_',
          weights=star_mass, alpha=0.5, color=c)

  (bins, n) = histOutline(jzonc, bins=np.linspace(-1.5,1.5,61),
                          weights=star_mass)
  ax.errorbar(bins, n, c=c)

  ax.set_xlabel(r'$j_z/j_c(E)$')
  ax.set_ylabel(r'$M_*$ [$10^{10} $M$_\odot$]')

  ax.grid(b=True)
  ax.set_xlim((-1.5,1.5))
  ax.set_ylim((0,1.7))

  if save:
    fname = '../galaxies/' + name + '/results/j_zonc_hist_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()
  return

def plot_jz_e(star_pos, star_vel, star_mass, star_bind,
              binding_e, j_circ,
              hexbins=False, square=False, kde=False,
              name='', snap='', save=False, ax=None):
  '''
  '''
  if ax==None:
    fig, ax = plt.subplots()

  #assumed already aligned
  star_j_z = np.cross(star_pos, star_vel)[:,2]

  x = star_j_z/10**3
  y = star_bind/10**5

  xmax = np.amax(np.abs(x))
  ymax = np.amax(y)
  ymin = np.amin(y)

  n_bin_edges = 200 #int(np.ceil(min((np.sqrt(len(x)/(10)), 200))))
  bins = [np.linspace(-xmax, xmax, n_bin_edges),
          np.linspace(ymin, ymax, n_bin_edges)]

  my_density_plot(x, y, bins=bins, ax=ax, v_transition=None)

  ax.errorbar(j_circ/10**3,  binding_e/10**5, fmt='',ls='-.', c='r',
              alpha=0.5, label=r'$j_z/j_c = \pm 1$')
  ax.errorbar(-j_circ/10**3, binding_e/10**5, fmt='',ls='-.', c='r',
              alpha=0.5)

  ax.set_xlabel(r'$j_z$ [$10^3$ kpc km/s]')
  ax.set_ylabel(r'E [$10^5$ (km/s)$^2$]')
  ax.legend(loc='lower left')

  ax.set_xlim((-xmax, xmax))
  ax.set_ylim((ymin, ymax))

  ax.grid()

  if save:
    fname = '../galaxies/' + name + '/results/jz_e_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_jzonc_r(star_j_c, star_pos, star_vel, star_mass,
                 name='', snap='', save=False, ax=None, fifty=50):
  '''
  '''
  if ax==None:
    fig, ax = plt.subplots()

  #assumed already aligned
  star_j_z = np.cross(star_pos, star_vel)[:,2]

  star_r = np.linalg.norm(star_pos, axis=1)

  n_bin_edges = 200 #int(np.ceil(min((np.sqrt(len(r)/(10)), 200))))
  bins = [np.linspace(0,fifty, n_bin_edges), np.linspace(-1.5,1.5, n_bin_edges)]

  my_density_plot(star_r, star_j_z/star_j_c, bins=bins, ax=ax, v_transition=None)

  ax.errorbar([0,np.amax(star_r)], [1,1], fmt='',  ls=':', c='k',
              label=r'$j_z/j_c(E) = 1$')

  ax.set_xlabel(r'r [kpc]')
  ax.set_ylabel(r'$j_z/j_c(E)$')
  ax.legend()

  # ax.set_xticks([0,20,40])
  # ax.set_xticklabels([0,20,40])
  # ax.set_yticks([-1,0,1])
  # ax.set_yticklabels([-1,0,1])
  ax.grid()
  ax.set_xlim((0,fifty))
  ax.set_ylim((-1.5,1.5))

  if save:
    fname = '../galaxies/' + name + '/results/r_jzonc_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

def plot_density_profile(star_pos, MassStar,
                         name='', snap='', save=False, ax=None):
  '''
  '''
  R = np.linalg.norm((star_pos[:,0], star_pos[:,1]), axis=0)
  #get bins
  n_per_bin = np.sqrt(len(R))*2
  bins = get_equal_number_bins(R, n_per_bin) # * u.kpc

  # bins = np.arange(0,50,20/np.sqrt(len(R)))

  #calculate surface density
  bin_count, _ = np.histogram(R, bins=bins)
  bin_area = np.pi * (np.roll(bins, -1)**2 - bins**2)[:-1]
  #maybe should be mean of points in each bin
  bin_R = 0.5 * (bins[:-1] + bins[1:])
  surface_density = MassStar * bin_count / bin_area * 10**10 # M_sun / kpc^2

  R_sort = np.argsort(R)
  if len(R)%2 == 0:
    half_mass_radii = (R[R_sort][len(R)>>1] + R[R_sort][(len(R)>>1)-1])/2
  else:
    half_mass_radii = R[R_sort][int(len(R)/2)]

  if ax == None:
    fig, ax = plt.subplots()

  bar = ax.errorbar(bin_R, np.log10(surface_density), ls='-', fmt='', alpha=0.5,
                    yerr=np.array((np.log10(1+1/np.sqrt(bin_count+1)),
                                   np.log10(1-1/np.sqrt(bin_count+1)))))
  ax.fill_between(bin_R, np.log10(surface_density*(1+1/np.sqrt(bin_count+1))),
                         np.log10(surface_density*(1-1/np.sqrt(bin_count+1))),
                         alpha=0.5)

  ax.vlines(half_mass_radii, 5,9, ls=':', color=bar[0].get_color())

  ax.set_xlabel(r'R [kpc]')
  ax.set_ylabel(r'log $\Sigma$ [$M_\odot$/kpc$^2$]')

  ax.set_xticks([0,5,10,15,20,30,40,50])
  ax.set_xticklabels([0,5,10,15,'','','',''])
  ax.set_yticks([5,6,7,8,9])
  ax.set_yticklabels(['',6,7,8,''])
  ax.grid()
  ax.set_xlim((0,20))
  ax.set_ylim((5,9))

  if save:
    fname = '../galaxies/' + name + '/results/density_profile_' + snap
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

def plot_dispersions(star_pos, star_vel, j_z, j_p, j_c,
                     j_c_interp, R_bins,
                     name='', snap='', save=False, ax=None):
  '''Not kept up
  '''
  rho, varphi, z, v_rho, v_varphi, v_z = get_cylindrical(star_pos, star_vel)
  j_zonc = j_z/j_c
  # j_zonc = np.sqrt(j_z**2 + j_p**2)/j_c


  n=10
  bin_edges = np.arange(n+1)
  bin_centres = 0.5 * (bin_edges[:-1] + bin_edges[1:])
  bin_mask  = np.digitize(rho, bin_edges)

  disp_v_rho    = np.zeros(n)
  disp_v_varphi = np.zeros(n)
  disp_v_z      = np.zeros(n)

  disp_v_x = np.zeros(n)
  disp_v_y = np.zeros(n)

  bin_j_zonc    = np.zeros(n)

  interp = InterpolatedUnivariateSpline(R_bins, j_c_interp/R_bins, k=1)
  v_circ = interp(bin_centres)

  for i in range(n):

    mask = bin_mask == i

    disp_v_rho[i]    = np.std(v_rho[mask])
    disp_v_varphi[i] = np.std(v_varphi[mask])
    disp_v_z[i]      = np.std(v_z[mask])

    disp_v_x[i]      = np.std(star_vel[mask,0])
    disp_v_y[i]      = np.std(star_vel[mask,1])

    bin_j_zonc[i] = np.mean(j_zonc[mask])

  plt.figure()
  plt.errorbar(bin_centres, disp_v_rho,    fmt='*', ls='-.', c='C0',
               label=r'$\sigma_{v_\rho}$')
  plt.errorbar(bin_centres, disp_v_varphi, fmt='o', ls='--', c='C1',
               label=r'$\sigma_{v_\varphi}$')
  plt.errorbar(bin_centres, disp_v_z,      fmt='x', ls='-',  c='C2',
               label=r'$\sigma_{v_z}$')

  plt.errorbar(bin_centres, disp_v_x,      fmt='+', ls=':',  c='C3',
               label=r'$\sigma_{v_x}$')
  plt.errorbar(bin_centres, disp_v_y,      fmt='s', ls=':',  c='C4',
               label=r'$\sigma_{v_y}$')

  plt.xlabel(r'$\rho$ [kpc]')
  plt.ylabel(r'$\sigma_X$ [km/s]')
  # plt.xlim((0,1.1))
  plt.legend()

  plt.figure()
  plt.errorbar(bin_j_zonc, disp_v_rho/v_circ,    fmt='*', ls='-.', c='C0',
               label=r'$\sigma_{v_\rho}$')
  plt.errorbar(bin_j_zonc, disp_v_varphi/v_circ, fmt='o', ls='--', c='C1',
               label=r'$\sigma_{v_\varphi}$')
  plt.errorbar(bin_j_zonc, disp_v_z/v_circ,      fmt='x', ls='-',  c='C2',
               label=r'$\sigma_{v_z}$')

  plt.errorbar(bin_j_zonc, disp_v_x/v_circ,      fmt='+', ls=':',  c='C3',
                label=r'$\sigma_{v_x}$')
  plt.errorbar(bin_j_zonc, disp_v_y/v_circ,      fmt='s', ls=':',  c='C4',
                label=r'$\sigma_{v_y}$')

  e = np.linspace(0, 1)
  plt.errorbar(e, np.sqrt(1-e))

  plt.xlabel(r'$\langle j_z / j_c \rangle$')
  plt.ylabel(r'$\sigma_X/v_c$ [km/s]')
  plt.xlim((0,1.2))
  plt.ylim((0,1.2))
  plt.legend()

  return

def plot_stellar_exponential(star_pos, star_mass, ax=None):
  '''
  '''
  star_R = np.linalg.norm((star_pos[:,0], star_pos[:,1]), axis=0)

  #get bins
  n_per_bin = np.sqrt(len(star_R))*2
  bins = get_equal_number_bins(star_R, n_per_bin) # * u.kpc

  # bins = np.arange(0,50,20/np.sqrt(len(R)))

  #calculate surface density
  bin_count, _ = np.histogram(star_R, bins=bins)
  bin_mass, _  = np.histogram(star_R, bins=bins, weights=star_mass)

  bin_area = np.pi * (np.roll(bins, -1)**2 - bins**2)[:-1]
  #maybe should be mean of points in each bin
  bin_R = 0.5 * (bins[:-1] + bins[1:])
  surface_density = bin_count / bin_area * 10**10 # M_sun / kpc^2

  if ax == None:
    fig, ax = plt.subplots()

  ax.errorbar(bin_R, np.log10(surface_density), ls='', fmt='o', alpha=0.5, capsize=3,
              yerr=1/np.sqrt(bin_count))
  # ax.fill_between(bin_R, np.log10(surface_density*(1+1/np.sqrt(bin_count+1))),
  #                        np.log10(surface_density*(1-1/np.sqrt(bin_count+1))),
  #                        alpha=0.5)

  #work out best fit
  likely = lambda R_d : -np.sum( np.log( star_R / np.square(R_d) * np.exp(
    -star_R / R_d ) ))

  R_d = minimize(likely, 1)
  R_d = R_d.x[0]

  intercept = lambda i_0 : np.sum(np.square(i_0 * np.exp(-bin_R/R_d) -
                                            surface_density))
  i_0 = minimize(intercept, surface_density[0])
  i_0 = i_0.x[0]

  ax.errorbar(bin_R, np.log10(i_0 * np.exp(-bin_R/R_d)), ls='--')

  ax.set_xlabel('Projected Radii [kpc]')
  ax.set_ylabel(r'$\log (\Sigma$ [M$_\odot /$ kpc$^2$]$)$')
  ax.set_xlim((0,20))
  ax.set_ylim((8,14))

  return

'''
###############################################################################
######################## all galaxies, single snapshot ########################
###############################################################################
'''
#TODO make generic function work for plots vs time / plots that load data
def plot_single_snapshot_all_galaxies_generic(plot_function, plot_name, time=0,
                                              figsize=(10,15), save=False,
                                              axs=None):
  '''
  '''
  #sims to look at
  muse  = ['1', '5', '25']
  Mdm   = ['8p0', '7p5', '7p0', '6p5', '6p0']
  Mdmstr= ['8.0', '7.5', '7.0', '6.5', '6.0']
  snaps = [ 400,   400,   101,   101,   101]
  n_mu = len(muse)
  n_M  = len(Mdm)
  # t_mult = 3.086e21 / 1e5 / (1e9 * 365.25 * 24 * 3600)

  if np.all(axs == None):
    #set up plots
    fig, axs = plt.subplots(nrows=n_M, ncols=n_mu, sharex=True, sharey=True,
                            figsize=figsize)
    fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for j in range(n_mu):
    for i in range(n_M):

      #skip the loner
      if not((muse[j] == '1') and (Mdm[i] == '6p0')):
      # if (Mdm[i] != '6p0') and (Mdm[i] != '6p5'):

        #pick the right file to load
        name = 'mu_{0}/fdisk_0p01_lgMdm_{1}_V200-200kmps'.format(
          muse[j], Mdm[i])
        snap = '{0:03d}'.format(int(time/10 * snaps[i]))
        if snap == '400':
          #the potential of snap 400 is briken
          snap = '399'
        print('\n' + name + '_snap_' + snap)

        #select inputs
        loaded_strings = ['', 'MassDM',   'PosDMs',   'VelDMs',   'IDDMs',
                              'MassStar', 'star_pos', 'star_vel', 'IDStars',
                          'j_z_stars', 'j_p_stars', 'j_c_stars', 'energy_stars',
                          'E_interp', 'j_circ_interp', 'R_interp']

        inputs = inspect.getfullargspec(plot_function)[0]

        #check if inputs are needed
        need_to_load = False
        for put in inputs:
          for string in loaded_strings:
            if string == put:
              need_to_load = True

        #only load if needed
        if need_to_load:
          #load file
          loaded_values = load_EAGLE.load_and_calculate(name, snap)

        plot_args = []
        for put in inputs:
          for value, string in zip(loaded_values, loaded_strings):
            if string == put:
              plot_args.append(value)

        #plot function
        plot_function(*plot_args, ax=axs[i,j])

        #remove lables
        xlabel = axs[i,j].get_xlabel()
        ylabel = axs[i,j].get_ylabel()
        axs[i,j].set_xlabel('')
        axs[i,j].set_ylabel('')

        #add labels
        if i == 0:
          axs[i,j].set_title(r'$\mu = $'+muse[j])
        if j == n_mu-1:
          axs[i,j].yaxis.set_label_position('right')
          axs[i,j].set_ylabel(r'$M_{DM} = $' + Mdmstr[i])
        #remove ticks
        if i != n_M-1:
          axs[i,j].tick_params(labelbottom=False)
        if j != 0:
          axs[i,j].tick_params(labelleft=False)
        #remove legend
        if i != 0 or j != 0:
          legend = axs[i,j].get_legend()
          if legend != None:
            legend.remove()
      #remove loner
      else:
        axs[i,j].set_visible(False)

  axs[3,0].tick_params(labelbottom=True)
  axs[4,1].set_xlabel(xlabel)
  axs[2,0].set_ylabel(ylabel)
  # axs[0,0].legend()

  # if colorbar:
  #   fig.subplots_adjust(right=0.8)
  #   cbar_ax = fig.add_axes([0.81, 0.11, 0.04, 0.77])
  #   cbar = fig.colorbar(cax=cbar_ax)
  #   cbar.set_label(r'$\log ( M / 10^{10} M_\odot )$')

  if save:
    fname = '../galaxies/' + plot_name + '_' + str(time)
    plt.savefig(fname, bbox_inches='tight')#, dpi=200)
    plt.close()

  return(axs)

#to get star particle age
def lbt(z=[],a=[],omm=0.307,oml=0.693,h=0.6777):
    if a != []:
      z = 1.0/a-1
    t = np.zeros(len(z))
    for i in range(len(z)):
        t[i] = 1e+3*3.086e+16/(3.154e+7*1e+9)*(1.0/(100*h))*quad(lambda z: 1/((1+z)*np.sqrt(omm*(1+z)**3+oml)),0,z[i])[0]#in billion years
    return(t)

def starmass_pct_starformation(pct='95',
    filename='/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/starformation_from_z0.hdf5'):
  '''
  '''
  data = h5.File(filename,'r')

  #get ids
  log_mass = data['/GalaxyData/LogStarMass'][()]
  pct_form = data['/GalaxyData/z' + str(pct)][()]
  top_leaf = data['/GalaxyData/TopLeafID'][()]

  data.close()

  plt.figure()

  plt.errorbar(log_mass, pct_form, ls='', fmt='.')

  plt.xlabel(r'$(z=0) \log M_\star / $M$_\odot$')
  plt.ylabel(r'$z$ at 0.' + str(pct) + ' of stellar mass')

  plt.xlim((8.5, 12))
  plt.ylim((0,5.05))

  t_form = lbt(pct_form)

  plt.figure()

  plt.errorbar(log_mass, t_form, ls='', fmt='.')

  plt.xlabel(r'$(z=0) \log M_\star / $M$_\odot$')
  plt.ylabel(r'$t$ [Gyr] at 0.' + str(pct) + ' of stellar mass')

  plt.xlim((8.5, 12))
  plt.ylim((0, 13.8))

  args = np.argsort(pct_form)
  print(top_leaf[args][-20:-1])
  print(pct_form[args][-20:-1])

  return

def starmass_pct_starformation(
    filename='/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/starformation_from_z0.hdf5',
    save=True):
  '''
  '''
  data = h5.File(filename,'r')

  #get ids
  log_mass    = data['/GalaxyData/LogStarMass'][()]
  pct_form_95 = data['/GalaxyData/z95'][()]
  pct_form_50 = data['/GalaxyData/z50'][()]
  top_leaf    = data['/GalaxyData/TopLeafID'][()]

  data.close()

  top_sort = np.argsort(top_leaf)
  log_mass = log_mass[top_sort]
  pct_form_95 = pct_form_95[top_sort]
  pct_form_50 = pct_form_50[top_sort]
  top_leaf = top_leaf[top_sort]

  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + '028_z000p000' +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  stellar_half_mass_radius = galaxies['/GalaxyProfiles/StellarHalfMassRadius'][()]

  bin_mass = galaxies['/GalaxyProfiles/BinMass'][()]
  bin_N    = galaxies['/GalaxyProfiles/BinN'][()]

  fjzjc00 = galaxies['/GalaxyProfiles/fjzjc00'][()]

  kappa_rot = galaxies['/GalaxyProfiles/kappaRot'][()]
  kappa_co  = galaxies['/GalaxyProfiles/kappaCo'][()]

  sigma_z    = galaxies['/GalaxyProfiles/sigmaz'][()]
  sigma_R    = galaxies['/GalaxyProfiles/sigmaR'][()]
  sigma_phi  = galaxies['/GalaxyProfiles/sigmaphi'][()]
  sigma_tot  = galaxies['/GalaxyProfiles/sigmatot'][()]
  mean_v_phi = galaxies['/GalaxyProfiles/MeanVphi'][()]

  z_half = galaxies['/GalaxyProfiles/zHalf'][()]

  galaxies.close()

  top_sort_num = np.argsort(top_leaf_numbers)
  top_leaf_numbers = top_leaf_numbers[top_sort_num]

  central_unmask = np.logical_and(np.isin(top_leaf, top_leaf_numbers), top_leaf != 0)

  central_mask = np.logical_and(np.isin(top_leaf_numbers, top_leaf), top_leaf_numbers != 0)

  log_mass = log_mass[central_unmask]
  pct_form_95 = pct_form_95[central_unmask]
  pct_form_50 = pct_form_50[central_unmask]
  top_leaf = top_leaf[central_unmask]

  top_leaf_numbers = top_leaf_numbers[central_mask]

  stellar_half_mass_radius = stellar_half_mass_radius[top_sort_num][central_mask]

  bin_mass = bin_mass[top_sort_num][central_mask]
  bin_N    = bin_N[top_sort_num][central_mask]

  fjzjc00 = fjzjc00[top_sort_num][central_mask]

  kappa_rot = kappa_rot[top_sort_num][central_mask]
  kappa_co  = kappa_co[top_sort_num][central_mask]

  sigma_z    = sigma_z[top_sort_num][central_mask]
  sigma_R    = sigma_R[top_sort_num][central_mask]
  sigma_phi  = sigma_phi[top_sort_num][central_mask]
  sigma_tot  = sigma_tot[top_sort_num][central_mask]
  mean_v_phi = mean_v_phi[top_sort_num][central_mask]

  z_half = z_half[top_sort_num][central_mask]

  # plt.figure()

  # plt.errorbar(log_mass, pct_form, ls='', fmt='.')

  # plt.xlabel(r'$(z=0) \log M_\star / $M$_\odot$')
  # plt.ylabel(r'$z$ at 0.' + str(pct) + ' of stellar mass')

  # plt.xlim((8.5, 12))
  # plt.ylim((0,5.05))

  t_form_95 = lbt(pct_form_95)
  t_form_50 = lbt(pct_form_50)

  # plt.figure()

  # plt.errorbar(log_mass, t_form, ls='', fmt='.')

  # plt.xlabel(r'$(z=0) \log M_\star / $M$_\odot$')
  # plt.ylabel(r'$t$ [Gyr] at 0.' + str(pct) + ' of stellar mass')

  # plt.xlim((8.5, 12))
  # plt.ylim((0, 13.8))

  # if save:
  #   plt.savefig('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/z0xstarformationtime',
  #               bbox_inches='tight')
  #   plt.close()
  #   plt.close()


  data = np.array([log_mass, t_form_50, t_form_95,
                   stellar_half_mass_radius, z_half[:,14],
                   kappa_co[:,14], 2*fjzjc00[:,14]]).T
  labels = [r'$\log M_\star / $M$_\odot$', r'0.5$M_\star$ $t$ [Gyr]', r'0.95$M_\star$ $t$ [Gyr]',
            r'$r_{1/2}$ [kpc]', r'$z_{1/2}$ [kpc]',
            r'$\kappa_{co}$', r'S/T']

  splt.cornerplot(data, labels=labels, #pair_type='hist2D')
                  pair_type='scatter', scatter_kw={'s':3, 'alpha':0.2})

  args = np.argsort(pct_form_95)
  # print(top_leaf[args][-20:-1])
  # print(pct_form[args][-20:-1])

  return(top_leaf[args][-20:-1])


def kappa_evolution(x=0, y=1, small=0.05, save=True):
  '''
  '''
  snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
               '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
               '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
               '016_z001p737', '015_z002p012']


  # filename='/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/starformation_from_z0.hdf5'
  # data = h5.File(filename,'r')

  # #get ids
  # # log_mass    = data['/GalaxyData/LogStarMass'][()]
  # # pct_form_95 = data['/GalaxyData/z95'][()]
  # # pct_form_50 = data['/GalaxyData/z50'][()]

  # top_leaf = data['/GalaxyData/TopLeafID'][()]
  # if x==0:
  #   pct_x = np.ones(len(top_leaf))
  # else:
  #   pct_x = data['/GalaxyData/pct' + snap_list[x][1:3]][()]

  # if y==0:
  #   pct_y = np.ones(len(top_leaf))
  # else:
  #   pct_y = data['/GalaxyData/pct' + snap_list[y][1:3]][()]

  # data.close()

  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + snap_list[x] +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  bin_mass = galaxies['/GalaxyProfiles/BinMass'][()]

  kappa_rot = galaxies['/GalaxyProfiles/kappaRot'][()]
  kappa_co  = galaxies['/GalaxyProfiles/kappaCo'][()]

  galaxies.close()

  #
  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + snap_list[y] +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers1 = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  bin_mass1 = galaxies['/GalaxyProfiles/BinMass'][()]

  kappa_rot1 = galaxies['/GalaxyProfiles/kappaRot'][()]
  kappa_co1  = galaxies['/GalaxyProfiles/kappaCo'][()]

  galaxies.close()

  #work out mask
  top_sort_num = np.argsort(top_leaf_numbers)
  top_sort_num1 = np.argsort(top_leaf_numbers1)

  top_leaf_numbers = top_leaf_numbers[top_sort_num]
  top_leaf_numbers1 = top_leaf_numbers1[top_sort_num1]

  central_mask = np.logical_and(np.isin(top_leaf_numbers, top_leaf_numbers1), top_leaf_numbers)
  central_mask1 = np.logical_and(np.isin(top_leaf_numbers1, top_leaf_numbers), top_leaf_numbers1)

  top_leaf_numbers = top_leaf_numbers[central_mask]
  top_leaf_numbers1 = top_leaf_numbers1[central_mask1]

  bin_mass = bin_mass[top_sort_num][central_mask]
  bin_mass1 = bin_mass1[top_sort_num1][central_mask1]

  kappa_rot = kappa_rot[top_sort_num][central_mask]
  kappa_co  = kappa_co[top_sort_num][central_mask]

  kappa_rot1 = kappa_rot1[top_sort_num1][central_mask1]
  kappa_co1  = kappa_co1[top_sort_num1][central_mask1]

  bin_i = 14

  missing_mask = (kappa_co1[:,bin_i]!=0)

  x_data = kappa_co1[missing_mask, bin_i]
  y_data = kappa_co[missing_mask, bin_i]

  bin_mass_ratio = bin_mass1[missing_mask, bin_i] / bin_mass[missing_mask, bin_i]

  # small = 0.05
  mass_mask = np.logical_and(bin_mass_ratio <= 1 / (1 - small), bin_mass_ratio >= 1 - small)

  x_data = x_data[mass_mask]
  y_data = y_data[mass_mask]

  # plt.figure()
  # plt.hist2d(x_data, y_data,
  #             bins=[np.linspace(0,1,21), np.linspace(0,1,21)])

  # plt.errorbar([0,1], [0,1], ls='--', c='r')

  # xlab = r'$\kappa_{co}$,' + snap_list[x]
  # ylab = r'$\kappa_{co}$,' + snap_list[y]
  # plt.xlabel(xlab)
  # plt.ylabel(ylab)

  real_x = bin_mass[missing_mask, bin_i][mass_mask]
  real_y = x_data / y_data

  plt.figure()
  plt.scatter(real_x, real_y, alpha=0.3)

  plt.errorbar([0.05, 50], [1,1], ls='--', c='r')

  tot = 21
  bins = np.logspace(np.log10(0.05),np.log10(50), tot)
  idx  = np.digitize(real_x,bins)
  running_median = [np.median(real_y[idx==k]) for k in range(tot)]

  plt.errorbar(bins, running_median, c='C1', fmt='o', ls='-')

  plt.loglog()

  plt.xlabel('Stellar Mass [$10^{10} $M$_\odot$]')
  plt.ylabel(r'$\kappa_{co}$[' + snap_list[x][1:3] + r'] / $\kappa_{co}$[' + snap_list[y][1:3] + ']')

  if save:
    plt.savefig('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' +
                '_kappa_fraction_change', bbox_inches='tight')
  return()

def dispersion_evolution(x=0, y=2, small=0.15, save=True):
  '''
  '''
  snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
               '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
               '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
               '016_z001p737', '015_z002p012']

  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + snap_list[x] +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  bin_mass = galaxies['/GalaxyProfiles/BinMass'][()]

  sigma_z = galaxies['/GalaxyProfiles/sigmaz'][()]

  galaxies.close()

  #
  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + snap_list[y] +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers1 = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  bin_mass1 = galaxies['/GalaxyProfiles/BinMass'][()]

  sigma_z1 = galaxies['/GalaxyProfiles/sigmaz'][()]

  galaxies.close()

  #work out mask
  top_sort_num = np.argsort(top_leaf_numbers)
  top_sort_num1 = np.argsort(top_leaf_numbers1)

  top_leaf_numbers = top_leaf_numbers[top_sort_num]
  top_leaf_numbers1 = top_leaf_numbers1[top_sort_num1]

  central_mask = np.logical_and(np.isin(top_leaf_numbers, top_leaf_numbers1), top_leaf_numbers)
  central_mask1 = np.logical_and(np.isin(top_leaf_numbers1, top_leaf_numbers), top_leaf_numbers1)

  top_leaf_numbers = top_leaf_numbers[central_mask]
  top_leaf_numbers1 = top_leaf_numbers1[central_mask1]

  bin_mass = bin_mass[top_sort_num][central_mask]
  bin_mass1 = bin_mass1[top_sort_num1][central_mask1]

  sigma_z = sigma_z[top_sort_num][central_mask]
  sigma_z1 = sigma_z1[top_sort_num1][central_mask1]

  bin_i = 14

  missing_mask = (sigma_z1[:,bin_i]!=0)

  x_data = sigma_z1[missing_mask, bin_i]
  y_data = sigma_z[missing_mask, bin_i]

  bin_mass_ratio = bin_mass1[missing_mask, bin_i] / bin_mass[missing_mask, bin_i]

  # small = 0.05
  mass_mask = np.logical_and(bin_mass_ratio <= 1 / (1 - small), bin_mass_ratio >= 1 - small)

  x_data = x_data[mass_mask]
  y_data = y_data[mass_mask]

  # real_x = bin_mass[missing_mask, bin_i][mass_mask]
  real_x = y_data
  real_y = x_data / y_data

  plt.figure()
  plt.scatter(real_x, real_y, alpha=0.3)

  plt.errorbar([20, 400], [1,1], ls='--', c='r')

  tot = 21
  bins = np.logspace(np.log10(20),np.log10(400), tot)
  idx  = np.digitize(real_x,bins)
  running_median = [np.median(real_y[idx==k]) for k in range(tot)]

  plt.errorbar(bins, running_median, c='C1', fmt='o', ls='-')

  plt.loglog()

  # plt.xlabel('Stellar Mass [$10^{10} $M$_\odot$]')
  plt.xlabel(r'$\sigma_z$[' + snap_list[y][1:3] + r'] [km/s]')
  plt.ylabel(r'$\sigma_z$[' + snap_list[x][1:3] + r'] / $\sigma_z$[' + snap_list[y][1:3] + ']')

  if save:
    plt.savefig('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' +
                '_sigma_z_fraction_change', bbox_inches='tight')
  return()

def now_vs_later(save=True):
  '''
  '''
  filename='/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/starformation_from_z0.hdf5'
  data = h5.File(filename,'r')

  #get ids
  log_mass    = data['/GalaxyData/LogStarMass'][()]
  pct_form_95 = data['/GalaxyData/z95'][()]
  pct_form_50 = data['/GalaxyData/z50'][()]
  top_leaf    = data['/GalaxyData/TopLeafID'][()]

  data.close()

  top_sort = np.argsort(top_leaf)
  log_mass = log_mass[top_sort]
  pct_form_95 = pct_form_95[top_sort]
  pct_form_50 = pct_form_50[top_sort]
  top_leaf = top_leaf[top_sort]

  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + '028_z000p000' +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  stellar_half_mass_radius = galaxies['/GalaxyProfiles/StellarHalfMassRadius'][()]

  bin_mass = galaxies['/GalaxyProfiles/BinMass'][()]
  bin_N    = galaxies['/GalaxyProfiles/BinN'][()]

  fjzjc00 = galaxies['/GalaxyProfiles/fjzjc00'][()]

  kappa_rot = galaxies['/GalaxyProfiles/kappaRot'][()]
  kappa_co  = galaxies['/GalaxyProfiles/kappaCo'][()]

  sigma_z    = galaxies['/GalaxyProfiles/sigmaz'][()]
  sigma_R    = galaxies['/GalaxyProfiles/sigmaR'][()]
  sigma_phi  = galaxies['/GalaxyProfiles/sigmaphi'][()]
  sigma_tot  = galaxies['/GalaxyProfiles/sigmatot'][()]
  mean_v_phi = galaxies['/GalaxyProfiles/MeanVphi'][()]

  z_half = galaxies['/GalaxyProfiles/zHalf'][()]

  galaxies.close()

  #
  galaxies = h5.File('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + '019_z001p004' +
                     '_galaxy_kinematic_profile.hdf5','r')

  #find inxex
  top_leaf_numbers1 = galaxies['/GalaxyProfiles/TopLeafID'][()]

  #save values
  stellar_half_mass_radius1 = galaxies['/GalaxyProfiles/StellarHalfMassRadius'][()]

  bin_mass1 = galaxies['/GalaxyProfiles/BinMass'][()]
  bin_N1    = galaxies['/GalaxyProfiles/BinN'][()]

  fjzjc001 = galaxies['/GalaxyProfiles/fjzjc00'][()]

  kappa_rot1 = galaxies['/GalaxyProfiles/kappaRot'][()]
  kappa_co1  = galaxies['/GalaxyProfiles/kappaCo'][()]

  sigma_z1    = galaxies['/GalaxyProfiles/sigmaz'][()]
  sigma_R1    = galaxies['/GalaxyProfiles/sigmaR'][()]
  sigma_phi1  = galaxies['/GalaxyProfiles/sigmaphi'][()]
  sigma_tot1  = galaxies['/GalaxyProfiles/sigmatot'][()]
  mean_v_phi1 = galaxies['/GalaxyProfiles/MeanVphi'][()]

  z_half1 = galaxies['/GalaxyProfiles/zHalf'][()]

  galaxies.close()

  #work out mask
  top_sort_num = np.argsort(top_leaf_numbers)
  top_sort_num1 = np.argsort(top_leaf_numbers1)

  central_mask = np.logical_and(np.isin(top_leaf_numbers, top_leaf_numbers1), top_leaf_numbers)
  central_mask1 = np.logical_and(np.isin(top_leaf_numbers1, top_leaf_numbers), top_leaf_numbers1)

  #mask
  top_leaf_numbers = top_leaf_numbers[central_mask]

  stellar_half_mass_radius = stellar_half_mass_radius[top_sort_num][central_mask]

  bin_mass = bin_mass[top_sort_num][central_mask]
  bin_N    = bin_N[top_sort_num][central_mask]

  fjzjc00 = fjzjc00[top_sort_num][central_mask]

  kappa_rot = kappa_rot[top_sort_num][central_mask]
  kappa_co  = kappa_co[top_sort_num][central_mask]

  sigma_z    = sigma_z[top_sort_num][central_mask]
  sigma_R    = sigma_R[top_sort_num][central_mask]
  sigma_phi  = sigma_phi[top_sort_num][central_mask]
  sigma_tot  = sigma_tot[top_sort_num][central_mask]
  mean_v_phi = mean_v_phi[top_sort_num][central_mask]

  z_half = z_half[top_sort_num][central_mask]

  #mask1
  top_leaf_numbers1 = top_leaf_numbers1[central_mask1]

  stellar_half_mass_radius1 = stellar_half_mass_radius1[top_sort_num][central_mask1]

  bin_mass1 = bin_mass1[top_sort_num][central_mask1]
  bin_N1    = bin_N1[top_sort_num][central_mask1]

  fjzjc001 = fjzjc001[top_sort_num][central_mask1]

  kappa_rot1 = kappa_rot1[top_sort_num][central_mask1]
  kappa_co1  = kappa_co1[top_sort_num][central_mask1]

  sigma_z1    = sigma_z1[top_sort_num][central_mask1]
  sigma_R1    = sigma_R1[top_sort_num][central_mask1]
  sigma_phi1  = sigma_phi1[top_sort_num][central_mask1]
  sigma_tot1  = sigma_tot1[top_sort_num][central_mask1]
  mean_v_phi1 = mean_v_phi1[top_sort_num][central_mask1]

  z_half1 = z_half1[top_sort_num][central_mask1]

  plt.figure()

  # plt.errorbar(kappa_co1[:,14][kappa_co1[:,14]!=0], kappa_co[:,14][kappa_co1[:,14]!=0], ls='', fmt='.')
  plt.hist2d(kappa_co1[:,14][kappa_co1[:,14]!=0], kappa_co[:,14][kappa_co1[:,14]!=0],
              bins=[np.linspace(0,1,11), np.linspace(0,1,11)])

  plt.xlabel(r'$\kappa_{co}, z=1$')
  plt.ylabel(r'$\kappa_{co}, z=0$')

  return()

def single_galaxy_kinematic_evolution(top_leaf_id = 18292479, save=True):
  '''
  '''
  formation_filename='/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/starformation_from_z0.hdf5'
  formation_data = h5.File(formation_filename,'r')

  #find correct id
  top_leaf_numbers = formation_data['/GalaxyData/TopLeafID'][()]
  top_index = np.where(top_leaf_numbers == top_leaf_id)

  #percentage formations
  pct = np.zeros(29)

  for snap_i in np.arange(15,28):
    pct[snap_i] = formation_data['/GalaxyData/pct' + str(snap_i)][top_index]
  pct[28] = 1

  formation_data.close()

  kinematic_profile_data_location = '/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/'

  snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
               '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
               '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
               '016_z001p737', '015_z002p012']

  snap_z = np.array([0.0,   0.101, 0.183, 0.271,
                     0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487,
                     1.737, 2.012])
  # snap_a = 1 / (1 + snap_z)
  # snap_t = lbt(z = snap_z)
  snap_t = lbt(z = np.array([1e4])) - lbt(z = snap_z)

  bin_edges = [0,1,2,3,4,5,6,7,8,9,10,12,15,20,30]
  #index 14 is all stars r < 30kpc
  #index 15 is all stars r < r_half

  stellar_half_mass_radius = np.zeros(29, dtype=np.float64)

  bin_mass  = np.zeros((29,16), dtype=np.float64)
  bin_N     = np.zeros((29,16), dtype=np.int64)

  # fjzjc09 = np.zeros((29,16), dtype=np.float64)
  # fjzjc08 = np.zeros((29,16), dtype=np.float64)
  # fjzjc07 = np.zeros((29,16), dtype=np.float64)
  fjzjc00 = np.zeros((29,16), dtype=np.float64)

  kappa_rot = np.zeros((29,16), dtype=np.float64)
  kappa_co  = np.zeros((29,16), dtype=np.float64)

  sigma_z    = np.zeros((29,16), dtype=np.float64)
  sigma_R    = np.zeros((29,16), dtype=np.float64)
  sigma_phi  = np.zeros((29,16), dtype=np.float64)
  sigma_tot  = np.zeros((29,16), dtype=np.float64)
  mean_v_phi = np.zeros((29,16), dtype=np.float64)

  z_half    = np.zeros((29,16), dtype=np.float64)

  for snap in snap_list:

    snap_i = int(snap[:3])

    galaxies = h5.File(kinematic_profile_data_location + snap +
                       '_galaxy_kinematic_profile.hdf5','r')

    #find inxex
    top_leaf_numbers = galaxies['/GalaxyProfiles/TopLeafID'][()]

    galaxy_index = np.where(top_leaf_id == top_leaf_numbers)[0]

    # gal_id = galaxies['/GalaxyProfiles/GalaxyID'][galaxy_index]
    # print(gal_id)

    #save values
    stellar_half_mass_radius[snap_i] = galaxies['/GalaxyProfiles/StellarHalfMassRadius'][galaxy_index]

    bin_mass[snap_i, :] = galaxies['/GalaxyProfiles/BinMass'][galaxy_index]
    bin_N[snap_i, :]    = galaxies['/GalaxyProfiles/BinN'][galaxy_index]

    fjzjc00[snap_i, :] = galaxies['/GalaxyProfiles/fjzjc00'][galaxy_index]

    kappa_rot[snap_i, :] = galaxies['/GalaxyProfiles/kappaRot'][galaxy_index]
    kappa_co[snap_i, :]  = galaxies['/GalaxyProfiles/kappaCo'][galaxy_index]

    sigma_z[snap_i, :]    = galaxies['/GalaxyProfiles/sigmaz'][galaxy_index]
    sigma_R[snap_i, :]    = galaxies['/GalaxyProfiles/sigmaR'][galaxy_index]
    sigma_phi[snap_i, :]  = galaxies['/GalaxyProfiles/sigmaphi'][galaxy_index]
    sigma_tot[snap_i, :]  = galaxies['/GalaxyProfiles/sigmatot'][galaxy_index]
    mean_v_phi[snap_i, :] = galaxies['/GalaxyProfiles/MeanVphi'][galaxy_index]

    z_half[snap_i, :] = galaxies['/GalaxyProfiles/zHalf'][galaxy_index]

    galaxies.close()

  # plot_quantity_vs_time(bin_mass[15:29,14],  r'$M_\star (<%d$kpc, $z=0)$ [$10^{10} $M$_\odot$]'%(30))

  # plot_quantity_vs_time(kappa_rot[15:29,14], r'$\kappa_{rot}$', ylim=[0,1])
  # plot_quantity_vs_time(kappa_co[15:29,14],  r'$\kappa_{co}$', ylim=[0,1])

  # plot_quantity_vs_time(fjzjc00[15:29,14], r'Fraction of particles with $j_z/j_c(E) > 0.0$', ylim=[0,1])

  # plot_quantity_vs_time(sigma_z[15:29,14],     r'$\sigma_z$ [km/s]')
  # plot_quantity_vs_time(sigma_R[15:29,14],     r'$\sigma_R$ [km/s]')
  # plot_quantity_vs_time(sigma_phi[15:29,14],   r'$\sigma_\phi$ [km/s]')
  # plot_quantity_vs_time(mean_v_phi[15:29,14],  r'$\overline{v_\phi}$ [km/s]')

  fig = plt.figure()
  fig.set_size_inches(10, 20, forward=True)
  fig.subplots_adjust(hspace=0.03,wspace=0)

  ax1 = fig.add_subplot(611)
  ax_twin = ax1.twiny()
  ax2 = fig.add_subplot(612)
  ax3 = fig.add_subplot(613)
  ax4 = fig.add_subplot(614)
  ax5 = fig.add_subplot(615)
  ax6 = fig.add_subplot(616)

  ax1lim = [0, 1.1* np.amax(bin_mass[15:29,14])]
  ax2lim = [0, 1]
  ax3lim = [0, 1]
  ax4lim = [0, 1.1* np.amax(sigma_tot[15:29,14])]
  ax5lim = [np.amin((0.0, 1.1* np.amin(mean_v_phi[15:29,14]))), 1.1* np.amax(mean_v_phi[15:29,14])]
  ax6lim = [0, 1.1* np.amax(z_half[15:29,14])]

  ax1.vlines(snap_t, ax1lim[0], ax1lim[1], linestyles='--', alpha=0.2)
  ax2.vlines(snap_t, ax2lim[0], ax2lim[1], linestyles='--', alpha=0.2)
  ax3.vlines(snap_t, ax3lim[0], ax3lim[1], linestyles='--', alpha=0.2)
  ax4.vlines(snap_t, ax4lim[0], ax4lim[1], linestyles='--', alpha=0.2)
  ax5.vlines(snap_t, ax5lim[0], ax5lim[1], linestyles='--', alpha=0.2)
  ax6.vlines(snap_t, ax6lim[0], ax6lim[1], linestyles='--', alpha=0.2)

  #the actual plot
  ax1.errorbar(np.flip(snap_t), pct[15:29] * bin_mass[28,14], fmt='o', ls=':', label=r'$z=0$ SFH')
  ax1.errorbar(np.flip(snap_t), bin_mass[15:29,14], fmt='o', ls=':', label='Snapshot Mass')
  ax1.set_ylabel('$M_\star$ [$10^{10} $M$_\odot$]')
  ax1.legend(ncol=2)

  # ax2.errorbar(np.flip(snap_t), fjzjc90[15:29,14], fmt='o', ls=':', label='$j_z/j_c(E) > 0.9$')
  # ax2.errorbar(np.flip(snap_t), fjzjc80[15:29,14], fmt='o', ls=':', label='$j_z/j_c(E) > 0.8$')
  # ax2.errorbar(np.flip(snap_t), fjzjc70[15:29,14], fmt='o', ls=':', label='$j_z/j_c(E) > 0.7$')
  ax2.errorbar(np.flip(snap_t), fjzjc00[15:29,14], fmt='o', ls=':', label='$j_z/j_c(E) < 0.0$')
  ax2.set_ylabel(r'$F[ j_z/j_c(E) > x ]$')
  ax2.legend(ncol=4)

  ax3.errorbar(np.flip(snap_t), kappa_rot[15:29,14], fmt='o', ls=':', label='$\kappa_{rot}$')
  ax3.errorbar(np.flip(snap_t), kappa_co[15:29,14],  fmt='o', ls=':', label='$\kappa_{co}$')
  ax3.set_ylabel(r'$\kappa$')
  ax3.legend(ncol=2)

  ax4.errorbar(np.flip(snap_t), sigma_z[15:29,14],   fmt='o', ls=':', label=r'$\sigma_z$')
  ax4.errorbar(np.flip(snap_t), sigma_R[15:29,14],   fmt='o', ls=':', label=r'$\sigma_R$')
  ax4.errorbar(np.flip(snap_t), sigma_phi[15:29,14], fmt='o', ls=':', label=r'$\sigma_\phi$')
  ax4.errorbar(np.flip(snap_t), sigma_tot[15:29,14], fmt='o', ls=':', label=r'$\sigma_{tot}$')
  ax4.set_ylabel(r'$\sigma$ [km/s]')
  ax4.legend(ncol=4)

  ax5.errorbar(np.flip(snap_t), mean_v_phi[15:29,14], fmt='o', ls=':')
  ax5.set_ylabel(r'$\overline{v_\phi}$ [km/s]')

  ax6.errorbar(np.flip(snap_t), z_half[15:29,14], fmt='o', ls=':')
  ax6.set_ylabel(r'$z_{1/2}$ [kpc]')

  ax6.set_xlabel(r'$t$ [Gyr]')
  ax_twin.set_xlabel(r'$z$')
  ax_twin.set_xticks(np.concatenate((snap_t, [snap_t[-2]])))
  label = []
  for i in range(len(snap_z)):
    if i%2 == 0: label.append(str(np.round(snap_z[i],2)))
    else: label.append('')
  ax_twin.set_xticklabels(label)

  ax1.set_xlim(0, snap_t[-1])
  ax2.set_xlim(0, snap_t[-1])
  ax3.set_xlim(0, snap_t[-1])
  ax4.set_xlim(0, snap_t[-1])
  ax5.set_xlim(0, snap_t[-1])
  ax6.set_xlim(0, snap_t[-1])

  ax1.set_ylim(ax1lim)
  ax2.set_ylim(ax2lim)
  ax3.set_ylim(ax3lim)
  ax4.set_ylim(ax4lim)
  ax5.set_ylim(ax5lim)
  ax6.set_ylim(ax6lim)

  if save:
    plt.savefig('/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' +
                str(top_leaf_id) + '_30kpc_kinematic_evolution', bbox_inches='tight')
    # plt.close()

  # print(bin_N[15:29,14])

  return

def plot_quantity_vs_time(quantity, label='', ylim=None):
  '''
  '''
  snap_z = np.array([0.0,   0.101, 0.183, 0.271,
                     0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487,
                     1.737, 2.012])
  # snap_a = 1 / (1 + snap_z)
  snap_t = lbt(z = snap_z)

  fig = plt.figure()
  fig.set_size_inches(8, 5, forward=True)

  ax = plt.subplot(111)
  ax2 = ax.twiny()

  if ylim==None:
    ylim = [np.amin((0, np.amin(quantity))), 1.1*np.amax(quantity)]

  ax.vlines(snap_t, ylim[0], ylim[1], linestyles='--', alpha=0.2)

  #the actual plot
  ax.errorbar(np.flip(snap_t), quantity, fmt='o', ls=':')

  ax.set_ylabel(label)
  ax.set_xlabel(r'$t$ [Gyr]')

  ax2.set_xlabel(r'$z$')
  ax2.set_xticks(np.concatenate((snap_t, [snap_t[-2]])))

  label = []
  for i in range(len(snap_z)):
    if i%2 == 0: label.append(str(np.round(snap_z[i],2)))
    else: label.append('')
  ax2.set_xticklabels(label)

  ax.set_xlim(0, snap_t[-1])
  ax.set_ylim(ylim)

  return

def plot_morpholocial_corner_less_than(top_leaf_id, save=True):
  '''
  '''

  formation_filename='/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/starformation_from_z0.hdf5'
  formation_data = h5.File(formation_filename,'r')

  #find correct id
  top_leaf_numbers = formation_data['/GalaxyData/TopLeafID'][()]
  top_index = np.where(top_leaf_numbers == top_leaf_id)

  #percentage formations
  pct = np.zeros(29)

  for snap_i in np.arange(15,28):
    pct[snap_i] = formation_data['/GalaxyData/pct' + str(snap_i)][top_index]
  pct[28] = 1

  formation_data.close()

  kinematic_profile_data_location = '/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/'

  snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
               '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
               '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
               '016_z001p737', '015_z002p012']

  snap_z = np.array([0.0,   0.101, 0.183, 0.271,
                     0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487,
                     1.737, 2.012])
  # snap_a = 1 / (1 + snap_z)
  snap_t = lbt(z = snap_z)

  bin_edges = [0,1,2,3,4,5,6,7,8,9,10,12,15,20,30]
  #index 14 is all stars r < 30kpc
  #index 15 is all stars r < r_half

  stellar_half_mass_radius = np.zeros(29, dtype=np.float64)

  bin_mass  = np.zeros((29,16), dtype=np.float64)
  bin_N     = np.zeros((29,16), dtype=np.int64)

  # fjzjc09 = np.zeros((29,16), dtype=np.float64)
  # fjzjc08 = np.zeros((29,16), dtype=np.float64)
  # fjzjc07 = np.zeros((29,16), dtype=np.float64)
  fjzjc00 = np.zeros((29,16), dtype=np.float64)

  kappa_rot = np.zeros((29,16), dtype=np.float64)
  kappa_co  = np.zeros((29,16), dtype=np.float64)

  sigma_z    = np.zeros((29,16), dtype=np.float64)
  sigma_R    = np.zeros((29,16), dtype=np.float64)
  sigma_phi  = np.zeros((29,16), dtype=np.float64)
  sigma_tot  = np.zeros((29,16), dtype=np.float64)
  mean_v_phi = np.zeros((29,16), dtype=np.float64)

  z_half    = np.zeros((29,16), dtype=np.float64)

  for snap in snap_list:

    snap_i = int(snap[:3])

    galaxies = h5.File(kinematic_profile_data_location + snap +
                       '_galaxy_kinematic_profile.hdf5','r')

    #find inxex
    top_leaf_numbers = galaxies['/GalaxyProfiles/TopLeafID'][()]

    galaxy_index = np.where(top_leaf_id == top_leaf_numbers)[0]

    # gal_id = galaxies['/GalaxyProfiles/GalaxyID'][galaxy_index]
    # print(gal_id)

    #save values
    stellar_half_mass_radius[snap_i] = galaxies['/GalaxyProfiles/StellarHalfMassRadius'][galaxy_index]

    bin_mass[snap_i, :] = galaxies['/GalaxyProfiles/BinMass'][galaxy_index]
    bin_N[snap_i, :]    = galaxies['/GalaxyProfiles/BinN'][galaxy_index]

    fjzjc00[snap_i, :] = galaxies['/GalaxyProfiles/fjzjc00'][galaxy_index]

    kappa_rot[snap_i, :] = galaxies['/GalaxyProfiles/kappaRot'][galaxy_index]
    kappa_co[snap_i, :]  = galaxies['/GalaxyProfiles/kappaCo'][galaxy_index]

    sigma_z[snap_i, :]    = galaxies['/GalaxyProfiles/sigmaz'][galaxy_index]
    sigma_R[snap_i, :]    = galaxies['/GalaxyProfiles/sigmaR'][galaxy_index]
    sigma_phi[snap_i, :]  = galaxies['/GalaxyProfiles/sigmaphi'][galaxy_index]
    sigma_tot[snap_i, :]  = galaxies['/GalaxyProfiles/sigmatot'][galaxy_index]
    mean_v_phi[snap_i, :] = galaxies['/GalaxyProfiles/MeanVphi'][galaxy_index]

    z_half[snap_i, :] = galaxies['/GalaxyProfiles/zHalf'][galaxy_index]

    galaxies.close()

  # t = np.linspace(0, snap_t[-1], len(snap_t))

  glob = 14
  half = 15
  srl = 15
  srh = 29

  data = np.vstack((snap_t, #mean_elipticity_value,
                    np.zeros(len(snap_t)), 2*fjzjc00[srl:srh, glob],
                    kappa_rot[srl:srh, glob], kappa_co[srl:srh, glob],
                    mean_v_phi[srl:srh, glob] / sigma_tot[srl:srh, glob],
                    sigma_R[srl:srh, glob], sigma_phi[srl:srh, glob], sigma_z[srl:srh, glob],
                    sigma_tot[srl:srh, glob], mean_v_phi[srl:srh, glob])).T


  data_half = np.vstack((snap_t, #mean_elipticity_value,
                         np.zeros(len(snap_t)), 2*fjzjc00[srl:srh, half],
                         kappa_rot[srl:srh, half], kappa_co[srl:srh, half],
                         mean_v_phi[srl:srh, half] / sigma_tot[srl:srh, half],
                         sigma_R[srl:srh, half], sigma_phi[srl:srh, glob], sigma_z[srl:srh, half],
                         sigma_tot[srl:srh:, half], mean_v_phi[srl:srh, half])).T

  #manual corner plot
  big_data = (data_half, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C1', 'k')

  labels=[r'$t$ [Gyr]', #r'$\bar{\epsilon}$',
          r'$F(\epsilon > 0.7)$', r'Spheroid Fraction',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0, snap_t[-1]), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(25,25))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        if data_i > 3:
          axs[axis_y-1, axis_x].errorbar(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            ls=':', fmt='', c=cs[data_i], linewidth=3)
        else:
          axs[axis_y-1, axis_x].scatter(
            big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
            s=5, c=cs[data_i], alpha=1)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'$<r_{50}$',r'$<30$kpc'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  if save:
    fname = '/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + str(top_leaf_id) + 'corner_full'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  data = np.vstack((snap_t, #mean_elipticity_value,
                    np.zeros(len(snap_t)), 2*fjzjc00[srl:srh, glob],
                    kappa_rot[srl:srh, glob], kappa_co[srl:srh, glob],
                    mean_v_phi[srl:srh, glob] / sigma_tot[srl:srh, glob])).T


  data_half = np.vstack((snap_t, #mean_elipticity_value,
                         np.zeros(len(snap_t)), 2*fjzjc00[srl:srh, half],
                         kappa_rot[srl:srh, half], kappa_co[srl:srh, half],
                         mean_v_phi[srl:srh, half] / sigma_tot[srl:srh, half])).T

  #manual corner plot
  big_data = (data_half, data)
  # cmaps = ('Oranges_r', 'Greens_r', 'Purples_r', 'Greys_r')
  cs = ('C1', 'k')

  labels=[r'$t$ [Gyr]', #r'$\bar{\epsilon}$',
          r'$F(\epsilon > 0.7)$', r'Spheroid Fraction',
          r'$\kappa_{rot}$', r'$\kappa_{co}$', r'$\overline{v_\phi}/\sigma_{tot}$',
          r'$\sigma_{v_R}$',r'$\sigma_{v_\phi}$',r'$\sigma_{v_z}$',
          r'$\sigma_{tot}$', r'$\overline{v_\phi}$']
  lims = [(0, snap_t[-1]), (0,0.999), (0,0.999), (0,0.999), (0,0.999), (0,2.999),
          (0,199), (0,199), (0,199), (0,299), (0,299)]
  n = np.shape(data)[1]

  fig, axs = plt.subplots(nrows=n-1, ncols=n-1, sharex=False, sharey=False,
                          figsize=(13,13))
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  for data_i in range(len(big_data)):
    for axis_y in range(n):
      for axis_x in range(axis_y):
        # axs[axis_y-1, axis_x].scatter(
        #   big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
        #   s=2, c=t, cmap=cmaps[data_i], alpha=0.3)
        axs[axis_y-1, axis_x].scatter(
          big_data[data_i].T[axis_x], big_data[data_i].T[axis_y],
          s=5, c=cs[data_i], alpha=1)

        axs[axis_y-1, axis_x].set_xlim(lims[axis_x])
        axs[axis_y-1, axis_x].set_ylim(lims[axis_y])
        if axis_x != 0:
          axs[axis_y-1, axis_x].set_yticklabels([])
        if axis_y != n-1:
          axs[axis_y-1, axis_x].set_xticklabels([])

  for axis_y in range(n-1):
    axs[axis_y, 0].set_ylabel(labels[axis_y+1])
  for axis_x in range(n-1):
    axs[n-2, axis_x].set_xlabel(labels[axis_x])

  for axis_y in range(n-1):
    for axis_x in range(n-1):
      if n - axis_y + axis_x > n:
        axs[axis_y, axis_x].set_visible(False)

  axs[0,0].legend([r'$<r_{50}$',r'$<30$kpc'],
                  bbox_to_anchor=(1, 1), loc='upper left')

  if save:
    fname = '/home/matt/Documents/UWA/EAGLE/L0100N1504/REFERENCE/' + str(top_leaf_id) + 'corner_indicators'
    plt.savefig(fname, bbox_inches='tight')
    plt.close()

  return

if __name__ == '__main__':

  # m = 100
  # n = 10000
  # values = np.zeros(2*m)

  # for m_ in range(m):


  #   x = 2*np.random.rand(n,3) -1
  #   v = 2*np.random.rand(n,3) -1

  #   r = np.linalg.norm(x, axis=1)

  #   x = x[r < 1]
  #   v = v[r < 1]

  #   _,_,_, _,w,_ = get_cylindrical(x, v)

  #   k_tot = np.sum(np.square(np.linalg.norm(v, axis=1)))
  #   k_rot = np.sum(np.square(w[w>0]))

  #   values[2*m_]   = k_rot / k_tot
  #   values[2*m_+1] = np.sum(np.square(v[:,0][v[:,0]>0])) / k_tot


  # print(np.mean(values))
  # print(np.std(values))


  #low star formation, somewhat rotationally supported galaxies
  #14096298
  #16673564 #all bar
  #11419723 #all bar
  #9669740
  #17494095 #all bar
  #3274741 #all bar #extreme star formation feedback
  #16895384 #all bar
  #17397709 #all bar
  #16636222 #all bar #extreme star formation feedback #cool z=1.487 pics
  # single_galaxy_kinematic_evolution(8739471)#top_leaf_id = 21109786)
  # single_galaxy_kinematic_evolution(14096298, save=False)

  # plot_morpholocial_corner_less_than(14096298, save=True)
  # plot_morpholocial_corner_less_than(9669740, save=True)

  # starmass_pct_starformation(save=False)

  # kappa_evolution()
  # dispersion_evolution(0, 8)

  # now_vs_later(save=False)

  # for id_ in ids:
  # single_galaxy_kinematic_evolution(save=False)

  #TODO keep this clean

  # save = False

  # base             = '/home/matt/Documents/UWA/EAGLE/'
  # # data_location    = base + 'L0012N0188/EAGLE_REFERENCE/data/'
  # data_destination = base + 'processed_data/'

  # identity         = '_12Mpc_188'
  # snap             = '028_z000p000'

  # (file_name,
  #  gas_pos,  gas_vel,  gas_mass,  gas_bind,
  #  dm_pos,   dm_vel,   dm_mass,   dm_bind,
  #  star_pos, star_vel, star_mass, star_bind,
  #  bh_pos,   bh_vel,   bh_mass,   bh_bind,
  #  j_circ, binding_e, r_bins,
  #  gas_j_c, dm_j_c, star_j_c, bh_j_c) = load_EAGLE.load_and_calculate(
  #    data_destination, snap, identity, group=3, subgroup=0)

  # #star
  # plot_projection(star_pos)
  # plot_jzonc(star_j_c, star_pos, star_vel, star_mass)
  # plot_jz_e(star_pos, star_vel, star_mass, star_bind, binding_e, j_circ)
  # plot_jzonc_r(star_j_c, star_pos, star_vel, star_mass)

  # plot_stellar_exponential(star_pos, star_mass)

  # halo.plot_potential_verify(gas_pos, gas_vel, gas_mass, gas_bind,
  #                             dm_pos, dm_vel, dm_mass, dm_bind,
  #                             star_pos, star_vel, star_mass, star_bind,
  #                             bh_pos, bh_vel, bh_mass, bh_bind)


  #gas
  # plot_projection(gas_pos)
  # plot_jzonc(gas_j_c,  gas_pos,  gas_vel,  gas_mass)
  # plot_jz_e(gas_pos,  gas_vel,  gas_mass,  gas_bind,  binding_e, j_circ)
  # plot_jzonc_r(gas_j_c,  gas_pos,  gas_vel,  gas_mass)

  # print(starmass_pct_starformation(save=False))

  pass