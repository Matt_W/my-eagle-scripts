#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 15:35:27 2021

@author: matt
"""

#import os
import numpy as np

import matplotlib
matplotlib.use('Agg')

import h5py  as h5

from scipy.integrate         import quad
from scipy.optimize          import brentq
from scipy.spatial.transform import Rotation
from scipy.stats             import binned_statistic

import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

LITTLE_H = 0.6777
#box size should be saved and read in
BOX_SIZE = 147.55792
#SCALE_A  = 1

def load_halo_stars(particle_data_location, galaxy_data_location,
                    snap, name, z0group=1, z0subgroup=0):
  '''Finds the top leaf id of the galaxy that corresponds to the z=0 input group
  and subgroup,
  then finds the galaxy with that top leaf id at the given snapshot,
  then returns the star particle data.
  '''
  #read z=0 halos
  file_name = '%s%s_%s_galaxies.hdf5'%(galaxy_location, '028_z000p000', name)
  z0_data = h5.File(file_name,'r')

  #get ids
  z0_group_numbers      = z0_data['/IDs/GroupID'][()]
  z0_subgroup_numbers   = z0_data['/SubHaloData/SubHaloID'][()]
  z0_top_leaf_numbers   = z0_data['/IDs/TopLeafID'][()]
  centre_of_pot_numbers = z0_data['SubHaloData/Pos_cop'][()]

  SCALE_A = z0_data['/Header/a'][()]

  z0_data.close()

  #work out top leafid
  top_leaf_id         = None
  centre_of_potential = None

  for index, (group, subgroup) in enumerate(zip(z0_group_numbers, z0_subgroup_numbers)):
    if group - 28000000000000 + 1 == z0group:
      if subgroup == z0subgroup:
        top_leaf_id = z0_top_leaf_numbers[index]
        centre_of_potential = centre_of_pot_numbers[index]

  if top_leaf_id == None:
    print('No such group / subgroup combination.')

  #stars
  file_name = '%s%s_%s_Star.hdf5'%(particle_data_location, snap, name)
  star_data = h5.File(file_name, 'r')

  star_group    = star_data['PartData/GrpNum_Star'][()]
  star_subgroup = star_data['PartData/SubNum_Star'][()]

  #TODO this should use kdtrees to speed up.
  star_mask = np.logical_and((z0group    == star_group),
                             (z0subgroup == star_subgroup))
  #use kdtrees!

  #load masked quantities
  star_pos         = star_data['PartData/PosStar'][()][star_mask]
  star_vel         = star_data['PartData/VelStar'][()][star_mask]
  star_mass        = star_data['PartData/MassStar'][()][star_mask]
  star_formation_a = star_data['PartData/StellarFormationTime'][()][star_mask]

  star_data.close()

  #star_bind = star_data['PartData/ParticleBindingEnergy'][()][star_mask] * SCALE_A #(km/s)^2
  star_pos  = star_pos - centre_of_potential - BOX_SIZE/2
  star_pos  %= BOX_SIZE
  star_pos  -= BOX_SIZE/2
  star_pos  *= 1000 * SCALE_A / LITTLE_H #kpc
  star_vel  *= np.sqrt(SCALE_A) #km/s
  star_mass /= LITTLE_H #10^10 M_sun
  #star_bind = star_data['PartData/ParticleBindingEnergy'][()][star_mask] * SCALE_A #(km/s)^2

  return(top_leaf_id,
         star_pos, star_vel, star_mass, star_formation_a)

def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  R = find_rotaton_matrix(galaxy_anular_momentum)
  pos = R.apply(pos)
  '''
  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

def align(pos, vel, mass, apature=30):
  '''Aligns the z cartesian direction with the direction of angular momentum.
  Can use an apature.
  '''
  #direction to align
  if apature != None:
    r = np.linalg.norm(pos, axis=1)
    mask = r < apature

    j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

  else:
    j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

  #find rotation
  rotation = find_rotaion_matrix(j_tot)

  #rotate stars
  pos = rotation.apply(pos)
  vel = rotation.apply(vel)

  return(pos, vel)

#to get star particle age
def lbt(a,omm=0.307,oml=0.693,h=0.6777):
    z = 1.0/a-1
    t = np.zeros(len(z))
    for i in range(len(z)):
        t[i] = 1e+3*3.086e+16/(3.154e+7*1e+9)*(1.0/(100*h))*quad(lambda z: 1/((1+z)*np.sqrt(omm*(1+z)**3+oml)),0,z[i])[0]#in billion years
    return(t)

def plot_star_formation_history(star_pos, star_mass, star_formation_a,
                                top_leaf_id, sim, apature=30):
  '''Assume already centred.
  Plot star formation history of stars within apature at z=0.
  '''
  onepointone = 1.1

  if apature != False:
    star_r = np.linalg.norm(star_pos, axis=1)

    apature_mask = (star_r < apature)

    star_formation_a = star_formation_a[apature_mask]
    star_mass        = star_mass[apature_mask]

  star_formation_z = 1/star_formation_a + 1

  star_formation_t = lbt(star_formation_a)

  arg_order = np.argsort(star_formation_z)
  mass_order = star_mass[arg_order]
  # cum_z    = star_formation_z[arg_order]
  cum_t    = star_formation_t[arg_order]
  cum_mass = np.flip(np.cumsum(np.flip(mass_order)))

  snap_z = np.array([0.0, 0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487, 1.737, 2.012, 1000])
  snap_a = 1 / (1 + snap_z)
  snap_t = lbt(snap_a)

  fig = plt.figure()
  fig.set_size_inches(8, 5, forward=True)

  ax = plt.subplot(111)
  ax2 = ax.twiny()

  (star_mass_formed ,_,_
   ) = binned_statistic(star_formation_z, star_mass, statistic='sum',
                        bins = snap_z)

  z0_mass = sum(star_mass_formed)

  ax.vlines(snap_t, 0, onepointone*z0_mass, linestyles='--', alpha=0.4)

  ax.errorbar(cum_t, cum_mass, fmt=',', linestyle='-')

  if not apature:
    ax.set_ylabel(r'$M_\star ($subhalo, $z=0)$ [$10^{10} $M$_\odot$]')
  else:
    ax.set_ylabel(r'$M_\star (<%d$kpc, $z=0)$ [$10^{10} $M$_\odot$]'%(apature))
  ax.set_xlabel(r'$t$ [Gyr]')

  ax2.set_xlabel(r'$z$')
  ax2.set_xticks(np.concatenate((snap_t[:-1:2], [snap_t[-2]])))
  ax2.set_xticklabels(np.round(snap_z[:-1:2],2))

  # ax0.set_xlim((0,2.012))
  ax.set_xlim((0,snap_t[-2]))
  ax.set_ylim((0, z0_mass*onepointone))

  fname = '/home/mwilkins/EAGLE/plots/' + sim + '/topleaf' + str(top_leaf_id) + 'sfh'
  plt.savefig(fname, bbox_inches="tight")
  plt.close()

  return

if __name__ == '__main__':

  sim = 'L0100N1504/REFERENCE'
  particle_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/'
  galaxy_location        = '/fred/oz009/ejimenez/Galaxies/' + sim + '/'

  name = '100Mpc_1504'

  group_list = [1,2,3,10,100,1000]

  for group in group_list:

    (top_leaf_id,
     star_pos, star_vel, star_mass, star_formation_a
     ) = load_halo_stars(particle_data_location, galaxy_location,
                         '028_z000p000', name, z0group=group, z0subgroup=0)

    (star_pos, star_vel) = align(star_pos, star_vel, star_mass, apature=30)

    plot_star_formation_history(star_pos, star_mass, star_formation_a,
                                top_leaf_id, sim, apature=30)

  pass