#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 14:57:33 2020

@author: matt
"""

import h5py as h5
import os
import numpy as np

def get_number_of_groups(data_location, snap):
  '''
  '''
  subfind_tab_name = (data_location + 'groups_' + snap + '/eagle_subfind_tab_' +
                      snap + '.0.hdf5')
  subfind_tab = h5.File(subfind_tab_name, 'r')

  number_of_groups = subfind_tab['Header'].attrs.get('TotNgroups')

  subfind_tab.close()

  return(number_of_groups)

def get_dm_mass(data_location, snap):
  '''
  '''
  subfind_particle_name = (data_location + 'particledata_' + snap +
                           '/eagle_subfind_particles_' + snap + '.0.hdf5')
  subfind_particle = h5.File(subfind_particle_name, 'r')

  dm_mass = subfind_particle['Header'].attrs.get('MassTable')[1]

  subfind_particle.close()

  return(dm_mass)

def get_group_lengths(data_location, snap, NTasks=128):
  '''
  '''
  group_lengths = np.zeros(get_number_of_groups(data_location, snap), dtype=int)
  j = 0

  for i in range(NTasks):
    subfind_tab_name = (data_location + 'groups_' + snap + '/eagle_subfind_tab_' +
                        snap + '.' + str(i) + '.hdf5')
    subfind_tab = h5.File(subfind_tab_name, 'r')

    group_sub_lengths = subfind_tab['FOF/GroupLength'][()]

    k = len(group_sub_lengths)
    subfind_tab.close()

    group_lengths[j : j+k] = group_sub_lengths
    j += k

    #need the breakdown by type to really be usefull
    #only have the type breakdown for subhalos
    #sum of subhalos != group total
  return(group_lengths)

def get_number_of_subhalos(data_location, snap, NTasks=128):
  '''
  '''
  subhalos = np.zeros(get_number_of_groups(data_location, snap), dtype=int)
  j = 0

  for i in range(NTasks):
    subfind_tab_name = (data_location + 'groups_' + snap + '/eagle_subfind_tab_' +
                        snap + '.' + str(i) + '.hdf5')
    subfind_tab = h5.File(subfind_tab_name, 'r')

    number_of_subhalos = subfind_tab['FOF/NumOfSubhalos'][()]

    k = len(number_of_subhalos)
    subfind_tab.close()

    subhalos[j : j+k] = number_of_subhalos
    j += k

  return(subhalos)

class group_particle_data(object):
  def __init__(self):
    '''
    '''
    #can't use np.array becuase don't know how big arrays are before
    #PartType0
    self.gas_coordinates_list = []
    self.gas_velocity_list    = []
    self.gas_mass_list        = []
    self.gas_particle_binding_energy_list = []

    self.gas_particle_ID_list     = []
    self.gas_group_number_list    = []
    self.gas_subgroup_number_list = []

    #PartType1
    self.dm_coordinates_list = []
    self.dm_velocity_list    = []
    self.dm_mass_list        = []
    self.dm_particle_binding_energy_list = []

    self.dm_particle_ID_list     = []
    self.dm_group_number_list    = []
    self.dm_subgroup_number_list = []

    #PartType4
    self.star_coordinates_list = []
    self.star_velocity_list    = []
    self.star_mass_list        = []
    self.star_particle_binding_energy_list = []

    self.star_particle_ID_list     = []
    self.star_group_number_list    = []
    self.star_subgroup_number_list = []

    #PartType5
    self.bh_coordinates_list = []
    self.bh_velocity_list    = []
    self.bh_mass_list        = []
    self.bh_particle_binding_energy_list = []

    self.bh_particle_ID_list     = []
    self.bh_group_number_list    = []
    self.bh_subgroup_number_list = []

    return

  def add_gas_particle(self, coordinates, velocity, mass, binding_energy,
                       particle_ID, group_number, subgroup_number):

    self.gas_coordinates_list.append(coordinates)
    self.gas_velocity_list.append(velocity)
    self.gas_mass_list.append(mass)
    self.gas_particle_binding_energy_list.append(binding_energy)

    self.gas_particle_ID_list.append(particle_ID)
    self.gas_group_number_list.append(group_number)
    self.gas_subgroup_number_list.append(subgroup_number)

    return

  def add_dm_particle(self, coordinates, velocity, mass, binding_energy,
                      particle_ID, group_number, subgroup_number):

    self.dm_coordinates_list.append(coordinates)
    self.dm_velocity_list.append(velocity)
    self.dm_mass_list.append(mass)
    self.dm_particle_binding_energy_list.append(binding_energy)

    self.dm_particle_ID_list.append(particle_ID)
    self.dm_group_number_list.append(group_number)
    self.dm_subgroup_number_list.append(subgroup_number)

    return

  def add_star_particle(self, coordinates, velocity, mass, binding_energy,
                        particle_ID, group_number, subgroup_number):

    self.star_coordinates_list.append(coordinates)
    self.star_velocity_list.append(velocity)
    self.star_mass_list.append(mass)
    self.star_particle_binding_energy_list.append(binding_energy)

    self.star_particle_ID_list.append(particle_ID)
    self.star_group_number_list.append(group_number)
    self.star_subgroup_number_list.append(subgroup_number)

    return

  def add_bh_particle(self, coordinates, velocity, mass, binding_energy,
                      particle_ID, group_number, subgroup_number):

    self.bh_coordinates_list.append(coordinates)
    self.bh_velocity_list.append(velocity)
    self.bh_mass_list.append(mass)
    self.bh_particle_binding_energy_list.append(binding_energy)

    self.bh_particle_ID_list.append(particle_ID)
    self.bh_group_number_list.append(group_number)
    self.bh_subgroup_number_list.append(subgroup_number)

    return

def make_hdf_groups(base, data_locaiton, data_destination, identity, snap_list,
                    NSubs=16, NTasks=128):
  '''
  '''
  #this should raise errors if there is something wrong with the file destinations
  if not os.path.exists(data_destination):
    os.mkdir(data_destination)

  for snap in snap_list:

    #create folder for this redshift
    data_destination_with_snap = data_destination + '/' + snap

    if not os.path.exists(data_destination_with_snap):
      os.mkdir(data_destination_with_snap)

    #get number of groups from subfind tab
    number_of_groups = get_number_of_groups(data_location, snap)

    print('Creating', number_of_groups, 'hdf5 files.')

    #make and open a whole bunch of hdf5 files
    #also groups start from 1?, subgroups start from 0
    group_file_list = [[]]
    group_particle_data_list = [[]]

    centre_of_potential_list = [[]]
    number_of_subhalos = get_number_of_subhalos(data_location, snap)

    dm_mass = get_dm_mass(data_location, snap)

    for i in range(number_of_groups):
      group_name = data_destination_with_snap + '/' + identity + '.' + str(i+1) + '.hdf5'
      group_file = h5.File(group_name, 'w')

      group_file_list.append(group_file)
      group_particle_data_list.append(group_particle_data())
      centre_of_potential_list.append(np.zeros((number_of_subhalos[i], 3)))

    print('Getting subhalo centre of potential.')

    for i in range(NTasks):
      subfind_tab_name = (data_location + 'groups_' + snap + '/eagle_subfind_tab_' +
                          snap + '.' + str(i) + '.hdf5')
      subfind_tab = h5.File(subfind_tab_name, 'r')

      centre_of_potential = subfind_tab['Subhalo/CentreOfPotential'][()]
      group_number        = subfind_tab['Subhalo/GroupNumber'][()]
      subgroup_number     = subfind_tab['Subhalo/SubGroupNumber'][()]

      for (potential, group, sub) in zip(centre_of_potential,
                                         group_number, subgroup_number):
        centre_of_potential_list[group][sub] = potential

    for i in range(number_of_groups+1):
      if i != 0:
        sub_group = group_file_list[i].create_group('SubGroup')

        sub_group.create_dataset('CentreOfPotential', data = centre_of_potential_list[i])

    print('Reading and setting up data to be saved.')

    #loop through ALL the subfind data
    for i in range(NSubs):
      subfind_particles_name = (data_location + 'particledata_' + snap +
                                '/eagle_subfind_particles_' + snap + '.' + str(i) +
                                '.hdf5')
      subfind_particles = h5.File(subfind_particles_name, 'r')

      #gas
      gas_coordinates     = subfind_particles['PartType0/Coordinates'][()]
      gas_velocity        = subfind_particles['PartType0/Velocity'][()]
      gas_mass            = subfind_particles['PartType0/Mass'][()]
      gas_binding_energy  = subfind_particles['PartType0/ParticleBindingEnergy'][()]

      gas_particle_ID     = subfind_particles['PartType0/ParticleIDs'][()]
      gas_group_number    = subfind_particles['PartType0/GroupNumber'][()]
      gas_subgroup_number = subfind_particles['PartType0/SubGroupNumber'][()]

      #DM
      dm_coordinates     = subfind_particles['PartType1/Coordinates'][()]
      dm_velocity        = subfind_particles['PartType1/Velocity'][()]
      dm_binding_energy  = subfind_particles['PartType1/ParticleBindingEnergy'][()]

      dm_particle_ID     = subfind_particles['PartType1/ParticleIDs'][()]
      dm_group_number    = subfind_particles['PartType1/GroupNumber'][()]
      dm_subgroup_number = subfind_particles['PartType1/SubGroupNumber'][()]

      #stars
      star_coordinates     = subfind_particles['PartType4/Coordinates'][()]
      star_velocity        = subfind_particles['PartType4/Velocity'][()]
      star_mass            = subfind_particles['PartType4/Mass'][()]
      star_binding_energy  = subfind_particles['PartType4/ParticleBindingEnergy'][()]

      star_particle_ID     = subfind_particles['PartType4/ParticleIDs'][()]
      star_group_number    = subfind_particles['PartType4/GroupNumber'][()]
      star_subgroup_number = subfind_particles['PartType4/SubGroupNumber'][()]

      #BH
      bh_coordinates     = subfind_particles['PartType5/Coordinates'][()]
      bh_velocity        = subfind_particles['PartType5/Velocity'][()]
      bh_mass            = subfind_particles['PartType5/Mass'][()]
      bh_binding_energy  = subfind_particles['PartType5/ParticleBindingEnergy'][()]

      bh_particle_ID     = subfind_particles['PartType5/ParticleIDs'][()]
      bh_group_number    = subfind_particles['PartType5/GroupNumber'][()]
      bh_subgroup_number = subfind_particles['PartType5/SubGroupNumber'][()]

      subfind_particles.close()

      #gas
      for (coordinates, velocity, mass, binding_energy,
           ID, group_number, subgroup_number
            ) in zip(gas_coordinates, gas_velocity, gas_mass, gas_binding_energy,
                     gas_particle_ID, gas_group_number, gas_subgroup_number):

        group_particle_data_list[np.abs(group_number)].add_gas_particle(
          coordinates, velocity, mass, binding_energy,
          ID, group_number, subgroup_number)

      #DM
      for (coordinates, velocity, binding_energy,
           ID, group_number, subgroup_number
            ) in zip(dm_coordinates, dm_velocity, dm_binding_energy,
                     dm_particle_ID, dm_group_number, dm_subgroup_number):

        group_particle_data_list[np.abs(group_number)].add_dm_particle(
          coordinates, velocity, dm_mass, binding_energy,
          ID, group_number, subgroup_number)

      #stars
      for (coordinates, velocity, mass, binding_energy,
           ID, group_number, subgroup_number
            ) in zip(star_coordinates, star_velocity, star_mass, star_binding_energy,
                     star_particle_ID, star_group_number, star_subgroup_number):

        group_particle_data_list[np.abs(group_number)].add_star_particle(
          coordinates, velocity, mass, binding_energy,
          ID, group_number, subgroup_number)

      #BH
      for (coordinates, velocity, mass, binding_energy,
           ID, group_number, subgroup_number
            ) in zip(bh_coordinates, bh_velocity, bh_mass, bh_binding_energy,
                     bh_particle_ID, bh_group_number, bh_subgroup_number):

        group_particle_data_list[np.abs(group_number)].add_bh_particle(
          coordinates, velocity, mass, binding_energy,
          ID, group_number, subgroup_number)

      print('Done ' + subfind_particles_name)

    print('Saving')

    for i in range(number_of_groups+1):
      if i != 0:
        if i % 100 == 0:
          print('Saved first', i, 'groups')

        part_type_0 = group_file_list[i].create_group('PartType0')
        part_type_1 = group_file_list[i].create_group('PartType1')
        part_type_4 = group_file_list[i].create_group('PartType4')
        part_type_5 = group_file_list[i].create_group('PartType5')

        #gas
        part_type_0.create_dataset('Coordinates', data = np.array(
          group_particle_data_list[i].gas_coordinates_list))
        part_type_0.create_dataset('Velocity', data = np.array(
          group_particle_data_list[i].gas_velocity_list))
        part_type_0.create_dataset('Mass', data = np.array(
          group_particle_data_list[i].gas_mass_list))
        part_type_0.create_dataset('ParticleBindingEnergy', data = np.array(
          group_particle_data_list[i].gas_particle_binding_energy_list))

        part_type_0.create_dataset('GroupNumber', data = np.array(
          group_particle_data_list[i].gas_group_number_list, dtype=int))
        part_type_0.create_dataset('SubGroupNumber', data = np.array(
          group_particle_data_list[i].gas_subgroup_number_list, dtype=int))
        part_type_0.create_dataset('ParticleIDs', data = np.array(
          group_particle_data_list[i].gas_particle_ID_list, dtype=int))

        #DM
        part_type_1.create_dataset('Coordinates', data = np.array(
          group_particle_data_list[i].dm_coordinates_list))
        part_type_1.create_dataset('Velocity', data = np.array(
          group_particle_data_list[i].dm_velocity_list))
        part_type_1.create_dataset('Mass', data = np.array(
          group_particle_data_list[i].dm_mass_list))
        part_type_1.create_dataset('ParticleBindingEnergy', data = np.array(
          group_particle_data_list[i].dm_particle_binding_energy_list))

        part_type_1.create_dataset('GroupNumber', data = np.array(
          group_particle_data_list[i].dm_group_number_list, dtype=int))
        part_type_1.create_dataset('SubGroupNumber', data = np.array(
          group_particle_data_list[i].dm_subgroup_number_list, dtype=int))
        part_type_1.create_dataset('ParticleIDs', data = np.array(
          group_particle_data_list[i].dm_particle_ID_list, dtype=int))

        #star
        part_type_4.create_dataset('Coordinates', data = np.array(
          group_particle_data_list[i].star_coordinates_list))
        part_type_4.create_dataset('Velocity', data = np.array(
          group_particle_data_list[i].star_velocity_list))
        part_type_4.create_dataset('Mass', data = np.array(
          group_particle_data_list[i].star_mass_list))
        part_type_4.create_dataset('ParticleBindingEnergy', data = np.array(
          group_particle_data_list[i].star_particle_binding_energy_list))

        part_type_4.create_dataset('GroupNumber', data = np.array(
          group_particle_data_list[i].star_group_number_list, dtype=int))
        part_type_4.create_dataset('SubGroupNumber', data = np.array(
          group_particle_data_list[i].star_subgroup_number_list, dtype=int))
        part_type_4.create_dataset('ParticleIDs', data = np.array(
          group_particle_data_list[i].star_particle_ID_list, dtype=int))

        #BH
        part_type_5.create_dataset('Coordinates', data = np.array(
          group_particle_data_list[i].bh_coordinates_list))
        part_type_5.create_dataset('Velocity', data = np.array(
          group_particle_data_list[i].bh_velocity_list))
        part_type_5.create_dataset('Mass', data = np.array(
          group_particle_data_list[i].bh_mass_list))
        part_type_5.create_dataset('ParticleBindingEnergy', data = np.array(
          group_particle_data_list[i].bh_particle_binding_energy_list))

        part_type_5.create_dataset('GroupNumber', data = np.array(
          group_particle_data_list[i].bh_group_number_list, dtype=int))
        part_type_5.create_dataset('SubGroupNumber', data = np.array(
          group_particle_data_list[i].bh_subgroup_number_list, dtype=int))
        part_type_5.create_dataset('ParticleIDs', data = np.array(
          group_particle_data_list[i].bh_particle_ID_list, dtype=int))

    print('Closing files.')

    for i in range(number_of_groups+1):
      if i != 0:
        group_file_list[i].close()

  print('Done')
  return()

if __name__ == '__main__':

  #TODO should make this argv argc inputs
  #file stuff
  base             = '/home/matt/Documents/UWA/EAGLE/'
  data_location    = base + 'L0012N0188/EAGLE_REFERENCE/data/'
  data_destination = base + 'processed_data/'

  #name to postpend to files
  identity = '_12Mpc_188'

  #which snapshots to look at
  # snap_list = ['028_z000p000', '023_z000p503']
  # snap_list = ['023_z000p503']
  snap_list = ['019_z001p004']

  # print(get_number_of_groups(data_location, snap_list[0]))
  # print(get_group_lengths(data_location, snap_list[0], NTasks=128))
  # print(get_number_of_subhalos(data_location, snap_list[0]))
  # print(get_dm_mass(data_location, snap_list[0]))

  make_hdf_groups(base, data_location, data_destination, identity, snap_list)
