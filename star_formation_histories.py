#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 15:35:27 2021

@author: matt
"""

#import os
import numpy as np

import matplotlib
matplotlib.use('Agg')

import h5py  as h5

from scipy.integrate         import quad
from scipy.optimize          import brentq
from scipy.spatial.transform import Rotation
from scipy.stats             import binned_statistic

import pickle

import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

LITTLE_H = 0.6777
#box size should be saved and read in
BOX_SIZE = 147.55792
#SCALE_A  = 1

def do_everything(particle_data_location, galaxy_data_location, tree_data_location,
                  name, n_centrals=10000, apature=30):
  '''Needs to do everything because of memory / speed things.
  '''
  #read z=0 halos
  file_name = '%s%s_%s_galaxies.hdf5'%(galaxy_data_location, '028_z000p000', name)
  z0_data = h5.File(file_name,'r')

  #get ids
  z0_group_numbers      = z0_data['/IDs/GroupID'][()]
  z0_subgroup_numbers   = z0_data['/SubHaloData/SubHaloID'][()]
  # z0_top_leaf_numbers   = z0_data['/IDs/TopLeafID'][()]
  centre_of_pot_numbers = z0_data['SubHaloData/Pos_cop'][()]

  SCALE_A = z0_data['/Header/a'][()]

  z0_data.close()

  #tree stuff
  f = open(tree_data_location,'rb')
  kd_tree = pickle.load(f)
  f.close()

  #stars
  file_name = '%s%s_%s_Star.hdf5'%(particle_data_location, '028_z000p000', name)
  star_data = h5.File(file_name, 'r')

  # star_group    = star_data['PartData/GrpNum_Star'][()]
  # star_subgroup = star_data['PartData/SubNum_Star'][()]

  #load quantities
  star_pos         = star_data['PartData/PosStar'][()]
  star_vel         = star_data['PartData/VelStar'][()]
  star_mass        = star_data['PartData/MassStar'][()]
  star_formation_a = star_data['PartData/StellarFormationTime'][()]

  star_data.close()

  #plot stuff
  # mass_ranges = [12, 11.5, 11, 10.5, 10, 9.5, 9]

  #snapshot redshifts
  snap_z = np.array([0.0, 0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487, 1.737, 2.012, 1000])
  snap_a = 1 / (1 + snap_z)
  snap_t = lbt(snap_a)

  #12
  (fig12,  ax12,  ax212)  = setup_plot(100,   apature, snap_t, snap_z)
  (fig115, ax115, ax2115) = setup_plot(31.6,  apature, snap_t, snap_z)
  (fig11,  ax11,  ax211)  = setup_plot(10,    apature, snap_t, snap_z)
  (fig105, ax105, ax2105) = setup_plot(3.16,  apature, snap_t, snap_z)
  (fig10,  ax10,  ax210)  = setup_plot(1,     apature, snap_t, snap_z)
  (fig95,  ax95,  ax295)  = setup_plot(0.316, apature, snap_t, snap_z)
  (fig9,   ax9,   ax29)   = setup_plot(0.1,   apature, snap_t, snap_z)

  for halo_i in range(n_centrals):

    centre_of_potential = None

    for index, (group, subgroup) in enumerate(zip(z0_group_numbers, z0_subgroup_numbers)):
      #numbers are annoying in EAGLE database
      if group - 28000000000000 + 1 == halo_i:
        #centrals only
        if subgroup == 0:
          centre_of_potential = centre_of_pot_numbers[index]

    if np.any(centre_of_potential == None):
      print("Warning: Didn't find centre of potential for ", halo_i)
      continue

    #use the tree
    star_index_mask = kd_tree.query_ball_point(x = centre_of_potential * SCALE_A / LITTLE_H,
                                               r = apature/1000) #in Mpc

    pos    = star_pos[star_index_mask]
    vel    = star_vel[star_index_mask]
    mass   = star_mass[star_index_mask]
    form_a = star_formation_a[star_index_mask]

    pos = pos - centre_of_potential - BOX_SIZE/2
    pos %= BOX_SIZE
    pos -= BOX_SIZE/2
    pos *= 1000 * SCALE_A / LITTLE_H #to kpc

    vel  *= np.sqrt(SCALE_A) #km/s
    mass /= LITTLE_H #10^10 M_sun
    vel = vel - np.sum(mass[:, np.newaxis] * vel, axis=0) / np.sum(mass)

    form_t = lbt(form_a)

    arg_order  = np.argsort(form_t)
    mass_order = mass[arg_order]
    cum_t      = form_t[arg_order]
    cum_mass   = np.flip(np.cumsum(np.flip(mass_order)))

    log_star_mass_tot = np.log10(cum_mass[0])

    if   log_star_mass_tot > 1.5:  ax12.errorbar( cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)
    elif log_star_mass_tot > 1:    ax115.errorbar(cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)
    elif log_star_mass_tot > 0.5:  ax11.errorbar( cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)
    elif log_star_mass_tot > 0:    ax105.errorbar(cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)
    elif log_star_mass_tot > -0.5: ax10.errorbar( cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)
    elif log_star_mass_tot > -1:   ax95.errorbar( cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)
    elif log_star_mass_tot > -1.5: ax9.errorbar(  cum_t, cum_mass, fmt='', linestyle='-', alpha=0.6)

  fig12.savefig( '/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh12',  bbox_inches='tight')
  fig115.savefig('/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh115', bbox_inches='tight')
  fig11.savefig( '/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh11',  bbox_inches='tight')
  fig105.savefig('/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh105', bbox_inches='tight')
  fig10.savefig( '/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh10',  bbox_inches='tight')
  fig95.savefig( '/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh95',  bbox_inches='tight')
  fig9.savefig(  '/home/mwilkins/EAGLE/plots/' + sim + '/stacked_sfh9',   bbox_inches='tight')

  return

def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  R = find_rotaton_matrix(galaxy_anular_momentum)
  pos = R.apply(pos)
  '''
  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

def align(pos, vel, mass, apature=30):
  '''Aligns the z cartesian direction with the direction of angular momentum.
  Can use an apature.
  '''
  #direction to align
  if apature != None:
    r = np.linalg.norm(pos, axis=1)
    mask = r < apature

    j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

  else:
    j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

  #find rotation
  rotation = find_rotaion_matrix(j_tot)

  #rotate stars
  pos = rotation.apply(pos)
  vel = rotation.apply(vel)

  return(pos, vel)

#to get star particle age
def lbt(a,omm=0.307,oml=0.693,h=0.6777):
    z = 1.0/a-1
    t = np.zeros(len(z))
    for i in range(len(z)):
        t[i] = 1e+3*3.086e+16/(3.154e+7*1e+9)*(1.0/(100*h))*quad(lambda z: 1/((1+z)*np.sqrt(omm*(1+z)**3+oml)),0,z[i])[0]#in billion years
    return(t)

def setup_plot(max_y, apature, snap_t, snap_z):
  '''
  '''
  fig12 = plt.figure()
  fig12.set_size_inches(8, 5, forward=True)

  ax12 = fig12.add_subplot(111)
  ax212 = ax12.twiny()

  ax12.vlines(snap_t, 0, 1.1*max_y, linestyles='--', alpha=0.4)

  ax12.set_ylabel(r'$M_\star (<%d$kpc, $z=0)$ [$10^{10} $M$_\odot$]'%(apature))
  ax12.set_xlabel(r'$t$ [Gyr]')

  ax212.set_xlabel(r'$z$')
  ax212.set_xticks(np.concatenate((snap_t[:-1:2], [snap_t[-2]])))
  ax212.set_xticklabels(np.round(snap_z[:-1:2],2))

  ax12.set_xlim((0,snap_t[-2]))
  ax12.set_ylim((0, 1.1*max_y))

  return(fig12, ax12, ax212)

if __name__ == '__main__':

  sim = 'L0100N1504/REFERENCE'
  particle_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/'
  galaxy_data_location   = '/fred/oz009/ejimenez/Galaxies/' + sim + '/'
  tree_data_location     = '/fred/oz009/amanuwal/startree_z000p000.p'

  name = '100Mpc_1504'

  do_everything(particle_data_location, galaxy_data_location, tree_data_location,
                name, n_centrals=10000, apature=30)

  print('done :)')
  pass