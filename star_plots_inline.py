#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 14:34:48 2021

@author: matt
"""

import os
import sys
import time
import pickle

import numpy as np

# import matplotlib
# matplotlib.use('Agg')

import h5py as h5

from scipy.integrate         import quad
from scipy.optimize import brentq
from scipy.spatial.transform import Rotation
from scipy.stats import binned_statistic

# import pickle
from scipy.spatial import cKDTree

import matplotlib
matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

# ignore devide by zero
np.seterr(divide='ignore', invalid='ignore')

LITTLE_H = 0.6777
# box size should be saved and read in
# BOX_SIZE = 67.77
BOX_SIZE = 33.885
# SCALE_A  = 1
DM_MASS = 6.570332889156362E-4 / 7

n_centrals = 1000

#to get star particle age
def lbt(z=[],a=[],omm=0.307,oml=0.693,h=0.6777):
  if a != []:
    z = 1.0/a-1
  t = np.zeros(len(z))
  for i in range(len(z)):
    t[i] = 1e+3*3.086e+16/(3.154e+7*1e+9)*(1.0/(100*h))*quad(lambda z: 1/((1+z)*np.sqrt(omm*(1+z)**3+oml)),0,z[i])[0]#in billion years
  return(t)


def read(particle_data_location, halo_data_location, output_data_location, kdtree_location, snap):
    fn = halo_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.0.hdf5'
    print('Halos:', fn, ' ...')

    with h5.File(fn, "r") as fs:

        Header = fs['Header'].attrs
        Ntask = Header['NTask']
        TotNgroups = Header['TotNgroups']
        # TotNsubgroups = Header['TotNsubgroups']

    # Halo arrays
    Group_M_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    Group_R_Crit200 = np.zeros(TotNgroups, dtype=np.float32)
    GroupCentreOfPotential = np.zeros((TotNgroups, 3), dtype=np.float32)
    # FirstSub = np.zeros(TotNgroups, dtype=np.int64)

    # Subhalo arrays
    # SubGroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    # GroupNumber = np.zeros(TotNsubgroups, dtype=np.int64)
    # SubGroupCentreOfPotential = np.zeros((TotNsubgroups, 3), dtype=np.float64)

    NGrp_c = 0
    # NSub_c = 0

    for ifile in range(Ntask):
        fn = particle_data_location + 'groups_' + snap + '/eagle_subfind_tab_' + snap + '.' + str(ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:

            Header = fs['Header'].attrs

            Ngroups = Header['Ngroups']
            Nsubgroups = Header['Nsubgroups']

            if Ngroups > 0:
                Group_M_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_M_Crit200"][()]
                Group_R_Crit200[NGrp_c:NGrp_c + Ngroups] = fs["FOF/Group_R_Crit200"][()]

                GroupCentreOfPotential[NGrp_c:NGrp_c + Ngroups] = fs["FOF/GroupCentreOfPotential"][()]
                # FirstSub[NGrp_c:NGrp_c + Ngroups] = fs["FOF/FirstSubhaloID"][()]

                NGrp_c += Ngroups

            # if Nsubgroups > 0:
            # SubGroupCentreOfPotential[NSub_c:NSub_c + Nsubgroups, :] = fs["Subhalo/CentreOfPotential"][()]
            # SubGroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/SubGroupNumber"][()]
            # GroupNumber[NSub_c:NSub_c + Nsubgroups] = fs["Subhalo/GroupNumber"][()]
            # NSub_c += Nsubgroups

    print('Loaded halos')

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    # if NumPartTot[4] > 0:
    PosStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    VelStar = np.zeros((NumPartTot[4], 3), dtype=np.float32)
    MassStar = np.zeros(NumPartTot[4], dtype=np.float32)
    # BirthDensity   = np.zeros(NumPartTot[4],    dtype=np.float32)
    # Metallicity = np.zeros(NumPartTot[4], dtype=np.float32)
    Star_aform = np.zeros(NumPartTot[4], dtype=np.float32)
    # Star_tform     = np.zeros(NumPartTot[4],    dtype=np.float32)
    BindingEnergyStar = np.zeros(NumPartTot[4], dtype=np.float32)
    # HSML_Star      = np.zeros(NumPartTot[4],    dtype=np.float32)
    # ParticleIDs = np.zeros(NumPartTot[4], dtype=np.int32)
    GrpNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)
    SubNum_Star = np.zeros(NumPartTot[4], dtype=np.int32)

    PosGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    VelGas = np.zeros((NumPartTot[0], 3), dtype=np.float32)
    MassGas = np.zeros(NumPartTot[0], dtype=np.float32)
    BindingEnergyGas = np.zeros(NumPartTot[0], dtype=np.float32)
    GrpNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)
    SubNum_Gas = np.zeros(NumPartTot[0], dtype=np.int32)

    # PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    # VelDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    # GrpNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)
    # SubNum_DM = np.zeros(NumPartTot[1], dtype=np.int32)

    NStar_c = 0
    NGas_c = 0
    # NDM_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            if NumParts[4] > 0:
                PosStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Coordinates"].value
                VelStar[NStar_c:NStar_c + NumParts[4], :] = fs["PartType4/Velocity"].value
                MassStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/Mass"].value
                # BirthDensity[NStar_c:NStar_c+NumParts[4]] = fs["PartType4/BirthDensity"].value
                # Metallicity[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SmoothedMetallicity"].value
                Star_aform[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/StellarFormationTime"].value
                # Star_tform[NStar_c:NStar_c+NumParts[4]]   = lbt(Star_aform[NStar_c:NStar_c+NumParts[4]])
                BindingEnergyStar[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleBindingEnergy"].value
                # HSML_Star[NStar_c:NStar_c+NumParts[4]]    = fs["PartType4/SmoothingLength"].value
                # ParticleIDs[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/ParticleIDs"].value
                GrpNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/GroupNumber"].value
                SubNum_Star[NStar_c:NStar_c + NumParts[4]] = fs["PartType4/SubGroupNumber"].value

                NStar_c += NumParts[4]

            if NumParts[0] > 0:
                PosGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Coordinates"].value
                VelGas[NGas_c:NGas_c + NumParts[0], :] = fs["PartType0/Velocity"].value
                MassGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/Mass"].value
                BindingEnergyGas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/ParticleBindingEnergy"].value
                GrpNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/GroupNumber"].value
                SubNum_Gas[NGas_c:NGas_c + NumParts[0]] = fs["PartType0/SubGroupNumber"].value

                NGas_c += NumParts[0]

            # if NumParts[1] > 0:
            #     PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"].value
            #     VelDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Velocity"].value
            #     GrpNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/GroupNumber"].value
            #     SubNum_DM[NDM_c:NDM_c + NumParts[1]] = fs["PartType1/SubGroupNumber"].value
            #
            #     NDM_c += NumParts[1]

    print('loaded particles')

    print('loaded everything')

    return (output_data_location, kdtree_location, snap,
            Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
            PosStar, VelStar, MassStar, Star_aform,
            BindingEnergyStar,
            GrpNum_Star, SubNum_Star,
            PosGas, VelGas, MassGas,
            BindingEnergyGas,
            GrpNum_Gas, SubNum_Gas)
            # PosDM, VelDM,
            # GrpNum_DM, SubNum_DM)


def calculate(output_data_location, kdtree_location, snap,
              Group_M_Crit200, Group_R_Crit200, GroupCentreOfPotential,
              PosStar, VelStar, MassStar, Star_aform,
              BindingEnergyStar,
              GrpNum_Star, SubNum_Star,
              PosGas, VelGas, MassGas,
              BindingEnergyGas,
              GrpNum_Gas, SubNum_Gas):
              # PosDM, VelDM,
              # GrpNum_DM, SubNum_DM):

    start_time = time.time()
    star_kd_tree = cKDTree(PosStar, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated star kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')
    start_time = time.time()
    gas_kd_tree  = cKDTree(PosGas, leafsize=10, boxsize=BOX_SIZE)
    print('Calculated gas kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')

    # dm_tree_file_name = kdtree_location + snap + '_dm_tree.pickle'
    # if os.path.exists(dm_tree_file_name):
    #     with open(dm_tree_file_name, 'r') as dm_tree_file:
    #         dm_kd_tree = pickle.load(dm_tree_file)
    #     print('Saved dm kdtree')
    #
    # else:
    #     start_time = time.time()
    #     dm_kd_tree = cKDTree(PosDM, leafsize=10, boxsize=BOX_SIZE)
    #     with open(dm_tree_file_name, 'wb') as dm_tree_file:
    #         pickle.dump(dm_kd_tree, dm_tree_file)
    #     print('Calculated dm kdtrees in ' + str(np.round(time.time() - start_time, 1)) + 's')

    print('Done all kdtrees.')

    # lin_bin_edges = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
    #              21,22,23,24,25,26,27,28,29,30]
    lin_bin_edges = np.linspace(0, 30, 11)  # *2 for proj and 3d
    log_bin_edges = np.logspace(np.log10(1), np.log10(30), 6)  # *2 for proj and 3d
    # + <30kpc, <r200, <R_half, <2*R_half, <5*R_half, <R_quater, <R_3quater

    # change if bins change
    n_lin = 10
    n_log = 5
    n_scale = 8
    n_dim = 2 * n_lin + 2 * n_log + n_scale

    for group_id in range(n_centrals):
        # try:

        # extract ids
        # subfind_index = np.argmax([group_numbers == group_id and subgroup_numbers == 0])
        group_ids[group_id] = group_id
        subgroup_ids[group_id] = 0

        r200 = Group_R_Crit200[group_id]  # eagle units
        galaxy_m200[group_id]  = Group_M_Crit200[group_id]  # eagle units
        centre_of_potential = GroupCentreOfPotential[group_id]

        # use the tree
        star_index_mask = star_kd_tree.query_ball_point(x=centre_of_potential,
                                                        r=30 / 1000 * SCALE_A / LITTLE_H)  # in Mpc

        # mask relevant
        pos = PosStar[star_index_mask]
        vel = VelStar[star_index_mask]
        mass = MassStar[star_index_mask]
        pot = BindingEnergyStar[star_index_mask]
        form_a = Star_aform[star_index_mask]

        if len(mass) == 0:
            print('Warning: group', group_id, 'has no star particles < 30kpc.')
            continue

        # real units and centre and everything
        pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
        pos %= SCALE_A * BOX_SIZE
        pos -= SCALE_A * BOX_SIZE / 2
        pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

        r = np.linalg.norm(pos, axis=1)
        mask = (r < 30)

        vel *= np.sqrt(SCALE_A)  # km/s
        mass /= LITTLE_H  # 10^10 M_sun

        vel_offset = np.sum(mass[mask, np.newaxis] * vel[mask, :], axis=0) / np.sum(mass[mask])
        vel -= vel_offset

        # pot *= SCALE_A #(km/s)^2

        (pos, vel, rotation) = align(pos, vel, mass, apature=30)

        # cylindrical coords
        (R, phi, z, v_R, v_phi, v_z
         ) = get_cylindrical(pos, vel)
        # r = np.linalg.norm(pos, axis=1)

        # j_z = np.cross(R, v_phi)[:, 2]
        j_z = pos[:, 0] * vel[:, 1] - pos[:, 1] * vel[:, 0]
        j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

        arg_pot = np.argsort(pot)
        arg_arg = np.argsort(arg_pot)

        # TODO check
        j_E = max_within_50(j_tot[arg_pot])[arg_arg]
        j_zonc = j_z / j_E

        # aparures
        pr200 = r200 * SCALE_A / LITTLE_H * 1000  # kcp
        (r_half, r_quater, r3quater) = get_radii_that_are_interesting(r, mass)
        (R_half, R_quater, R3quater) = get_radii_that_are_interesting(R, mass)

        # calculate profiles
        lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        proj_lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                   form_a)
        proj_log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                   form_a)

        # calculate
        apature_30 = get_kinematic_apature(30, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature_r200 = get_kinematic_apature(pr200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature1half = get_kinematic_apature(R_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature2half = get_kinematic_apature(2 * R_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature5half = get_kinematic_apature(5 * R_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)

        annuli_half = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
        annuli_quarter = get_kinematic_annuli(R_quater, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
        annuli3quarter = get_kinematic_annuli(R3quater, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
        # annuli_half   = get_kinematic_apature(r_half,   pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        # apature2half   = get_kinematic_apature(2*r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        # annuli_quarter = get_kinematic_apature(r_quater, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        # annuli3quarter = get_kinematic_apature(r3quater, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)

        # use the tree
        gas_index_mask = gas_kd_tree.query_ball_point(x=centre_of_potential,
                                                      r=30 / 1000 * SCALE_A / LITTLE_H)  # in Mpc

        # mask relevant
        pos = PosGas[gas_index_mask]
        vel = VelGas[gas_index_mask]
        mass = MassGas[gas_index_mask]
        pot = BindingEnergyGas[gas_index_mask]
        form_a = np.zeros(len(mass))

        if len(mass) == 0:
            print('Warning: group', group_id, 'has no gas particles < 30kpc.')
            continue

        # real units and centre and everything
        pos = pos - centre_of_potential - SCALE_A * BOX_SIZE / 2
        pos %= SCALE_A * BOX_SIZE
        pos -= SCALE_A * BOX_SIZE / 2
        pos *= 1000 * SCALE_A / LITTLE_H  # to kpc

        r = np.linalg.norm(pos, axis=1)
        mask = (r < 30)

        vel *= np.sqrt(SCALE_A)  # km/s
        mass /= LITTLE_H  # 10^10 M_sun

        vel -= vel_offset

        # pot *= SCALE_A #(km/s)^2

        # (pos, vel, rotation) = align(pos, vel, mass, apature=30)
        pos = rotation.apply(pos)
        vel = rotation.apply(vel)

        # cylindrical coords
        (R, phi, z, v_R, v_phi, v_z
         ) = get_cylindrical(pos, vel)
        # r = np.linalg.norm(pos, axis=1)

        # j_z = np.cross(R, v_phi)
        j_z = pos[:, 0] * vel[:, 1] - pos[:, 1] * vel[:, 0]
        j_tot = np.linalg.norm(np.cross(pos, vel), axis=1)

        arg_pot = np.argsort(pot)
        arg_arg = np.argsort(arg_pot)

        # TODO check
        j_E = max_within_50(j_tot[arg_pot])[arg_arg]
        j_zonc = j_z / j_E

        # calculate profiles
        lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        proj_lin_profiles = get_kinematic_profiles(lin_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                   form_a)
        proj_log_profiles = get_kinematic_profiles(log_bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc,
                                                   form_a)

        # calculate
        apature_30 = get_kinematic_apature(30, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature_r200 = get_kinematic_apature(pr200, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature1half = get_kinematic_apature(R_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature2half = get_kinematic_apature(2 * R_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        apature5half = get_kinematic_apature(5 * R_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)

        annuli_half = get_kinematic_annuli(R_half, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
        annuli_quarter = get_kinematic_annuli(R_quater, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
        annuli3quarter = get_kinematic_annuli(R3quater, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a)
        # annuli_half   = get_kinematic_apature(r_half,   pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        # apature2half   = get_kinematic_apature(2*r_half, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        # annuli_quarter = get_kinematic_apature(r_quater, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)
        # annuli3quarter = get_kinematic_apature(r3quater, pos, vel, mass, r, z, v_R, v_phi, v_z, j_zonc, form_a)

        # #DM stuff
        # dm_index_mask = dm_kd_tree.query_ball_point(x = centre_of_potential,
        #                                             r = 30 / 1000 * SCALE_A / LITTLE_H)
        #
        # dpos = PosDM[dm_index_mask]
        # dvel = VelDM[dm_index_mask]
        #
        # #real units and centre and everything
        # dpos = dpos - centre_of_potential - SCALE_A * BOX_SIZE/2
        # dpos %= SCALE_A * BOX_SIZE
        # dpos -= SCALE_A * BOX_SIZE/2
        # dpos *= 1000 * SCALE_A / LITTLE_H #to kpc
        #
        # dvel *= np.sqrt(SCALE_A) #km/s
        # dvel -= vel_offset
        #
        # dpos = rotation.apply(pos)
        # dvel = rotation.apply(vel)
        #
        # #dm profiles
        # dm_r = np.linalg.norm(dpos, axis=1)
        # dm_R = np.sqrt(dpos[:, 0]**2 + dpos[:, 1]**2)
        #
        # dm_half_mask = np.logical_and(10**-0.1<dm_R/R_half, dm_R/R_half<10**0.1)
        # dm_quarter_mask = np.logical_and(10**-0.1<dm_R/R_quater, dm_R/R_quater<10**0.1)
        # dm3quarter_mask = np.logical_and(10**-0.1<dm_R/R3quater, dm_R/R3quater<10**0.1)

        if group_id % 100 == 0:
            print('done', group_id)

        # except Exception as e:
        #     print('Exception was thrown for group: ', group_id)
        #     print(e)

    print('Done all calculations.')

    return (output_data_location, snap,
            galaxy_r200, galaxy_m200,
            star_half_mass_proj_radius, star_quater_mass_proj_radius, star3quater_mass_proj_radius,
            star_half_mass_radius, star_quater_mass_radius, star3quater_mass_radius,
            star_bin_mass, star_bin_N, fjzjc,
            star_kappa_rot, star_kappa_co,
            star_mean_v_R, star_mean_v_phi, star_median_v_phi,
            star_sigma_z, star_sigma_R, star_sigma_phi,
            star_z_half, formation_a_median,
            star_axis_a, star_axis_b, star_axis_c,
            gas_bin_mass, gas_bin_N,
            gas_kappa_rot, gas_kappa_co,
            gas_mean_v_R, gas_mean_v_phi, gas_median_v_phi,
            gas_sigma_z, gas_sigma_R, gas_sigma_phi,
            gas_z_half,
            gas_axis_a, gas_axis_b, gas_axis_c)
            # dm_N,
            # dm_sigma_x, dm_sigma_y, dm_sigma_z)


def get_cylindrical(PosStars, VelStars):
    """Calculates cylindrical coordinates.
    """
    rho = np.sqrt(np.square(PosStars[:, 0]) + np.square(PosStars[:, 1]))
    varphi = np.arctan2(PosStars[:, 1], PosStars[:, 0])
    z = PosStars[:, 2]

    v_rho = VelStars[:, 0] * np.cos(varphi) + VelStars[:, 1] * np.sin(varphi)
    v_varphi = -VelStars[:, 0] * np.sin(varphi) + VelStars[:, 1] * np.cos(varphi)
    v_z = VelStars[:, 2]

    return (rho, varphi, z, v_rho, v_varphi, v_z)


def find_rotaion_matrix(j_vector):
    """Returns a scipy.spatial.transform.Rotation object.
    R = find_rotaton_matrix(galaxy_anular_momentum)
    pos = R.apply(pos)
    """
    # rotate until x coord = 0
    fy = lambda y: Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
    y = brentq(fy, 0, 180)

    # rotate until y coord = 0
    fx = lambda x: Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)[1]
    x = brentq(fx, 0, 180)

    # check it isn't upsidedown
    j_tot = Rotation.from_euler('yx', [y, x], degrees=True).apply(j_vector)

    if j_tot[2] < 0:
        x += 180

    return Rotation.from_euler('yx', [y, x], degrees=True)


def align(pos, vel, mass, apature=30):
    """Aligns the z cartesian direction with the direction of angular momentum.
    Can use an apature.
    """
    # direction to align
    if apature is not None:
        r = np.linalg.norm(pos, axis=1)
        mask = r < apature

        j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

    else:
        j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

    # find rotation
    rotation = find_rotaion_matrix(j_tot)

    # rotate stars
    pos = rotation.apply(pos)
    vel = rotation.apply(vel)

    return pos, vel, rotation


def get_radii_that_are_interesting(R, mass):
    """Find star half mass, quater mass and 3 quater mass.
    """
    arg_order = np.argsort(R)
    cum_mass = np.cumsum(mass[arg_order])
    # fraction
    cum_mass /= cum_mass[-1]

    half = R[arg_order][np.where(cum_mass > 0.50)[0][0]]
    quater = R[arg_order][np.where(cum_mass > 0.25)[0][0]]
    three = R[arg_order][np.where(cum_mass > 0.75)[0][0]]

    return half, quater, three


def max_within_50(array, fifty=50):
    """returns the largest value of array within 50 indices
    probably can be faster
    """
    list_len = np.size(array)

    max_array = np.zeros(list_len)

    for i in range(list_len):
        low = np.amax((0, i - fifty))
        hi = np.amin((i + fifty, list_len))

        max_array[i] = np.amax(array[low:hi])

    return max_array


def reduced_quadrupole_moments_of_mass_tensor(r_p, m_p, e2_p):
    """Calculates the reduced inertia tensor
    M_i,j = sum_p m_p/r_~p^2 . r_p,i r_p,j / sum_p m_p/r_p^2
    Itterative selection is done in the other function.
    """
    norm = m_p / e2_p

    m = np.zeros((3, 3))

    for i in range(3):
        for j in range(3):
            m[i, j] = np.sum(norm * r_p[:, i] * r_p[:, j])

    m /= np.sum(norm)

    return m


def process_tensor(m):
    """
    """
    # I think I messed np.linalg.eigh(m) up when I was first writing thins
    if np.any(np.isnan(m)) or np.any(np.isinf(m)):
        print('Found a nan or inf', m)
        return np.zeros(3, dtype=np.float64), np.identity(3, dtype=np.float64)

    (eigan_values, eigan_vectors) = np.linalg.eig(m)

    order = np.flip(np.argsort(eigan_values))

    eigan_values = eigan_values[order]
    eigan_vectors = eigan_vectors[:, order]

    return eigan_values, eigan_vectors


def defined_particles(pos, mass, eigan_values, eigan_vectors):
    """Assumes eigan values are sorted
    """
    # projection along each axis
    projected_a = (pos[:, 0] * eigan_vectors[0, 0] + pos[:, 1] * eigan_vectors[1, 0] +
                   pos[:, 2] * eigan_vectors[2, 0])
    projected_b = (pos[:, 0] * eigan_vectors[0, 1] + pos[:, 1] * eigan_vectors[1, 1] +
                   pos[:, 2] * eigan_vectors[2, 1])
    projected_c = (pos[:, 0] * eigan_vectors[0, 2] + pos[:, 1] * eigan_vectors[1, 2] +
                   pos[:, 2] * eigan_vectors[2, 2])

    # ellipse distance #Thob et al. 2019 eqn 4.
    ellipse_distance = (np.square(projected_a) + np.square(projected_b) / (eigan_values[1] / eigan_values[0]) +
                        np.square(projected_c) / (eigan_values[2] / eigan_values[0]))

    # #this seems to make almost no difference.
    # #I'm not convinced that the method in Thob is the correct way to do this
    # #ellipse radius #Thob et al. 2019 eqn 4.
    # ellipse_radius = np.power((eigan_values[1] * eigan_values[2]) / np.square(eigan_values[0]), 1/3
    #                           ) * 900
    # #Thob et al. 2019 eqn 4.
    # inside_mask = ellipse_distance <= ellipse_radius
    # return(pos[inside_mask], mass[inside_mask], ellipse_distance[inside_mask])

    return pos, mass, ellipse_distance


def find_abc(pos, mass, converge_tol2=0.0001, max_iter=100):
    """Finds the major, intermediate and minor axes.
    Follows Thob et al. 2019 using quadrupole moment of mass to bias towards
    particles closer to the centre
    """
    # no clue why this isn't working
    # try except
    try:
        # start off speherical
        r2 = np.square(np.linalg.norm(pos, axis=1))

        # stop problems with r=0
        pos = pos[r2 != 0]
        mass = mass[r2 != 0]
        r2 = r2[r2 != 0]

        # mass tensor of particles
        m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, r2)

        # linear algebra stuff
        (eigan_values, eigan_vectors) = process_tensor(m)

        # to see convergeance
        cona = np.sqrt(eigan_values[2] / eigan_values[0])
        bona = np.sqrt(eigan_values[1] / eigan_values[0])

        # done = False
        for i in range(max_iter):

            # redefine particles, calculate ellipse distance
            (pos, mass, ellipse_r2) = defined_particles(pos, mass, eigan_values, eigan_vectors)

            if len(mass) == 0:
                print('Warning: No particles left when finding shape')
                return (np.zeros(3, dtype=np.float64))

            # mass tensor of new particles
            m = reduced_quadrupole_moments_of_mass_tensor(pos, mass, ellipse_r2)

            # linear algebra stuff
            (eigan_values, eigan_vectors) = process_tensor(m)

            if (1 - eigan_values[2] / eigan_values[0] / cona < converge_tol2) and (
                    1 - eigan_values[1] / eigan_values[0] / bona < converge_tol2):
                # converged
                # done = True
                break

            else:
                cona = np.sqrt(eigan_values[2] / eigan_values[0])
                bona = np.sqrt(eigan_values[1] / eigan_values[0])

        # some warnings
        # if not done:
        #   print('Warning: Shape did not converge.')

        # if len(mass) < 100:
        #   print('Warning: Defining shape with <100 particles.')

    except ValueError:
        return (np.zeros(3, dtype=np.float64))

    return np.sqrt(eigan_values)


def my_binned_statistic(x, value, bins, statistic='sum'):
    """Calls binned statistic, but returns zeros when x and value are empty.
    """
    if len(x) == 0 and len(value) == 0:
        out = np.zeros(len(bins) - 1)
    else:
        out = binned_statistic(x, value, bins=bins, statistic=statistic)[0]

    return out


def my_already_binned_statistic(value, digitized, bin_edges, statistic='sum'):
    """Calculates the statistic in bins if binned_statistic has already digitized
    the data set.
    Should be faster than calling binned stastic a bunch of times ......
    """
    n = len(bin_edges) - 1
    out_array = np.zeros(n, dtype=np.float64)

    if statistic == 'sum':
        func = np.sum
    elif statistic == 'median':
        func = np.median
    else:
        raise ValueError('Statistic ' + str(statistic) + ' not understood.')

    # would be faster without a for loop
    for i in range(n):
        out_array[i] = func(value[digitized == i + 1])

    return out_array


def get_kinematic_profiles(bin_edges, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a):
    """Given galaxy particle data and bins, calculates kenematic profiles.
    See Genel et al. 2015, Lagos et al. 2017, Correra et al. 2017, Wilkinson et al. 2021
    Probably not super efficent to call binned statistic a bunch of times, can use
    the third output to mask bins. Too lazy / will be more confusing to code though.
    """
    # binned
    (bin_mass, _, bin_number) = binned_statistic(R, mass, bins=bin_edges, statistic='sum')
    bin_N = np.histogram(R, bins=bin_edges)[0]

    number_of_bins = len(bin_edges) - 1

    # j_zonc
    # I don't like this. Needs to be done though
    fjzjc = np.zeros((number_of_bins, 21), dtype=np.float64)

    # calculate kappas
    K_tot = np.zeros(number_of_bins, dtype=np.float64)
    K_rot = np.zeros(number_of_bins, dtype=np.float64)
    K_co = np.zeros(number_of_bins, dtype=np.float64)

    # sigmas
    mean_v_R = np.zeros(number_of_bins, dtype=np.float64)
    mean_v_phi = np.zeros(number_of_bins, dtype=np.float64)
    median_v_phi = np.zeros(number_of_bins, dtype=np.float64)

    sigma_z = np.zeros(number_of_bins, dtype=np.float64)
    sigma_R = np.zeros(number_of_bins, dtype=np.float64)
    sigma_phi = np.zeros(number_of_bins, dtype=np.float64)

    z_half = np.zeros(number_of_bins, dtype=np.float64)

    form_a_median = np.zeros(number_of_bins, dtype=np.float64)

    a_axis = np.zeros(number_of_bins, dtype=np.float64)
    b_axis = np.zeros(number_of_bins, dtype=np.float64)
    c_axis = np.zeros(number_of_bins, dtype=np.float64)

    # would be faster without a for loop
    for i in range(number_of_bins):
        mask = bin_number == i + 1

        bin_massi = 1 / bin_mass[i]

        mmass = mass[mask]
        mj_zonc = j_zonc[mask]
        mv_phi = v_phi[mask]

        # j_zonc
        for j, X in enumerate(np.linspace(-1, 1, 21)):
            fjzjc[i, j] = np.sum(mmass[mj_zonc >= X]) * bin_massi

        # calculate kappas
        K_tot[i] = np.sum(mmass * np.square(np.linalg.norm(vel[mask], axis=1)))
        K_rot[i] = np.sum(mmass * np.square(mv_phi))

        co = (mv_phi > 0)
        K_co[i] = np.sum(mmass[co] * np.square(mv_phi[co]))

        # sigmas
        mean_v_R[i] = np.sum(mmass * v_R[mask]) * bin_massi
        mean_v_phi[i] = np.sum(mmass * mv_phi) * bin_massi
        median_v_phi[i] = np.median(mv_phi)

        sigma_z[i] = np.sum(mmass * np.square(v_z[mask])) * bin_massi
        sigma_R[i] = np.sum(mmass * np.square(v_R[mask])) * bin_massi
        sigma_phi[i] = np.sum(mmass * np.square(mv_phi - mean_v_phi[i])) * bin_massi

        # TODO replace with mass weighted median
        z_half[i] = np.median(np.abs(z[mask]))

        # TODO replace with mass weighted median
        # TODO add initial mass weighted median
        # probably doesn't need to be a profile
        form_a_median[i] = np.median(form_a[mask])

        if bin_N[i] > 10:
            try:
                a_axis[i], b_axis[i], c_axis[i] = find_abc(pos[mask], mmass)
            except Exception as e:
                a_axis[i], b_axis[i], c_axis[i] = 0., 0., 0.
                pass
                # print(e)

    kappa_rot = K_rot / K_tot
    kappa_co = K_co / K_tot

    return (bin_mass, bin_N,
            fjzjc,
            kappa_rot, kappa_co,
            mean_v_R, mean_v_phi, median_v_phi,
            sigma_z, sigma_R, sigma_phi,
            z_half, form_a_median,
            a_axis, b_axis, c_axis)


def get_kinematic_apature(apature, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a):
    """Same as get_kinematic_profile, except apature rather than profile.
    """
    # mask
    mask = (R < apature)

    pos = pos[mask]
    vel = vel[mask]
    mass = mass[mask]
    (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])
    j_zonc = j_zonc[mask]
    form_a = form_a[mask]

    bin_mass = np.sum(mass)
    bin_N = np.size(mass)

    bin_massi = 1 / bin_mass

    # jz/jc
    fjzjc = np.zeros(21, dtype=np.float64)
    for j, X in enumerate(np.linspace(-1, 1, 21)):
        fjzjc[j] = np.sum(mass[j_zonc >= X]) * bin_massi

    # calculate kappas
    K_tot = np.sum(mass * np.square(np.linalg.norm(vel, axis=1)))
    K_rot = np.sum(mass * np.square(v_phi))
    K_co = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

    kappa_rot = K_rot / K_tot
    kappa_co = K_co / K_tot

    # sigmas
    mean_v_R = np.sum(mass * v_R) * bin_massi
    mean_v_phi = np.sum(mass * v_phi) * bin_massi
    median_v_phi = np.median(v_phi)

    sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
    sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
    sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

    z_half = np.median(np.abs(z))

    form_a_median = np.median(form_a)

    a_axis, b_axis, c_axis = find_abc(pos, mass)

    return (bin_mass, bin_N,
            fjzjc,
            kappa_rot, kappa_co,
            mean_v_R, mean_v_phi, median_v_phi,
            sigma_z, sigma_R, sigma_phi,
            z_half, form_a_median,
            a_axis, b_axis, c_axis)


def get_kinematic_annuli(apature, pos, vel, mass, R, z, v_R, v_phi, v_z, j_zonc, form_a):
    """Same as get_kinematic_profile, except apature rather than profile.
    """
    # mask
    dex = 0.2
    mask = np.logical_and(10 ** (-dex / 2) < R / apature, R / apature < 10 ** (dex / 2))

    pos = pos[mask]
    vel = vel[mask]
    mass = mass[mask]
    (R, z, v_z, v_R, v_phi) = (R[mask], z[mask], v_z[mask], v_R[mask], v_phi[mask])
    j_zonc = j_zonc[mask]
    form_a = form_a[mask]

    bin_mass = np.sum(mass)
    bin_N = np.size(mass)

    bin_massi = 1 / bin_mass

    # jz/jc
    fjzjc = np.zeros(21, dtype=np.float64)
    for j, X in enumerate(np.linspace(-1, 1, 21)):
        fjzjc[j] = np.sum(mass[j_zonc >= X]) * bin_massi

    # calculate kappas
    K_tot = np.sum(mass * np.square(np.linalg.norm(vel, axis=1)))
    K_rot = np.sum(mass * np.square(v_phi))
    K_co = np.sum(mass[v_phi > 0] * np.square(v_phi[v_phi > 0]))

    kappa_rot = K_rot / K_tot
    kappa_co = K_co / K_tot

    # sigmas
    mean_v_R = np.sum(mass * v_R) * bin_massi
    mean_v_phi = np.sum(mass * v_phi) * bin_massi
    median_v_phi = np.median(v_phi)

    sigma_z = np.sqrt(np.sum(mass * np.square(v_z)) * bin_massi)
    sigma_R = np.sqrt(np.sum(mass * np.square(v_R)) * bin_massi)
    sigma_phi = np.sqrt(np.sum(mass * np.square(v_phi - mean_v_phi)) * bin_massi)

    z_half = np.median(np.abs(z))

    form_a_median = np.median(form_a)

    a_axis, b_axis, c_axis = find_abc(pos, mass)

    return (bin_mass, bin_N,
            fjzjc,
            kappa_rot, kappa_co,
            mean_v_R, mean_v_phi, median_v_phi,
            sigma_z, sigma_R, sigma_phi,
            z_half, form_a_median,
            a_axis, b_axis, c_axis)


def read_calculate_write(args):
    # time
    start_time = time.time()

    raw_data = read(*args)
    print(str(np.round(time.time() - start_time, 1)))

    processed_data = calculate(*raw_data)
    print(str(np.round(time.time() - start_time, 1)))

    return


if __name__ == '__main__':
    _, snap_index = sys.argv

    snap_index = int(snap_index)

    # sim = 'L0050/REFERENCE'
    sim = 'L0050/REF_7x752dm'

    # ozstar 7xdm
    particle_data_location = '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/'
    halo_data_location = '/fred/oz009/mwilkinson/EAGLE/L0050N0752/REF_7x752dm/data/'
    output_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/'

    # # ozstar fiducial 50mpc
    # particle_data_location = '/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/'
    # halo_data_location = '/fred/oz009/clagos/EAGLE/L0050N0752/PE/REFERENCE/data/'
    # output_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/'

    # # hyades 7xdm
    # particle_data_location = '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/'
    # halo_data_location = '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/'
    # output_data_location = '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm'

    kdtree_location = output_data_location

    name = '50Mpc_0752'

    # snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
    #              '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
    #              '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
    #              '016_z001p737', '015_z002p012']
    snap_list = ['028_z000p000', '023_z000p503', '019_z001p004', '015_z002p012']
    # for snap in snap_list:
    snap = snap_list[snap_index]

    SCALE_A_list = [1, 1 / 1.503, 1 / 1.004, 1 / 2.012]
    SCALE_A = SCALE_A_list[snap_index]

    #TODO change medians to mass weighted median!

    # do the thing
    read_calculate_write((particle_data_location, halo_data_location, output_data_location, kdtree_location, snap))

    pass
