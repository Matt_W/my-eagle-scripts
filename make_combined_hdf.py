#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 15:36:38 2021

@author: matt
"""

import numpy as np

import h5py as h5

snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
             '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
             '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
             '016_z001p737', '015_z002p012']

def get_data():
  '''Load and cut data in the specified way...
  Needs like 40 Gb...
  '''

  #TODO staro formation from z0 is currently centrals only. Should be for all galaxies

  #get all data in same top leaf order as z0 star formation
  z0_star_formation_name = '/fred/oz009/mwilkinson/EAGLE/processed_data/starformation_from_z0.hdf5'

  #1.7Mb
  with h5.File(z0_star_formation_name,'r') as z0_star_formation_data:

    formed_top_leaf = z0_star_formation_data['/GalaxyData/TopLeafID'][()]

    # log_mass     = z0_star_formation_data['/GalaxyData/LogStarMass'][()]
    # star_mass_30 = z0_star_formation_data['/GalaxyData/StarMassz0r30'][()]

    formed_95 = z0_star_formation_data['/GalaxyData/z95'][()]
    formed_90 = z0_star_formation_data['/GalaxyData/z90'][()]
    formed_85 = z0_star_formation_data['/GalaxyData/z85'][()]
    formed_80 = z0_star_formation_data['/GalaxyData/z80'][()]
    formed_75 = z0_star_formation_data['/GalaxyData/z75'][()]
    formed_50 = z0_star_formation_data['/GalaxyData/z50'][()]

    formed_pct27 = z0_star_formation_data['/GalaxyData/pct27'][()]
    formed_pct26 = z0_star_formation_data['/GalaxyData/pct26'][()]
    formed_pct25 = z0_star_formation_data['/GalaxyData/pct25'][()]
    formed_pct24 = z0_star_formation_data['/GalaxyData/pct24'][()]
    formed_pct23 = z0_star_formation_data['/GalaxyData/pct23'][()]
    formed_pct22 = z0_star_formation_data['/GalaxyData/pct22'][()]
    formed_pct21 = z0_star_formation_data['/GalaxyData/pct21'][()]
    formed_pct20 = z0_star_formation_data['/GalaxyData/pct20'][()]
    formed_pct19 = z0_star_formation_data['/GalaxyData/pct19'][()]
    formed_pct18 = z0_star_formation_data['/GalaxyData/pct18'][()]
    formed_pct17 = z0_star_formation_data['/GalaxyData/pct17'][()]
    formed_pct16 = z0_star_formation_data['/GalaxyData/pct16'][()]
    formed_pct15 = z0_star_formation_data['/GalaxyData/pct15'][()]

  formed_pct28 = np.ones(np.shape(formed_pct27))

  #set up large arrays
  n_halo_z0 = np.size(formed_top_leaf)
  print('N halos z=0:', n_halo_z0)
  n_dim   = 49
  n_snaps = 14

  #galaxy, then snap, then profile

  #galaxy quantities
  #ids
  # top_leaf_id
  galaxy_id   = np.zeros((n_halo_z0, n_snaps), dtype=np.int64)
  dec_id      = np.zeros((n_halo_z0, n_snaps), dtype=np.int64)
  prog_id     = np.zeros((n_halo_z0, n_snaps), dtype=np.int64)

  group_id    = np.zeros((n_halo_z0, n_snaps), dtype=np.int64)
  subgroup_id = np.zeros((n_halo_z0, n_snaps), dtype=np.int64)

  #distances
  r_half     = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  r_quarter  = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  r_3quarter = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)

  axis_a     = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  axis_b     = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  axis_c     = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)

  r200       = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  m200       = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)

  rvmax      = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  vmax       = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)

  #masses
  mass      = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  bh_mass   = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  dm_mass   = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  gas_mass  = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  star_mass = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)

  bh_sub_mass = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  bh_sub_acc  = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)

  # cop
  pos_cop = np.zeros((n_halo_z0, n_snaps, 3), dtype=np.float64)

  #profiles
  #star
  bin_mass = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  bin_n    = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.int64)

  mean_v_R     = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  mean_v_phi   = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  median_v_phi = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)

  sigma_R   = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  sigma_phi = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  sigma_z   = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)

  kappa_co  = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  kappa_rot = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)

  z_half = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)

  median_form_a = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)

  fjzjc00 = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  fjzjc   = np.zeros((n_halo_z0, n_snaps, n_dim, 21), dtype=np.float64)

  #DM
  dm_bin_n = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.int64)

  dm_sigma_x = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  dm_sigma_y = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)
  dm_sigma_z = np.zeros((n_halo_z0, n_snaps, n_dim), dtype=np.float64)

  formation_percentages = np.zeros((n_halo_z0, n_snaps), dtype=np.float64)
  formation_percentages[:, 0]  = formed_pct28
  formation_percentages[:, 1]  = formed_pct27
  formation_percentages[:, 2]  = formed_pct26
  formation_percentages[:, 3]  = formed_pct25
  formation_percentages[:, 4]  = formed_pct24
  formation_percentages[:, 5]  = formed_pct23
  formation_percentages[:, 6]  = formed_pct22
  formation_percentages[:, 7]  = formed_pct21
  formation_percentages[:, 8]  = formed_pct20
  formation_percentages[:, 9]  = formed_pct19
  formation_percentages[:, 10] = formed_pct18
  formation_percentages[:, 11] = formed_pct17
  formation_percentages[:, 12] = formed_pct16
  formation_percentages[:, 13] = formed_pct15

  for snap_i in range(14):
    print('Started loading', snap_list[snap_i])
    star_profile_name = '/fred/oz009/mwilkinson/EAGLE/processed_data/' + snap_list[snap_i] + '_galaxy_kinematic_profile.hdf5'
    dm_profile_name   = '/fred/oz009/mwilkinson/EAGLE/processed_data/' + snap_list[snap_i] + '_galaxy_DM_profile.hdf5'
    halo_data_name    = '/fred/oz009/mwilkinson/EAGLE/processed_data/' + snap_list[snap_i] + '_100Mpc_1504_halodat.hdf5'

    #max 2.2Gb
    with h5.File(star_profile_name,'r') as star_profile_data:
      #ids
      star_top_leaf  = star_profile_data['/GalaxyQuantities/TopLeafID'][()]
      # star_galaxy_id = star_profile_data['/GalaxyQuantities/GalaxyID'][()]
      # star_group     = star_profile_data['/GalaxyQuantities/GroupNUmebr'][()] #spelling mistake nooo
      # star_gubgroup  = star_profile_data['/GalaxyQuantities/SubGroupNumber'][()]

      #quantities
      star_axis_a = star_profile_data['/GalaxyQuantities/Axisa'][()]
      star_axis_b = star_profile_data['/GalaxyQuantities/Axisb'][()]
      star_axis_c = star_profile_data['/GalaxyQuantities/Axisc'][()]

      star_r_3quart = star_profile_data['/GalaxyQuantities/Stellar3QuaterMassRaduis'][()]
      star_r_half   = star_profile_data['/GalaxyQuantities/StellarHalfMassRadius'][()]
      star_r_quart  = star_profile_data['/GalaxyQuantities/StellarQuaterMassRaduis'][()]

      #profiles
      star_bin_mass = star_profile_data['/GalaxyProfiles/BinMass'][()]
      star_bin_n    = star_profile_data['/GalaxyProfiles/BinN'][()]

      star_mean_v_R   = star_profile_data['/GalaxyProfiles/MeanVR'][()]
      star_mean_v_phi = star_profile_data['/GalaxyProfiles/MeanVphi'][()]

      star_sigma_R   = star_profile_data['/GalaxyProfiles/sigmaR'][()]
      star_sigma_phi = star_profile_data['/GalaxyProfiles/sigmaphi'][()]
      star_sigma_z   = star_profile_data['/GalaxyProfiles/sigmaz'][()]

      star_kappa_co  = star_profile_data['/GalaxyProfiles/kappaCo'][()]
      star_kappa_rot = star_profile_data['/GalaxyProfiles/kappaRot'][()]

      star_z_half = star_profile_data['/GalaxyProfiles/zHalf'][()]

      star_median_form_a = star_profile_data['/GalaxyProfiles/MedianFormationa'][()]

      star_fjzjc00 = star_profile_data['/GalaxyProfiles/fjzjc00'][()]
      star_fjzjc01 = star_profile_data['/GalaxyProfiles/fjzjc01'][()]
      star_fjzjc02 = star_profile_data['/GalaxyProfiles/fjzjc02'][()]
      star_fjzjc03 = star_profile_data['/GalaxyProfiles/fjzjc03'][()]
      star_fjzjc04 = star_profile_data['/GalaxyProfiles/fjzjc04'][()]
      star_fjzjc05 = star_profile_data['/GalaxyProfiles/fjzjc05'][()]
      star_fjzjc06 = star_profile_data['/GalaxyProfiles/fjzjc06'][()]
      star_fjzjc07 = star_profile_data['/GalaxyProfiles/fjzjc07'][()]
      star_fjzjc08 = star_profile_data['/GalaxyProfiles/fjzjc08'][()]
      star_fjzjc09 = star_profile_data['/GalaxyProfiles/fjzjc09'][()]
      star_fjzjc10 = star_profile_data['/GalaxyProfiles/fjzjc10'][()]
      star_fjzjcm0 = star_profile_data['/GalaxyProfiles/fjzjcm0'][()]
      star_fjzjcm1 = star_profile_data['/GalaxyProfiles/fjzjcm1'][()]
      star_fjzjcm2 = star_profile_data['/GalaxyProfiles/fjzjcm2'][()]
      star_fjzjcm3 = star_profile_data['/GalaxyProfiles/fjzjcm3'][()]
      star_fjzjcm4 = star_profile_data['/GalaxyProfiles/fjzjcm4'][()]
      star_fjzjcm5 = star_profile_data['/GalaxyProfiles/fjzjcm5'][()]
      star_fjzjcm6 = star_profile_data['/GalaxyProfiles/fjzjcm6'][()]
      star_fjzjcm7 = star_profile_data['/GalaxyProfiles/fjzjcm7'][()]
      star_fjzjcm8 = star_profile_data['/GalaxyProfiles/fjzjcm8'][()]
      star_fjzjcm9 = star_profile_data['/GalaxyProfiles/fjzjcm9'][()]

    #max 300Mb
    with h5.File(dm_profile_name,'r') as dm_profile_data:
      #ids
      dm_top_leaf  = dm_profile_data['/GalaxyQuantities/TopLeafID'][()]
      # dm_galaxy_id = dm_profile_data['/GalaxyQuantities/GalaxyID'][()]
      # dm_group     = dm_profile_data['/GalaxyQuantities/GroupNumber'][()]
      # dm_subgroup  = dm_profile_data['/GalaxyQuantities/SubGroupNumber'][()]

      #profiles
      dm_file_bin_n = dm_profile_data['/GalaxyProfiles/DMN'][()]

      dm_file_sigma_x = dm_profile_data['/GalaxyProfiles/DMsigmaz'][()]
      dm_file_sigma_y = dm_profile_data['/GalaxyProfiles/DMsigmax'][()]
      dm_file_sigma_z = dm_profile_data['/GalaxyProfiles/DMsigmaz'][()]

    #max 30Mb
    with h5.File(halo_data_name,'r') as halo_data:
      #ids
      halo_top_leaf  = halo_data['/HaloData/TopLeafID'][()]
      halo_galaxy_id = halo_data['/HaloData/GalaxyID'][()]
      halo_group     = halo_data['/HaloData/GroupNumber'][()]
      halo_subgroup  = halo_data['/HaloData/SubGroupNumber'][()]

      halo_dec_id    = halo_data['/HaloData/DescendantID'][()]
      halo_pog_id    = halo_data['/HaloData/LastProgID'][()]

      #quantities
      halo_pos_cop   = halo_data['/HaloData/Pos_cop'][()]

      halo_mass      = halo_data['/HaloData/Mass'][()]
      halo_bh_mass   = halo_data['/HaloData/MassType_BH'][()]
      halo_dm_mass   = halo_data['/HaloData/MassType_DM'][()]
      halo_gas_mass  = halo_data['/HaloData/MassType_Gas'][()]
      halo_star_mass = halo_data['/HaloData/MassType_Star'][()]

      halo_sub_bh_mass = halo_data['/HaloData/BlackHoleMass'][()]
      halo_sub_bh_acc  = halo_data['/HaloData/BlackHoleMassAccretionRate'][()]

      halo_m200 = halo_data['/HaloData/Group_M_Crit200'][()]
      halo_r200 = halo_data['/HaloData/Group_R_Crit200'][()]

      halo_vmax  = halo_data['/HaloData/Vmax'][()]
      halo_rvmax = halo_data['/HaloData/Rmax'][()]

    #for loop because I'm too dumb
    #get all data in same top leaf order as z0 star formation
    for tl_index, tln in enumerate(formed_top_leaf):
      profile_index = np.nonzero( star_top_leaf == tln )[0]
      if len(profile_index) == 1:
        profile_index = profile_index[0]

        r_half[tl_index, snap_i]     = star_r_half[profile_index]
        r_quarter[tl_index, snap_i]  = star_r_quart[profile_index]
        r_3quarter[tl_index, snap_i] = star_r_3quart[profile_index]

        axis_a[tl_index, snap_i] = star_axis_a[profile_index]
        axis_b[tl_index, snap_i] = star_axis_b[profile_index]
        axis_c[tl_index, snap_i] = star_axis_c[profile_index]

        #profiles
        #star
        bin_mass[tl_index, snap_i] = star_bin_mass[profile_index]
        bin_n[tl_index, snap_i]    = star_bin_n[profile_index]

        mean_v_R[tl_index, snap_i]     = star_mean_v_R[profile_index]
        mean_v_phi[tl_index, snap_i]   = star_mean_v_phi[profile_index]
        median_v_phi[tl_index, snap_i] = star_mean_v_phi[profile_index]

        sigma_R[tl_index, snap_i]   = star_sigma_R[profile_index]
        sigma_phi[tl_index, snap_i] = star_sigma_phi[profile_index]
        sigma_z[tl_index, snap_i]   = star_sigma_z[profile_index]

        kappa_co[tl_index, snap_i]  = star_kappa_co[profile_index]
        kappa_rot[tl_index, snap_i] = star_kappa_rot[profile_index]

        z_half[tl_index, snap_i]  = star_z_half[profile_index]

        median_form_a[tl_index, snap_i] = star_median_form_a[profile_index]

        fjzjc00[tl_index, snap_i] = star_fjzjc00[profile_index]

        fjzjc[tl_index, snap_i, :, 0]  = star_fjzjc10[profile_index]
        fjzjc[tl_index, snap_i, :, 1]  = star_fjzjc09[profile_index]
        fjzjc[tl_index, snap_i, :, 2]  = star_fjzjc08[profile_index]
        fjzjc[tl_index, snap_i, :, 3]  = star_fjzjc07[profile_index]
        fjzjc[tl_index, snap_i, :, 4]  = star_fjzjc06[profile_index]
        fjzjc[tl_index, snap_i, :, 5]  = star_fjzjc05[profile_index]
        fjzjc[tl_index, snap_i, :, 6]  = star_fjzjc04[profile_index]
        fjzjc[tl_index, snap_i, :, 7]  = star_fjzjc03[profile_index]
        fjzjc[tl_index, snap_i, :, 8]  = star_fjzjc02[profile_index]
        fjzjc[tl_index, snap_i, :, 9]  = star_fjzjc01[profile_index]
        fjzjc[tl_index, snap_i, :, 10] = star_fjzjc00[profile_index]
        fjzjc[tl_index, snap_i, :, 11] = star_fjzjcm1[profile_index]
        fjzjc[tl_index, snap_i, :, 12] = star_fjzjcm2[profile_index]
        fjzjc[tl_index, snap_i, :, 13] = star_fjzjcm3[profile_index]
        fjzjc[tl_index, snap_i, :, 14] = star_fjzjcm4[profile_index]
        fjzjc[tl_index, snap_i, :, 15] = star_fjzjcm5[profile_index]
        fjzjc[tl_index, snap_i, :, 16] = star_fjzjcm6[profile_index]
        fjzjc[tl_index, snap_i, :, 17] = star_fjzjcm7[profile_index]
        fjzjc[tl_index, snap_i, :, 18] = star_fjzjcm8[profile_index]
        fjzjc[tl_index, snap_i, :, 19] = star_fjzjcm9[profile_index]
        fjzjc[tl_index, snap_i, :, 20] = star_fjzjcm0[profile_index]


      elif len(profile_index) > 1 and tln != 0:
        print('Warning: Top leaf id', tln, 'had multiple star profiles.')

    for tl_index, tln in enumerate(formed_top_leaf):
      profile_index = np.nonzero( dm_top_leaf == tln )[0]
      if len(profile_index) == 1:
        profile_index = profile_index[0]

        dm_bin_n[tl_index, snap_i] = dm_file_bin_n[profile_index]

        dm_sigma_x[tl_index, snap_i] = dm_file_sigma_x[profile_index]
        dm_sigma_y[tl_index, snap_i] = dm_file_sigma_y[profile_index]
        dm_sigma_z[tl_index, snap_i] = dm_file_sigma_z[profile_index]

      elif len(profile_index) > 1 and tln != 0:
        print('Warning: Top leaf id', tln, 'had multiple dm profiles.')

      #halo
      halo_index = np.nonzero( halo_top_leaf == tln )[0]
      if len(halo_index) == 1:
        halo_index = halo_index[0]

        galaxy_id[tl_index, snap_i]   = halo_galaxy_id[halo_index]
        dec_id[tl_index, snap_i]      = halo_dec_id[halo_index]
        prog_id[tl_index, snap_i]     = halo_pog_id[halo_index]

        group_id[tl_index, snap_i]    = halo_group[halo_index]
        subgroup_id[tl_index, snap_i] = halo_subgroup[halo_index]

        r200[tl_index, snap_i] = halo_r200[halo_index]
        m200[tl_index, snap_i] = halo_m200[halo_index]

        rvmax[tl_index, snap_i] = halo_rvmax[halo_index]
        vmax[tl_index, snap_i]  = halo_vmax[halo_index]

        #masses
        mass[tl_index, snap_i]      = halo_mass[halo_index]
        bh_mass[tl_index, snap_i]   = halo_bh_mass[halo_index]
        dm_mass[tl_index, snap_i]   = halo_dm_mass[halo_index]
        gas_mass[tl_index, snap_i]  = halo_gas_mass[halo_index]
        star_mass[tl_index, snap_i] = halo_star_mass[halo_index]

        bh_sub_mass[tl_index, snap_i] = halo_sub_bh_mass[halo_index]
        bh_sub_acc[tl_index, snap_i]  = halo_sub_bh_acc[halo_index]

        # cop
        pos_cop[tl_index, snap_i] = halo_pos_cop[halo_index]

      elif len(halo_index) > 1 and tln != 0:
        print('Warning: Top leaf id', tln, 'had multiple halos.')

  print('Profile data loaded for all snapshots.')

  #TODO there is way better ways of doing this. ..
  #TODO turn into a hdf5 file
  return( {'TopLeafID'    : formed_top_leaf,          #(n_halos)
           'GalaxyID'     : galaxy_id,                #(n_halos, n_snaps)
           'DescendantID' : dec_id,                   #(n_halos, n_snaps)
           'LastProgID'   : prog_id,                  #(n_halos, n_snaps)
           'GroupID'      : group_id,                 #(n_halos, n_snaps)
           'SubGroupID'   : subgroup_id,              #(n_halos, n_snaps)
           #distances
           'r_half'          : r_half,                #(n_halos, n_snaps)
           'r_quater'        : r_quarter,             #(n_halos, n_snaps)
           'r_3quater'       : r_3quarter,            #(n_halos, n_snaps)
           'Axisa'           : axis_a,                #(n_halos, n_snaps)
           'Axisb'           : axis_b,                #(n_halos, n_snaps)
           'Axisc'           : axis_c,                #(n_halos, n_snaps)
           'Group_R_Crit200' : r200,                  #(n_halos, n_snaps)
           'Group_M_Crit200' : m200,                  #(n_halos, n_snaps)
           'VmaxRadius'      : rvmax,                 #(n_halos, n_snaps)
           'Vmax'            : vmax,                  #(n_halos, n_snaps)
           #masses
           'Mass'          : mass,                    #(n_halos, n_snaps)
           'MassType_BH'   : bh_mass,                 #(n_halos, n_snaps)
           'MassType_DM'   : dm_mass,                 #(n_halos, n_snaps)
           'MassType_Gas'  : gas_mass,                #(n_halos, n_snaps)
           'MassType_Star' : star_mass,               #(n_halos, n_snaps)
           'BlackHoleMass' : bh_sub_mass,             #(n_halos, n_snaps)
           'BlackHoleMassAccretionRate' : bh_sub_acc, #(n_halos, n_snaps)
           # cop
           'CentreOfPotential' : pos_cop,             #(n_halos, n_snaps, 3)
           #profiles
           #star
           'BinMass' : bin_mass,                      #(n_halos, n_snaps, n_bins)
           'BinN'    : bin_n,                         #(n_halos, n_snaps, n_bins)
           #mean v
           'MeanVR'   : mean_v_R,                     #(n_halos, n_snaps, n_bins)
           'MeanVphi' : mean_v_phi,                   #(n_halos, n_snaps, n_bins)
           'MedianVphi' : mean_v_phi,                 #(n_halos, n_snaps, n_bins)
           #sigma
           'SigmaR'   : sigma_R,                      #(n_halos, n_snaps, n_bins)
           'Sigmaphi' : sigma_phi,                    #(n_halos, n_snaps, n_bins)
           'Sigmaz'   : sigma_z,                      #(n_halos, n_snaps, n_bins)
           #kappa
           'KappaCo'  : kappa_co,                     #(n_halos, n_snaps, n_bins)
           'KappaRot' : kappa_rot,                    #(n_halos, n_snaps, n_bins)
           #z
           'zHalf' : z_half,                          #(n_halos, n_snaps, n_bins)
           #a
           'MedianFormationa' : median_form_a,        #(n_halos, n_snaps, n_bins)
           # fjzjc
           'fjzjc00' : fjzjc00,                       #(n_halos, n_snaps, n_bins)
           'fjzjc'   : fjzjc,                         #(n_halos, n_snaps, n_bins, 21)
           #DM
           'DMN' : dm_bin_n,                          #(n_halos, n_snaps, n_bins)
           #dm sigma
           'DMSigmax' : dm_sigma_x,                   #(n_halos, n_snaps, n_bins)
           'DMSigmay' : dm_sigma_y,                   #(n_halos, n_snaps, n_bins)
           'DMSigmaz' : dm_sigma_z,                   #(n_halos, n_snaps, n_bins)
           #formation
           'pct_95' : formed_95,                      #(n_halos)
           'pct_90' : formed_90,                      #(n_halos)
           'pct_85' : formed_85,                      #(n_halos)
           'pct_80' : formed_80,                      #(n_halos)
           'pct_75' : formed_75,                      #(n_halos)
           'pct_50' : formed_50,                      #(n_halos)
           'Formation_pct': formation_percentages     #(n_halos, n_snaps)
           } )


def make_hdf(data):
  '''
  '''
  combined_name = '/fred/oz009/mwilkinson/EAGLE/processed_data/combined_profiles.hdf5'

  #save all quantities
  with h5.File(combined_name, 'w') as output:
    grp_ids = output.create_group('IDs')
    grp_gms = output.create_group('GalaxyMeasures')
    grp_cop = output.create_group('CoP')
    grp_pof = output.create_group('Profiles')
    grp_ext = output.create_group('Extras')

    grp_ids.create_dataset('TopLeafID',    data=data['TopLeafID'])
    grp_ids.create_dataset('GalaxyID',     data=data['GalaxyID'])
    grp_ids.create_dataset('DescendantID', data=data['DescendantID'])
    grp_ids.create_dataset('LastProgID',   data=data['LastProgID'])
    grp_ids.create_dataset('GroupID',      data=data['GroupID'])
    grp_ids.create_dataset('SubGroupID',   data=data['SubGroupID'])

    grp_gms.create_dataset('r_half',    data=data['r_half'])
    grp_gms.create_dataset('r_quater',  data=data['r_quater'])
    grp_gms.create_dataset('r_3quater', data=data['r_3quater'])
    grp_gms.create_dataset('Axisa',     data=data['Axisa'])
    grp_gms.create_dataset('Axisb',     data=data['Axisb'])
    grp_gms.create_dataset('Axisc',     data=data['Axisc'])
    grp_gms.create_dataset('Group_R_Crit200', data=data['Group_R_Crit200'])
    grp_gms.create_dataset('Group_M_Crit200', data=data['Group_M_Crit200'])
    grp_gms.create_dataset('VmaxRadius',      data=data['VmaxRadius'])
    grp_gms.create_dataset('Vmax',            data=data['Vmax'])
    grp_gms.create_dataset('Mass',            data=data['Mass'])
    grp_gms.create_dataset('MassType_BH',     data=data['MassType_BH'])
    grp_gms.create_dataset('MassType_DM',     data=data['MassType_DM'])
    grp_gms.create_dataset('MassType_Gas',    data=data['MassType_Gas'])
    grp_gms.create_dataset('MassType_Star',   data=data['MassType_Star'])
    grp_gms.create_dataset('BlackHoleMass',   data=data['BlackHoleMass'])
    grp_gms.create_dataset('BlackHoleMassAccretionRate', data=data['BlackHoleMassAccretionRate'])

    grp_cop.create_dataset('CentreOfPotential', data=data['CentreOfPotential'])

    grp_pof.create_dataset('BinMass',  data=data['BinMass'])
    grp_pof.create_dataset('BinN',     data=data['BinN'])

    grp_pof.create_dataset('MeanVR',     data=data['MeanVR'])
    grp_pof.create_dataset('MeanVphi',   data=data['MeanVphi'])
    grp_pof.create_dataset('MedianVphi', data=data['MedianVphi'])

    grp_pof.create_dataset('SigmaR',   data=data['SigmaR'])
    grp_pof.create_dataset('Sigmaphi', data=data['Sigmaphi'])
    grp_pof.create_dataset('Sigmaz',   data=data['Sigmaz'])

    grp_pof.create_dataset('KappaCo',  data=data['KappaCo'])
    grp_pof.create_dataset('KappaRot', data=data['KappaRot'])

    grp_pof.create_dataset('zHalf',    data=data['zHalf'])

    grp_pof.create_dataset('MedianFormationa', data=data['MedianFormationa'])

    grp_pof.create_dataset('fjzjc00',  data=data['fjzjc00'])

    grp_pof.create_dataset('fjzjc',    data=data['fjzjc'])

    grp_pof.create_dataset('DMN',      data=data['DMN'])
    grp_pof.create_dataset('DMSigmax', data=data['DMSigmax'])
    grp_pof.create_dataset('DMSigmay', data=data['DMSigmay'])
    grp_pof.create_dataset('DMSigmaz', data=data['DMSigmaz'])

    grp_ext.create_dataset('aFormed95pct',  data=data['pct_95'])
    grp_ext.create_dataset('aFormed90pct',  data=data['pct_90'])
    grp_ext.create_dataset('aFormed85pct',  data=data['pct_85'])
    grp_ext.create_dataset('aFormed80pct',  data=data['pct_80'])
    grp_ext.create_dataset('aFormed75pct',  data=data['pct_75'])
    grp_ext.create_dataset('aFormed50pct',  data=data['pct_50'])
    grp_ext.create_dataset('Formation_pct', data=data['Formation_pct'])

  return

if __name__ == '__main__':

  data = get_data()
  print('Writing hdf5')
  make_hdf(data)
  print('Done')

  pass