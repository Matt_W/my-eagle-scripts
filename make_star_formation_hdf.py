#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 09:41:28 2021

@author: matt
"""

#import os
import numpy as np

import h5py  as h5

import pickle

import time

LITTLE_H = 0.6777
#box size should be saved and read in
BOX_SIZE = 147.55792
SCALE_A  = 1

def read_calculate_write(particle_data_location, galaxy_data_location, tree_data_location,
                         output_data_location, name, apature=30):
  '''Needs to do everything because of memory / speed things.
  '''
  #read z=0 halos
  file_name = '%s%s_%s_halodat.hdf5'%(galaxy_data_location, '028_z000p000', name)
  with h5.File(file_name,'r') as z0_data:

    #get ids
    # z0_group_numbers      = z0_data['HaloData/GroupNumber'][()]
    # z0_subgroup_numbers   = z0_data['HaloData/SubGroupNumber'][()]
    z0_top_leaf_numbers   = z0_data['HaloData/TopLeafID'][()]
    centre_of_pot_numbers = z0_data['HaloData/Pos_cop'][()]

  n_galaxies = len(z0_top_leaf_numbers)

  #set up arrays to save values

  star_mass_tot     = np.zeros(n_galaxies, dtype=np.float64)
  log_star_mass_tot = np.zeros(n_galaxies, dtype=np.float64)

  z95 = np.zeros(n_galaxies, dtype=np.float64)
  z90 = np.zeros(n_galaxies, dtype=np.float64)
  z85 = np.zeros(n_galaxies, dtype=np.float64)
  z80 = np.zeros(n_galaxies, dtype=np.float64)
  z75 = np.zeros(n_galaxies, dtype=np.float64)
  z50 = np.zeros(n_galaxies, dtype=np.float64)

  pct27 = np.zeros(n_galaxies, dtype=np.float64)
  pct26 = np.zeros(n_galaxies, dtype=np.float64)
  pct25 = np.zeros(n_galaxies, dtype=np.float64)
  pct24 = np.zeros(n_galaxies, dtype=np.float64)
  pct23 = np.zeros(n_galaxies, dtype=np.float64)
  pct22 = np.zeros(n_galaxies, dtype=np.float64)
  pct21 = np.zeros(n_galaxies, dtype=np.float64)
  pct20 = np.zeros(n_galaxies, dtype=np.float64)
  pct19 = np.zeros(n_galaxies, dtype=np.float64)
  pct18 = np.zeros(n_galaxies, dtype=np.float64)
  pct17 = np.zeros(n_galaxies, dtype=np.float64)
  pct16 = np.zeros(n_galaxies, dtype=np.float64)
  pct15 = np.zeros(n_galaxies, dtype=np.float64)

  #tree stuff
  with open(tree_data_location,'rb') as f:
    kd_tree = pickle.load(f)

  #stars
  file_name = '%s%s_%s_Star.hdf5'%(particle_data_location, '028_z000p000', name)
  star_data = h5.File(file_name, 'r')

  # star_group    = star_data['PartData/GrpNum_Star'][()]
  # star_subgroup = star_data['PartData/SubNum_Star'][()]

  #load quantities
  # star_pos         = star_data['PartData/PosStar'][()]
  # star_vel         = star_data['PartData/VelStar'][()]
  star_mass        = star_data['PartData/MassStar'][()]
  star_formation_a = star_data['PartData/StellarFormationTime'][()]

  star_data.close()

  #snapshot redshifts
  # snap_z = np.array([0.0, 0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736,
  #                    0.865, 1.004, 1.259, 1.487, 1.737, 2.012])

  #'random' order
  for halo_i in range(n_galaxies):

    #use the tree
    star_index_mask = kd_tree.query_ball_point(x = centre_of_pot_numbers[halo_i] * SCALE_A / LITTLE_H,
                                               r = apature/1000) #in Mpc

    # pos    = star_pos[star_index_mask]
    # vel    = star_vel[star_index_mask]
    mass   = star_mass[star_index_mask]
    form_a = star_formation_a[star_index_mask]

    # pos = pos - centre_of_potential - BOX_SIZE/2
    # pos %= BOX_SIZE
    # pos -= BOX_SIZE/2
    # pos *= 1000 * SCALE_A / LITTLE_H #to kpc

    # vel  *= np.sqrt(SCALE_A) #km/s
    mass /= LITTLE_H #10^10 M_sun
    # vel = vel - np.sum(mass[:, np.newaxis] * vel, axis=0) / np.sum(mass)

    form_z = 1 / form_a - 1

    arg_order  = np.argsort(form_z)
    cum_z      = form_z[arg_order]
    mass_order = mass[arg_order]
    cum_mass   = np.flip(np.cumsum(np.flip(mass_order)))

    M = cum_mass[0]
    star_mass_tot[halo_i]     = M
    log_star_mass_tot[halo_i] = np.log10(star_mass_tot[halo_i]) + 10

    z95[halo_i] = cum_z[cum_mass < 0.95 * M][0]
    z90[halo_i] = cum_z[cum_mass < 0.90 * M][0]
    z85[halo_i] = cum_z[cum_mass < 0.85 * M][0]
    z80[halo_i] = cum_z[cum_mass < 0.80 * M][0]
    z75[halo_i] = cum_z[cum_mass < 0.75 * M][0]
    z50[halo_i] = cum_z[cum_mass < 0.50 * M][0]

    try:
      pct27[halo_i] = cum_mass[cum_z > 0.101][0] / M
      pct26[halo_i] = cum_mass[cum_z > 0.183][0] / M
      pct25[halo_i] = cum_mass[cum_z > 0.271][0] / M
      pct24[halo_i] = cum_mass[cum_z > 0.366][0] / M
      pct23[halo_i] = cum_mass[cum_z > 0.503][0] / M
      pct22[halo_i] = cum_mass[cum_z > 0.615][0] / M
      pct21[halo_i] = cum_mass[cum_z > 0.736][0] / M
      pct20[halo_i] = cum_mass[cum_z > 0.865][0] / M
      pct19[halo_i] = cum_mass[cum_z > 1.004][0] / M
      pct18[halo_i] = cum_mass[cum_z > 1.259][0] / M
      pct17[halo_i] = cum_mass[cum_z > 1.487][0] / M
      pct16[halo_i] = cum_mass[cum_z > 1.737][0] / M
      pct15[halo_i] = cum_mass[cum_z > 2.012][0] / M

    except IndexError:
      print('IndexWarning: for', z0_top_leaf_numbers[halo_i], 'M=', M)

    if (halo_i % 1000) == 0:
      print('Done ', halo_i, 'of', n_galaxies)

  #save all quantities
  with h5.File(output_data_location + 'starformation_from_z0.hdf5', "w") as output:
    grp     = output.create_group("GalaxyData")

    grp.create_dataset('TopLeafID',     data = z0_top_leaf_numbers)
    grp.create_dataset('StarMassz0r30', data = star_mass_tot) #z=0 setallar mass r<30kpc
    grp.create_dataset('LogStarMass',   data = log_star_mass_tot) #z=0 setallar mass r<30kpc

    #redshift where x% of z=0, r<30kpc stars were formed
    grp.create_dataset('z95', data = z95)
    grp.create_dataset('z90', data = z90)
    grp.create_dataset('z85', data = z85)
    grp.create_dataset('z80', data = z80)
    grp.create_dataset('z75', data = z75)
    grp.create_dataset('z50', data = z50)

    #% of z=0, r<30kpc stars formed by snapshot number n
    grp.create_dataset('pct27', data = pct27)
    grp.create_dataset('pct26', data = pct26)
    grp.create_dataset('pct25', data = pct25)
    grp.create_dataset('pct24', data = pct24)
    grp.create_dataset('pct23', data = pct23)
    grp.create_dataset('pct22', data = pct22)
    grp.create_dataset('pct21', data = pct21)
    grp.create_dataset('pct20', data = pct20)
    grp.create_dataset('pct19', data = pct19)
    grp.create_dataset('pct18', data = pct18)
    grp.create_dataset('pct17', data = pct17)
    grp.create_dataset('pct16', data = pct16)
    grp.create_dataset('pct15', data = pct15)

  return

if __name__ == '__main__':

  start_time = time.time()

  sim = 'L0100N1504/REFERENCE'
  particle_data_location = '/fred/oz009/mwilkinson/EAGLE/processed_data/'
  galaxy_data_location   = '/fred/oz009/mwilkinson/EAGLE/processed_data/'
  tree_data_location     = '/fred/oz009/amanuwal/startree_z000p000.p'

  output_data_location   = '/fred/oz009/mwilkinson/EAGLE/processed_data/'

  name = '100Mpc_1504'

  read_calculate_write(particle_data_location, galaxy_data_location, tree_data_location,
                       output_data_location, name, apature=30)

  print('done in ' +  str(np.round(time.time() - start_time, 1)) + 's')
  pass