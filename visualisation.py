#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

import os
import sys
import time

import numpy as np

import h5py as h5

from scipy.ndimage import gaussian_filter

import matplotlib
matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 12#20
rcParams['xtick.labelsize'] = 12#20
rcParams['ytick.labelsize'] = 12#20
rcParams['axes.labelsize'] = 12#22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

GRAV_CONST = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
HUBBLE_CONST = 0.06777 #km/s/kpc
LITTLE_H = 0.6777
SCALE_A = 1
BOX_SIZE = 33.885 #50 * 0.6777 #kpc

UNIVERSE_AGE = 13.82968685 #Gyr

PC_ON_M = 3.0857e16  # pc/m = kpc/km (wiki)
GYR_ON_S = 3.15576e16  # gyr/s

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

Omega_b = 0.04825
Omega_m = 0.307

EAGLE_EPS = 0.7 # kpc


#TODO read/make data in multi processing way
def my_read(particle_data_location, snap):

    # from scipy.interpolate import interp1d
    #
    # #random noise for testing
    #
    # #for normalization calc
    # N_grid = 10
    #
    # NDM = 20**3
    # if '7x' in particle_data_location:
    #     NDM *= 7
    # _x = np.linspace(0,1, N_grid)
    # x,y,z = np.meshgrid(_x,_x,_x)
    #
    # nx = 200
    # np.random.seed(1)
    # wave_freq_x = np.random.rand(nx) * 80 #[13, 19, 27]
    # wave_phase_x = np.random.rand(nx) #[0, 0.5, 0.25]
    # wave_freq_y = np.random.rand(nx) * 80 #[11, 23, 29]
    # wave_phase_y = np.random.rand(nx) #[0.25, 0, 0.75]
    # wave_freq_z = np.random.rand(nx) * 80 #[11, 23, 29]
    # wave_phase_z = np.random.rand(nx) #[0.25, 0, 0.75]
    #
    # wave_weight_x = (np.random.rand(nx) * 3 + 1) / np.sqrt(wave_freq_x**2 + wave_freq_y**2 + wave_freq_z**2) #[-0.5, 0.5, 0.5]
    #
    # # f = lambda x,y: np.sum((wave_weight_x[ix] * np.sin((x + wave_phase_x[ix]) * wave_freq_x[ix])
    # #                          for ix in range(len(wave_freq_x)))) + np.sum((wave_weight_y[iy] *
    # #                                                                         np.sin((y + wave_phase_y[iy]) * wave_freq_y[iy])
    # #                          for iy in range(len(wave_freq_y))))
    #
    # f = lambda x,y,z: np.sum((wave_weight_x[ix] * np.sin((x + wave_phase_x[ix]) * wave_freq_x[ix] +
    #                                                      (y + wave_phase_y[ix]) * wave_freq_y[ix] +
    #                                                      (z + wave_phase_z[ix]) * wave_freq_z[ix])
    #                          for ix in range(len(wave_freq_x))))
    #
    # w = f(x,y,z)
    # flat_max = np.amax(w)
    # flat_min = np.amin(w)
    #
    # norm = 1
    #
    # f = lambda x,y,z: ((np.sum((wave_weight_x[ix] * np.sin((x + wave_phase_x[ix]) * wave_freq_x[ix] +
    #                                                        (y + wave_phase_y[ix]) * wave_freq_y[ix] +
    #                                                        (z + wave_phase_z[ix]) * wave_freq_z[ix])
    #                                 for ix in range(len(wave_freq_x)))) - flat_min) / (flat_max - flat_min))**(10) * norm
    #
    # z = f(x,y,z)
    # norm = np.nanmax(z) #np.sum(z)
    #
    # #x,y,z, prob
    # np.random.seed(NDM)
    # # rolls = np.random.rand(int(1.4 * NDM), 4)
    # rolls = np.random.rand(int(80 * NDM), 4)
    #
    # evaluation = f(rolls[:, 0], rolls[:, 1], rolls[:, 2])
    # accept = rolls[:, 3] * norm < evaluation
    #
    # # print(evaluation)
    # # print(norm)
    #
    # print('successes/number required', np.sum(accept) / NDM)
    # PosDM = rolls[accept, 0:3]
    # PosDM = PosDM[:NDM]
    #
    # PosDM *= BOX_SIZE
    #
    # return PosDM

    fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.0.hdf5'
    print('Particles:', fn, '...')

    with h5.File(fn, "r") as fs:

        RuntimePars = fs['RuntimePars'].attrs
        Header = fs['Header'].attrs

        FNumPerSnap = RuntimePars['NumFilesPerSnapshot']
        NumParts = Header['NumPart_ThisFile']
        NumPartTot = Header['NumPart_Total']

    # PosDM = np.zeros((NumPartTot[1], 3), dtype=np.float32)
    PosDM = np.zeros((NumPartTot[4], 3), dtype=np.float32)

    NDM_c = 0

    for ifile in range(FNumPerSnap):
        fn = particle_data_location + 'particledata_' + snap + '/eagle_subfind_particles_' + snap + '.' + str(
            ifile) + '.hdf5'

        with h5.File(fn, "r") as fs:
            Header = fs['Header'].attrs
            NumParts = Header['NumPart_ThisFile']

            # if NumParts[1] > 0:
            #     PosDM[NDM_c:NDM_c + NumParts[1], :] = fs["PartType1/Coordinates"][()]
            #     NDM_c += NumParts[1]

            if NumParts[4] > 0:
                PosDM[NDM_c:NDM_c + NumParts[4], :] = fs["PartType4/Coordinates"][()]
                NDM_c += NumParts[4]

    print('loaded particles')

    return PosDM

def make_visualisation(particle_data_location_0, snap='028_z000p000',
                       nx_edges= 752//2,
                       name='visualisation', save=False):

    PosDM_0 = my_read(particle_data_location_0, snap)

    #Mpc/h
    x_edges = np.linspace(0, BOX_SIZE, nx_edges)

    # if '7x' in particle_data_location_0:
    #     DM_hist_xy_0,_,_ = np.histogram2d(PosDM_0[:, 1], PosDM_0[:, 0], weights=1/7 * np.ones(len(PosDM_0[:, 0])),
    #                                       bins = [x_edges, x_edges])
    # else:
    DM_hist_xy_0,_,_ = np.histogram2d(PosDM_0[:, 1], PosDM_0[:, 0],
                                      bins = [x_edges, x_edges])

    print('2d histograms done.')

    # DM_hist_xy_0 = DM_hist_xy_0 / len(PosDM_1) **(1/3) * nx_edges**2 # 1 is proj cosmic matter dist

    DM_hist_xy_0 = DM_hist_xy_0 / 752 * nx_edges**2 # 1 is proj cosmic matter dist

    # # #blur
    # DM_hist_xy_0 = gaussian_filter(DM_hist_xy_0, 0.1)

    DM_hist_xy_0 = np.flip(DM_hist_xy_0, axis=0)

    print('max', np.amax(DM_hist_xy_0), np.log10(np.amax(DM_hist_xy_0)))
    print('min', np.amin(DM_hist_xy_0[DM_hist_xy_0 != 0]), np.log10(np.amin(DM_hist_xy_0[DM_hist_xy_0 != 0])))

    #stretch image
    log_hi_lim  = np.log10(1000)
    # log_low_lim = np.log10(0.2)
    log_low_lim = np.log10(100)

    stretch_lambda = lambda array: np.amax((np.log10(array + 1e-100) - log_low_lim,
                                            np.zeros(np.shape(array))), axis=0) / (log_hi_lim - log_low_lim)

    DM_hist_xy_0 = stretch_lambda(DM_hist_xy_0)

    print(np.amax(DM_hist_xy_0), np.amin(DM_hist_xy_0))
    print(np.mean(np.mean(DM_hist_xy_0, axis=0), axis=0))

    #
    print('Starting plot')

    fig, axs = plt.subplots(1, 1)
    fig.set_size_inches(6.6, 6.6, forward=True)

    axs.set_aspect(1)
    axs.set_xlabel(r'$x$ / Mpc h$^{-1}$')
    axs.set_ylabel(r'$y$ / Mpc h$^{-1}$')
    fig.subplots_adjust(hspace=0.00, wspace=0.00)

    plt.imshow(DM_hist_xy_0, extent=[0, BOX_SIZE, 0, BOX_SIZE], cmap='inferno')

    if snap == '028_z000p000':
        zstr = r'$z=0$'
    if '7x' in particle_data_location:
        res_lab = 'HR, ' + zstr
    else:
        res_lab = 'LR, ' + zstr

    plt.text(1, 1, res_lab,
             c='white', horizontalalignment='left', verticalalignment='bottom', fontsize=20)

    if save:
        print('Saving.')

        plt.savefig(name, bbox_inches='tight', dpi=200)
        plt.close()

    print('Done!')

    return


if __name__ == '__main__':
    _, snap_index = sys.argv
    snap_index = int(snap_index)

    # snap_index = 0

    #hyades
    particle_data_location = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
                              '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/'][snap_index]
    # halo_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/L0050N0752/REFERENCE/',
    #                            '/mnt/su3ctm/EAGLE/L0050N0752/REF_7x752dm/data/'][snap_index]
    # output_data_location_list = ['/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REFERENCE/',
    #                              '/mnt/su3ctm/mwilkinson/EAGLE/processed_data/L0050N752/REF_7x752dm/'][snap_index]
    output_data_location = ['/home/mwilkinson/EAGLE/visualisation_box_LR_star.pdf',
                            '/home/mwilkinson/EAGLE/visualisation_box_HR_star.pdf'][snap_index]

    # output_data_location = '../results/lss_test'

    make_visualisation(particle_data_location,
                       name=output_data_location, save=True)

    plt.show()
    pass