#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:02:04 2021

@author: matt
"""

# import os
import time

import numpy as np

import h5py as h5

# from scipy.interpolate import InterpolatedUnivariateSpline
# from scipy.interpolate import interp1d
# from scipy.interpolate import interpn

from scipy.integrate   import quad

# from scipy.optimize    import minimize
# from scipy.optimize    import brentq

# from scipy.signal      import savgol_filter

from scipy.stats       import binned_statistic

import matplotlib
matplotlib.use('Agg')

#matplotlib
import matplotlib.pyplot as plt
from matplotlib.cm     import Greys
from matplotlib.cm     import Purples
from matplotlib.cm     import Blues
from matplotlib.cm     import Greens
from matplotlib.cm     import Oranges
from matplotlib.cm     import Reds

# from matplotlib.colors import LogNorm
# from matplotlib.colors import ListedColormap
# from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               # AutoMinorLocator)
# from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = False
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

grav_const   = 4.302e4 #kpc (km/s)^2 / (10^10 Msun) (source ??)
LITTLE_H = 0.6777

DM_MASS = 6.570332889156362E-4 / 0.6777 #10^10 Msun

#to get star particle age
def lbt(z=[],a=[],omm=0.307,oml=0.693,h=0.6777):
  if a != []:
    z = 1.0/a-1
  t = np.zeros(len(z))
  for i in range(len(z)):
    t[i] = 1e+3*3.086e+16/(3.154e+7*1e+9)*(1.0/(100*h))*quad(lambda z: 1/((1+z)*np.sqrt(omm*(1+z)**3+oml)),0,z[i])[0]#in billion years
  return(t)

snap_list = ['028_z000p000', '027_z000p101', '026_z000p183', '025_z000p271',
             '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
             '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
             '016_z001p737', '015_z002p012']

pwd = '/home/mwilkins/EAGLE/plots/L0100N1504/REFERENCE/'

def select_galaxy_sample(raw_data):
  '''Cut sample up in interesting ways
  '''
  #TODO
  return(raw_data)

def plot_data_in_bins(data_x, data_y, data_bins, bin_edges, bin_string, snap='',
                      xlabel='', ylabel='', xlim=None, ylim=None, equal=False,
                      save=False, save_name='', close=True):
  '''For single snapshot
  '''
  fig = plt.figure()
  fig.set_size_inches(8, 18, forward=True)
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  n_plots = len(bin_edges) + 1

  index = np.digitize(data_bins, bins=bin_edges)

  axs_list = []

  for i in range(n_plots):
    axs_list.append(fig.add_subplot(n_plots, 1, i+1))

    axs_list[i].scatter(data_x[index==i], data_y[index==i], s=2, alpha=0.2, c='k')

    axs_list[i].text(xlim[0] + xlim[1]*0.1, ylim[0] + ylim[1]*0.9, bin_string[i])

    if i == 0:
      axs_list[i].text(xlim[0] + xlim[1]*0.9, ylim[0] + ylim[1]*0.9, snap,
                       horizontalalignment='right')

    if i+1 == n_plots:
      axs_list[i].set_xlabel(xlabel)
    else:
      axs_list[i].set_xticklabels([])

    if i == n_plots//2:
      axs_list[i].set_ylabel(ylabel)

    axs_list[i].set_xlim(xlim)
    axs_list[i].set_ylim(ylim)

    if equal:
      axs_list[i].set_aspect('equal')

  if save:
    plt.savefig(save_name, bbox_inches='tight')

  if close:
    plt.close()
    return()

  return(fig, axs_list)

def plot_dispersion_snap(raw_data, bin_n=5, z_snap=0):
  '''Plot dispersion vs time, cut up in a few different ways..
  '''
  # z0_snap = 0

  lin_bin_edges = np.linspace(0, 30, 31)

  volume = 4.0 / 3.0 * np.pi * (lin_bin_edges[bin_n+1]**3 - lin_bin_edges[bin_n]**3)

  bin_density = DM_MASS * raw_data['Profiles/DMN'][:,:,bin_n] / volume

  DMSigma_avg = np.sqrt((np.square(raw_data['Profiles/DMSigmax'][:,:,bin_n]) +
                         np.square(raw_data['Profiles/DMSigmay'][:,:,bin_n]) +
                         np.square(raw_data['Profiles/DMSigmaz'][:,:,bin_n]))/3)

  ####
  #try different binning ...
  bin_edges = np.array([8,9,10,11])

  data_x    = DMSigma_avg[:,z_snap]
  data_y    = raw_data['Profiles/Sigmaz'][:,z_snap,bin_n]
  data_bins = np.log10(raw_data['GalaxyMeasures/MassType_Star'][:,z_snap])

  bin_string = [r'$\log(M_\star / $M$_\odot) < 8$',
                r'$8 < \log(M_\star / $M$_\odot) < 9$',
                r'$9 < \log(M_\star / $M$_\odot) < 10$',
                r'$10 < \log(M_\star / $M$_\odot) < 11$',
                r'$11 < \log(M_\star / $M$_\odot)$']

  snap = snap_list[z_snap][4:]
  xlabel = r'$\sigma^{}_{DM}$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'
  ylabel = r'$\sigma^{}_z$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'

  xlim = (0,500)
  ylim = (0,300)

  equal = True
  save  = False
  save_name = pwd + 'other/' + 'sigma_z_dm_sigma_' + snap_list[z_snap][4:]
  close = False

  #the main part
  (fig, axs_list) = plot_data_in_bins(data_x, data_y, data_bins, bin_edges, bin_string, snap,
                                      xlabel, ylabel, xlim, ylim, equal,
                                      save, save_name, close)

  for axs in axs_list:
    axs.errorbar([0,300], [0,300], ls='--', c='r')

  plt.savefig(save_name, bbox_inches='tight')
  plt.close()

  ####
  data_y    = raw_data['Profiles/SigmaR'][:,z_snap,bin_n]

  ylabel = r'$\sigma^{}_R$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'

  save_name = pwd + 'other/' + 'sigma_R_dm_sigma_' + snap_list[z_snap][4:]

  #the main part
  (fig, axs_list) = plot_data_in_bins(data_x, data_y, data_bins, bin_edges, bin_string, snap,
                                      xlabel, ylabel, xlim, ylim, equal,
                                      save, save_name, close)

  for axs in axs_list:
    axs.errorbar([0,300], [0,300], ls='--', c='r')

  plt.savefig(save_name, bbox_inches='tight')
  plt.close()

  ####
  data_y    = raw_data['Profiles/Sigmaphi'][:,z_snap,bin_n]

  xlabel = r'$\sigma^{}_{DM}$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'
  ylabel = r'$\sigma^{}_\phi$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'

  save_name = pwd + 'other/' + 'sigma_phi_dm_sigma_' + snap_list[z_snap][4:]

  #the main part
  (fig, axs_list) = plot_data_in_bins(data_x, data_y, data_bins, bin_edges, bin_string, snap,
                                      xlabel, ylabel, xlim, ylim, equal,
                                      save, save_name, close)

  for axs in axs_list:
    axs.errorbar([0,300], [0,300], ls='--', c='r')

  plt.savefig(save_name, bbox_inches='tight')
  plt.close()

  return

def plot_ellipticity_vonsigma(raw_data, thirty_bin=43, z_snap=0):
  '''That plot everyone has.. Thob et al. 2019 etc.
  '''
  Sigma_tot = np.sqrt(np.square(raw_data['Profiles/SigmaR'][:,z_snap,thirty_bin]) +
                      np.square(raw_data['Profiles/Sigmaphi'][:,z_snap,thirty_bin]) +
                      np.square(raw_data['Profiles/Sigmaz'][:,z_snap,thirty_bin]))

  x_data = 1 - raw_data['GalaxyMeasures/Axisc'][:,z_snap] / raw_data['GalaxyMeasures/Axisa'][:,z_snap]

  y_data = raw_data['Profiles/MeanVphi'][:,z_snap,thirty_bin] / (Sigma_tot / np.sqrt(3))

  bin_edges = np.array([8,9,10,11])

  data_x    = x_data
  data_y    = y_data
  data_bins = np.log10(raw_data['GalaxyMeasures/MassType_Star'][:,z_snap])

  bin_string = [r'$\log(M_\star / $M$_\odot) < 8$',
                r'$8 < \log(M_\star / $M$_\odot) < 9$',
                r'$9 < \log(M_\star / $M$_\odot) < 10$',
                r'$10 < \log(M_\star / $M$_\odot) < 11$',
                r'$11 < \log(M_\star / $M$_\odot)$']

  snap = snap_list[z_snap][4:]
  xlabel = r'$ \epsilon = 1 - c/a $'
  ylabel = r'$ v^{}_\phi / ( \sigma^{}_{tot} / \sqrt{3} ) $'

  xlim = (0,1)
  ylim = (0,4)

  equal = False
  save  = True
  save_name = pwd + 'other/' + 'ellipticity_vonsigma_' + snap_list[z_snap][4:]
  close = True

  #the main part
  plot_data_in_bins(data_x, data_y, data_bins, bin_edges, bin_string, snap,
                    xlabel, ylabel, xlim, ylim, equal,
                    save, save_name, close)

  return

def plot_m_star_r(raw_data, snap_i=0):
  '''
  '''
  thirty_bin=43
  # x_data = raw_data['Profiles/BinMass'][:,snap_i,thirty_bin]
  x_data = raw_data['GalaxyMeasures/MassType_Star'][:,snap_i]

  y_data = raw_data['GalaxyMeasures/r_half'][:,snap_i]

  x_bin_edges   = np.logspace(7, 12, 11)
  x_bin_middles = np.logspace(7.25, 11.75, 10)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started r half')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((1e-1, 3e1))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ R_{1/2} $[kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 1e1, snap_list[snap_i][4:], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_rhalf_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  #zhalf over whole galaxy

  x_data = raw_data['GalaxyMeasures/MassType_Star'][:,snap_i]

  y_data = raw_data['Profiles/zHalf'][:,snap_i,thirty_bin]

  x_bin_edges   = np.logspace(7, 12, 11)
  x_bin_middles = np.logspace(7.25, 11.75, 10)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started z half')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((1e-1, 3e1))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ z_{1/2} $[kpc] [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 1e1, snap_list[snap_i][4:], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_zhalf_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  pass

def plot_m_star_sigma(raw_data, snap_i=0):
  '''
  '''
  thirty_bin = 43
  # rhalf_bin  = 45

  #R

  x_data = raw_data['GalaxyMeasures/MassType_Star'][:,snap_i]

  y_data = raw_data['Profiles/SigmaR'][:,snap_i,thirty_bin]

  x_bin_edges   = np.logspace(7, 12, 11)
  x_bin_middles = np.logspace(7.25, 11.75, 10)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started R')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((5e0, 3e2))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \sigma_R $[km/s] [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e2, snap_list[snap_i][4:], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_sigma_R_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  #z

  y_data = raw_data['Profiles/Sigmaz'][:,snap_i,thirty_bin]

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started z')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((5e0, 3e2))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \sigma_z $[km/s] [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e2, snap_list[snap_i][4:], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_sigma_z_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  #phi

  y_data = raw_data['Profiles/Sigmaphi'][:,snap_i,thirty_bin]

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started phi')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((5e0, 3e2))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \sigma_\phi $[km/s] [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e2, snap_list[snap_i], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_sigma_phi_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  #v phi

  y_data = raw_data['Profiles/MeanVphi'][:,snap_i,thirty_bin]

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started mean v phi')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((1e-1, 3e2))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \overline{v_\phi} $[km/s] [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e2, snap_list[snap_i], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_mean_v_phi_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  return

def plot_m_star_sigma_ratio(raw_data, snap_i=0):
  '''
  '''
  thirty_bin = 43
  # rhalf_bin  = 45

  #R
  DMSigma_avg = np.sqrt((np.square(raw_data['Profiles/DMSigmax'][:,snap_i,thirty_bin]) +
                         np.square(raw_data['Profiles/DMSigmay'][:,snap_i,thirty_bin]) +
                         np.square(raw_data['Profiles/DMSigmaz'][:,snap_i,thirty_bin]))/3)

  x_data = raw_data['GalaxyMeasures/MassType_Star'][:,snap_i]

  y_data = raw_data['Profiles/SigmaR'][:,snap_i,thirty_bin] / DMSigma_avg

  x_bin_edges   = np.logspace(7, 12, 11)
  x_bin_middles = np.logspace(7.25, 11.75, 10)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started R')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((3e-2, 3e0))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \sigma_R / \sigma_{DM} $ [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e0, snap_list[snap_i][4:], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_sigma_R_sigma_dm_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  #z

  y_data = raw_data['Profiles/Sigmaz'][:,snap_i,thirty_bin] / DMSigma_avg

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started z')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((3e-2, 3e0))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \sigma_z / \sigma_{DM} $ [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e0, snap_list[snap_i][4:], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_sigma_z_sigma_dm_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  #phi

  y_data = raw_data['Profiles/Sigmaphi'][:,snap_i,thirty_bin] / DMSigma_avg

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started phi')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()

  plt.xlim((1e7, 1e12))
  plt.ylim((3e-2, 3e0))

  plt.xlabel(r'$ M_\star $[M$_\odot$]')
  plt.ylabel(r'$ \sigma_\phi / \sigma_{DM} $ [stars $r < 30$kpc]')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(3e11, 2e0, snap_list[snap_i], ha='right')

  save_name = pwd + 'vs-mass/' + 'mass_sigma_phi_sigma_dm_' + snap_list[snap_i][4:]
  plt.savefig(save_name, bbox_inches='tight')

  return

def my_nonzero_nan_quantile(array, q):
  '''
  '''
  copy = array[np.logical_not(np.isnan(array))]
  copy = copy[copy!=0]
  if len(copy) == 0:
    return(0)

  return(np.quantile(copy, q))

def my_nonzero_nan_median(array):
  return(my_nonzero_nan_quantile(array, 0.5))
def my_nonzero_nan_psigma(array):
  return(my_nonzero_nan_quantile(array, 0.84))
def my_nonzero_nan_msigma(array):
  return(my_nonzero_nan_quantile(array, 0.16))

def my_nonzero_nan_median_axis(array, q):
  '''
  '''
  out = np.zeros(np.shape(array)[1])
  for i, row in enumerate(array.T):
    out[i] = my_nonzero_nan_quantile(row, q)

  return(out)

def plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog=False,
                                xlabel='', xlim=None, save=False, save_name='', close=True):

  '''
  '''
  snap_z = np.array([0.0,   0.101, 0.183, 0.271,
                     0.366, 0.503, 0.615, 0.736,
                     0.865, 1.004, 1.259, 1.487,
                     1.737, 2.012])
  # snap_a = 1 / (1 + snap_z)
  snap_t = lbt(z=np.array([1e4])) - lbt(z = snap_z)

  ylim = (3.28, 13.828)

  snap_ts = np.broadcast_to(snap_t, (np.shape(data_xs)[0], len(snap_t)))

  fig = plt.figure()
  fig.set_size_inches(8, 18, forward=True)
  fig.subplots_adjust(hspace=0.03,wspace=0.03)

  n_plots = len(bin_edges) + 1

  index = np.digitize(data_bins, bins=bin_edges)

  axs_list = []

  for i in range(n_plots):
    axs_list.append(fig.add_subplot(n_plots, 1, i+1))

    if semilog:
      axs_list[i].semilogy()

    axs_list[i].set_ylim(xlim)
    axs_list[i].set_xlim(ylim)

    # print(np.sum(index == i))

    #Try to plot faster
    axs_list[i].plot(snap_ts[index == i].T, data_xs[index == i, 0:14].T,
                     'k-', linewidth=0.05, rasterized=True)

    axs_list[i].text(ylim[0] + (ylim[1]-ylim[0])*0.1, xlim[0] + (xlim[1]-xlim[0])*0.9,
                     bin_string[i], verticalalignment='top')

    #median
    median = my_nonzero_nan_median_axis(data_xs[index == i, 0:14], 0.5)
    axs_list[i].plot(snap_t, median, 'c-', linewidth=2)

    plus = my_nonzero_nan_median_axis(data_xs[index == i, 0:14], 0.84)
    mins = my_nonzero_nan_median_axis(data_xs[index == i, 0:14], 0.16)
    axs_list[i].plot(snap_t, plus, 'c--', linewidth=1)
    axs_list[i].plot(snap_t, mins, 'c--', linewidth=1)

    if i+1 == n_plots:
      axs_list[i].set_xlabel(r'$t$ [Gyr]')
    else:
      axs_list[i].set_xticklabels([])

    if i == n_plots//2:
      axs_list[i].set_ylabel(xlabel)

  if save:
    plt.savefig(save_name, bbox_inches='tight')

  if close:
    plt.close()
    return()

  return(fig, axs_list)

def plot_dispersion_evolution(raw_data, bin_n=5):
  '''Plot dispersion vs time, cut up in a few different ways..
  '''
  z0_snap = 0

  # lin_bin_edges = np.linspace(0, 30, 31)
  # volume = 4.0 / 3.0 * np.pi * (lin_bin_edges[bin_n+1]**3 - lin_bin_edges[bin_n]**3)
  # bin_density = DM_MASS * raw_data['Profiles/DMN'][:,:,bin_n] / volume

  DMSigma_avg = np.sqrt((np.square(raw_data['Profiles/DMSigmax'][:,:,bin_n]) +
                         np.square(raw_data['Profiles/DMSigmay'][:,:,bin_n]) +
                         np.square(raw_data['Profiles/DMSigmaz'][:,:,bin_n]))/3)

  ####
  data_xs = raw_data['Profiles/Sigmaz'][:,:,bin_n]

  data_bins = np.log10(raw_data['GalaxyMeasures/MassType_Star'][:,z0_snap])

  bin_edges = np.array([8,9,10,11])

  bin_range_str = ' [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'
  if bin_n == 43:
    bin_range_str = r' [$< 30$]kpc'

  semilog = True

  bin_string = [r'$\log(M_\star / $M$_\odot) < 8$',
                r'$8 < \log(M_\star / $M$_\odot) < 9$',
                r'$9 < \log(M_\star / $M$_\odot) < 10$',
                r'$10 < \log(M_\star / $M$_\odot) < 11$',
                r'$11 < \log(M_\star / $M$_\odot)$']

  xlabel = r'$ \sigma^{}_z $ [km/s]' + bin_range_str

  xlim = (5,300)

  save  = True
  save_name = pwd + 'vs-time/' + 'time_sigma_z_z0mass_' + str(bin_n)
  close = True

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma z')

  ####
  data_xs = raw_data['Profiles/SigmaR'][:,:,bin_n]

  xlabel = r'$ \sigma^{}_R $ [km/s]' + bin_range_str

  save_name = pwd + 'vs-time/' + 'time_sigma_R_z0mass_' + str(bin_n)

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma R')

  ####
  data_xs = raw_data['Profiles/Sigmaphi'][:,:,bin_n]

  xlabel = r'$ \sigma^{}_\phi $ [km/s]' + bin_range_str

  save_name = pwd + 'vs-time/' + 'time_sigma_phi_z0mass_' + str(bin_n)

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma phi')

  ####
  data_xs = raw_data['Profiles/Sigmaz'][:,:,bin_n] / DMSigma_avg

  semilog = True

  xlabel = r'$ \sigma^{}_z / \sigma^{}_{DM} $' + bin_range_str

  xlim = (1e-1,3e0)

  save_name = pwd + 'vs-time/' + 'time_sigma_z_on_sigma_dm_z0mass_' + str(bin_n)

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma z / DM')

  ####
  data_xs = raw_data['Profiles/SigmaR'][:,:,bin_n] / DMSigma_avg

  xlabel = r'$ \sigma^{}_R / \sigma^{}_{DM} $' + bin_range_str

  save_name = pwd + 'vs-time/' + 'time_sigma_R_on_sigma_dm_z0mass_' + str(bin_n)

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma R / DM')

  ####
  data_xs = raw_data['Profiles/Sigmaphi'][:,:,bin_n] / DMSigma_avg

  xlabel = r'$ \sigma^{}_\phi / \sigma^{}_{DM} $' + bin_range_str

  save_name = pwd + 'vs-time/' + 'time_sigma_phi_on_sigma_dm_z0mass_' + str(bin_n)

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma phi / DM')

  return

def plot_selected_dispersion_evolution(raw_data, bin_n=5):
  '''Plot dispersion vs time, cut up in a few different ways..
  '''
  #data details
  z2_snap  = 13
  z1_snap  = 9
  z05_snap = 5
  z0_snap = 0

  thirty_bin = 43

  #choices
  snap_choice = z2_snap
  mass_diff_threshold = 0.2
  kappa_threshold     = 0.4

  galaxy_masses      = raw_data['GalaxyMeasures/MassType_Star'][:,:]
  # spheroid_fractions = raw_data['Profiles/fjzjc00'][:,:,thirty_bin]
  kappa_fractions    = raw_data['Profiles/KappaCo'][:,:,thirty_bin]

  merger_cut = ((np.abs(galaxy_masses[:,snap_choice] - galaxy_masses[:,z0_snap])
                 / galaxy_masses[:,z0_snap]) < mass_diff_threshold)

  kappa_cut = (kappa_fractions[:,snap_choice] > kappa_threshold)

  mask = np.logical_and(merger_cut, kappa_cut)

  #do the rest
  DMSigma_avg = np.sqrt((np.square(raw_data['Profiles/DMSigmax'][:,:,bin_n]) +
                         np.square(raw_data['Profiles/DMSigmay'][:,:,bin_n]) +
                         np.square(raw_data['Profiles/DMSigmaz'][:,:,bin_n]))/3)
  DMSigma_avg = DMSigma_avg[mask,:]

  ####
  data_xs = raw_data['Profiles/Sigmaz'][:,:,bin_n]
  data_xs = data_xs[mask,:]

  data_bins = np.log10(raw_data['GalaxyMeasures/MassType_Star'][:,z0_snap])
  data_bins = data_bins[mask]

  bin_edges = np.array([8,9,10,11])

  bin_range_str = ' [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc'
  if bin_n == 43:
    bin_range_str = r' [$< 30$]kpc'

  semilog = True

  bin_string = [r'$\log(M_\star / $M$_\odot) < 8$',
                r'$8 < \log(M_\star / $M$_\odot) < 9$',
                r'$9 < \log(M_\star / $M$_\odot) < 10$',
                r'$10 < \log(M_\star / $M$_\odot) < 11$',
                r'$11 < \log(M_\star / $M$_\odot)$']

  xlabel = r'$ \sigma^{}_z $ [km/s]' + bin_range_str

  xlim = (5,300)

  save  = True
  save_name = pwd + 'cut-test/' + 'time_sigma_z_z0mass_' + str(bin_n)
  close = True

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma z')

  ####
  data_xs = raw_data['Profiles/Sigmaz'][:,:,bin_n]
  data_xs = data_xs[mask,:] / DMSigma_avg

  semilog = True

  xlabel = r'$ \sigma^{}_z / \sigma^{}_{DM} $' + bin_range_str

  xlim = (1e-1,3e0)

  save_name = pwd + 'cut-test/' + 'time_sigma_z_on_sigma_dm_z0mass_' + str(bin_n)

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done sigma z / DM')

  return

def plot_ellipticity_evolution(raw_data):
  '''
  '''
  z0_snap = 0

  thirty_bin = 43
  r_half_bin = 45
  jzonc_07 = 3

  ####
  # data_xs = 1 - 2 * raw_data['Profiles/fjzjc00'][:,:,thirty_bin]
  data_xs = 2 * (1 - raw_data['Profiles/fjzjc00'][:,:,thirty_bin])
  data_xs[data_xs == 2] = np.nan
  data_xs[data_xs == 0] = np.nan

  data_bins = np.log10(raw_data['GalaxyMeasures/MassType_Star'][:,z0_snap])

  bin_edges = np.array([8,9,10,11])

  semilog = False

  bin_string = [r'$\log(M_\star / $M$_\odot) < 8$',
                r'$8 < \log(M_\star / $M$_\odot) < 9$',
                r'$9 < \log(M_\star / $M$_\odot) < 10$',
                r'$10 < \log(M_\star / $M$_\odot) < 11$',
                r'$11 < \log(M_\star / $M$_\odot)$']

  xlabel = r'Spheroid Fraction'

  xlim = (0,1)

  save  = True
  save_name = pwd + 'vs-time/' + 'time_spheroid_ndm30'
  close = True

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done spheroid')

  # data_xs = raw_data['Profiles/fjzjc'][:,:,thirty_bin, jzonc_07]

  # xlabel = r'$j_z / j_c(E) > 0.7$ Fraction'

  # save_name = pwd + 'vs-time/' + 'time_fjzjc07_ndm30'

  # plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
  #                             xlabel, xlim, save, save_name, close)

  # print('Done thin disk')

  return

def plot_kappa_evolution(raw_data):
  '''
  '''
  z0_snap = 0

  thirty_bin = 43

  ####
  data_xs = raw_data['Profiles/KappaCo'][:,:,thirty_bin]

  data_bins = np.log10(raw_data['GalaxyMeasures/MassType_Star'][:,z0_snap])

  bin_edges = np.array([8,9,10,11])

  semilog = False

  bin_string = [r'$\log(M_\star / $M$_\odot) < 8$',
                r'$8 < \log(M_\star / $M$_\odot) < 9$',
                r'$9 < \log(M_\star / $M$_\odot) < 10$',
                r'$10 < \log(M_\star / $M$_\odot) < 11$',
                r'$11 < \log(M_\star / $M$_\odot)$']

  xlabel = r'$\kappa_{co}$'

  xlim = (0,1)

  save  = True
  save_name = pwd + 'vs-time/' + 'kappa_co_mass'
  close = True

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done co')

  data_xs = raw_data['Profiles/KappaRot'][:,:,thirty_bin]

  xlabel = r'$\kappa_{rot}$'

  save_name = pwd + 'vs-time/' + 'kappa_rot_mass'

  plot_data_evolution_in_bins(data_xs, data_bins, bin_edges, bin_string, semilog,
                              xlabel, xlim, save, save_name, close)

  print('Done rot')

  return

###############################################################################

def single_galaxy_kinematic_evolution(raw_data, top_leaf_id, bin_n=5):
  '''
  '''
  top_leaf_index = np.nonzero(raw_data['IDs/TopLeafID'][()] == top_leaf_id)[0]
  print(top_leaf_index)
  if len(top_leaf_index) == 1:
    top_leaf_index = top_leaf_index[0]

    snap_z = np.array([0.0,   0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736, 0.865,
                       1.004, 1.259, 1.487, 1.737, 2.012])
    snap_t = lbt(z=np.array([1e4])) - lbt(z = snap_z)
    xlim = (3.28, 13.828)

    thirty_bin = 43

    fig = plt.figure()
    fig.set_size_inches(10, 25, forward=True)
    fig.subplots_adjust(hspace=0.03,wspace=0.03)

    ax1 = fig.add_subplot(811)
    ax_twin = ax1.twiny()
    ax2 = fig.add_subplot(812)
    ax3 = fig.add_subplot(813)
    ax4 = fig.add_subplot(814)
    ax5 = fig.add_subplot(815)
    ax6 = fig.add_subplot(816)
    ax7 = fig.add_subplot(817)
    ax8 = fig.add_subplot(818)

    DMSigma_avg = np.sqrt((np.square(raw_data['Profiles/DMSigmax'][top_leaf_index,0:14,bin_n]) +
                           np.square(raw_data['Profiles/DMSigmay'][top_leaf_index,0:14,bin_n]) +
                           np.square(raw_data['Profiles/DMSigmaz'][top_leaf_index,0:14,bin_n]))/3)

    ax1lim = [0, 1.2 * np.amax(raw_data['Profiles/BinMass'][top_leaf_index, 0:14, thirty_bin])]
    ax2lim = [0, 1.2]
    ax3lim = [0, 1.2]
    ax4lim = [0, 1.2]
    ax5lim = [0, 1.2 * np.amax(DMSigma_avg)]
    ax6lim = [1e-2, 1e1]
    ax7lim = [0, 1.2 * np.amax(DMSigma_avg)]
    ax8lim = [0, 1.2 * np.amax(raw_data['Profiles/zHalf'][top_leaf_index, 0:14, bin_n])]

    #not important to include
    ax1.vlines(snap_t, ax1lim[0], ax1lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax2.vlines(snap_t, ax2lim[0], ax2lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax3.vlines(snap_t, ax3lim[0], ax3lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax4.vlines(snap_t, ax4lim[0], ax4lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax5.vlines(snap_t, ax5lim[0], ax5lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax6.vlines(snap_t, ax6lim[0], ax6lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax7.vlines(snap_t, ax5lim[0], ax5lim[1], linestyles='--', alpha=0.2, linewidth=1)
    ax8.vlines(snap_t, ax6lim[0], ax6lim[1], linestyles='--', alpha=0.2, linewidth=1)

    #the actual plot
    ax1.errorbar(snap_t, raw_data['Extras/Formation_pct'][top_leaf_index, 0:14] *
                 raw_data['Profiles/BinMass'][top_leaf_index, 0, thirty_bin],
                 fmt='o', ls=':', label=r'$z=0$ SFH')
    ax1.errorbar(snap_t, raw_data['Profiles/BinMass'][top_leaf_index, 0:14, thirty_bin],
                 fmt='o', ls=':', label='Snapshot Mass')
    ax1.set_ylabel('$M_\star$ [$10^{10} $M$_\odot$]')
    ax1.legend(ncol=2)

    ax2.errorbar(snap_t, raw_data['GalaxyMeasures/Axisc'][top_leaf_index, 0:14] /
                 raw_data['GalaxyMeasures/Axisa'][top_leaf_index, 0:14],
                 fmt='o', ls=':', label=r'$c/a$')
    ax2.errorbar(snap_t, raw_data['GalaxyMeasures/Axisb'][top_leaf_index, 0:14] /
                 raw_data['GalaxyMeasures/Axisa'][top_leaf_index, 0:14],
                 fmt='o', ls=':', label=r'$b/a$')
    tri_shape = ((raw_data['GalaxyMeasures/Axisa'][top_leaf_index, 0:14] ** 2 -
                  raw_data['GalaxyMeasures/Axisb'][top_leaf_index, 0:14] ** 2) /
                 (raw_data['GalaxyMeasures/Axisa'][top_leaf_index, 0:14] ** 2 -
                  raw_data['GalaxyMeasures/Axisc'][top_leaf_index, 0:14] ** 2))
    ax2.errorbar(snap_t, tri_shape,
                 fmt='o', ls=':', label=r'T')
    ax2.set_ylabel('$M_\star$ [$10^{10} $M$_\odot$]')
    ax2.legend(ncol=3)

    ax3.errorbar(snap_t, raw_data['Profiles/fjzjc'][top_leaf_index, 0:14, thirty_bin, 1],
                 fmt='o', ls=':', label='$\epsilon > 0.9$')
    ax3.errorbar(snap_t, raw_data['Profiles/fjzjc'][top_leaf_index, 0:14, thirty_bin, 2],
                 fmt='o', ls=':', label='$\epsilon > 0.8$')
    ax3.errorbar(snap_t, raw_data['Profiles/fjzjc'][top_leaf_index, 0:14, thirty_bin, 3],
                 fmt='o', ls=':', label='$\epsilon > 0.7$')
    ax3.errorbar(snap_t, raw_data['Profiles/fjzjc'][top_leaf_index, 0:14, thirty_bin, 10],
                 fmt='o', ls=':', label='$\epsilon > 0.0$')
    ax3.set_ylabel(r'$F[ j_z/j_c(E) > x ]$')
    ax3.legend(ncol=4)

    ax4.errorbar(snap_t, raw_data['Profiles/KappaRot'][top_leaf_index, 0:14, thirty_bin],
                 fmt='o', ls=':', label='$\kappa_{rot}$')
    ax4.errorbar(snap_t, raw_data['Profiles/KappaCo'][top_leaf_index, 0:14, thirty_bin],
                 fmt='o', ls=':', label='$\kappa_{co}$')
    ax4.set_ylabel(r'$\kappa$')
    ax4.legend(ncol=2)

    sigma_z   = raw_data['Profiles/Sigmaz'][top_leaf_index, 0:14, bin_n]
    sigma_R   = raw_data['Profiles/SigmaR'][top_leaf_index, 0:14, bin_n]
    sigma_phi = raw_data['Profiles/Sigmaphi'][top_leaf_index, 0:14, bin_n]

    ax5.errorbar(snap_t, sigma_z,
                 fmt='o', ls=':', label=r'$\sigma^{}_z$')
    ax5.errorbar(snap_t, sigma_R,
                 fmt='o', ls=':', label=r'$\sigma^{}_R$')
    ax5.errorbar(snap_t, sigma_phi,
                 fmt='o', ls=':', label=r'$\sigma^{}_\phi$')
    ax5.errorbar(snap_t, DMSigma_avg,
                 fmt='o', ls=':', label=r'$\sigma^{}_{DM}$')
    ax5.set_ylabel(r'$\sigma$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc')
    ax5.legend(ncol=4)

    ax6.errorbar(snap_t, sigma_z / DMSigma_avg,
                 fmt='o', ls=':', label=r'$\sigma^{}_z / \sigma_{DM}$')
    ax6.errorbar(snap_t, sigma_R / DMSigma_avg,
                 fmt='o', ls=':', label=r'$\sigma^{}_R / \sigma_{DM}$')
    ax6.errorbar(snap_t, sigma_phi / DMSigma_avg,
                 fmt='o', ls=':', label=r'$\sigma^{}_\phi / \sigma_{DM}$')
    ax6.set_ylabel(r'$\sigma_\star / \sigma_{DM}$ [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc')
    ax6.legend(ncol=4)
    ax6.semilogy()

    ax7.errorbar(snap_t, raw_data['Profiles/MeanVphi'][top_leaf_index, 0:14, bin_n],
                 fmt='o', ls=':')
    ax7.set_ylabel(r'$\overline{v_\phi}$ [km/s] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc')

    ax8.errorbar(snap_t, raw_data['Profiles/zHalf'][top_leaf_index, 0:14, bin_n],
                 fmt='o', ls=':')
    ax8.set_ylabel(r'$z_{1/2}$ [kpc] [' + str(int(bin_n)) + '-' + str(int(bin_n)+1) + ']kpc')
    ax8.set_xlabel(r'$t$ [Gyr]')

    ax_twin.set_xlabel(r'$z$')
    ax_twin.set_xticks(snap_t - snap_t[-1])
    label = []
    for i in range(len(snap_z)):
      if i%2 == 0: label.append(str(np.round(snap_z[i],2)))
      else: label.append('')
    ax_twin.set_xticklabels(label)

    ax1.set_xlim(xlim)
    ax2.set_xlim(xlim)
    ax3.set_xlim(xlim)
    ax4.set_xlim(xlim)
    ax5.set_xlim(xlim)
    ax6.set_xlim(xlim)
    ax7.set_xlim(xlim)
    ax8.set_xlim(xlim)

    ax1.set_ylim(ax1lim)
    ax2.set_ylim(ax2lim)
    ax3.set_ylim(ax3lim)
    ax4.set_ylim(ax4lim)
    ax5.set_ylim(ax5lim)
    ax6.set_ylim(ax6lim)
    ax7.set_ylim(ax7lim)
    ax8.set_ylim(ax8lim)

    plt.savefig(pwd + 'single-galaxy/' + str(top_leaf_id) + '_kinematic_evolution', bbox_inches='tight')
    plt.close()

  else:
    print('Warning: Missing top leaf id.')

  return

def single_galaxy_profile_evolution(raw_data, top_leaf_id):
  '''
  '''
  top_leaf_index = np.nonzero(raw_data['IDs/TopLeafID'][()] == top_leaf_id)[0]
  print(top_leaf_index)
  if len(top_leaf_index) == 1:
    top_leaf_index = top_leaf_index[0]

    snap_z = np.array([0.0,   0.101, 0.183, 0.271, 0.366, 0.503, 0.615, 0.736, 0.865,
                       1.004, 1.259, 1.487, 1.737, 2.012])
    snap_t = lbt(z=np.array([1e4])) - lbt(z = snap_z)

    x = np.linspace(0.5, 29.5, 30)
    xlim = (0, 30)

    fig = plt.figure()
    fig.set_size_inches(10, 25, forward=True)
    fig.subplots_adjust(hspace=0.03,wspace=0.03)

    #TODO add mass profile
    ax1 = fig.add_subplot(811)
    ax2 = fig.add_subplot(812)
    ax3 = fig.add_subplot(813)
    ax4 = fig.add_subplot(814)
    ax5 = fig.add_subplot(815)
    ax6 = fig.add_subplot(816)
    ax7 = fig.add_subplot(817)
    ax8 = fig.add_subplot(818)

    DMSigma_avg = np.sqrt((np.square(raw_data['Profiles/DMSigmax'][top_leaf_index,0:14,0:30]) +
                           np.square(raw_data['Profiles/DMSigmay'][top_leaf_index,0:14,0:30]) +
                           np.square(raw_data['Profiles/DMSigmaz'][top_leaf_index,0:14,0:30]))/3)

    sigma_z   = raw_data['Profiles/Sigmaz'][top_leaf_index, 0:14, 0:30]
    sigma_R   = raw_data['Profiles/SigmaR'][top_leaf_index, 0:14, 0:30]
    sigma_phi = raw_data['Profiles/Sigmaphi'][top_leaf_index, 0:14, 0:30]

    mean_v_phi = raw_data['Profiles/MeanVphi'][top_leaf_index, 0:14, 0:30]

    ax1lim = [0, 1.2 * np.nanmax(DMSigma_avg)]
    ax2lim = [0, 1.2 * np.nanmax(DMSigma_avg)]
    ax3lim = [0, 1.2 * np.nanmax(DMSigma_avg)]
    ax4lim = [1e-1, 3e1]
    ax5lim = [1e-1, 3e1]
    ax6lim = [1e-1, 3e1]
    ax7lim = [-1.2 * np.nanmax(np.abs(mean_v_phi)), 1.2 * np.nanmax(np.abs(mean_v_phi))]
    ax8lim = [-0.2, 1.2]

    ax1.set_xlim(xlim)
    ax2.set_xlim(xlim)
    ax3.set_xlim(xlim)
    ax4.set_xlim(xlim)
    ax5.set_xlim(xlim)
    ax6.set_xlim(xlim)
    ax7.set_xlim(xlim)
    ax8.set_xlim(xlim)

    ax1.set_ylim(ax1lim)
    ax2.set_ylim(ax2lim)
    ax3.set_ylim(ax3lim)
    ax4.set_ylim(ax4lim)
    ax5.set_ylim(ax5lim)
    ax6.set_ylim(ax6lim)
    ax7.set_ylim(ax7lim)
    ax8.set_ylim(ax8lim)

    ax4.semilogy()
    ax5.semilogy()
    ax6.semilogy()

    #color by time
    grys  = Greys(snap_t / snap_t[0])
    purps = Purples(snap_t / snap_t[0])
    grns  = Greens(snap_t / snap_t[0])
    orngs = Oranges(snap_t / snap_t[0])
    bls   = Blues(snap_t / snap_t[0])
    rds   = Reds(snap_t / snap_t[0])

    #the actual plot
    for j in range(len(snap_t)):
      i = len(snap_t) - j - 1

      ax1.errorbar(x, sigma_z[i,:],     c=orngs[i])
      ax1.errorbar(x, DMSigma_avg[i,:], c=grys[i])

      ax2.errorbar(x, sigma_R[i,:],     c=grns[i])
      ax2.errorbar(x, DMSigma_avg[i,:], c=grys[i])

      ax3.errorbar(x, sigma_phi[i,:],   c=purps[i])
      ax3.errorbar(x, DMSigma_avg[i,:], c=grys[i])

      ax4.errorbar(x, sigma_z[i,:] / DMSigma_avg[i,:], c=orngs[i])

      ax5.errorbar(x, sigma_R[i,:] / DMSigma_avg[i,:], c=grns[i])

      ax6.errorbar(x, sigma_phi[i,:] / DMSigma_avg[i,:], c=purps[i])

      ax7.errorbar(x, mean_v_phi[i,:], alpha=0.6, c=rds[i])

      ax8.errorbar(x, raw_data['Profiles/fjzjc'][top_leaf_index, i, 0:30, 10], c=bls[i])

      ax8.errorbar(x, raw_data['Profiles/fjzjc'][top_leaf_index, i, 0:30, 3], c=rds[i])
      # ax8.errorbar(x, raw_data['Profiles/fjzjc'][top_leaf_index, i, 0:30, 2], c=rds[i])
      # ax8.errorbar(x, raw_data['Profiles/fjzjc'][top_leaf_index, i, 0:30, 1], c=orngs[i])

    ax1.set_ylabel(r'$ \sigma^{}_z$ [km/s]')
    ax2.set_ylabel(r'$ \sigma^{}_R$ [km/s]')
    ax3.set_ylabel(r'$ \sigma^{}_\phi$ [km/s]')
    ax4.set_ylabel(r'$ \sigma^{}_z / \sigma_{DM} $')
    ax5.set_ylabel(r'$ \sigma^{}_R / \sigma_{DM} $')
    ax6.set_ylabel(r'$ \sigma^{}_\phi / \sigma_{DM} $')
    ax7.set_ylabel(r'$ \overline{v_\phi} $ [km/s]')
    ax8.set_ylabel(r'$F[ j_z/j_c(E) > x ]$')

    ax8.set_xlabel(r'$R$ [kpc]')

    #guiding lines
    ax4.hlines(1, xlim[0], xlim[1], linestyles='--', alpha=0.5, linewidth=1)
    ax5.hlines(1, xlim[0], xlim[1], linestyles='--', alpha=0.5, linewidth=1)
    ax6.hlines(1, xlim[0], xlim[1], linestyles='--', alpha=0.5, linewidth=1)
    ax7.hlines(0, xlim[0], xlim[1], linestyles='--', alpha=0.5, linewidth=1)
    ax8.hlines(0, xlim[0], xlim[1], linestyles='--', alpha=0.5, linewidth=1)

    # ax1.legend([r'$\sigma_z$', r'$\sigma_R$', r'$\sigma_\phi$', r'$\sigma_{DM}$'], ncol=4)
    # ax2.legend([r'$\sigma_z/ \sigma_{DM}$', r'$\sigma_R / \sigma_{DM}$',
    #             r'$\sigma_\phi / \sigma_{DM}$'], ncol=3)
    ax8.legend([r'$\epsilon > 0.0$', r'$\epsilon > 0.7$'], ncol=2)

    plt.savefig(pwd + 'single-galaxy/' + str(top_leaf_id) + '_profile_evolution', bbox_inches='tight')
    plt.close()

  else:
    print('Warning: Missing top leaf id.')

  return

###############################################################################

def plot_main_galaxy_sequence(raw_data, snap_i):
  '''
  '''
  centrals = (raw_data['IDs/SubGroupID'][:,snap_i] == 0)
  # centrals = np.ones(np.shape(raw_data['GalaxyMeasures/Group_M_Crit200'][:, 0]), dtype=bool)

  thirty_bin = 43

  # x_data = raw_data['GalaxyMeasures/Group_M_Crit200'][centrals, snap_i] / LITTLE_H * 1e10
  #TODO change to this format
  x_data = raw_data['GalaxyMeasures/Group_M_Crit200'][:, snap_i]
  x_data = x_data[centrals] / LITTLE_H * 1e10

  y_data = raw_data['Profiles/BinMass'][:, snap_i, thirty_bin] * 1e10
  y_data = y_data[centrals] / x_data

  x_bin_edges   = np.logspace(7, 13, 13)
  x_bin_middles = np.logspace(7.25, 12.75, 12)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started potting')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.loglog()
  plt.xlim((1e8, 1e13))
  plt.ylim((1e-4, 1e0))

  plt.text(3e12, 0.9, snap_list[snap_i][4:], ha='right', va='top')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.xlabel(r'$M_{200} [$M$_\odot]$')
  plt.ylabel(r'$M_\star / M_{200}$')

  plt.savefig(pwd + 'vs-mass/' + 'main_sequence' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  return

def plot_kappa_age(raw_data, snap_i):
  '''
  '''
  thirty_bin = 43

  x_data = raw_data['Extras/aFormed50pct'][:]
  x_data = lbt(z = np.array([1e4])) - lbt(a = x_data) #time since bb in Gyr

  y_data = raw_data['Profiles/KappaCo'][:, snap_i, thirty_bin]

  x_bin_edges   = np.linspace(3.5, 13.5, 21)
  x_bin_middles = np.linspace(3.75, 13.25, 20)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started kappa co')

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.xlim((3.28, 13.828))
  plt.ylim((0, 1))

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(12, 0.9, snap_list[snap_i][4:], ha='right')

  plt.xlabel(r'$50\%$ mass formation time [Gyr]')
  plt.ylabel(r'$\kappa_{co}$')

  plt.savefig(pwd + 'vs-age/' + 'age_kappa_co' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  #kappa rot

  y_data = raw_data['Profiles/KappaCo'][:, snap_i, thirty_bin]

  x_bin_edges   = np.linspace(3.5, 13.5, 21)
  x_bin_middles = np.linspace(3.75, 13.25, 20)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('Started kappa rot')

  fig = plt.figure()
  fig.set_size_inches(8, 6, forward=True)

  plt.xlim((3.28, 13.828))
  plt.ylim((0, 1))

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.errorbar(x_bin_middles, medians, c='c')
  plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
  plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

  plt.text(12, 0.9, snap_list[snap_i][4:], ha='right')

  plt.xlabel(r'$50\%$ mass formation time [Gyr]')
  plt.ylabel(r'$\kappa_{rot}$')

  plt.savefig(pwd + 'vs-age/' + 'age_kappa_rot' + snap_list[snap_i], bbox_inches='tight')
  plt.close()

  return

def height_evolution_binned(raw_data, snap_i=0, m200_cut_index=15):
  '''
  '''
  snap_z0 = 0
  #to check galaxy selection...
  centrals = (raw_data['IDs/SubGroupID'][:,snap_z0] == 0)
  # centrals = np.ones(np.shape(raw_data['GalaxyMeasures/Group_M_Crit200'][:, 0]), dtype=bool)

  # m200_cut_index = 11 #m200 = 1e10
  # m200_cut_index = 15 #m200 = 1e11
  # m200_cut_index = 19 #m200 = 1e12

  thirty_bin = 43

  x_data = raw_data['GalaxyMeasures/Group_M_Crit200'][:, snap_z0]
  x_data = x_data[centrals] / LITTLE_H * 1e10

  y_data = raw_data['Profiles/BinMass'][:, snap_z0, thirty_bin] * 1e10
  y_data = y_data[centrals] / x_data

  x_bin_edges   = np.logspace(7.125, 13.125, 25)
  x_bin_middles = np.logspace(7.25,  13,     24)

  medians, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_median)
  psigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_psigma)
  msigmas, _,_ = binned_statistic(x_data, y_data, bins=x_bin_edges, statistic=my_nonzero_nan_msigma)

  print('cut is')
  print(np.log10(x_bin_middles[m200_cut_index]),   np.log10(medians[m200_cut_index]))
  print(np.log10(x_bin_middles[m200_cut_index+1]), np.log10(medians[m200_cut_index+1]))

  #already selected centrals
  m200_mask  = np.logical_and((x_data > x_bin_middles[m200_cut_index]),
                              (x_data < x_bin_middles[m200_cut_index+1]))

  y_data > 10**(np.log10(msigmas[m200_cut_index]) +
                np.log10(msigmas[m200_cut_index-1] / msigmas[m200_cut_index]) /
                np.log10(x_bin_middles[m200_cut_index-1] / x_bin_middles[m200_cut_index]) *
                np.log10(x_data / x_bin_middles[m200_cut_index]))

  y_data < 10**(np.log10(psigmas[m200_cut_index]) +
                np.log10(psigmas[m200_cut_index-1] / psigmas[m200_cut_index]) /
                np.log10(x_bin_middles[m200_cut_index-1] / x_bin_middles[m200_cut_index]) *
                np.log10(x_data / x_bin_middles[m200_cut_index]))


  ratio_mask = np.logical_and((y_data > medians[m200_cut_index]),
                              (y_data < medians[m200_cut_index+1]))




  selection_mask = np.logical_and(m200_mask, ratio_mask)

  if snap_i == snap_z0:
    print('Started main sequence selection plot')

    fig = plt.figure()
    fig.set_size_inches(6, 6, forward=True)

    plt.loglog()
    plt.xlim((1e8, 3e13))
    plt.ylim((1e-4, 2e-1))

    plt.text(1e13, 1e-1, snap_list[snap_z0][4:], ha='right', va='top')

    plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

    plt.errorbar(x_bin_middles, medians, c='c')
    plt.errorbar(x_bin_middles, psigmas, c='c', ls='--')
    plt.errorbar(x_bin_middles, msigmas, c='c', ls='--')

    plt.errorbar([x_bin_middles[m200_cut_index],   x_bin_middles[m200_cut_index+1],
                  x_bin_middles[m200_cut_index+1], x_bin_middles[m200_cut_index],
                  x_bin_middles[m200_cut_index]],
                 [msigmas[m200_cut_index],   psigmas[m200_cut_index],
                  psigmas[m200_cut_index+1], msigmas[m200_cut_index+1],
                  msigmas[m200_cut_index]], c='r', ls='-')

    plt.xlabel(r'$M_{200} [$M$_\odot]$')
    plt.ylabel(r'$M_\star / M_{200}$')

    plt.savefig(pwd + 'cut-test/' + 'main_sequence_selection' + snap_list[snap_z0] +
                str(m200_cut_index), bbox_inches='tight')
    plt.close()

  #the actual plot(s)
  x_data = raw_data['Profiles/MedianFormationa'][:, snap_i, thirty_bin]
  x_data = x_data[centrals]
  x_data = lbt(z = [1e4]) - lbt(a = x_data[selection_mask])

  y_data = raw_data['Profiles/Sigmaz'][:, snap_i, thirty_bin]
  y_data = y_data[centrals]
  y_data = y_data[selection_mask]

  print('Started sigma z')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.semilogy()
  plt.xlim((0, 13.828))
  plt.ylim((5, 300))

  plt.text(13, 250, snap_list[snap_i][4:], ha='right', va='top')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.xlabel(r'Median Star Formation Age [Gyr]')
  plt.ylabel(r'$\sigma_z$ [$r < 30$kpc] [km/s]')

  plt.savefig(pwd + 'cut-test/' + 'median_age_sigma_z_' + snap_list[snap_i] +
              str(m200_cut_index), bbox_inches='tight')
  plt.close()

  #the actual plot(s)
  x_data = raw_data['Profiles/MedianFormationa'][:, snap_i, thirty_bin]
  x_data = x_data[centrals]
  x_data = lbt(z = [1e4]) - lbt(a = x_data[selection_mask])

  y_data = raw_data['Profiles/zHalf'][:, snap_i, thirty_bin]
  y_data = y_data[centrals]
  y_data = y_data[selection_mask]

  print('Started z half')

  fig = plt.figure()
  fig.set_size_inches(6, 6, forward=True)

  plt.semilogy()
  plt.xlim((0, 13.828))
  plt.ylim((1e-1, 3e1))

  plt.text(13, 2.5e1, snap_list[snap_i][4:], ha='right', va='top')

  plt.scatter(x_data, y_data, s=1, alpha=1, c='k')

  plt.xlabel(r'Median Star Formation Age [Gyr]')
  plt.ylabel(r'$z_{1/2}$ [$r < 30$kpc] [kpc]')

  plt.savefig(pwd + 'cut-test/' + 'median_age_z_half_' + snap_list[snap_i] +
              str(m200_cut_index), bbox_inches='tight')
  plt.close()

  return

if __name__ == '__main__':

  start_time = time.time()

  ids = [16636222, 17397709, 16895384,  9378448, 10229132, 8988032,  17640326,  8892366,
         16262553,  3274741, 17494095,  9669740, 11419723, 18118087, 16673564,  9197993,
         14096298,  9736205,  9125288, 20099652, 20440731, 21109786, 21379548, 21379548,
         21573612,  8739471]

  combined_name = '/fred/oz009/mwilkinson/EAGLE/processed_data/combined_profiles.hdf5'
  with h5.File(combined_name, 'r') as raw_data:

    # for i in range(14):
    #   plot_dispersion_snap(raw_data, z_snap=i)
    #   print(i)

    # for i in range(14):
    #   plot_ellipticity_vonsigma(raw_data, z_snap=i)
    #   print(i)

    for i in range(14):
      # plot_m_star_sigma(raw_data, snap_i=i)
      # plot_m_star_r(raw_data, snap_i=i)
      # plot_m_star_sigma_ratio(raw_data, snap_i=i)
      height_evolution_binned(raw_data, i)
      print(i)

    # thirty_bin = 43
    # # for i in range(10):
    # #   print('Doing dispersion evolution graphs.')
    # #   plot_dispersion_evolution(raw_data, bin_n=i)
    # print('Doing dispersion evolution graphs.')
    # plot_dispersion_evolution(raw_data, bin_n=thirty_bin)

    # print('Doing dispersion evolution graphs.')
    # thirty_bin = 43
    # plot_selected_dispersion_evolution(raw_data, bin_n=thirty_bin)

    # print('Doing ellipticity graphs')
    # plot_ellipticity_evolution(raw_data)
    # plot_kappa_evolution(raw_data)

    # print(raw_data['IDs/TopLeafID'])
    # print(len(raw_data['IDs/TopLeafID']))

    # for top_id in ids:
    #   print(top_id)
    #   single_galaxy_profile_evolution(raw_data, top_id)

    # print('Starting')
    # for i in range(14):
      # plot_m_star_r(raw_data, i)
      # plot_main_galaxy_sequence(raw_data, i)
      # print(i)

    # for i in range(14):
    #   plot_kappa_age(raw_data, i)
    #   print(i)

    print(np.round(time.time() - start_time))
    print('Done.')

  pass