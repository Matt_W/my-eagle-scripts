#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 17:29:54 2021

@author: matt
"""

#import os
import numpy as np

import matplotlib
matplotlib.use('Agg')

import h5py  as h5

from scipy.optimize          import brentq
from scipy.spatial.transform import Rotation
from scipy.stats             import binned_statistic_2d

import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = False
rcParams['text.usetex'] = False
rcParams['font.sans-serif'] = ['cmr10', 'Times-Roman']
rcParams['font.weight'] = 'normal'
rcParams['font.size'] = 20
rcParams['xtick.labelsize'] = 20
rcParams['ytick.labelsize'] = 20
rcParams['axes.labelsize'] = 22
rcParams['axes.linewidth'] = 1
rcParams['axes.unicode_minus'] = False
rcParams['axes.grid'] = True
rcParams['xtick.minor.width'] = 1
rcParams['ytick.minor.width'] = 1
rcParams['xtick.major.width'] = 1
rcParams['ytick.major.width'] = 1
rcParams['xtick.minor.size'] = 4
rcParams['ytick.minor.size'] = 4
rcParams['xtick.major.size'] = 5
rcParams['ytick.major.size'] = 5
rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'
rcParams['xtick.top'] = True
rcParams['ytick.right'] = True
rcParams['mathtext.fontset'] = 'cm'

LITTLE_H = 0.6777
#box size should be saved and read in
BOX_SIZE = 67.77
#SCALE_A  = 1

def load_halo_stars(particle_data_location, galaxy_data_location,
                    snap, name, z0group=1, z0subgroup=0):
  '''Finds the top leaf id of the galaxy that corresponds to the z=0 input group
  and subgroup,
  then finds the galaxy with that top leaf id at the given snapshot,
  then returns the star particle data.
  '''
  #read z=0 halos
  file_name = '%s%s_%s_galaxies.hdf5'%(galaxy_location, '028_z000p000', name)
  z0_data = h5.File(file_name,'r')

  #get ids
  z0_group_numbers    = z0_data['/IDs/GroupID'][()]
  z0_subgroup_numbers = z0_data['/SubHaloData/SubHaloID'][()]
  z0_top_leaf_numbers = z0_data['/IDs/TopLeafID'][()]

  z0_data.close()

  #work out top leafid
  top_leaf_id = None

  for index, (group, subgroup) in enumerate(zip(z0_group_numbers, z0_subgroup_numbers)):
    if group - 28000000000000 + 1 == z0group:
      if subgroup == z0subgroup:
        top_leaf_id = z0_top_leaf_numbers[index]

  if top_leaf_id == None:
    raise ValueError('No such group / subgroup combination.')

  #read halos
  file_name = '%s%s_%s_galaxies.hdf5'%(galaxy_location, snap, name)
  galaxy_data = h5.File(file_name,'r')

  group_numbers     = galaxy_data['/IDs/GroupID'][()]
  subgroup_numbers  = galaxy_data['/SubHaloData/SubHaloID'][()]

  top_leaf_numbers  = galaxy_data['/IDs/TopLeafID'][()]
  galaxy_numbers    = galaxy_data['/IDs/GalaxyID'][()]
  last_prog_numbers = galaxy_data['/IDs/LastProgID'][()]
  # decendant_numbers = galaxy_data['/IDs/DecendantID'][()]

  cops = galaxy_data['SubHaloData/Pos_cop'][()]

  #work out group and subgroup id
  group_id            = None
  subgroup_id         = None
  centre_of_potential = None

  galaxy_id    = None
  last_prog_id = None
  decendant_id = None

  for index, leaf in enumerate(top_leaf_numbers):
    if leaf == top_leaf_id:
      if group_id != None:
        raise ValueError('Top leaf galaxy has multiple galaxies.')

      group_id            = (group_numbers[index]  % 1000000) + 1
      subgroup_id         = subgroup_numbers[index]
      centre_of_potential = cops[index]

      galaxy_id    = galaxy_numbers[index]
      last_prog_id = last_prog_numbers[index]
      # decendant_id = decendant_numbers[index]

  #TODO this error should probably be handeled ...
  if group_id == None:
    raise ValueError('Top leaf galaxy vanished.')

  SCALE_A = galaxy_data['/Header/a'][()]

  galaxy_data.close()

  #stars
  file_name = '%s%s_%s_Star.hdf5'%(particle_data_location, snap, name)
  star_data = h5.File(file_name, 'r')

  star_group    = star_data['PartData/GrpNum_Star'][()]
  star_subgroup = star_data['PartData/SubNum_Star'][()]

  #TODO this should use kdtrees to speed up.
  star_mask = np.logical_and((group_id    == star_group),
                             (subgroup_id == star_subgroup))
  #use kdtrees!

  #load masked quantities
  star_pos  = star_data['PartData/PosStar'][()][star_mask]
  star_vel  = star_data['PartData/VelStar'][()][star_mask]
  star_mass = star_data['PartData/MassStar'][()][star_mask]

  star_data.close()

  #star_bind = star_data['PartData/ParticleBindingEnergy'][()][star_mask] * SCALE_A #(km/s)^2
  star_pos  = star_pos - centre_of_potential - BOX_SIZE/2
  star_pos  %= BOX_SIZE
  star_pos  -= BOX_SIZE/2
  star_pos  *= 1000 * SCALE_A / LITTLE_H #kpc
  star_vel  *= np.sqrt(SCALE_A) #km/s
  star_mass /= LITTLE_H #10^10 M_sun
  #star_bind = star_data['PartData/ParticleBindingEnergy'][()][star_mask] * SCALE_A #(km/s)^2

  return(top_leaf_id, galaxy_id, last_prog_id, decendant_id,
         star_pos, star_vel, star_mass)

def get_cylindrical(PosStars, VelStars):
  '''Calculates cylindrical coordinates.
  '''
  rho      = np.sqrt(PosStars[:,0]**2 + PosStars[:,1]**2)
  varphi   = np.arctan2(PosStars[:,1], PosStars[:,0])
  z        = PosStars[:,2]

  v_rho    = VelStars[:,0] * np.cos(varphi) + VelStars[:,1] * np.sin(varphi)
  v_varphi = -VelStars[:,0]* np.sin(varphi) + VelStars[:,1] * np.cos(varphi)

  v_z      = VelStars[:,2]

  return(rho, varphi, z, v_rho, v_varphi, v_z)

def find_rotaion_matrix(j_vector):
  '''Returns a scipy.spatial.transform.Rotation object.
  R = find_rotaton_matrix(galaxy_anular_momentum)
  pos = R.apply(pos)
  '''
  #rotate until x coord = 0
  fy = lambda y : Rotation.from_euler('y', y, degrees=True).apply(j_vector)[0]
  y = brentq(fy, 0, 180)

  #rotate until y coord = 0
  fx = lambda x : Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)[1]
  x = brentq(fx, 0, 180)

  #check it isn't upsidedown
  j_tot = Rotation.from_euler('yx', [y,x], degrees=True).apply(j_vector)

  if j_tot[2] < 0:
    x += 180

  return(Rotation.from_euler('yx', [y,x], degrees=True))

def align(pos, vel, mass, apature=30):
  '''Aligns the z cartesian direction with the direction of angular momentum.
  Can use an apature.
  '''

  #direction to align
  if apature != None:
    r = np.linalg.norm(pos, axis=1)
    mask = r < apature

    j_tot = np.sum(np.cross(pos[mask], vel[mask]) * mass[mask, np.newaxis], axis=0)

  else:
    j_tot = np.sum(np.cross(pos, vel) * mass[:, np.newaxis], axis=0)

  #find rotation
  rotation = find_rotaion_matrix(j_tot)

  #rotate stars
  pos = rotation.apply(pos)
  vel = rotation.apply(vel)

  return(pos, vel)

def plot_projection(PosStars, MassStars,
                    top_leaf_id, snap, galaxy_id, last_prog_id, decendant_id, sim):
  '''Plot
  '''
  xlim=[-30,30]
  bins = np.linspace(xlim[0], xlim[1], 121) #61

  fig = plt.figure()
  fig.set_size_inches(5, 8, forward=True)

  ax0 = plt.subplot(211)
  ax1 = plt.subplot(212)

  fig.subplots_adjust(hspace=0.03,wspace=0)

  #Calculate the correct colors
  (mass_xy, _,_,_
   ) = binned_statistic_2d(PosStars[:,1], PosStars[:,0], values=MassStars,
                           statistic='sum', bins=bins)
  (mass_xz, _,_,_
   ) = binned_statistic_2d(PosStars[:,2], PosStars[:,0], values=MassStars,
                           statistic='sum', bins=bins)

  #EAGLE mass unit is 10^10 M_sun. Convert back to M_sun
  units = 10

  min_mass = np.amin(MassStars)
  log_mass_xy = np.log10(mass_xy + min_mass) + units
  log_mass_xz = np.log10(mass_xz + min_mass) + units

  min_mass = np.log10(min_mass) + units
  max_cell = np.amax((log_mass_xy, log_mass_xz))

  _     = ax0.pcolormesh(bins, bins, log_mass_xy, vmin=min_mass, vmax=max_cell)
  image = ax1.pcolormesh(bins, bins, log_mass_xz, vmin=min_mass, vmax=max_cell)

  ax1.text(-28,25,  'Snap='       +str(snap),         color='lightgrey', fontsize=16)
  ax1.text(-28,20,  'TopLeafID='  +str(top_leaf_id),  color='lightgrey', fontsize=16)
  ax1.text(-28,15,  'GalaxyID='   +str(galaxy_id),    color='lightgrey', fontsize=16)
  ax1.text(-28,-23, 'LastProgID=' +str(last_prog_id), color='lightgrey', fontsize=16)
  ax1.text(-28,-28, 'DecendantID='+str(decendant_id), color='lightgrey', fontsize=16)

  ax0.set_ylabel('y [kpc]')
  ax1.set_ylabel('z [kpc]')
  ax1.set_xlabel('x [kpc]')
  ax0.set_xticklabels([])

  cbar_ax = fig.add_axes([0.84, 0.11, 0.02, 0.77])
  cbar = fig.colorbar(image, cax=cbar_ax)
  cbar.set_label(r'$\log_{10} $ M$_\odot $')

  ax0.set_aspect('equal')
  ax1.set_aspect('equal')

  fname = '/home/mwilkins/EAGLE/plots/' + sim + '/topleaf' + str(top_leaf_id) + 'snap' + snap
  plt.savefig(fname, bbox_inches="tight")
  plt.close()

  return

if __name__ == '__main__':

  sim = 'L0100N1504/REFERENCE'
  particle_data_location = '/fred/oz009/ejimenez/' + sim + '/'
  galaxy_location        = '/fred/oz009/ejimenez/Galaxies/' + sim + '/'

  name = '100Mpc_1504'

  snap_list = ['027_z000p101', '026_z000p183', '025_z000p271',
               '024_z000p366', '023_z000p503', '022_z000p615', '021_z000p736',
               '020_z000p865', '019_z001p004', '018_z001p259', '017_z001p487',
               '016_z001p737', '015_z002p012']

  group_list = [10,100,1000]

  for group in group_list:
    for snap in snap_list:

      try:

        (top_leaf_id, galaxy_id, last_prog_id, decendant_id,
         star_pos, star_vel, star_mass
         ) = load_halo_stars(particle_data_location, galaxy_location,
                             snap, name, z0group=group, z0subgroup=0)

        (star_pos, star_vel) = align(star_pos, star_vel, star_mass, apature=30)

        plot_projection(star_pos, star_mass,
                        top_leaf_id, snap, galaxy_id, last_prog_id, decendant_id, sim)

      except ValueError:
        pass

  pass