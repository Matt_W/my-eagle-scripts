#!/bin/bash
# # Lista de parametros a imprimir, NO poner GalaxyID, incluido por defecto
# rm *temp
# set para = "LastProgID,TopLeafID,DescendantID,GroupID,GroupNumber,SubGroupNumber"
# # set para = "DHaloID,mdhalo,stellarmass,type,LastProgenitorID,MainLeafID"
# 
# # Nombre del archivo de Salida, la extencion por defecto es .csv, los resultados estaran separados
# # por comas, aplicar sed -i 's/,/ /g' Salida.csv
# set output = "Tree_028"
# sh MSqueryMulti_Gal.sh "select "$para" from Eagle..RefL0100N1504_Subhalo where snapnum = 28 and MassType_Star > 1e9" galaxyID $output.csv
# 
# exit


#for i in $(seq 14 28)
#do
#   wget --http-user=dnp388 --http-passwd=ievKSFza "http://galaxy-catalogue.dur.ac.uk:8080/Eagle?action=doQuery&SQL=select 
#	SUB.GalaxyID,
#	SUB.LastProgID,
#	SUB.TopLeafID,
#	SUB.DescendantID,
#	SUB.GroupNumber, 
#	SUB.SubGroupNumber, 
#	SUB.BlackHoleMass,
#	SUB.BlackHoleMassAccretionRate,
#	SUB.CentreOfPotential_x, 
#	SUB.CentreOfPotential_y, 
#	SUB.CentreOfPotential_z, 
#	SUB.Mass,
#	SUB.MassType_DM,
#	SUB.MassType_Gas,
#	SUB.MassType_Star,
#	SUB.MassType_BH,
#	SUB.Snapnum
#   from 
#   	Eagle..RefL0100N1504_Subhalo as SUB 
#   where 
#   	SUB.Snapnum = $i and 
#   	SUB.MassType_Star > 1e7
#   ORDER BY 
#   	SUB.GroupNumber,
#   	SUB.SubGroupNumber" -O Merger_0$i.csv
#done

for i in 19
do
   wget --http-user=dnp388 --http-passwd=ievKSFza "http://galaxy-catalogue.dur.ac.uk:8080/Eagle?action=doQuery&SQL=select 
	SUB.Mass,
	SUB.MassType_DM,
	SUB.MassType_Gas,
	SUB.MassType_Star
   from 
   	Eagle..RefL0100N1504_Subhalo as SUB 
   where 
   	SUB.Snapnum = $i and 
   	SUB.MassType_Star > 1e7
   ORDER BY 
   	SUB.Mass" -O mass_function_0$i.csv
done

# for i in $(seq 14 28)
# do
#    wget --http-user=dnp388 --http-passwd=ievKSFza "http://galaxy-catalogue.dur.ac.uk:8080/Eagle?action=doQuery&SQL=select GalaxyID, LastProgID, TopLeafID, DescendantID, GroupID, GroupNumber, SubGroupNumber, snapnum, redshift from Eagle..RefL0100N1504_Subhalo where snapnum=$i and MassType_Star > 1e9" -O Tree_0$i.csv
# done


#wget --http-user=dnp388 --http-passwd=ievKSFza "http://galaxy-catalogue.dur.ac.uk:8080/Eagle?action=doQuery&SQL=select GalaxyID, LastProgID, TopLeafID, DescendantID, GroupID, GroupNumber, SubGroupNumber from Eagle..RefL0100N1504_Subhalo where snapnum=14 and MassType_Star > 1e9" -O Tree_014.csv
